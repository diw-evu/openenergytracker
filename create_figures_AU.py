#%%###########################################################################################
# Import packages
##############################################################################################

from pathlib import Path

import numpy as np
import pandas as pd

from sklearn.linear_model import LinearRegression

import plotly.graph_objs as go
from plotly.subplots import make_subplots

import shutil

# update pending Jan 2024

#%%###########################################################################################
# Create directory
##############################################################################################

Path("docs/australia/figures").mkdir(parents=True, exist_ok=True)

#%%###########################################################################################
# Generic / utility functions
##############################################################################################

#--------------------------------------------------------------------------------------------
# Create linear projections based on the historical data (i.e. y = mx + c)
# df = pd.DataFrame [TimeStamp,'X','historical_data' and future NaNs]
#--------------------------------------------------------------------------------------------

def historicalProjection(df, sample_years, max_value = np.nan, min_value = np.nan):
    df_projection = pd.DataFrame()

    # Determine the years in the jhistorical record
    df_reg_data = df.dropna()
    hist_year_end = df_reg_data.iat[-1,0].year
    hist_year_start = hist_year_end - sample_years + 1
    projection_name = str(hist_year_start) + "-" + str(hist_year_end)

    # Determine the slope of the historical record (i.e find m)
    df_reg_data = df_reg_data[(df_reg_data.iloc[:,0] >= str(hist_year_start)) & (df_reg_data.iloc[:,0] <= str(hist_year_end))]
    df_reg_x    = np.array(df_reg_data['X']).reshape((-1, 1))
    df_reg      = LinearRegression().fit(df_reg_x, df_reg_data.iloc[:,-1])

    # Calculate the projection into the future (i.e. y = mx + c)
    df_projection = df.copy()
    df_projection = df_projection[(df_projection.iloc[:,0] >= str(hist_year_end))].reset_index()
    df_projection['index'] = df_projection.index
    df_projection[projection_name] = df_projection.iat[0,-1] + df_projection['index'] * df_reg.coef_
    df_projection = df_projection[[df_projection.columns[1],projection_name]]

    # Clip using the maximum or minimum (if configured to do so)
    if not(np.isnan(max_value)):
        df_projection.loc[df_projection[projection_name] > max_value, projection_name] = max_value
    if not(np.isnan(min_value)):
        df_projection.loc[df_projection[projection_name] < min_value, projection_name] = min_value
    
    return df_projection

#--------------------------------------------------------------------------------------------
# Create linear projections based on future plan values (i.e. y = mx + c)
# df = pd.DataFrame [TimeStamp,'X','historical_data' and future NaNs, 'planned']
#--------------------------------------------------------------------------------------------

def plannedProjection(df):
    df_projection = pd.DataFrame()

    df_actuals = df.iloc[:,0:3].dropna()
    start_value = df_actuals.iat[-1,2]
    start_X = df_actuals.iat[-1,1]

    # Determine the number of planned elements
    df_plan = df.iloc[:,[0,1,3]].dropna()

    # Setup the projection dataframe
    df_projection = df.copy()
    df_projection.loc[:,'projection'] = np.nan

    # Create a linear projection between the actuals and planned indexes
    for i in range(len(df_plan)):
        df_plan_i = df_plan.iloc[i]
        if start_X < df_plan_i.X:
            X_span = df_plan_i.X - start_X
            df_projection.loc[start_X:df_plan_i.X,'projection'] = np.linspace(start_value,df_plan_i[2],X_span+1)
            start_X = df_plan_i.X
            start_value = df_plan_i[2]

    return df_projection.iloc[:,[0,-1]]

#%%###########################################################################################
#                                                             [Section]: Renewable Electricity
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#%%###########################################################################################
# Renewable Electricity Source (RES) shares
##############################################################################################

rs_data = pd.read_csv("docs/australia/data/resshares.csv", sep = ",")
rs_data['date'] = pd.to_datetime(rs_data['date'], format='%Y')
rs_data['X'] = rs_data.index

# Determine the historical projection
predict1 = historicalProjection(rs_data[['date','X','actual_bsv']], 5, 100)
rs_data = rs_data.merge(predict1, how='left', on='date')

# Project path to government ambition
plan1 = plannedProjection(rs_data[['date','X','actual_bsv','plan']])
rs_data = rs_data.merge(plan1, how='left', on='date')

# create Renewable Energy Source shares figure
# --------------------------------------------

def create_resshares(data,title,legend_title_achieved,
                     reached_consumption,
                     linear,trend_2017_2021,
                     goal,percentage,
                     til_2030,til_2050,
                     legend_title_trends,hist_5yr_projection,plan_prj
                     ):

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['actual_bsv'],
                         name = reached_consumption,
                         marker_color = '#ff7f0e',
                         hovertemplate = '%{y:.1f}%',
                         legendgroup = 'achieved',
                         legendgrouptitle_text = legend_title_achieved))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             mode = "markers", name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.0f}%',
                             legendgroup = 'trends',
                             legendgrouptitle_text = legend_title_trends))

    fig.add_trace(go.Scatter(x = data['date'], y = data[plan_prj],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'trends'))

    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr_projection],
                             name = hist_5yr_projection + trend_2017_2021,
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legendgroup = 'achieved'))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0.015, y = 1,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2009-06", "2030-12"],
                            'yaxis.range': [0, 100]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2009-06", "2050-12"],
                            'yaxis.range': [0, 110]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = percentage, range = [0,100])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2009-06","2030-12"],
                     tickvals = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042,
                                 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050],
                     ticktext = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040', '2041',
                                '2042', '2043', '2044', '2045', '2046', '2047', '2048', '2049',
                                '2050'])
    
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            margin=dict(l=0, r=0, t=40, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = 'x',
                            font=dict(family = "'sans-serif', 'arial'", size=14 ,color='#000000'),
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {'traceorder':'grouped', 'orientation':'v',
                                      'x': 0.015, 'y': .875,
                                      'yanchor': 'top', 'xanchor': 'left',
                                      'grouptitlefont_size': 13,
                                      'font': {'size': 12, 'color': '#000000'},
                                      'bgcolor': 'rgba(255,255,255,0)',
                                      'borderwidth': 1},
                            modebar_remove=['lasso','autoScale']
                            )

    return fig

fig_rs_en = create_resshares(rs_data, "Share of renewable energy in the power sector",
                             "Actual", "Electricity generation (Net)",
                             "Linear path to Step Change",
                             " projection",
                             "Step Change (AEMO ISP 2022)", "Percentage (%)",
                             "until 2030","until 2050",
                             "Goals",
                             predict1.columns[-1],plan1.columns[-1]
                             )

# fig_rs_de = create_resshares(rs_data, "Anteil erneuerbarer Energien im Stromsektor",
#                              "Tatsächlich erreichter Anteil",
#                              "Bruttostromverbrauch",
#                              "Nettostromerzeugung","Linearisierter Verlauf",
#                              "2017-2021 (Anteil Bruttostromverbrauch)",
#                              "Ziele der Koalition", "Legislaturperiode", "Prozent",
#                              "Nur Legislaturperiode","bis 2030","bis 2045",
#                              "Historische Trends",
#                              "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                              "Szenariokorridor")
# fig_rs_fr = create_resshares(rs_data,
#                              "Part de l'énergie renouvelable dans le secteur de l'électricité",
#                              "Part d'électricité atteinte ",
#                              "Consommation brute (CB)",
#                              "Production nette",
#                              "Progression linéarisée",
#                              "2017-2021 (CB)",
#                              "Objectifs de la coalition","Période du mandat","Pourcentage",
#                              "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                              "Tendances historiques",
#                              "Scénarios Ariadne","Modèle de référence",
#                              "Modèle de référence (tendance)",
#                              "Intervalle de scénarios")

fig_rs_en.write_html("docs/australia/figures/resshares.en.html", include_plotlyjs="directory")
# fig_rs_de.write_html("docs/australia/figures/resshares.de.html", include_plotlyjs="directory")
# fig_rs_fr.write_html("docs/australia/figures/resshares.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Solar PV capacity (all)
##############################################################################################

# Read pv data
# ------------
pv_data = pd.read_csv("docs/australia/data/pv.csv", sep = ",", thousands = ",")
pv_data['year']  = pd.to_datetime(pv_data['year'], format='%Y')
pv_data['distr_MW'] = pv_data['small_MW'] + pv_data['medium_MW'] 
pv_data['all_MW'] = pv_data['small_MW'] + pv_data['medium_MW'] + pv_data['large_MW']  
pv_data['X'] = pv_data.index

# Determine the historical projection
predict1 = historicalProjection(pv_data[['year','X','all_MW']], 5)
pv_data = pv_data.merge(predict1, how='left', on='year')

# Create PV (all) figure
# ----------------------
def create_all_pv_fig(data, title, small_scale, medium_scale, large_scale,
                      til_2030, til_2050,
                      legend_title_installed,
                      legend_title_trends,
                      legend_title_goals,
                      forecast_name1,
                      y_axis_title,
                      y_max_2030, y_max_2050, hist_5yr, projection):
               
    fig = go.Figure()

    # Calculate annual capacity additionals
    data_additions = data.copy()
    data_additions = data_additions.map(lambda x: np.nan)
    data_additions['year'] = data['year']
    predict_colname = data_additions.columns[-1]
    for i in range(len(data_additions)):
        if i == 0:
            small_MW_now = data.at[i, 'small_MW']
            data_additions.at[i, 'small_MW'] = small_MW_now
            medium_MW_now = data.at[i, 'medium_MW']
            data_additions.at[i, 'medium_MW'] = medium_MW_now
            large_MW_now = data.at[i, 'large_MW']
            data_additions.at[i, 'large_MW'] = large_MW_now
            isp_step_change_large_now = data.at[i, 'isp_step_change_large']
            isp_step_change_distr_now = data.at[i, 'isp_step_change_distributed']
            predict_now = data.at[i,predict_colname]
        else:
            small_MW_prev = small_MW_now
            small_MW_now = data.at[i, 'small_MW']
            if not(np.isnan(small_MW_now)):
                data_additions.at[i, 'small_MW'] = small_MW_now - small_MW_prev
            medium_MW_prev = medium_MW_now
            medium_MW_now = data.at[i, 'medium_MW']
            if not(np.isnan(medium_MW_now)):
                data_additions.at[i, 'medium_MW'] = medium_MW_now - medium_MW_prev
            large_MW_prev = large_MW_now
            large_MW_now = data.at[i, 'large_MW']
            if not(np.isnan(large_MW_now)):
                data_additions.at[i, 'large_MW'] = large_MW_now - large_MW_prev
            isp_step_change_large_prev = isp_step_change_large_now
            isp_step_change_large_now = data.at[i, 'isp_step_change_large']
            if not(np.isnan(isp_step_change_large_prev)):
                if not(np.isnan(isp_step_change_large_now)):
                    data_additions.at[i, 'isp_step_change_large'] = isp_step_change_large_now - isp_step_change_large_prev
            isp_step_change_distr_prev = isp_step_change_distr_now
            isp_step_change_distr_now = data.at[i, 'isp_step_change_distributed']
            if not(np.isnan(isp_step_change_distr_prev)):
                if not(np.isnan(isp_step_change_distr_now)):
                    data_additions.at[i, 'isp_step_change_distributed'] = isp_step_change_distr_now - isp_step_change_distr_prev
            predict_prev = predict_now
            predict_now = data.at[i, predict_colname]
            if not(np.isnan(predict_prev)):
                if not(np.isnan(predict_now)):
                    data_additions.at[i, predict_colname] = predict_now - predict_prev

    # Cumulative installed capacity
    fig.add_trace(go.Bar(x = data['year'], y = data['small_MW']/1000, name = small_scale,
                         marker_color='#FFB400', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Bar(x = data['year'], y = data['medium_MW']/1000, name = medium_scale,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc"))
    fig.add_trace(go.Bar(x = data['year'], y = data['large_MW']/1000, name = large_scale,
                         marker_color='#FF2D00', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible='legendonly',
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['year'],
                             y = (data['isp_step_change_distributed']+data['isp_step_change_large'])/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    # Annual capacity additionals
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['small_MW']/1000, name = small_scale,
                         marker_color='#FFB400', hovertemplate = '%{y:.2f}GW',
                         visible = False,
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['medium_MW']/1000, name = medium_scale,
                         visible = False,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc"))
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['large_MW']/1000, name = large_scale,
                         visible = False,
                         marker_color='#FF2D00', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible=False,
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['year'],
                             y = (data_additions['isp_step_change_distributed']+data_additions['isp_step_change_large'])/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             visible = False,
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    fig.update_layout(barmode='stack',
        updatemenus=[
            dict(
                type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                xanchor="left", yanchor="top", x = 0, y = 1.10,
                buttons=list([
                    dict(label=til_2030,
                        method="relayout",
                        args=[{'xaxis.range': ["2009", "2031"],
                                'yaxis.range': [0, y_max_2030]}]),
                    dict(label=til_2050,
                        method="relayout",
                        args=[{'xaxis.range': ["2009", "2051"],
                                'yaxis.range': [0, y_max_2050]}]),
                    ]),
            ),
            dict(
                type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                xanchor="left", yanchor="top", x = 0, y = 1.20,
                buttons=list([
                    dict(label="Cumulative", 
                        method="update", 
                        args=[{"visible": [True,True,True,'legendonly',True,False,False,False,False,False]}]),
                    dict(label="per Year", 
                        method="update", 
                        args=[{"visible": [False,False,False,False,False,True,True,True,'legendonly',True]}]),
                ]),                
            ),
            ]
        )

    if title is None:
        title = ""
        pad_top = 0
    else:
        pad_top = 120

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2009", "2031"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=pad_top, b=0, pad=0),
        xaxis_hoverformat='%Y',
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'v',
                  'yanchor': 'top', 'xanchor': 'left',
                  'grouptitlefont_size': 13,
                  'font': {'size': 12},
                  'x': 0.015, 'y': 1,
                  'bgcolor': 'rgba(255,255,255,0)',
                  'borderwidth': 1
                  },
        modebar_remove=['lasso','autoScale'])

    return fig

fig_pv_en = create_all_pv_fig(pv_data, None,
                              "Small-scale and rooftop", "Medium-scale", "Utility",
                              "until 2030","until 2050",
                              "Installed",
                              "Trends",
                              "Goals",
                              "Step Change (AEMO ISP 2022)",
                              "Gigawatt (GW)",
                              65,150, predict1.columns[-1],
                              " projection")

# fig_pv_de = create_all_pv_fig(pv_data,"Installierte Leistung Photovoltaik","Legislaturperiode",
#                           "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
#                           "Linearisierter Verlauf", "Ziele der Koalition",
#                           "Nur Legislaturperiode", "bis 2030","bis 2045",
#                           "Historische Trends",
#                           "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                           "Szenariokorridor","Gigawatt",
#                           140,250,550)
# fig_pv_fr = create_all_pv_fig(pv_data,"Capacité photovoltaïque installée","Période du mandat",
#                           "Déjà installée","2017-2021","12 derniers mois",
#                           "Progression linéarisée","Objectifs de la coalition",
#                           "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                           "Tendances historiques",
#                           "Scénarios Ariadne","Modèle de référence",
#                           "Modèle de référence (tendance)",
#                           "Intervalle de scénarios","Gigawatt",
#                           140,250,550)

fig_pv_en.write_html("docs/australia/figures/pv.en.html", include_plotlyjs="directory")
# fig_pv_de.write_html("docs/australia/figures/pv.de.html", include_plotlyjs="directory")
# fig_pv_fr.write_html("docs/australia/figures/pv.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Solar PV capacity (distributed)
##############################################################################################

distr_pv_data = pd.read_csv("docs/australia/data/pv.csv", sep = ",", thousands = ",")
distr_pv_data['year']  = pd.to_datetime(distr_pv_data['year'], format='%Y')
distr_pv_data['distr_MW'] = distr_pv_data['small_MW'] + distr_pv_data['medium_MW'] 
distr_pv_data['X'] = distr_pv_data.index

# Determine the historical projection
predict1 = historicalProjection(distr_pv_data[['year','X','distr_MW']], 5)
distr_pv_data = distr_pv_data.merge(predict1, how='left', on='year')

# Create distributed PV figure
# ----------------------------
def create_distr_pv_fig(data,title,small_scale,medium_scale,
                        til_2030,til_2050,
                        legend_title_installed,
                        legend_title_trends,
                        legend_title_goals,
                        forecast_name1,
                        y_axis_title,
                        y_max_2030,y_max_2050,hist_5yr,projection):

    # Calculate annual capacity additionals
    data_additions = data.copy()
    data_additions = data_additions.map(lambda x: np.nan)
    data_additions['year'] = data['year']
    predict_colname = data_additions.columns[-1]
    for i in range(len(data_additions)):
        if i == 0:
            small_MW_now = data.at[i, 'small_MW']
            data_additions.at[i, 'small_MW'] = small_MW_now
            medium_MW_now = data.at[i, 'medium_MW']
            data_additions.at[i, 'medium_MW'] = medium_MW_now
            isp_step_change_distr_now = data.at[i, 'isp_step_change_distributed']
            predict_now = data.at[i,predict_colname]
        else:
            small_MW_prev = small_MW_now
            small_MW_now = data.at[i, 'small_MW']
            if not(np.isnan(small_MW_now)):
                data_additions.at[i, 'small_MW'] = small_MW_now - small_MW_prev
            medium_MW_prev = medium_MW_now
            medium_MW_now = data.at[i, 'medium_MW']
            if not(np.isnan(medium_MW_now)):
                data_additions.at[i, 'medium_MW'] = medium_MW_now - medium_MW_prev
            isp_step_change_distr_prev = isp_step_change_distr_now
            isp_step_change_distr_now = data.at[i, 'isp_step_change_distributed']
            if not(np.isnan(isp_step_change_distr_prev)):
                if not(np.isnan(isp_step_change_distr_now)):
                    data_additions.at[i, 'isp_step_change_distributed'] = isp_step_change_distr_now - isp_step_change_distr_prev
            predict_prev = predict_now
            predict_now = data.at[i, predict_colname]
            if not(np.isnan(predict_prev)):
                if not(np.isnan(predict_now)):
                    data_additions.at[i, predict_colname] = predict_now - predict_prev

    fig = go.Figure()

    # Cumulative install capacities
    fig.add_trace(go.Bar(x = data['year'], y = data['small_MW']/1000, name = small_scale,
                         marker_color='#FFB400', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Bar(x = data['year'], y = data['medium_MW']/1000, name = medium_scale,
                         marker_color='#FF7F0E', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible='legendonly',
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['year'], y = data['isp_step_change_distributed']/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    # Annual capacity additionals
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['small_MW']/1000, name = small_scale,
                         marker_color='#FFB400', hovertemplate = '%{y:.2f}GW',
                         visible = False,
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['medium_MW']/1000, name = medium_scale,
                         marker_color='#FF7F0E', hovertemplate = '%{y:.2f}GW',
                         visible = False,
                         legendgroup="misc"))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible=False,
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions['isp_step_change_distributed']/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             visible = False,
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    fig.update_layout(barmode='stack',
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.20,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2031"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2051"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            ),
            dict(
                type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                xanchor="left", yanchor="top", x = 0, y = 1.36,
                buttons=list([
                    dict(label="Cumulative", 
                        method="update", 
                        args=[{"visible": [True,True,'legendonly',True,False,False,False,False]}]),
                    dict(label="per Year", 
                        method="update", 
                        args=[{"visible": [False,False,False,False,True,True,'legendonly',True]}]),
                ]),                
            ),
            ]
        )

    if title is None:
        title = ""
        pad_top = 0
    else:
        pad_top = 120

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2009", "2031"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=pad_top, b=0, pad=0),
        xaxis_hoverformat='%Y',
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'v',
                  'x': 0.015, 'y': 1,
                  'yanchor': 'top', 'xanchor': 'left',
                  'grouptitlefont_size': 13,
                  'font': {'size': 12},
                  'bgcolor': 'rgba(255,255,255,0)',
                  'borderwidth': 1},
        modebar_remove=['lasso','autoScale'])

    return fig

fig_pv_en = create_distr_pv_fig(distr_pv_data,None,
                          "Small-scale and rooftop PV","Medium-scale PV",
                          "until 2030","until 2050",
                          "Installed",
                          "Trends",
                          "Goals",
                          "Step Change (AEMO ISP 2022)","Gigawatt (GW)",
                          50,110,predict1.columns[-1]," projection")

# fig_pv_de = create_distr_pv_fig(pv_data,"Installierte Leistung Photovoltaik","Legislaturperiode",
#                           "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
#                           "Linearisierter Verlauf", "Ziele der Koalition",
#                           "Nur Legislaturperiode", "bis 2030","bis 2045",
#                           "Historische Trends",
#                           "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                           "Szenariokorridor","Gigawatt",
#                           140,250,550)
# fig_pv_fr = create_distr_pv_fig(pv_data,"Capacité photovoltaïque installée","Période du mandat",
#                           "Déjà installée","2017-2021","12 derniers mois",
#                           "Progression linéarisée","Objectifs de la coalition",
#                           "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                           "Tendances historiques",
#                           "Scénarios Ariadne","Modèle de référence",
#                           "Modèle de référence (tendance)",
#                           "Intervalle de scénarios","Gigawatt",
#                           140,250,550)

fig_pv_en.write_html("docs/australia/figures/pv_distr.en.html", include_plotlyjs="directory")
# fig_pv_de.write_html("docs/australia/figures/pv_distr.de.html", include_plotlyjs="directory")
# fig_pv_fr.write_html("docs/australia/figures/pv_distr.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Solar PV capacity (utility)
##############################################################################################

util_pv_data = pd.read_csv("docs/australia/data/pv.csv", sep = ",", thousands = ",")
util_pv_data['year']  = pd.to_datetime(util_pv_data['year'], format='%Y')
util_pv_data['X'] = util_pv_data.index

# Determine the historical projection
predict1 = historicalProjection(util_pv_data[['year','X','large_MW']], 5)
util_pv_data = util_pv_data.merge(predict1, how='left', on='year')

data = util_pv_data

# Create utility PV figure
# ------------------------
def create_util_pv_fig(data,title,util_scale,
                       til_2030,til_2050,
                       legend_title_installed,
                       legend_title_trends,
                       legend_title_goals,
                       forecast_name1,
                       y_axis_title,
                       y_max_2030,y_max_2050,hist_5yr,projection):

    # Calculate annual capacity additionals
    data_additions = data.copy()
    data_additions = data_additions.map(lambda x: np.nan)
    data_additions['year'] = data['year']
    predict_colname = data_additions.columns[-1]
    for i in range(len(data_additions)):
        if i == 0:
            large_MW_now = data.at[i, 'large_MW']
            data_additions.at[i, 'large_MW'] = large_MW_now
            isp_step_change_large_now = data.at[i, 'isp_step_change_large']
            predict_now = data.at[i,predict_colname]
        else:
            large_MW_prev = large_MW_now
            large_MW_now = data.at[i, 'large_MW']
            if not(np.isnan(large_MW_now)):
                data_additions.at[i, 'large_MW'] = large_MW_now - large_MW_prev
            isp_step_change_large_prev = isp_step_change_large_now
            isp_step_change_large_now = data.at[i, 'isp_step_change_large']
            if not(np.isnan(isp_step_change_large_prev)):
                if not(np.isnan(isp_step_change_large_now)):
                    data_additions.at[i, 'isp_step_change_large'] = isp_step_change_large_now - isp_step_change_large_prev
            predict_prev = predict_now
            predict_now = data.at[i, predict_colname]
            if not(np.isnan(predict_prev)):
                if not(np.isnan(predict_now)):
                    data_additions.at[i, predict_colname] = predict_now - predict_prev

    fig = go.Figure()

    # Cumulative installed capacities
    fig.add_trace(go.Bar(x = data['year'], y = data['large_MW']/1000, name = util_scale,
                         marker_color='#FF2D00', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc",
                         legendgrouptitle_text = legend_title_installed))
    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible='legendonly',
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['year'], y = data['isp_step_change_large']/1000,
                             name = forecast_name1,
#                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    # Annual capacity additions
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['large_MW']/1000, name = util_scale,
                         marker_color='#FF2D00', hovertemplate = '%{y:.2f}GW',
                         visible=False,
                         legendgroup="misc",
                         legendgrouptitle_text = legend_title_installed))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible=False,
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions['isp_step_change_large']/1000,
                             name = forecast_name1,
#                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             visible=False,
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))
    
    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.2,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2031"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2051"],
                            'yaxis.range': [0, y_max_2050]}])
                    ]),
                ),
                dict(
                    type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                    xanchor="left", yanchor="top", x = 0, y = 1.36,
                    buttons=list([
                        dict(label="Cumulative", 
                            method="update", 
                            args=[{"visible": [True,'legendonly',True,False,False,False]}]),
                        dict(label="per Year", 
                            method="update", 
                            args=[{"visible": [False,False,False,True,'legendonly',True]}]),
                    ]),                
                ),
            ]
        )

    if title is None:
        title = ""
        pad_top = 0
    else:
        pad_top = 120

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2009", "2031"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=pad_top, b=0, pad=0),
        xaxis_hoverformat='%Y',
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'v',
                  'x': 0.015, 'y': 1,
                  'yanchor': 'top', 'xanchor': 'left',
                  'grouptitlefont_size': 13,
                  'font': {'size': 12},
                  'bgcolor': 'rgba(255,255,255,0)',
                  'borderwidth': 1},
        modebar_remove=['lasso','autoScale'])

    return fig

fig_pv_en = create_util_pv_fig(util_pv_data,None,
                               "Utility PV",
                               "until 2030","until 2050",
                               "Installed",
                               "Trends",
                               "Goals",
                               "Step Change (AEMO ISP 2022)","Gigawatt (GW)",
                               20,75,predict1.columns[-1]," projection")

# fig_pv_de = create_util_pv_fig(pv_data,"Installierte Leistung Photovoltaik","Legislaturperiode",
#                           "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
#                           "Linearisierter Verlauf", "Ziele der Koalition",
#                           "Nur Legislaturperiode", "bis 2030","bis 2045",
#                           "Historische Trends",
#                           "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                           "Szenariokorridor","Gigawatt",
#                           140,250,550)
# fig_pv_fr = create_util_pv_fig(pv_data,"Capacité photovoltaïque installée","Période du mandat",
#                           "Déjà installée","2017-2021","12 derniers mois",
#                           "Progression linéarisée","Objectifs de la coalition",
#                           "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                           "Tendances historiques",
#                           "Scénarios Ariadne","Modèle de référence",
#                           "Modèle de référence (tendance)",
#                           "Intervalle de scénarios","Gigawatt",
#                           140,250,550)

fig_pv_en.write_html("docs/australia/figures/pv_util.en.html", include_plotlyjs="directory")
# fig_pv_de.write_html("docs/australia/figures/pv_util.de.html", include_plotlyjs="directory")
# fig_pv_fr.write_html("docs/australia/figures/pv_util.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Rooftop PV with BESS penetration rate   
##############################################################################################

smallpv_bess_data = pd.read_csv("docs/australia/data/smallpv_bess.csv", sep = ",", thousands=",")
smallpv_bess_data['year']  = pd.to_datetime(smallpv_bess_data['year'], format='%Y')
smallpv_bess_data['percentage'] = smallpv_bess_data['bess_installs'] / smallpv_bess_data['smallpv_install'] * 100
smallpv_bess_data['X'] = smallpv_bess_data.index

# Determine the historical projection
predict1 = historicalProjection(smallpv_bess_data[['year','X','percentage']], 5)
smallpv_bess_data = smallpv_bess_data.merge(predict1, how='left', on='year')

# Create PV and BESS figure
# -------------------------
def create_smallpv_bess_fig(data,title,besspercentage,
                  legend_title_trends,
                  y_axis_title,
                  y_max_2030,hist_5yr,projection,title_on):

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['year'], y = data['percentage'], name = besspercentage,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.1f}%',
                         legendgroup="misc",
                         legendgrouptitle_text="Actual"))

    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr],
                             name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legendgroup="trends",mode="lines",
                             legendgrouptitle_text = legend_title_trends))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.3,
            )
            ]
        )

    if not(title_on):
        title = ""
        pad_top = 0
    else:
        pad_top = 120
    
    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2013", "2031"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
#        plot_bgcolor='#F2F3F4',
        margin=dict(l=0, r=0, t=pad_top, b=0, pad=0),
        xaxis_hoverformat='%Y',
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'h',
                  'x': 0.0, 'y': -0.8,
                  'yanchor': 'bottom', 'xanchor': 'left'})

    return fig

fig_smallpv_bess_en = create_smallpv_bess_fig(smallpv_bess_data,"Percentage of new rooftop PV with BESS",
                          "Percentage",
                          "Trends",
                          "Percentage (%)",
                          20,predict1.columns[-1]," projection", False)
# fig_smallpv_bess_de = create_smallpv_bess_fig(windon_data,"Installierte Leistung Windkraft an Land",
#                                "Legislaturperiode",
#                                "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
#                                "Linearisierter Verlauf", "Ziele der Koalition",
#                                "Nur Legislaturperiode", "bis 2030","bis 2045",
#                                "Historische Trends",
#                                "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                                "Szenariokorridor","Gigawatt",
#                                100,150,250)
# fig_smallpv_bess_fr = create_smallpv_bess_fig(windon_data,"Capacité installée - éolien terrestre",
#                                "Période du mandat",
#                                "Déjà installée","2017-2021","12 derniers mois",
#                                "Progression linéarisée","Objectifs de la coalition",
#                                "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                                "Tendances historiques",
#                                "Scénarios Ariadne","Modèle de référence",
#                                "Modèle de référence (tendance)",
#                                "Intervalle de scénarios","Gigawatts",
#                                100,150,250)

fig_smallpv_bess_en.write_html("docs/australia/figures/smallpv_bess.en.html", include_plotlyjs="directory")
# fig_smallpv_bess_de.write_html("docs/australia/figures/smallpv_bess.en.html", include_plotlyjs="directory")
# fig_smallpv_bess_fr.write_html("docs/australia/figures/smallpv_bess.en.html", include_plotlyjs="directory")

#%%###########################################################################################
# Wind onshore  
##############################################################################################

windon_data = pd.read_csv("docs/australia/data/wind.csv", sep = ",", thousands=",")
windon_data['year'] = pd.to_datetime(windon_data['year'], format='%Y')
windon_data['X'] = windon_data.index

# Determine the historical projection
predict1 = historicalProjection(windon_data[['year','X','wind_onshore']], 5)
windon_data = windon_data.merge(predict1, how='left', on='year')

# Create wind figure
# ------------------
def create_wind_fig(data,title,onshorewind,
                  til_2030,til_2050,
                  legend_title_installed,
                  legend_title_trends,
                  legend_title_goals,
                  forecast_name1,
                  y_axis_title,
                  y_max_2030,y_max_2050,hist_5yr,projection):

    # Calculate annual capacity additionals
    data_additions = data.copy()
    data_additions = data_additions.map(lambda x: np.nan)
    data_additions['year'] = data['year']
    predict_colname = data_additions.columns[-1]
    for i in range(len(data_additions)):
        if i == 0:
            wind_onshore_now = data.at[i, 'wind_onshore']
            data_additions.at[i, 'wind_onshore'] = wind_onshore_now
            isp_step_change_onshorewind_now = data.at[i, 'isp_step_change_onshorewind']
            predict_now = data.at[i,predict_colname]
        else:
            wind_onshore_prev = wind_onshore_now
            wind_onshore_now = data.at[i, 'wind_onshore']
            if not(np.isnan(wind_onshore_now)):
                data_additions.at[i, 'wind_onshore'] = wind_onshore_now - wind_onshore_prev
            isp_step_change_onshorewind_prev = isp_step_change_onshorewind_now
            isp_step_change_onshorewind_now = data.at[i, 'isp_step_change_onshorewind']
            if not(np.isnan(isp_step_change_onshorewind_prev)):
                if not(np.isnan(isp_step_change_onshorewind_now)):
                    data_additions.at[i, 'isp_step_change_onshorewind'] = isp_step_change_onshorewind_now - isp_step_change_onshorewind_prev
            predict_prev = predict_now
            predict_now = data.at[i, predict_colname]
            if not(np.isnan(predict_prev)):
                if not(np.isnan(predict_now)):
                    data_additions.at[i, predict_colname] = predict_now - predict_prev

    fig = go.Figure()

    # Cumulative installed capacities
    fig.add_trace(go.Bar(x = data['year'], y = data['wind_onshore']/1000, name = onshorewind,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible='legendonly',
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['year'], y = data['isp_step_change_onshorewind']/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    # Annual capacity additions
    fig.add_trace(go.Bar(x = data_additions['year'], y = data_additions['wind_onshore']/1000, name = onshorewind,
                         marker_color='#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         visible = False,
                         legendgroup="misc",
                         legendgrouptitle_text=legend_title_installed))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions[hist_5yr]/1000,
                         name = hist_5yr + projection, line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.2f}GW', visible=False,
                         legendgroup="trends",
                         legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['year'], y = data_additions['isp_step_change_onshorewind']/1000,
                             name = forecast_name1,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW',
                             visible = False,
                             legendgroup="goals",
                             legendgrouptitle_text = legend_title_goals))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0.015, y = 0.91,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2031"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2009", "2051"],
                            'yaxis.range': [0, y_max_2050]}])
                    ]),
                ),
                dict(
                    type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                    xanchor="left", yanchor="top", x = 0.015, y = 1,
                    buttons=list([
                        dict(label="Cumulative", 
                            method="update", 
                            args=[{"visible": [True,'legendonly',True,False,False,False]}]),
                        dict(label="per Year", 
                            method="update", 
                            args=[{"visible": [False,False,False,True,'legendonly',True]}]),
                    ]),                
                ),
            ]
        )

    if title is None:
        title = ""
        pad_top = 0
    else:
        pad_top = 40

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2009", "2031"])
    fig.update_layout(
        title = title,
        template = "simple_white",
        font=dict(family = "'sans-serif','arial'",size=14,color='#000000'),
        uniformtext_mode = 'hide',
        margin=dict(l=0, r=0, t=pad_top, b=0, pad=0),
        xaxis_hoverformat='%Y',
        hovermode="x unified",
        hoverlabel=dict(font_size=14),
        legend_title_text = '',
        legend_groupclick = "toggleitem",
        legend = {'traceorder':'grouped', 'orientation':'v',
                  'x': 0.015, 'y': 0.8,
                  'yanchor': 'top', 'xanchor': 'left',
                  'grouptitlefont_size': 13,
                  'font': {'size': 12},
                  'bgcolor': 'rgba(255,255,255,0)',
                  'borderwidth': 1},
        modebar_remove=['lasso','autoScale'])

    return fig

fig_windon_en = create_wind_fig(windon_data,"Installed wind capacity (onshore)",
                          "Wind (onshore)",
                          "until 2030","until 2050",
                          "Installed",
                          "Trends",
                          "Goals",
                          "Step Change (AEMO ISP 2022)","Gigawatt (GW)",
                          35,75,predict1.columns[-1]," projection")

# fig_windon_de = create_wind_fig(windon_data,"Installierte Leistung Windkraft an Land",
#                                "Legislaturperiode",
#                                "Tatsächlich erreicht","2017-2021","Letzte 12 Monate",
#                                "Linearisierter Verlauf", "Ziele der Koalition",
#                                "Nur Legislaturperiode", "bis 2030","bis 2045",
#                                "Historische Trends",
#                                "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                                "Szenariokorridor","Gigawatt",
#                                100,150,250)
# fig_windon_fr = create_wind_fig(windon_data,"Capacité installée - éolien terrestre",
#                                "Période du mandat",
#                                "Déjà installée","2017-2021","12 derniers mois",
#                                "Progression linéarisée","Objectifs de la coalition",
#                                "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
#                                "Tendances historiques",
#                                "Scénarios Ariadne","Modèle de référence",
#                                "Modèle de référence (tendance)",
#                                "Intervalle de scénarios","Gigawatts",
#                                100,150,250)

fig_windon_en.write_html("docs/australia/figures/wind_onshore.en.html", include_plotlyjs="directory")
# fig_windon_de.write_html("docs/australia/figures/wind_onshore.de.html", include_plotlyjs="directory")
# fig_windon_fr.write_html("docs/australia/figures/wind_onshore.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
#                                                                 [Section]: Electric Mobility
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#%%###########################################################################################
# BEV data
##############################################################################################

bev_data = pd.read_csv("docs/australia/data/bev.csv", sep = ",")
bev_data['date'] = pd.to_datetime(bev_data['date'], format='%Y')
bev_data['actual_ev'] = bev_data['actual_bev'] + bev_data['actual_hybrid']
bev_data['X'] = bev_data.index

# Determine the historical projection
predict1 = historicalProjection(bev_data[['date','X','actual_ev']], 5)
bev_data = bev_data.merge(predict1, how='left', on='date')

# Project path to government ambition
plan1 = plannedProjection(bev_data[['date','X','actual_ev','plan']])
bev_data = bev_data.merge(plan1, how='left', on='date')

# Create BEV figure
# -----------------
def create_bev_fig(data,title,label_bev,label_phev,
                   linear,goal,
                   legend_title_actual,
                   legend_title_trends,
                   y_axis_title,y_max_2030,plan_prj,step_change,til_2030,til_2050,y_max_2050,hist_5yr, projection, group_title_trends):

    # Calculate annual additionals
    data_additions = data.copy()
    data_additions = data_additions.map(lambda x: np.nan)
    data_additions['date'] = data['date']
    predict_colname = data_additions.columns[-2]
    for i in range(len(data_additions)):
        if i == 0:
            actual_bev_now = data.at[i, 'actual_bev']
            data_additions.at[i, 'actual_bev'] = actual_bev_now
            actual_hybrid_now = data.at[i, 'actual_hybrid']
            data_additions.at[i, 'actual_hybrid'] = actual_hybrid_now
            predict_now = data.at[i,predict_colname]
            plan_prj_now = data.at[i, plan_prj]
            step_change_now = data.at[i, 'step_change']
        else:
            actual_bev_prev = actual_bev_now
            actual_bev_now = data.at[i, 'actual_bev']
            if not(np.isnan(actual_bev_now)):
                data_additions.at[i, 'actual_bev'] = actual_bev_now - actual_bev_prev
            actual_hybrid_prev = actual_hybrid_now
            actual_hybrid_now = data.at[i, 'actual_hybrid']
            if not(np.isnan(actual_hybrid_now)):
                data_additions.at[i, 'actual_hybrid'] = actual_hybrid_now - actual_hybrid_prev
            predict_prev = predict_now
            predict_now = data.at[i, predict_colname]
            if not(np.isnan(predict_prev)):
                if not(np.isnan(predict_now)):
                    data_additions.at[i, predict_colname] = predict_now - predict_prev
            plan_prj_prev = plan_prj_now
            plan_prj_now = data.at[i, plan_prj]
            if not(np.isnan(plan_prj_prev)):
                if not(np.isnan(plan_prj_now)):
                    data_additions.at[i, plan_prj] = plan_prj_now - plan_prj_prev
            step_change_prev = step_change_now
            step_change_now = data.at[i, 'step_change']
            if not(np.isnan(step_change_prev)):
                if not(np.isnan(step_change_now)):
                    data_additions.at[i, 'step_change'] = step_change_now - step_change_prev

    fig = go.Figure()

    # Cumulative number of BEV and PHEV cars
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_bev'], name = label_bev,
                         marker_color='#ff7f0e',
                         hovertemplate = '%{y:.3s}',
                         legendgroup="actual",
                         legendgrouptitle_text = legend_title_actual))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_hybrid'], name = label_phev,
                         marker_color='#d62728',
                         hovertemplate = '%{y:.3s}',
                         legendgroup="actual"))
    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr],
                             name = hist_5yr + projection,
                             mode = "lines",
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%',visible='legendonly',
                             legendgroup = 'trends',
                             legendgrouptitle_text = group_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             mode = "markers", name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.3s}',
                             legendgroup = 'goals'))
    fig.add_trace(go.Scatter(x = data['date'], y = data[plan_prj],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'),
                             hovertemplate = '%{y:.3s}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['step_change'],
                             name = step_change,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.3s}',
                             legendgroup="goals"))

    # Annual additions of BEV and PHEV cars
    fig.add_trace(go.Bar(x = data_additions['date'], y = data_additions['actual_bev'], name = label_bev,
                         marker_color='#ff7f0e',
                         hovertemplate = '%{y:.3s}',
                         visible = False,
                         legendgroup="actual",
                         legendgrouptitle_text = legend_title_actual))
    fig.add_trace(go.Bar(x = data_additions['date'], y = data_additions['actual_hybrid'], name = label_phev,
                         marker_color='#d62728',
                         hovertemplate = '%{y:.3s}',
                         visible = False,
                         legendgroup="actual"))
    fig.add_trace(go.Scatter(x = data_additions['date'], y = data_additions[hist_5yr],
                             name = hist_5yr + projection,
                             mode = "lines",
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%',visible=False,
                             legendgroup = 'trends',
                             legendgrouptitle_text = group_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['date'], y = data_additions[plan_prj],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'),
                             hovertemplate = '%{y:.3s}',
                             visible = False,
                             legendgroup = 'goals',
                             legendgrouptitle_text = legend_title_trends))
    fig.add_trace(go.Scatter(x = data_additions['date'], y = data_additions['step_change'],
                             name = step_change,
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.3s}',
                             visible = False,
                             legendgroup="goals"))

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                xanchor="left", yanchor="top", x = 0.015, y = 0.91,
                buttons=list([
                    dict(label=til_2030,
                        method="relayout",
                        args=[{'xaxis.range': ["2018-01", "2031-05"],
                                'yaxis.range': [0, y_max_2030]}]),
                    dict(label=til_2050,
                        method="relayout",
                        args=[{'xaxis.range': ["2018-01", "2051-05"],
                                'yaxis.range': [0, y_max_2050]}])
                    ]),
                ),
                dict(
                    type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                    xanchor="left", yanchor="top", x = 0.015, y = 1,
                    buttons=list([
                        dict(label="Cumulative", 
                            method="update", 
                            args=[{"visible": [True,True,'legendonly',True,True,True,False,False,False,False,False]}]),
                        dict(label="per Year", 
                            method="update", 
                            args=[{"visible": [False,False,False,False,False,False,True,True,'legendonly',True,True]}]),
                    ]),                
                ),
            ]
        )

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,y_max_2030])
    fig.update_xaxes(title=None, range = ["2018-01", "2031-05"])
    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide',
                      font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      legend = {'traceorder':'grouped',
                                'yanchor':'top', 'xanchor':'left',
                                'x': 0.015, 'y': 0.8, 'orientation': 'v',
                                'grouptitlefont_size': 13,
                                'font': {'size': 12},
                                'bgcolor': 'rgba(255,255,255,0)',
                                'borderwidth': 1},
                      modebar_remove=['lasso','autoScale'],
                      legend_groupclick = "toggleitem",
                      margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      barmode='stack',
                      xaxis_tickformat ='%Y',
                      xaxis_hoverformat='%Y',
                      hovermode = "x")

    return fig

fig_bev_en = create_bev_fig(bev_data,"Electric vehicle passenger car fleet",
                            "Battery electric passenger cars","Plug-in hybrid passenger cars",
                            "Linear trend to Powering Australia Plan", "Powering Australia Plan",
                            "# of cars", "Goals", "Units",
                            4e6,plan1.columns[-1],'Step Change (AEMO ISP 2022)','until 2030','until 2050',16e6,predict1.columns[-1]," projection", 'Trends')

# fig_bev_de = create_bev_fig(bev_data,"Bestand batterieelektrischer Pkw",
#                             "Legislaturperiode",
#                             "Tatsächlich erreicht","2018-2021","Letzte 12 Monate",
#                             "Linearer Verlauf", "Logistischer Verlauf", "Ziel der Koalition",
#                             "Nur Legislaturperiode","bis 2030","bis 2045",
#                             "Historische Trends",
#                             "Ariadne-Szenarien","Leitmodell","Leitmodell (interpoliert)",
#                             "Szenariokorridor", "Anzahl",
#                             20000000,30000000,60000000)
# fig_bev_fr = create_bev_fig(bev_data,"Flotte de voitures particulières électriques à batterie",
#                             "Période du mandat",
#                             "En circulation","2018-2021","12 derniers mois",
#                             "Progression linéaire","Progression logistique","Objectif de la coalition",
#                             "Période du mandat", "Jusqu'en  2030", "Jusqu'en  2045",
#                             "Tendances historiques",
#                             "Scénarios Ariadne","Modèle de référence",
#                             "Modèle de référence (tendance)",
#                             "Intervalle de scénarios", "Unités",
                            # 20000000,30000000,60000000)

fig_bev_en.write_html("docs/australia/figures/bev.en.html", include_plotlyjs="directory")
# fig_bev_de.write_html("docs/australia/figures/bev.de.html", include_plotlyjs="directory")
# fig_bev_fr.write_html("docs/australia/figures/bev.fr.html", include_plotlyjs="directory")


#%%###########################################################################################
# BEV new admissions 
##############################################################################################

bev_adm_data = pd.read_csv("docs/australia/data/bev_adm.csv", sep = ",", thousands=",")
bev_adm_data['year'] = pd.to_datetime(bev_adm_data['year'], format='%Y')
bev_adm_data['per_bev'] = bev_adm_data['sales_bev'] / bev_adm_data['sales_all'] * 100
bev_adm_data['per_phev'] = bev_adm_data['sales_phev'] / bev_adm_data['sales_all'] * 100
bev_adm_data['per_ev'] = (bev_adm_data['sales_bev'] + bev_adm_data['sales_phev']) / bev_adm_data['sales_all'] * 100
bev_adm_data['per_plan'] = (bev_adm_data['plan']) / bev_adm_data['sales_all'] * 100
bev_adm_data['X'] = bev_adm_data.index

# Determine the historical projection
predict1 = historicalProjection(bev_adm_data[['year','X','per_ev']], 5)
bev_adm_data = bev_adm_data.merge(predict1, how='left', on='year')

# Project path to government ambition
plan1 = plannedProjection(bev_adm_data[['year','X','per_ev','per_plan']])
bev_adm_data = bev_adm_data.merge(plan1, how='left', on='year')

# Create EV new registrations figure
# ----------------------------------
def create_bev_adm(data,title,
                   group_title_achieved,
                   group_title_trends,
                   group_title_goal,
                   bev_adm_bev,bev_adm_phev,
                   goal,linear,
                   y_axis, hist_5yr, projection, plan_prj):

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['year'], y = data['per_bev'],
                            name = bev_adm_bev,
                            marker_color = '#ff7f0e', hovertemplate = '%{y:.2f}%',
                            legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['year'], y = data['per_phev'],
                            name = bev_adm_phev,
                            marker_color = '#d62728', hovertemplate = '%{y:.2f}%',
                            legendgroup = 'actual',
                            legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Scatter(x = data['year'], y = data[hist_5yr],
                             name = hist_5yr + projection,
                             mode = "lines",
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}%',visible='legendonly',
                             legendgroup = 'trends',
                             legendgrouptitle_text = group_title_trends))
    fig.add_trace(go.Scatter(x = data['year'], y = data['per_plan'],
                             mode = "markers", name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.0f}%',
                             legendgroup = 'goals'))
    fig.add_trace(go.Scatter(x = data['year'], y = data[plan_prj],
                             name = linear,
                             mode = "lines",
                             line = dict(color="#1f77b4", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goal))

    fig.update_yaxes(matches = None, title = y_axis, range = [0,100])
    fig.update_xaxes(title=None, range = ["2018", "2031"])

    fig.update_layout(title = title, template = "simple_white",
                            uniformtext_mode = 'hide',
                            font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                            legend = {'traceorder':'grouped',
                                'yanchor':'top', 'xanchor':'left',
                                'x': 0.015, 'y': 1, 'orientation': 'v',
                                'grouptitlefont_size': 13,
                                'font': {'size': 12},
                                'bgcolor': 'rgba(255,255,255,0)',
                                'borderwidth': 1},
                            modebar_remove=['lasso','autoScale'],
                            legend_groupclick = "toggleitem",
                            margin=dict(l=0, r=0, t=40, b=0, pad=0),
                            barmode='stack',
                            xaxis_tickformat ='%Y',
                            xaxis_hoverformat='%Y',
                            hovermode = "x")
    return fig

fig_bev_adm_en = create_bev_adm(bev_adm_data,"Shares in new registrations",
                                "Achieved shares","Trends","Goals",
                                "Battery electric passenger cars","Plug-in hybrid passenger cars",
                                "Powering Australia Plan","Linear trend to Powering Australia Plan","Percentage (%)",
                                predict1.columns[-1]," projection",plan1.columns[-1])
# fig_bev_adm_de = create_bev_adm(bev_adm_data,"Anteile an den monatlichen Neuzulassungen",
#                                 "Legislaturperiode","Tatsächlich erreicht",
#                                 "Batterieelektrische Pkw","Plug-in-Hybrid-Pkw",
#                                 "Nur Legislaturperiode","bis 2030","Prozent")
# fig_bev_adm_fr = create_bev_adm(bev_adm_data,"Parts des nouvelles immatriculations mensuelles",
#                                 "Période du mandat","Réalisé","Voitures électriques à batterie",
#                                 "Voitures hybrides plug-in","Période du mandat","Jusqu'en 2030",
#                                 "Pourcentage")

fig_bev_adm_en.write_html("docs/australia/figures/bev_adm.en.html", include_plotlyjs="directory")
# fig_bev_adm_de.write_html("docs/australia/figures/bev_adm.de.html", include_plotlyjs="directory")
# fig_bev_adm_fr.write_html("docs/australia/figures/bev_adm.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Charging stations 
##############################################################################################

cs_data  = pd.read_csv("docs/australia/data/cs.csv", sep = ",")
cs_data['months'] = cs_data['months'].astype(str)
cs_data['date'] = pd.to_datetime(cs_data['date'], format='%d.%m.%Y')
cs_data['X'] = cs_data.index

# Create charging station figure
# ------------------------------
def create_cs(data, title, cs_unspecified, cs_regular, cs_fast, cs_ultrafast, y_axis):

    fig = go.Figure()

    # fig.add_trace(go.Bar(x = data['date'], y = data['regular'],
    #                         name = cs_regular,
    #                         marker_color = '#E59866', hovertemplate = '%{y:.r}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['fast'],
                            name = cs_fast,
                            marker_color = '#BA4A00', hovertemplate = '%{y:.r}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['ultrafast'],
                            name = cs_ultrafast,
                            marker_color = '#6E2C00', hovertemplate = '%{y:.r}'))
    # fig.add_trace(go.Bar(x = data['date'], y = data['unspecified'],
    #                         name = cs_unspecified,
    #                         marker_color = '#616A6B', hovertemplate = '%{y:.r}'))

    fig.update_yaxes(matches = None, title = y_axis, range = [0,2000])
    fig.update_xaxes(title=None, range = ["2021-08", "2031-03"])

    fig.update_layout(title = title, template = "simple_white",
                            uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                            font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                            legend = {'traceorder':'grouped',
                                'yanchor':'top', 'xanchor':'left',
                                'x': 0.015, 'y': 1, 'orientation': 'v',
                                'grouptitlefont_size': 13,
                                'font': {'size': 12},
                                'bgcolor': 'rgba(255,255,255,0)',
                                'borderwidth': 1},
                            modebar_remove=['lasso','autoScale'],
                            legend_groupclick = "toggleitem",
                            margin=dict(l=0, r=0, t=40, b=0, pad=0),
                            barmode='stack',
                            xaxis_hoverformat='%b %Y',
                            hovermode = "x")

    return fig

fig_cs_en = create_cs(cs_data,"Fast and ultra-fast public charging locations in operation",
                      "Unspecified",
                      "Regular charging (<24kW)",
                      "Fast charging (24-99kW)",
                      "Ultra-fast charging (+100kW)",
                      "Locations")

# fig_cs_de = create_cs(cs_data,"Ladepunkte in Betrieb","Legislaturperiode",
#                       "Ziel der Koalition","Linearer Verlauf","Logistischer Verlauf",
#                       "Tatsächlich erreicht","Nicht spezifiziert","Normalladepunkte","Schnellladepunkte","Ultraschnellladepunkte",
#                       "Historische Trends (Summe)","2017-2021","Letzte 12 Monate",
#                       "Nur Legislaturperiode","bis 2030",
#                       "Anzahl")
# fig_cs_fr = create_cs(cs_data,"Points de charge en opération","Période du mandat",
#                       "Objectif de la coalition","Progression linéaire","Progression logistique",
#                       "Réalisé","Non spécifié","Points de charge normaux","Points de charge rapides","Points de charge ultrarapides",
#                       "Tendances historiques (somme)","2017-2021","12 derniers mois",
#                       "Période du mandat","Jusqu'en  2030",
#                       "Unités")

fig_cs_en.write_html("docs/australia/figures/cs.en.html", include_plotlyjs="directory")
# fig_cs_de.write_html("docs/australia/figures/cs.de.html", include_plotlyjs="directory")
# fig_cs_fr.write_html("docs/australia/figures/cs.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
#                                                                [Section]: Energy Consumption
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------

#%%###########################################################################################
# Primary energy consumption (pec) 
##############################################################################################

pec_data = pd.read_csv("docs/australia/data/pec.csv", sep = ",")
pec_data['date'] = pd.to_datetime(pec_data['date'], format='%Y')
pec_data['X'] = pec_data.index

# Determine the historical projection
predict1 = historicalProjection(pec_data[['date','X','sum_fossil']], 5)
pec_data = pec_data.merge(predict1, how='left', on='date')

# Project path to government ambition
plan1 = plannedProjection(pec_data[['date','X','sum_fossil','plan']])
pec_data = pec_data.merge(plan1, how='left', on='date')

# Create primary energy consumption figure
# ----------------------------------------
def create_pec_figure(data,title,oil,coal,natural_gas,
                      goal,
                      linear_reduction,
                      legend_group_title_actual,
                      legend_group_title_trends,
                      legend_group_title_goals,
                      y_axis_title,
                      hist_5yr,
                      projection,
                      plan_prj):

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['oil'],
                         name = oil,
                         marker_color = "rgba(26, 55, 77, .80)",
                         marker_line_width = 0,
                         legendgroup="fossils",
                         legendgrouptitle_text = legend_group_title_actual,
                         hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['coal'],
                         name = coal,
                         marker_color = "rgba(26, 55, 77, .60)",
                         marker_line_width = 0,
                         legendgroup="fossils",
                         hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['natural_gas'],
                         name = natural_gas,
                         marker_color = "rgba(205, 182, 153, .50)",
                         marker_line_width = 0,
                         legendgroup="fossils",
                         hovertemplate = '%{y:.2f} TWh'))
    
    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr],
                             name = hist_5yr + projection,
                             mode = 'lines',
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}',
                             visible='legendonly',
                             legendgroup='trends',
                             legendgrouptitle_text = legend_group_title_trends))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "arrow-down",
                             marker_color = "#2ca02c",
                             hovertemplate = '%{y:.2f} TWh',
                             legendgroup = 'goals',
                             legendgrouptitle_text = legend_group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data[plan_prj],
                             name = linear_reduction,
                             line = dict(color="#989C94", dash='dot', width = 1),
                             legendgroup = 'goals',
                             hovertemplate = '%{y:.0f} TWh'))

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,2000])
    fig.update_xaxes(matches = None, title = "Financial Year (1 July - 30 June)", range = ['1990','2051'])

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide', legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      xaxis_hoverformat='FY%Y',
                      barmode='stack', bargap=0,
                      hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      legend_traceorder = "reversed",
                      legend_groupclick = "toggleitem",
                      legend = {'font': {'size': 12},
                                'grouptitlefont_size': 13,
                                'traceorder':'grouped',
                                'yanchor':'top', 'xanchor':'right',
                                'x': 1, 'y': 1, 'orientation': 'v',
                                'bgcolor': 'rgba(0,0,0,0)',
                                'borderwidth': 1},
                      modebar_remove=['lasso','autoScale']
                      )
    
    return fig

fig_pec_figure_en = create_pec_figure(pec_data,
                                      "Primary energy consumption of fossil fuels",
                                      "Oil",
                                      "Coal",
                                      "Natural gas",
                                      "Zero in 2050",
                                      "Linear path",
                                      "Actual",
                                      "Trends",
                                      "Goals",
                                      "Terawatt Hour (TWh)",
                                      predict1.columns[-1],
                                      " projection",
                                      plan1.columns[-1])

# fig_pec_figure_de = create_pec_figure(pec_data,"Fossiler Primärenergieverbrauch",
#                                   "Mineralöl", "Kohle",
#                                   "Erdgas",
#                                   "Ziel der Koalition","Linearer Verlauf",
#                                   "Energieträger", 
#                                   "Terawattstunden",
#                                   "Historische Trends", "2017-2021")
# fig_pec_figure_fr = create_pec_figure(pec_data,"Consommation primaire d'énergie fossile",
#                                   "Pétrole","Charbon anthracite",
#                                   "Gaz naturel",
#                                   "Objectif de la coalition","Trajectoire linéaire",
#                                   "Vecteur d'énergie",
#                                   "Térawattheure",
#                                   "Tendance historique", "2017-2021")

fig_pec_figure_en.write_html("docs/australia/figures/fig_pec.en.html", include_plotlyjs="directory")
# fig_pec_figure_de.write_html("docs/australia/figures/fig_pec.de.html", include_plotlyjs="directory")
# fig_pec_figure_fr.write_html("docs/australia/figures/fig_pec.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Primary energy trade flows (pe_trade) 
##############################################################################################

pe_trade_data = pd.read_csv("docs/australia/data/pe_trade.csv", sep = ",")
pe_trade_data['date'] = pd.to_datetime(pe_trade_data['date'], format='%Y')
pe_trade_data['X'] = pe_trade_data.index

# Create primary energy trade figure
# ----------------------------------
def create_pe_trade_figure(data,title,oil,coal,natural_gas,
                      legend_imports,legend_exports,y_axis_title):

    fig = go.Figure()

    # Actual imports
    fig.add_trace(go.Bar(x = data['date'], y = data['oil'],
                             name = oil,
                             marker_color = "rgba(26, 55, 77, .80)",
                             marker_line_width = 0,
                             legendgroup="exports",
                             legendgrouptitle_text = legend_exports,
                             hovertemplate = '%{y:.2f} TWh'))

    # Actual exports
    fig.add_trace(go.Bar(x = data['date'], y = data['coal'],
                             name = coal,
                             marker_color = "rgba(26, 55, 77, .60)",
                             marker_line_width = 0,
                             legendgroup="imports",
                             legendgrouptitle_text = legend_imports,
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['natural_gas'],
                             name = natural_gas,
                             marker_color = "rgba(205, 182, 153, .50)",
                             marker_line_width = 0,
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f} TWh'))

    fig.update_yaxes(matches = None, title = y_axis_title, range = [-1000,5000])
    fig.update_xaxes(matches = None, title = "Financial Year (1 July - 30 June)", range = ['1990','2020'])

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide', legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      xaxis_hoverformat='FY%Y',
                      barmode='stack', bargap=0,
                      hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      legend_traceorder = "reversed",
                      legend_groupclick = "toggleitem",
                      legend = {'font': {'size': 12},
                                'grouptitlefont_size': 13,
                                'traceorder':'grouped',
                                'yanchor':'top', 'xanchor':'left',
                                'x': 0.015, 'y': 1, 'orientation': 'v',
                                'bgcolor': 'rgba(255,255,255,1)',
                                'borderwidth': 1},
                      modebar_remove=['lasso','autoScale'])
    return fig

fig_pe_trade_figure_en = create_pe_trade_figure(pe_trade_data,"Net primary energy trade flow of fossil fuels",
                                  "Oil","Coal",
                                  "Natural gas",
                                  "Exports",
                                  "Imports",
                                  "Terawatt Hour (TWh)",
                                  )
# fig_pe_trade_figure_de = create_pec_figure(pe_trade_data,"Fossiler Primärenergieverbrauch",
#                                   "Mineralöl", "Kohle",
#                                   "Erdgas",
#                                   "Ziel der Koalition","Linearer Verlauf",
#                                   "Energieträger", 
#                                   "Terawattstunden",
#                                   "Historische Trends", "2017-2021")
# fig_pe_trade_figure_fr = create_pec_figure(pe_trade_data,"Consommation primaire d'énergie fossile",
#                                   "Pétrole","Charbon anthracite",
#                                   "Gaz naturel",
#                                   "Objectif de la coalition","Trajectoire linéaire",
#                                   "Vecteur d'énergie",
#                                   "Térawattheure",
#                                   "Tendance historique", "2017-2021")

fig_pe_trade_figure_en.write_html("docs/australia/figures/fig_pe_trade.en.html", include_plotlyjs="directory")
# fig_pe_trade_figure_de.write_html("docs/australia/figures/fig_pe_trade.de.html", include_plotlyjs="directory")
# fig_pe_trade_figure_fr.write_html("docs/australia/figures/fig_pe_trade.fr.html", include_plotlyjs="directory")


#%%###########################################################################################
#                                                          [Section]: Greenhouse Gas Emissions
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#%%###########################################################################################
# Sectoral emissions 
##############################################################################################

emissions_sect_data = pd.read_csv("docs/australia/data/emissions_sect.csv", sep = ",")
emissions_sect_data['date'] = pd.to_datetime(emissions_sect_data['date'], format='%Y')
emissions_sect_data['X'] = emissions_sect_data.index

# Determine the historical projection (without LULUCF)
predict1 = historicalProjection(emissions_sect_data[['date','X','sum_wo_lulucf']], 5)
predict1 = predict1.rename(columns={predict1.columns[-1]:predict1.columns[-1]+' wo. LULUCF'})
emissions_sect_data = emissions_sect_data.merge(predict1, how='left', on='date')

# Determine the historical projection (with LULUCF)
predict2 = historicalProjection(emissions_sect_data[['date','X','sum_w_lulucf']], 5)
predict2 = predict2.rename(columns={predict2.columns[-1]:predict2.columns[-1]+' incl. LULUCF'})
emissions_sect_data = emissions_sect_data.merge(predict2, how='left', on='date')

# Project path to government targets (with LULUCF)
plan1 = plannedProjection(emissions_sect_data[['date','X','sum_w_lulucf','plan_sum']])
plan1 = plan1.rename(columns={plan1.columns[-1]:plan1.columns[-1]+' incl. LULUCF'})
emissions_sect_data = emissions_sect_data.merge(plan1, how='left', on='date')

# Project path to government targets (without LULUCF)
plan2 = plannedProjection(emissions_sect_data[['date','X','sum_wo_lulucf','plan_sum']])
plan2 = plan2.rename(columns={plan2.columns[-1]:plan2.columns[-1]+' wo. LULUCF'})
emissions_sect_data = emissions_sect_data.merge(plan2, how='left', on='date')

# Create sectoral emissions figure
# --------------------------------
def create_emissions_sect(data,title,plan_sum,plan_linear_sum_wo_lulucf,plan_linear_sum_w_lulucf,
              energy_elect_heat,energy_transport,energy_other,industry,agriculture,waste_other,lulucf,
              group_title_achieved,
              group_title_trends,
              group_title_goals,
              y_axis,hist_5yr_wo_lulucf,hist_5yr_w_lulucf,projection,plan_prj_wo_lulucf,plan_prj_w_lulucf,total_net_emissions):

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['electricity_heat'],
                         name = energy_elect_heat,
                         marker_color = '#ff8800', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['transport'],
                         name = energy_transport,
                         marker_color = '#880088', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['energy_other'],
                         name = energy_other,
                         marker_color = '#000088', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))

    fig.add_trace(go.Bar(x = data['date'], y = data['industry'],
                         name = industry,
                         marker_color = '#ffff00', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))

    fig.add_trace(go.Bar(x = data['date'], y = data['agriculture'],
                         name = agriculture,
                         marker_color = '#008800', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['waste_other'],
                         name = waste_other,
                         marker_color = '#880000', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['lulucf'],
                         name = lulucf,
                         marker_color = '#888888', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Scatter(x = data['date'], y = data['sum_w_lulucf'],
                         name = total_net_emissions,
                         marker_color = 'black', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    
    # Trends
    x_split = hist_5yr_wo_lulucf.split(" ")
    x_split[0] = x_split[0]+projection
    hist_5yr_wo_lulucf_name = " ".join(x_split)

    x_split = hist_5yr_w_lulucf.split(" ")
    x_split[0] = x_split[0]+projection
    hist_5yr_w_lulucf_name = " ".join(x_split)

    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr_wo_lulucf],
                             name = hist_5yr_wo_lulucf_name, line = dict(color="#ABB2B9", dash='dot'),
                             hovertemplate = '%{y:.1f}',
                             visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = group_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr_w_lulucf],
                             name = hist_5yr_w_lulucf_name, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}',
                             legendgroup="trends"))
    
    # Targets

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_sum'],
                             name = plan_sum,
                             mode = "markers",
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#2ca02c",
                             hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals'))
    fig.add_trace(go.Scatter(x = data['date'], y = data[plan_prj_w_lulucf],
                             name = plan_linear_sum_w_lulucf,
                             line = dict(color="#212F3D", dash='dot'), hovertemplate = '%{y:.1f}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))


    fig.update_yaxes(matches = None, title = y_axis, range = [-100,700], zeroline=True, zerolinewidth=1)
    fig.update_xaxes(title=None, range = ["1989-01", "2101-01"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                         margin = dict(l=0, r=0, t=40, b=0, pad=0),
                         barmode = 'relative',
                         xaxis_hoverformat = '%Y',
                         hovermode = "x",
                         legend_groupclick = "toggleitem",
                         legend = {'font': {'size': 12},
                                'grouptitlefont_size': 13,
                                'traceorder':'reversed+grouped',
                                'yanchor':'top', 'xanchor':'right',
                                'x': 1, 'y': 1, 'orientation': 'v',
                                'bgcolor': 'rgba(255,255,255,1)',
                                'borderwidth': 1},
                         modebar_remove=['lasso','autoScale'])

    return fig

fig_emissions_sect_en = create_emissions_sect(emissions_sect_data,
                      "Sectoral greenhouse gas emissions",
                      "Government targets",
                      "xxx",
                      "Linear trend to government target",
                      "Energy (Electricity & Heat)",
                      "Energy (Transport)",
                      "Energy (Other)",
                      "Industry","Agriculture",
                      "Waste and others",
                      "LULUCF",
                      "Actual",
                      "Trends",
                      "Goals",
                      "Million tons CO<sub>2</sub>e (MtCO<sub>2</sub>e)",
                      predict1.columns[-1],
                      predict2.columns[-1],
                      " projection",'xxx',
                      plan1.columns[-1],
                      "Total net emissions")

# fig_emissions_sect_de = create_emissions_sect(emissions_sect_data,"Sektorale Treibhausgasemissionen","Legislaturperiode",
#                       "Energiewirtschaft (Electricity & Heat)","Energiewirtschaft (Verkehr)","Energiewirtschaft (Other)","Industrie","Landwirtschaft","Abfallw. & Sonstiges","Gesamtemissionen","Linearer Verlauf",
#                       "Regierungsziele",
#                       "Energiewirtschaft (Electricity & Heat)","Energiewirtschaft (Verkehr)","Energiewirtschaft (Other)","Industrie","Landwirtschaft","Abfallw. & Sonstiges","LULUCF",
#                       "Tatsächlich erreicht",
#                       "Trend (Gesamtemissionen)","2016-2020",
#                       "Mio t CO2-äquivalent")
# fig_emissions_sect_fr = create_emissions_sect(emissions_sect_data,"Émissions sectorielles de gaz à effet de serre","Période du mandat",
#                       "Énergie (Electricity & Heat)","Énergie (Transport)","Énergie (Other)","Industrie","Agriculture","Déchets et autres","Émissions totales","Progression linéaire",
#                       "Objectifs du gouvernement",
#                       "Énergie (Electricity & Heat)","Énergie (Transport)","Énergie (Other)","Industrie","Agriculture","Déchets et autres","LULUCF",
#                       "Émissions passées",
#                       "Tendances historiques (émissions totales)","2016-2020",
#                       "Millions tCO2éq")

fig_emissions_sect_en.write_html("docs/australia/figures/emissions_sect.en.html", include_plotlyjs="directory")
# fig_emissions_sect_de.write_html("docs/australia/figures/emissions_sect.de.html", include_plotlyjs="directory")
# fig_emissions_sect_fr.write_html("docs/australia/figures/emissions_sect.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
# Electricity sector emissions
##############################################################################################

emissions_electr_data = pd.read_csv("docs/australia/data/emissions_electr.csv", sep = ",")
emissions_electr_data['date'] = pd.to_datetime(emissions_electr_data['date'], format='%Y')
emissions_electr_data['emission_factor'] = emissions_electr_data['elec_emissions_scope1'] / emissions_electr_data['elec_generation'] * 1000
emissions_electr_data['X'] = emissions_electr_data.index

# Determine the historical projection
df = emissions_electr_data[['date','X','elec_emissions_scope1']]
predict1 = historicalProjection(emissions_electr_data[['date','X','elec_emissions_scope1']], 5,min_value = 0)
emissions_electr_data = emissions_electr_data.merge(predict1, how='left', on='date')

# Scale up the AEMO step change emissions to the whole electricity sector (national)
isfound = False
latest_nem_emissions_MTCO2e = 0.0
latest_nem_emissions_index = 0
i = 0
for i in range(len(emissions_electr_data)):
    if not(np.isnan(emissions_electr_data.at[i, 'nem_emissions_MTCO2e'])):
        isfound = True
        latest_nem_emissions_MTCO2e = emissions_electr_data.at[i, 'nem_emissions_MTCO2e']
        latest_nem_emissions_index = i
    else:
        if (isfound):
            break # Search until there is a gap in data
if isfound:
    national_elec_emissions_MTCO2e = emissions_electr_data.at[latest_nem_emissions_index,'elec_emissions_scope1'] / 1000000
    scaling_factor = national_elec_emissions_MTCO2e / latest_nem_emissions_MTCO2e
    emissions_electr_data['plan_stepchange_emissions_MTCO2e'] = np.nan
    emissions_electr_data['plan_stepchange_emissionsintensity_g/kWh'] = np.nan
    for i in range(len(emissions_electr_data)):
        if not(np.isnan(emissions_electr_data.at[i, 'nem_stepchange_emissions_MtCO2e'])):
            emissions_electr_data.at[i,'plan_stepchange_emissions_MTCO2e'] = scaling_factor * emissions_electr_data.at[i, 'nem_stepchange_emissions_MtCO2e']
            emissions_electr_data.at[i,'plan_stepchange_emissionsintensity_g/kWh'] = emissions_electr_data.at[i, 'nem_stepchange_emissions_MtCO2e'] / emissions_electr_data.at[i, 'nem_stepchange_generation_GWh'] * 1000000  

# Create primary electricity sector emissions figure
# --------------------------------------------------
def create_emissions_electr_figure(data,title,emissions,emission_factor,goal,linear_reduction,
                                   legend_group_achieved,
                                   legend_group_title_trends,
                                   legend_group_title_goals,
                                   x_axis_title,
                                   y_axis_title,
                                   y2_axis_title,
                                   hist_5yr,
                                   projection):

    fig = go.Figure()

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Bar(x = data['date'], y = data['elec_emissions_scope1']/1e6,
                             name = emissions,
                             marker_color = "#ff7f0e",
                             marker_line_width = 0,
                             legendgroup="actual",
                             legendgrouptitle_text = legend_group_achieved,
                             hovertemplate = '%{y:.0f} MtCO<sub>2</sub>e'),
                             secondary_y=False)

    fig.add_trace(go.Scatter(x = data['date'], y = data['emission_factor'],
                             name = emission_factor,
                             mode = 'lines',
                             line_shape="spline",
                             line = dict(color="#d62728", dash = "solid", width=3),
                             legendgroup="actual",
                             hovertemplate = '%{y:.0f} gCO<sub>2</sub>e/kWh'),
                             secondary_y=True)

    fig.add_trace(go.Scatter(x = data['date'], y = data[hist_5yr]/1e6,
                         name = hist_5yr + projection,
                         mode = 'lines',
                         line = dict(color="#ff7f0e", dash='dot'),
                         hovertemplate = '%{y:.3s} MtCO<sub>2</sub>e',
                         visible='legendonly',
                         legendgroup='trends',
                         legendgrouptitle_text = legend_group_title_trends,
                         ),
                         secondary_y=False)


    fig.add_trace(go.Bar(x = data['date'], y = data['plan_stepchange_emissions_MTCO2e'],
                             name = linear_reduction,
                             marker_color = "moccasin",
                             marker_line_width = 0,
                             legendgroup="goals",
                             legendgrouptitle_text = legend_group_title_goals,
                             hovertemplate = '%{y:.0f} MtCO<sub>2</sub>e'),
                             secondary_y=False)



    # fig.add_trace(go.Scatter(x = data['date'], y = data['plan_emissions']/1e6,
    #                          name = goal,
    #                          mode = "markers", marker_size=12, marker_symbol = "x-dot",
    #                          marker_color = "#2ca02c",
    #                          hovertemplate = '%{y:.2f} MtCO<sub>2</sub>e',
    #                          legendgroup = 'goals',
    #                          legendgrouptitle_text = legend_group_title_goals,),
    #                          secondary_y=False)
    # fig.add_trace(go.bar(x = data['date'], y = data['plan_stepchange_emissions_MTCO2e'],
    #                          name = linear_reduction,
    #                          marker_color = "#ff7f0e",
    #                          marker_line_width = 0,
    #                          legendgroup = 'goals', legendgrouptitle_text = legend_group_title_goals,
    #                          hovertemplate = '%{y:.0f} MtCO<sub>2</sub>e'),
    #                          secondary_y=False,)
                            #  name = linear_reduction,
                            #  line = dict(color="#1f77b4", dash='dash'),
                            #  legendgroup = 'goals', legendgrouptitle_text = legend_group_title_goals,
                            #  hovertemplate = '%{y:.0f} MtCO<sub>2</sub>e'),
                            #  secondary_y=False,)
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_stepchange_emissionsintensity_g/kWh'],
                             name = emission_factor,
                             line = dict(color="#1f77b4", dash='dash'),
                             legendgroup = 'goals',
                             hovertemplate = '%{y:.0f} g/kWh'),
                             secondary_y=True,)

    fig.update_yaxes(matches = None, title = y_axis_title,  range = [0,200], secondary_y=False)
    fig.update_yaxes(matches = None, title = y2_axis_title, range = [0,860], secondary_y=True)
    fig.update_xaxes(matches = None, title = x_axis_title,  range = ['2012','2051'])

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide',
                      legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      xaxis_hoverformat='FY%Y',
                      barmode='stack', bargap=0,
                      hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      legend_groupclick = "toggleitem",
                      legend = {'font': {'size': 12},
                                'grouptitlefont_size': 13,
                                'traceorder': 'grouped',
                                'yanchor': 'top', 'xanchor':'right',
                                'x': 0.92, 'y': 1, 'orientation': 'v',
                                'bgcolor': 'rgba(255,255,255,1)',
                                'borderwidth': 1},
                      modebar_remove=['lasso','autoScale'])
    return fig

fig_emissions_electr_figure_en = create_emissions_electr_figure(emissions_electr_data,
                                  "CO<sub>2</sub> emissions of electricity generation",
                                  "Emissions",
                                  "Emission factor",
                                  "xxx",
                                  "Step Change 'extrapolated' (AEMO ISP 2022)",
                                  "Actual",
                                  "Trends",
                                  "Goals",
                                  "Financial Year (1 July - 30 June)",
                                  "Million tons CO<sub>2</sub>e (MtCO<sub>2</sub>e)",
                                  "g/kWh",
                                  predict1.columns[-1],
                                  " projection")

# fig_emissions_electr_figure_de = create_emissions_electr_figure(emissions_electr_data,"CO<sub>2</sub>-Emissionen der Stromerzeugung",
#                                   "Emissionen", "Emissionsfaktor",
#                                   "Ziel der Koalition","Linearer Verlauf",
#                                   "Tatsächlich erreicht", 
#                                   "Millionen Tonnen", "g/kWh",
#                                   "Historischer Trend", "2017-2021")
# fig_emissions_electr_figure_fr = create_emissions_electr_figure(emissions_electr_data,"CO<sub>2</sub> émis par la production d'électricité",
#                                   "Émissions","Intensité de CO2",
#                                   "Objectif de la coalition","Trajectoire linéaire",
#                                   "Niveaux observés",
#                                   "Millions de tonnes", "g/kWh",
#                                   "Tendance historique", "2017-2021")

fig_emissions_electr_figure_en.write_html("docs/australia/figures/emissions_electr.en.html", include_plotlyjs="directory")
# fig_emissions_electr_figure_de.write_html("docs/australia/figures/emissions_electr.de.html", include_plotlyjs="directory")
# fig_emissions_electr_figure_fr.write_html("docs/australia/figures/emissions_electr.fr.html", include_plotlyjs="directory")

#%%###########################################################################################
#                                                                     [Section]: Energy Prices
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
#%%###########################################################################################
# Household electricity tariffs 
##############################################################################################

hh_elec_tariff_data = pd.read_csv("docs/australia/data/hh_elec_tariff.csv", sep = ",")
hh_elec_tariff_data['date'] = pd.to_datetime(hh_elec_tariff_data['date'], format='%Y')
hh_elec_tariff_data['X'] = hh_elec_tariff_data.index


data = hh_elec_tariff_data

is_nominal = False

# create household electricity tariff figure
def create_hh_elec_tariff(data,is_nominal,
                            title_nominal,title_real,
                            metering,network_distr,network_transm,generation,retailer,generator_retailer,policies_lret,policies_sres,policies_other,
                            group_title_proc,group_title_nonproc,group_title_networks,y_axis,
                            button_current_price, button_real_price):

    fig = go.Figure()

    data_nominal = data.copy()

    # Obtain the real price data
    most_recent_price_year = data[data['total'] > 0].iloc[-1] 
    cpi_ref_year = most_recent_price_year['date'].year
    cpi_in_ref_year = most_recent_price_year.cpi
    data['scaling_factor'] = cpi_in_ref_year / data['cpi']
    data['policies_lret'] *= data['scaling_factor']
    data['policies_sres'] *= data['scaling_factor']
    data['policies_other'] *= data['scaling_factor']
    data['policies_carbon'] *= data['scaling_factor']
    data['network_transm'] *= data['scaling_factor']
    data['network_distr'] *= data['scaling_factor']
    data['network_metering'] *= data['scaling_factor']
    data['generation'] *= data['scaling_factor']
    data['retailer'] *= data['scaling_factor']
    data['generation_retailer'] *= data['scaling_factor']
    data['total'] *= data['scaling_factor']
    data_real = data.copy()

    # Add Traces for Nominal Prices                         \
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['generation_retailer'],
                         name = generator_retailer,
                         marker_color = '#616A6B', hovertemplate = '%{y:.2f}',
                         legendgroup = 'proc',
                         legendgrouptitle_text = group_title_proc))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['retailer'],
                         name = retailer,
                         marker_color = '#5DADE2', hovertemplate = '%{y:.2f}',
                         legendgroup = 'proc'))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['generation'],
                         name = generation,
                         marker_color = '#2E86C1', hovertemplate = '%{y:.2f}',
                         legendgroup = 'proc'))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['network_metering'],
                         name = metering,
                         marker_color = '#4A235A', hovertemplate = '%{y:.2f}',
                         legendgroup = 'networks',
                         legendgrouptitle_text = group_title_networks))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['network_distr'],
                         name = network_distr,
                         marker_color = '#492d9c', hovertemplate = '%{y:.2f}',
                         legendgroup = 'networks'))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['network_transm'],
                         name = network_transm,
                         marker_color = '#967dc4', hovertemplate = '%{y:.2f}',
                         legendgroup = 'networks'))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = (data_nominal['policies_carbon'] + data_nominal['policies_other']),
                         name = policies_other,
                         marker_color = '#145A32', hovertemplate = '%{y:.2f}',
                         legendgroup = 'nonproc',
                         legendgrouptitle_text = group_title_nonproc))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['policies_sres'],
                         name = policies_sres,
                         marker_color = '#95c486', hovertemplate = '%{y:.2f}',
                         legendgroup = 'nonproc'))
    fig.add_trace(go.Bar(x = data_nominal['date'], y = data_nominal['policies_lret'],
                         name = policies_lret,
                         marker_color = '#008800', hovertemplate = '%{y:.2f}',
                         legendgroup = 'nonproc'))
    fig.add_trace(go.Scatter(x = data_nominal['date'], y = data_nominal['total'],
                         text = data_nominal['total'],
                         texttemplate = '%{y:.2f}',
                         mode = 'text',
                         textposition='top center',
                         textfont=dict(size=10),
                         hoverinfo = 'skip',
                         showlegend=False))

    # Add Traces for Real Prices                         
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['generation_retailer'],
                         name = generator_retailer,
                         marker_color = '#616A6B', hovertemplate = '%{y:.2f}',
                         legendgroup = 'proc',
                         visible = False,
                         legendgrouptitle_text = group_title_proc))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['retailer'],
                         name = retailer,
                         marker_color = '#5DADE2', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'proc'))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['generation'],
                         name = generation,
                         marker_color = '#2E86C1', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'proc'))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['network_metering'],
                         name = metering,
                         marker_color = '#4A235A', hovertemplate = '%{y:.2f}',
                         legendgroup = 'networks',
                         visible = False,
                         legendgrouptitle_text = group_title_networks))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['network_distr'],
                         name = network_distr,
                         marker_color = '#492d9c', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'networks'))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['network_transm'],
                         name = network_transm,
                         marker_color = '#967dc4', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'networks'))
    fig.add_trace(go.Bar(x = data_real['date'], y = (data_real['policies_carbon'] + data_real['policies_other']),
                         name = policies_other,
                         marker_color = '#145A32', hovertemplate = '%{y:.2f}',
                         legendgroup = 'nonproc',
                         visible = False,
                         legendgrouptitle_text = group_title_nonproc))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['policies_sres'],
                         name = policies_sres,
                         marker_color = '#95c486', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'nonproc'))
    fig.add_trace(go.Bar(x = data_real['date'], y = data_real['policies_lret'],
                         name = policies_lret,
                         marker_color = '#008800', hovertemplate = '%{y:.2f}',
                         visible = False,
                         legendgroup = 'nonproc'))
    fig.add_trace(go.Scatter(x = data_real['date'], y = data_real['total'],
                         text = data_real['total'],
                         texttemplate = '%{y:.2f}',
                         mode = 'text',
                         textposition='top center',
                         textfont=dict(size=10),
                         hoverinfo = 'skip',
                         visible = False,
                         showlegend=False))
    
    fig.update_yaxes(matches = None, title = y_axis, range = [0,40])
    fig.update_xaxes(title="Financial Year (1 July - 30 June)", 
                     range = ["2012-05", "2023-07"],
                     tickvals = [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023],
                     ticktext = [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023],
#                     ticktext = ['2012-13', '2013-14', '2014-15', '2015-16', '2016-17', '2017-18', '2018-19', '2019-20', '2020-21','2021-22','2022-23'],
                     )

    fig.add_vrect(x0 = "2021-06", x1 = "2023-07",
                  annotation_text = 'Under AEMC review',
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")
    
    fig.update_layout(title = title_nominal, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", font=dict(size = 12)),
                         legend_grouptitlefont_size = 13,
                         legend_traceorder = "grouped+reversed",
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x",
                         modebar_remove=['lasso','autoScale'])

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 40},
            xanchor="left", yanchor="top", x = 0, y = 1.125,
            buttons=list([dict(label=button_current_price, method="update", 
                               args=[{"visible": [True,True,True,True,True,True,True,True,True,True,False,False,False,False,False,False,False,False,False,False]},
                                      {"title" : title_nominal}]),
                          dict(label=button_real_price, method="update",
                               args=[{"visible": [False,False,False,False,False,False,False,False,False,False,True,True,True,True,True,True,True,True,True,True]},
                                      {"title" : title_real}]),
                ]),
            )
            ]
        )
    return fig

# Nominal retail tariff figure
# ----------------------------
fig_hh_elec_tariff_en = create_hh_elec_tariff(hh_elec_tariff_data, True,
                        "Household electricity prices (nominal)","Household electricity prices (real)",
                        "Metering","Distribution","Transmission","Wholesale","Residual (i.e. retail)","Wholesale, Residual","LRET","SRES","Other",
                        "Electricity procurement","Environmental policies","Networks","cents per kWh [AUD]",'Nominal','Real')

# fig_hh_elec_tariff_en = create_hh_elec_tariff(hh_elec_tariff_data, True,
#                         "Household electricity prices (nominal)",
#                         "Metering","Distribution","Transmission","Wholesale","Residual (i.e. retail)","Wholesale, Residual","LRET","SRES","Other",
#                         "Electricity procurement","Environmental policies","Networks","cents per kWh [AUD]")
# fig_hh_elec_tariff_de = create_hh_elec_tariff(hh_elec_tariff_data,
#                       "Haushalts-Strompreise",
#                       "Meters","Distribution","Transmission","Wholesale","Residual","Wholesale, Residual","LRET","SRES","Other levies",
#                       "Beschaffung, Netzentgelt,<br>Vertrieb","Steuern, Abgaben, Umlagen","Networks","Cent pro kWh")
# fig_hh_elec_tariff_fr = create_hh_elec_tariff(hh_elec_tariff_data,
#                       "Prix de l'électricité pour les ménages",
#                       "Meters","Distribution","Transmission","Wholesale","Residual","Wholesale, Residual","LRET","SRES","Other levies",
#                       "Approvisionnement, rémunération<br>du réseau, distribution","Taxes, impôts, prélèvements","Networks", "Cent par kWh")
             
fig_hh_elec_tariff_en.write_html("docs/australia/figures/hh_elec_tariff.en.html", include_plotlyjs="directory")
# fig_hh_elec_tariff_de.write_html("docs/australia/figures/hh_elec_tariff.de.html", include_plotlyjs="directory")
# fig_hh_elec_tariff_fr.write_html("docs/australia/figures/hh_elec_tariff.fr.html", include_plotlyjs="directory")

# %% Duplicate plotly.min.js for the EN and FR #####################################################

# Specify the source file path
source_file = "docs/australia/figures/plotly.min.js"

# Specify the destination file path with a different name
destination_file_en = "docs/australia/figures/plotly.min.en.js"
destination_file_fr = "docs/australia/figures/plotly.min.fr.js"

# Copy the file to the destination with a different name
shutil.copyfile(source_file, destination_file_en)
shutil.copyfile(source_file, destination_file_fr)

#%% ################################################################################################
