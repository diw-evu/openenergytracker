#%% Packages

from datetime import datetime, timedelta
from datetime import date
import os
from turtle import end_fill
import pandas as pd
import numpy as np
import sklearn
import plotly.express as px
from wetterdienst import Wetterdienst
from wetterdienst.provider.dwd.observation import DwdObservationRequest
from wetterdienst import Settings
import urllib

#%% Import gas data


def import_ncg(data_path):
    """
    Function imports and cleans NetConnect Germany legacy data
    Input:
        data_path: directory path with arbitrarily named csv file containing NCG data
    Output:
        ncg: DataFrame containing a date column and a total NCG consumption column in TWh
    """
    ncg_file = data_path+os.listdir(data_path)[0]
    ncg = pd.read_csv(ncg_file,sep=";")
    # drop all unit columns
    ncg = ncg.drop(ncg.filter(regex='Unit').columns,axis=1)
    # convert to float
    ncg[ncg.columns.difference(['DayOfUse','Status'])] = ncg[ncg.columns.difference(['DayOfUse','Status'])].astype(float)
    # compute total consumption in TWh and drop other columns
    ncg = ncg.assign(ncg=ncg[ncg.columns.difference(['DayOfUse','Status'])].sum(axis=1)/1e9)
    fin_cols = ['DayOfUse','ncg'] 
    ncg =ncg[fin_cols]
    # Rename and format date column
    ncg = ncg.rename(columns={'DayOfUse':'date'})
    ncg['date'] = pd.to_datetime(ncg['date'],dayfirst=True,utc=True)
    ncg = ncg.set_index('date')
    return ncg



def import_gaspool(data_path):
    """
    Function imports and cleans Gaspool legacy data
    Input:
        data_path: directory path with arbitrarily named csv file containing Gaspool data
    Output:
        gp: DataFrame containing a date column and a total Gaspool consumption column in TWh
    """
    gp_file = data_path+os.listdir(data_path)[0]
    gp = pd.read_csv(gp_file,sep=";")
    # convert to float
    gp[gp.columns.difference(['Datum'])] = gp[gp.columns.difference(['Datum'])].astype(float)
    # compute total consumption in TWh (data in MWh)
    gp = gp.assign(gp = gp[gp.columns.difference(['Datum'])].sum(axis=1)/1e6)
    gp = gp.rename(columns={'Datum':'date'})
    fin_cols = ['date','gp']
    gp = gp[fin_cols]
    # Format date
    gp['date'] = pd.to_datetime(gp['date'],dayfirst=True,utc=True)
    gp = gp.set_index('date')

    return gp


def add_date(dataframe,month):
    """
    Helper function to add date to dataframe in import_the
    """

    dat = dataframe.assign(
        date = pd.to_datetime(dataframe['Tag'].astype(str)+
        "-"+month,dayfirst=True,utc=True
    ))
    return dat



def import_the(data_path):
    """
    Function to import THE Germany data from monthly consumption figures
    """
    files =os.listdir(data_path)
    months = [str.replace('.csv','') for str in files]
    dat = []
    for file in files:
        dat.append(pd.read_csv(data_path+file,sep=";"))
    
    dat = [add_date(dat[x],months[x]) for x in range(0,len(files))]
    dat = [dat[x][dat[x].columns.difference(['Tag'])] for x in range(0,len(files))]

    # compute total
    fin_cols = ['date','the']
    for x in range(0,len(files)):
        dat[x] = dat[x].assign(the = dat[x][dat[x].columns.difference(['date'])].sum(axis=1)/1e9)
        dat[x] = dat[x][fin_cols]
    
    the = pd.concat(dat,axis=0,ignore_index=False)
    the = the.set_index('date')
    the = the.sort_index()
    
    return the
    

def combine_sources(ncg,gp,the):
    '''
    Combine NCG, Gaspool and THE data into one dataset
    '''
    hist = pd.merge(gp,ncg,left_index=True,right_index=True)
    hist = hist.assign(total = hist.gp+hist.ncg)
    hist = hist.reset_index()
    fin_cols = ['date','total']
    hist = hist[fin_cols]
    hist = hist.set_index('date')

    the = the.rename(columns={"the":"total"})
    list = [hist,the]
    out = pd.concat(list)
    out = out.replace(0,np.nan)
    return out

def get_consumption_data(ncg_data,gp_data,the_data):
    ncg =import_ncg(ncg_data)
    gp = import_gaspool(gp_data)
    the = import_the(the_data)

    df = combine_sources(ncg,gp,the)
    df = df.rename(columns = {'total':'consumption'})
    return df


def import_restlast():
    from requests import get
    website = 'https://www.tradinghub.eu/de-de/Ver%C3%B6ffentlichungen/Transparenz/Aggregierte-Verbrauchsdaten'
    r = get(website)
    html = r.text
    e = html.find('Restlast ab 1. Januar 2018') - 2
    b = html.find('Verbrauchsdaten SLP Bereich') + len('Verbrauchsdaten SLP Bereich') + 20
    url  = html[b:e]
    temp = pd.read_excel(url,skiprows=2,sheet_name=1)
    temp = temp.iloc[:,0:2]
    temp = temp.rename(columns={'Gastag':'date','Restlast [kWh]*':'volume'})
    temp = temp.assign(volume = temp.volume/1e6)
    return temp



def get_temperature_data(start_date,end_date,write_csv=False,out_dir=""):
    """
    Download temperature data from DWD (takes around 15 minutes)
    Inputs:
        out_dir = directory where to save CSV
        start_date = First day in data set
        end_date = Last day in data set
        write_csv = Boolean to determine whether to save as CSV 
    """
    API = Wetterdienst("dwd", "observation")
    Settings.tidy = True  # default, tidy data
    Settings.humanize = True  # default, humanized parameters
    Settings.si_units = False  # default, convert values to SI units

    # 
    # Get temperature data for year 2016-2022 (only once)

    request = DwdObservationRequest(
        parameter=["temperature_air_mean_200","temperature_air_min_200","temperature_air_max_200","sunshine_duration"],
        resolution="daily",
        start_date=start_date,  # if not given timezone defaulted to UTC
        end_date=end_date  # if not given timezone defaulted to UTC
        ).all()
    
    data = request.values.all().df
    data = data.dropna()
    if write_csv:
     data.to_csv(out_dir + 'temp_data.csv')
    return data

def import_temperature_data(out_dir,filename):
   if os.path.exists(out_dir + filename):
    temp = pd.read_csv(out_dir + filename)
    return temp
   else:
    print("File does not exit use get_temperature_data function")

def clean_temperature_date(temp_data):
    df = temp_data
    df['date'] = pd.to_datetime(df['date'])
    cols = ['station_id','date','value']
    df = df[cols]
    df = df.rename(columns={'value':'temperature'})
    return df


def update_temperature_data(static_data):
    start_date = static_data.date.max()+timedelta(days=1)
    end_date =  datetime.today()
    new_data = get_temperature_data(start_date,end_date)
    new_data = clean_temperature_date(new_data)
    list = [static_data,new_data]
    compiled = pd.concat(list)
    return compiled

def get_stations(start_date,end_date,write_csv=False,out_dir=""):
    """
    Download temperature data from DWD (takes around 15 minutes)
    Inputs:
        out_dir = directory where to save CSV
        start_date = First day in data set
        end_date = Last day in data set
        write_csv = Boolean to determine whether to save as CSV 
    """
    API = Wetterdienst("dwd", "observation")
    Settings.tidy = True  # default, tidy data
    Settings.humanize = True  # default, humanized parameters
    Settings.si_units = False  # default, convert values to SI units

    # 
    # Get temperature data for year 2016-2022 (only once)

    request = DwdObservationRequest(
        parameter=["temperature_air_mean_200","sunshine_duration","wind_force_beaufort"],
        resolution="daily",
        start_date=start_date,  # if not given timezone defaulted to UTC
        end_date=end_date  # if not given timezone defaulted to UTC
        )
    
    data = request.all().df
    data = data.dropna()
    if write_csv:
     data.to_csv(out_dir + 'temp_data.csv')
    return data
# %%
def population(input_path):

    # Set URL and directories
    url = 'https://ec.europa.eu/eurostat/cache/GISCO/geodatafiles/GEOSTAT-grid-POP-1K-2011-V2-0-1.zip'
    population_path = os.path.join(input_path, 'population')
    os.makedirs(population_path, exist_ok=True)
    destination = os.path.join(population_path, 'GEOSTAT-grid-POP-1K-2011-V2-0-1.zip')
    unzip_dir = os.path.join(population_path, 'Version 2_0_1')

    # Download file
    if not os.path.isfile(destination):
        urllib.request.urlretrieve(url, destination)
    else:
        print('{} already exists. Download is skipped.'.format(destination))
    # Unzip file
    if not os.path.isdir(unzip_dir):
        with zipfile.ZipFile(destination, 'r') as f:
            f.extractall(population_path)
    else:
        print('{} already exists. Unzipping is skipped.'.format(unzip_dir))

    clear_output(wait=False)
    print("Download successful")