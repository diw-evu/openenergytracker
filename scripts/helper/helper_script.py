import os
import pandas as pd
import numpy as np

from datetime import datetime, timedelta
import holidays

from wetterdienst import Wetterdienst
from wetterdienst.provider.dwd.observation import DwdObservationRequest
from wetterdienst import Settings

def import_restlast():
    from requests import get
    website = 'https://www.tradinghub.eu/de-de/Ver%C3%B6ffentlichungen/Transparenz/Aggregierte-Verbrauchsdaten'
    r = get(website)
    html = r.text
    e = html.find('Restlast ab 1. Januar 2018') - 2
    b = html.find('Verbrauchsdaten SLP Bereich') + len('Verbrauchsdaten SLP Bereich') + 20
    url  = html[b:e]
    temp = pd.read_excel(url,skiprows=2,sheet_name=1)
    temp = temp.iloc[:,0:2]
    temp = temp.rename(columns={'Gastag':'date','Restlast [kWh]*':'volume'})
    temp = temp.assign(volume = temp.volume/1e6)
    return temp

def get_temperature_data(start_date,end_date,write_csv=False,out_dir=""):
    """
    Download temperature data from DWD (takes around 15 minutes)
    Inputs:
        out_dir = directory where to save CSV
        start_date = First day in data set
        end_date = Last day in data set
        write_csv = Boolean to determine whether to save as CSV 
    """
    API = Wetterdienst("dwd", "observation")
    Settings.tidy = True  # default, tidy data
    Settings.humanize = True  # default, humanized parameters
    Settings.si_units = False  # default, convert values to SI units

    # 
    # Get temperature data for year 2016-2022 (only once)

    request = DwdObservationRequest(
        parameter=["temperature_air_mean_200","temperature_air_min_200","temperature_air_max_200"],
        resolution="daily",
        start_date=start_date,  # if not given timezone defaulted to UTC
        end_date=end_date  # if not given timezone defaulted to UTC
        ).all()
    
    data = request.values.all().df
    data = data.dropna()
    if write_csv:
        data.to_csv(out_dir + 'temp_data.csv')
    return data

def import_temperature_data(out_dir,filename):
   if os.path.exists(out_dir + filename):
        temp = pd.read_csv(out_dir + filename)
        return temp
   else:
       print("File does not exit use get_temperature_data function")

def clean_temperature_date(temp_data):
    df = temp_data
    df['date'] = pd.to_datetime(df['date'])
    #cols = ['station_id','date','value']
    #df = df[cols]
    #df = df.rename(columns={'value':'temperature'})
    return df

def update_temperature_data(static_data):
    static_data['date'] = pd.to_datetime(static_data['date'])
    start_date  = static_data.date.max()-timedelta(days=2)
    today       = datetime.today()
    idx         = (today.weekday() + 1) % 7 # MON = 0, SUN = 6 -> SUN = 0 .. SAT = 6
    last_sunday = today - timedelta(idx)
    end_date    =  last_sunday
    new_data    = get_temperature_data(start_date,end_date)
    new_data    = clean_temperature_date(new_data)
    #list = [new_data]
    #compiled = pd.concat(list)
    return new_data

def compute_summ_temp(temp,stat):
    temp = temp.set_index('date')
    temp = temp.drop(["dataset","quality"],axis=1).reset_index().pivot(index=["date","station_id"],columns="parameter",values="value").reset_index("station_id").dropna()
    temp.index = temp.index.tz_localize(None)
    df = temp.groupby('date').agg({'temperature_air_mean_200':stat,
                                    'temperature_air_min_200':stat,
                                    'temperature_air_max_200':stat})
    df = df.rename(columns={'temperature_air_mean_200':'temp_mean','temperature_air_min_200':'temp_min','temperature_air_max_200':'temp_max'})                                
    return df

def create_lags(df):
    df = df.assign(temp_mean_t1 = df.temp_mean.shift(1),
                   temp_mean_t2 = df.temp_mean.shift(2),
                   temp_mean_t3 = df.temp_mean.shift(3),
                   temp_min_t1 = df.temp_min.shift(1),
                   temp_min_t2 = df.temp_min.shift(2),
                   temp_min_t3 = df.temp_min.shift(3),
                   temp_max_t1 = df.temp_max.shift(1),
                   temp_max_t2 = df.temp_max.shift(2),
                   temp_max_t3 = df.temp_max.shift(3)
                    )
    df = df.dropna()
    return df

def add_weekend_dummy(df):
    df = pd.get_dummies(df.assign(weekday = df.index.strftime("%a")))
    df = df.assign(weekend = df['weekday_Sat'] + df['weekday_Sun'])
    df = df.drop(df.filter(regex='weekday').columns,axis=1)
    return df

def check_holiday(date):
    german_holiday = holidays.Germany()
    if date in german_holiday:
        return 1
    else:
        return 0

def add_holiday_var(df):
    df['holiday'] = pd.Series(df.reset_index()['date'].apply(check_holiday)).array
    return df

def add_off_day(df):
    df = add_holiday_var(df)
    df = add_weekend_dummy(df)
    df = df.assign(off_day = np.where(df.weekend + df.holiday > 0 ,1, 0))
    return df

def add_month(df):
    df = df.assign(month = df.index.month)
    return df

def add_time_vars(temp):
    temp = add_month(temp)
    temp = add_off_day(temp)
    return temp

def turn_to_array(df):
     df = df.reset_index().drop(columns=['date'],axis=1).to_numpy()
     return df