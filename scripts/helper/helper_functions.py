import numpy as np
import holidays
import pandas as pd

def compute_avg_temp(temp):
    temp.index = temp.index.tz_localize(None)
    df = temp.groupby('date').mean('temperature')
    return df

def compute_summ_temp(temp,stat):
    temp.index = temp.index.tz_localize(None)
    df = temp.groupby('date').agg({'temperature_air_mean_200':stat,
                                    'temperature_air_min_200':stat,
                                    'temperature_air_max_200':stat})
    return df

def merge_data(temp_data,consumption_data):
    return temp_data.merge(consumption_data,how='right',
    left_index=True,
    right_index=True)

def compute_therm_inertia(temp_data):
    df = temp_data
    df = df.assign(ref_temp = (df.temperature + 
                              0.5*df.temperature.shift(1) +
                              0.25*df.temperature.shift(2)+
                              0.125*df.temperature.shift(3))/
                              (1+0.5+0.25+0.125)
                              )
    return df

def compute_degree_days(temp_data,thermal_inertia=True):
    if thermal_inertia:
        df = compute_therm_inertia(temp_data)
    else:
        df = temp_data.assign(ref_temp = temp_data.temperature)
    df = df.assign(degree_days = np.where((20-df.ref_temp) < 5, 0, (20-df.ref_temp)))
    return df

def add_weekend_dummy(df):
    df = pd.get_dummies(df.assign(weekday = df.index.strftime("%a")))
    df = df.assign(weekend = df['weekday_Sat'] + df['weekday_Sun'])
    df = df.drop(df.filter(regex='weekday').columns,axis=1)
    return df

def check_holiday(date):
    german_holiday = holidays.Germany()
    if date in german_holiday:
        return 1
    else:
        return 0
def add_holiday_var(df):
    df['holiday'] = pd.Series(df.reset_index()['date'].apply(check_holiday)).array
    return df

def add_off_day(df):
    df = add_holiday_var(df)
    df = add_weekend_dummy(df)
    df = df.assign(off_day = np.where(df.weekend + df.holiday > 0 ,1, 0))
    return df

