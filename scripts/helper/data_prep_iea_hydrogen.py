#%% Packages
import pandas as pd


#%% Functions
# Function to import and summarize data
def prepare_data_by_comm_year(path,columns):
    # Import and name columns
    df = pd.read_excel(path,sheet_name="Projects",skiprows=3)
    df = df.set_axis(columns,axis =1)
    # create chart dataset
    ch = df[(df['country']=="DEU") & (df['decommision_date'].isnull())].groupby(['status','date_online'])['norm_capa_mw_el'].sum()
    ch = ch.to_frame().reset_index().sort_values(['status','date_online']).reset_index(drop=False)
    ch=ch.pivot(index='date_online',columns='status',values='norm_capa_mw_el').fillna(0)#.reset_index()
    # Determine min max year
    min_y = 2003#int(ch['date_online'].min())
    max_y = 2036#int(ch['date_online'].max()+1)
    #
    ys = pd.DataFrame(list(range(min_y,max_y)),columns=['date_online']).join(ch,how='left',on="date_online")
    ys = ys.fillna(0)
    ys=ys.set_index(['date_online']).stack().reset_index()
    ys=ys.astype({'date_online':'int'})
    ys=ys.rename(columns={'level_1':'status',0:'capacity'})
    ys['cum_capacity']=ys.groupby(['status'])['capacity'].cumsum()
    ys = ys.assign(cum_capacity =ys.cum_capacity/1000 )
    ys=ys.pivot(index='date_online',columns='status',values='cum_capacity')
    ys = ys.assign(Total = ys['Concept']+ys.DEMO+ys.FID+ys['Feasibility study']+ys.Operational+ys['Under construction'])
    ys.columns = ys.columns.str.strip().str.lower()
    ys = ys.reset_index()
    # Compute total including projects without commisisioning date
    tt =df[(df['country']=="DEU") & (df['decommision_date'].isnull())].groupby(['status'])['norm_capa_mw_el'].sum().to_frame().reset_index()
    tt.status = tt.status.str.lower()
    tt = tt.rename(columns={'norm_capa_mw_el':'cum_capacity'})
    tt = tt.assign(cum_capacity =tt.cum_capacity/1000 )
    tt = tt.assign(total = "Total")
    
    return (ys,tt)
#%% Import data
# Define path
dir = "docs/germany/data/"
filename = "Hydrogen projects database public version October 2022.xlsx"
path = dir + filename
path

# Define columns
columns = (
"ref","project_name","country","date_online","decommision_date","status",
"technology","tech_comments","tech_electricity","tech_renewables",
"product",
"end_use_refining","end_use_ammonia","end_use_methanol","end_use_iron_steel",
"end_use_other_ind","end_use_mobility","end_use_power","end_use_grid_inj",
"end_use_chp","end_use_dom_heat","end_use_biofuel",
"end_use_synfuel","end_use_ch4_grid_inj","end_use_ch4_mobility",
"announced_size","norm_capa_mw_el","norm_capa_nm3_h2_h",
"norm_capa_kt_h2_h","norm_capa_tco2_y","norm_capa_iea_nm3_h2_h","refs"
)

# Prepare data
tup = prepare_data_by_comm_year(path,columns=columns)

# %% # save to csv
out1 = dir + "iea_h2_by_year.csv"; out2 = dir + "iea_h2_total.csv"
tup[0].to_csv(out1,sep=",",index=False); tup[1].to_csv(out2,sep=",",index=False)
