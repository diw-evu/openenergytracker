#%% Load libraries #################################################################################

import os
import sys
import pandas as pd
import numpy as np
import copy
from pathlib import Path
from datetime import date

import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'scripts':
    os.chdir('../../')
else:
    sys.path.append('.')

print(os.getcwd())

import scripts.util as oet
import scripts.colors as colors

#%% Create directory

Path("docs/germany/figures").mkdir(parents=True, exist_ok=True)

####################################################################################################
# Storage ##########################################################################################
####################################################################################################

print("")
print("---------- Script: storage ----------")
print("")

base_default = {
    "title"         : "Installierte Leistung Photovoltaik",
    "data"          : None,
    "color_dict"    : colors.standard,
    "x_axis_title"  : None,
    "x_axis_range"  : ["2019-01", "2025-06"],
    "y_axis_title"  : "Bestand [GWh]",
    "y_axis2_title" : "Zubau [MWh]",
    "y_axis_range"  : [0,20],
    "y_axis2_range"  : [0,750],
    "source_file"     : "docs/germany/data/batteries_energy.csv",
    "language"        : "de"
    }

#%% ################################################################################################

# Battery charts

batteries_energy = pd.read_csv(os.path.join("docs","germany","data","batteries_energy.csv"))

#%%

base_storage = copy.deepcopy(base_default)

base_storage["subplots"] =  {
        "rows"        : 2,
        "cols"        : 1,
        "row_heights" : [0.75,0.25]}

dict_actual = oet.actual.copy()
dict_actual["hover"] = '%{y:.2f} GWh'

dict_add = oet.additions.copy()
dict_add["hover"] = '%{y:.2f} MWh'

#  Battery energy ##################################################################################

dict_battery_energy_de = {
    "base"                          : base_storage,
    "figure_actual1"                : dict_actual.copy(),
    "figure_actual2"                : dict_actual.copy(),
    "figure_actual3"                : dict_actual.copy(),
    "figure_additions1"             : dict_add.copy(),
    "figure_additions2"             : dict_add.copy(),
    "figure_additions3"             : dict_add.copy(),
    "figure_actual_total"           : dict_actual.copy(),
    "figure_add_total"              : dict_add.copy(),
}

dict_battery_energy_de["base"]["data"]     = batteries_energy
dict_battery_energy_de["base"]["barmode"]  = "stack"
dict_battery_energy_de["base"]["title"]    = "Installierte Speicherkapazität"
dict_battery_energy_de["base"]["margin"]   = {"l": 0, "r": 0, "t": 40, "b": 60}

# Heimspeicher
dict_battery_energy_de["figure_actual1"]["y"]        = "Heimspeicher_value_cum"
dict_battery_energy_de["figure_additions1"]["y"]     = "Heimspeicher_value"
dict_battery_energy_de["figure_actual1"]["name"]     = "Heimspeicher (< 30 kWh)"
dict_battery_energy_de["figure_additions1"]["name"]  = "Heimspeicher (< 30 kWh)"

# Gewerbespeicher
dict_battery_energy_de["figure_actual2"]["y"]         = "Gewerbespeicher_value_cum"
dict_battery_energy_de["figure_additions2"]["y"]      = "Gewerbespeicher_value"
dict_battery_energy_de["figure_actual2"]["name"]      = "Gewerbespeicher (30 kWh - 1 MWh)"
dict_battery_energy_de["figure_additions2"]["name"]   = "Gewerbespeicher (30 kWh - 1 MWh)"
dict_battery_energy_de["figure_actual2"]["color"]     = "second"
dict_battery_energy_de["figure_additions2"]["color"]  = "second"

# Großspeicher
dict_battery_energy_de["figure_actual3"]["y"]         = "Großspeicher_value_cum"
dict_battery_energy_de["figure_additions3"]["y"]      = "Großspeicher_value"
dict_battery_energy_de["figure_actual3"]["name"]      = "Großspeicher (> 1 MWh)"
dict_battery_energy_de["figure_additions2"]["name"]   = "Gewerbespeicher (30 kWh - 1 MWh)"
dict_battery_energy_de["figure_additions2"]["name"]   = "Gewerbespeicher (30 kWh - 1 MWh)"
dict_battery_energy_de["figure_actual3"]["color"]     = "third"
dict_battery_energy_de["figure_additions3"]["color"]  = "third"

# Total
dict_battery_energy_de["figure_actual_total"]["name"]       = "Gesamt"
dict_battery_energy_de["figure_actual_total"]["y"]          = "Total_value_cum"
dict_battery_energy_de["figure_actual_total"]["type"]       = "scatter"
dict_battery_energy_de["figure_actual_total"]["mode"]       = "lines"
dict_battery_energy_de["figure_actual_total"]["line"]       = {"dash":"solid", "width":1}
dict_battery_energy_de["figure_actual_total"]["showlegend"] = False
dict_battery_energy_de["figure_actual_total"]["color"]      = "first"

dict_battery_energy_de["figure_add_total"]["name"]       = "Gesamt"
dict_battery_energy_de["figure_add_total"]["y"]          = "Total_value"
dict_battery_energy_de["figure_add_total"]["type"]       = "scatter"
dict_battery_energy_de["figure_add_total"]["mode"]       = "lines"
dict_battery_energy_de["figure_add_total"]["line"]       = {"dash":"solid", "width":1}
dict_battery_energy_de["figure_add_total"]["showlegend"] = False
dict_battery_energy_de["figure_add_total"]["color"]      = "first"

# English

dict_battery_energy_en = copy.deepcopy(dict_battery_energy_de)

dict_battery_energy_en["base"]["language"]      = "en"
dict_battery_energy_en["base"]["title"]         = "Installed storage capacity"
dict_battery_energy_en["base"]["y_axis_title"]  = "Stock [GWh]"
dict_battery_energy_en["base"]["y_axis2_title"] = "Additions [MWh]"

dict_battery_energy_en["figure_actual1"]["name"]     = "Home storage (< 30 kWh)"
dict_battery_energy_en["figure_additions1"]["name"]  = "Home storage (< 30 kWh)"
dict_battery_energy_en["figure_actual2"]["name"]     = "Industrial storage (30 kWh - 1 MWh)"
dict_battery_energy_en["figure_additions2"]["name"]  = "Industrial storage (30 kWh - 1 MWh)"
dict_battery_energy_en["figure_actual3"]["name"]     = "Large-scale storage (> 1 MWh)"
dict_battery_energy_en["figure_additions3"]["name"]  = "Large-scale storage (> 1 MWh)"

# French

dict_battery_energy_fr = copy.deepcopy(dict_battery_energy_de)

dict_battery_energy_fr["base"]["language"]      = "fr"
dict_battery_energy_fr["base"]["title"]         = "Capacité de stockage installée"
dict_battery_energy_fr["base"]["y_axis_title"]  = "Stock [GWh]"
dict_battery_energy_fr["base"]["y_axis2_title"] = "Additions [MWh]"

dict_battery_energy_fr["figure_actual1"]["name"]     = "Système de stockage à domicile (< 30 kWh)"
dict_battery_energy_fr["figure_additions1"]["name"]  = "Système de stockage à domicile (< 30 kWh)"
dict_battery_energy_fr["figure_actual2"]["name"]     = "Système de stockage industriel (30 kWh - 1 MWh)"
dict_battery_energy_fr["figure_additions2"]["name"]  = "Système de stockage industriel (30 kWh - 1 MWh)"
dict_battery_energy_fr["figure_actual3"]["name"]     = "Système de stockage à grande échelle (> 1 MWh)"
dict_battery_energy_fr["figure_additions3"]["name"]  = "Système de stockage à grande échelle (> 1 MWh)"

# Create figures

fig_battery_energy_de = oet.create_fig(dict_battery_energy_de)
fig_battery_energy_en = oet.create_fig(dict_battery_energy_en)
fig_battery_energy_fr = oet.create_fig(dict_battery_energy_fr)

fig_battery_energy_de.write_html(os.path.join("docs","germany","figures","battery_energy.de.html"), include_plotlyjs="directory")
fig_battery_energy_en.write_html(os.path.join("docs","germany","figures","battery_energy.en.html"), include_plotlyjs="directory")
fig_battery_energy_fr.write_html(os.path.join("docs","germany","figures","battery_energy.fr.html"), include_plotlyjs="directory")

#%%

# Battery power ####################################################################################

batteries_power = pd.read_csv(os.path.join("docs","germany","data","batteries_power.csv"))

dict_battery_power_de = copy.deepcopy(dict_battery_energy_de)

dict_battery_power_de["base"]["title"]          = "Installierte Speicherleistung"
dict_battery_power_de["base"]["data"]           = batteries_power
dict_battery_power_de["base"]["y_axis_range"]   = [0,15]
dict_battery_power_de["base"]["y_axis2_range"]  = [0,500]
dict_battery_power_de["base"]["y_axis_title"]   = "Bestand [GW]"
dict_battery_power_de["base"]["y_axis2_title"]  = "Zubau [MW]"
dict_battery_power_de["source_file"]           = "docs/germany/data/batteries_power.csv"

dict_battery_power_de["figure_actual1"]["hover"]      = '%{y:.2f} GW'
dict_battery_power_de["figure_actual2"]["hover"]      = '%{y:.2f} GW'
dict_battery_power_de["figure_actual3"]["hover"]      = '%{y:.2f} GW'
dict_battery_power_de["figure_actual_total"]["hover"] = '%{y:.2f} GW'

dict_battery_power_de["figure_additions1"]["hover"]  = '%{y:.2f} MW'
dict_battery_power_de["figure_additions2"]["hover"]  = '%{y:.2f} MW'
dict_battery_power_de["figure_additions3"]["hover"]  = '%{y:.2f} MW'
dict_battery_power_de["figure_add_total"]["hover"]   = '%{y:.2f} MW'

# English

dict_battery_power_en = copy.deepcopy(dict_battery_power_de)

dict_battery_power_en["base"]["language"]      = "en"
dict_battery_power_en["base"]["title"]         = "Installed storage power"
dict_battery_power_en["base"]["y_axis_title"]  = "Stock [GW]"
dict_battery_power_en["base"]["y_axis2_title"] = "Additions [MW]"

dict_battery_power_en["figure_actual1"]["name"]     = "Home storage (< 30 kWh)"
dict_battery_power_en["figure_additions1"]["name"]  = "Home storage (< 30 kWh)"
dict_battery_power_en["figure_actual2"]["name"]     = "Industrial storage (30 kWh - 1 MWh)"
dict_battery_power_en["figure_additions2"]["name"]  = "Industrial storage (30 kWh - 1 MWh)"
dict_battery_power_en["figure_actual3"]["name"]     = "Large-scale storage (> 1 MWh)"
dict_battery_power_en["figure_additions3"]["name"]  = "Large-scale storage (> 1 MWh)"

# French

dict_battery_power_fr = copy.deepcopy(dict_battery_power_de)

dict_battery_power_fr["base"]["language"]      = "fr"
dict_battery_power_fr["base"]["title"]         = "Puissance de stockage installée"
dict_battery_power_fr["base"]["y_axis_title"]  = "Stock [GW]"
dict_battery_power_fr["base"]["y_axis2_title"] = "Additions [MW]"

dict_battery_power_fr["figure_actual1"]["name"]     = "Système de stockage à domicile (< 30 kWh)"
dict_battery_power_fr["figure_additions1"]["name"]  = "Système de stockage à domicile (< 30 kWh)"
dict_battery_power_fr["figure_actual2"]["name"]     = "Système de stockage industriel (30 kWh - 1 MWh)"
dict_battery_power_fr["figure_additions2"]["name"]  = "Système de stockage industriel (30 kWh - 1 MWh)"
dict_battery_power_fr["figure_actual3"]["name"]     = "Système de stockage à grande échelle (> 1 MWh)"
dict_battery_power_fr["figure_additions3"]["name"]  = "Système de stockage à grande échelle (> 1 MWh)"

# Create figures

fig_battery_power_de = oet.create_fig(dict_battery_power_de)
fig_battery_power_en = oet.create_fig(dict_battery_power_en)
fig_battery_power_fr = oet.create_fig(dict_battery_power_fr)

fig_battery_power_de.write_html(os.path.join("docs","germany","figures","battery_power.de.html"), include_plotlyjs="directory")
fig_battery_power_en.write_html(os.path.join("docs","germany","figures","battery_power.en.html"), include_plotlyjs="directory")
fig_battery_power_fr.write_html(os.path.join("docs","germany","figures","battery_power.fr.html"), include_plotlyjs="directory")

#%%