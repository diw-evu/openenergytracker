#%% Load libraries #################################################################################

import sys
import os
import pandas as pd
import numpy as np
import plotly.express as px
from datetime import date
from pandas.tseries.offsets import *

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'scripts':
    os.chdir('../../')
else:
    sys.path.append('.')

print(os.getcwd())

import scripts.util as oet
import scripts.colors as colors

####################################################################################################
# Utility PV #######################################################################################
####################################################################################################

print("")
print("---------- Script: deep dive PV ----------")
print("")

#%% Data preparation

mastr_data = pd.read_csv("docs/germany/data/pv_mastr_data.csv")
mastr_data = mastr_data.assign(date=pd.to_datetime(dict(year=mastr_data.year,month=mastr_data.month,day= 1)))

#%%

start_date = "2017-01-01"
pv_util_additions = mastr_data.query(f"date>='{start_date}'")
pv_util_additions['reductions']    = pv_util_additions['reductions'].fillna(0)
pv_util_additions['net_additions'] = pv_util_additions['additions'] - pv_util_additions['reductions']
pv_util_additions = pv_util_additions.groupby(['date','Lage','bins']).agg({'net_additions':'sum'}).reset_index()
pv_util_additions
pv_util_additions = pv_util_additions.query(f"date<='{date.today()}'")
pv_util_additions = pv_util_additions.assign(net_additions = pv_util_additions.net_additions/1e3)

pv_util_additions['bins'].replace(
        {
            "(0.0, 10.0]"           : "< 1 MW",
            "(10.0, 25.0]"          : "< 1 MW",
            "(25.0, 100.0]"         : "< 1 MW",
            "(100.0, 500.0]"        : "< 1 MW" ,
            "(500.0, 1000.0]"       : "< 1 MW" ,
            "(1000.0, 5000.0]"      : "1 - 5 MW" ,
            "(5000.0, 10000.0]"     : "5 - 10 MW" ,
            "(10000.0, 20000.0]"    : "10 - 20 MW" ,
            "(20000.0, 50000.0]"    : "20 - 50 MW" ,
            "(50000.0, 10000000.0]" : "> 50 MW" },
        inplace = True)

pv_util_additions = pv_util_additions.query("Lage == 'Freifläche'").reset_index().filter(['date','bins','net_additions'])
pv_util_additions = pv_util_additions.groupby(['date','bins']).agg({'net_additions':'sum'}).reset_index()
pv_util_additions = pv_util_additions.pivot(index='date',columns='bins',values='net_additions').reset_index()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

##### Figure Net additions #####

dict_pv_util_additions = {}

dict_pv_util_additions["base"]                 = oet.base_default.copy()
dict_pv_util_additions["base"]["title"]        = "Freiflächenphotovoltaik: Nettozubau nach Anlagengröße"
dict_pv_util_additions["base"]["data"]         = pv_util_additions
dict_pv_util_additions["base"]["source_file"]  = "docs/germany/data/pv_mastr_data.csv"
dict_pv_util_additions["base"]["language"]     = "de"

dict_pv_util_additions["base"]["y_axis_range"] = [0, 1000]
dict_pv_util_additions["base"]["x_axis_range"] = ["2017-01-01","2025-06-01"]
dict_pv_util_additions["base"]["y_axis_title"] = "Zubau [MW]"
dict_pv_util_additions["base"]["barmode"]      = "stack"
dict_pv_util_additions["base"]["margin"]       = dict(l=0, r=0, t=40, b=40, pad=0)

dict_pv_util_additions["base"]["color_dict"] = colors.colors_gradient

dict_pv_util_additions["base"]["legend_trace_order"] = "reversed"

del dict_pv_util_additions["base"]["buttons"]

dict_pv_util_additions["figure_actual1"]              = {}
dict_pv_util_additions["figure_actual1"]["type"]      = "bar"
dict_pv_util_additions["figure_actual1"]["hover"]     = "%{y:.1f} MW"

dict_pv_util_additions["figure_actual1"]["x"]    = "date"
dict_pv_util_additions["figure_actual1"]["name"] = "> 50 MW"
dict_pv_util_additions["figure_actual1"]["y"]    = "> 50 MW"
dict_pv_util_additions["figure_actual1"]["color"] = 10

dict_pv_util_additions["figure_actual2"] = dict_pv_util_additions["figure_actual1"].copy()
dict_pv_util_additions["figure_actual2"]["name"] = "20 - 50 MW"
dict_pv_util_additions["figure_actual2"]["y"]    = "20 - 50 MW"
dict_pv_util_additions["figure_actual2"]["color"] = 8

dict_pv_util_additions["figure_actual3"] = dict_pv_util_additions["figure_actual1"].copy()
dict_pv_util_additions["figure_actual3"]["name"] = "10 - 20 MW"
dict_pv_util_additions["figure_actual3"]["y"]    = "10 - 20 MW"
dict_pv_util_additions["figure_actual3"]["color"] = 6

dict_pv_util_additions["figure_actual4"]           = dict_pv_util_additions["figure_actual1"].copy()
dict_pv_util_additions["figure_actual4"]["name"]   = "5 - 10 MW"
dict_pv_util_additions["figure_actual4"]["y"]      = "5 - 10 MW"
dict_pv_util_additions["figure_actual4"]["color"]  = 4

dict_pv_util_additions["figure_actual5"]           = dict_pv_util_additions["figure_actual1"].copy()
dict_pv_util_additions["figure_actual5"]["name"]   = "1 - 5 MW"
dict_pv_util_additions["figure_actual5"]["y"]      = "1 - 5 MW"
dict_pv_util_additions["figure_actual5"]["color"]  = 3

dict_pv_util_additions["figure_actual6"] = dict_pv_util_additions["figure_actual1"].copy()
dict_pv_util_additions["figure_actual6"]["name"]      = "< 1 MW" 
dict_pv_util_additions["figure_actual6"]["y"]         = "< 1 MW"
dict_pv_util_additions["figure_actual6"]["color"]     = 1

fig_pv_util_additions = oet.create_fig(dict_pv_util_additions)

fig_pv_util_additions.update_layout(bargap=0.0,bargroupgap=0.0)

fig_pv_util_additions.write_html("docs/germany/figures/pv_util_additions.de.html", include_plotlyjs="directory")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

##### Stock and additions ######

bestand_oet = oet.prepare_data('https://gitlab.com/diw-evu/oet/openenergytracker/-/raw/main/docs/germany/data/pv.csv',"months")
bestand_oet = oet.calculate_trend(bestand_oet,"actual","12-months")
bestand_oet = bestand_oet[["date","plan","plan_linear"]]
bestand_oet.date += MonthEnd()

today = pd.Timestamp.today().strftime("%Y-%m-%d")

bestand = mastr_data.copy(deep=True)

bestand["Lage"] = np.where(bestand["Lage"].str.contains("Freifläche"),"Freifläche","Sonstige")

bestand = bestand.groupby(["Lage","date"]).agg({'net_additions':'sum'}).reset_index()
#bestand = bestand.groupby(["Lage"]).agg({'net_additions':'cumsum'}).reset_index()
bestand["actual"] = bestand.groupby("Lage")["net_additions"].cumsum()
bestand = bestand.query("date >= '2017-01-01'").reset_index(drop=True)
bestand["actual"] = bestand["actual"] / (1e6)
bestand["actual"] = np.where(bestand["date"] > today,np.nan,bestand["actual"])
bestand["net_additions"] = bestand["net_additions"].replace({0:np.nan})
bestand_ext = bestand.pivot(index='date',columns='Lage',values='actual').reset_index()
bestand_ext.date += MonthEnd()
bestand_ext = bestand_ext.merge(bestand_oet, on="date", how="outer")

bestand_ext = oet.calculate_trend(bestand_ext,"Freifläche","12-months")

# Replace the two values before the NaNs with NaN
bestand_ext["trend_12months"][bestand_ext["trend_12months"].first_valid_index():bestand_ext["trend_12months"].first_valid_index() + 1] = pd.NA

# Adjust target to 50% of the plan
bestand_ext["plan_util"] = bestand_ext["plan"] * 0.5
bestand_ext["plan_linear_util"] = bestand_ext["plan_linear"] * 0.5

bestand_ext["total"] = bestand_ext["Freifläche"] + bestand_ext["Sonstige"]

#%% 

##### Figure Stock and additions #####

dict_pv_util_stock = {}
dict_pv_util_stock["base"]             = oet.base_default.copy()
dict_pv_util_stock["base"]["title"]    = "Freiflächenphotovoltaik: Bestand und Trend"
dict_pv_util_stock["base"]["data"]     = bestand_ext
dict_pv_util_stock["base"]["x_axis_range"]  = ["2017-01-31","2041-02-15"]
dict_pv_util_stock["base"]["y_axis_range"]  = [0,210]
dict_pv_util_stock["base"]["margin"]       = dict(l=0, r=0, t=40, b=40, pad=0)
dict_pv_util_stock["base"]["showlegend"]   = False
dict_pv_util_stock["base"]["source_file"]  = "docs/germany/data/pv_mastr_data.csv"
dict_pv_util_stock["base"]["language"]     = "de"

del dict_pv_util_stock["base"]["buttons"]

dict_pv_util_stock["figure_actual1"]         = {}
dict_pv_util_stock["figure_actual1"]["name"] = "Sonstige"
dict_pv_util_stock["figure_actual1"]["x"]    = "date"
dict_pv_util_stock["figure_actual1"]["y"]    = "total"
dict_pv_util_stock["figure_actual1"]["type"] = "scatter"
dict_pv_util_stock["figure_actual1"]["mode"] = "lines"
dict_pv_util_stock["figure_actual1"]["line"] = {"dash" : "solid", "width" : 0}
dict_pv_util_stock["figure_actual1"]["fill"] = "tozeroy"
dict_pv_util_stock["figure_actual1"]["fillcolor"] = "pv_sonstige"
# dict_pv_util_stock["figure_actual1"]["hover"] = "%{customdata}"
# dict_pv_util_stock["figure_actual1"]["customdata"] = "Sonstige"

dict_pv_util_stock["figure_actual2"] = dict_pv_util_stock["figure_actual1"].copy()
dict_pv_util_stock["figure_actual2"]["name"] = "Freifläche"
dict_pv_util_stock["figure_actual2"]["y"]    = "Freifläche"
dict_pv_util_stock["figure_actual2"]["fillcolor"] = "pv_util_fill"
dict_pv_util_stock["figure_actual2"]["hover"] = "%{y:.1f} GW"

dict_pv_util_stock["figure_actual3"] = dict_pv_util_stock["figure_actual2"].copy()
dict_pv_util_stock["figure_actual3"]["name"] = ""
dict_pv_util_stock["figure_actual3"]["showlegend"] = False
dict_pv_util_stock["figure_actual3"]["hoverinfo"]  = "skip"
dict_pv_util_stock["figure_actual3"]["y"]          = "Freifläche"
dict_pv_util_stock["figure_actual3"]["mode"]       = "lines"
dict_pv_util_stock["figure_actual3"]["line"]       = {"dash" : "solid", "width" : 1}
dict_pv_util_stock["figure_actual3"]["color"]      = "pv_util"
del dict_pv_util_stock["figure_actual3"]["fill"]
del dict_pv_util_stock["figure_actual3"]["hover"]

dict_pv_util_stock["figure_actual4"] = dict_pv_util_stock["figure_actual2"].copy()
dict_pv_util_stock["figure_actual4"]["name"] = "Freifläche (Trend)"
dict_pv_util_stock["figure_actual4"]["y"]    = "trend_12months"
dict_pv_util_stock["figure_actual4"]["fillcolor"] = "pv_util_trend_fill"

dict_pv_util_stock["figure_actual5"] = {}
dict_pv_util_stock["figure_actual5"]["name"] = "Gesamt"
dict_pv_util_stock["figure_actual5"]["x"]    = "date"
dict_pv_util_stock["figure_actual5"]["y"]    = "total"
dict_pv_util_stock["figure_actual5"]["type"] = "scatter"
dict_pv_util_stock["figure_actual5"]["mode"] = "lines"
dict_pv_util_stock["figure_actual5"]["line"] = {"dash" : "solid", "width" : 1}
dict_pv_util_stock["figure_actual5"]["color"] = "first"
dict_pv_util_stock["figure_actual5"]["hover"] = "%{y:.1f} GW"

# Regierungsziele
dict_pv_util_stock["figure_actual6"] = dict_pv_util_stock["figure_actual5"].copy()
dict_pv_util_stock["figure_actual6"]["y"] = "plan_linear_util"
dict_pv_util_stock["figure_actual6"]["name"] = "Regierungsziel (linear)"
dict_pv_util_stock["figure_actual6"]["line"] = {"dash" : "dot", "width" : 1.5}

# Regierungsziele (Jahre)
dict_pv_util_stock["figure_actual7"] = dict_pv_util_stock["figure_actual6"].copy()
dict_pv_util_stock["figure_actual7"]["y"] = "plan_util"
dict_pv_util_stock["figure_actual7"]["name"] = "Regierungsziel (Jahr)"
dict_pv_util_stock["figure_actual7"]["line"] = {"dash" : "dot", "width" : 1.5}
dict_pv_util_stock["figure_actual7"]["mode"] = "markers+text"

text_points = bestand_ext['plan_util'].apply(lambda x: '{0:1.0f} GW'.format(x))

dict_pv_util_stock["figure_actual7"]["text"] = text_points
dict_pv_util_stock["figure_actual7"]["textposition"] = "top left"

dict_pv_util_stock["annotation1"] = {}
dict_pv_util_stock["annotation1"]["text"] = "Bestand"
dict_pv_util_stock["annotation1"]["x"] = "2023-06-30"
dict_pv_util_stock["annotation1"]["y"] = 11
dict_pv_util_stock["annotation1"]["xref"] = "x"
dict_pv_util_stock["annotation1"]["yref"] = "y"
dict_pv_util_stock["annotation1"]["showarrow"] = False
dict_pv_util_stock["annotation1"]["font"] = dict(size=14,family="Arial", color = "rgb(0, 0, 0)")

dict_pv_util_stock["annotation2"] = {}
dict_pv_util_stock["annotation2"]["text"] = "Trend"
dict_pv_util_stock["annotation2"]["x"] = "2033"
dict_pv_util_stock["annotation2"]["y"] = 25
dict_pv_util_stock["annotation2"]["xref"] = "x"
dict_pv_util_stock["annotation2"]["yref"] = "y"
dict_pv_util_stock["annotation2"]["showarrow"] = False
dict_pv_util_stock["annotation2"]["font"] = dict(size=14,family="Arial", color = "rgb(0, 0, 0)")

dict_pv_util_stock["annotation3"] = {}
dict_pv_util_stock["annotation3"]["text"] = "Andere PV"
dict_pv_util_stock["annotation3"]["x"] = "2018-06-30"
dict_pv_util_stock["annotation3"]["y"] = 25
dict_pv_util_stock["annotation3"]["xref"] = "x"
dict_pv_util_stock["annotation3"]["yref"] = "y"
dict_pv_util_stock["annotation3"]["showarrow"] = False
dict_pv_util_stock["annotation3"]["font"] = dict(size=12,family="Arial", color = "rgb(0, 0, 0)")

dict_pv_util_stock["annotation4"] = {}
dict_pv_util_stock["annotation4"]["text"] = "Regierungsziele"
dict_pv_util_stock["annotation4"]["x"] = "2030-12-31"
dict_pv_util_stock["annotation4"]["y"] = 110
dict_pv_util_stock["annotation4"]["xref"] = "x"
dict_pv_util_stock["annotation4"]["yref"] = "y"
dict_pv_util_stock["annotation4"]["showarrow"] = True
dict_pv_util_stock["annotation4"]["arrowwidth"] = 1.5
dict_pv_util_stock["annotation4"]["arrowcolor"] = "rgba(0, 0, 0, .5)"
dict_pv_util_stock["annotation4"]["yshift"] = 5
dict_pv_util_stock["annotation4"]["ay"] = -45
dict_pv_util_stock["annotation4"]["ax"] = 15
dict_pv_util_stock["annotation4"]["font"] = dict(size=14,family="Arial", color = "rgba(0, 0, 0,.75)")

# Dynamic last month code
last_number = bestand_ext["Freifläche"].max()
last_date1  = bestand_ext.loc[bestand_ext["Freifläche"] == last_number, "date"].values[0]
last_date2 = pd.to_datetime(str(last_date1)).strftime('%Y-%m-%d')

last_year = np.datetime_as_string(last_date1, unit='Y')
last_month = last_date1.astype('datetime64[M]').item().strftime('%B')

months_translation = {
    'January': 'Januar', 'February': 'Februar', 'March': 'März', 'April': 'April',
    'May': 'Mai', 'June': 'Juni', 'July': 'Juli', 'August': 'August',
    'September': 'September', 'October': 'Oktober', 'November': 'November', 'December': 'Dezember'
}

last_month_de = months_translation[last_month]

dict_pv_util_stock["annotation5"] = {}
dict_pv_util_stock["annotation5"]["text"] = f"{last_month_de} {last_year}:<br>{round(last_number,1)} GW "
dict_pv_util_stock["annotation5"]["x"] = last_date2
dict_pv_util_stock["annotation5"]["y"] = last_number
dict_pv_util_stock["annotation5"]["xref"] = "x"
dict_pv_util_stock["annotation5"]["yref"] = "y"
dict_pv_util_stock["annotation5"]["showarrow"] = True
dict_pv_util_stock["annotation5"]["arrowwidth"] = 1.5
dict_pv_util_stock["annotation5"]["arrowcolor"] = "rgba(252, 186, 3, 1)"
dict_pv_util_stock["annotation5"]["yshift"] = 5
dict_pv_util_stock["annotation5"]["xshift"] = -5
dict_pv_util_stock["annotation5"]["ay"] = -150
dict_pv_util_stock["annotation5"]["ax"] = -50
dict_pv_util_stock["annotation5"]["font"] = dict(size=16,family="Arial", color = "rgba(185, 135, 0, 1)")

fig_pv_util_stock = oet.create_fig(dict_pv_util_stock)

# Add custom data
fig_pv_util_stock["data"][0]["customdata"] = bestand_ext["Sonstige"]
fig_pv_util_stock["data"][0]["hovertemplate"] = "%{customdata:.1f} GW"

fig_pv_util_stock.write_html("docs/germany/figures/pv_util_stock.de.html", include_plotlyjs="directory")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
