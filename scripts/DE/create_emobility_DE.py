#%% Load libraries #################################################################################

import sys
import os
import pandas as pd
import numpy as np
import copy
from pathlib import Path
from datetime import date
from pandas.tseries.offsets import *

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px
import statsmodels.api as sm

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'scripts':
    os.chdir('../../')
else:
    sys.path.append('.')

print(os.getcwd())

import scripts.util as oet
import scripts.colors as colors

####################################################################################################
# EMobility #######################################################################################
####################################################################################################

print("")
print("---------- Script: e-mobility ----------")
print("")

#%% Create directory

Path("docs/germany/figures").mkdir(parents=True, exist_ok=True)

####################################################################################################
# Define base dictionaries - to be re-used
####################################################################################################

base_default = {
    "title"         : "Installierte Leistung Photovoltaik",
    "data"          : None,
    "color_dict"    : colors.standard,
    "x_axis_title"  : None,
    "x_axis_range"  : ["2017-01", "2031-03"],
    "y_axis_title"  : "Bestand [GW]",
    "y_axis_range"  : [0,250],
    "buttons"  : {
        "y_max_2030"  : 250,
        "y_max_2045"  : 500,
        "button2030"  : "bis 2030",
        "button2045"  : "bis 2045"
        },
    #"source_file"     : "",
    #"language"        : "de"
    }

dict_default = {
    "base"                     : base_default.copy(),
    "legislative_period"       : oet.legislative_period.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend"             : oet.trend.copy(),
    "figure_goal"              : oet.goal.copy(),
    "figure_goal_lin"          : oet.goal_lin.copy(),
    "figure_ar_lead"           : oet.ar_lead.copy(),
    "figure_ar_corr"           : oet.ar_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_needed"  : oet.additions_needed.copy(),
}

#%%
mobility_url = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/kba_bna/"

####################################################################################################
# BEV #############################################################################################
####################################################################################################

#%% BEV admissions ---------------------------------------------------------------------------------

bev_data = oet.prepare_data(mobility_url + "bev_stock_monthly.csv","months")
bev_data = oet.calculate_trend(bev_data,"actual","12-months")

base_bev = copy.deepcopy(base_default)

base_bev["subplots"] =  {
        "rows"        : 2,
        "cols"        : 1,
        "row_heights" : [0.75,0.25]}

dict_bev_de = {
    "base"                         : base_bev,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_trend"                 : oet.trend.copy(),
    "figure_goal"                  : oet.goal.copy(),
    "figure_goal_lin"              : oet.goal_lin.copy(),
    "figure_goal_log"              : oet.goal_lin.copy(),
    "figure_ar_lead"               : oet.ar_lead.copy(),
    "figure_ar_corr"               : oet.ar_corr.copy(),
    "figure_additions"             : oet.additions.copy(),
    "figure_additions_needed_lin"  : oet.additions_needed.copy(),
    "figure_additions_needed_log"  : oet.additions_needed.copy(),
}

dict_bev_de["base"]["data"]                  = bev_data
dict_bev_de["base"]["title"]                 = "Bestand batterieelektrischer Pkw" 
dict_bev_de["base"]["y_axis_title"]          = "Fahrzeugbestand"
dict_bev_de["base"]["y_axis2_title"]         = "Zuwachs"
dict_bev_de["base"]["y_axis_range"]          = [0,16000000]
dict_bev_de["base"]["y_axis2_range"]         = [0,210000]
dict_bev_de["base"]["buttons"]["y_max_2030"] = 16000000
dict_bev_de["base"]["buttons"]["y_max_2045"] = 50000000

dict_bev_de["figure_actual"]["name"]    = "Bestand"
dict_bev_de["figure_actual"]["hover"]   = "%{y:.3s}"

dict_bev_de["figure_trend"]["hover"]   = "%{y:.3s}"

dict_bev_de["figure_goal"]["hover"]   = "%{y:.3s}"
dict_bev_de["figure_goal_lin"]["hover"] = "%{y:.3s}"
dict_bev_de["figure_goal_log"]["hover"] = "%{y:.3s}"

dict_bev_de["figure_goal_log"]["name"]  = "Logistischer Verlauf"
dict_bev_de["figure_goal_log"]["y"]     = "plan_logistic"
dict_bev_de["figure_goal_log"]["line"]  = {"dash" : "dashdot"}

dict_bev_de["figure_additions"]["name"]  = "Zuwachs"
dict_bev_de["figure_additions"]["y"]     = "net_adm_bev"
dict_bev_de["figure_additions"]["hover"] = "%{y:.3s}"

dict_bev_de["figure_additions_needed_lin"]["name"]  = "Benötigter linearer Zuwachs"
dict_bev_de["figure_additions_needed_lin"]["y"]     = "add_plan_linear"
dict_bev_de["figure_additions_needed_lin"]["hover"] = "%{y:.3s}"

dict_bev_de["figure_additions_needed_log"]["name"]  = "Benötigter logistischer Zuwachs"
dict_bev_de["figure_additions_needed_log"]["y"]     = "add_plan_logistic"
dict_bev_de["figure_additions_needed_log"]["hover"] = "%{y:.3s}"
dict_bev_de["figure_additions_needed_log"]["line"]  = {"dash" : "dashdot"}

# English figure

dict_bev_en = copy.deepcopy(dict_bev_de)

dict_bev_en["base"]["title"]                         = "Battery electric passenger car fleet" 
dict_bev_en["base"]["y_axis_title"]                  = "Stock"
dict_bev_en["base"]["y_axis2_title"]                 = "Additions"
dict_bev_en["base"]["buttons"]["button2030"]         = "until 2030"
dict_bev_en["base"]["buttons"]["button2045"]         = "until 2045"
dict_bev_en["figure_actual"]["name"]                 = "Stock"
dict_bev_en["figure_trend"]["name"]                  = "12-month-trend"
dict_bev_en["figure_goal"]["title_group"]            = "<i>Government goals</i>"
dict_bev_en["figure_goal"]["name"]                   = "Target of the coalition"
dict_bev_en["figure_goal_lin"]["name"]               = "Linear growth"
dict_bev_en["figure_goal_log"]["name"]               = "Logistic growth"
dict_bev_en["figure_additions"]["name"]              = "Additions"
dict_bev_en["figure_additions_needed_lin"]["name"]   = "Linear additions needed"
dict_bev_en["figure_additions_needed_log"]["name"]   = "Logistic additions needed"
dict_bev_en["figure_ar_lead"]["title_group"]         = "Ariadne scenarios"
dict_bev_en["figure_ar_lead"]["name"]                = "Lead model"
dict_bev_en["figure_ar_corr"]["name"]                = "Scenario corridor"
dict_bev_en["legislative_period"]["text"]            = "Traffic Light Coalition"

# French figure

dict_bev_fr = copy.deepcopy(dict_bev_de)

dict_bev_fr["base"]["title"]                         = "Flotte de voitures particulières électriques à batterie" 
dict_bev_fr["base"]["y_axis_title"]                  = "Stock"
dict_bev_fr["base"]["y_axis2_title"]                 = "Additions"
dict_bev_fr["base"]["buttons"]["button2030"]         = "jusqu'en 2030"
dict_bev_fr["base"]["buttons"]["button2045"]         = "jusqu'en 2045"
dict_bev_fr["figure_actual"]["name"]                 = "Stock"
dict_bev_fr["figure_trend"]["name"]                  = "12 dernier mois"
dict_bev_fr["figure_goal"]["title_group"]            = "<i>Réglementation</i>"
dict_bev_fr["figure_goal"]["name"]                   = "Objectif de la coalition"
dict_bev_fr["figure_goal_lin"]["name"]               = "Progression linéaire"
dict_bev_fr["figure_goal_log"]["name"]               = "Progression logistique"
dict_bev_fr["figure_additions"]["name"]              = "Additions"
dict_bev_fr["figure_additions_needed_lin"]["name"]   = "Ajouts linéaires nécessaires"
dict_bev_fr["figure_additions_needed_log"]["name"]   = "Ajouts logistiques nécessaires"
dict_bev_fr["figure_ar_lead"]["title_group"]         = "Scénarios Ariadne"
dict_bev_fr["figure_ar_lead"]["name"]                = "Modèle de référence"
dict_bev_fr["figure_ar_corr"]["name"]                = "Intervalle de scénarios"
dict_bev_fr["legislative_period"]["text"]            = "coalition en feu tricolore"

# Create and save figures

fig_bev_de = oet.create_fig(dict_bev_de)
fig_bev_en = oet.create_fig(dict_bev_en)
fig_bev_fr = oet.create_fig(dict_bev_fr)

fig_bev_de.write_html("docs/germany/figures/bev.de.html", include_plotlyjs="directory")
fig_bev_en.write_html("docs/germany/figures/bev.en.html", include_plotlyjs="directory")
fig_bev_fr.write_html("docs/germany/figures/bev.fr.html", include_plotlyjs="directory")

#%% BEV new admissions -----------------------------------------------------------------------------

bev_adm_data = oet.prepare_data(mobility_url + "bev_adm.csv","months")
bev_adm_data = oet.calculate_trend(bev_adm_data,"actual_total","12-months")

###############
#preparing data
###############

##
#Calculate the average share of BEV and electric cars for each year
##

bev_adm_data["year"] = bev_adm_data["date"].dt.year

#calculate the sums for each year
avg_adm_bev = bev_adm_data.groupby("year").agg({
    "actual_bev":"sum",
    "actual_phev":"sum",
    "actual_total":"sum"
    }).reset_index()
avg_adm_bev.rename(columns={"actual_bev_share":"avg_adm_bev",
                            "actual_electric_share" : "avg_adm_electric"}, inplace=True)

#calculate the average share of BEV and BEV+PHEV
avg_adm_bev["avg_adm_bev"] = (avg_adm_bev["actual_bev"] / avg_adm_bev["actual_total"] * 100).round(1)
avg_adm_bev["avg_adm_electric"] = ((avg_adm_bev["actual_bev"] + avg_adm_bev["actual_phev"]) / avg_adm_bev["actual_total"] * 100).round(1)

#add the data to the initial dataframe
bev_adm_data = pd.merge(bev_adm_data, avg_adm_bev[["year","avg_adm_bev", "avg_adm_electric"]], on="year", right_index=False)

###############
#creating the dictionary
###############

base_bev_adm = copy.deepcopy(base_default)

dict_bev_adm_de = {
    "base"                         : base_bev_adm,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
}

dict_bev_adm_de["base"]["data"]                  = bev_adm_data
dict_bev_adm_de["base"]["color_dict"]            = colors.emobility
dict_bev_adm_de["base"]["title"]                 = "Anteil an den monatlichen Neuzulassungen"
dict_bev_adm_de["base"]["x_axis_range"]          = ["2018-01-01","2027-12-31"]
dict_bev_adm_de["base"]["y_axis_title"]          = "Prozent"
dict_bev_adm_de["base"]["y_axis_range"]          = [0,82]
dict_bev_adm_de["base"]["barmode"]               = "stack"
dict_bev_adm_de["base"]["legend_trace_order"]    = "reversed + grouped"
dict_bev_adm_de["base"]["margin"]                = dict(l=0, r=0, t=30, b=0, pad=0)

dict_bev_adm_de["base"]["legend_dict"]           = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 0.01,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "left",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "groupclick"     : "toggleitem"}

del dict_bev_adm_de["base"]["buttons"]

dict_bev_adm_de["figure_actual"]["group"]       = "admissions"
dict_bev_adm_de["figure_actual"]["y"]           = "actual_phev_share"      
dict_bev_adm_de["figure_actual"]["name"]        = "Plug-in-Hybrid"
dict_bev_adm_de["figure_actual"]["color"]       = "phev"
dict_bev_adm_de["figure_actual"]["hover"]       = "%{y:.1f}%"

dict_bev_adm_de["figure_actual2"]["title_group"]  = "PKW-Typ"     
dict_bev_adm_de["figure_actual2"]["group"]        = "admissions"
dict_bev_adm_de["figure_actual2"]["y"]            = "actual_bev_share"     
dict_bev_adm_de["figure_actual2"]["name"]         = "Batterieelektrisch"
dict_bev_adm_de["figure_actual2"]["color"]        = "bev"
dict_bev_adm_de["figure_actual2"]["hover"]        = "%{y:.1f}%"

#Adding Lines for the average share of BEV and electric cars

dict_bev_adm_de["figure_actual3"]["group"]       = "avg"
dict_bev_adm_de["figure_actual3"]["title_group"] = "Jahresdurchschnitt"
dict_bev_adm_de["figure_actual3"]["name"]        = "BEV"
dict_bev_adm_de["figure_actual3"]["y"]           = "avg_adm_bev" 
dict_bev_adm_de["figure_actual3"]["type"]        = "scatter"
dict_bev_adm_de["figure_actual3"]["mode"]        = "lines"
dict_bev_adm_de["figure_actual3"]["line"]        = {"dash": "dashdot"}
dict_bev_adm_de["figure_actual3"]["color"]       = "line"
dict_bev_adm_de["figure_actual3"]["hover"]        = "%{y:.1f}%"     

dict_bev_adm_de["figure_actual4"]["group"]       = "avg"
dict_bev_adm_de["figure_actual4"]["name"]        = "BEV + PHEV"
dict_bev_adm_de["figure_actual4"]["y"]           = "avg_adm_electric"
dict_bev_adm_de["figure_actual4"]["type"]        = "scatter"
dict_bev_adm_de["figure_actual4"]["mode"]        = "lines"
dict_bev_adm_de["figure_actual4"]["line"]        = {"dash": "dot"}
dict_bev_adm_de["figure_actual4"]["color"]       = "line"
dict_bev_adm_de["figure_actual4"]["hover"]       = "%{y:.1f}%"

# English figure

dict_bev_adm_en = copy.deepcopy(dict_bev_adm_de)

dict_bev_adm_en["base"]["title"]                 = "Monthly share in new registrations"
dict_bev_adm_en["base"]["y_axis_title"]          = "Percent"
dict_bev_adm_en["figure_actual"]["title_group"]  = "Type of passenger car"
dict_bev_adm_en["figure_actual3"]["title_group"] = "Annual average"
dict_bev_adm_en["figure_actual4"]["name"] = "BEV + PHEV"
dict_bev_adm_en["figure_actual3"]["name"] = "BEV"
dict_bev_adm_en["figure_actual2"]["name"]         = "Battery-electric"
dict_bev_adm_en["figure_actual"]["name"]        = "Plug-in hybrid"
dict_bev_adm_en["legislative_period"]["text"]    = "Traffic Light Coalition"

# French figure

dict_bev_adm_fr = copy.deepcopy(dict_bev_adm_de)

dict_bev_adm_fr["base"]["title"]                 = "Part mensuelle des nouveaux enregistrements"
dict_bev_adm_fr["base"]["y_axis_title"]          = "Pourcentage"
dict_bev_adm_fr["figure_actual"]["title_group"]  = "Type de voiture "  
dict_bev_adm_fr["figure_actual3"]["title_group"] = "Moyenne annuelle"
dict_bev_adm_fr["figure_actual4"]["name"] = "BEV + PHEV"
dict_bev_adm_fr["figure_actual3"]["name"] = "BEV"   
dict_bev_adm_fr["figure_actual2"]["name"]         = "Électriques à batterie"
dict_bev_adm_fr["figure_actual"]["name"]        = "Hybrides plug-in"
dict_bev_adm_fr["legislative_period"]["text"]    = "coalition en feu tricolore"


# Create and save figures

fig_bev_adm_de = oet.create_fig(dict_bev_adm_de)
fig_bev_adm_en = oet.create_fig(dict_bev_adm_en)
fig_bev_adm_fr = oet.create_fig(dict_bev_adm_fr)


fig_bev_adm_de.write_html("docs/germany/figures/bev_adm.de.html", include_plotlyjs="directory")
fig_bev_adm_en.write_html("docs/germany/figures/bev_adm.en.html", include_plotlyjs="directory")
fig_bev_adm_fr.write_html("docs/germany/figures/bev_adm.fr.html", include_plotlyjs="directory")


#%% cars stock by fuel type quarterly - new by template

cars_stock_data = oet.prepare_data(mobility_url + "cars_stock.csv","quarters")

###############
#preparing data
###############

#add relative shares to the dataframe
columns = ["petrol","diesel","electric","hydrogen_fuelcell","plug_in_hybrid","no_plugin_hybrid","hybrid_nodifferentiation","gas","hydrogen","other"]
for index, row in cars_stock_data.iterrows():
    if pd.isnull(row["total"]):
        relevant_columns = row.drop(["month","date","total"])
        cars_stock_data.at[index,"total"] = relevant_columns.sum()

for col in columns:
    relative_data = cars_stock_data[col] / cars_stock_data["total"] * 100
    relative_data = relative_data.round(2)
    cars_stock_data[col + "_share"] = relative_data

#replace NaN with 0 (Otherwise the traces will not be staced correctly)
cars_stock_data.fillna(0, inplace=True)



car_stock_base = copy.deepcopy(base_default)

fig_actual_template = oet.actual.copy()

# Add , as thousand separator
fig_actual_template["hover"] = "%{y:,}"

dict_cars_stock_de = {
    "base"                : car_stock_base,
    "legislative_period"  : oet.legislative_period.copy(),
    "figure_actual"       : fig_actual_template.copy(),
    "figure_actual1"      : fig_actual_template.copy(),
    "figure_actual2"      : fig_actual_template.copy(),
    "figure_actual3"      : fig_actual_template.copy(),
    "figure_actual4"      : fig_actual_template.copy(),
    "figure_actual5"      : fig_actual_template.copy(),
    "figure_actual6"      : fig_actual_template.copy(),
    "figure_actual7"      : fig_actual_template.copy(),
    "figure_actual8"      : fig_actual_template.copy(),
    "figure_actual9"      : fig_actual_template.copy(),
    "figure_actual10"     : fig_actual_template.copy(),
    "figure_actual11"     : fig_actual_template.copy(),
    "figure_actual12"     : fig_actual_template.copy(),
    "figure_actual13"     : fig_actual_template.copy(),
    "figure_actual14"     : fig_actual_template.copy(),
    "figure_actual15"     : fig_actual_template.copy(),
}

#BasisGrafik
dict_cars_stock_de["base"]["data"]                  = cars_stock_data
dict_cars_stock_de["base"]["color_dict"]            = colors.emobility     
dict_cars_stock_de["base"]["title"]                 = "Vierteljährlicher Bestand - PKW"
dict_cars_stock_de["base"]["title_x"]               = 0.2
dict_cars_stock_de["base"]["y_axis_title"]          = "Bestand"
dict_cars_stock_de["base"]["y_axis_range"]          = [0,56000000]
#dict_cars_stock_de["base"]["y_axis_range"]          = [0,56000000]
dict_cars_stock_de["base"]["x_axis_range"]          = ["2012-06-01","2029-12-31"]
dict_cars_stock_de["base"]["barmode"]               = "stack"
dict_cars_stock_de["base"]["margin"]                = dict(l=0, r=50, t=65, b=0, pad=0)
dict_cars_stock_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "groupclick"     : "toggleitem"}
#Legend rechts: x = 0.7, xanchor left, orientation = v

#Buttons raus
#del dict_cars_stock_de["base"]["buttons"]

if "button2030" in dict_cars_stock_de["base"]["buttons"]:
    del dict_cars_stock_de["base"]["buttons"]["button2030"]
if "button2045" in dict_cars_stock_de["base"]["buttons"]:
    del dict_cars_stock_de["base"]["buttons"]["button2045"]
dict_cars_stock_de["base"]["buttons"]["custom_button1"] = "Gesamtbestand"
dict_cars_stock_de["base"]["buttons"]["custom_button2"] = "Anteil alternative Antriebe"
dict_cars_stock_de["base"]["buttons"]['custom_traces1'] = [True, True, True, True,True, True,True,True, True, True, False, False,False, False, False, True]
dict_cars_stock_de["base"]["buttons"]['custom_traces2'] = [False, False, False, False,False, False, False,False, False, False, True, True,  True, True, True, False]
dict_cars_stock_de["base"]["buttons"]["y_max_custom1"]  = 56000000
dict_cars_stock_de["base"]["buttons"]["y_max_custom2"]  = 20
dict_cars_stock_de["base"]["buttons"]["y_title_1"]      = "Bestand"
dict_cars_stock_de["base"]["buttons"]["y_title_2"]      = "Anteil alternative Antriebe in %"
dict_cars_stock_de["base"]["y_axis_title"]              = dict_cars_stock_de["base"]["buttons"]["y_title_1"]
dict_cars_stock_de["base"]["y_axis_range"]              = [0, dict_cars_stock_de["base"]["buttons"]["y_max_custom1"]]
dict_cars_stock_de["base"]["buttons"]["position"]       = {
    "x": 0.0,
    "y": 1.10 
}

#Add figures    

dict_cars_stock_de["figure_actual"]["y"]           = "other"      
dict_cars_stock_de["figure_actual"]["name"]        = "Sonstige"
dict_cars_stock_de["figure_actual"]["color"]       = "other"
dict_cars_stock_de["figure_actual"]["group"]       = "other"
dict_cars_stock_de["figure_actual"]["title_group"]  = "Sonstige" 
dict_cars_stock_de["figure_actual"]["visible"]     = True
dict_cars_stock_de["figure_actual"]["legendrank"]   = 10


dict_cars_stock_de["figure_actual1"]["title_group"] = "Verbrenner" 
dict_cars_stock_de["figure_actual1"]["y"]           = "diesel"      
dict_cars_stock_de["figure_actual1"]["name"]        = "Diesel"
dict_cars_stock_de["figure_actual1"]["color"]       =  "diesel"
dict_cars_stock_de["figure_actual1"]["group"]       = "combustion" 
dict_cars_stock_de["figure_actual1"]["visible"]     = True
dict_cars_stock_de["figure_actual1"]["legendrank"]  = 9


dict_cars_stock_de["figure_actual2"]["y"]           = "petrol"      
dict_cars_stock_de["figure_actual2"]["name"]        = "Benzin"
dict_cars_stock_de["figure_actual2"]["color"]       = "petrol"
dict_cars_stock_de["figure_actual2"]["group"]       = "combustion"
dict_cars_stock_de["figure_actual2"]["visible"]     = True
dict_cars_stock_de["figure_actual2"]["legendrank"]  = 8


dict_cars_stock_de["figure_actual3"]["y"]           = "gas"      
dict_cars_stock_de["figure_actual3"]["name"]        = "Gas (LPG & CNG)"
dict_cars_stock_de["figure_actual3"]["color"]       = "gas"
dict_cars_stock_de["figure_actual3"]["group"]       = "combustion" 
dict_cars_stock_de["figure_actual3"]["visible"]     = True
dict_cars_stock_de["figure_actual3"]["legendrank"]  = 7


dict_cars_stock_de["figure_actual4"]["y"]           = "hydrogen"      
dict_cars_stock_de["figure_actual4"]["name"]        = "Wasserstoff"
dict_cars_stock_de["figure_actual4"]["color"]       = "hydrogen"
dict_cars_stock_de["figure_actual4"]["group"]       = "combustion"
dict_cars_stock_de["figure_actual4"]["visible"]     = True
dict_cars_stock_de["figure_actual4"]["legendrank"]  = 6


dict_cars_stock_de["figure_actual5"]["y"]           = "hybrid_nodifferentiation"   
dict_cars_stock_de["figure_actual5"]["name"]        = "Hybride undifferenziert"
dict_cars_stock_de["figure_actual5"]["color"]       = "hybrid_nodiff"
dict_cars_stock_de["figure_actual5"]["group"]       = "hybrid"
dict_cars_stock_de["figure_actual5"]["title_group"] = "Hybride" 
dict_cars_stock_de["figure_actual5"]["visible"]     = True
dict_cars_stock_de["figure_actual5"]["legendrank"]   = 5


dict_cars_stock_de["figure_actual6"]["y"]           = "no_plugin_hybrid"      
dict_cars_stock_de["figure_actual6"]["name"]        = "No Plug-in-Hybrid"
dict_cars_stock_de["figure_actual6"]["color"]       = "hev"
dict_cars_stock_de["figure_actual6"]["group"]       = "hybrid"
dict_cars_stock_de["figure_actual6"]["title_group"]  = "Hybride"
dict_cars_stock_de["figure_actual6"]["visible"]     = True
dict_cars_stock_de["figure_actual6"]["legendrank"]   = 4


dict_cars_stock_de["figure_actual7"]["y"]           = "plug_in_hybrid"      
dict_cars_stock_de["figure_actual7"]["name"]        = "Plug-in-Hybrid"
dict_cars_stock_de["figure_actual7"]["color"]       = "phev"
dict_cars_stock_de["figure_actual7"]["group"]       = "hybrid" 
dict_cars_stock_de["figure_actual7"]["visible"]     = True
dict_cars_stock_de["figure_actual7"]["legendrank"]   = 3


dict_cars_stock_de["figure_actual8"]["y"]           = "hydrogen_fuelcell"      
dict_cars_stock_de["figure_actual8"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_cars_stock_de["figure_actual8"]["color"]       = "fuel_cell"
dict_cars_stock_de["figure_actual8"]["group"]       = "electric"
dict_cars_stock_de["figure_actual8"]["title_group"]  = "Reine Elektroantriebe" 
dict_cars_stock_de["figure_actual8"]["visible"]     = True
dict_cars_stock_de["figure_actual8"]["legendrank"]  = 2


dict_cars_stock_de["figure_actual9"]["y"]            = "electric"     
dict_cars_stock_de["figure_actual9"]["name"]         = "Batterieelektrisch"
dict_cars_stock_de["figure_actual9"]["color"]        = "bev"
dict_cars_stock_de["figure_actual9"]["group"]        = "electric"
dict_cars_stock_de["figure_actual9"]["title_group"]  = "Reine Elektroantriebe" 
dict_cars_stock_de["figure_actual9"]["visible"]      = True
dict_cars_stock_de["figure_actual9"]["legendrank"]   = 1

dict_cars_stock_de["figure_actual10"]["y"]           = "hybrid_nodifferentiation_share"
dict_cars_stock_de["figure_actual10"]["name"]        = "Hybride undifferenziert"
dict_cars_stock_de["figure_actual10"]["color"]       = "hybrid_nodiff"
dict_cars_stock_de["figure_actual10"]["group"]       = "hybrid"
dict_cars_stock_de["figure_actual10"]["visible"]     = False
dict_cars_stock_de["figure_actual10"]["legendrank"]  = 5
dict_cars_stock_de["figure_actual10"]["hover"]       = "%{y:.1f}%"

dict_cars_stock_de["figure_actual11"]["y"]           = "no_plugin_hybrid_share"      
dict_cars_stock_de["figure_actual11"]["name"]        = "No Plug-in-Hybrid"
dict_cars_stock_de["figure_actual11"]["color"]       = "hev"
dict_cars_stock_de["figure_actual11"]["group"]       = "hybrid"
dict_cars_stock_de["figure_actual11"]["visible"]     = False
dict_cars_stock_de["figure_actual11"]["legendrank"]  = 4
dict_cars_stock_de["figure_actual11"]["hover"]       = "%{y:.1f}%"

dict_cars_stock_de["figure_actual12"]["y"]           = "plug_in_hybrid_share"      
dict_cars_stock_de["figure_actual12"]["name"]        = "Plug-in-Hybrid"
dict_cars_stock_de["figure_actual12"]["color"]       = "phev"
dict_cars_stock_de["figure_actual12"]["group"]       = "hybrid"
dict_cars_stock_de["figure_actual12"]["title_group"]  = "Hybride - Anteile" 
dict_cars_stock_de["figure_actual12"]["visible"]     = False
dict_cars_stock_de["figure_actual12"]["legendrank"]  = 3
dict_cars_stock_de["figure_actual12"]["hover"]       = "%{y:.1f}%"

dict_cars_stock_de["figure_actual13"]["y"]           = "hydrogen_fuelcell_share"
dict_cars_stock_de["figure_actual13"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_cars_stock_de["figure_actual13"]["color"]       = "fuel_cell"
dict_cars_stock_de["figure_actual13"]["group"]       = "electric"
#dict_cars_stock_de["figure_actual13"]["title_group"]  = "reine Elektroantriebe" 
dict_cars_stock_de["figure_actual13"]["visible"]     = False
dict_cars_stock_de["figure_actual13"]["legendrank"]  = 2
dict_cars_stock_de["figure_actual13"]["hover"]       = "%{y:.1f}%"

dict_cars_stock_de["figure_actual14"]["y"]           = "electric_share"      
dict_cars_stock_de["figure_actual14"]["name"]        = "Batterieelektrisch"
dict_cars_stock_de["figure_actual14"]["color"]       = "bev"
dict_cars_stock_de["figure_actual14"]["group"]       = "electric"
dict_cars_stock_de["figure_actual14"]["title_group"] = "Reine Elektroantriebe - Anteile" 
dict_cars_stock_de["figure_actual14"]["visible"]     = False
dict_cars_stock_de["figure_actual14"]["legendrank"]  = 1
dict_cars_stock_de["figure_actual14"]["hover"]       = "%{y:.1f}%"

dict_cars_stock_de["figure_actual15"]["y"]           = "total"
dict_cars_stock_de["figure_actual15"]["name"]        = "Gesamtbestand"
dict_cars_stock_de["figure_actual15"]["color"]       = "black"
dict_cars_stock_de["figure_actual15"]["type"]       = "scatter"
dict_cars_stock_de["figure_actual15"]["mode"]       = "lines"
dict_cars_stock_de["figure_actual15"]["line"]       = {"dash": "dot"}
dict_cars_stock_de["figure_actual15"]["visible"]    = True
dict_cars_stock_de["figure_actual15"]["legendrank"]      = 0

"""
dict_bev_adm_de["figure_actual4"]["type"]        = "scatter"
dict_bev_adm_de["figure_actual4"]["mode"]        = "lines"
dict_bev_adm_de["figure_actual4"]["line"]        = {"dash": "dot"}
"""

#dict


# English figure

dict_cars_stock_en = copy.deepcopy(dict_cars_stock_de)

dict_cars_stock_en["base"]["title"]                     = "Quarterly stock - cars"
dict_cars_stock_en["base"]["y_axis_title"]              = "Stock"
dict_cars_stock_en["base"]["buttons"]["y_title_1"]      = "Stock"
dict_cars_stock_en["base"]["buttons"]["y_title_2"]      = "Share of alternative drives in %"
dict_cars_stock_en["base"]["buttons"]["custom_button1"] = "Total stock"
dict_cars_stock_en["base"]["buttons"]["custom_button2"] = "Share of alternative drives"

dict_cars_stock_en["figure_actual1"]["title_group"]      = "Combustion"
dict_cars_stock_en["figure_actual1"]["name"]             = "Diesel"
dict_cars_stock_en["figure_actual2"]["name"]            = "Petrol"
dict_cars_stock_en["figure_actual3"]["name"]            = "Gas (LPG & CNG)"
dict_cars_stock_en["figure_actual4"]["name"]            = "Hydrogen"

dict_cars_stock_en["figure_actual5"]["title_group"]     = "Hybrid"  
dict_cars_stock_en["figure_actual5"]["name"]            = "Hybrid not differenciated"
dict_cars_stock_en["figure_actual6"]["name"]            = "No Plug-in-Hybrid" 
dict_cars_stock_en["figure_actual7"]["name"]            = "Plug-in-Hybrid" 

dict_cars_stock_en["figure_actual9"]["title_group"]     = "Pure electric"
dict_cars_stock_en["figure_actual8"]["name"]            = "Fuel cell (hydrogen)"
dict_cars_stock_en["figure_actual9"]["name"]            = "Battery-electric"

dict_cars_stock_en["figure_actual"]["title_group"]    = "Others"
dict_cars_stock_en["figure_actual"]["name"]           = "Others"

dict_cars_stock_en["figure_actual14"]["title_group"]    = "Electric - share"
dict_cars_stock_en["figure_actual14"]["name"]           = "Battery-electric"
dict_cars_stock_en["figure_actual13"]["name"]           = "Fuel cell"

dict_cars_stock_en["figure_actual12"]["title_group"]    = "Hybrid - share" 
dict_cars_stock_en["figure_actual12"]["name"]           = "Plug-in-hybrid"
dict_cars_stock_en["figure_actual11"]["name"]           = "No plug-in-hybrid"
dict_cars_stock_en["figure_actual10"]["name"]           = "Hybrid not differenciated"

dict_cars_stock_en["legislative_period"]["text"]        = "Traffic Light Coalition"

# French figure

dict_cars_stock_fr = copy.deepcopy(dict_cars_stock_de)

dict_cars_stock_fr["base"]["title"]                     = "Stock trimestriel - voitures"
dict_cars_stock_fr["base"]["y_axis_title"]              = "Stock"
dict_cars_stock_fr["base"]["buttons"]["y_title_1"]      = "Stock"
dict_cars_stock_fr["base"]["buttons"]["y_title_2"]      = "Part des entraînements alternatifs en %"
dict_cars_stock_fr["base"]["buttons"]["custom_button1"] = "Stock total"
dict_cars_stock_fr["base"]["buttons"]["custom_button2"] = "Part des entraînements alternatifs"

dict_cars_stock_fr["figure_actual1"]["title_group"]   = "Combustion"
dict_cars_stock_fr["figure_actual1"]["name"]          = "Diesel"
dict_cars_stock_fr["figure_actual2"]["name"]         = "Essence"
dict_cars_stock_fr["figure_actual3"]["name"]         = "Gaz (LPG & CNG)"
dict_cars_stock_fr["figure_actual4"]["name"]         = "Hydrogène"

dict_cars_stock_fr["figure_actual5"]["title_group"]  = "Hybride"
dict_cars_stock_fr["figure_actual5"]["name"]         = "Hybrides indifférencié"
dict_cars_stock_fr["figure_actual6"]["name"]         = "Non Hybrides plug-in"
dict_cars_stock_fr["figure_actual7"]["name"]         = "Hybrides plug-in"

dict_cars_stock_fr["figure_actual9"]["title_group"]  = "Électrique"
dict_cars_stock_fr["figure_actual8"]["name"]         = "Pile à combustible (hydrogène)"
dict_cars_stock_fr["figure_actual9"]["name"]         = "Électriques à batterie"

dict_cars_stock_fr["figure_actual"]["title_group"] = "Autres"  
dict_cars_stock_fr["figure_actual"]["name"]        = "Autres"

dict_cars_stock_fr["figure_actual14"]["title_group"] = "Part des voitures électriques"
dict_cars_stock_fr["figure_actual14"]["name"]        = "Électriques à batterie"
dict_cars_stock_fr["figure_actual13"]["name"]        = "Pile à combustible"

dict_cars_stock_fr["figure_actual12"]["title_group"] = "Part des voitures hybrides" 
dict_cars_stock_fr["figure_actual12"]["name"]        = "Hybride plug-in"
dict_cars_stock_fr["figure_actual11"]["name"]        = "Non hybride plug-in"
dict_cars_stock_fr["figure_actual10"]["name"]        = "Hybride indifférencié"

dict_cars_stock_fr["legislative_period"]["text"]     = "coalition en feu tricolore"

# Create and save figures

fig_cars_stock_de = oet.create_fig(dict_cars_stock_de)
fig_cars_stock_en = oet.create_fig(dict_cars_stock_en)
fig_cars_stock_fr = oet.create_fig(dict_cars_stock_fr)

fig_cars_stock_de.write_html("docs/germany/figures/cars_stock.de.html", include_plotlyjs="directory")
fig_cars_stock_en.write_html("docs/germany/figures/cars_stock.en.html", include_plotlyjs="directory")
fig_cars_stock_fr.write_html("docs/germany/figures/cars_stock.fr.html", include_plotlyjs="directory")



###############################################################################
# Heavy Vehicles - Stock
###############################################################################

#%% Trucks General (all trucks) Quarterly Stock

trucks_gen_stock_data = oet.prepare_data(mobility_url + "trucks_gen_stock.csv","quarters")

# replace NaN with 0
trucks_gen_stock_data.fillna(0, inplace=True)

truck_gen_stock_base = copy.deepcopy(base_default)

dict_trucks_gen_stock_de = {
    "base"                         : truck_gen_stock_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual1"               : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
    "figure_actual7"               : oet.actual.copy(),
    "figure_actual8"               : oet.actual.copy(),
    "figure_actual9"               : oet.actual.copy(),
    "figure_actual10"              : oet.actual.copy(),
    "figure_actual11"              : oet.actual.copy(),
    }

dict_trucks_gen_stock_de["base"]["data"]                  = trucks_gen_stock_data
dict_trucks_gen_stock_de["base"]["color_dict"]            = colors.emobility
dict_trucks_gen_stock_de["base"]["title"]                 = "Vierteljährlicher Bestand - Lastkraftwagen"
dict_trucks_gen_stock_de["base"]["title_y"]               = 0.98
dict_trucks_gen_stock_de["base"]["barmode"]               = "stack"
dict_trucks_gen_stock_de["base"]["legend_trace_order"]    = "grouped"
dict_trucks_gen_stock_de["base"]["margin"]                = dict(l=0, r=50, t=65, b=0, pad=0)
dict_trucks_gen_stock_de["base"]["x_axis_range"]          = ["2018-04-01","2027-12-31"]
dict_trucks_gen_stock_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }

if "button2030" in dict_trucks_gen_stock_de["base"]["buttons"]:
    del dict_trucks_gen_stock_de["base"]["buttons"]["button2030"]
if "button2045" in dict_trucks_gen_stock_de["base"]["buttons"]:
    del dict_trucks_gen_stock_de["base"]["buttons"]["button2045"]
dict_trucks_gen_stock_de["base"]["buttons"]["custom_button1"] = "Gesamtbestand"
dict_trucks_gen_stock_de["base"]["buttons"]["custom_button2"] = "Anteil alternative Antriebe"
dict_trucks_gen_stock_de["base"]["buttons"]['custom_traces1'] = [True, True, True, True, True,True, True,True,False, False,False, False]
dict_trucks_gen_stock_de["base"]["buttons"]['custom_traces2'] = [False, False, False, False, False,False, False, False, True, True,  True, True]
dict_trucks_gen_stock_de["base"]["buttons"]["y_max_custom1"]  = 5000000
dict_trucks_gen_stock_de["base"]["buttons"]["y_max_custom2"]  = 5
dict_trucks_gen_stock_de["base"]["buttons"]["y_title_1"]      = "Bestand"
dict_trucks_gen_stock_de["base"]["buttons"]["y_title_2"]      = "Anteil elektrische Antriebe in %"
dict_trucks_gen_stock_de["base"]["y_axis_title"]              = dict_trucks_gen_stock_de["base"]["buttons"]["y_title_1"]
dict_trucks_gen_stock_de["base"]["y_axis_range"]              = [0, dict_trucks_gen_stock_de["base"]["buttons"]["y_max_custom1"]]
dict_trucks_gen_stock_de["base"]["buttons"]["position"]       = {
    "x": 0.0,
    "y": 1.10 
}

dict_trucks_gen_stock_de["figure_actual"]["y"]           = "other"      
dict_trucks_gen_stock_de["figure_actual"]["name"]        = "Sonstige"
dict_trucks_gen_stock_de["figure_actual"]["color"]       = "other"
dict_trucks_gen_stock_de["figure_actual"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual"]["group"]       = "Sonstige" 
dict_trucks_gen_stock_de["figure_actual"]["title_group"] = "Sonstige"
dict_trucks_gen_stock_de["figure_actual"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual"]["legendrank"]  = 12

dict_trucks_gen_stock_de["figure_actual1"]["y"]           = "diesel_petrol"      
dict_trucks_gen_stock_de["figure_actual1"]["name"]        = "Diesel & Benzin"
dict_trucks_gen_stock_de["figure_actual1"]["color"]       = "diesel_petrol"
dict_trucks_gen_stock_de["figure_actual1"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual1"]["group"]       = "combustion"
dict_trucks_gen_stock_de["figure_actual1"]["title_group"] = "Verbrennungsmotoren" 
dict_trucks_gen_stock_de["figure_actual1"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual1"]["legendrank"]  = 11

dict_trucks_gen_stock_de["figure_actual2"]["title_group"] = "Verbrenner" 
dict_trucks_gen_stock_de["figure_actual2"]["y"]           = "gas"      
dict_trucks_gen_stock_de["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_gen_stock_de["figure_actual2"]["color"]       = "gas"
dict_trucks_gen_stock_de["figure_actual2"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual2"]["group"]       = "combustion"
dict_trucks_gen_stock_de["figure_actual2"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual2"]["legendrank"]  = 10

dict_trucks_gen_stock_de["figure_actual3"]["y"]           = "hydrogen"      
dict_trucks_gen_stock_de["figure_actual3"]["name"]        = "Wasserstoff"
dict_trucks_gen_stock_de["figure_actual3"]["color"]       = "hydrogen"
dict_trucks_gen_stock_de["figure_actual3"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual3"]["group"]       = "combustion"
dict_trucks_gen_stock_de["figure_actual3"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual3"]["legendrank"]  = 9

dict_trucks_gen_stock_de["figure_actual4"]["y"]           = "no_plugin_hybrid"      
dict_trucks_gen_stock_de["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_gen_stock_de["figure_actual4"]["color"]       = "hev"
dict_trucks_gen_stock_de["figure_actual4"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual4"]["group"]       = "hybrid"
dict_trucks_gen_stock_de["figure_actual4"]["title_group"] = "Hybride"
dict_trucks_gen_stock_de["figure_actual4"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual4"]["legendrank"]  = 8

dict_trucks_gen_stock_de["figure_actual5"]["y"]           = "plug_in_hybrid"      
dict_trucks_gen_stock_de["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_trucks_gen_stock_de["figure_actual5"]["color"]       = "phev"
dict_trucks_gen_stock_de["figure_actual5"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual5"]["group"]       = "hybrid" 
dict_trucks_gen_stock_de["figure_actual5"]["visible"]     = True
dict_trucks_gen_stock_de["figure_actual5"]["legendrank"]  = 7

dict_trucks_gen_stock_de["figure_actual6"]["y"]           = "hydrogen_fuelcell"      
dict_trucks_gen_stock_de["figure_actual6"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_gen_stock_de["figure_actual6"]["color"]       = "fuel_cell"
dict_trucks_gen_stock_de["figure_actual6"]["group"]       = "electric"
dict_trucks_gen_stock_de["figure_actual6"]["hover"]       = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual6"]["visible"]      = True
dict_trucks_gen_stock_de["figure_actual6"]["legendrank"]   = 6 

dict_trucks_gen_stock_de["figure_actual7"]["y"]            = "electric"     
dict_trucks_gen_stock_de["figure_actual7"]["name"]         = "Batterieelektrisch"
dict_trucks_gen_stock_de["figure_actual7"]["color"]        = "bev"
dict_trucks_gen_stock_de["figure_actual7"]["hover"]        = "%{y:.0f}"
dict_trucks_gen_stock_de["figure_actual7"]["group"]        = "electric"
dict_trucks_gen_stock_de["figure_actual7"]["title_group"]  = "Reine Elektroantriebe"
dict_trucks_gen_stock_de["figure_actual7"]["visible"]      = True 
dict_trucks_gen_stock_de["figure_actual7"]["legendrank"]   = 5

dict_trucks_gen_stock_de["figure_actual8"]["y"]           = "no_plugin_hybrid_share"      
dict_trucks_gen_stock_de["figure_actual8"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_gen_stock_de["figure_actual8"]["color"]       = "hev"
dict_trucks_gen_stock_de["figure_actual8"]["hover"]       = "%{y:.1f}%"
dict_trucks_gen_stock_de["figure_actual8"]["group"]       = "hybrid"
dict_trucks_gen_stock_de["figure_actual8"]["title_group"] = "Hybride - Anteile"
dict_trucks_gen_stock_de["figure_actual8"]["visible"]     = False
dict_trucks_gen_stock_de["figure_actual8"]["legendrank"]  = 4

dict_trucks_gen_stock_de["figure_actual9"]["y"]           = "plug_in_hybrid_share"      
dict_trucks_gen_stock_de["figure_actual9"]["name"]        = "Plug-in-Hybrid"
dict_trucks_gen_stock_de["figure_actual9"]["color"]       = "phev"
dict_trucks_gen_stock_de["figure_actual9"]["hover"]       = "%{y:.1f}%"
dict_trucks_gen_stock_de["figure_actual9"]["group"]       = "hybrid" 
dict_trucks_gen_stock_de["figure_actual9"]["visible"]     = False
dict_trucks_gen_stock_de["figure_actual9"]["legendrank"]  = 3

dict_trucks_gen_stock_de["figure_actual10"]["y"]           = "hydrogen_fuelcell_share"      
dict_trucks_gen_stock_de["figure_actual10"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_gen_stock_de["figure_actual10"]["color"]       = "fuel_cell"
dict_trucks_gen_stock_de["figure_actual10"]["group"]       = "electric"
dict_trucks_gen_stock_de["figure_actual10"]["hover"]       = "%{y:.1f}%"
dict_trucks_gen_stock_de["figure_actual10"]["visible"]     = False
dict_trucks_gen_stock_de["figure_actual10"]["legendrank"]  = 2

dict_trucks_gen_stock_de["figure_actual11"]["y"]            = "electric_share"     
dict_trucks_gen_stock_de["figure_actual11"]["name"]         = "Batterieelektrisch"
dict_trucks_gen_stock_de["figure_actual11"]["color"]        = "bev"
dict_trucks_gen_stock_de["figure_actual11"]["hover"]        = "%{y:.1f}%"
dict_trucks_gen_stock_de["figure_actual11"]["group"]        = "electric"
dict_trucks_gen_stock_de["figure_actual11"]["title_group"]  = "Reine Elektroantriebe - Anteile" 
dict_trucks_gen_stock_de["figure_actual11"]["visible"]      = False 
dict_trucks_gen_stock_de["figure_actual11"]["legendrank"]   = 1


# English figure

dict_trucks_gen_stock_en = copy.deepcopy(dict_trucks_gen_stock_de)

dict_trucks_gen_stock_en["base"]["title"]                 = "Quarterly Stock - Trucks general"
dict_trucks_gen_stock_en["base"]["buttons"]["y_title_1"]  = "Stock"
dict_trucks_gen_stock_en["base"]["buttons"]["y_title_2"]  = "Share electric drive in %"
dict_trucks_gen_stock_en["base"]["y_axis_title"]          = dict_trucks_gen_stock_en["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_trucks_gen_stock_en["figure_actual"]["title_group"]  = "Others" 
dict_trucks_gen_stock_en["figure_actual3"]["title_group"] = "Combustion"
dict_trucks_gen_stock_en["figure_actual4"]["title_group"] = "Hybrid"  
dict_trucks_gen_stock_en["figure_actual7"]["title_group"] = "Fully Electric"
dict_trucks_gen_stock_en["figure_actual8"]["title_group"] = "Hybrid - Shares"
dict_trucks_gen_stock_en["figure_actual11"]["title_group"]= "Fully Electric - Shares"

dict_trucks_gen_stock_en["figure_actual"]["name"]         = "Others"
dict_trucks_gen_stock_en["figure_actual1"]["name"]        = "Diesel & Petrol"
dict_trucks_gen_stock_en["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_gen_stock_en["figure_actual3"]["name"]        = "Hydrogen"
dict_trucks_gen_stock_en["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_gen_stock_en["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_trucks_gen_stock_en["figure_actual6"]["name"]        = "Electric"
dict_trucks_gen_stock_en["figure_actual7"]["name"]        = "Fuel cell (hydrogen)"
dict_trucks_gen_stock_en["figure_actual8"]["name"]        = "No plug-in-hybrid"
dict_trucks_gen_stock_en["figure_actual9"]["name"]        = "Plug-in-hybrid"
dict_trucks_gen_stock_en["figure_actual10"]["name"]       = "Fuel cell (hydrogen)"
dict_trucks_gen_stock_en["figure_actual11"]["name"]       = "Battery-electric"
dict_trucks_gen_stock_en["legislative_period"]["text"]    = "Traffic Light Coalition"
dict_trucks_gen_stock_en["base"]["buttons"]["custom_button1"] = "Total Stock"
dict_trucks_gen_stock_en["base"]["buttons"]["custom_button2"] = "Share Electric Drive"

# French figure

dict_trucks_gen_stock_fr = copy.deepcopy(dict_trucks_gen_stock_de)

dict_trucks_gen_stock_fr["base"]["title"]                 = "Effectif trimestriel - poids lourd"
dict_trucks_gen_stock_fr["base"]["buttons"]["y_title_1"]  = "Stock"
dict_trucks_gen_stock_fr["base"]["buttons"]["y_title_2"]  = "Part des entraînements électriques en %"
dict_trucks_gen_stock_fr["base"]["y_axis_title"]          = dict_trucks_gen_stock_fr["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_trucks_gen_stock_fr["figure_actual"]["title_group"]  = "Autres" 
dict_trucks_gen_stock_fr["figure_actual3"]["title_group"]  = "Combustion"
dict_trucks_gen_stock_fr["figure_actual4"]["title_group"] = "Hybride"  
dict_trucks_gen_stock_fr["figure_actual7"]["title_group"] = "Entraînements purement électriques"
dict_trucks_gen_stock_fr["figure_actual8"]["title_group"] = "Hybride" 
dict_trucks_gen_stock_fr["figure_actual11"]["title_group"]= "Entraînements purement électriques - Proportions"

dict_trucks_gen_stock_fr["figure_actual"]["name"]         = "Autres"
dict_trucks_gen_stock_fr["figure_actual1"]["name"]        = "Diesel et essence"
dict_trucks_gen_stock_fr["figure_actual2"]["name"]        = "Gaz (LPG & CNG)"
dict_trucks_gen_stock_fr["figure_actual3"]["name"]        = "Hydrogène"
dict_trucks_gen_stock_fr["figure_actual4"]["name"]        = "Non hybride rechargeable"
dict_trucks_gen_stock_fr["figure_actual5"]["name"]        = "Hybride plug-in"
dict_trucks_gen_stock_fr["figure_actual6"]["name"]        = "Électrique sur batterie"
dict_trucks_gen_stock_fr["figure_actual7"]["name"]        = "Pile à combustible à hydrogène"
dict_trucks_gen_stock_fr["figure_actual8"]["name"]        = "Électrique sur batterie"
dict_trucks_gen_stock_fr["figure_actual9"]["name"]        = "Pile à combustible à hydrogène"
dict_trucks_gen_stock_fr["figure_actual10"]["name"]       = "Non hybride rechargeable"
dict_trucks_gen_stock_fr["figure_actual11"]["name"]       = "Hybride plug-in"
dict_trucks_gen_stock_fr["legislative_period"]["text"]    = "coalition en feu tricolore"
dict_trucks_gen_stock_fr["base"]["buttons"]["custom_button1"] = "Total des effectifs"
dict_trucks_gen_stock_fr["base"]["buttons"]["custom_button2"] = "Part des moteurs électriques"


fig_trucks_gen_stock_de = oet.create_fig(dict_trucks_gen_stock_de)
fig_trucks_gen_stock_en = oet.create_fig(dict_trucks_gen_stock_en)
fig_trucks_gen_stock_fr = oet.create_fig(dict_trucks_gen_stock_fr)

fig_trucks_gen_stock_de.write_html("docs/germany/figures/trucks_gen_stock.de.html")
fig_trucks_gen_stock_en.write_html("docs/germany/figures/trucks_gen_stock.en.html")
fig_trucks_gen_stock_fr.write_html("docs/germany/figures/trucks_gen_stock.fr.html")


#%% Trucks Quarterly Stock (Only Semitrailers)
trucks_stock_data = oet.prepare_data(mobility_url + "trucks_semitrailers_stock.csv","quarters")

# replace NaN with 0
trucks_stock_data.fillna(0, inplace=True)

truck_stock_base = copy.deepcopy(base_default)

dict_trucks_stock_de = {
    "base"                         : truck_stock_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual1"               : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
    "figure_actual7"               : oet.actual.copy(),
    "figure_actual8"               : oet.actual.copy(),
    "figure_actual9"               : oet.actual.copy(),
    "figure_actual10"              : oet.actual.copy(),
    "figure_actual11"              : oet.actual.copy()
    }

dict_trucks_stock_de["base"]["data"]                  = trucks_stock_data
dict_trucks_stock_de["base"]["color_dict"]            = colors.emobility
dict_trucks_stock_de["base"]["title"]                 = "Vierteljährlicher Bestand - Sattelzugmaschinen"
dict_trucks_stock_de["base"]["title_y"]               = 0.98
dict_trucks_stock_de["base"]["barmode"]               = "stack"
dict_trucks_stock_de["base"]["legend_trace_order"]    = "grouped"
dict_trucks_stock_de["base"]["margin"]                = dict(l=0, r=50, t=65, b=0, pad=0)
dict_trucks_stock_de["base"]["x_axis_range"]          = ["2018-04-01","2027-12-31"]
dict_trucks_stock_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }

if "button2030" in dict_trucks_stock_de["base"]["buttons"]:
    del dict_trucks_stock_de["base"]["buttons"]["button2030"]
if "button2045" in dict_trucks_stock_de["base"]["buttons"]:
    del dict_trucks_stock_de["base"]["buttons"]["button2045"]
dict_trucks_stock_de["base"]["buttons"]["custom_button1"] = "Gesamtbestand"
dict_trucks_stock_de["base"]["buttons"]["custom_button2"] = "Anteil alternative Antriebe"
dict_trucks_stock_de["base"]["buttons"]['custom_traces1'] = [True, True, True, True, True,True, True,True,False, False,False, False]
dict_trucks_stock_de["base"]["buttons"]['custom_traces2'] = [False, False, False, False, False,False, False, False, True, True,  True, True]
dict_trucks_stock_de["base"]["buttons"]["y_max_custom1"]  = 300000
dict_trucks_stock_de["base"]["buttons"]["y_max_custom2"]  = 1
dict_trucks_stock_de["base"]["buttons"]["y_title_1"]      = "Bestand"
dict_trucks_stock_de["base"]["buttons"]["y_title_2"]      = "Anteil elektrische Antriebe in %"
dict_trucks_stock_de["base"]["y_axis_title"]              = dict_trucks_stock_de["base"]["buttons"]["y_title_1"]
dict_trucks_stock_de["base"]["y_axis_range"]              = [0, dict_trucks_stock_de["base"]["buttons"]["y_max_custom1"]]
dict_trucks_stock_de["base"]["buttons"]["position"]       = {
    "x": 0.0,
    "y": 1.10 
}

dict_trucks_stock_de["figure_actual"]["y"]           = "other"      
dict_trucks_stock_de["figure_actual"]["name"]        = "Sonstige"
dict_trucks_stock_de["figure_actual"]["color"]       = "other"
dict_trucks_stock_de["figure_actual"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual"]["group"]       = "Sonstige" 
dict_trucks_stock_de["figure_actual"]["title_group"] = "Sonstige"
dict_trucks_stock_de["figure_actual"]["visible"]     = True
dict_trucks_stock_de["figure_actual"]["legendrank"]  = 10

dict_trucks_stock_de["figure_actual1"]["y"]           = "diesel_petrol"      
dict_trucks_stock_de["figure_actual1"]["name"]        = "Diesel & Benzin"
dict_trucks_stock_de["figure_actual1"]["color"]       = "diesel_petrol"
dict_trucks_stock_de["figure_actual1"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual1"]["group"]       = "combustion"
dict_trucks_stock_de["figure_actual1"]["title_group"] = "Verbrennungsmotoren" 
dict_trucks_stock_de["figure_actual1"]["visible"]     = True
dict_trucks_stock_de["figure_actual1"]["legendrank"]  = 9

dict_trucks_stock_de["figure_actual2"]["title_group"] = "Verbrenner" 
dict_trucks_stock_de["figure_actual2"]["y"]           = "gas"      
dict_trucks_stock_de["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_stock_de["figure_actual2"]["color"]       = "gas"
dict_trucks_stock_de["figure_actual2"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual2"]["group"]       = "combustion"
dict_trucks_stock_de["figure_actual2"]["visible"]     = True
dict_trucks_stock_de["figure_actual2"]["legendrank"]  = 8

dict_trucks_stock_de["figure_actual3"]["y"]           = "hydrogen"      
dict_trucks_stock_de["figure_actual3"]["name"]        = "Wasserstoff"
dict_trucks_stock_de["figure_actual3"]["color"]       = "hydrogen"
dict_trucks_stock_de["figure_actual3"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual3"]["group"]       = "combustion"
dict_trucks_stock_de["figure_actual3"]["visible"]     = True
dict_trucks_stock_de["figure_actual3"]["legendrank"]  = 7

dict_trucks_stock_de["figure_actual4"]["y"]           = "no_plugin_hybrid"      
dict_trucks_stock_de["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_stock_de["figure_actual4"]["color"]       = "hev"
dict_trucks_stock_de["figure_actual4"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual4"]["group"]       = "hybrid"
dict_trucks_stock_de["figure_actual4"]["title_group"] = "Hybride"
dict_trucks_stock_de["figure_actual4"]["visible"]     = True
dict_trucks_stock_de["figure_actual4"]["legendrank"]  = 6

dict_trucks_stock_de["figure_actual5"]["y"]           = "plug_in_hybrid"      
dict_trucks_stock_de["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_trucks_stock_de["figure_actual5"]["color"]       = "phev"
dict_trucks_stock_de["figure_actual5"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual5"]["group"]       = "hybrid" 
dict_trucks_stock_de["figure_actual5"]["visible"]     = True
dict_trucks_stock_de["figure_actual5"]["legendrank"]  = 5

dict_trucks_stock_de["figure_actual6"]["y"]           = "hydrogen_fuelcell"      
dict_trucks_stock_de["figure_actual6"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_stock_de["figure_actual6"]["color"]       = "fuel_cell"
dict_trucks_stock_de["figure_actual6"]["group"]       = "electric"
dict_trucks_stock_de["figure_actual6"]["hover"]       = "%{y:.0f}"
dict_trucks_stock_de["figure_actual6"]["visible"]      = True 
dict_trucks_stock_de["figure_actual6"]["legendrank"]   = 2

dict_trucks_stock_de["figure_actual7"]["y"]            = "electric"     
dict_trucks_stock_de["figure_actual7"]["name"]         = "Batterieelektrisch"
dict_trucks_stock_de["figure_actual7"]["color"]        = "bev"
dict_trucks_stock_de["figure_actual7"]["hover"]        = "%{y:.0f}"
dict_trucks_stock_de["figure_actual7"]["group"]        = "electric"
dict_trucks_stock_de["figure_actual7"]["title_group"]  = "Reine Elektroantriebe"
dict_trucks_stock_de["figure_actual7"]["visible"]      = True 
dict_trucks_stock_de["figure_actual7"]["legendrank"]   = 1

dict_trucks_stock_de["figure_actual8"]["y"]           = "no_plugin_hybrid_share"      
dict_trucks_stock_de["figure_actual8"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_stock_de["figure_actual8"]["color"]       = "hev"
dict_trucks_stock_de["figure_actual8"]["hover"]       = "%{y:.1f}%"
dict_trucks_stock_de["figure_actual8"]["group"]       = "hybrid"
dict_trucks_stock_de["figure_actual8"]["title_group"] = "Hybride - Anteile"
dict_trucks_stock_de["figure_actual8"]["visible"]     = False
dict_trucks_stock_de["figure_actual8"]["legendrank"]  = 4

dict_trucks_stock_de["figure_actual9"]["y"]           = "plug_in_hybrid_share"      
dict_trucks_stock_de["figure_actual9"]["name"]        = "Plug-in-Hybrid"
dict_trucks_stock_de["figure_actual9"]["color"]       = "phev"
dict_trucks_stock_de["figure_actual9"]["hover"]       = "%{y:.1f}%"
dict_trucks_stock_de["figure_actual9"]["group"]       = "hybrid" 
dict_trucks_stock_de["figure_actual9"]["visible"]     = False
dict_trucks_stock_de["figure_actual9"]["legendrank"]  = 3

dict_trucks_stock_de["figure_actual10"]["y"]           = "hydrogen_fuelcell_share"      
dict_trucks_stock_de["figure_actual10"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_stock_de["figure_actual10"]["color"]       = "fuel_cell"
dict_trucks_stock_de["figure_actual10"]["group"]       = "electric"
dict_trucks_stock_de["figure_actual10"]["hover"]       = "%{y:.1f}%"
dict_trucks_stock_de["figure_actual10"]["visible"]      = False 
dict_trucks_stock_de["figure_actual10"]["legendrank"]   = 2

dict_trucks_stock_de["figure_actual11"]["y"]            = "electric_share"     
dict_trucks_stock_de["figure_actual11"]["name"]         = "Batterieelektrisch"
dict_trucks_stock_de["figure_actual11"]["color"]        = "bev"
dict_trucks_stock_de["figure_actual11"]["hover"]        = "%{y:.1f}%"
dict_trucks_stock_de["figure_actual11"]["group"]        = "electric"
dict_trucks_stock_de["figure_actual11"]["title_group"]  = "Reine Elektroantriebe - Anteile" 
dict_trucks_stock_de["figure_actual11"]["visible"]      = False 
dict_trucks_stock_de["figure_actual11"]["legendrank"]   = 1



# English figure

dict_trucks_stock_en = copy.deepcopy(dict_trucks_stock_de)

dict_trucks_stock_en["base"]["title"]                 = "Quarterly Stock - Semitrailers"
dict_trucks_stock_en["base"]["buttons"]["y_title_1"]  = "Stock"
dict_trucks_stock_en["base"]["buttons"]["y_title_2"]  = "Share electric drive in %"
dict_trucks_stock_en["base"]["y_axis_title"]          = dict_trucks_stock_en["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_trucks_stock_en["figure_actual"]["title_group"] = "Others" 
dict_trucks_stock_en["figure_actual3"]["title_group"] = "Combustion"
dict_trucks_stock_en["figure_actual4"]["title_group"] = "Hybrid"
dict_trucks_stock_en["figure_actual7"]["title_group"] = "Fully Electric"
dict_trucks_stock_en["figure_actual8"]["title_group"] = "Hybrid - Shares"
dict_trucks_stock_en["figure_actual11"]["title_group"]= "Fully Electric - Shares"
dict_trucks_stock_en["figure_actual1"]["name"]        = "Diesel & Petrol"
dict_trucks_stock_en["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_stock_en["figure_actual3"]["name"]        = "Hydrogen"
dict_trucks_stock_en["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_stock_en["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_trucks_stock_en["figure_actual6"]["name"]        = "Fuel cell (hydrogen)"
dict_trucks_stock_en["figure_actual7"]["name"]        = "Electric"
dict_trucks_stock_en["figure_actual8"]["name"]        = "No plug-in-hybrid"
dict_trucks_stock_en["figure_actual9"]["name"]        = "Plug-in-hybrid"
dict_trucks_stock_en["figure_actual10"]["name"]       = "Fuel cell (hydrogen)"
dict_trucks_stock_en["figure_actual11"]["name"]       = "Battery-electric"

dict_trucks_stock_en["legislative_period"]["text"]    = "Traffic Light Coalition"
dict_trucks_stock_en["base"]["buttons"]["custom_button1"] = "Total Stock"
dict_trucks_stock_en["base"]["buttons"]["custom_button2"] = "Share Electric Drive"

# French figure

dict_trucks_stock_fr = copy.deepcopy(dict_trucks_stock_de)

dict_trucks_stock_fr["base"]["title"]                 = "Effectif trimestriel - semi-remorques"
dict_trucks_stock_fr["base"]["buttons"]["y_title_1"]  = "Stock"
dict_trucks_stock_fr["base"]["buttons"]["y_title_2"]  = "Part des entraînements électriques en %"
dict_trucks_stock_fr["base"]["y_axis_title"]          = dict_trucks_stock_fr["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_trucks_stock_fr["figure_actual"]["title_group"]  = "Autres"
dict_trucks_stock_fr["figure_actual3"]["title_group"] = "Combustion"
dict_trucks_stock_fr["figure_actual4"]["title_group"] = "Hybride"  
dict_trucks_stock_fr["figure_actual7"]["title_group"] = "Entraînements purement électriques"
dict_trucks_stock_fr["figure_actual9"]["title_group"] = "Hybride - Proportions"
dict_trucks_stock_fr["figure_actual11"]["title_group"]= "Entraînements purement électriques - Proportions"
dict_trucks_stock_fr["figure_actual"]["name"]         = "Autres"
dict_trucks_stock_fr["figure_actual1"]["name"]        = "Diesel et essence"
dict_trucks_stock_fr["figure_actual2"]["name"]        = "Gaz (LPG & CNG)"
dict_trucks_stock_fr["figure_actual3"]["name"]        = "Hydrogène"
dict_trucks_stock_fr["figure_actual4"]["name"]        = "Non hybride rechargeable"
dict_trucks_stock_fr["figure_actual5"]["name"]        = "Hybride plug-in"
dict_trucks_stock_fr["figure_actual6"]["name"]        = "Pile à combustible à hydrogène"
dict_trucks_stock_fr["figure_actual7"]["name"]        = "Électrique sur batterie"
dict_trucks_stock_fr["figure_actual8"]["name"]        = "Non hybride rechargeable"
dict_trucks_stock_fr["figure_actual9"]["name"]        = "Hybride plug-in"
dict_trucks_stock_fr["figure_actual10"]["name"]       = "Pile à combustible à hydrogène"
dict_trucks_stock_fr["figure_actual11"]["name"]       = "Électrique sur batterie"
dict_trucks_stock_fr["legislative_period"]["text"]    = "coalition en feu tricolore"
dict_trucks_stock_fr["base"]["buttons"]["custom_button1"] = "Total des effectifs"
dict_trucks_stock_fr["base"]["buttons"]["custom_button2"] = "Part des moteurs électriques"


fig_trucks_stock_de = oet.create_fig(dict_trucks_stock_de)
fig_trucks_stock_en = oet.create_fig(dict_trucks_stock_en)
fig_trucks_stock_fr = oet.create_fig(dict_trucks_stock_fr)

fig_trucks_stock_de.write_html("docs/germany/figures/trucks_stock.de.html")
fig_trucks_stock_en.write_html("docs/germany/figures/trucks_stock.en.html")
fig_trucks_stock_fr.write_html("docs/germany/figures/trucks_stock.fr.html")


#%% Quarterly Stock - Busses

busses_stock_data = oet.prepare_data(mobility_url + "busses_stock.csv","quarters")

# replace NaN with 0
busses_stock_data.fillna(0, inplace=True)

bus_stock_base = copy.deepcopy(base_default)

dict_busses_stock_de = {
    "base"                         : bus_stock_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual1"                : oet.actual.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
    "figure_actual7"               : oet.actual.copy(),
    "figure_actual8"               : oet.actual.copy(),
    "figure_actual9"               : oet.actual.copy(),
    "figure_actual10"              : oet.actual.copy(),
    "figure_actual11"              : oet.actual.copy(),}

dict_busses_stock_de["base"]["data"]                  = busses_stock_data
dict_busses_stock_de["base"]["color_dict"]            = colors.emobility  
dict_busses_stock_de["base"]["title"]                 = "Vierteljährlicher Bestand - Kraftomnibusse"
dict_busses_stock_de["base"]["title_y"]               = 0.98
dict_busses_stock_de["base"]["barmode"]               = "stack"
dict_busses_stock_de["base"]["legend_trace_order"]    = "grouped"
dict_busses_stock_de["base"]["margin"]                = dict(l=0, r=50, t=65, b=0, pad=0)
dict_busses_stock_de["base"]["x_axis_range"]          = ["2018-04-01","2027-12-31"]
dict_busses_stock_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }


if "button2030" in dict_busses_stock_de["base"]["buttons"]:
    del dict_busses_stock_de["base"]["buttons"]["button2030"]
if "button2045" in dict_busses_stock_de["base"]["buttons"]:
    del dict_busses_stock_de["base"]["buttons"]["button2045"]
dict_busses_stock_de["base"]["buttons"]["custom_button1"] = "Gesamtbestand"
dict_busses_stock_de["base"]["buttons"]["custom_button2"] = "Anteil alternative Antriebe"
dict_busses_stock_de["base"]["buttons"]['custom_traces1'] = [True, True, True, True, True,True, True,True,False, False,False, False]
dict_busses_stock_de["base"]["buttons"]['custom_traces2'] = [False, False, False, False, False,False, False, False, True, True,  True, True]
dict_busses_stock_de["base"]["buttons"]["y_max_custom1"]  = 100000
dict_busses_stock_de["base"]["buttons"]["y_max_custom2"]  = 20
dict_busses_stock_de["base"]["buttons"]["y_title_1"]      = "Bestand"
dict_busses_stock_de["base"]["buttons"]["y_title_2"]      = "Anteil alternative Antriebe in %"
dict_busses_stock_de["base"]["y_axis_title"]              = dict_busses_stock_de["base"]["buttons"]["y_title_1"]
dict_busses_stock_de["base"]["y_axis_range"]              = [0, dict_busses_stock_de["base"]["buttons"]["y_max_custom1"]]
dict_busses_stock_de["base"]["buttons"]["position"]       = {
    "x": 0.0,
    "y": 1.10 
}

dict_busses_stock_de["figure_actual1"]["y"]           = "other"      
dict_busses_stock_de["figure_actual1"]["name"]        = "Sonstige"
dict_busses_stock_de["figure_actual1"]["color"]       = "other"
dict_busses_stock_de["figure_actual1"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual1"]["group"]       = "others"
dict_busses_stock_de["figure_actual1"]["title_group"] = "Sonstige" 
dict_busses_stock_de["figure_actual1"]["visible"]     = True
dict_busses_stock_de["figure_actual1"]["legendrank"]  = 10

dict_busses_stock_de["figure_actual"]["y"]           = "diesel_petrol"      
dict_busses_stock_de["figure_actual"]["name"]        = "Diesel & Benzin"
dict_busses_stock_de["figure_actual"]["color"]       = "diesel"
dict_busses_stock_de["figure_actual"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual"]["group"]       = "combustion"
dict_busses_stock_de["figure_actual"]["title_group"] = "Verbrennungsmotoren" 
dict_busses_stock_de["figure_actual"]["visible"]     = True
dict_busses_stock_de["figure_actual"]["legendrank"]  = 9

dict_busses_stock_de["figure_actual2"]["title_group"] = "Verbrenner" 
dict_busses_stock_de["figure_actual2"]["y"]           = "gas"      
dict_busses_stock_de["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_busses_stock_de["figure_actual2"]["color"]       = "gas"
dict_busses_stock_de["figure_actual2"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual2"]["group"]       = "combustion"
dict_busses_stock_de["figure_actual2"]["visible"]     = True
dict_busses_stock_de["figure_actual2"]["legendrank"]  = 8

dict_busses_stock_de["figure_actual3"]["y"]           = "hydrogen"      
dict_busses_stock_de["figure_actual3"]["name"]        = "Wasserstoff"
dict_busses_stock_de["figure_actual3"]["color"]       = "hydrogen"
dict_busses_stock_de["figure_actual3"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual3"]["group"]       = "combustion"
dict_busses_stock_de["figure_actual3"]["visible"]     = True
dict_busses_stock_de["figure_actual3"]["legendrank"]  = 7

dict_busses_stock_de["figure_actual4"]["y"]           = "no_plugin_hybrid"      
dict_busses_stock_de["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_busses_stock_de["figure_actual4"]["color"]       = "hev"
dict_busses_stock_de["figure_actual4"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual4"]["group"]       = "hybrid"
dict_busses_stock_de["figure_actual4"]["title_group"] = "Hybride"
dict_busses_stock_de["figure_actual4"]["visible"]     = True
dict_busses_stock_de["figure_actual4"]["legendrank"]  = 6

dict_busses_stock_de["figure_actual5"]["y"]           = "plug_in_hybrid"      
dict_busses_stock_de["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_busses_stock_de["figure_actual5"]["color"]       = "phev"
dict_busses_stock_de["figure_actual5"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual5"]["group"]       = "hybrid" 
dict_busses_stock_de["figure_actual5"]["visible"]     = True
dict_busses_stock_de["figure_actual5"]["legendrank"]  = 5

dict_busses_stock_de["figure_actual6"]["y"]            = "electric"     
dict_busses_stock_de["figure_actual6"]["name"]         = "Batterieelektrisch"
dict_busses_stock_de["figure_actual6"]["color"]        = "bev"
dict_busses_stock_de["figure_actual6"]["hover"]        = "%{y:.0f}"
dict_busses_stock_de["figure_actual6"]["group"]        = "electric"
dict_busses_stock_de["figure_actual6"]["title_group"]  = "Reine Elektroantriebe"
dict_busses_stock_de["figure_actual6"]["visible"]      = True 
dict_busses_stock_de["figure_actual6"]["legendrank"]   = 1

dict_busses_stock_de["figure_actual7"]["y"]           = "hydrogen_fuelcell"      
dict_busses_stock_de["figure_actual7"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_busses_stock_de["figure_actual7"]["color"]       = "fuel_cell"
dict_busses_stock_de["figure_actual7"]["group"]       = "electric"
dict_busses_stock_de["figure_actual7"]["hover"]       = "%{y:.0f}"
dict_busses_stock_de["figure_actual7"]["visible"]      = True
dict_busses_stock_de["figure_actual7"]["legendrank"]   = 2

dict_busses_stock_de["figure_actual8"]["y"]           = "no_plugin_hybrid_share"      
dict_busses_stock_de["figure_actual8"]["name"]        = "No Plug-in-Hybrid"
dict_busses_stock_de["figure_actual8"]["color"]       = "hev"
dict_busses_stock_de["figure_actual8"]["hover"]       = "%{y:.1f}%"
dict_busses_stock_de["figure_actual8"]["group"]       = "hybrid_share"
dict_busses_stock_de["figure_actual8"]["title_group"] = "Hybride - Anteile"
dict_busses_stock_de["figure_actual8"]["visible"]     = False
dict_busses_stock_de["figure_actual8"]["legendrank"]  = 4

dict_busses_stock_de["figure_actual9"]["y"]           = "plug_in_hybrid_share"      
dict_busses_stock_de["figure_actual9"]["name"]        = "Plug-in-Hybrid"
dict_busses_stock_de["figure_actual9"]["color"]       = "phev"
dict_busses_stock_de["figure_actual9"]["hover"]       = "%{y:.1f}%"
dict_busses_stock_de["figure_actual9"]["group"]       = "hybrid_share" 
dict_busses_stock_de["figure_actual9"]["visible"]     = False
dict_busses_stock_de["figure_actual9"]["legendrank"]  = 3

dict_busses_stock_de["figure_actual10"]["y"]           = "hydrogen_fuelcell_share"      
dict_busses_stock_de["figure_actual10"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_busses_stock_de["figure_actual10"]["color"]       = "fuel_cell"
dict_busses_stock_de["figure_actual10"]["group"]       = "electric-share"
dict_busses_stock_de["figure_actual10"]["hover"]       = "%{y:.1f}%"
dict_busses_stock_de["figure_actual10"]["visible"]      = False 
dict_busses_stock_de["figure_actual10"]["legendrank"]   = 2

dict_busses_stock_de["figure_actual11"]["y"]            = "electric_share"     
dict_busses_stock_de["figure_actual11"]["name"]         = "Batterieelektrisch"
dict_busses_stock_de["figure_actual11"]["color"]        = "bev"
dict_busses_stock_de["figure_actual11"]["hover"]        = "%{y:.1f}%"
dict_busses_stock_de["figure_actual11"]["group"]        = "electric-share"
dict_busses_stock_de["figure_actual11"]["title_group"]  = "Reine Elektroantriebe - Anteile" 
dict_busses_stock_de["figure_actual11"]["visible"]      = False 
dict_busses_stock_de["figure_actual11"]["legendrank"]   = 1






# English figure

dict_busses_stock_en = copy.deepcopy(dict_busses_stock_de)

dict_busses_stock_en["base"]["title"]                 = "Quarterly Stock - Busses"
dict_busses_stock_en["base"]["buttons"]["y_title_1"]  = "Stock"
dict_busses_stock_en["base"]["buttons"]["y_title_2"]  = "Share electric drive in %"
dict_busses_stock_en["figure_actual1"]["title_group"] = "Others"
dict_busses_stock_en["figure_actual"]["title_group"]  = "Combustion"
dict_busses_stock_en["figure_actual3"]["title_group"] = "Hybrid"  
dict_busses_stock_en["figure_actual6"]["title_group"] = "Fully Electric"
dict_busses_stock_en["figure_actual8"]["title_group"] = "Fully Electric"        
dict_busses_stock_en["figure_actual10"]["title_group"] = "Hybrid"
dict_busses_stock_en["figure_actual1"]["name"]        = "Others" 
dict_busses_stock_en["figure_actual"]["name"]         = "Diesel & Petrol"
dict_busses_stock_en["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_busses_stock_en["figure_actual3"]["name"]        = "Hydrogen"
dict_busses_stock_en["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_busses_stock_en["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_busses_stock_en["figure_actual6"]["name"]        = "Electric"
dict_busses_stock_en["figure_actual7"]["name"]        = "Fuel cell (hydrogen)"
dict_busses_stock_en["figure_actual8"]["name"]        = "Share battery-electric"
dict_busses_stock_en["figure_actual9"]["name"]        = "Share fuel cell (hydrogen)"
dict_busses_stock_en["figure_actual10"]["name"]       = "Share no plug-in-hybrid"
dict_busses_stock_en["figure_actual11"]["name"]       = "Share plug-in-hybrid"
dict_busses_stock_en["legislative_period"]["text"]    = "Traffic Light Coalition"
dict_busses_stock_en["base"]["buttons"]["custom_button1"] = "Total Stock"
dict_busses_stock_en["base"]["buttons"]["custom_button2"] = "Share Electric Drive"

# French figure

dict_busses_stock_fr = copy.deepcopy(dict_busses_stock_de)

dict_busses_stock_fr["base"]["title"]                 = "Effectif trimestriel - Autobus et autocars"
dict_busses_stock_fr["base"]["buttons"]["y_title_1"]  = "Stock"
dict_busses_stock_fr["base"]["buttons"]["y_title_2"]  = "Part des entraînements électriques en %"
dict_busses_stock_fr["figure_actual1"]["title_group"]  = "Autres"
dict_busses_stock_fr["figure_actual"]["title_group"]  = "Combustion"
dict_busses_stock_fr["figure_actual3"]["title_group"] = "Hybride"  
dict_busses_stock_fr["figure_actual6"]["title_group"] = "entraînements purement électriques"
dict_busses_stock_fr["figure_actual8"]["title_group"] = "entraînements purement électriques"        
dict_busses_stock_fr["figure_actual10"]["title_group"] = "Hybride" 
dict_busses_stock_fr["figure_actual1"]["name"]         = "Autres"
dict_busses_stock_fr["figure_actual"]["name"]         = "Diesel et essence"
dict_busses_stock_fr["figure_actual2"]["name"]        = "Gaz (LPG & CNG)"
dict_busses_stock_fr["figure_actual3"]["name"]        = "Hydrogène"
dict_busses_stock_fr["figure_actual4"]["name"]        = "Non hybride rechargeable"
dict_busses_stock_fr["figure_actual5"]["name"]        = "Hybride plug-in"
dict_busses_stock_fr["figure_actual6"]["name"]        = "Électrique sur batterie"
dict_busses_stock_fr["figure_actual7"]["name"]        = "Pile à combustible à hydrogène"
dict_busses_stock_fr["figure_actual8"]["name"]        = "Proportion Électrique sur batterie"
dict_busses_stock_fr["figure_actual9"]["name"]        = "Proportion Pile à combustible à hydrogène"
dict_busses_stock_fr["figure_actual10"]["name"]       = "Proportion Non hybride rechargeable"
dict_busses_stock_fr["figure_actual11"]["name"]       = "Proportion Hybride plug-in"
dict_busses_stock_fr["legislative_period"]["text"]    = "coalition en feu tricolore"
dict_busses_stock_fr["base"]["buttons"]["custom_button1"] = "Total des effectifs"
dict_busses_stock_fr["base"]["buttons"]["custom_button2"] = "Part des moteurs électriques"

fig_busses_stock_de = oet.create_fig(dict_busses_stock_de)
fig_busses_stock_en = oet.create_fig(dict_busses_stock_en)
fig_busses_stock_fr = oet.create_fig(dict_busses_stock_fr)

fig_busses_stock_de.write_html("docs/germany/figures/busses_stock.de.html")
fig_busses_stock_en.write_html("docs/germany/figures/busses_stock.en.html")
fig_busses_stock_fr.write_html("docs/germany/figures/busses_stock.fr.html")


#%% Tractors Total (including Semitrailers)
tractors_stock_data = oet.prepare_data(mobility_url + "tractors_tot_stock.csv","quarters")

# replace NaN with 0
tractors_stock_data.fillna(0, inplace=True)

tractors_stock_base = copy.deepcopy(base_default)

dict_tractors_stock_de = {
    "base"                         : tractors_stock_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual1"               : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
    "figure_actual7"               : oet.actual.copy(),
    "figure_actual8"               : oet.actual.copy(),
    "figure_actual9"               : oet.actual.copy(),
    "figure_actual10"              : oet.actual.copy(),
    "figure_actual11"              : oet.actual.copy(),
    }

dict_tractors_stock_de["base"]["data"]                  = tractors_stock_data
dict_tractors_stock_de["base"]["color_dict"]            = colors.emobility
dict_tractors_stock_de["base"]["title"]                 = "Vierteljährlicher Bestand - Zugmaschinen (inkl. Sattelzugmaschinen)"
dict_tractors_stock_de["base"]["title_y"]               = 0.98
dict_tractors_stock_de["base"]["barmode"]               = "stack"
dict_tractors_stock_de["base"]["legend_trace_order"]    = "grouped"
dict_tractors_stock_de["base"]["margin"]                = dict(l=0, r=50, t=65, b=0, pad=0)
dict_tractors_stock_de["base"]["x_axis_range"]          = ["2018-04-01","2026-12-31"]
dict_tractors_stock_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }

if "button2030" in dict_tractors_stock_de["base"]["buttons"]:
    del dict_tractors_stock_de["base"]["buttons"]["button2030"]
if "button2045" in dict_tractors_stock_de["base"]["buttons"]:
    del dict_tractors_stock_de["base"]["buttons"]["button2045"]
dict_tractors_stock_de["base"]["buttons"]["custom_button1"] = "Gesamtbestand"
dict_tractors_stock_de["base"]["buttons"]["custom_button2"] = "Anteil alternative Antriebe"
dict_tractors_stock_de["base"]["buttons"]['custom_traces1'] = [True ,True, True, True, True,True, True,True,False, False,False, False]
dict_tractors_stock_de["base"]["buttons"]['custom_traces2'] = [False, False, False, False, False,False, False, False, True, True,  True, True]
dict_tractors_stock_de["base"]["buttons"]["y_max_custom1"]  = 3000000
dict_tractors_stock_de["base"]["buttons"]["y_max_custom2"]  = 0.5
dict_tractors_stock_de["base"]["buttons"]["y_title_1"]      = "Bestand"
dict_tractors_stock_de["base"]["buttons"]["y_title_2"]      = "Anteil elektrische Antriebe in %"
dict_tractors_stock_de["base"]["y_axis_title"]              = dict_tractors_stock_de["base"]["buttons"]["y_title_1"]
dict_tractors_stock_de["base"]["y_axis_range"]              = [0, dict_tractors_stock_de["base"]["buttons"]["y_max_custom1"]]
dict_tractors_stock_de["base"]["buttons"]["position"]       = {
    "x": 0.01,
    "y": 1.10 
}


dict_tractors_stock_de["figure_actual"]["y"]           = "other"      
dict_tractors_stock_de["figure_actual"]["name"]        = "Sonstige"
dict_tractors_stock_de["figure_actual"]["color"]       = "other"
dict_tractors_stock_de["figure_actual"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual"]["group"]       = "Sonstige" 
dict_tractors_stock_de["figure_actual"]["title_group"] = "Sonstige"
dict_tractors_stock_de["figure_actual"]["visible"]     = True

dict_tractors_stock_de["figure_actual1"]["y"]           = "diesel_petrol"      
dict_tractors_stock_de["figure_actual1"]["name"]        = "Diesel & Benzin"
dict_tractors_stock_de["figure_actual1"]["color"]       = "diesel_petrol"
dict_tractors_stock_de["figure_actual1"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual1"]["group"]       = "combustion"
dict_tractors_stock_de["figure_actual1"]["title_group"] = "Verbrennungsmotoren" 
dict_tractors_stock_de["figure_actual1"]["visible"]     = True
dict_tractors_stock_de["figure_actual1"]["legendrank"]  = 12

dict_tractors_stock_de["figure_actual2"]["title_group"] = "Verbrenner" 
dict_tractors_stock_de["figure_actual2"]["y"]           = "gas"      
dict_tractors_stock_de["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_tractors_stock_de["figure_actual2"]["color"]       = "gas"
dict_tractors_stock_de["figure_actual2"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual2"]["group"]       = "combustion"
dict_tractors_stock_de["figure_actual2"]["visible"]     = True
dict_tractors_stock_de["figure_actual2"]["legendrank"]  = 11

dict_tractors_stock_de["figure_actual3"]["y"]           = "hydrogen"      
dict_tractors_stock_de["figure_actual3"]["name"]        = "Wasserstoff"
dict_tractors_stock_de["figure_actual3"]["color"]       = "hydrogen"
dict_tractors_stock_de["figure_actual3"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual3"]["group"]       = "combustion"
dict_tractors_stock_de["figure_actual3"]["visible"]     = True
dict_tractors_stock_de["figure_actual3"]["legendrank"]  = 10

dict_tractors_stock_de["figure_actual4"]["y"]           = "no_plugin_hybrid"      
dict_tractors_stock_de["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_tractors_stock_de["figure_actual4"]["color"]       = "hev"
dict_tractors_stock_de["figure_actual4"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual4"]["group"]       = "hybrid"
dict_tractors_stock_de["figure_actual4"]["title_group"] = "Hybride"
dict_tractors_stock_de["figure_actual4"]["visible"]     = True
dict_tractors_stock_de["figure_actual4"]["legendrank"]  = 9

dict_tractors_stock_de["figure_actual5"]["y"]           = "plug_in_hybrid"      
dict_tractors_stock_de["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_tractors_stock_de["figure_actual5"]["color"]       = "phev"
dict_tractors_stock_de["figure_actual5"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual5"]["group"]       = "hybrid" 
dict_tractors_stock_de["figure_actual5"]["visible"]     = True
dict_tractors_stock_de["figure_actual5"]["legendrank"]  = 8

dict_tractors_stock_de["figure_actual6"]["y"]           = "hydrogen_fuelcell"      
dict_tractors_stock_de["figure_actual6"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_tractors_stock_de["figure_actual6"]["color"]       = "fuel_cell"
dict_tractors_stock_de["figure_actual6"]["group"]       = "electric"
dict_tractors_stock_de["figure_actual6"]["hover"]       = "%{y:.0f}"
dict_tractors_stock_de["figure_actual6"]["visible"]      = True
dict_tractors_stock_de["figure_actual6"]["legendrank"]   = 7

dict_tractors_stock_de["figure_actual7"]["y"]            = "electric"     
dict_tractors_stock_de["figure_actual7"]["name"]         = "Batterieelektrisch"
dict_tractors_stock_de["figure_actual7"]["color"]        = "bev"
dict_tractors_stock_de["figure_actual7"]["hover"]        = "%{y:.0f}"
dict_tractors_stock_de["figure_actual7"]["group"]        = "electric"
dict_tractors_stock_de["figure_actual7"]["title_group"]  = "Reine Elektroantriebe"
dict_tractors_stock_de["figure_actual7"]["visible"]      = True
dict_tractors_stock_de["figure_actual7"]["legendrank"]   = 6

dict_tractors_stock_de["figure_actual8"]["y"]           = "no_plugin_hybrid_share"      
dict_tractors_stock_de["figure_actual8"]["name"]        = "No Plug-in-Hybrid"
dict_tractors_stock_de["figure_actual8"]["color"]       = "hev"
dict_tractors_stock_de["figure_actual8"]["hover"]       = "%{y:.2f}%"
dict_tractors_stock_de["figure_actual8"]["group"]       = "hybrid"
dict_tractors_stock_de["figure_actual8"]["title_group"] = "Hybride - Anteile"
dict_tractors_stock_de["figure_actual8"]["visible"]     = False
dict_tractors_stock_de["figure_actual8"]["legendrank"]  = 5

dict_tractors_stock_de["figure_actual9"]["y"]           = "plug_in_hybrid_share"      
dict_tractors_stock_de["figure_actual9"]["name"]        = "Plug-in-Hybrid"
dict_tractors_stock_de["figure_actual9"]["color"]       = "phev"
dict_tractors_stock_de["figure_actual9"]["hover"]       = "%{y:.2f}%"
dict_tractors_stock_de["figure_actual9"]["group"]       = "hybrid" 
dict_tractors_stock_de["figure_actual9"]["visible"]     = False
dict_tractors_stock_de["figure_actual9"]["legendrank"]  = 4


dict_tractors_stock_de["figure_actual10"]["y"]           = "hydrogen_fuelcell_share"      
dict_tractors_stock_de["figure_actual10"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_tractors_stock_de["figure_actual10"]["color"]       = "fuel_cell"
dict_tractors_stock_de["figure_actual10"]["group"]       = "electric"
dict_tractors_stock_de["figure_actual10"]["hover"]       = "%{y:.2f}%"
dict_tractors_stock_de["figure_actual10"]["visible"]      = False 
dict_tractors_stock_de["figure_actual10"]["legendrank"]   = 3

dict_tractors_stock_de["figure_actual11"]["y"]            = "electric_share"     
dict_tractors_stock_de["figure_actual11"]["name"]         = "Batterieelektrisch"
dict_tractors_stock_de["figure_actual11"]["color"]        = "bev"
dict_tractors_stock_de["figure_actual11"]["hover"]        = "%{y:.2f}%"
dict_tractors_stock_de["figure_actual11"]["group"]        = "electric"
dict_tractors_stock_de["figure_actual11"]["title_group"]  = "Reine Elektroantriebe - Anteile" 
dict_tractors_stock_de["figure_actual11"]["visible"]      = False
dict_tractors_stock_de["figure_actual11"]["legendrank"]   = 1



# English figure

dict_tractors_stock_en = copy.deepcopy(dict_tractors_stock_de)

dict_tractors_stock_en["base"]["title"]                 = "Quarterly Stock - Tractors (including Semitrailers)"
dict_tractors_stock_en["base"]["buttons"]["y_title_1"]  = "Stock"
dict_tractors_stock_en["base"]["buttons"]["y_title_2"]  = "Share electric drive in %"
dict_tractors_stock_en["base"]["y_axis_title"]          = dict_tractors_stock_en["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_tractors_stock_en["figure_actual"]["title_group"] = "Others" 
dict_tractors_stock_en["figure_actual1"]["title_group"]  = "Combustion"
dict_tractors_stock_en["figure_actual4"]["title_group"] = "Hybrid"  
dict_tractors_stock_en["figure_actual7"]["title_group"] = "Fully Electric"
dict_tractors_stock_en["figure_actual9"]["title_group"] = "Hybrid - Shares"
dict_tractors_stock_en["figure_actual11"]["title_group"] ="Fully Electric - Shares"

dict_tractors_stock_en["figure_actual"]["name"]       = "Others"
dict_tractors_stock_en["figure_actual1"]["name"]         = "Diesel & Petrol"
dict_tractors_stock_en["figure_actual2"]["name"]        = "Gas (LPG & CNG)"
dict_tractors_stock_en["figure_actual3"]["name"]        = "Hydrogen"
dict_tractors_stock_en["figure_actual4"]["name"]        = "No Plug-in-Hybrid"
dict_tractors_stock_en["figure_actual5"]["name"]        = "Plug-in-Hybrid"
dict_tractors_stock_en["figure_actual6"]["name"]        = "Fuel cell (hydrogen)"
dict_tractors_stock_en["figure_actual7"]["name"]        = "Electric"
dict_tractors_stock_en["figure_actual8"]["name"]        = "No plug-in-hybrid"
dict_tractors_stock_en["figure_actual9"]["name"]        = "Plug-in-hybrid"
dict_tractors_stock_en["figure_actual10"]["name"]       = "Fuel cell (hydrogen)"
dict_tractors_stock_en["figure_actual11"]["name"]       = "Battery-electric"

dict_tractors_stock_en["legislative_period"]["text"]    = "Traffic Light Coalition"
dict_tractors_stock_en["base"]["buttons"]["custom_button1"] = "Total Stock"
dict_tractors_stock_en["base"]["buttons"]["custom_button2"] = "Share Electric Drive"

# French figure

dict_tractors_stock_fr = copy.deepcopy(dict_tractors_stock_de)

dict_tractors_stock_fr["base"]["title"]                 = "Effectif trimestriel - Tractuers (compris semi-remorques)"
dict_tractors_stock_fr["base"]["buttons"]["y_title_1"]  = "Stock"
dict_tractors_stock_fr["base"]["buttons"]["y_title_2"]  = "Part des entraînements électriques en %"
dict_tractors_stock_fr["base"]["y_axis_title"]          = dict_tractors_stock_fr["base"]["buttons"]["y_title_1"] #setting the y axis title before one of the buttons is clicked
dict_tractors_stock_fr["figure_actual"]["title_group"] = "Autres" 
dict_tractors_stock_fr["figure_actual3"]["title_group"]  = "Combustion"
dict_tractors_stock_fr["figure_actual4"]["title_group"] = "Hybride"  
dict_tractors_stock_fr["figure_actual7"]["title_group"] = "Entraînements purement électriques"
dict_tractors_stock_fr["figure_actual9"]["title_group"] = "Hybride - Proportions" 
dict_tractors_stock_fr["figure_actual11"]["title_group"] = "Entraînements purement électriques - Proportions"

dict_tractors_stock_fr["figure_actual"]["name"]       = "Autres"
dict_tractors_stock_fr["figure_actual1"]["name"]         = "Diesel et essence"
dict_tractors_stock_fr["figure_actual2"]["name"]        = "Gaz (LPG & CNG)"
dict_tractors_stock_fr["figure_actual3"]["name"]        = "Hydrogène"
dict_tractors_stock_fr["figure_actual4"]["name"]        = "Non hybride rechargeable"
dict_tractors_stock_fr["figure_actual5"]["name"]        = "Hybride plug-in"
dict_tractors_stock_fr["figure_actual6"]["name"]        = "Pile à combustible à hydrogène"
dict_tractors_stock_fr["figure_actual7"]["name"]        = "Électrique sur batterie"
dict_tractors_stock_fr["figure_actual8"]["name"]        = "Non hybride rechargeable"
dict_tractors_stock_fr["figure_actual9"]["name"]        = "Hybride plug-in"
dict_tractors_stock_fr["figure_actual10"]["name"]       = "Pile à combustible à hydrogène"
dict_tractors_stock_fr["figure_actual11"]["name"]       = "Électrique sur batterie"

dict_tractors_stock_fr["legislative_period"]["text"]    = "coalition en feu tricolore"
dict_tractors_stock_fr["base"]["buttons"]["custom_button1"] = "Total des effectifs"
dict_tractors_stock_fr["base"]["buttons"]["custom_button2"] = "Part des moteurs électriques"

fig_tractors_stock_de = oet.create_fig(dict_tractors_stock_de)
fig_tractors_stock_en = oet.create_fig(dict_tractors_stock_en)
fig_tractors_stock_fr = oet.create_fig(dict_tractors_stock_fr)

fig_tractors_stock_de.write_html("docs/germany/figures/tractors_stock.de.html")
fig_tractors_stock_en.write_html("docs/germany/figures/tractors_stock.en.html")
fig_tractors_stock_fr.write_html("docs/germany/figures/tractors_stock.fr.html")

###############################################################################
# Heavy Vehicles - Admissions
###############################################################################

#%% Trucks general new admissions by default

trucks_gen_adm_data = oet.prepare_data(mobility_url + "trucks_gen_adm.csv","months")

trucks_gen_adm_base = copy.deepcopy(base_default)

dict_trucks_gen_adm_de = {
    "base"                         : trucks_gen_adm_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
}

#BasisGrafik
dict_trucks_gen_adm_de["base"]["data"]                  = trucks_gen_adm_data
dict_trucks_gen_adm_de["base"]["color_dict"]            = colors.emobility
dict_trucks_gen_adm_de["base"]["title"]                 = "Anteil alternativer Antriebe an den monatlichen Neuzulassungen - Lastkraftwagen"
dict_trucks_gen_adm_de["base"]["x_axis_range"]          = ["2020-12-01","2026-12-31"]
dict_trucks_gen_adm_de["base"]["y_axis_title"]          = "Prozent"
dict_trucks_gen_adm_de["base"]["y_axis_range"]          = [0,25]
dict_trucks_gen_adm_de["base"]["barmode"]               = "stack"
dict_trucks_gen_adm_de["base"]["legend_trace_order"]    = "grouped"
dict_trucks_gen_adm_de["base"]["margin"]                = dict(l=0, r=0, t=30, b=0, pad=0)
dict_trucks_gen_adm_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }

#Buttons raus
del dict_trucks_gen_adm_de["base"]["buttons"]

#Addfigures

dict_trucks_gen_adm_de["figure_actual"]["y"]           = "gas_share"      
dict_trucks_gen_adm_de["figure_actual"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_gen_adm_de["figure_actual"]["color"]       = "gas"
dict_trucks_gen_adm_de["figure_actual"]["hover"]       = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual"]["group"]       = "combustion"
dict_trucks_gen_adm_de["figure_actual"]["title_group"] = "Verbrenner" 
dict_trucks_gen_adm_de["figure_actual"]["visible"]     = 'legendonly' 
dict_trucks_gen_adm_de["figure_actual"]["legendrank"]  = 6

dict_trucks_gen_adm_de["figure_actual2"]["y"]           = "hydrogen_share"      
dict_trucks_gen_adm_de["figure_actual2"]["name"]        = "Wasserstoff"
dict_trucks_gen_adm_de["figure_actual2"]["color"]       = "hydrogen"
dict_trucks_gen_adm_de["figure_actual2"]["hover"]       = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual2"]["group"]       = "combustion"
dict_trucks_gen_adm_de["figure_actual2"]["legendrank"]  = 5

dict_trucks_gen_adm_de["figure_actual3"]["y"]           = "no_plugin_hybrid_share"      
dict_trucks_gen_adm_de["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_gen_adm_de["figure_actual3"]["color"]       = "hev"
dict_trucks_gen_adm_de["figure_actual3"]["hover"]       = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual3"]["group"]       = "hybrid"
dict_trucks_gen_adm_de["figure_actual3"]["title_group"] = "Hybride" 
dict_trucks_gen_adm_de["figure_actual3"]["legendrank"]  = 4

dict_trucks_gen_adm_de["figure_actual4"]["y"]           = "plug_in_hybrid_share"      
dict_trucks_gen_adm_de["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_trucks_gen_adm_de["figure_actual4"]["color"]       = "phev"
dict_trucks_gen_adm_de["figure_actual4"]["hover"]       = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual4"]["group"]       = "hybrid"
dict_trucks_gen_adm_de["figure_actual4"]["legendrank"]  = 3

dict_trucks_gen_adm_de["figure_actual5"]["y"]           = "hydrogen_fuelcell_share"      
dict_trucks_gen_adm_de["figure_actual5"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_gen_adm_de["figure_actual5"]["color"]       = "fuel_cell"
dict_trucks_gen_adm_de["figure_actual5"]["hover"]       = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual5"]["group"]       = "electric"
dict_trucks_gen_adm_de["figure_actual5"]["legendrank"]   = 2

dict_trucks_gen_adm_de["figure_actual6"]["y"]            = "electric_share"     
dict_trucks_gen_adm_de["figure_actual6"]["name"]         = "Batterieelektrisch"
dict_trucks_gen_adm_de["figure_actual6"]["color"]        = "bev"
dict_trucks_gen_adm_de["figure_actual6"]["hover"]        = "%{y:.2f}%"
dict_trucks_gen_adm_de["figure_actual6"]["group"]        = "electric"
dict_trucks_gen_adm_de["figure_actual6"]["title_group"]  = "Reine Elektroantriebe"
dict_trucks_gen_adm_de["figure_actual6"]["legendrank"]   = 1

# English figure

dict_trucks_gen_adm_en = copy.deepcopy(dict_trucks_gen_adm_de)

dict_trucks_gen_adm_en["base"]["title"]                 = "Monthly share in new registrations - Trucks"
dict_trucks_gen_adm_en["base"]["y_axis_title"]          = "Percent"
dict_trucks_gen_adm_en["figure_actual"]["title_group"]  = "Combustion"
dict_trucks_gen_adm_en["figure_actual3"]["title_group"] = "Hybrid"  
dict_trucks_gen_adm_en["figure_actual6"]["title_group"] = "Electric"
dict_trucks_gen_adm_en["figure_actual"]["name"]         = "Gas (LPG & CNG)"
dict_trucks_gen_adm_en["figure_actual2"]["name"]        = "Hydrogen"
dict_trucks_gen_adm_en["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_gen_adm_en["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_trucks_gen_adm_en["figure_actual5"]["name"]        = "Fuel cell (hydrogen)"
dict_trucks_gen_adm_en["figure_actual6"]["name"]        = "Battery-electric"
dict_trucks_gen_adm_en["legislative_period"]["text"]    = "Traffic Light Coalition"

# French figure

dict_trucks_gen_adm_fr = copy.deepcopy(dict_trucks_gen_adm_de)

dict_trucks_gen_adm_fr["base"]["title"]                 = "Part mensuelle des nouveaux enregistrements - Camion"
dict_trucks_gen_adm_fr["base"]["y_axis_title"]          = "Pourcentage"
dict_trucks_gen_adm_fr["figure_actual"]["title_group"]  = "Combustion"       
dict_trucks_gen_adm_fr["figure_actual3"]["title_group"] = "Hybride"  
dict_trucks_gen_adm_fr["figure_actual6"]["title_group"] = "Électrique"
dict_trucks_gen_adm_fr["figure_actual"]["name"]         = "Gaz (LPG & CNG)"
dict_trucks_gen_adm_fr["figure_actual2"]["name"]        = "Hydrogène" 
dict_trucks_gen_adm_fr["figure_actual3"]["name"]        = "Non Hybrides plug-in"
dict_trucks_gen_adm_fr["figure_actual4"]["name"]        = "Hybrides plug-in"
dict_trucks_gen_adm_fr["figure_actual5"]["name"]        = "Pile à combustible (hydrogène)"
dict_trucks_gen_adm_fr["figure_actual6"]["name"]        = "Électriques à batterie"
dict_trucks_gen_adm_fr["legislative_period"]["text"]    = "coalition en feu tricolore"

# Create and save figures

fig_trucks_gen_adm_de = oet.create_fig(dict_trucks_gen_adm_de)
fig_trucks_gen_adm_en = oet.create_fig(dict_trucks_gen_adm_en)
fig_trucks_gen_adm_fr = oet.create_fig(dict_trucks_gen_adm_fr)

fig_trucks_gen_adm_de.write_html("docs/germany/figures/trucks_gen_adm.de.html", include_plotlyjs="directory")
fig_trucks_gen_adm_en.write_html("docs/germany/figures/trucks_gen_adm.en.html", include_plotlyjs="directory")
fig_trucks_gen_adm_fr.write_html("docs/germany/figures/trucks_gen_adm.fr.html", include_plotlyjs="directory")

#%% Trucks - semitrailers, new admissions by default

trucks_adm_data = oet.prepare_data(mobility_url + "trucks_semitrailers_adm.csv","months")

trucks_adm_base = copy.deepcopy(base_default)

dict_trucks_adm_de = {
    "base"                         : trucks_adm_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
}
   
#BasisGrafik
dict_trucks_adm_de["base"]["data"]                  = trucks_adm_data
dict_trucks_adm_de["base"]["color_dict"]            = colors.emobility
dict_trucks_adm_de["base"]["title"]                 = "Anteil an den monatlichen Neuzulassungen - Sattelzugmaschinen"
dict_trucks_adm_de["base"]["x_axis_range"]          = ["2020-12-01","2026-12-31"]
dict_trucks_adm_de["base"]["y_axis_title"]          = "Prozent"
dict_trucks_adm_de["base"]["y_axis_range"]          = [0,8]
dict_trucks_adm_de["base"]["barmode"]               = "stack"
dict_trucks_adm_de["base"]["legend_trace_order"]    = "grouped"
dict_trucks_adm_de["base"]["margin"]                = dict(l=0, r=0, t=30, b=0, pad=0)
dict_trucks_adm_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }


#Buttons raus
del dict_trucks_adm_de["base"]["buttons"]

#Addfigures    

dict_trucks_adm_de["figure_actual"]["y"]           = "gas_share"      
dict_trucks_adm_de["figure_actual"]["name"]        = "Gas (LPG & CNG)"
dict_trucks_adm_de["figure_actual"]["color"]       = "gas"
dict_trucks_adm_de["figure_actual"]["hover"]       = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual"]["group"]       = "combustion"
dict_trucks_adm_de["figure_actual"]["title_group"] = "Verbrenner"
dict_trucks_adm_de["figure_actual"]["visible"]     = 'legendonly'  
dict_trucks_adm_de["figure_actual"]["legendrank"]  = 6

dict_trucks_adm_de["figure_actual2"]["y"]           = "hydrogen_share"      
dict_trucks_adm_de["figure_actual2"]["name"]        = "Wasserstoff"
dict_trucks_adm_de["figure_actual2"]["color"]       = "hydrogen"
dict_trucks_adm_de["figure_actual2"]["hover"]       = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual2"]["group"]       = "combustion"
dict_trucks_adm_de["figure_actual2"]["legendrank"]  = 5

dict_trucks_adm_de["figure_actual3"]["y"]           = "no_plugin_hybrid_share"      
dict_trucks_adm_de["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_adm_de["figure_actual3"]["color"]       = "hev"
dict_trucks_adm_de["figure_actual3"]["hover"]       = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual3"]["group"]       = "hybrid"
dict_trucks_adm_de["figure_actual3"]["title_group"] = "Hybride"
dict_trucks_adm_de["figure_actual3"]["legendrank"]  = 4

dict_trucks_adm_de["figure_actual4"]["y"]           = "plug_in_hybrid_share"      
dict_trucks_adm_de["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_trucks_adm_de["figure_actual4"]["color"]       = "phev"
dict_trucks_adm_de["figure_actual4"]["hover"]       = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual4"]["group"]       = "hybrid"
dict_trucks_adm_de["figure_actual4"]["legendrank"]  = 3

dict_trucks_adm_de["figure_actual5"]["y"]           = "hydrogen_fuelcell_share"      
dict_trucks_adm_de["figure_actual5"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_trucks_adm_de["figure_actual5"]["color"]       = "fuel_cell"
dict_trucks_adm_de["figure_actual5"]["hover"]       = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual5"]["group"]       = "electric"
dict_trucks_adm_de["figure_actual5"]["legendrank"]   = 2

dict_trucks_adm_de["figure_actual6"]["y"]            = "electric_share"     
dict_trucks_adm_de["figure_actual6"]["name"]         = "Batterieelektrisch"
dict_trucks_adm_de["figure_actual6"]["color"]        = "bev"
dict_trucks_adm_de["figure_actual6"]["hover"]        = "%{y:.2f}%"
dict_trucks_adm_de["figure_actual6"]["group"]        = "electric"
dict_trucks_adm_de["figure_actual6"]["title_group"]  = "Reine Elektroantriebe"
dict_trucks_adm_de["figure_actual6"]["legendrank"]   = 1

# English figure

dict_trucks_adm_en = copy.deepcopy(dict_trucks_adm_de)

dict_trucks_adm_en["base"]["title"]                 = "Monthly share in new registrations - Semitrailers"
dict_trucks_adm_en["base"]["y_axis_title"]          = "Percent"
dict_trucks_adm_en["figure_actual"]["title_group"]  = "Combustion"       
dict_trucks_adm_en["figure_actual3"]["title_group"] = "Hybrid"  
dict_trucks_adm_en["figure_actual6"]["title_group"] = "Electric"
dict_trucks_adm_en["figure_actual"]["name"]         = "Gas (LPG & CNG)"
dict_trucks_adm_en["figure_actual2"]["name"]        = "Hydrogen"
dict_trucks_adm_en["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_trucks_adm_en["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_trucks_adm_en["figure_actual5"]["name"]        = "Fuel cell (hydrogen)"
dict_trucks_adm_en["figure_actual6"]["name"]        = "Battery-electric"
dict_trucks_adm_en["legislative_period"]["text"]    = "Traffic Light Coalition"

# French figure

dict_trucks_adm_fr = copy.deepcopy(dict_trucks_adm_de)

dict_trucks_adm_fr["base"]["title"]                 = "Part mensuelle des nouveaux enregistrements - Semi-remorque"
dict_trucks_adm_fr["base"]["y_axis_title"]          = "Pourcentage"
dict_trucks_adm_fr["figure_actual"]["title_group"]  = "Combustion"       
dict_trucks_adm_fr["figure_actual3"]["title_group"] = "Hybride"  
dict_trucks_adm_fr["figure_actual6"]["title_group"] = "Électrique"
dict_trucks_adm_fr["figure_actual"]["name"]         = "Gaz (LPG & CNG)"
dict_trucks_adm_fr["figure_actual2"]["name"]        = "Hydrogène"
dict_trucks_adm_fr["figure_actual3"]["name"]        = "Non Hybrides plug-in"
dict_trucks_adm_fr["figure_actual4"]["name"]        = "Hybrides plug-in"
dict_trucks_adm_fr["figure_actual5"]["name"]        = "Pile à combustible (hydrogène)"
dict_trucks_adm_fr["figure_actual6"]["name"]        = "Électriques à batterie"
dict_trucks_adm_fr["legislative_period"]["text"]    = "coalition en feu tricolore"

# Create and save figures

fig_trucks_adm_de = oet.create_fig(dict_trucks_adm_de)
fig_trucks_adm_en = oet.create_fig(dict_trucks_adm_en)
fig_trucks_adm_fr = oet.create_fig(dict_trucks_adm_fr)

fig_trucks_adm_de.write_html("docs/germany/figures/trucks_adm.de.html", include_plotlyjs="directory")
fig_trucks_adm_en.write_html("docs/germany/figures/trucks_adm.en.html", include_plotlyjs="directory")
fig_trucks_adm_fr.write_html("docs/germany/figures/trucks_adm.fr.html", include_plotlyjs="directory")

#%% Busses new admissions by default

busses_adm_data = oet.prepare_data(mobility_url + "busses_adm.csv","months")

buses_adm_base = copy.deepcopy(base_default)

dict_busses_adm_de = {
    "base"                         : buses_adm_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
}

#BasisGrafik
dict_busses_adm_de["base"]["data"]                  = busses_adm_data
dict_busses_adm_de["base"]["color_dict"]            = colors.emobility
dict_busses_adm_de["base"]["title"]                 = "Anteil an den monatlichen Neuzulassungen - Kraftomnibusse"
dict_busses_adm_de["base"]["x_axis_range"]          = ["2020-12-01","2026-12-31"]
dict_busses_adm_de["base"]["y_axis_title"]          = "Prozent"
dict_busses_adm_de["base"]["y_axis_range"]          = [0,80]
dict_busses_adm_de["base"]["barmode"]               = "stack"
dict_busses_adm_de["base"]["legend_trace_order"]    = "grouped"
dict_busses_adm_de["base"]["margin"]                = dict(l=0, r=0, t=30, b=0, pad=0)
dict_busses_adm_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }


#Buttons raus
del dict_busses_adm_de["base"]["buttons"]

#Addfigures    


dict_busses_adm_de["figure_actual"]["y"]           = "gas_share"      
dict_busses_adm_de["figure_actual"]["name"]        = "Gas (LPG & CNG)"
dict_busses_adm_de["figure_actual"]["color"]       = "gas"
dict_busses_adm_de["figure_actual"]["hover"]       = "%{y:.2f}%"
dict_busses_adm_de["figure_actual"]["group"]       = "combustion"
dict_busses_adm_de["figure_actual"]["title_group"] = "Verbrenner" 
dict_busses_adm_de["figure_actual"]["visible"]     = 'legendonly' 
dict_busses_adm_de["figure_actual"]["legendrank"]  = 6

dict_busses_adm_de["figure_actual2"]["y"]           = "hydrogen_share"      
dict_busses_adm_de["figure_actual2"]["name"]        = "Wasserstoff"
dict_busses_adm_de["figure_actual2"]["color"]       = "hydrogen"
dict_busses_adm_de["figure_actual2"]["hover"]       = "%{y:.2f}%"
dict_busses_adm_de["figure_actual2"]["group"]       = "combustion"
dict_busses_adm_de["figure_actual2"]["legendrank"]  = 5

dict_busses_adm_de["figure_actual3"]["y"]           = "no_plugin_hybrid_share"      
dict_busses_adm_de["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_busses_adm_de["figure_actual3"]["color"]       = "hev"
dict_busses_adm_de["figure_actual3"]["hover"]       = "%{y:.2f}%"
dict_busses_adm_de["figure_actual3"]["group"]       = "hybrid"
dict_busses_adm_de["figure_actual3"]["title_group"] = "Hybride" 
dict_busses_adm_de["figure_actual3"]["legendrank"]  = 4

dict_busses_adm_de["figure_actual4"]["y"]           = "plug_in_hybrid_share"      
dict_busses_adm_de["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_busses_adm_de["figure_actual4"]["color"]       = "phev"
dict_busses_adm_de["figure_actual4"]["hover"]       = "%{y:.2f}%"
dict_busses_adm_de["figure_actual4"]["group"]       = "hybrid"
dict_busses_adm_de["figure_actual4"]["legendrank"]  = 3

dict_busses_adm_de["figure_actual5"]["y"]           = "hydrogen_fuelcell_share"      
dict_busses_adm_de["figure_actual5"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_busses_adm_de["figure_actual5"]["color"]       = "fuel_cell"
dict_busses_adm_de["figure_actual5"]["hover"]       = "%{y:.2f}%"
dict_busses_adm_de["figure_actual5"]["group"]       = "electric"
dict_busses_adm_de["figure_actual5"]["legendrank"]  = 2

dict_busses_adm_de["figure_actual6"]["y"]            = "electric_share"     
dict_busses_adm_de["figure_actual6"]["name"]         = "Batterieelektrisch"
dict_busses_adm_de["figure_actual6"]["color"]        = "bev"
dict_busses_adm_de["figure_actual6"]["hover"]        = "%{y:.2f}%"
dict_busses_adm_de["figure_actual6"]["group"]        = "electric"
dict_busses_adm_de["figure_actual6"]["title_group"]  = "Reine Elektroantriebe"
dict_busses_adm_de["figure_actual6"]["legendrank"]   = 1



# English figure
dict_busses_adm_en = copy.deepcopy(dict_busses_adm_de)

dict_busses_adm_en["base"]["title"]                 = "Monthly share in new registrations - Busses"
dict_busses_adm_en["base"]["y_axis_title"]          = "Percent"
dict_busses_adm_en["figure_actual"]["title_group"] = "Combustion"
dict_busses_adm_en["figure_actual3"]["title_group"] = "Hybrid"  
dict_busses_adm_en["figure_actual6"]["title_group"]  = "Electric"
dict_busses_adm_en["figure_actual"]["name"]        = "Gas (LPG & CNG)"
dict_busses_adm_en["figure_actual2"]["name"]        = "Hydrogen"       
dict_busses_adm_en["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_busses_adm_en["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_busses_adm_en["figure_actual5"]["name"]        = "Fuel cell (hydrogen)"
dict_busses_adm_en["figure_actual6"]["name"]         = "Battery-electric"
dict_busses_adm_en["legislative_period"]["text"]    = "Traffic Light Coalition"

# French figure
dict_busses_adm_fr = copy.deepcopy(dict_busses_adm_de)

dict_busses_adm_fr["base"]["title"]                 = "Part mensuelle des nouveaux enregistrements - omnibus"
dict_busses_adm_fr["base"]["y_axis_title"]          = "Pourcentage"
dict_busses_adm_fr["figure_actual"]["title_group"]  = "Combustion"
dict_busses_adm_fr["figure_actual3"]["title_group"] = "Hybride"  
dict_busses_adm_fr["figure_actual6"]["title_group"] = "Électrique"    
dict_busses_adm_fr["figure_actual"]["name"]        = "Gaz (LPG & CNG)"
dict_busses_adm_fr["figure_actual2"]["name"]        = "Hydrogène"
dict_busses_adm_fr["figure_actual3"]["name"]        = "Non Hybrides plug-in"
dict_busses_adm_fr["figure_actual4"]["name"]        = "Hybrides plug-in"
dict_busses_adm_fr["figure_actual5"]["name"]        = "Pile à combustible (hydrogène)"
dict_busses_adm_fr["figure_actual6"]["name"]         = "Électriques à batterie"
dict_busses_adm_fr["legislative_period"]["text"]    = "coalition en feu tricolore"

# Create and save figures

fig_busses_adm_de = oet.create_fig(dict_busses_adm_de)
fig_busses_adm_en = oet.create_fig(dict_busses_adm_en)
fig_busses_adm_fr = oet.create_fig(dict_busses_adm_fr)

fig_busses_adm_de.write_html("docs/germany/figures/busses_adm.de.html", include_plotlyjs="directory")
fig_busses_adm_en.write_html("docs/germany/figures/busses_adm.en.html", include_plotlyjs="directory")
fig_busses_adm_fr.write_html("docs/germany/figures/busses_adm.fr.html", include_plotlyjs="directory")

#%% Tractors new admissions by default

tractors_adm_data = oet.prepare_data(mobility_url + "tractors_tot_adm.csv","months")

tractors_adm_base = copy.deepcopy(base_default)

dict_tractors_adm_de = {
    "base"                         : tractors_adm_base,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_actual3"               : oet.actual.copy(),
    "figure_actual4"               : oet.actual.copy(),
    "figure_actual5"               : oet.actual.copy(),
    "figure_actual6"               : oet.actual.copy(),
}


dict_tractors_adm_de["base"]["data"]                  = tractors_adm_data
dict_tractors_adm_de["base"]["color_dict"]            = colors.emobility
dict_tractors_adm_de["base"]["title"]                 = "Anteil an den monatlichen Neuzulassungen - Zugmaschinen inkl. Sattelzugmaschinen"
dict_tractors_adm_de["base"]["x_axis_range"]          = ["2020-12-01","2026-12-31"]
dict_tractors_adm_de["base"]["y_axis_title"]          = "Prozent"
dict_tractors_adm_de["base"]["y_axis_range"]          = [0,5]
dict_tractors_adm_de["base"]["barmode"]               = "stack"
dict_tractors_adm_de["base"]["legend_trace_order"]    = "grouped"
dict_tractors_adm_de["base"]["margin"]                = dict(l=0, r=0, t=30, b=0, pad=0)
dict_tractors_adm_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 1.05,
            "y"              : 1.0,
            "yanchor"        : "top",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},  
            "groupclick"     : "toggleitem"
            }

#Buttons raus
del dict_tractors_adm_de["base"]["buttons"]

#Addfigures
dict_tractors_adm_de["figure_actual"]["y"]           = "gas_share"      
dict_tractors_adm_de["figure_actual"]["name"]        = "Gas (LPG & CNG)"
dict_tractors_adm_de["figure_actual"]["color"]       = "gas"
dict_tractors_adm_de["figure_actual"]["hover"]       = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual"]["group"]       = "combustion"
dict_tractors_adm_de["figure_actual"]["title_group"] = "Verbrenner" 
dict_tractors_adm_de["figure_actual"]["visible"]     = 'legendonly' 
dict_tractors_adm_de["figure_actual"]["legendrank"]  = 6

dict_tractors_adm_de["figure_actual2"]["y"]           = "hydrogen_share"      
dict_tractors_adm_de["figure_actual2"]["name"]        = "Wasserstoff"
dict_tractors_adm_de["figure_actual2"]["color"]       = "hydrogen"
dict_tractors_adm_de["figure_actual2"]["hover"]       = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual2"]["group"]       = "combustion"
dict_tractors_adm_de["figure_actual2"]["legendrank"]  = 5

dict_tractors_adm_de["figure_actual3"]["y"]           = "no_plugin_hybrid_share"      
dict_tractors_adm_de["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_tractors_adm_de["figure_actual3"]["color"]       = "hev"
dict_tractors_adm_de["figure_actual3"]["hover"]       = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual3"]["group"]       = "hybrid"
dict_tractors_adm_de["figure_actual3"]["title_group"] = "Hybride" 
dict_tractors_adm_de["figure_actual3"]["legendrank"]  = 4

dict_tractors_adm_de["figure_actual4"]["y"]           = "plug_in_hybrid_share"      
dict_tractors_adm_de["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_tractors_adm_de["figure_actual4"]["color"]       = "phev"
dict_tractors_adm_de["figure_actual4"]["hover"]       = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual4"]["group"]       = "hybrid"
dict_tractors_adm_de["figure_actual4"]["legendrank"]  = 3

dict_tractors_adm_de["figure_actual5"]["y"]           = "hydrogen_fuelcell_share"      
dict_tractors_adm_de["figure_actual5"]["name"]        = "Wasserstoff-Brennstoffzelle"
dict_tractors_adm_de["figure_actual5"]["color"]       = "fuel_cell"
dict_tractors_adm_de["figure_actual5"]["hover"]       = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual5"]["group"]       = "electric"
dict_tractors_adm_de["figure_actual5"]["legendrank"]  = 2

dict_tractors_adm_de["figure_actual6"]["y"]            = "electric_share"     
dict_tractors_adm_de["figure_actual6"]["name"]         = "Batterieelektrisch"
dict_tractors_adm_de["figure_actual6"]["color"]        = "bev"
dict_tractors_adm_de["figure_actual6"]["hover"]        = "%{y:.2f}%"
dict_tractors_adm_de["figure_actual6"]["group"]        = "electric"
dict_tractors_adm_de["figure_actual6"]["title_group"]  = "reine Elektroantriebe"
dict_tractors_adm_de["figure_actual6"]["legendrank"]   = 1




# English figure
dict_tractors_adm_en = copy.deepcopy(dict_tractors_adm_de)

dict_tractors_adm_en["base"]["title"]                 = "Monthly share in new registrations - Tractors"
dict_tractors_adm_en["base"]["y_axis_title"]          = "Percent"
dict_tractors_adm_en["figure_actual"]["title_group"]  = "Combustion"       
dict_tractors_adm_en["figure_actual3"]["title_group"] = "Hybrid"  
dict_tractors_adm_en["figure_actual6"]["title_group"] = "Electric"
dict_tractors_adm_en["figure_actual"]["name"]         = "Gas (LPG & CNG)"
dict_tractors_adm_en["figure_actual2"]["name"]        = "Hydrogen"
dict_tractors_adm_en["figure_actual3"]["name"]        = "No Plug-in-Hybrid"
dict_tractors_adm_en["figure_actual4"]["name"]        = "Plug-in-Hybrid"
dict_tractors_adm_en["figure_actual5"]["name"]        = "Fuel cell (hydrogen)"
dict_tractors_adm_en["figure_actual6"]["name"]        = "Battery-electric"
dict_tractors_adm_en["legislative_period"]["text"]    = "Traffic Light Coalition"

# French figure

dict_tractors_adm_fr = copy.deepcopy(dict_tractors_adm_de)

dict_tractors_adm_fr["base"]["title"]                 = "Part mensuelle des nouveaux enregistrements - Tracteurs"
dict_tractors_adm_fr["base"]["y_axis_title"]          = "Pourcentage"
dict_tractors_adm_fr["figure_actual"]["title_group"]  = "Combustion"
dict_tractors_adm_fr["figure_actual3"]["title_group"] = "Hybride"  
dict_tractors_adm_fr["figure_actual6"]["title_group"] = "Électrique"
dict_tractors_adm_fr["figure_actual"]["name"]         = "Gaz (LPG & CNG)"
dict_tractors_adm_fr["figure_actual2"]["name"]        = "Hydrogène"
dict_tractors_adm_fr["figure_actual3"]["name"]        = "Non Hybrides plug-in"
dict_tractors_adm_fr["figure_actual4"]["name"]        = "Hybrides plug-in"
dict_tractors_adm_fr["figure_actual5"]["name"]        = "Pile à combustible (hydrogène)"
dict_tractors_adm_fr["figure_actual6"]["name"]        = "Électriques à batterie"
dict_tractors_adm_fr["legislative_period"]["text"]    = "coalition en feu tricolore"

# Create and save figures

fig_tractors_adm_de = oet.create_fig(dict_tractors_adm_de)
fig_tractors_adm_en = oet.create_fig(dict_tractors_adm_en)
fig_tractors_adm_fr = oet.create_fig(dict_tractors_adm_fr)

fig_tractors_adm_de.write_html("docs/germany/figures/tractors_adm.de.html", include_plotlyjs="directory")
fig_tractors_adm_en.write_html("docs/germany/figures/tractors_adm.en.html", include_plotlyjs="directory")
fig_tractors_adm_fr.write_html("docs/germany/figures/tractors_adm.fr.html", include_plotlyjs="directory")

##############################################################################
# Charging infrastructure
##############################################################################

#%% charging stations ##############################################################################

cs_data           = oet.prepare_data(mobility_url +  "cs_quarterly.csv","quarters")
cs_data['actual'] = cs_data['actual_normal'] + cs_data['actual_fast']
cs_data = oet.calculate_trend(cs_data,"actual","4-quarters")

base_cs = copy.deepcopy(base_default)

del base_cs["buttons"]

dict_cs_de = {
    "base"                         : base_cs,
    "legislative_period"           : oet.legislative_period.copy(),
    "figure_actual"                : oet.actual.copy(),
    "figure_actual2"               : oet.actual.copy(),
    "figure_trend"                 : oet.trend.copy(),
    "figure_goal"                  : oet.goal.copy(),
    "figure_goal_log"              : oet.goal_lin.copy(),
}

dict_cs_de["base"]["data"]              = cs_data
dict_cs_de["base"]["title"]             = "Öffentliche Ladepunkte" 
dict_cs_de["base"]["y_axis_title"]      = "Anzahl"
dict_cs_de["base"]["y_axis_range"]      = [0,1200000]
dict_cs_de["base"]["margin"]            = dict(l=0, r=0, t=40, b=0, pad=0)
dict_cs_de["base"]["barmode"]           = "stack"

dict_cs_de["figure_actual"]["name"]    = "Normalladepunkte"
dict_cs_de["figure_actual"]["hover"]   = "%{y:.0f}"
dict_cs_de["figure_actual"]["y"]       = "actual_normal"

dict_cs_de["figure_actual2"]["name"]    = "Schnellladepunkte"
dict_cs_de["figure_actual2"]["hover"]   = "%{y:.0f}"
dict_cs_de["figure_actual2"]["y"]       = "actual_fast"
dict_cs_de["figure_actual2"]["color"]   = "second"

dict_cs_de["figure_goal_log"]["name"]  = "Logistischer Verlauf"
dict_cs_de["figure_goal_log"]["y"]     = "logistic_plan"
dict_cs_de["figure_goal_log"]["hover"] = "%{y:.3r}"

dict_cs_de["figure_goal"]["hover"]     = "%{y:.3r}"
dict_cs_de["figure_trend"]["hover"]    = "%{y:.3r}"

# English figure

dict_cs_en = copy.deepcopy(dict_cs_de)

dict_cs_en["base"]["title"]                         = "Public charging points" 
dict_cs_en["base"]["y_axis_title"]                  = "Units"
dict_cs_en["figure_actual"]["name"]                 = "Normal charging points"
dict_cs_en["figure_actual2"]["name"]                = "Fast charging points"
dict_cs_en["figure_trend"]["name"]                  = "12-month-trend"
dict_cs_en["figure_goal"]["title_group"]            = "<i>Government goals</i>"
dict_cs_en["figure_goal"]["name"]                   = "Target of the coalition"
dict_cs_en["figure_goal_log"]["name"]               = "Logistic growth"
dict_cs_en["legislative_period"]["text"]            = "Traffic Light Coalition"

# French figure

dict_cs_fr = copy.deepcopy(dict_cs_de)
dict_cs_fr["base"]["title"]                         = "Points de charge publics"
dict_cs_fr["base"]["y_axis_title"]                  = "Unités"
dict_cs_fr["figure_actual"]["name"]                 = "Points de charge normaux"
dict_cs_fr["figure_actual2"]["name"]                = "Points de charge rapide"
dict_cs_fr["figure_trend"]["name"]                  = "12 dernier mois"
dict_cs_fr["figure_goal"]["title_group"]            = "<i>Réglementation</i>"
dict_cs_fr["figure_goal"]["name"]                   = "Objectif de la coalition"
dict_cs_fr["figure_goal_log"]["name"]               = "Progression logistique"
dict_cs_fr["legislative_period"]["text"]            = "coalition en feu tricolore"

# Create and save figures

fig_cs_de = oet.create_fig(dict_cs_de)
fig_cs_en = oet.create_fig(dict_cs_en)
fig_cs_fr = oet.create_fig(dict_cs_fr)

fig_cs_de.write_html("docs/germany/figures/cs.de.html", include_plotlyjs="directory")
fig_cs_en.write_html("docs/germany/figures/cs.en.html", include_plotlyjs="directory")
fig_cs_fr.write_html("docs/germany/figures/cs.fr.html", include_plotlyjs="directory")

#%% Charging Capacity Figure -----------------------------------------------------------------------

cs_data           = pd.read_csv(mobility_url + "cs_quarterly.csv", sep = ",")
cs_data['quarter'] = cs_data['quarter'].astype(str)
cs_data['date']   = pd.to_datetime(cs_data['date'], format='%d.%m.%Y')
cs_data['actual'] = cs_data['actual_normal'] + cs_data['actual_fast']

cs_data['X']          = cs_data.index

# trend 2018-2021

# Total Capacity

cs_reg1_data = cs_data[['date','X','charging_cap_total']].dropna()
cs_reg1_data = cs_reg1_data[(cs_reg1_data['date'] >= '2017-01') &
                            (cs_reg1_data['date'] <= '2021-09')]
cs_reg1_x    = np.array(cs_reg1_data['X']).reshape((-1, 1))
cs_reg1      = LinearRegression().fit(cs_reg1_x, cs_reg1_data['charging_cap_total'])

predict1_cs = cs_data[['date','X','charging_cap_total']]
predict1_cs = predict1_cs[(predict1_cs['date'] >= '2021-10')].reset_index()
predict1_cs['index'] = predict1_cs.index
predict1_cs['trend_since_2017'] = predict1_cs.loc[0,'charging_cap_total'] + predict1_cs['index'] * cs_reg1.coef_
predict1_cs = predict1_cs[['date','trend_since_2017']]

# Average Capacity
cs_reg1_data_avg = cs_data[['date','X','charging_cap_avg']].dropna()
cs_reg1_data_avg = cs_reg1_data_avg[(cs_reg1_data_avg['date'] >= '2017-01') &
                            (cs_reg1_data_avg['date'] <= '2021-09')]
cs_reg1_x_avg    = np.array(cs_reg1_data_avg['X']).reshape((-1, 1))
cs_reg1_avg      = LinearRegression().fit(cs_reg1_x_avg, cs_reg1_data_avg['charging_cap_avg'])

predict1_cs_avg = cs_data[['date','X','charging_cap_avg']]
predict1_cs_avg = predict1_cs_avg[(predict1_cs_avg['date'] >= '2021-10')].reset_index()
predict1_cs_avg['index'] = predict1_cs_avg.index
predict1_cs_avg['trend_since_2017_avg'] = predict1_cs_avg.loc[0,'charging_cap_avg'] + predict1_cs_avg['index'] * cs_reg1_avg.coef_
predict1_cs_avg = predict1_cs_avg[['date','trend_since_2017_avg']]

# dynamic trend last 12 months

# Total Capacity

cs_reg2_data = cs_data[['date','charging_cap_total','X']].dropna()
cs_reg2_data = cs_reg2_data.tail(4)
cs_reg2_x    = np.array(cs_reg2_data['X']).reshape((-1, 1))
cs_reg2      = LinearRegression().fit(cs_reg2_x, cs_reg2_data['charging_cap_total'])

predict2_cs = cs_data[['date','X','charging_cap_total']]
predict2_cs = predict2_cs[(predict2_cs['date'] >= cs_reg2_data.iloc[-1]['date'])].reset_index()
predict2_cs['index'] = predict2_cs.index
predict2_cs['trend_last_12months'] = predict2_cs.loc[0,'charging_cap_total'] + predict2_cs['index'] * cs_reg2.coef_
predict2_cs = predict2_cs[['date','trend_last_12months']]

# Average Capacity
cs_reg2_data_avg = cs_data[['date','charging_cap_avg','X']].dropna()
cs_reg2_data_avg = cs_reg2_data_avg.tail(4)
cs_reg2_x_avg    = np.array(cs_reg2_data_avg['X']).reshape((-1, 1))
cs_reg2_avg      = LinearRegression().fit(cs_reg2_x_avg, cs_reg2_data_avg['charging_cap_avg'])

predict2_cs_avg = cs_data[['date','X','charging_cap_avg']]
predict2_cs_avg = predict2_cs_avg[(predict2_cs_avg['date'] >= cs_reg2_data_avg.iloc[-1]['date'])].reset_index()
predict2_cs_avg['index'] = predict2_cs_avg.index
predict2_cs_avg['trend_last_12months_avg'] = predict2_cs_avg.loc[0,'charging_cap_avg'] + predict2_cs_avg['index'] * cs_reg2_avg.coef_
predict2_cs_avg = predict2_cs_avg[['date','trend_last_12months_avg']]

# merge
cs_data = cs_data.merge(predict1_cs, how='left', on='date')
cs_data = cs_data.merge(predict1_cs_avg, how='left', on='date')
cs_data = cs_data.merge(predict2_cs, how='left', on='date')
cs_data = cs_data.merge(predict2_cs_avg, how='left', on='date')

# create charging capacity figure

def create_cs_capa(data,title,legislative_period,
              group_title_achieved,cs_capa_total,cs_capa_avg,
#              group_title_trends,trend_2017_2021,trend_12_months,
              group_title_trends,trend_12_months,
#              trend_2017_2021_avg,trend_12_months_avg,
              trend_12_months_avg,
              button_legislative_period,button_til_2030,
              y_axis, y_axis2):

    """
    Create CS capacity figure
    """

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(x = data['date'], y = data['charging_cap_total'],
                         name = cs_capa_total,
                         mode='lines', line_shape="spline",
                         line=dict(color="black", dash="solid", width=3),hovertemplate='%{y:.3s}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved), 
                         secondary_y=False)
    
    fig.add_trace(go.Scatter(x = data['date'], y = data['charging_cap_avg'],
                         name = cs_capa_avg,
                         line_shape="spline",
                         line=dict(color="#ff7f0e", dash="solid", width=3), hovertemplate = '%{y:.r}',
                         legendgroup = 'actual'), 
                         secondary_y=True)
    
    fig.add_vrect(x0 = "2021-12", x1 = "2025-11",
                  annotation_text = legislative_period,
                  fillcolor = "gray", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

#    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
#                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
#                             hovertemplate = '%{y:.3s}', visible='legendonly',
#                             legendgroup="trends",
#                             legendgrouptitle_text = group_title_trends), 
#                             secondary_y=False)
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months'],
                            name = trend_12_months, line = dict(color="black", dash='dot'),
                            hovertemplate = '%{y:.3s}', visible='legendonly',
                            legendgroup="trends"), 
                            secondary_y=False)
#    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017_avg'],
#                             name = trend_2017_2021_avg, line = dict(color="#ff7f0e", dash='dot'),
#                             hovertemplate = '%{y:.2f}', visible='legendonly',
#                             legendgroup="trends",
#                             legendgrouptitle_text = group_title_trends), 
#                             secondary_y=True)
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months_avg'],
                            name = trend_12_months_avg, line = dict(color="#ff7f0e", dash='dot'),
                            hovertemplate = '%{y:.2f}', visible='legendonly',
                            legendgroup="trends"), 
                            secondary_y=True)    

    fig.update_yaxes(matches = None, title = y_axis, range = [0,8], secondary_y=False)
    fig.update_yaxes(matches = None, title = y_axis2, range = [0,50], secondary_y=True)
    fig.update_xaxes(title=None, range = ["2017-01", "2031-03"])

    fig.update_layout(
        title = title,
        template = "simple_white",
        uniformtext_mode = 'hide', #legend_title_text = 'Typ',
        font=dict(
            family = "'sans-serif','arial'", size=12, color='#000000'),
        legend = dict(
            yanchor="bottom", 
            xanchor="center", x=0.70, y=0.01,
            orientation = "v",
            font = {'size':11},
            borderwidth=1),
        legend_groupclick = "toggleitem",
        margin=dict(l=0, r=0, t=40, b=0, pad=0),
        barmode='stack',
        xaxis_hoverformat='%b %Y',
        hovermode = "x"
        )
    
    return fig


fig_cs_capa_de = create_cs_capa(cs_data,"Ladeleistung","Legislaturperiode",
                      "Tatsächlich erreicht", "Ladeleistung insgesamt","Ladeleistung je Ladepunkt (rechte Achse)",
#                        "Historische Trends","Gesamtladeleistung Trend 2017-2021","Gesamtladeleistung Trend 12 Monate",
                        "Historische Trends","Gesamtladeleistung Trend 12 Monate",
#                        "Durchschnittliche Leistung Trend 2017-2021","Durchschnittliche Leistung Trend 12 Monate",
                        "Durchschnittliche Leistung Trend 12 Monate",
                      "Nur Legislaturperiode","bis 2030",
                      "Ladeleistung insgesamt in GW","Ladeleistung je Ladepunkt in kW")

fig_cs_capa_en = create_cs_capa(cs_data,"Charging power", "Traffic Light Coalition",
                      "Actually achieved", "Total charging power", "Charging power per charging point (right axis)",
                        "Historical Trends", "Total charging power trend 12 months",
                        "Average power trend 12 months",
                      "Legislative period only", "until 2030",
                      "Total charging power in GW", "Charging power per charging point in kW")

fig_cs_capa_fr = create_cs_capa(cs_data,"Puissance de charge", "coalition en feu tricolore",
                      "Réalisation effective", "Puissance de charge totale", "Puissance de charge par point de charge (axe de droite)",
                        "Tendances historiques", "Puissance de charge totale tendance 12 mois",
                        "Puissance moyenne tendance 12 mois",
                      "Législature uniquement", "jusqu'en 2030",
                      "Puissance de charge totale en GW", "Puissance de charge par point de charge en kW")

fig_cs_capa_de.write_html("docs/germany/figures/fig_cs_capa.de.html", include_plotlyjs="directory")
fig_cs_capa_en.write_html("docs/germany/figures/fig_cs_capa.en.html", include_plotlyjs="directory")
fig_cs_capa_fr.write_html("docs/germany/figures/fig_cs_capa.fr.html", include_plotlyjs="directory")

#%% BEV per charging point -------------------------------------------------------------------------

# Define bev_per_cp_data by using bev_data
bev_per_cp_data = bev_data

# Drop 2017 charging station data for which we don't have BEV stock data
cs_data = cs_data.loc[cs_data['date'] >= '01.01.2018']
cs_data = cs_data.reset_index(drop=True)

# Keep only quarterly BEV data 
bev_per_cp_data['month'] = pd.to_datetime(bev_per_cp_data['date']).dt.month
#bev_per_cp_data = bev_per_cp_data[bev_per_cp_data['month'].isin([1,4,7,10])]
bev_per_cp_data = bev_per_cp_data[bev_per_cp_data['month'].isin([3,6,9,12])]
bev_per_cp_data = bev_per_cp_data.reset_index(drop=True)

# Define variables of interest
bev_per_cp_data['normal'] = bev_per_cp_data['actual'] / cs_data['actual_normal']
bev_per_cp_data['fast']   = bev_per_cp_data['actual'] / cs_data['actual_fast']
bev_per_cp_data['total']  = bev_per_cp_data['actual'] / (cs_data['actual_normal']+cs_data['actual_fast'])
bev_per_cp_data['capacity'] = cs_data['charging_cap_total'] *1000000 / bev_per_cp_data['actual']

bev_per_cp_data['X']      = bev_per_cp_data.index

# create bev_per_cp figure

def create_bev_per_cp(data,title,legislative_period,
              group_title_achieved,bev_per_normal,bev_per_fast,bev_per_total,capacity_avg,
              button_legislative_period,button_til_2030,
              y_axis,y_axis2):

    """
    Create bev_per_cp figure
    """

    fig = make_subplots(specs=[[{"secondary_y": True}]])



    fig.add_trace(go.Scatter(x = data['date'], y = data['normal'],
                         name = bev_per_normal,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="#ff7f0e", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}',
                         legendgrouptitle_text = group_title_achieved), 
                         secondary_y=False)

    fig.add_trace(go.Scatter(x = data['date'], y = data['fast'],
                         name = bev_per_fast,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="#d62728", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'),
                         secondary_y=False)
                        
    fig.add_trace(go.Scatter(x = data['date'], y = data['total'],
                         name = bev_per_total,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="black", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'),
                         secondary_y=False)
    
    fig.add_trace(go.Scatter(x = data['date'], y = data['capacity'],
                         name = capacity_avg,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="blue", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'),
                         secondary_y=True)    
    
    fig.add_vrect(x0 = "2021-12", x1 = "2025-11",
                  annotation_text = legislative_period,
                  fillcolor = "gray", opacity = 0.1, layer = 'below',
                  annotation_position = "top right")

    fig.update_yaxes(matches = None, title = y_axis, range = [0,120],secondary_y=False)
    fig.update_yaxes(matches = None, title = y_axis2, range = [0,10],secondary_y=True)
    fig.update_xaxes(title=None, range = ["2018-01", "2028-12"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", x=0.01, y=1,
                                       orientation = "v",borderwidth=1),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x")

    return fig

fig_bev_per_cp_de = create_bev_per_cp(bev_per_cp_data,
                                      "Verhältnis von BEV und Ladepunkten bzw. -leistung",
                                      "Legislaturperiode","Bisher erreicht",
                                      "BEV pro Normalladepunkt","BEV pro Schnellladepunkt",
                                      "BEV pro Ladepunkt (Schnell+Normal)","Ladeleistung in kW pro BEV (rechte Achse)",
                                      "Nur Legislaturperiode","bis 2030","","Ladeleistung in kW pro BEV")

fig_bev_per_cp_en = create_bev_per_cp(bev_per_cp_data,
                                      "Ratio of BEVs and charging points or power",
                                      "Traffic Light Coalition","Actually achieved",
                                      "BEV per normal charging point","BEV per fast charging point",
                                      "BEV per charging point (fast+normal)","Charging power in kW per BEV (right axis)",
                                      "Legislative period only","until 2030","","Charging power in kW per BEV")

fig_bev_per_cp_fr = create_bev_per_cp(bev_per_cp_data,
                                      "Rapport entre BEV et points de charge ou puissance",
                                      "coalition en feu tricolore","Réalisé","BEV par borne de recharge normake",
                                      "BEV par borne de recharge rapide",
                                      "BEV par borne de recharge (rapide+normale)","Puissance de charge en kW par BEV (axe de droite)",
                                      "Période du mandat","Jusqu'en  2030","","Puissance de charge en kW par BEV")

fig_bev_per_cp_de.write_html("docs/germany/figures/bev_per_cp.de.html", include_plotlyjs="directory")
fig_bev_per_cp_en.write_html("docs/germany/figures/bev_per_cp.en.html", include_plotlyjs="directory")
fig_bev_per_cp_fr.write_html("docs/germany/figures/bev_per_cp.fr.html", include_plotlyjs="directory")

##############################################################################
# Rail 
##############################################################################

#%% railroad electrification data ##################################################################

rail_elec_data         = pd.read_csv("docs/germany/data/rail_electrification.csv", sep = ",")
rail_elec_data['date'] = pd.to_datetime(rail_elec_data['date'], format='%Y')
rail_elec_data['X']    = rail_elec_data.index

# trend 2017-2021
rail_elec1_data = rail_elec_data[['date','X','actual']].dropna()
rail_elec1_data = rail_elec1_data[(rail_elec1_data['date'] >= '2017') &
                                  (rail_elec1_data['date'] <= '2021')]
rail_elec_reg1_x    = np.array(rail_elec1_data['X']).reshape((-1, 1))
rail_elec_reg1      = LinearRegression().fit(rail_elec_reg1_x, rail_elec1_data['actual'])
predict1_rail_elec = rail_elec_data[['date','X','actual']]
predict1_rail_elec = predict1_rail_elec[(predict1_rail_elec['date'] >= '2021')].reset_index()
predict1_rail_elec['index'] = predict1_rail_elec.index
predict1_rail_elec['trend_since_2017'] = predict1_rail_elec.loc[0,'actual'] + predict1_rail_elec['index'] * rail_elec_reg1.coef_
predict1_rail_elec = predict1_rail_elec[['date','trend_since_2017']]

# dynamic trend last 12 months
rail_elec_reg2_data = rail_elec_data[['date','actual','X']].dropna()
rail_elec_reg2_data = rail_elec_reg2_data.tail(12)
rail_elec_reg2_x    = np.array(rail_elec_reg2_data['X']).reshape((-1, 1))
rail_elec_reg2      = LinearRegression().fit(rail_elec_reg2_x, rail_elec_reg2_data['actual'])

predict2_rail_elec = rail_elec_data[['date','X','actual']]
predict2_rail_elec = predict2_rail_elec[(predict2_rail_elec['date'] >= rail_elec_reg2_data.iloc[-1]['date'])].reset_index()
predict2_rail_elec['index'] = predict2_rail_elec.index
predict2_rail_elec['trend_last_12months'] = predict2_rail_elec.loc[0,'actual'] + predict2_rail_elec['index'] * rail_elec_reg2.coef_
predict2_rail_elec = predict2_rail_elec[['date','trend_last_12months']]

# merge
rail_elec_data = rail_elec_data.merge(predict1_rail_elec, how='left', on='date')
rail_elec_data = rail_elec_data.merge(predict2_rail_elec, how='left', on='date')

# create railroad electrification figure

def create_rail(data,title,legislative_period,
                goal,linear,
                actual,
                group_title_trends,trend_2017_2021,trend_12_months,
                y_axis):
    
    """
    Create rail figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "#ac452f",
                             hovertemplate = '%{y:.3s}%',
                             legendgroup = 'misc',
                             legendgrouptitle_text = None))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan'],
                         name = linear,
                         mode = 'lines', line = dict(color="#ac452f", dash='dot'),
                         hovertemplate = '%{y:.3s}%',
                         legendgroup = 'misc'))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = actual,
                         marker_color = '#3c435a', hovertemplate = '%{y:.3s}%',
                         legendgroup = 'misc'))

    fig.add_vrect(x0 = "2021-05", x1 = "2025-11",
                  annotation_text = legislative_period,
                  fillcolor = "gray", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#bfbab1", dash='dot'),
                             hovertemplate = '%{y:.3s}', visible='legendonly',
                             legendgroup="trends", mode = 'lines',
                             legendgrouptitle_text = group_title_trends))

    fig.update_yaxes(matches = None, title = y_axis, range = [0,100])
    fig.update_xaxes(title=None)

    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                      font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                      legend = dict(yanchor="top", xanchor="left", x=0.01, y=1, orientation="v",
                                    borderwidth=1),
                      legend_title_text = '', margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      xaxis_hoverformat='%Y', hovermode = "x",
                      legend_groupclick = "toggleitem")

    return fig

fig_rail_elec_de = create_rail(rail_elec_data, "Elektrifizierter Anteil des Schienennetzes",
                               "Legislaturperiode",
                               "Ziel der Koalition","Linearer Verlauf","Tatsächlich erreicht",
                               "Historische Trends","2017-2021","Letzte 12 Monate",
                               "Prozent")
fig_rail_elec_en = create_rail(rail_elec_data, "Electrified share of the rail network",
                               "Traffic Light Coalition",
                               "Target of the coalition","Linear progression","Actually achieved",
                               "Historic trends","2017-2021","Last 12 months",
                               "Percentage")
fig_rail_elec_fr = create_rail(rail_elec_data, "Part du réseau ferroviaire électrifié",
                               "coalition en feu tricolore",
                               "Objectif de la coalition","Progression linéaire","Réalisé",
                               "Tendances historiques","2017-2021","12 derniers mois",
                               "Pourcentage")

fig_rail_elec_de.write_html("docs/germany/figures/electrification.de.html", include_plotlyjs="directory")
fig_rail_elec_en.write_html("docs/germany/figures/electrification.en.html", include_plotlyjs="directory")
fig_rail_elec_fr.write_html("docs/germany/figures/electrification.fr.html", include_plotlyjs="directory")


""" #%% cars vs. population figure ---------------------------------------------------------------------


# Load data 
cars_pop = pd.read_csv("docs/germany/data/cars_pop.csv", sep = ",")
cars_pop['months'] = cars_pop['month'].astype(str)
cars_pop['date']   = pd.to_datetime(cars_pop['date'], format='%d.%m.%Y')

cars_pop['X']      = cars_pop.index

# Recode pop in thousands
cars_pop['poptsd'] = cars_pop['pop'] / 1000
cars_pop['car_dens'] = cars_pop['cars'] / cars_pop['poptsd']

cars_pop['X']      = cars_pop.index

# create cars_pop figure

def create_cars_pop(data, title, legislative_period,
                    car_density, population, cars_abs,
                    button_legislative_period, button_til_2030,
                    y_axis, y_axis2):

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(x=data['date'], y=data['car_dens'],
                             name=car_density,
                             mode='lines',
                             line_shape="spline",
                             line=dict(color="#ff7f0e", dash="solid", width=3),
                             hovertemplate='%{y:.0f}'),
                  secondary_y=False)

    fig.add_trace(go.Scatter(x=data['date'], y=data['pop'],
                             name=population,
                             mode='lines',
                             line_shape="spline",
                             line=dict(color="black", dash="solid", width=3),
                             hovertemplate='%{y:.4s}'),
                  secondary_y=True)

    fig.add_trace(go.Scatter(x=data['date'], y=data['cars'],
                             name=cars_abs,
                             mode='lines',
                             line_shape="spline",
                             line=dict(color="#d62728", dash="solid", width=3),
                             hovertemplate='%{y:.4s}'),
                  secondary_y=True)
    
    fig.add_vrect(
        x0 = "2021-12", x1 = "2025-11",
        annotation_text = legislative_period,
        fillcolor = "gray", opacity = 0.1, layer = 'below',
        annotation_position = "top left")

    fig.update_yaxes(title_text=y_axis, range=[200,1000], secondary_y=False)
    fig.update_yaxes(title_text=y_axis2, range = [20000000,100000000],secondary_y=True)
    fig.update_xaxes(title=None, range=["2012-09", "2028-12"])

    fig.update_layout(
        title=title, template="simple_white",
        uniformtext_mode='hide',
        font=dict(family="'sans-serif','arial'", size=12, color='#000000'),
        legend=dict(yanchor="bottom", xanchor="left", x=0.01, y=0.01, orientation="v",borderwidth=1),
        legend_groupclick="toggleitem",
        margin=dict(l=0, r=0, t=40, b=0, pad=0),
        barmode='stack',
        xaxis_hoverformat='%b %Y',
        hovermode="x",
        showlegend=True
    )

    return fig

create_cars_pop_de = create_cars_pop(cars_pop,
                                     "Pkw pro 1000 Personen","Legislaturperiode","Anzahl Pkw je 1000 Personen",
                                     "Bevölkerung","Pkw-Gesamtbestand","Nur Legislaturperiode",
                                     "bis 2030", "Anzahl von Pkw je 1000 Personen",
                                     "Anzahl Bevölkerung und Pkw")


create_cars_pop_en = create_cars_pop(cars_pop,
                                      "Passenger cars per 1000 people (left axis)",
                                      "Legislative period","Number of passenger cars per 1000 inhabitants","Population","Total car stock",
                                      "Legislative period only","until 2030","Cars per 1000 inhabitants", "Total population and car fleet")

create_cars_pop_fr = create_cars_pop(cars_pop,
                                      "Voitures pour 1000 personnes (axe gauche)",
                                      "Période du mandat","Nombre de voitures pour 1000 habitants","Population","Stock total de voitures",
                                      "Période du mandat","Jusqu'en  2030","Par 1000 habitants","Population totale et flotte de voitures")

create_cars_pop_de.write_html("docs/germany/figures/cars_pop.de.html", include_plotlyjs="directory")
create_cars_pop_en.write_html("docs/germany/figures/cars_pop.en.html", include_plotlyjs="directory")
create_cars_pop_fr.write_html("docs/germany/figures/cars_pop.fr.html", include_plotlyjs="directory") 
"""
