#%% Import packages

import sys
import os
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
from datetime import datetime

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px
import statsmodels.api as sm

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'scripts':
    os.chdir('../../')
else:
    sys.path.append('.')

print(os.getcwd())

# import custom packages

import scripts.colors as colors
import scripts.util as oet

import re
import time


'''
Information about the Figures and the Script:

The figures are created in the following order:
1. Household electricity tariffs - nominal and real
2. Hourly wholesale electricity prices - Violin Chart
3. Duckcur
4. Negative Prices cummulated
5. Negative price duration curve
6. Negative prices hourly distribution
7. Negative Prices Histogram (10€ Price Bins)
8. Negative Prices Histogram (Consecutive Hours)
10. Generation Shares (Slider)
11. Market Values / Generation Sharaes Renewables
12. Revenues PV

for the basic layout dicts are defined in the beginning:
- basic_layout_dict: for the figures without buttons
- basic_layout_dict_button: for the figures with buttons or Dropdown Menus
- legend_layout_dict_top_reverse: for the legend on the right, top-aligned and reversed order

Colors are used from the colors module:
- mostly from the dict 'prices', Generation Shares take colors from the dict 'tech'
- the List 'years_fading' is used for the colors of the traces with several years (two types, one red and one blue on top)

years are defined as a list of integers for the years in the data
- the current year for the summer duck curve has to be activated as it has no data for the first months

some y axis ranges are calculated based on the data and adjust automatically 

'''

print("")
print("---------- Script: prices ----------")
print("")

#%%Prepare Smard Data Wide Table

####################################################################################################
#Smard Data#
####################################################################################################

smard_data = pd.read_csv('https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/smard/smard_data.csv', sep = ',')

# Prepare data

smard_data['time'] = pd.to_datetime(smard_data['time'], format = 'ISO8601')
smard_data['daytime'] = smard_data['time'].dt.strftime('%m-%d %H:%M')
smard_data['year'] = smard_data['time'].dt.year
smard_data['hour'] = smard_data['time'].dt.strftime('%H:%M')

#create wide table for prices with years as columns and daytime as index
smard_wide = smard_data.pivot_table(index = 'daytime', columns='year', values='da_price')
smard_wide.reset_index(inplace=True)
smard_wide.insert(1, 'hour', smard_wide['daytime'].str[-5:])

smard_wide = smard_wide.reset_index(drop=True)

####################################################################################################
#calculate the max amount of hours with negative prices (for y-Axis range)
negative_values_count = smard_wide.drop(columns = ['daytime', 'hour'])
negative_values_count = (negative_values_count < 0).sum()
max_numb_negative_price = max(negative_values_count.values)
max_numb_negative_price.astype(int)

#Calculates the nearest multiple of 50 to the maximum number of hours with negative prices
def roundup_to_multiple(x, multiple):
    return int(np.ceil(x / multiple)) * multiple




#%%
#################################################################################################
#################### Set Basic Layout Parameters 
####################################################################################################


basic_layout_dict = dict(template = 'simple_white',
                     uniformtext_mode = 'hide',
                     font=dict(family = "sans-serif,arial", size=14, color='#000000'),
                    showlegend=True,
                    margin=dict(l=0, r=0, t=50, b=0, pad=0),
                    xaxis_title_font=dict(size=13),
                    yaxis_title_font=dict(size=13))

basic_layout_dict_button = dict(template = 'simple_white',
                     uniformtext_mode = 'hide',
                     font=dict(family = "sans-serif,arial", size=14, color='#000000'),
                    showlegend=True,
                    margin=dict(l=0, r=0, t=120, b=0, pad=0),
                    xaxis_title_font=dict(size=13),
                    yaxis_title_font=dict(size=13))

#dict for the legend layout (on the right, top-aligned, reversed order)
legend_layout_dict_top_reverse = dict(yanchor='top', xanchor='left', x=1.1, y=1,
            orientation = 'v',
            traceorder = 'reversed')



##%%####################################################################################################
#  Household electricity tariffs - nominal and real -----------------------------------------------
####################################################################################################

####################
#Data Import
####################

hh_elec_tariff_nom_data           = pd.read_csv('docs/germany/data/hh_elec_tariff_nom.csv', sep = ',')

hh_elec_tariff_real_data           = pd.read_csv('docs/germany/data/hh_elec_tariff_real.csv', sep = ',')

#####################
#Further settings
#####################

traces = ['proc_net_dist', 'proc_dist', 'net', 'conc', 'eeg', 'other', 'tax', 'vat']

tickvals = list(range(1998, 2025)) # years from 1998 to 2024
ticktext = [str(i) for i in tickvals]
#ticktext[-1] = f'{ticktext[-1]}-7' # add month to last year 

#totals for the text -  Adjusting to have only one decimal place
hh_elec_tariff_nom_data['total'] = hh_elec_tariff_nom_data[traces].sum(axis = 1)
hh_elec_tariff_real_data['total'] = hh_elec_tariff_real_data[traces].sum(axis = 1)

text_nom = hh_elec_tariff_nom_data['total'].apply(lambda x: f'{x:.1f}')
text_real = hh_elec_tariff_real_data['total'].apply(lambda x: f'{x:.1f}')

#Dictionary for the language settings
                                                                    
language_dict = {}

language_dict['de'] = {
    'title_nom' : 'Haushalts-Strompreise (laufende Preise)',
    'title_real' : 'Haushalts-Strompreise (reale Preise)',
    'proc_net_dist' : 'Beschaffung, Netzentgelt,<br>Vertrieb',
    'proc_dist' : 'Beschaffung, Vertrieb',
    'net' : 'Netzentgelt',
    'vat' : 'Mehrwertsteuer',
    'tax' : 'Stromsteuer',
    'conc' : 'Konzessionsabgabe',
    'eeg' : 'EEG-Umlage',
    'other' : 'Sonstige Umlagen',
    'group_title_proc' : 'Beschaffung, Netzentgelt, <br>Vertrieb',
    'group_title_nonproc' : 'Steuern, Abgaben, Umlagen',
    'button_current_price' : 'Laufende Preise',
    'button_real_price' : 'Reale Preise',
    'y_axis' : 'Cent pro kWh'
    }

language_dict['en'] = {
    'title_nom' : 'Household electricity prices (current prices)',
    'title_real' : 'Household electricity prices (real prices)',
    'proc_net_dist' : 'Procurement, network<br>charges, distribution',
    'proc_dist' : 'Procurement,<br>distribution',
    'net' : 'Network charges',
    'vat' : 'Value-added tax',
    'tax' : 'Electricity tax',
    'conc' : 'Concession fee',
    'eeg' : 'EEG levy',
    'other' : 'Other levies',
    'group_title_proc' : 'Procurement, network<br>charges,distribution',
    'group_title_nonproc' : 'Taxes, duties, levies',
    'button_current_price' : 'Current prices',
    'button_real_price' : 'Real prices',
    'y_axis' : 'cents per kWh'
    }

language_dict['fr'] = {
    'title_nom' : "Prix de l'électricité pour les ménages (prix courants)",
    'title_real' : "Prix de l'électricité pour les ménages (prix réels)",
    'proc_net_dist' : 'Approvisionnement, rémunération<br>du réseau, distribution',
    'proc_dist' : 'Approvisionnement, distribution',
    'net' : 'Rémunération du réseau',
    'vat' : 'TVA',
    'tax' : "Taxe sur l'électricité",
    'conc' : 'Taxe de concession',
    'eeg' : 'Prélèvement EEG',
    'other' : 'Autres prélèvements',
    'group_title_proc' : 'Approvisionnement, rémunération<br>du réseau, distribution',
    'group_title_nonproc' : 'Taxes, impôts, prélèvements',
    'button_current_price' : 'Prix courantes',
    'button_real_price' : 'Prix réeles',
    'y_axis' : 'Cent par kWh'
    }

#######################
#Create figure
#######################

def price_components_bars(data_nom, data_real, language):

    fig = go.Figure()

    for i, trace in enumerate(traces):
        fig.add_trace(go.Bar(x = data_nom['date'], 
                             y = data_nom[trace],
                             name = language_dict[language][trace],
                             marker_color = colors.prices[trace],
                             hovertemplate = '%{y:.1f}',
                             legendgroup = language_dict[language]['group_title_proc'] if i < 3 else language_dict[language]['group_title_nonproc'],
                             legendgrouptitle= dict(text = language_dict[language]['group_title_proc'] if i < 3 else language_dict[language]['group_title_nonproc']),
                             visible = True))

        fig.add_trace(go.Bar(x = data_real['date'], 
                             y = data_real[trace],
                             name = language_dict[language][trace],
                             marker_color = colors.prices[trace],
                             hovertemplate = '%{y:.1f}',
                             legendgroup = language_dict[language]['group_title_proc'] if i < 3 else language_dict[language]['group_title_nonproc'],
                             legendgrouptitle= dict(text = language_dict[language]['group_title_proc'] if i < 3 else language_dict[language]['group_title_nonproc']),
                             visible = False))
        
    #add the sums as text
    fig.add_trace(go.Scatter(x = data_nom['date'], y = data_nom['total'],
                                text = text_nom,
                                mode = 'text',
                                textposition='top center',
                                textfont=dict(size=10),
                                hoverinfo = 'skip',
                                showlegend=False))
    
    fig.add_trace(go.Scatter(x = data_real['date'], y = data_real['total'],
                                text = text_real,
                                mode = 'text',
                                textposition='top center',  
                                textfont=dict(size=10),
                                hoverinfo = 'skip',
                                showlegend=False,
                                visible = False))

    fig.update_layout(basic_layout_dict_button,
                    title = language_dict[language]['title_nom'],
                      showlegend = True,
                      barmode='stack',
                        legend = dict(yanchor='top', xanchor='left', font=dict(size = 12)),
                        legend_grouptitlefont_size = 13,
                         legend_traceorder = 'grouped+reversed',
                         legend_groupclick = 'toggleitem',
                      xaxis_hoverformat='%b %Y',
                      hovermode = 'x unified')
    
    fig.update_yaxes(matches = None, 
                     title = language_dict[language]['y_axis'], 
                     range = [0,50])
    
    fig.update_xaxes(title=None,
                    range = [tickvals[0] - 1, tickvals[-1] + 1],
                    tickvals = tickvals,
                    ticktext = ticktext
)
                     
    
    #add buttons to switch between current and real prices

    nominal_traces = [(i % 2 == 0) for i in range(len(fig.data) - 2)] + [True, False]
    real_traces = [(i % 2 == 1) for i in range(len(fig.data) - 2)] + [False, True]
    
    fig.update_layout(
    updatemenus=[
        dict(type='buttons', direction='left', active=0, showactive=True, pad={'r': 10, 't': 0},
        xanchor='left', yanchor='top', x = 0, y = 1.125,
        buttons=list([dict(label=language_dict[language]['button_current_price'], method='update',
                            args=[{'visible': nominal_traces},
                                  {'title' : language_dict[language]['title_nom']}]),
                         dict(label=language_dict[language]['button_real_price'], method='update',
                            args=[{'visible': real_traces},
                                  {'title' : language_dict[language]['title_real']}]),
                ]),
          )
          ])
        

    return fig


fig_hh_elec_tariff_de = price_components_bars(hh_elec_tariff_nom_data, hh_elec_tariff_real_data, 'de')
fig_hh_elec_tariff_en = price_components_bars(hh_elec_tariff_nom_data, hh_elec_tariff_real_data, 'en')  
fig_hh_elec_tariff_fr = price_components_bars(hh_elec_tariff_nom_data, hh_elec_tariff_real_data, 'fr')

fig_hh_elec_tariff_de.write_html('docs/germany/figures/hh_elec_tariff.de.html', include_plotlyjs='directory')
fig_hh_elec_tariff_en.write_html('docs/germany/figures/hh_elec_tariff.en.html', include_plotlyjs='directory')
fig_hh_elec_tariff_fr.write_html('docs/germany/figures/hh_elec_tariff.fr.html', include_plotlyjs='directory')




#%% ################################################################################################
#Hourly wholesale electricity prices - Violin Chart
####################################################################################################

years = list(smard_data['year'].unique())

# Years as Strings for the legend and labels
years_str = [str(i) for i in years]
years_str[-1] = f'{years_str[-1]}*'

#calculate maximum value for y-axis
maximum = smard_data['da_price'].max()
maximum = roundup_to_multiple(maximum, 100)

language_dict = {}

language_dict['de'] = {
    'title' : 'Stündlicher Großhandelspreis Strom',
    'legend' : 'Jahre',
    'y_axis' : 'EUR/MWh'}

language_dict['en'] = {
    'title' : 'Hourly wholesale price for electricity',
    'legend' : 'Years',
    'y_axis' : 'EUR/MWh'}

language_dict['fr'] = {
    'title' : "Prix de gros horaire de l'électricité",
    'legend' : 'Années',
    'y_axis' : 'EUR/MWh'}



def create_wholesale_electricity(data,language):

    '''
    Create hourly wholesale electricity price figure
    '''

    fig = go.Figure()

    #setting basic layout
    fig.update_layout(basic_layout_dict)

    for i, year in enumerate(years): 
        fig.add_trace(go.Violin(y = data[year], box_visible=True, line_color='black',
                               meanline_visible=True, 
                               fillcolor=colors.prices['net'], #color for the violin (cornflowerblue) -  Check the dcit for alternatives
                               opacity=1,  points = False,
                               hoveron = 'violins+points',                               
                               x0=year, name = years_str[i], 
                               legendgroup = 'All'))

    fig.update_yaxes(matches = None, title = language_dict[language]['y_axis'], range = [-200,maximum],
                     tickmode = 'linear', dtick = 100)
    
    fig.update_xaxes(title=None, 
                        tickvals = years,
                        ticktext = years_str
                        )
    
    fig.update_layout(
        hovermode='x unified',
        title = language_dict[language]['title'],
        showlegend=False,)

    return fig


wholesale_electricity_de = create_wholesale_electricity(smard_wide, 'de')
wholesale_electricity_en = create_wholesale_electricity(smard_wide, 'en')
wholesale_electricity_fr = create_wholesale_electricity(smard_wide, 'fr')

wholesale_electricity_de.write_html('docs/germany/figures/wholesale_electricity.de.html', include_plotlyjs='directory')
wholesale_electricity_en.write_html('docs/germany/figures/wholesale_electricity.en.html', include_plotlyjs='directory')
wholesale_electricity_fr.write_html('docs/germany/figures/wholesale_electricity.fr.html', include_plotlyjs='directory')


#%% ################################################################################################
#Duckcurves
####################################################################################################

#############
#Data Preparation
#############
smard_subset = smard_data.iloc[:, :2]
tmp = copy.deepcopy(smard_subset)
tmp['hour'] = tmp['time'].dt.hour
tmp['year'] = tmp['time'].dt.year
tmp['season'] = tmp['time'].dt.month.apply(lambda x: 'summer' if 4 <= x <= 9 else 'winter')

#Data for the Summer
summer = tmp[tmp['season'] == 'summer']
grouped = summer.groupby(['hour', 'year'])['da_price'].mean().unstack()
yearly_avg = summer.groupby('year')['da_price'].mean()
duck_summer = grouped.div(yearly_avg, axis=1)

#Data for the Winter
winter = tmp[tmp['season'] == 'winter']
grouped = winter.groupby(['hour', 'year'])['da_price'].mean().unstack()
yearly_avg = winter.groupby('year')['da_price'].mean()
duck_winter = grouped.div(yearly_avg, axis=1)

#############
#Additional Settings
#############

#Setting the color list for the traces
color_list = colors.prices['years_fading']

#Create tick values and labels (every second hour as tick)
tickvals = list(range(24, -1, -2))

#Dictionary for the language settings
language_dict = {}

language_dict['de'] = {
    'title_summer' : 'Relative Preisverteilung über Stunden des Tages (Sommer)',
    'title_winter' : 'Relative Preisverteilung über Stunden des Tages (Winter)',
    'x_axis' : 'Stunde des Tages',
    'y_axis' : 'Preis relativ zum saisonalen Mittel',
    'legend_title': 'Jahr'
    }

language_dict['en'] = {
    'title_summer' : 'Relative price distribution over hours of the day (summer)',
    'title_winter' : 'Relative price distribution over hours of the day (winter)',
    'x_axis' : 'Hour of the day',
    'y_axis' : 'Price relative to the seasonal average',
    'legend_title': 'Year'
    }

language_dict['fr'] = {
    'title_summer' : 'Distribution relative des prix au cours de la journée (été)',
    'title_winter' : 'Distribution relative des prix au cours de la journée (hiver)',
    'x_axis' : 'Heure du jour',
    'y_axis' : 'Prix relatif à la moyenne saisonnière',
    'legend_title': 'Année'
    }


#calculate the max value for the y-axis range (based onm the maximum value of the summer-duckcurve)
y_max = duck_summer.max().max()
y_max = roundup_to_multiple(y_max, 0.5)


def duckcurve(data, language, season_choice):
    
    #setting the years for the figure
    years = list(reversed(data.columns))
    current_year = years[0]

    #####################
    ##creating the figure
    #####################
    
    fig = go.Figure()

    #setting basic layout
    fig.update_layout(basic_layout_dict,
                        showlegend=True,
                        legend = legend_layout_dict_top_reverse)
    
    #create the traces for every year
    for i, year in enumerate(years): 
            
        #Setting the color for the trace - after seven years the last color is used (fading out)
        color = color_list[i] if i < len(color_list) else color_list[-1]

        fig.add_trace(go.Scatter(x=data.index, 
                                y=data[year], 
                                mode='lines', 
                                name= str(year),
                                line=dict(color = color,
                                        width = 2 if year != current_year else 3),
                                        hovertemplate='%{y:.2f}'  # Format für Hover
        ))

    fig.data[0].name = f'{years[0]}*' #mark the current year with an asterisk
    fig.data = fig.data[::-1] #reverse the order of the traces so the current year is on the top 

    #Setting the title and the labels & further layout
    fig.update_layout(
        title=language_dict[language][f"title_{season_choice}"],
        xaxis_title=language_dict[language]['x_axis'],
        yaxis_title=language_dict[language]['y_axis'],
        legend_title=language_dict[language]['legend_title'],
        hovermode='x unified',
)

    #Setting y-axis range
    fig.update_yaxes(range=[0, y_max])

    #Setting the ticks for the x-axis
    fig.update_xaxes(tickvals = tickvals,
                    tickmode = 'array',
                    title_standoff = 5)
    
    return fig

fig_duckcurve_sum_DE = duckcurve(duck_summer, 'de', 'summer')
fig_duckcurve_sum_DE.write_html('docs/germany/figures/fig_duckcuve_sum.de.html')

fig_duckcurve_win_DE = duckcurve(duck_winter, 'de', 'winter')
fig_duckcurve_win_DE.write_html('docs/germany/figures/fig_duckcuve_win.de.html')

fig_duckcurve_sum_EN = duckcurve(duck_summer, 'en', 'summer')
fig_duckcurve_sum_EN.write_html('docs/germany/figures/fig_duckcuve_sum.en.html')

fig_duckcurve_win_EN = duckcurve(duck_winter, 'en', 'winter')
fig_duckcurve_win_EN.write_html('docs/germany/figures/fig_duckcuve_win.en.html')

fig_duckcurve_sum_FR = duckcurve(duck_summer, 'fr', 'summer')
fig_duckcurve_sum_FR.write_html('docs/germany/figures/fig_duckcuve_sum.fr.html')

fig_duckcurve_win_FR = duckcurve(duck_winter, 'fr', 'winter')
fig_duckcurve_win_FR.write_html('docs/germany/figures/fig_duckcuve_win.fr.html')

#%%################################################################################################
# Neg Prices cummulated
####################################################################################################

#####################
#preparing data
#####################

data = copy.deepcopy(smard_data)

#separate the date into day and year (for pivoting) and filter negative data
data['time'] = pd.to_datetime(data['time'], format = 'ISO8601')

data['year'] = data['time'].dt.year
data['daytime'] = data['time'].dt.strftime('%m-%d')
data['negative_price'] = data['da_price'] < 0

#calculate the amount of negative hours for every day
daily_neg_prices = data.groupby(['daytime', 'year'])['negative_price'].sum().reset_index()

#create the cumulated hours with negative prices for every year
daily_neg_prices['hours_cumulated'] = daily_neg_prices.groupby('year')['negative_price'].cumsum()
daily_neg_prices['hours_cumulated'] = daily_neg_prices['hours_cumulated'].astype(int)
daily_neg_prices.insert(2, 'full_date', pd.to_datetime(daily_neg_prices['year'].astype(str) + '-' + daily_neg_prices['daytime'], format = '%Y-%m-%d'))
daily_neg_prices['day_of_year'] = (daily_neg_prices['full_date'].dt.dayofyear)   


#####################
#Further Settings for the figure
#####################

#extracting the max value for the y-axis
y_max = roundup_to_multiple(daily_neg_prices['hours_cumulated'].max(), 50)

#Setting the displayed years (takes the years from the data)
years = data['year'].unique()

#mirror the years so the current year is first
years = list(reversed(years))
current_year = years[0]

language_dict = {}

language_dict['de'] = {'title'     : 'Jährliche Stunden mit negativem Strompreis, kumuliert',
                        'x_axis_title'    : 'Monate',
                        'y_axis_title'    : 'Anzahl Stunden mit negativem Preis',
                        'hovertext': '%{y} Stunden',
                        'tick_prefix' : 'Anzahl Stunden mit negativem Preis nach ',
                        'tick_suffix' : ' Tagen',
                        'legend_title' : 'Jahre',
                        'months' : {
                            'Januar': 1,
                            'Februar': 32,
                            'März': 60,
                            'April': 91,
                            'Mai': 121,
                            'Juni': 152,
                            'Juli': 182,
                            'August': 213,
                            'September': 244,
                            'Oktober': 274,
                            'November': 305,
                            'Dezember': 335}
                       }

language_dict['en'] = {'title'     : 'Annual hours with negative electricity price, cumulated',
                        'x_axis_title'    : 'Months',
                        'y_axis_title'    : 'Number of hours with negative price',
                        'hovertext': '%{y} hours',
                        'tick_prefix' : 'Number of hours with negative price after ',
                        'tick_suffix' : ' days',
                        'legend_title' : 'Years',
                        'months' : {
                            'January': 1,
                            'February': 32,
                            'March': 60,
                            'April': 91,
                            'May': 121,
                            'June': 152,
                            'July': 182,
                            'August': 213,
                            'September': 244,
                            'October': 274,
                            'November': 305,
                            'December': 335}
                          }

language_dict['fr'] = {'title'     : "Heures annuelles avec un prix négatif de l\'électricité, cumulées",
                        'x_axis_title'    : 'Mois',
                        'y_axis_title'    : "Nombre d'heures avec un prix négatif",
                        'hovertext': '%{y} heures',
                        'tick_prefix' : "Nombre d'heures avec un prix négatif après",
                        'tick_suffix' : 'jours',
                        'legend_title' : 'Années',
                        'months' : {
                            'Janvier': 1,
                            'Février': 32,
                            'Mars': 60,
                            'Avril': 91,
                            'Mai': 121,
                            'Juin': 152,
                            'Juillet': 182,
                            'Août': 213,
                            'Septembre': 244,
                            'Octobre': 274,
                            'Novembre': 305,
                            'Décembre': 335}
                            }



def create_cumulated_neg_price(data, language):
    
    fig = go.Figure()

    #Setting the layout
    fig.update_layout(basic_layout_dict, 
                      legend = legend_layout_dict_top_reverse)

    #Loading the List of Colors from the module colors
    color_list = colors.prices['years_fading']

    for i, year in enumerate(years):

        #Loading the data for the every year
        yearly_data = data[data['year'] == int(year)]

        #Setting the color for the trace - after seven years the last color is used (fading out)
        color = color_list[i] if i < len(color_list) else color_list[-1] 

        fig.add_trace(go.Scatter(
            x=yearly_data['day_of_year'], #the day of the year is making less problems that date (leapyears)
            y=yearly_data['hours_cumulated'],
            mode='lines', 
            name=str(year),
            customdata=yearly_data['full_date'].dt.strftime('%m-%d'),
            hovertemplate= language_dict[language]['hovertext'],
            line=dict(color=color, 
                      width = 2 if year != current_year else 3), #the current year is highlighted
            ))
        
    # Add a star for the actual year to note that the data is constantly updated
    fig.data[0].name = f'{years[0]}*'

    #Sets the order of the traces (actual year on top) 
    fig.data = fig.data[::-1]
        

    fig.update_layout(title = language_dict[language]['title'],
                        yaxis=dict(
                            title = language_dict[language]['y_axis_title'],
                            zeroline=True,
                            range=[0, y_max]),    
                        xaxis=dict(
                            title = language_dict[language]['x_axis_title'],
                            tickmode='array',
                            tickvals= list(language_dict[language]['months'].values()),
                            ticktext= list(language_dict[language]['months'].keys()),
                            range=[1, 366],
                            tickprefix = language_dict[language]['tick_prefix'],
                            ticksuffix = language_dict[language]['tick_suffix']),
                        legend_title = language_dict[language]['legend_title'],
                        hovermode = 'x unified',
                        )
                           
                      

    return fig


fig_cumulated_neg_price_de = create_cumulated_neg_price(daily_neg_prices, 'de')
fig_cumulated_neg_price_en = create_cumulated_neg_price(daily_neg_prices, 'en')
fig_cumulated_neg_price_fr = create_cumulated_neg_price(daily_neg_prices, 'fr')


fig_cumulated_neg_price_de.write_html('docs/germany/figures/negative_price_cumulated.de.html', include_plotlyjs='directory')
fig_cumulated_neg_price_en.write_html('docs/germany/figures/negative_price_cumulated.en.html', include_plotlyjs='directory')
fig_cumulated_neg_price_fr.write_html('docs/germany/figures/negative_price_cumulated.fr.html', include_plotlyjs='directory')


#%%################################################################################################ 
# negative price duration curve
####################################################################################################

################
#Further settings for the figure
################

#Setting the displayed years
years = list(reversed(smard_data['year'].unique()))
current_year = years[0]

#setting the color list for the traces
color_list = colors.prices['years_fading']

# calculating the range of the x-axis 
x_axis_max = roundup_to_multiple(max_numb_negative_price, 50)

language_dict = {}

language_dict['de'] = {'title': 'Negative Preis-Dauer-Kurve', 
                       'legend_title': 'Jahre', 
                       'x_axis': 'Stunden pro Jahr', 
                       'y_axis': 'Preis, EUR/MWh', 
                       'hovertext': '%{x}h',
                       #'tickprefix': 'Stunden unter einem Preis von ',
                       #'ticksuffix': ' EUR/MWh'}
                       }
                 
language_dict['en'] = {'title': 'Negative price-duration-curve',
                        'legend_title': 'Years',
                        'x_axis': 'Hours per year',
                        'y_axis': 'Price, EUR/MWh',
                        'hovertext': '%{x} hours',
                        #'tickprefix': 'Hours with price below ',
                        #'ticksuffix': ' EUR/MWh'}
                        }

language_dict['fr'] = {'title': 'Courbe prix-durée négative',
                        'legend_title': 'Années',
                        'x_axis': 'Heures par an',
                        'y_axis': 'Prix, EUR/MWh',
                        'hovertext': '%{x} heures',
                        #'tickprefix': 'Heures avec le prix ci-dessous ',
                        #'ticksuffix': ' EUR/MWh'}
                        }



def create_price_duration_curve(data, language):

    fig = go.Figure()

    #Setting the basic layout
    fig.update_layout(basic_layout_dict,
                    legend = legend_layout_dict_top_reverse)

    for i, year in enumerate(years): 
        
        ##################
        #filtering the data for the year
        ##################
        yearly_data = data.loc[:, ['hour', year]]

        #sorting the data by the prices
        yearly_data = yearly_data.sort_values(by=year)
        
        #filter for negative prices
        yearly_data = yearly_data[yearly_data[year] < 0]

        #adding the duration of negative prices per year by counting the hours with negative prices
        yearly_data.insert(2, 'duration', range(1, len(yearly_data) + 1))

        ##################
        #creating the figure
        ##################

        #Setting the color for the trace - after seven years the last color is used (fading out)
        color = color_list[i] if i < len(color_list) else color_list[-1] 

        fig.add_trace(go.Scatter(x=yearly_data['duration'], 
                                 y=yearly_data[year], 
                                 mode='lines', 
                                 name= str(year),
                                 line=dict(color = color,
                                            width = 2 if year != current_year else 3),
                                 hovertemplate = language_dict[language]['hovertext'],
                                 )
                                 )
        
    # Add a star for the actual year to note that the data is constantly updated
    fig.data[0].name = f'{years[0]}*'

    #Sets the order of the traces (actual year on top) 
    fig.data = fig.data[::-1]

    #set the layout of the figure
    fig.update_layout(
        title = language_dict[language]['title'],
        legend_title_text = language_dict[language]['legend_title'],
        hovermode = 'y unified',
        hoverlabel_align = 'right'
)

    fig.update_xaxes(
        position=0,  # Verschiebt die x-Achse nach oben
        mirror='ticks',
        title_standoff=0,
        title = language_dict[language]['x_axis'],
        tickmode = 'linear',
        dtick =50,
        showgrid=True,  # Gitterlinien anzeigen
        gridcolor='LightGray',  # Farbe der Gitterlinien
        gridwidth=1,
        range = [0, x_axis_max],) 
    
    fig.update_yaxes(
        title = language_dict[language]['y_axis'],
        zeroline=True,
        range = [0, None],
        tickvals = list(range(-500, 0, 100)), #setting tickvals so the prefix&suffix dont show up
        #tickprefix = language_dict[language]['tickprefix'],
        #ticksuffix = language_dict[language]['ticksuffix'])
        )

    return fig  

fig_neg_price_dur_de = create_price_duration_curve(smard_wide, 'de')
fig_neg_price_dur_en = create_price_duration_curve(smard_wide, 'en')
fig_neg_price_dur_fr = create_price_duration_curve(smard_wide, 'fr')


fig_neg_price_dur_de.write_html('docs/germany/figures/negative_price_duration.de.html', include_plotlyjs='directory')
fig_neg_price_dur_en.write_html('docs/germany/figures/negative_price_duration.en.html', include_plotlyjs='directory')
fig_neg_price_dur_fr.write_html('docs/germany/figures/negative_price_duration.fr.html', include_plotlyjs='directory')

#%%################################################################################################ 
# negative prices by hours 
####################################################################################################

#Setting the displayed years
years = list(reversed(smard_data['year'].unique()))
current_year = years[0]

#############
#prepare data
#############
prepared_data = pd.Series([f'{hour:02d}:00' for hour in range(24)], name='hour')
prepared_data = prepared_data.to_frame()

hourly_negative = smard_data.loc[smard_data['da_price'] < 0, ['da_price','hour', 'year']]
hourly_negative['negative_price'] = hourly_negative['da_price'] < 0
hourly_negative = hourly_negative.groupby(['hour', 'year'])['negative_price'].count().reset_index()
hourly_negative = hourly_negative.pivot_table(index = 'hour', columns='year', values='negative_price')
hourly_negative = hourly_negative.fillna(0)
hourly_negative = hourly_negative.reset_index()

#####################
##Further Settings for the figure
#####################

#Setting the color list for the traces
color_list = colors.prices['years_fading']

#Create tick values and labels
tickvals = list(range(24, -1, -2))
ticktext = [f'{hour}:00' for hour in tickvals]

#calculate the max amount of hours with negative prices (for y-Axis range)
y_max = hourly_negative.set_index('hour')
y_max = y_max.max().max()
y_max = roundup_to_multiple(y_max, 10)


language_dict = {}

language_dict['de'] = {'title': 'Negative Preise im Tagesverlauf',
                        'legend_title': 'Jahr',
                        'x_axis': 'Uhrzeit',
                        'y_axis': 'Anzahl der Stunden mit negativem Preis',
                        'hovertext': '%{y} h',
                        }

language_dict['en'] = {'title': 'Negative prices in the course of the day',
                        'legend_title': 'Year',
                        'x_axis': 'Hour',
                        'y_axis': 'Number of hours with negative prices',
                        'hovertext': '%{y} h with negative prices',
                        }

language_dict['fr'] = {'title': 'Prix négatifs au cours de la journée',
                        'legend_title': 'Année',
                        'x_axis': 'Heure',
                        'y_axis': "Nombre d'heures avec des prix négatifs",
                        'hovertext': '%{y} h avec des prix négatifs'}



def create_neg_price_hourly (data, language):
                    

    #####################
    ##creating the figure
    #####################
    
    fig = go.Figure()

    #Setting the basic layout
    fig.update_layout(basic_layout_dict,
                    legend = legend_layout_dict_top_reverse)
    
    #create the traces for every year
    for i, year in enumerate(years): 
            
        #Setting the color for the trace - after seven years the last color is used (fading out)
        color = color_list[i] if i < len(color_list) else color_list[-1]

        fig.add_trace(go.Scatter(x=data.index, 
                                y=data[year], 
                                mode='lines', 
                                name= str(year),
                                line=dict(color = color,
                                            width = 2 if year != current_year else 3),
                                hovertemplate = language_dict[language]['hovertext'],)
                                )
            
    fig.data[0].name = f'{years[0]}*'
    
    fig.data = fig.data[::-1]

    
    

    #Set Layout
    fig.update_layout(
                    title = language_dict[language]['title'],
                    xaxis=dict(
                            type='linear',
                            title = language_dict[language]['x_axis'],
                            tickvals = tickvals,
                            ticktext = ticktext,
                            zeroline=True
                        ),
                    yaxis=dict(
                            title = language_dict[language]['y_axis'],
                            range = [0, y_max],
                            zeroline=True,),
                    hovermode = 'x unified'
                        )

    return fig
        


fig_neg_price_hourly_de = create_neg_price_hourly(hourly_negative, 'de')
fig_neg_price_hourly_en = create_neg_price_hourly(hourly_negative, 'en')
fig_neg_price_hourly_fr = create_neg_price_hourly(hourly_negative, 'fr')

fig_neg_price_hourly_de.write_html('docs/germany/figures/negative_price_hourly.de.html', include_plotlyjs='directory')
fig_neg_price_hourly_en.write_html('docs/germany/figures/negative_price_hourly.en.html', include_plotlyjs='directory')
fig_neg_price_hourly_fr.write_html('docs/germany/figures/negative_price_hourly.fr.html', include_plotlyjs='directory')


#%%################################################################################################
#Negative Prices Histogram 10€/MWh Bins
####################################################################################################

###############################
#Basic Settings for the Figure
###############################

#Setting the years (taking all years from the data)
years = smard_data['year'].unique()

#setting the initial year
initial_year = years[-1]

#Setting the price bins (-1e10 captures all below -100€)
bins = [-1e10, -100, -90, -80, -70, -60, -50, -40, -30, -20, -10, 0]

#setting tickvals for the x-axis
tickvals = list(range(0, -120, -10))
tickvals.reverse()

#calculating midpoints so the bars are placed between the ticks
midpoints = [(tickvals[i] + tickvals[i+1]) / 2 for i in range(len(tickvals)-1)]
#midpoints.reverse()

#setting the intervals for the dataframe
interval_separator = 'to' #labels for the rows will be '0€ to -10€', '-10€ to -20€' etc.
labels = [f'0€ {interval_separator} -10€', f'-10€ {interval_separator} -20€', f'-20€ {interval_separator} -30€', f'-30€ {interval_separator} -40€', f'-40€ {interval_separator} -50€', f'-50€ {interval_separator} -60€', f'-60€ {interval_separator} -70€', f'-70€ {interval_separator} -80€', f'-80€ {interval_separator} -90€' ,f'-90€ {interval_separator} -100€','<-100€']
labels.reverse()


#%%
#####################
#preparing data
#####################

#empty dataframe for collecting the counted bins of every year
prepared_data = pd.DataFrame(labels, columns=['price_bin'])
prepared_data = prepared_data[::-1] 

for i in years:

    #filter the data for negative prices and create bins
    df_negative = smard_wide.loc[:,[i]] 
    df_negative = df_negative[df_negative[i] < 0]     
    df_negative['price_bin'] = pd.cut(df_negative[i], bins=bins, labels=labels, right=False)

    #count the number of hours in each bin
    hist_data = df_negative['price_bin'].value_counts().sort_index()
    hist_data = hist_data.to_frame().reset_index()
    hist_data.columns = ['price_bin', i]

    prepared_data = pd.merge(prepared_data, hist_data, on='price_bin', how='left')

#calculating average Price per year
average_price = []
average_price = [smard_wide[year][smard_wide[year] < 0].mean() for year in years]
average_price = [round(price, 2) for price in average_price]

df_avg_price = pd.DataFrame({'years' : years,'average_price' : average_price})

#%%
##################
# Further Settings for the figure
##################

#calculate the y_max for the y-axis
max_y = max(prepared_data.iloc[:,1:].max())
max_y = roundup_to_multiple(max_y, 50)

language_dict = {}

language_dict['de'] = {'title': 'Histogramm der negativen Strompreise',
                        'xaxis_title': 'Preis, EUR/MWh',
                        'yaxis_title': 'Anzahl der Stunden',
                        'annotation_str1': 'Gesamt:',
                        'annotation_str2': 'Stunden mit negativem Preis',
                        'avg_price_str': 'Durchschnittspreis',
                        'price_interval_str': 'Preisintervall',
                        'interval_separator': 'bis',
                        'hours_str': 'Stunden'}

language_dict['en'] = {'title': 'Histogram of negative electricity prices',
                        'xaxis_title': 'Price, EUR/MWh',
                        'yaxis_title': 'Number of hours',
                        'annotation_str1': 'Total:',
                        'annotation_str2': 'hours with negative price',
                        'avg_price_str': 'Average price',
                        'price_interval_str': 'Price interval',
                        'interval_separator': 'to',
                        'hours_str': 'Hours'}

language_dict['fr'] = {'title': "Histogramme des prix négatifs de l'électricité",
                        'xaxis_title': 'Prix, EUR/MWh',
                        'yaxis_title': "Nombre d'heures",
                        'annotation_str1': 'Total:',
                        'annotation_str2': 'heures avec un prix négatif',
                        'avg_price_str': 'Prix moyen',
                        'price_interval_str': 'Intervalle de prix',
                        'interval_separator': 'à',
                        'hours_str': 'Heures'}


#%%

def create_negative_price_histogram(data, language, language_dict):

    fig = go.Figure()

    #Setting the basic layout
    fig.update_layout(basic_layout_dict_button,showlegend=False)

    #mirror labels for the hovertext
    
    separator = language_dict[language]['interval_separator']
    
    hover_labels = [f'0€ {separator} -10€', f'-10€ {separator} -20€', f'-20€ {separator} -30€', f'-30€ {separator} -40€', f'-40€ {separator} -50€', f'-50€ {separator} -60€', f'-60€ {separator} -70€', f'-70€ {separator} -80€', f'-80€ {separator} -90€' ,f'-90€ {separator} -100€', '<-100€']

    for i, year in enumerate(years):
        hovertext = [f"<br>{year}\
                    <br>{language_dict[language]['price_interval_str']}: {hover_labels[i]}\
                    <br>{language_dict[language]['hours_str']}: {data[year].values[i]}<extra></extra>"
                    for i in range(len(labels))]
        

        fig.add_trace(go.Bar(
            x=midpoints, 
            y=data[year],
            name=str(year),
            visible = True if year == initial_year else False,
            hovertemplate=hovertext,
            hoverlabel=dict(bgcolor='white', bordercolor='black'),
            marker=dict(color=colors.prices['single_price'])
        ))

    #adding the annotation for the initial year

    fig.add_annotation(dict(
                            text= f"{language_dict[language]['annotation_str1']} {sum(data[year])} {language_dict[language]['annotation_str2']}\
                                <br>{language_dict[language]['avg_price_str']} {df_avg_price[df_avg_price['years'] == initial_year]['average_price'].values[0]} EUR/MWh",
                            xref='paper', yref='paper', x=0.11, y=1.10, showarrow=False,
                            font=dict(family = "sans-serif,arial",size=14, color='black'),
                            align='left'
                        ))   

    #adding dropdown menu

    mirror_years = years[::-1] #mirror so that the current year is on top

    fig.update_layout(
        updatemenus=[
            dict(
                buttons=[
                    dict(
                        args=[{
                            'visible': [year == data.columns[i] for i in range(1, len(years)+1)]},{'annotations': [dict(text= f"{language_dict[language]['annotation_str1']} {sum(data[year])} {language_dict[language]['annotation_str2']}\ <br>{language_dict[language]['avg_price_str']} {df_avg_price[df_avg_price['years'] == year]['average_price'].values[0]} EUR/MWh",
                            #setting layout and position of the annotation
                            xref='paper', yref='paper', x=0.11, y=1.10, showarrow=False,
                            font=dict(size=14, color='black'),
                            align='left')]}
                        ],    
                        label=str(year),
                        method='update')
                    for year in mirror_years],
                direction='down', #dropdown menu opens down
                showactive=True,
                x=0.09, #x-position of the button 
                y=1.09, #y-position of the button
                borderwidth=1)
        ])

    #Setting of the ticks for the x-axis
    #i dont know why, but this is neccecary to let the labels start at 0
    ticks = ['0', '0', '-10', '-20', '-30', '-40', '-50', '-60', '-70', '-80', '-90', '<-100']
    shift_value = -10

    #Setting the layout
    fig.update_layout(
        title = dict(
            text = language_dict[language]['title'],
            x = 0.06),
        xaxis=dict(
            title = language_dict[language]['xaxis_title'],
            tickvals=[val + shift_value for val in bins],
            ticktext=ticks,
            tickmode='array'),
        yaxis=dict(
            title = language_dict[language]['yaxis_title'],
            range = [0, max_y]),
        bargap=0)

    return fig

fig_neg_price_histogram_de = create_negative_price_histogram(prepared_data, 'de', language_dict)
fig_neg_price_histogram_en = create_negative_price_histogram(prepared_data, 'en', language_dict)
fig_neg_price_histogram_fr = create_negative_price_histogram(prepared_data, 'fr', language_dict)

fig_neg_price_histogram_de.write_html('docs/germany/figures/negative_price_histo.de.html', include_plotlyjs='directory')
fig_neg_price_histogram_en.write_html('docs/germany/figures/negative_price_histo.en.html', include_plotlyjs='directory')
fig_neg_price_histogram_fr.write_html('docs/germany/figures/negative_price_histo.fr.html', include_plotlyjs='directory')

#%%################################################################################################ 
# Histogram Negative Stunden zusammenhängend
####################################################################################################

#####################
#Basic Settings for the Figure
#####################

# Creating Bins & Labels for the histogram - Last bin catches all values above the threshold (now 10)
bins = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, float('inf')]
labels = [f'{i}' for i in range(1, 10)] + ['10', '>10'] #Labels from 1 to 10 Hrs + >10hrs

#setting tickvals for the x-axis (here 11 ticks)
tickvals = list(range(1, 12, 1))

#creating the dataframe for the histogram data
prepared_data = pd.DataFrame(labels, columns=['price_bin'])

#Dict for the longest intervalls of every year (used in the annoation)
longest_intervalls = {}

#Setting the years (taking all years from the data)
years = smard_data['year'].unique()

#setting the initial year
initial_year = years[-1]

#####################
#preparing data
#####################

for year in years:
    #filter for negative prices, creates boolean values for occurence of negative prices
    negative_prices = smard_wide[year] < 0

    #creates groups of consecutive negative prices
    negative_groups = (negative_prices != negative_prices.shift()).cumsum()

    #counts the number of consecutive hours with negative prices
    negative_streaks = smard_wide[negative_prices].groupby(negative_groups).size()
    
    #Adding the longest Intervall of every year to the dict
    max_value = (max(negative_streaks))
    longest_intervalls[year] = max_value  

    #sort into bins
    negative_streaks_binned = pd.cut(negative_streaks, bins=bins, labels=labels, right=True)

    #frequency of bins
    binned_frequencies = negative_streaks_binned.value_counts().reindex(labels, fill_value=0)

    #add the frequency to the dataframe prepared data
    prepared_data[year] = binned_frequencies.values

#Setting the maximum of y-axis range
y_max = roundup_to_multiple(prepared_data.iloc[:, 1:].max().max(), 5)

#####################
#Further Settings for the figure
#####################

language_dict = {}

language_dict['de'] = {'title': 'Zusammenhängende Stunden mit negativem Strompreis - Länge in Stunden',
                        'xaxis_title': 'Intervalle mit negativem Preis, Länge der zusammenhängenden Stunden',
                        'yaxis_title': 'Anzahl an Intervallen im Jahr',
                        'hover_str': 'Anzahl Intervalle mit', 
                        'annotation_str1': 'Längstes Intervall:', 
                        'annotation_str2': 'Stunden' 
                        }

language_dict['en'] = {'title': 'Consecutive hours with negative price - Duration in Hours',
                        'xaxis_title': 'Duration in hours',
                        'yaxis_title': 'Number of intervals per year',
                        'hover_str': 'Number of intervals with',
                        'annotation_str1': 'Longest interval:', 
                        'annotation_str2': 'hours' 
                        }

language_dict['fr'] = {'title': 'Heures consécutives avec un prix négatif - Durée en heures',
                        'xaxis_title': 'Durée en heures',
                        'yaxis_title': "Nombre d'intervalle par an",
                        'hover_str': "Nombre d'intervalle avec", 
                        'annotation_str1': 'Intervalle le plus long:',
                        'annotation_str2': 'heures' 
                        }

#Setting the baisc layout for the figure
basic_layout_dict = dict(template = 'simple_white',
                     uniformtext_mode = 'hide',
                     font=dict(family = "sans-serif,arial", size=14, color='#000000'),
                    showlegend=False,
                    margin=dict(l=0, r=0, t=120, b=0, pad=0))

#setting the annotation Layout
annotation_layout_dict = dict(
                            xref='paper', yref='paper', x=0.10, y=1.08, showarrow=False,
                            font=dict(family = "sans-serif,arial",size=14, color='black'),
                            align='left'
                        )

#####################
##creating the figure
#####################

def consencutive_neg_hrs_histogram(data, language, tickvals, labels, y_max):
    
    fig = go.Figure()

    #Setting the basic layout from the basic layout dict above
    fig.update_layout(basic_layout_dict_button,
                      showlegend=False)

    #adding the traces for every year

    for year in years:
        #creating the hovertext for every trace an x-value
        hovertext = [f"<br>{year}<br>{language_dict[language]['hover_str']} {labels[i]}h: {data[year].iloc[::1].values[i]} <br><extra></extra>"
                        for i in range(len(labels))]

        fig.add_trace(go.Bar(
            x=tickvals, 
            y=data[year],
            name=str(year),
            visible = True if year == initial_year else False,
            hovertemplate=hovertext,
            hoverlabel=dict(bgcolor='white', bordercolor='black'),
            marker=dict(color=colors.prices['single_price'])
        ))

    #adding the annotation for the initial year
    fig.add_annotation(dict(text= f"{language_dict[language]['annotation_str1']} {longest_intervalls[initial_year]} {language_dict[language]['annotation_str2']}",
                            **annotation_layout_dict)) #layout settinmgs from the dict


    ###    
    #adding dropdown menu
    ###

    #Mirror the years so the current year is on top of the Drowdown
    mirror_years = years[::-1]
    
    fig.update_layout(
        updatemenus=[
            dict(
                buttons=[
                    dict(
                        args=[{'visible': [year == prepared_data.columns[i] for i in range(1, len(years)+1)]},
                        {'annotations': [dict(
                            text= f"{language_dict[language]['annotation_str1']} {longest_intervalls[year]} {language_dict[language]['annotation_str2']}",
                            **annotation_layout_dict)]},],
                        label=str(year),
                        method='update')
                    for year in mirror_years],
                direction='down', #dropdown menu opens down
                showactive=True, #show the active year
                x=0.09, 
                y=1.09,
                borderwidth=1,
            )
        ]
    )
    
    ###
    #Setting Title, labels and ranges
    ###
    fig.update_layout(
    title=dict(text = language_dict[language]['title'],
               x = 0.06),
    xaxis_title=language_dict[language]['xaxis_title'],
    yaxis=dict(
        title=language_dict[language]['yaxis_title'],
        range=[0, y_max]),
    xaxis=dict(
        categoryorder='array',
        categoryarray=tickvals,
        tickvals=tickvals,  # Place labels between bars
        ticktext=labels, 
        tickmode='array'
    ),
    bargap=0,  # Space between bars
    )
    

    return fig


fig_consecutive_hrs_neg_price_de = consencutive_neg_hrs_histogram(prepared_data, 'de', tickvals, labels, y_max)
fig_consecutive_hrs_neg_price_en = consencutive_neg_hrs_histogram(prepared_data, 'en', tickvals, labels, y_max)
fig_consecutive_hrs_neg_price_fr = consencutive_neg_hrs_histogram(prepared_data, 'fr', tickvals, labels, y_max)

fig_consecutive_hrs_neg_price_de.write_html('docs/germany/figures/negative_price_consecutive.de.html', include_plotlyjs='directory')
fig_consecutive_hrs_neg_price_en.write_html('docs/germany/figures/negative_price_consecutive.en.html', include_plotlyjs='directory')
fig_consecutive_hrs_neg_price_fr.write_html('docs/germany/figures/negative_price_consecutive.fr.html', include_plotlyjs='directory')

 
#%%################################################################################################
#Negative Prices by Technology (Generataion Shares & Slider)
####################################################################################################

smard_tech = copy.deepcopy(smard_data)

#####################
#preparing data
#####################

#setting the year for the figure
year = 2024

#setting the range and the stepsize for the slider
price_limits = list(range(-130, 1, 10)[::-1])

# Filter for negative prices in the selected year
smard_negative = smard_tech[smard_tech['da_price'] < 0]
smard_negative = smard_negative[(smard_negative['time'] > f'{year}-01-01') & (smard_negative['time'] < f'{year+1}-01-01')]
smard_negative = smard_negative.sort_values(by='da_price', ascending=False)

technologies = ['pv', 'wind_onshore','wind_offshore','lignite','hydro','conventional_misc','renewable_misc','biomass','hard_coal','pump_storage','gas']

#Function for calculating the generation shares of the technologies
def get_filtered_sums(price_limit):

    '''
    Function that calculates the generation shares of the technologies for a given price limit

    price_limit: int, the price limit for the negative prices
    technologies: list, list of the technologies to be considered 
    '''

    # Filter for negative prices below the price limit
    filtered_df = smard_negative[smard_negative['da_price'] <= price_limit] #filter for negative prices below the price limit
    negative_price_production =  {}
    
    #calculating the total production of every technology in GWh 
    total = float(sum(filtered_df[tech].sum()/1000 for tech in technologies))
    total = round(total, 2)
    negative_price_production['price_limit'] = price_limit
    negative_price_production['total'] = total
    
    #caculating the generation of every technology in GWh
    for tech in technologies:
        negative_price_production[tech] = float(filtered_df[tech].sum()/1000) #adds the generation of every technology in GWh
        negative_price_production[tech] = round(negative_price_production[tech], 2)
    for tech in technologies:
        negative_price_production[f'{tech}_share'] = round((negative_price_production[tech] / total)*100, 2) #calculates the share of every technology in percent
    df = pd.DataFrame(negative_price_production, index=[0])

    return df

prepared_data = pd.DataFrame()
for price in range(-130, 1, 10)[::-1]:
    df = get_filtered_sums(price)
    prepared_data = pd.concat([prepared_data, df], ignore_index=True)


prepared_data['price_limit'] = prepared_data['price_limit'].astype(str)

#####################
##Further Settings for the figure
#####################


#Dictionary for the language settings
language_dict = {}
language_dict['de'] = {'title': 'Anteil an Stromerzeugung bei negativen Strompreisen nach Technologie 2024', 
                        'xaxis': 'Technologie',
                        'yaxis': 'Erzeugungsanteil in %',
                        'price_limit': 'Preislimit: ',
                        technologies[0]: 'Photovoltaik',
                        technologies[1]: 'Onshore Wind',
                        technologies[2]: 'Offshore Wind',
                        technologies[3]: 'Braunkohle',
                        #technologies[4]: 'Kernenergie',
                        technologies[4]: 'Wasserkraft',
                        technologies[5]: 'Sonstige Konventionelle',
                        technologies[6]: 'Sonstige Erneuerbare',
                        technologies[7]: 'Biomasse',
                        technologies[8]: 'Steinkohle',
                        technologies[9]: 'Pumpspeicher',
                        technologies[10]: 'Gas',
                        'hovertext' : 'Erzeugungsanteil unter',
                        }

language_dict['en'] = {'title': 'Generation shares at negative prices by technology 2024',
                        'xaxis': 'Technology',
                        'yaxis': 'Generation share in %',
                        'price_limit': 'Price limit: ',
                        technologies[0]: 'Solar',
                        technologies[1]: 'Onshore Wind',
                        technologies[2]: 'Offshore Wind',
                        technologies[3]: 'Lignite',
                        #technologies[4]: 'Nuclear',
                        technologies[4]: 'Hydro',
                        technologies[5]: 'Other conventional',
                        technologies[6]: 'Other renewable',
                        technologies[7]: 'Biomass',
                        technologies[8]: 'Hard coal',
                        technologies[9]: 'Pump storage',
                        technologies[10]: 'Gas',
                        'hovertext' : 'Generation share at',
                        }

language_dict['fr'] = {'title': "Part de la production d'électricité en cas de prix négatifs par technologie 2024",
                        'xaxis': 'Technologie',
                        'yaxis': 'Part de la production en %',
                        'price_limit': 'Limite de prix: ',
                        technologies[0]: 'Photovoltaïque',
                        technologies[1]: 'Éolien terrestre',
                        technologies[2]: 'Éolien en mer',
                        technologies[3]: 'Lignite',
                        #technologies[4]: 'Nucléaire',
                        technologies[4]: 'Hydroélectrique',
                        technologies[5]: 'Autres conventionnels',
                        technologies[6]: 'Autres renouvelables',
                        technologies[7]: 'Biomasse',
                        technologies[8]: 'Charbon',
                        technologies[9]: 'Pompage-turbinage',
                        technologies[10]: 'Gaz',
                        'hovertext' : 'Part de la production à',
                        }

basic_layout_dict = dict(template = 'simple_white',
                        uniformtext_mode = 'hide',
                        font=dict(family = "sans-serif,arial", size=14, color='#000000'),
                        showlegend=False,
                        margin=dict(l=0, r=5, t=80, b=5, pad=0),
                        bargap=0.1
                        )


#####################
##creating the figure
#####################

def negative_price_tech_slider(data, language, price_limits, technologies, basic_layout_dict):
    fig = go.Figure()

    #Setting the layout
    fig.update_layout(basic_layout_dict)

    #adding the traces for every technology
    
    for i, tech in enumerate(technologies):
        fig.add_trace(go.Bar(
            x=[language_dict[language][tech]], #technologie auf x-achse    
            y = [data.loc[0, f'{tech}_share']],
            name=language_dict[language][tech],
            orientation='v',
            marker=dict(color=colors.tech[tech]),
            hovertemplate = (f"<br>{language_dict[language][tech]}<br>{language_dict[language]['hovertext']} {data['price_limit'].iloc[price_limits[0]]}€: %{{y}}%<br><extra></extra>"),
            ))

    #Setting Title, Labels and ticks
    fig.update_layout(
    title=language_dict[language]['title'],
    xaxis=dict(title=language_dict[language]['xaxis']),
    yaxis=dict(title=language_dict[language]['yaxis'], 
               range=[0, 100],
               title_standoff=5)
               )

    #setting the steps for the slider    
    steps = []
    for price_index, price_limit in enumerate(price_limits):
        step = dict(
            method='update',
            label=str(price_limit),
            args=[{
                'y': [[data.loc[price_index, f'{tech}_share']] for tech in technologies],
                #Hovertemplate sets the hovertext for every pricelimit
                'hovertemplate': [f"<br>{language_dict[language][tech]}<br>{language_dict[language]['hovertext']} {data['price_limit'].iloc[price_index]}€: %{{y}}%<br><extra></extra>" for tech in technologies]
            }]
        )
        steps.append(step)


    # configurate Slider
    fig.update_layout(
        sliders=[{
            'active': 0,
            'currentvalue': {'prefix': language_dict[language]['price_limit'], 'font': {'size': 16}, 'suffix': ' €/MWh'},
            'pad': {'t': 120},
            'steps': steps
        }]
    )
           

    return fig

    
fig_generation_shares_de = negative_price_tech_slider(prepared_data, 'de', price_limits, technologies, basic_layout_dict)
fig_generation_shares_en = negative_price_tech_slider(prepared_data, 'en', price_limits, technologies, basic_layout_dict) 
fig_generation_shares_fr = negative_price_tech_slider(prepared_data, 'fr', price_limits, technologies, basic_layout_dict) 

fig_generation_shares_de.write_html('docs/germany/figures/neg_price_generation_shares.de.html', include_plotlyjs='directory')
fig_generation_shares_en.write_html('docs/germany/figures/neg_price_generation_shares.en.html', include_plotlyjs='directory')   
fig_generation_shares_fr.write_html('docs/germany/figures/neg_price_generation_shares.fr.html', include_plotlyjs='directory')   



#%%###############################################################################
## Market values
##################################################################################

# Import data
mv_rel = pd.read_csv('docs/germany/data/market_values_relative.csv')
gen_shares = pd.read_csv('docs/germany/data/gen_shares.csv')

mv_rel['Date'] = pd.to_datetime(mv_rel['Date'],dayfirst=True)
gen_shares['Date'] = pd.to_datetime(gen_shares['Date'],dayfirst=True)

# Create figure
types = ['Wind onshore','Wind offshore','Solar']
technologies = ['wind_onshore', 'wind_offshore', 'pv']
colors_tech = [colors.tech[tech] for tech in technologies]

fig = make_subplots(cols=1,rows=3,shared_xaxes=True,vertical_spacing=0.1,y_title='Capture rate and generation share, %',subplot_titles=types)
for k in range(len(types)):
    fig.add_trace(go.Scatter(x=mv_rel['Date'],y=100*mv_rel[types[k]],name=types[k]+' capture rate',hovertemplate='%{y:.2f}%',line=dict(color='#3c435a',)),row=k+1,col=1)
    fig.add_trace(go.Scatter(x=gen_shares['Date'],y=100*gen_shares[types[k]],name=types[k]+' generation share',hovertemplate='%{y:.2f}%',fill='tozeroy',line=dict(color=colors_tech[k],)),row=k+1,col=1)
    fig.add_hline(y=100,line=dict(color='black',width=0.5,dash = 'dot'),row=k+1,col=1)
    fig.update_xaxes(linecolor='black',ticks='outside',row=k+1,col=1)
    fig.update_yaxes(linecolor='black',ticks='outside',row=k+1,col=1)
    #fig.update_yaxes(title=types[k],linecolor='black',ticks='outside',row=k+1,col=1)



fig.update_layout(
    #xaxis= dict(title='',tickangle=-45, tickmode='array',tickfont=dict(color='black'),linecolor='black',ticks='outside'),
    #yaxis = dict(title='',linecolor='black',ticks='outside'),
    plot_bgcolor='white',
    hovermode         = 'x unified',
    showlegend=False
)


fig.update_layout(
    legend=dict(
        x=0.01,
        y=1.4,
        title_font_family='arial',
        font=dict(
            family='arial',
            size=14,
            color='black'
        ),
        borderwidth=1,
    )
    
)

# translations
fig_DE = copy.deepcopy(fig)
fig_FR = copy.deepcopy(fig)
titel = ['Windkraft an Land','Windkraft auf See','Solar']
titres = ['Éolien terrestre','Éolien en mer','Solaire']
for k in range(3):
    fig_DE['layout']['annotations'][k]['text'] = titel[k]
    fig_FR['layout']['annotations'][k]['text'] = titres[k]
    fig_DE['data'][2*k]['name'] = titel[k] +' Marktwertfaktor'
    fig_DE['data'][2*k+1]['name'] = titel[k] +' Erzeugungsanteil'
    fig_FR['data'][2*k]['name'] = titres[k] +' taux de capture'
    fig_FR['data'][2*k+1]['name'] = titres[k] +' part de la génération'

fig_DE['layout']['annotations'][3]['text'] = 'Marktwertfaktor und Marktanteil, %'
fig_FR['layout']['annotations'][3]['text'] = 'Taux de capture et part de la génération'

fig_DE.write_html('docs/germany/figures/capture_rate.de.html')
fig.write_html('docs/germany/figures/capture_rate.en.html')
fig_FR.write_html('docs/germany/figures/capture_rate.fr.html')


fig2 = go.Figure()
for k in range(len(types)):
    #fig2.add_trace(go.Scatter(x=100*mv_rel[types[k]],y=100*gen_shares[types[k]],mode='none',name=types[k],customdata = gen_shares['Date'],hovertemplate='{customdata}'))
    fig2.add_trace(go.Scatter(x=100*gen_shares[types[k]],y=100*mv_rel[types[k]],mode='markers',name=titel[k],hovertemplate='Erzeugungsanteil: %{x:.2f}<br>Marktwertfaktor: %{y:.2f}',
    marker=dict(color=colors_tech[k]),legendgroup=k))
    X =sm.add_constant(100*gen_shares[types[k]])
    y = 100*mv_rel[types[k]]
    model = sm.OLS(y,X)
    res = model.fit()
    preds = res.predict(X)
    fig2.add_trace(go.Scatter(x=100*gen_shares[types[k]],y=preds,mode='lines',name=titel[k]+' regression',line=dict(color=color_list[k],dash='dash'),showlegend=False,legendgroup=k))
fig2.update_xaxes(title='Erzeugungsanteil, %',linecolor='black',ticks='outside',range=[0,50])
fig2.update_yaxes(title='Marktwertfaktor, %',linecolor='black',ticks='outside',range=[0,150])
fig2.update_layout(
    plot_bgcolor='white',
    hovermode         = 'closest',
    showlegend=True,
    legend=dict(
    x=0.8,
    y=1,
    title_font_family='arial',
    font=dict(
        family='arial',
        size=14,
        color='black'
    ),
    borderwidth=1
)
)

fig2_DE = copy.deepcopy(fig2)
fig2_EN = copy.deepcopy(fig2)
fig2_FR = copy.deepcopy(fig2)


fig2_EN['layout']['xaxis_title'] = 'Generation share, %'
fig2_EN['layout']['yaxis_title'] = 'Capture rate, %'
fig2_EN['data'][0]['name'] = 'Onshore wind'
fig2_EN['data'][1]['name'] = 'Offshore wind'
# Update hovertemplate for each trace in fig2_EN
for trace in fig2_EN['data']:
    trace['hovertemplate'] = 'Generation share: %{x:.2f}%<br>Capture rate: %{y:.2f}%<extra></extra>'
#fig2_EN.show()

#french translation
fig2_FR['layout']['xaxis_title'] = 'Part de la production, %'
fig2_FR['layout']['yaxis_title'] = 'Taux de capture, %'
fig2_FR['data'][0]['name'] = 'Éolien terrestre'
fig2_FR['data'][1]['name'] = 'Éolien en mer'
fig2_FR['data'][2]['name'] = 'Solaire'
# Update hovertemplate for each trace in fig2_FR
for trace in fig2_FR['data']:
    trace['hovertemplate'] = 'Part de la production: %{x:.2f}%<br>Taux de capture: %{y:.2f}%<extra></extra>'

fig2_DE.write_html('docs/germany/figures/capture_rate_scatter.de.html')
fig2_EN.write_html('docs/germany/figures/capture_rate_scatter.en.html')
fig2_FR.write_html('docs/germany/figures/capture_rate_scatter.fr.html')

#%%###############################################################################
## Revenues for PV
###############################################################################

years = smard_data['year'].unique()

color_list = colors.prices['years_fading_blue']

#Create tick values and labels (every second hour)
tickvals_hrs = list(range(24, -1, -2))

def create_pv_revs_charts(smard_data):
    # Get local time zone w/o daylight savings adjustment
    if smard_data['time'].dt.tz is None:
        smard_data['time'] = smard_data['time'].dt.tz_localize('UTC').dt.tz_convert('Etc/GMT-1')
    tmp = smard_data.copy()
    # create time vars
    tmp['year'] = tmp['time'].dt.year
    tmp['month'] = tmp['time'].dt.month
    tmp['hour'] = tmp['time'].dt.hour
    tmp['hour'] = tmp['hour'] + 1
    tmp['day'] = tmp['time'].dt.day
    tmp['season'] = np.where((tmp['month'] >= 4) & (tmp['month'] <= 9), 'summer', 'winter')

    # compute revenues
    tmp['revenue'] = tmp['da_price'] * tmp['pv']
    tmp['rev_noneg'] = tmp['pv'] * tmp['da_price'].apply(lambda x: x if x > 0 else 0)
    tmp_smr = tmp.groupby(['year','season','hour']).agg({'revenue':'sum','rev_noneg':'sum','pv':'mean'}).reset_index()
    tmp_smr['rev_total'] = tmp_smr.groupby(['year','season'])['revenue'].transform('sum')
    tmp_smr['rev_noneg_total'] = tmp_smr.groupby(['year','season'])['rev_noneg'].transform('sum')
    tmp_smr['rev_share'] = tmp_smr['revenue'] / tmp_smr['rev_total']
    tmp_smr['rev_noneg_share'] = tmp_smr['rev_noneg'] / tmp_smr['rev_noneg_total']

    # data for first plot
    df1 = tmp_smr[['year','season','hour','rev_share']]
    df1 = df1.pivot(index=['season','hour'], columns='year', values='rev_share')
    df1.reset_index(inplace=True)
    df1_summer = df1.query("season=='summer'")
    df1_winter = df1.query("season=='winter'")

    
    #create lists of years in the data for color iteration
    years_summer = (list(df1_summer.columns[2:-1]))[::-1] # all years except 2025 (slice [2:] to activate)    
    years_winter = (list(df1_winter.columns[2:-1]))[::-1] # all years except 2025 (slice [2:] to activate)    
    

    # first plot add traces
    fig = make_subplots(rows=1,cols=2,subplot_titles=['Summer','Winter'],shared_yaxes=True,x_title='Hour of the Day')
    for i, y in enumerate(years_summer):
        fig.add_trace(go.Scatter(
            x=df1_summer['hour'],y=100*df1_summer[y],name=y,hovertemplate = '%{y:.2f}%', 
            line_color=color_list[i] if i < len(color_list) else color_list[-1],
            legendgroup=y),row=1,col=1)
    for i, y in enumerate(years_winter):
        fig.add_trace(go.Scatter(x=df1_winter['hour'],y=100*df1_winter[y],name=y,showlegend=False,hovertemplate = '%{y:.2f}%',line_color = color_list[i] if i < len(color_list) else color_list[-1],legendgroup=y),row=1,col=2)
    
    #set layout
    fig.update_layout(yaxis_ticksuffix='%',legend={'traceorder':'reversed','x':0.9,'y':1},
                        hovermode = 'x unified',
                        template = 'simple_white',
                        barmode='relative',
                        margin = dict(l=0,r=0,b=50,t=50,pad=0), 
                    height=400) 
    
    fig.update_yaxes(title_text='Revenue share, %',row=1,col=1)

    fig.update_xaxes(
        tickvals=tickvals_hrs, 
        )

    #reverse the order of the traces to have the latest year on top
    fig.data = fig.data[::-1] 

    # data for second plot
    df2 = tmp_smr.query("season=='summer'")
    df2_2024 = df2.query('year==2024')
    df2_2023 = df2.query('year==2023')

    # second figure

    fig2 = make_subplots(rows=1,cols=2,subplot_titles=['2023','2024'],shared_yaxes=True,x_title='Hour of the Day')

    fig2.add_trace(go.Scatter(x=df2_2024['hour'],y=100*df2_2023['rev_share'],hovertemplate = '%{y:.2f}%', name='Actual',line=dict(color=color_list[0],dash='solid'),legendgroup='solid'),row=1,col=1)
    fig2.add_trace(go.Scatter(x=df2_2024['hour'],y=100*df2_2024['rev_share'],hovertemplate = '%{y:.2f}%', showlegend=False,name='2024 actual',line = dict(color=color_list[0],dash='solid'),legendgroup='solid'),row=1,col=2)

    fig2.add_trace(go.Scatter(x=df2_2024['hour'],y=100*df2_2023['rev_noneg_share'],hovertemplate = '%{y:.2f}%', name='No negative prices',line = dict(color=color_list[0],dash='dash'),legendgroup='dash'),row=1,col=1)
    fig2.add_trace(go.Scatter(x=df2_2024['hour'],y=100*df2_2024['rev_noneg_share'],hovertemplate = '%{y:.2f}%', showlegend=False,name='2024 no negative prices',line = dict(color=color_list[0],dash='dash'),legendgroup='dash'),row=1,col=2)
    fig2.update_layout(yaxis_ticksuffix='%',
                        hovermode = 'x unified',
                        template = 'simple_white',
                        barmode='relative',
                        margin = dict(l=0,r=0,b=50,t=50,pad=0),       
                        legend = dict(x=0.8,y=1),          
                    height=400) 
    fig2.update_yaxes(title_text='Revenue share in summer months, %',row=1,col=1)
    
    #set tickvals
    fig2.update_xaxes(tickvals=tickvals_hrs)
    
    return fig, fig2

fig_year,fig_cf = create_pv_revs_charts(smard_data)
#fig_year.write_html('docs/germany/figures/pv_revenue_year.en.html')
#fig_cf.write_html('docs/germany/figures/pv_revenue_cf.en.html')

# German translation
fig_year_DE = copy.deepcopy(fig_year)
fig_cf_DE = copy.deepcopy(fig_cf)

fig_year_DE['layout']['yaxis']['title']['text'] = 'Erlösanteil, %'
fig_year_DE['layout']['annotations'][0]['text'] = 'Sommer'
fig_year_DE['layout']['annotations'][2]['text'] = 'Stunde des Tages'

fig_cf_DE['layout']['yaxis']['title']['text'] = 'Erlösanteil in den Sommermonaten, %'
fig_cf_DE['layout']['annotations'][2]['text'] = 'Stunde des Tages'

fig_cf_DE['data'][0]['name'] = 'Realisiert'
fig_cf_DE['data'][1]['name'] = 'Realisiert'
fig_cf_DE['data'][2]['name'] = 'Ohne negative Preise'
fig_cf_DE['data'][3]['name'] = 'Ohne negative Preise'

# French translation
fig_year_FR = copy.deepcopy(fig_year)
fig_cf_FR = copy.deepcopy(fig_cf)

fig_year_FR['layout']['yaxis']['title']['text'] = 'Part des revenus, %'
fig_year_FR['layout']['annotations'][0]['text'] = 'Été'
fig_year_FR['layout']['annotations'][1]['text'] = 'Hiver'
fig_year_FR['layout']['annotations'][2]['text'] = 'Heure de la journée'
fig_cf_FR['layout']['yaxis']['title']['text'] = "Part des revenus pendant les mois d'été, %"
fig_cf_FR['layout']['annotations'][2]['text'] = 'Heure de la journée'

fig_cf_FR['data'][0]['name'] = 'Réalisé'
fig_cf_FR['data'][1]['name'] = 'Réalisé'
fig_cf_FR['data'][2]['name'] = 'Sans prix négatifs'
fig_cf_FR['data'][3]['name'] = 'Sans prix négatifs'


fig_year_DE.write_html('docs/germany/figures/pv_revs_year.de.html')
fig_year.write_html('docs/germany/figures/pv_revs_year.en.html')
fig_year_FR.write_html('docs/germany/figures/pv_revs_year.fr.html')

fig_cf_DE.write_html('docs/germany/figures/pv_revs_cf.de.html')
fig_cf.write_html('docs/germany/figures/pv_revs_cf.en.html')
fig_cf_FR.write_html('docs/germany/figures/pv_revs_cf.fr.html')


# %%################################################################################################
#  Monthly average PV market values and counterfactual MVs w/o negative prices
####################################################################################################

def monthly_avg_cf(smard_data,start_year):
    tmp = copy.deepcopy(smard_data)
    tmp['year'] = tmp.time.dt.year
    tmp['month'] = tmp.time.dt.month
    tmp['rev'] = tmp['da_price'] * tmp['pv']
    tmp['rev_noneg'] = tmp['pv'] * tmp['da_price'].apply(lambda x: x if x > 0 else 0)

    tmp = tmp.groupby(['year','month']).agg(dict(rev='sum',rev_noneg='sum',pv='sum')).reset_index()
    tmp['rev'] = tmp['rev']/tmp['pv']
    tmp['rev_noneg'] = tmp['rev_noneg']/tmp['pv']
    tmp['month_year'] = tmp['month'].astype(str) + '-' + tmp['year'].astype(str)

    tmp2 = tmp.query('year>=@start_year')
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=tmp2['month_year'],y=tmp2['rev'],name='Actual',line=dict(color=color_list[1]),hovertemplate='%{y:.2f} EUR/MWh'))
    fig.add_trace(go.Scatter(x=tmp2['month_year'],y=tmp2['rev_noneg'],name='No negative prices',line=dict(color=color_list[0],dash='dash'),hovertemplate='%{y:.2f} EUR/MWh'))
    fig.update_layout(
                            hovermode = 'x unified',
                            template = 'simple_white',
                            barmode='relative',
                            margin = dict(l=0,r=0,b=50,t=50,pad=0),       
                            legend = dict(x=0.8,y=1),          
                        height=400) 
    max_price = tmp2['rev'].max() + 10
    fig.update_yaxes(title_text='Monthly average PV market value, EUR/MWh',range=[0,max_price])
    return fig

fig_monthly_avg_pv_cf = monthly_avg_cf(smard_data, 2023)

fig_monthly_avg_pv_cf_DE = copy.deepcopy(fig_monthly_avg_pv_cf)

fig_monthly_avg_pv_cf_DE['layout']['yaxis']['title']['text'] = 'Monatlicher Durchschnitts-PV-Marktwert, EUR/MWh'
fig_monthly_avg_pv_cf_DE['data'][0]['name'] = 'Realisiert'
fig_monthly_avg_pv_cf_DE['data'][1]['name'] = 'Ohne negative Preise'


fig_monthly_avg_pv_cf_DE.write_html('docs/germany/figures/pv_monthly_avg_cf.de.html')
fig_monthly_avg_pv_cf.write_html('docs/germany/figures/pv_monthly_avg_cf.en.html')

