#%% Create and export figures for DIW Strommarkt Explainer

# Import packages

from pathlib import Path
import plotly.graph_objs as go

# Execute main script
from oet_main_de import *

# Create path
Path("docs/figures/export/diw_explainer").mkdir(parents=True, exist_ok=True)

#%% Abbildung Strompreise

strompreise =  go.Figure(fig_hh_elec_tariff_de)

# Remove title
strompreise['layout']['title']['text'] = ""

# Change y-axis
strompreise['layout']['yaxis']['title']['text'] = "Haushaltsstrompreise [Cent/kWh]"

# Change color
strompreise['data'][0]['marker']['color'] = '#0077B6'

# Add total
strompreise.add_trace(go.Scatter(
    x    = hh_elec_tariff_data['date'],
    y    = hh_elec_tariff_data['total'],
    text = hh_elec_tariff_data['total'],
    mode = 'text',
    textposition='top center',
    textfont=dict(
        size=12,
    ),
    showlegend=False
))

# Add gridlines
strompreise.update_yaxes(showgrid=True)

# Adapt layout
strompreise = strompreise.update_layout(width = 1000, height = 430,
                                        #legend = {'y':-.4},
                                        margin=dict(l=0, r=0, t=0, b=0, pad=0))
# Export
strompreise.write_image('docs/figures/export/diw_explainer/HH_strompreise.png', scale = 5)

#%%
strompreise

#%% Abbildung Gasimporte

gasimporte =  go.Figure(fig_gas_de)

# Adapt layout
gasimporte = gasimporte.update_layout(title = "",
                                      width = 1000, height = 400,
                                      legend = {'traceorder':'grouped',
                                                'orientation':'v',
                                                'x': 1, 'y': 1,
                                                'yanchor': 'top',
                                                'xanchor': 'left'},
                                      margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Show only >2021
gasimporte.update_xaxes(range=['2021-01-01','2022-08-31'],
                        dtick="M1",
                        tickformat="%m\n%Y",
                        ticklabelmode="period")

# Export
gasimporte.write_image('docs/figures/export/diw_explainer/gasimporte.png', scale = 5)

