#%% Create figures for PoKo 22/12 ##################################################################

# WARNING: run with IPython only

# Import packages

import os
from pathlib import Path
import plotly.graph_objs as go

#%% Change working directory
os.chdir('../../../')

# Run script
from create_figures_DE import *

# Define export path
export_path = "docs/figures/poko/2212/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

#%% Define functions ################

def transform_main_functions(input):
    
    fig = go.Figure(input)

    fig.layout.title.text = None

    fig.layout.legend.orientation = 'v'
    fig.layout.legend.y = 0.65
    fig.layout.legend.x = 0.02
    fig.layout.legend.borderwidth = 1
    fig.layout.legend.font.size = 14

    fig.layout.width  = 860
    fig.layout.height = 430

    fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
    fig.layout.updatemenus[0].buttons = None

    # Switch on visibility
    fig.data[1].visible = False
    fig.data[0].legendgrouptitle.text = None

    fig.data[2].legendgrouptitle.text = None
    fig.data[2].legendgrouptitle.font.size = 16
    fig.data[2].visible = None
    fig.data[2].name = "12-Monats-Trend"
    
    # Remove from legend
    fig.data[5].visible = False
    fig.data[6].visible = False
    fig.data[7]['legendgrouptitle']['text'] = None
    fig.data[7].legendgrouptitle.font.size = 16
    fig.data[7].visible = None
    fig.data[7].name = "Ariadneszenarien"

    return fig
    
    # Write
    #fig.write_image(os.path.join(export_path,filename), scale = 5)

#%% PV #############################################################################################

fig = transform_main_functions(fig_pv_de)
fig.write_image(os.path.join(export_path,"1_pv.png"), scale = 5)

#%% WIND ON ########################################################################################

fig = transform_main_functions(fig_windon_de)
fig.write_image(os.path.join(export_path,"2_windon.png"), scale = 5)

#%% WIND ON LAND ###################################################################################

fig = go.Figure(fig_wind_areas_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.write_image(os.path.join(export_path,"3_windareas.png"), scale = 5)

#%% WIND OFF #######################################################################################

fig = transform_main_functions(fig_windoff_de)
fig.write_image(os.path.join(export_path,"4_windoff.png"), scale = 5)

#%% SHARE ELEC #####################################################################################

fig = go.Figure(fig_rs_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.50
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
#fig.layout.legend.font.size = 12
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[1]['legendgrouptitle']['text'] = None

fig.data[3].visible = None
fig.data[4].visible = None
fig.data[4]['legendgrouptitle']['text'] = None
fig.data[4].name = "Trend 2017-2021"

fig.data[5].visible = False
fig.data[6].visible = False
fig.data[7].visible = None
fig.data[7]['legendgrouptitle']['text'] = None
fig.data[7].name = "Ariadneszenarien"

fig.write_image(os.path.join(export_path,"5_reselecshare.png"), scale = 5)

#%% HEAT PUMPS #####################################################################################

fig = go.Figure(fig_hp_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.63
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[2].legendgrouptitle.text = None

fig.data[3].visible = None
fig.data[3].name = "Trend 2017-2021"
fig.data[3]['legendgrouptitle']['text'] = None

fig.data[4].visible = False
fig.data[5].visible = False
fig.data[6].visible = None
fig.data[6].name = 'Ariadneszenarien'
fig.data[6]['legendgrouptitle']['text'] = None
fig.data[7].visible = False
fig.data[8].visible = False
fig.data[9].visible = False
fig.data[10].visible = False

fig.write_image(os.path.join(export_path,"6_heatpumps.png"), scale = 5)

#%% SHARE HEAT #####################################################################################

fig = go.Figure(fig_rs_heat_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[3].visible = None
fig.data[3].name    = "Trend 2017-2021"

fig.write_image(os.path.join(export_path,"7_heatshare.png"), scale = 5)

#%% BEV ############################################################################################

fig = go.Figure(fig_bev_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.57
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.yaxis.range = [0,16000000]

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[2]['legendgrouptitle']['text'] = None

fig.data[4].visible = False
fig.data[5].visible = None
fig.data[5].name = "12-Monats-Trend"
fig.data[5]['legendgrouptitle']['text'] = None

fig.data[6].visible = False
fig.data[7].visible = False
fig.data[8].visible = None
fig.data[8].name = "Ariadneszenarien"
fig.data[8]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"8_bevfleet.png"), scale = 5)

#%% BEV SHARE ######################################################################################

fig = go.Figure(fig_bev_adm_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.85
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.xaxis.range = ['2018-01', '2023-12']

fig.data[0]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"9_bevshare.png"), scale = 5)

#%% CP #############################################################################################

fig = go.Figure(fig_cs_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.50
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[1]['legendgrouptitle']['text'] = None
fig.data[5].visible = False
fig.data[6].visible = None
fig.data[6].name = "12-Monats-Trend"
fig.data[6]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"10_cs.png"), scale = 5)

#%% BEV per CP #####################################################################################

fig = go.Figure(fig_bev_per_cp_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.80
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.xaxis.range = ['2018-01', '2023-12']
fig.layout.yaxis.range = [0,100]

fig.data[0]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"11_bevpercs.png"), scale = 5)

#%% RAIL ###########################################################################################

fig = go.Figure(fig_rail_elec_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.04
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
#fig.layout.updatemenus[0].buttons = None

fig.layout.yaxis.range = [0,100]

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[3].visible = None
fig.data[3].name = "Trend 2017-2021"
fig.data[3]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"12_rail.png"), scale = 5)

#%% H2 #############################################################################################

fig = go.Figure(fig_h2_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None

fig.data[3].visible = False
fig.data[4].visible = False
fig.data[5].visible = None
fig.data[5].name = "Ariadneszenarien"
fig.data[5]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"13_h2.png"), scale = 5)

#%% H2 Status ######################################################################################

fig = go.Figure(fig_iea_h2_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 50}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14

fig.write_image(os.path.join(export_path,"14_h2status.png"), scale = 5)

#%% CON, year ######################################################################################

fig = go.Figure(fig_pec_figure_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.45
fig.layout.legend.x = 0.80
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[6].visible = False

fig.write_image(os.path.join(export_path,"15_fossilyearly.png"), scale = 5)

#%% CON, quar ######################################################################################

fig = go.Figure(fig_pec_3month_figure_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'h'
fig.layout.legend.y = -0.1
fig.layout.legend.x = 0.25
#fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"16_fossilquarterly.png"), scale = 5)

#%% GAS, monthly ###################################################################################

fig = go.Figure(fig_gas_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = .1
fig.layout.legend.x = 1.05
fig.layout.legend.borderwidth = 0
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"17_gasimports.png"), scale = 5)

#%% GAS, last 3 months #############################################################################

fig = go.Figure(fig_gas_3month_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = .15
fig.layout.legend.x = 1
#fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"18_gasimports3month.png"), scale = 5)

#%% gas1 ###########################################################################################

fig = go.Figure(fig_gas_consumption_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.y = 1
fig.layout.legend.x = 0.75
fig.layout.legend.font.size = 14

fig.layout.yaxis.range = [-25,30]
fig.layout.xaxis.position = 0.455
fig.write_image(os.path.join(export_path,"19_gas1.png"), scale = 5)

#%% gas2 ###########################################################################################

fig = go.Figure(fig_gas_savings_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.font.size = 14

fig.data[3].visible = None
fig.data[4].visible = None

fig.write_image(os.path.join(export_path,"20_gas2.png"), scale = 5)

#%% gas3 ###########################################################################################

fig = go.Figure(fig_gas_reached_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.y = 1
fig.layout.legend.x = 0.01
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14

fig.write_image(os.path.join(export_path,"21_gas3.png"), scale = 5)

#%% emissions sec ##################################################################################

fig = go.Figure(fig_emissions_sect_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.05
fig.layout.legend.x = 1
fig.layout.legend.font.size = 12

fig.data[15].visible = False

fig.write_image(os.path.join(export_path,"22_emissionssector.png"), scale = 5)

#%% emissions per elec #############################################################################

fig = go.Figure(fig_emissions_electr_figure_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.73
fig.layout.legend.font.size = 12
fig.layout.legend.borderwidth = 1

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[4].visible = False

fig.write_image(os.path.join(export_path,"23_emissionselec.png"), scale = 5)

#%% energy prices ##################################################################################

fig = go.Figure(fig_hh_elec_tariff_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.write_image(os.path.join(export_path,"24_elecprices.png"), scale = 5)

#%% 