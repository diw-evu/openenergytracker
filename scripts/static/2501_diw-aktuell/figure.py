#%% Import packages

import pandas as pd
import numpy as np

import plotly.graph_objects as go
from plotly.subplots import make_subplots

#%% 
# Load and aggregate data

def load_prepare(file_name):
    
    # Load data
    
    data = pd.read_csv("../../../docs/germany/data/"+file_name+".csv")
    data['date'] = pd.to_datetime(data['date'], format='%d.%m.%Y')
    data['year'] = data['date'].dt.year
    
    # Aggreat to yearly data
    
    data_year = data.groupby('year').agg({'add_gw': 'sum','add_plan_linear':'sum'}).reset_index()
    
    # Set plan values to NaN for years before 2022
    data_year.loc[data_year['year'] < 2022, 'add_plan_linear'] = np.nan
    
    # Add add_gw value of 2025 to 2024
    data_year.loc[data_year['year'] == 2024, 'add_gw'] += data_year.loc[data_year['year'] == 2025, 'add_gw'].values[0]
    
    # Set add_gw values of 2025 and beyond to NaN
    data_year.loc[data_year['year'] >= 2025, 'add_gw'] = np.nan
    
    return data_year

pv = load_prepare('pv')
wind = load_prepare('wind_onshore')

# %%

# Create figure

fig = make_subplots(rows=1, cols=2, subplot_titles=("Photovoltaik", "Windkraft an Land"))
fig.update_annotations(dict(font=dict(size=16,family='Arial')))

# Add traces

fig.add_trace(go.Bar(x=pv['year'], y=pv['add_gw'], marker_color="#FFAA33"), row=1, col=1)
fig.add_trace(go.Scatter(x=pv['year'], y=pv['add_plan_linear'], 
            mode='lines', line=dict(color='rgba(0,0,0,0.5)',dash="dot")), row=1, col=1)

fig.add_trace(go.Bar(x=wind['year'], y=wind['add_gw'], marker_color="#7393B3"), row=1, col=2)
fig.add_trace(go.Scatter(x=wind['year'], y=wind['add_plan_linear'], 
            mode='lines', line=dict(color='rgba(0,0,0,0.5)',dash="dot")), row=1, col=2)

fig.add_annotation(x=2024.4, y=17.3, text="Geplanter Ausbau<br>nach EEG 2023", showarrow=True, font=dict(color="rgba(0,0,0,0.75)", size = 13, family = "Arial"), arrowcolor="slategray", xref='x1', yref='y1', ax=-80, ay=-15)

fig.update_xaxes(
    linecolor="black",ticks='outside',range = [2016.5, 2030.5], dtick=1, tickangle=-45, 
    tickvals=wind['year'],  # Define the tick positions
    ticktext=[str(x) if i % 2 == 1 else '' for i, x in enumerate(wind['year'])])
fig.update_yaxes(linecolor="black",ticks='outside',showticklabels=True, gridcolor='lightgray', gridwidth =0.5, range = [0,23],   row=1, col=1, title=dict(text = "Netto-Zubau [GW]", font = dict(size=14,family='Arial')))
fig.update_yaxes(linecolor="black",ticks='outside',showticklabels=True, gridcolor='lightgray', gridwidth =0.5, range = [0,11.5], row=1, col=2)

fig.update_layout(
    #title_text="Zubau Photovoltaik und Wind Onshores in Deutschland",
    #template = 'simple_white',
    plot_bgcolor='white',
    font = dict(color='black',size=12,family='Arial'),
    width=600, height=400,
    margin=dict(l=0, r=0, t=50, b=0),
    showlegend=False,
)

#fig.add_annotation(
#            text="DRAFT",
#            textangle=-30,
#            #opacity=0.1,
#            font=dict(color="rgba(0,0,0,0.075)", size=100),
#            xref="paper",
#            yref="paper",
#            x=0.5,
#            y=0.5,
#            showarrow=False)

fig.write_html("figure.html")
fig.write_image("figure.png")
fig.write_image("figure.pdf")

fig.show()

#%%