#%% Create figures for Wirtschaftsdienst 24/06 #####################################################

# WARNING: run with IPython only

# Import packages

import os
from pathlib import Path
import plotly.graph_objs as go

#%% Change working directory
os.chdir('../../../')

# Run script
from create_figures_DE import *

# Define export path
#export_path = os.path.join("D:",os.sep,"OneDrive - DIW Berlin","Studium","PhD","TdE","03b_Activities","Ampel-Monitor","Wirtschaftsdienst 2024")

export_path = os.path.join("C:",os.sep,"Alex","OneDrive - DIW Berlin","Studium","PhD","TdE","03b_Activities","Ampel-Monitor","Wirtschaftsdienst 2024")

# Create folder
#Path(export_path).mkdir(parents=True, exist_ok=True)

#%%

fig_wd_1 = make_subplots(
    column_titles = ["Windkraft an Land","Photovoltaik"],
    rows=2, cols=2,
    row_heights=[0.75, 0.25],
    vertical_spacing=0.1,)

# Figure 1: Wind Bestand
colnr = 1
rownr = 1

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["actual"], 
    fill='tozeroy',
    fillcolor = "rgba(144, 173, 198, 1)",
    line_color = "rgba(144, 173, 198, 0.75)",
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["trend_12months"], 
    fill='tozeroy',
    fillcolor = "rgba(144, 173, 198, 0.1)",
    line_color = "rgba(144, 173, 198, 0.25)",
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["plan_linear"], 
    line_color = "rgba(51, 54, 82, 0.5)",
    line_dash = "dot",
    line_width = 1.5,
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["plan"], 
    text = dict_windon_de["base"]["data"]["plan"].apply(lambda x: '{0:1.0f} GW'.format(x)),
    mode = "markers+text",
    textposition="top left",
    line_color = "rgba(51, 54, 82, 1)",
    col=colnr, row=rownr)

#test_data = dict_windon_de["base"]["data"][["date","plan"]].dropna()
#
#fig_wd_1.add_scatter(
#    x=test_data["date"],
#    y=test_data["plan"], 
#    mode = "markers+lines",
#    marker_color = "rgba(51, 54, 82, 0.75)",
#    line_width = 1,
#    line_dash = "dot",
#    marker=dict(
#        symbol="arrow",
#        size=12,
#        angleref="previous"
#    ),
#    col=colnr, row=rownr)

# Figure 2: PV Bestand
colnr = 2
rownr = 1

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["actual"], 
    fill='tozeroy',
    fillcolor = "rgba(252, 186, 3, 0.50)",
    line_color = "rgba(252, 186, 3, 1)",
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["trend_12months"], 
    fill='tozeroy',
    fillcolor = "rgba(252, 186, 3, 0.1)",
    line_color = "rgba(252, 186, 3, 0.25)",
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["plan_linear"], 
    line_color = "rgba(51, 54, 82, 0.5)",
    line_dash = "dot",
    mode = "lines",
    line_width = 1.5,
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["plan"], 
    text = dict_pv_de["base"]["data"]["plan"].apply(lambda x: '{0:1.0f} GW'.format(x)),
    mode = "markers+text",
    textposition="top left",
    line_color = "rgba(51, 54, 82, 1)",
    col=colnr, row=rownr)

#test_data = dict_pv_de["base"]["data"][["date","plan"]].dropna()
#
#fig_wd_1.add_scatter(
#    x=test_data["date"],
#    y=test_data["plan"], 
#    mode = "markers+lines",
#    marker_color = "rgba(51, 54, 82, 0.75)",
#    line_width = 1,
#    line_dash = "dot",
#    marker=dict(
#        symbol="arrow",
#        size=12,
#        angleref="previous"
#    ),
#    col=colnr, row=rownr)

# Figure 3: Wind Zubau
colnr = 1
rownr = 2

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["add_gw"], 
    marker_color="rgba(144, 173, 198, 1)",
    fill='tozeroy',
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_windon_de["base"]["data"]["date"],
    y=dict_windon_de["base"]["data"]["add_plan_linear"], 
    mode = "lines",
    line_width = 1.5,
    line_color = "rgba(51, 54, 82, 0.75)",
    line_dash = "dot",
    col=colnr, row=rownr)

# Figure 4: PV Zubau
colnr = 2
rownr = 2

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["add_gw"], 
    marker_color="rgba(252, 186, 3, 1)",
    fill='tozeroy',
    col=colnr, row=rownr)

fig_wd_1.add_scatter(
    x=dict_pv_de["base"]["data"]["date"],
    y=dict_pv_de["base"]["data"]["add_plan_linear"], 
    mode = "lines",
    line_width = 1.5,
    line_color = "rgba(51, 54, 82, 0.75)",
    line_dash = "dot",
    col=colnr, row=rownr)


fig_wd_1.update_xaxes(showticklabels = False, row=1, col=1)
fig_wd_1.update_xaxes(showticklabels = False, row=1, col=2)
fig_wd_1.update_xaxes(range=["2020","2031-03-31"],tickformat="%Y",dtick="M24")#,dtick=1)

fig_wd_1.update_yaxes(range=[0,125], row=1, col=1, title = "Bestand (Gigawatt)", dtick = 25, titlefont=dict(size=13,family="Arial"))
fig_wd_1.update_yaxes(range=[0,250], row=1, col=2)

fig_wd_1.update_yaxes(range=[0,1], row=2, col=1, title = "Zubau (Gigawatt)",dtick = 0.25, titlefont=dict(size=13,family="Arial"))
fig_wd_1.update_yaxes(range=[0,2], row=2, col=2, dtick = 0.5)

#fig_wd_1.add_annotation(text="Gigawatt", xref="paper", yref="paper", x=-0.#16, y=0.5, showarrow=False,textangle=270,font=dict(size=14,family="Arial"))

fig_wd_1.add_annotation(text="Bestand", row = 1, col = 1,
                        x="2022",y=25,
                        arrowhead=2,
                        showarrow=False,
                        #xshift=15,yshift=-15,
                        #ax = 5,
                        arrowcolor = "rgba(244, 247, 249,1)",
                        arrowwidth = 1,
                        font=dict(size=14,family="Arial",
                                  color = "rgb(244, 247, 249)"))

fig_wd_1.add_annotation(text="Zubau", row = 2, col = 1,
                        x="2026",y=0.15,
                        arrowhead=2,
                        showarrow=False,
                        xshift=-25,
                        #yshift=-15,
                        #ax = 5,
                        arrowcolor = "rgb(244, 247, 249)",
                        arrowwidth = 1,
                        font=dict(size=14,family="Arial",
                                  color = "rgb(144, 173, 198)"))

fig_wd_1.add_annotation(text="Trend", row = 1, col = 1,
                        x="2028",y=25,arrowhead=2,
                        #xshift=15,yshift=-50,
                        ay = 25,
                        arrowwidth = 1,
                        showarrow=False,
                        #ax = 5,ay =-15,
                        arrowcolor = "rgba(144, 173, 198, 1)",
                        font=dict(size=14,family="Arial",
                                  color = "rgba(144, 173, 198, 1)"))

fig_wd_1.add_annotation(text="Regierungsziele", row = 1, col = 1,
                        x="2028",y=75,arrowhead=2,
                        textangle=-25,
                        showarrow=False,
                        xshift=30,
                        yshift=30,
                        #ax = -55, #ay = 100,
                        arrowwidth = 1.5,
                        arrowcolor = "rgba(51, 54, 82, 1)",
                        font=dict(size=14,family="Arial",
                                  color = "rgba(51, 54, 82, 1)"))

fig_wd_1.add_annotation(text="Zubaupläne der Regierung", row = 2, col = 1,
                        x="2028",y=0.80,arrowhead=2,
                        #textangle=-43,
                        showarrow=False,
                        #xshift=-10,
                        #yshift=5,
                        #ax = -55, #ay = 100,
                        arrowwidth = 1.5,
                        arrowcolor = "rgba(51, 54, 82, 1)",
                        font=dict(size=14,family="Arial",
                                  color = "rgba(51, 54, 82, 1)"))

fig_wd_1.update_layout(
    template = "simple_white",
    showlegend = False,
    width = 800,   height = 400,
    margin = dict(l=0, r=0, t=25, b=0, pad = 0),
    font = dict(size = 12, family = "Arial"),
)

fig_wd_1.write_image(os.path.join(export_path,"Abb1.pdf"), scale = 5)

fig_wd_1.show()

#%%

type(fig_wd_1['data'][0])

#%% Define functions ################

def transform_main_functions(input):
    
    fig = go.Figure(input)

    fig.layout.title.text = None

    #fig.layout.legend.orientation = 'v'
    #fig.layout.legend.y = 0.65
    #fig.layout.legend.x = 0.02
    #fig.layout.legend.borderwidth = 1
    #fig.layout.legend.font.size = 14
#
    #fig.layout.width  = 860
    #fig.layout.height = 430

    fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
    fig.layout.updatemenus[0].buttons = None

    # Switch on visibility
    fig.data[1].visible = False
    fig.data[0].legendgrouptitle.text = None

    fig.data[2].legendgrouptitle.text = None
    fig.data[2].legendgrouptitle.font.size = 16
    fig.data[2].visible = None
    fig.data[2].name = "12-Monats-Trend"
    
    # Remove from legend
    fig.data[5].visible = False
    fig.data[6].visible = False
    fig.data[7]['legendgrouptitle']['text'] = None
    fig.data[7].legendgrouptitle.font.size = 16
    fig.data[7].visible = None
    fig.data[7].name = "Ariadneszenarien"

    return fig
    
    # Write
    #fig.write_image(os.path.join(export_path,filename), scale = 5)

#%% PV #############################################################################################

fig = transform_main_functions(fig_pv_de)
fig.write_image(os.path.join(export_path,"1_pv.png"), scale = 5)

#%% WIND ON ########################################################################################

fig = transform_main_functions(fig_windon_de)
fig.write_image(os.path.join(export_path,"2_windon.png"), scale = 5)

#%% WIND ON LAND ###################################################################################

fig = go.Figure(fig_wind_areas_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.write_image(os.path.join(export_path,"3_windareas.png"), scale = 5)

#%% WIND OFF #######################################################################################

fig = transform_main_functions(fig_windoff_de)
fig.write_image(os.path.join(export_path,"4_windoff.png"), scale = 5)

#%% SHARE ELEC #####################################################################################

fig = go.Figure(fig_rs_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.50
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
#fig.layout.legend.font.size = 12
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[1]['legendgrouptitle']['text'] = None

fig.data[3].visible = None
fig.data[4].visible = None
fig.data[4]['legendgrouptitle']['text'] = None
fig.data[4].name = "Trend 2017-2021"

fig.data[5].visible = False
fig.data[6].visible = False
fig.data[7].visible = None
fig.data[7]['legendgrouptitle']['text'] = None
fig.data[7].name = "Ariadneszenarien"

fig.write_image(os.path.join(export_path,"5_reselecshare.png"), scale = 5)

#%% HEAT PUMPS #####################################################################################

fig = go.Figure(fig_hp_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.63
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[2].legendgrouptitle.text = None

fig.data[3].visible = None
fig.data[3].name = "Trend 2017-2021"
fig.data[3]['legendgrouptitle']['text'] = None

fig.data[4].visible = False
fig.data[5].visible = False
fig.data[6].visible = None
fig.data[6].name = 'Ariadneszenarien'
fig.data[6]['legendgrouptitle']['text'] = None
fig.data[7].visible = False
fig.data[8].visible = False
fig.data[9].visible = False
fig.data[10].visible = False

fig.write_image(os.path.join(export_path,"6_heatpumps.png"), scale = 5)

#%% SHARE HEAT #####################################################################################

fig = go.Figure(fig_rs_heat_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[3].visible = None
fig.data[3].name    = "Trend 2017-2021"

fig.write_image(os.path.join(export_path,"7_heatshare.png"), scale = 5)

#%% BEV ############################################################################################

fig = go.Figure(fig_bev_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.57
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.yaxis.range = [0,16000000]

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[2]['legendgrouptitle']['text'] = None

fig.data[4].visible = False
fig.data[5].visible = None
fig.data[5].name = "12-Monats-Trend"
fig.data[5]['legendgrouptitle']['text'] = None

fig.data[6].visible = False
fig.data[7].visible = False
fig.data[8].visible = None
fig.data[8].name = "Ariadneszenarien"
fig.data[8]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"8_bevfleet.png"), scale = 5)

#%% BEV SHARE ######################################################################################

fig = go.Figure(fig_bev_adm_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.85
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.xaxis.range = ['2018-01', '2023-12']

fig.data[0]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"9_bevshare.png"), scale = 5)

#%% CP #############################################################################################

fig = go.Figure(fig_cs_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.50
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[1]['legendgrouptitle']['text'] = None
fig.data[5].visible = False
fig.data[6].visible = None
fig.data[6].name = "12-Monats-Trend"
fig.data[6]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"10_cs.png"), scale = 5)

#%% BEV per CP #####################################################################################

fig = go.Figure(fig_bev_per_cp_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.80
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.layout.xaxis.range = ['2018-01', '2023-12']
fig.layout.yaxis.range = [0,100]

fig.data[0]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"11_bevpercs.png"), scale = 5)

#%% RAIL ###########################################################################################

fig = go.Figure(fig_rail_elec_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.04
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
#fig.layout.updatemenus[0].buttons = None

fig.layout.yaxis.range = [0,100]

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[3].visible = None
fig.data[3].name = "Trend 2017-2021"
fig.data[3]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"12_rail.png"), scale = 5)

#%% H2 #############################################################################################

fig = go.Figure(fig_h2_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None

fig.data[3].visible = False
fig.data[4].visible = False
fig.data[5].visible = None
fig.data[5].name = "Ariadneszenarien"
fig.data[5]['legendgrouptitle']['text'] = None

fig.write_image(os.path.join(export_path,"13_h2.png"), scale = 5)

#%% H2 Status ######################################################################################

fig = go.Figure(fig_iea_h2_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 50}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.65
fig.layout.legend.x = 0.02
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14

fig.write_image(os.path.join(export_path,"14_h2status.png"), scale = 5)

#%% CON, year ######################################################################################

fig = go.Figure(fig_pec_figure_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.45
fig.layout.legend.x = 0.80
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[6].visible = False

fig.write_image(os.path.join(export_path,"15_fossilyearly.png"), scale = 5)

#%% CON, quar ######################################################################################

fig = go.Figure(fig_pec_3month_figure_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'h'
fig.layout.legend.y = -0.1
fig.layout.legend.x = 0.25
#fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"16_fossilquarterly.png"), scale = 5)

#%% GAS, monthly ###################################################################################

fig = go.Figure(fig_gas_de)

fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = .1
fig.layout.legend.x = 1.05
fig.layout.legend.borderwidth = 0
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"17_gasimports.png"), scale = 5)

#%% GAS, last 3 months #############################################################################

fig = go.Figure(fig_gas_3month_de)
fig.layout.title.text = None
fig.layout.legend.orientation = 'v'
fig.layout.legend.y = .15
fig.layout.legend.x = 1
#fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}
# fig.layout.updatemenus[0].buttons = None

fig.write_image(os.path.join(export_path,"18_gasimports3month.png"), scale = 5)

#%% gas1 ###########################################################################################

fig = go.Figure(fig_gas_consumption_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.y = 1
fig.layout.legend.x = 0.75
fig.layout.legend.font.size = 14

fig.layout.yaxis.range = [-25,30]
fig.layout.xaxis.position = 0.455
fig.write_image(os.path.join(export_path,"19_gas1.png"), scale = 5)

#%% gas2 ###########################################################################################

fig = go.Figure(fig_gas_savings_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.font.size = 14

fig.data[3].visible = None
fig.data[4].visible = None

fig.write_image(os.path.join(export_path,"20_gas2.png"), scale = 5)

#%% gas3 ###########################################################################################

fig = go.Figure(fig_gas_reached_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.y = 1
fig.layout.legend.x = 0.01
fig.layout.legend.borderwidth = 1
fig.layout.legend.font.size = 14

fig.write_image(os.path.join(export_path,"21_gas3.png"), scale = 5)

#%% emissions sec ##################################################################################

fig = go.Figure(fig_emissions_sect_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.05
fig.layout.legend.x = 1
fig.layout.legend.font.size = 12

fig.data[15].visible = False

fig.write_image(os.path.join(export_path,"22_emissionssector.png"), scale = 5)

#%% emissions per elec #############################################################################

fig = go.Figure(fig_emissions_electr_figure_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.layout.legend.orientation = 'v'
fig.layout.legend.y = 0.70
fig.layout.legend.x = 0.73
fig.layout.legend.font.size = 12
fig.layout.legend.borderwidth = 1

fig.data[0]['legendgrouptitle']['text'] = None
fig.data[4].visible = False

fig.write_image(os.path.join(export_path,"23_emissionselec.png"), scale = 5)

#%% energy prices ##################################################################################

fig = go.Figure(fig_hh_elec_tariff_de)
fig.layout.title.text = None
fig.layout.width  = 860
fig.layout.height = 430
fig.layout.margin = {'b': 0, 'l': 0, 'pad': 0, 'r': 0, 't': 0}

fig.write_image(os.path.join(export_path,"24_elecprices.png"), scale = 5)

#%% 