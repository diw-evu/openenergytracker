#%% Export figures for Ampel-Monitor Blogs #########################################################

# Import packages

from pathlib import Path
import plotly.graph_objs as go

# Run script

from am_landing_page import *

#import create_online_figures
#import create_html_figures

#exec(open('create_online_figures.py').read())

# Create folder

#Path("docs/figures/monitor/blog").mkdir(parents=True, exist_ok=True)

# Define export path
export_path = "docs/figures/am/blog5/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

#%% Adapt and export figures


##### E-Autos: Bestand #####

# html: einfach von der Startseite nehmen
fig_bev_de_blog5 = go.Figure(fig_bev_de)

fig_bev_de_blog5.update_yaxes(range = [0, 16000000])

# Remove buttons and title
fig_bev_de_blog5.layout.updatemenus[0].buttons = None
fig_bev_de_blog5 = fig_bev_de_blog5.update_layout(title = "")

# Switch on visibility
fig_bev_de_blog5.data[3].visible = None
fig_bev_de_blog5.data[4].visible = None
fig_bev_de_blog5.data[5].visible = None

# Remove from legend
fig_bev_de_blog5.data[6].visible = False
fig_bev_de_blog5.data[7].visible = False
fig_bev_de_blog5.data[8].visible = False

# TEST
fig_bev_de_blog5 = fig_bev_de_blog5.update_layout(legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_de_blog5.write_html(os.path.join(export_path,"blog5_bev_de.html"),
                                     include_plotlyjs="directory")

#png:
fig_bev_de_blog5 = fig_bev_de_blog5.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_de_blog5.write_image(os.path.join(export_path,"blog5_bev_de.png"),
                                        scale = 3)

# english:

fig_bev_en_blog5 = go.Figure(fig_bev_en)

fig_bev_en_blog5.update_yaxes(range = [0, 16000000])

# Remove buttons and title
fig_bev_en_blog5.layout.updatemenus[0].buttons = None
fig_bev_en_blog5 = fig_bev_en_blog5.update_layout(title = "")

# Switch on visibility
fig_bev_en_blog5.data[3].visible = None
fig_bev_en_blog5.data[4].visible = None
fig_bev_en_blog5.data[5].visible = None

# Remove from legend
fig_bev_en_blog5.data[6].visible = False
fig_bev_en_blog5.data[7].visible = False
fig_bev_en_blog5.data[8].visible = False

fig_bev_en_blog5.write_html(os.path.join(export_path,"blog5_bev_en.html"),
                                     include_plotlyjs="directory")

#png:
fig_bev_en_blog5 = fig_bev_en_blog5.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_en_blog5.write_image(os.path.join(export_path,"blog5_bev_en.png"),
                                        scale = 3)


##### E-Autos: Anteil Neuzulassungen #####

# html hier neu:

fig_bev_adm_de_blog5 = go.Figure(fig_bev_adm_de)
fig_bev_adm_de_blog5 = fig_bev_adm_de_blog5.update_layout(legend = {'y':-.5},
                                     title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_bev_adm_de_blog5.layout.updatemenus[0].y = 1.2

# Remove buttons
fig_bev_adm_de_blog5.layout.updatemenus[0].buttons = None

fig_bev_adm_de_blog5.write_html(os.path.join(export_path,"blog5_bev_adm_de.html"),
                                     include_plotlyjs="directory")



fig_bev_adm_en_blog5 = go.Figure(fig_bev_adm_en)
fig_bev_adm_en_blog5 = fig_bev_adm_en_blog5.update_layout(legend = {'y':-.5},
                                     title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_bev_adm_en_blog5.layout.updatemenus[0].y = 1.2

# Remove buttons
fig_bev_adm_en_blog5.layout.updatemenus[0].buttons = None

fig_bev_adm_en_blog5.write_html(os.path.join(export_path,"blog5_bev_adm_en.html"),
                                     include_plotlyjs="directory")

   
# png:
fig_bev_adm_de_blog5 = fig_bev_adm_de_blog5.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_adm_de_blog5.write_image(os.path.join(export_path,"blog5_bev_adm_de.png"),
                                        scale = 3)

fig_bev_adm_en_blog5 = fig_bev_adm_en_blog5.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_adm_en_blog5.write_image(os.path.join(export_path,"blog5_bev_adm_en.png"),
                                        scale = 3)
