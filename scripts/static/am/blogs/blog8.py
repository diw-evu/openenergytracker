#%% Export figures for Ampel-Monitor blog #6 ##################################################

# Import packages

import sys
import os
from pathlib import Path
import plotly.graph_objs as go
import glob

# import plotly.io as pio
# pio.renderers.default = "notebook"

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'static':
    os.chdir('../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%% Run script
from create_figures_DE import *
import scripts.util as oet
import scripts.colors as colors

# Define export path
export_path = "docs/figures/am/blog8/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

#%% BEV Bestand ####################################################################################

dict_bev_blog8 = dict_bev_de.copy()
dict_bev_blog8 = oet.update_color(dict_bev_blog8,colors.diw)
fig_bev_blog8  = oet.create_fig(dict_bev_blog8)
fig_bev_blog8.update_layout(width = 860, height = 430, margin=dict(r=10, b=30))
fig_bev_blog8.write_html(os.path.join(export_path,"fig_bev_blog8.html"), include_plotlyjs="directory")

fig_bev_blog8_png = go.Figure(fig_bev_blog8)
fig_bev_blog8_png.layout.updatemenus[0].buttons = None
fig_bev_blog8_png = fig_bev_blog8_png.update_layout(margin=dict(l=0, r=0, t=30, b=0, pad=0))

fig_bev_blog8_png.data[1].visible = False
fig_bev_blog8_png.data[5].visible = False
fig_bev_blog8_png.data[6].visible = False

fig_bev_blog8_png.write_image(os.path.join(export_path,"fig_bev_blog8.png"), scale = 3)

#%% BEV Zulassungen ################################################################################

dict_adm_blog8 = dict_bev_adm_de.copy()
dict_adm_blog8 = oet.update_color(dict_adm_blog8,colors.diw)
fig_adm_blog8  = oet.create_fig(dict_adm_blog8)
fig_adm_blog8.update_layout(width = 860, height = 430, margin=dict(r=10, b=30))
fig_adm_blog8.write_html(os.path.join(export_path,"fig_adm_blog8.html"), include_plotlyjs="directory")

fig_adm_blog8_png = go.Figure(fig_adm_blog8)
fig_adm_blog8_png.write_image(os.path.join(export_path,"fig_adm_blog8.png"), scale = 3)

#%% Auto Bestand ###################################################################################

fig_car_stock_blog8 = go.Figure(fig_cars_quarterly_de)
fig_car_stock_blog8.update_layout(width = 860, height = 430, margin=dict(r=10, b=30))
fig_car_stock_blog8.write_html(os.path.join(export_path,"fig_car_stock_blog8.html"), include_plotlyjs="directory")

fig_car_stock_blog8_png = go.Figure(fig_car_stock_blog8)
fig_car_stock_blog8_png.write_image(os.path.join(export_path,"fig_car_stock_blog8.png"), scale = 3)

#%% PKW pro 1000 ###################################################################################

fig_cars_1000_blog8 = go.Figure(create_cars_pop_de)
fig_cars_1000_blog8.update_layout(width = 860, height = 430, margin=dict(r=30, b=30))
fig_cars_1000_blog8.write_html(os.path.join(export_path,"fig_cars_1000_blog8.html"), include_plotlyjs="directory")

fig_cars_1000_blog8_png = go.Figure(fig_cars_1000_blog8)
fig_cars_1000_blog8_png.write_image(os.path.join(export_path,"fig_cars_1000_blog8.png"), scale = 3)

#%% Ladepunkte #####################################################################################

fig_cs_blog8 = go.Figure(fig_cs_de)
fig_cs_blog8.update_layout(width = 860, height = 430, margin=dict(r=10, b=30))
fig_cs_blog8.data[5].visible = False
fig_cs_blog8.data[6].visible = False
fig_cs_blog8.write_html(os.path.join(export_path,"fig_cs_blog8.html"), include_plotlyjs="directory")

fig_cs_blog8_png = go.Figure(fig_cs_blog8)
fig_cs_blog8_png.write_image(os.path.join(export_path,"fig_cs_blog8.png"), scale = 3)


#%% Ladeleistung ###################################################################################

fig_cs_capa_blog8 = go.Figure(fig_cs_capa_de)
fig_cs_capa_blog8.update_layout(width = 860, height = 430, legend = dict(font=dict(size=12)), margin=dict(r=10, b=30))
fig_cs_capa_blog8.write_html(os.path.join(export_path,"fig_cs_capa_blog8.html"), include_plotlyjs="directory")

fig_cs_capa_blog8_png = go.Figure(fig_cs_capa_blog8)
fig_cs_capa_blog8_png.write_image(os.path.join(export_path,"fig_cs_capa_blog8.png"), scale = 3)

#%% Verhältnis Ladeleistung ########################################################################

fig_bev_per_cp_blog8 = go.Figure(fig_bev_per_cp_de)
fig_bev_per_cp_blog8.update_layout(width = 860, height = 430, margin=dict(r=30, b=30))
fig_bev_per_cp_blog8.write_html(os.path.join(export_path,"fig_bev_per_cp_blog8.html"), include_plotlyjs="directory")

fig_bev_per_cp_blog8_png = go.Figure(fig_bev_per_cp_blog8)
fig_bev_per_cp_blog8_png.write_image(os.path.join(export_path,"fig_bev_per_cp_blog8.png"), scale = 3)

#%%
