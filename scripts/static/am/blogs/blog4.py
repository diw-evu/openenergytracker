#%% Export figures for Ampel-Monitor Blogs #########################################################

# Import packages

import os
from pathlib import Path
import plotly.graph_objs as go

#%% Change working directory
os.chdir('../../../')

# Run script
from oet_main_de import *

# Define export path
export_path = "docs/figures/am/blog4/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

#%% GAS FIGURE 1 ###################################################################################

fig_gas_consumption_de_blog4 = go.Figure(fig_gas_consumption_de)
fig_gas_consumption_de_blog4.write_html(os.path.join(export_path,"am_blog4_figure1.html"),
                                        include_plotlyjs="directory")

fig_gas_consumption_de_blog4 = fig_gas_consumption_de_blog4.update_layout(
    width = 860, height = 430,
    #legend = {'y':-.4},
    margin=dict(l=0, r=0, t=75, b=0, pad=0))

fig_gas_consumption_de_blog4.write_image(os.path.join(export_path,"am_blog4_figure1.png"),
                                         scale = 5)

#%% GAS FIGURE 2 ###################################################################################

fig_gas_savings_de_blog4 = go.Figure(fig_gas_savings_de)
fig_gas_savings_de_blog4.write_html(os.path.join(export_path,"am_blog4_figure2.html"),
                                        include_plotlyjs="directory")

fig_gas_savings_de_blog4 = fig_gas_savings_de_blog4.update_layout(
    width = 860, height = 430,
    #legend = {'y':-.4},
    margin=dict(l=0, r=0, t=75, b=0, pad=0))

fig_gas_savings_de_blog4.write_image(os.path.join(export_path,"am_blog4_figure2.png"),
                                         scale = 5)

#%% GAS FIGURE 3 ###################################################################################

fig_gas_reached_de_blog4 = go.Figure(fig_gas_reached_de)
fig_gas_reached_de_blog4.write_html(os.path.join(export_path,"am_blog4_figure3.html"),
                                        include_plotlyjs="directory")

fig_gas_reached_de_blog4 = fig_gas_reached_de_blog4.update_layout(
    width = 860, height = 430,
    #legend = {'y':-.4},
    margin=dict(l=0, r=0, t=75, b=0, pad=0))

fig_gas_reached_de_blog4.write_image(os.path.join(export_path,"am_blog4_figure3.png"),
                                         scale = 5)

#%% ################################################################################################