#%% Export figures for Ampel-Monitor blog #6 ##################################################

# Import packages

import sys
import os
from pathlib import Path
import plotly.graph_objs as go
import glob

# import plotly.io as pio
# pio.renderers.default = "notebook"

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'static':
    os.chdir('../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%% Run script
from create_figures_DE import *

# Define export path
export_path = "docs/figures/am/blog6/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

#%% pv #############################################################################################

fig_blog6_pv_de_iframe = go.Figure(fig_pv_de)
fig_blog6_pv_de_iframe = fig_blog6_pv_de_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=30, pad=0))
fig_blog6_pv_de_iframe.layout.updatemenus[0].y = 1.15
fig_blog6_pv_de_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_de.html"),include_plotlyjs="directory")

#%% png version
fig_blog6_pv_de_png = go.Figure(fig_pv_de)
fig_blog6_pv_de_png = fig_blog6_pv_de_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, t=30, b=0, pad=0)
)

# Remove buttons
fig_blog6_pv_de_png.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_blog6_pv_de_png.data[1].visible = None
fig_blog6_pv_de_png.data[4].visible = False
fig_blog6_pv_de_png.data[5].visible = None

fig_blog6_pv_de_png.write_image(os.path.join(export_path,"fig_blog6_pv_de.png"),
                                        scale = 3)

#%% pv_type_split ##################################################################################

fig_blog6_pv_type_split_plt_iframe = go.Figure(pv_type_split_plt)
fig_blog6_pv_type_split_plt_iframe = fig_blog6_pv_type_split_plt_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=30, t = 30, pad=0))
fig_blog6_pv_type_split_plt_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_type_split_plt.html"),include_plotlyjs="directory")

#%% png version
fig_blog6_pv_type_split_plt_png = go.Figure(pv_type_split_plt)
fig_blog6_pv_type_split_plt_png = fig_blog6_pv_type_split_plt_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0,t=30))

fig_blog6_pv_type_split_plt_png.write_image(os.path.join(export_path,"fig_blog6_pv_type_split_plt.png"),
                                        scale = 3)

#%% pv_type_split_rel ##############################################################################

fig_blog6_pv_type_split_rel_plt_iframe = go.Figure(pv_type_annual_rel_plt)
fig_blog6_pv_type_split_rel_plt_iframe = fig_blog6_pv_type_split_rel_plt_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, t = 30, pad=0))
fig_blog6_pv_type_split_rel_plt_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_type_split_rel_plt.html"),include_plotlyjs="directory")

#%% png version
fig_blog6_pv_type_split_rel_plt_png = go.Figure(pv_type_annual_rel_plt)
fig_blog6_pv_type_split_rel_plt_png = fig_blog6_pv_type_split_rel_plt_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, t = 30, pad=0))
fig_blog6_pv_type_split_rel_plt_png.write_image(os.path.join(export_path,"fig_blog6_pv_type_split_rel_plt.png"),
                                        scale = 3)

#%% pv_size_split ##################################################################################

fig_blog6_pv_size_split_plt_iframe = go.Figure(pv_size_split_plt)
fig_blog6_pv_size_split_plt_iframe = fig_blog6_pv_size_split_plt_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, t=30, b=30, pad=0))
fig_blog6_pv_size_split_plt_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_size_split_plt.html"),include_plotlyjs="directory")

#%% png version
fig_blog6_pv_size_split_plt_png = go.Figure(pv_size_split_plt)
fig_blog6_pv_size_split_plt_png = fig_blog6_pv_size_split_plt_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, t=30, b=0, pad=0))

fig_blog6_pv_size_split_plt_png.write_image(os.path.join(export_path,"fig_blog6_pv_size_split_plt.png"),
                                        scale = 3)

#%% pv_eeg_split ###################################################################################

fig_blog6_pv_eeg_plt_iframe = go.Figure(pv_eeg_plt)
fig_blog6_pv_eeg_plt_iframe = fig_blog6_pv_eeg_plt_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, t=30, b=30, pad=0))
fig_blog6_pv_eeg_plt_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_eeg_plt.html"),include_plotlyjs="directory")

#%% png version
fig_blog6_pv_eeg_plt_png = go.Figure(pv_eeg_plt)
fig_blog6_pv_eeg_plt_png = fig_blog6_pv_eeg_plt_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, t=30, b=0, pad=0))
fig_blog6_pv_eeg_plt_png.write_image(os.path.join(export_path,"fig_blog6_pv_eeg_plt.png"),
                                        scale = 3)

#%% pv_butterfly ###################################################################################

fig_blog6_pv_butterfly_iframe = go.Figure(fig_pv_butterfly_DE)
fig_blog6_pv_butterfly_iframe = fig_blog6_pv_butterfly_iframe.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b = 30, t = 60, pad=0))
fig_blog6_pv_butterfly_iframe.write_html(os.path.join(export_path,"fig_blog6_butterfly.html"),include_plotlyjs="directory")

#%% png version

fig_blog6_pv_butterfly_png = go.Figure(fig_pv_butterfly_DE)
fig_blog6_pv_butterfly_png = fig_blog6_pv_butterfly_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, t = 60, pad=0))
fig_blog6_pv_butterfly_png.write_image(os.path.join(export_path,"fig_blog6_pv_butterfly.png"),
                                        scale = 3)

#%% pv_maps ########################################################################################

fig_blog6_pv_map_iframe = go.Figure(pv_map)
fig_blog6_pv_map_iframe = fig_blog6_pv_map_iframe.update_layout(
    width = 860, height = 430,  margin=dict(l=0, r=0, b=0, t = 60, pad=0))
fig_blog6_pv_map_iframe.write_html(os.path.join(export_path,"fig_blog6_pv_map.html"), include_plotlyjs="directory")

#%% png version
fig_blog6_pv_map_png = go.Figure(pv_map)
fig_blog6_pv_map_png = fig_blog6_pv_map_png.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0))
fig_blog6_pv_map_png.write_image(os.path.join(export_path,"fig_blog6_pv_map.png"),scale = 3)

#%% ################################################################################################

