#%% Export figures for Ampel-Monitor blog #6 ##################################################

# Import packages

import sys
import os
from pathlib import Path
import plotly.graph_objs as go
import glob

# import plotly.io as pio
# pio.renderers.default = "notebook"

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'static':
    os.chdir('../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%% Run script
from create_figures_DE import *

# Define export path
export_path = "docs/figures/am/blog7/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

# Note: I did not do dedicated htmls for AM here, but directly copied the version from OET for reasons of time

#%% pngs of all blog figures except main onshore wind figure ##################################################################################

fig_blog7_wind_on_dd_size_shares = go.Figure(size_share_fig)
fig_blog7_wind_on_dd_size_shares = fig_blog7_wind_on_dd_size_shares.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, t = 50, pad=0))
fig_blog7_wind_on_dd_size_shares.write_image(os.path.join(export_path,"fig_blog7_wind_on_dd_size_shares.png"),
                                        scale = 3)

fig_blog7_wind_on_dd_state_shares = go.Figure(state_share_fig)
fig_blog7_wind_on_dd_state_shares = fig_blog7_wind_on_dd_state_shares.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0, t=50))

fig_blog7_wind_on_dd_state_shares.write_image(os.path.join(export_path,"fig_blog7_wind_on_dd_state_shares.png"),
                                        scale = 3)

fig_blog7_wind_on_dd_attributes = go.Figure(attributes_fig)
fig_blog7_wind_on_dd_attributes = fig_blog7_wind_on_dd_attributes.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0, t=50))

fig_blog7_wind_on_dd_attributes.write_image(os.path.join(export_path,"fig_blog7_wind_on_dd_attributes.png"),
                                        scale = 3)

fig_blog7_wind_on_dd_size_shares = go.Figure(size_share_fig)
fig_blog7_wind_on_dd_size_shares = fig_blog7_wind_on_dd_size_shares.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0, t=50))

fig_blog7_wind_on_dd_size_shares.write_image(os.path.join(export_path,"fig_blog7_wind_on_dd_size_shares.png"),
                                        scale = 3)

fig_blog7_wind_on_dd_butterfly_wind = go.Figure(butterfly_wind_fig)
fig_blog7_wind_on_dd_butterfly_wind = fig_blog7_wind_on_dd_butterfly_wind.update_layout(
    width = 860, height = 430, margin=dict(l=0, r=0, b=0, pad=0, t=50))

fig_blog7_wind_on_dd_butterfly_wind.write_image(os.path.join(export_path,"fig_blog7_wind_on_dd_butterfly_wind.png"),
                                        scale = 3)

# %%
