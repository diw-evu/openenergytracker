#%% Export figures for Ampel-Monitor Blogs #########################################################

# Import packages

from pathlib import Path
import plotly.graph_objs as go

# Run script

from am_landing_page import *

#import create_online_figures
#import create_html_figures

#exec(open('create_online_figures.py').read())

# Create folder

Path("docs/figures/monitor/blog").mkdir(parents=True, exist_ok=True)

#%% Adapt and export figures

###### reached - also for blogs ######

fig_reached_de_monitor_blog = fig_reached_de_monitor_iframe

fig_reached_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_reached_de.png", scale = 3)

###### PV ######

fig_pv_de_monitor_blog = go.Figure(fig_pv_de)

fig_pv_de_monitor_blog = fig_pv_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))
# Remove buttons
fig_pv_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_pv_de_monitor_blog.data[1].visible = None
fig_pv_de_monitor_blog.data[2].visible = None

# Remove from legend
fig_pv_de_monitor_blog.data[5].visible = False
fig_pv_de_monitor_blog.data[6].visible = False
fig_pv_de_monitor_blog.data[7].visible = False

fig_pv_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_PV_de.png", scale = 3)

###### Wind Onshore ######

fig_windon_de_monitor_blog = go.Figure(fig_windon_de)

fig_windon_de_monitor_blog = fig_windon_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))
# Remove buttons
fig_windon_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_windon_de_monitor_blog.data[1].visible = None
fig_windon_de_monitor_blog.data[2].visible = None

# Remove from legend
fig_windon_de_monitor_blog.data[5].visible = False
fig_windon_de_monitor_blog.data[6].visible = False
fig_windon_de_monitor_blog.data[7].visible = False

fig_windon_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Wind_on_de.png",
                                        scale = 3)

###### Wind Offshore ######

fig_windoff_de_monitor_blog = go.Figure(fig_windoff_de)

fig_windoff_de_monitor_blog = fig_windoff_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))
# Remove buttons
fig_windoff_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_windoff_de_monitor_blog.data[1].visible = None
fig_windoff_de_monitor_blog.data[2].visible = None

# Remove from legend
fig_windoff_de_monitor_blog.data[5].visible = False
fig_windoff_de_monitor_blog.data[6].visible = False
fig_windoff_de_monitor_blog.data[7].visible = False

fig_windoff_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Wind_off_de.png",
                                        scale = 3)

##### Wind Fläche ######

fig_wind_areas_de_monitor_blog = go.Figure(fig_wind_areas_de)

fig_wind_areas_de_monitor_blog = fig_wind_areas_de_monitor_blog.update_layout(width = 860, height = 430,
                                                                  legend = {'y':-.2},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_wind_areas_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Wind_Flaeche_de.png",
                                        scale = 3)

##### Anteil Strom #####

fig_rs_de_monitor_blog = go.Figure(fig_rs_de)

fig_rs_de_monitor_blog = fig_rs_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.5},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_rs_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_rs_de_monitor_blog.data[3].visible = None
fig_rs_de_monitor_blog.data[4].visible = None

# Remove from legend
fig_rs_de_monitor_blog.data[5].visible = False
fig_rs_de_monitor_blog.data[6].visible = False
fig_rs_de_monitor_blog.data[7].visible = False

fig_rs_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Anteil_EE_Strom_de.png",
                                        scale = 3)

##### Anteil Wärme #####

fig_rs_heat_de_monitor_blog = go.Figure(fig_rs_heat_de)

fig_rs_heat_de_monitor_blog = fig_rs_heat_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.2},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_rs_heat_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_rs_heat_de_monitor_blog.data[3].visible = None

fig_rs_heat_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Anteil_EE_Warme_de.png",
                                        scale = 3)

##### Wärmepumpen #####

fig_hp_de_monitor_blog = go.Figure(fig_hp_de)

fig_hp_de_monitor_blog = fig_hp_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.5},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_hp_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_hp_de_monitor_blog.data[3].visible = None

# Remove from legend
fig_hp_de_monitor_blog.data[4].visible = False
fig_hp_de_monitor_blog.data[5].visible = False
fig_hp_de_monitor_blog.data[6].visible = False
fig_hp_de_monitor_blog.data[7].visible = False
fig_hp_de_monitor_blog.data[8].visible = False
fig_hp_de_monitor_blog.data[9].visible = False
fig_hp_de_monitor_blog.data[10].visible = False

fig_hp_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Waermepumpen_de.png",
                                        scale = 3)

##### E-Autos #####

fig_bev_de_monitor_blog = go.Figure(fig_bev_de)

fig_bev_de_monitor_blog = fig_bev_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_bev_de_monitor_blog.update_yaxes(range = [0, 16000000])

# Remove buttons
fig_bev_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_bev_de_monitor_blog.data[3].visible = None
fig_bev_de_monitor_blog.data[4].visible = None
fig_bev_de_monitor_blog.data[5].visible = None

# Remove from legend
fig_bev_de_monitor_blog.data[6].visible = False
fig_bev_de_monitor_blog.data[7].visible = False
fig_bev_de_monitor_blog.data[8].visible = False

fig_bev_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_BEV_Bestand_de.png",
                                        scale = 3)

##### E-Autos Share #####

fig_bev_adm_de_monitor_blog = go.Figure(fig_bev_adm_de)

fig_bev_adm_de_monitor_blog = fig_bev_adm_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_bev_adm_de_monitor_blog.layout.updatemenus[0].buttons = None


fig_bev_adm_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_BEV_PHEV_Neuzulassungen_de.png",
                                        scale = 3)

##### Ladepunkt #####

fig_cs_de_monitor_blog = go.Figure(fig_cs_de)

fig_cs_de_monitor_blog = fig_cs_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_cs_de_monitor_blog.layout.updatemenus[0].buttons = None

# Switch on visibility
fig_cs_de_monitor_blog.data[4].visible = None
fig_cs_de_monitor_blog.data[5].visible = None
fig_cs_de_monitor_blog.data[6].visible = None

fig_cs_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Ladepunkte_de.png",
                                        scale = 3)

##### BEV per Ladepunkt #####

fig_bev_per_cp_de_monitor_blog = go.Figure(fig_bev_per_cp_de)

fig_bev_per_cp_de_monitor_blog = fig_bev_per_cp_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_bev_per_cp_de_monitor_blog.layout.updatemenus[0].buttons = None

fig_bev_per_cp_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_BEV_pro_Ladepunkt_de.png",
                                        scale = 3)

##### Schiene #####

fig_rail_elec_de_monitor_blog = go.Figure(fig_rail_elec_de)


fig_rail_elec_de_monitor_blog = fig_rail_elec_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Switch on visibility
fig_rail_elec_de_monitor_blog.data[3].visible = None

fig_rail_elec_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Elektrifizierung_Bahn_de.png",
                                        scale = 3)

##### Elektrolyse #####

fig_h2_de_monitor_blog = go.Figure(fig_h2_de)

fig_h2_de_monitor_blog = fig_h2_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.4},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

# Remove buttons
fig_h2_de_monitor_blog.layout.updatemenus[0].buttons = None

# Remove from legend
fig_h2_de_monitor_blog.data[3].visible = False
fig_h2_de_monitor_blog.data[4].visible = False
fig_h2_de_monitor_blog.data[5].visible = False

fig_h2_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Elektrolyse_de.png",
                                        scale = 3)

##### Erdgas-Importe #####

fig_gas_de_monitor_blog = go.Figure(fig_gas_de)

fig_gas_de_monitor_blog = fig_gas_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.8},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_gas_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Erdgas_de.png",
                                        scale = 3)

##### Erdgas-Importe 3 Monate #####

fig_gas_3month_de_monitor_blog = go.Figure(fig_gas_3month_de)

fig_gas_3month_de_monitor_blog = fig_gas_3month_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_gas_3month_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Erdgas _3Monate_de.png",
                                        scale = 3)

##### Fossiler Energieverbrauch #####

fig_pec_figure_de_monitor_blog = go.Figure(fig_pec_figure_de)

fig_pec_figure_de_monitor_blog = fig_pec_figure_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  legend = {'y':-.6},
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_pec_figure_de_monitor_blog.data[6].visible = None

fig_pec_figure_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Fossiler_PEV_de.png",
                                        scale = 3)

##### Fossiler Energieverbrauch 3 Monate #####

fig_pec_3month_figure_de_monitor_blog = go.Figure(fig_pec_3month_figure_de)

fig_pec_3month_figure_de_monitor_blog = fig_pec_3month_figure_de_monitor_blog.update_layout(width = 860, height = 430,
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_pec_3month_figure_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Fossiler_PEV_3Monate_de.png",
                                        scale = 3)

##### Sektorale Treibhausgasemissionen #####

fig_emissions_sect_de_monitor_blog = go.Figure(fig_emissions_sect_de)

fig_emissions_sect_de_monitor_blog = fig_emissions_sect_de_monitor_blog.update_layout(width = 860, height = 430,
                                                 legend = dict(font = dict(size = 10), grouptitlefont = dict(size = 10)),
                                                 title = "",
                                                 margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_emissions_sect_de_monitor_blog.data[15].visible = None

fig_emissions_sect_de_monitor_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_Sektorale_THG-Emissionen_de.png",
                                        scale = 3)

##### CO2-Emissionen der Stromerzeugung #####

fig_emissions_electr_figure_de_blog = go.Figure(fig_emissions_electr_figure_de)

fig_emissions_electr_figure_de_blog = fig_emissions_electr_figure_de_blog.update_layout(width = 860, height = 430,
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=0, b=0, pad=0))

fig_emissions_electr_figure_de_blog.data[4].visible = None

fig_emissions_electr_figure_de_blog.write_image("docs/figures/monitor/blog/Ampel-Monitor#2_CO2-Emissionen_Stromerzeugung_de.png",
                                        scale = 3)
