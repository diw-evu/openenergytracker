#%% Export figures for Ampel-Monitor landing page ##################################################

# Import packages

import sys
import os
from pathlib import Path
import plotly.graph_objs as go
import glob
import copy

# import plotly.io as pio
# pio.renderers.default = "notebook"

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'static':
    os.chdir('../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%% Run script
from create_figures_DE import *

import scripts.util as oet
import scripts.colors as colors

# Define export path
export_path = "docs/figures/am/landingpage/"

# Create folder
Path(export_path).mkdir(parents=True, exist_ok=True)

print("")
print("---------- Script: AM ----------")
print("")

#%% Functions ######################################################################################

def create_am_figure_html(input_dict,color_dict):
    updated_dict = oet.update_color(input_dict,color_dict)
    fig_am       = oet.create_fig(updated_dict)
    fig_am       = fig_am.update_layout(title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
    return fig_am

def create_am_figure_png(input_dict,color_dict):
    updated_dict = oet.update_color(input_dict,color_dict)
    fig_am       = oet.create_fig(updated_dict)
    
    if len(fig_am["layout"]["updatemenus"]) != 0:
        fig_am["layout"]["updatemenus"][0]["buttons"] = None
    else:
        pass

    if any([x in input_dict["base"]["title"] for x in ["Photovoltaik","Wind","wind","pv"]]):
        fig_am.data[1].visible = None
        fig_am.data[4].visible = False
        fig_am.data[5].visible = False
    elif any([x in input_dict["base"]["title"] for x in [
        "Anteil erneuerbarer Energien im Stromsektor",
        "Share of renewable energy in the power sector"]]):
        fig_am.data[3].visible = False
        fig_am.data[4].visible = False
        fig_am.data[5].visible = None
        fig_am.data[6].visible = None
    elif any([x in input_dict["base"]["title"] for x in [
        "Endenergieverbrauch für Wärme und Kält",
        "Final energy consumption for heating and cooling"]]):
        fig_am.data[3].visible = None
    elif any([x in input_dict["base"]["title"] for x in [
        "Bestand an Wärmepumpen",
        "Stock of heat pumps"]]):
        fig_am.data[6].visible = False
        fig_am.data[7].visible = False
        fig_am.data[8].visible = True
        fig_am.data[9].visible = False
        fig_am.data[10].visible = False
        fig_am.data[11].visible = False
        fig_am.data[12].visible = False
        fig_am.layout.legend.y = 1
    else:
        pass
    
    # Update layout
    fig_am       = fig_am.update_layout(title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0), 
                                        width = 860, height = 430)
    return fig_am


#%% Figures ########################################################################################

##### reached ######################################################################################

config = {'displayModeBar': False}

fig_reached_de_monitor_iframe = go.Figure(fig_reached_de)
fig_reached_de_monitor_iframe.data[1]["marker_color"] = colors.diw["first_light"]
fig_reached_de_monitor_iframe.data[0]["marker_color"] = colors.diw["first"]

fig_reached_de_monitor_iframe = fig_reached_de_monitor_iframe.update_layout(legend = {'y':-.5}, title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_reached_de_monitor_iframe.write_html(os.path.join(export_path,"fig_reached_de.html"), include_plotlyjs="directory", config=config)

fig_reached_en_monitor_iframe = go.Figure(fig_reached_en)
fig_reached_en_monitor_iframe.data[1]["marker_color"] = colors.diw["first_light"]
fig_reached_en_monitor_iframe.data[0]["marker_color"] = colors.diw["first"]

fig_reached_en_monitor_iframe = fig_reached_en_monitor_iframe.update_layout(legend = {'y':-.5}, title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_reached_en_monitor_iframe.write_html(os.path.join(export_path,"fig_reached_en.html"), include_plotlyjs="directory", config=config)

# png version
fig_reached_de_monitor_png = fig_reached_de_monitor_iframe
fig_reached_de_monitor_png.write_image(os.path.join(export_path,"fig_reached_de.png"), scale = 3)

fig_reached_en_monitor_png = fig_reached_en_monitor_iframe
fig_reached_en_monitor_png.write_image(os.path.join(export_path,"fig_reached_en.png"), scale = 3)

#%% pv #############################################################################################

fig_pv_de_monitor_iframe = create_am_figure_html(dict_pv_de,colors.diw)
fig_pv_de_monitor_iframe.write_html(os.path.join(export_path,"fig_pv_de.html"), include_plotlyjs="directory")

fig_pv_en_monitor_iframe = create_am_figure_html(dict_pv_en,colors.diw)
fig_pv_en_monitor_iframe.write_html(os.path.join(export_path,"fig_pv_en.html"), include_plotlyjs="directory")

fig_pv_de_monitor_png = create_am_figure_png(dict_pv_de,colors.diw)
fig_pv_de_monitor_png.write_image(os.path.join(export_path,"fig_pv_de.png"), scale = 3)

fig_pv_en_monitor_png = create_am_figure_png(dict_pv_en,colors.diw)
fig_pv_en_monitor_png.write_image(os.path.join(export_path,"fig_pv_en.png"), scale = 3)

#%% wind onshore ###################################################################################

fig_windon_de_monitor_iframe = create_am_figure_html(dict_windon_de,colors.diw)
fig_windon_de_monitor_iframe.write_html(os.path.join(export_path,"fig_windon_de.html"), include_plotlyjs="directory")

fig_windon_en_monitor_iframe = create_am_figure_html(dict_windon_en,colors.diw)
fig_windon_en_monitor_iframe.write_html(os.path.join(export_path,"fig_windon_en.html"), include_plotlyjs="directory")

fig_windon_de_monitor_png = create_am_figure_png(dict_windon_de,colors.diw)
fig_windon_de_monitor_png.write_image(os.path.join(export_path,"fig_windon_de.png"), scale = 3)

fig_windon_en_monitor_png = create_am_figure_png(dict_windon_en,colors.diw)
fig_windon_en_monitor_png.write_image(os.path.join(export_path,"fig_windon_en.png"), scale = 3)

#%% wind offshore ##################################################################################

fig_windoff_de_monitor_iframe = create_am_figure_html(dict_windoff_de,colors.diw)
fig_windoff_de_monitor_iframe.write_html(os.path.join(export_path,"fig_windoff_de.html"), include_plotlyjs="directory")

fig_windoff_en_monitor_iframe = create_am_figure_html(dict_windoff_en,colors.diw)
fig_windoff_en_monitor_iframe.write_html(os.path.join(export_path,"fig_windoff_en.html"), include_plotlyjs="directory")

fig_windoff_de_monitor_png = create_am_figure_png(dict_windoff_de,colors.diw)
fig_windoff_de_monitor_png.write_image(os.path.join(export_path,"fig_windoff_de.png"), scale = 3)

fig_windoff_en_monitor_png = create_am_figure_png(dict_windoff_en,colors.diw)
fig_windoff_en_monitor_png.write_image(os.path.join(export_path,"fig_windoff_en.png"), scale = 3)

#%% elec share #####################################################################################

update_dict = dict(title=None, tickmode = 'array', range = ["2009-06","2031-03"],
                tickvals = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030],
                ticktext = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030'])

fig_rs_de_monitor_iframe = create_am_figure_html(dict_resshares_de,colors.diw)
fig_rs_de_monitor_iframe.update_xaxes(update_dict)
fig_rs_de_monitor_iframe.write_html(os.path.join(export_path,"fig_rs_de.html"), include_plotlyjs="directory")

fig_rs_en_monitor_iframe = create_am_figure_html(dict_resshares_en,colors.diw)
fig_rs_en_monitor_iframe.update_xaxes(update_dict)
fig_rs_en_monitor_iframe.write_html(os.path.join(export_path,"fig_rs_en.html"), include_plotlyjs="directory")

fig_rs_de_monitor_png = create_am_figure_png(dict_resshares_de,colors.diw)
fig_rs_de_monitor_png.write_image(os.path.join(export_path,"fig_rs_de.png"), scale = 3)

fig_rs_en_monitor_png = create_am_figure_png(dict_resshares_en,colors.diw)
fig_rs_en_monitor_png.write_image(os.path.join(export_path,"fig_rs_en.png"), scale = 3)

#%% heat share #####################################################################################

fig_rs_heat_de_monitor_iframe = create_am_figure_html(dict_rs_heat_de,colors.diw)
fig_rs_heat_de_monitor_iframe.write_html(os.path.join(export_path,"fig_rs_heat_de.html"), include_plotlyjs="directory")

fig_rs_heat_en_monitor_iframe = create_am_figure_html(dict_rs_heat_en,colors.diw)
fig_rs_heat_en_monitor_iframe.write_html(os.path.join(export_path,"fig_rs_heat_en.html"), include_plotlyjs="directory")

fig_rs_heat_de_monitor_png = create_am_figure_png(dict_rs_heat_de,colors.diw)
fig_rs_heat_de_monitor_png.write_image(os.path.join(export_path,"fig_rs_heat_de.png"), scale = 3)

fig_rs_heat_en_monitor_png = create_am_figure_png(dict_rs_heat_en,colors.diw)
fig_rs_heat_en_monitor_png.write_image(os.path.join(export_path,"fig_rs_heat_en.png"), scale = 3)

#%% hp #############################################################################################

update_dict = dict(title=None, tickmode = 'array', range = ["1999-06","2031-03"],
                tickvals = [2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017, 2018, 2019, 2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030],
                ticktext = ['2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030'])

fig_hp_de_monitor_iframe = create_am_figure_html(dict_hp_de,colors.diw)
fig_hp_de_monitor_iframe["data"][6]["visible"] = False
fig_hp_de_monitor_iframe["data"][7]["visible"] = False
fig_hp_de_monitor_iframe.update_xaxes(update_dict)
fig_hp_de_monitor_iframe.write_html(os.path.join(export_path,"fig_hp_de.html"), include_plotlyjs="directory")

fig_hp_en_monitor_iframe = create_am_figure_html(dict_hp_en,colors.diw)
fig_hp_en_monitor_iframe["data"][6]["visible"] = False
fig_hp_en_monitor_iframe["data"][7]["visible"] = False
fig_hp_en_monitor_iframe.update_xaxes(update_dict)
fig_hp_en_monitor_iframe.write_html(os.path.join(export_path,"fig_hp_en.html"), include_plotlyjs="directory")

fig_hp_de_monitor_png = create_am_figure_png(dict_hp_de,colors.diw)
fig_hp_de_monitor_png.write_image(os.path.join(export_path,"fig_hp_de.png"), scale = 3)
#
fig_hp_en_monitor_png = create_am_figure_png(dict_hp_en,colors.diw)
fig_hp_en_monitor_png.write_image(os.path.join(export_path,"fig_hp_en.png"), scale = 3)

#%% bev ############################################################################################

# DE
fig_bev_de_monitor_iframe = create_am_figure_html(dict_bev_de,colors.diw)
fig_bev_de_monitor_iframe.data[3].visible = 'legendonly'
fig_bev_de_monitor_iframe.data[5].visible = False
fig_bev_de_monitor_iframe.data[6].visible = False
fig_bev_de_monitor_iframe.data[8].visible = False
fig_bev_de_monitor_iframe.update_yaxes(dtick = 2500000,row = 1)
fig_bev_de_monitor_iframe.layout.updatemenus = None
fig_bev_de_monitor_iframe = fig_bev_de_monitor_iframe.update_layout(title = None, margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_bev_de_monitor_iframe.write_html(os.path.join(export_path,"fig_bev_de.html"),
                                     include_plotlyjs="directory")

# EN
fig_bev_en_monitor_iframe = create_am_figure_html(dict_bev_en,colors.diw)
fig_bev_en_monitor_iframe.data[3].visible = 'legendonly'
fig_bev_en_monitor_iframe.data[5].visible = False
fig_bev_en_monitor_iframe.data[6].visible = False
fig_bev_en_monitor_iframe.data[8].visible = False
fig_bev_en_monitor_iframe.update_yaxes(dtick = 2500000,row = 1)
fig_bev_en_monitor_iframe.layout.updatemenus = None
fig_bev_en_monitor_iframe = fig_bev_en_monitor_iframe.update_layout(title = None, margin=dict(l=0, r=0, t=0, b=0, pad=0))
fig_bev_en_monitor_iframe.write_html(os.path.join(export_path,"fig_bev_en.html"),
                                     include_plotlyjs="directory")

# png

# DE
fig_bev_de_monitor_png = go.Figure(fig_bev_de_monitor_iframe)
fig_bev_de_monitor_png = fig_bev_de_monitor_png.update_layout(width = 860, height = 430)
fig_bev_de_monitor_png.data[1].visible = True
fig_bev_de_monitor_png.data[3].visible = False
fig_bev_de_monitor_png.write_image(os.path.join(export_path,"fig_bev_de.png"), scale = 3)

# EN
fig_bev_en_monitor_png = go.Figure(fig_bev_en_monitor_iframe)
fig_bev_en_monitor_png = fig_bev_en_monitor_png.update_layout(width = 860, height = 430)

fig_bev_en_monitor_png.data[1].visible = True
fig_bev_en_monitor_png.data[3].visible = False
fig_bev_en_monitor_png.write_image(os.path.join(export_path,"fig_bev_en.png"), scale = 3)

#%% cs #############################################################################################

# HTML
fig_cs_de_monitor_iframe = create_am_figure_html(dict_cs_de,colors.diw)
fig_cs_de_monitor_iframe.write_html(os.path.join(export_path,"fig_cs_de.html"),include_plotlyjs="directory")
fig_cs_en_monitor_iframe = create_am_figure_html(dict_cs_en,colors.diw)
fig_cs_en_monitor_iframe.write_html(os.path.join(export_path,"fig_cs_en.html"),include_plotlyjs="directory")

# PNG
fig_cs_de_monitor_png = go.Figure(fig_cs_de_monitor_iframe)
fig_cs_de_monitor_png.data[2].visible = True
fig_cs_de_monitor_png.write_image(os.path.join(export_path,"fig_cs_de.png"), scale = 3)

fig_cs_en_monitor_png = go.Figure(fig_cs_en_monitor_iframe)
fig_cs_en_monitor_png.data[2].visible = True
fig_cs_en_monitor_png.write_image(os.path.join(export_path,"fig_cs_en.png"), scale = 3)

#%% h2 #############################################################################################

fig_h2_de_monitor_iframe = go.Figure(fig_iea_h2_de)
fig_h2_de_monitor_iframe = fig_h2_de_monitor_iframe.update_layout(
                                     title = "", margin=dict(l=0, r=0, t=50, b=0, pad=0))
#fig_h2_de_monitor_iframe.layout.updatemenus[0].y = 1.2
fig_h2_de_monitor_iframe.write_html(os.path.join(export_path,"fig_h2_de.html"),
                                    include_plotlyjs="directory")

fig_h2_en_monitor_iframe = go.Figure(fig_iea_h2_en)
fig_h2_en_monitor_iframe = fig_h2_en_monitor_iframe.update_layout(
                                     title = "", margin=dict(l=0, r=0, t=50, b=0, pad=0))
#fig_h2_en_monitor_iframe.layout.updatemenus[0].y = 1.2
fig_h2_en_monitor_iframe.write_html(os.path.join(export_path,"fig_h2_en.html"),
                                    include_plotlyjs="directory")

# png version for Ampel-Monitor landing page
fig_h2_de_monitor_png = go.Figure(fig_iea_h2_de)
fig_h2_de_monitor_png = fig_h2_de_monitor_png.update_layout(width = 860, height = 430,
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=50, b=0, pad=0))

# Remove buttons
#fig_h2_de_monitor_png.layout.updatemenus[0].buttons = None

# Remove from legend
#fig_h2_de_monitor_png.data[3].visible = False
#fig_h2_de_monitor_png.data[4].visible = False
#fig_h2_de_monitor_png.data[5].visible = False

fig_h2_de_monitor_png.write_image(os.path.join(export_path,"fig_h2_de.png"),
                                        scale = 3)

fig_h2_en_monitor_png = go.Figure(fig_iea_h2_en)
fig_h2_en_monitor_png = fig_h2_en_monitor_png.update_layout(width = 860, height = 430,
                                                  title = "",
                                                  margin=dict(l=0, r=0, t=50, b=0, pad=0))

# Remove buttons
#fig_h2_en_monitor_png.layout.updatemenus[0].buttons = None

# Remove from legend
#fig_h2_en_monitor_png.data[3].visible = False
#fig_h2_en_monitor_png.data[4].visible = False
#fig_h2_en_monitor_png.data[5].visible = False

fig_h2_en_monitor_png.write_image(os.path.join(export_path,"fig_h2_en.png"),
                                        scale = 3)

#%% gas savings ####################################################################################

fig_gas_savings_monitor_iframe_de = go.Figure(fig_gas_savings_abs_de)
fig_gas_savings_monitor_iframe_de.update_layout(title = "", margin=dict(l=0, r=0, t=30, b=0, pad=0))

fig_gas_savings_monitor_iframe_de.write_html(os.path.join(export_path,"fig_gas_savings_de.html"),
                                         include_plotlyjs="directory")

fig_gas_savings_monitor_iframe_de.update_layout(width = 860, height = 430)
fig_gas_savings_monitor_iframe_de.write_image(os.path.join(export_path,"fig_gas_savings_de.png"),
                                       scale = 5)

fig_gas_savings_monitor_iframe_en = go.Figure(fig_gas_savings_abs_en)
fig_gas_savings_monitor_iframe_en.update_layout(title = "", margin=dict(l=0, r=0, t=30, b=0, pad=0))

fig_gas_savings_monitor_iframe_en.write_html(os.path.join(export_path,"fig_gas_savings_en.html"),
                                         include_plotlyjs="directory")

fig_gas_savings_monitor_iframe_en.update_layout(width = 860, height = 430)
fig_gas_savings_monitor_iframe_en.write_image(os.path.join(export_path,"fig_gas_savings_en.png"),
                                       scale = 5)

#%% lng ships ######################################################################################
"""
config = {'staticPlot': True}

fig_gas_lng_monitor_iframe_de = go.Figure(tfig)
fig_gas_lng_monitor_iframe_en = go.Figure(tfig_en)

fig_gas_lng_monitor_iframe_de.update_layout(title = "", margin=dict(l=0, r=0, t=0, b=30, pad=0))
fig_gas_lng_monitor_iframe_en.update_layout(title = "", margin=dict(l=0, r=0, t=0, b=30, pad=0))

fig_gas_lng_monitor_iframe_de.write_html(os.path.join(export_path,"fig_gas_lng_de.html"), include_plotlyjs="directory",config=config)
fig_gas_lng_monitor_iframe_en.write_html(os.path.join(export_path,"fig_gas_lng_en.html"), include_plotlyjs="directory",config=config)
fig_gas_lng_monitor_iframe_en.write_image(os.path.join(export_path,"fig_gas_lng_en.png"), scale = 5)
fig_gas_lng_monitor_iframe_de.write_image(os.path.join(export_path,"fig_gas_lng_de.png"), scale = 5)
"""
#%% monthly gas imports ############################################################################
## note: this was so far only used for a blog, not the landing page
#
#fig_gas_de_monitor_iframe = go.Figure(fig_gas_de)
#fig_gas_de_monitor_iframe = fig_gas_de_monitor_iframe.update_layout(
#                                    title = "",
#                                    margin=dict(l=0, r=0, t=30, b=0, pad=0),
#                                    legend = {'y':-1.3})
#fig_gas_de_monitor_iframe.write_html("docs/figures/export/blog3_fig_gas_de.html", 
#                                    include_plotlyjs="directory")
#
## Export as png
#
#fig_gas_de_monitor_png = fig_gas_de_monitor_iframe
#fig_gas_de_monitor_png.update_layout(
#    width = 860, height = 430,
#    legend = {'y':-0.9})
#fig_gas_de_monitor_png.write_image("docs/figures/export/blog3_fig_gas_de.png", scale = 5)
#
##fig_gas_en_monitor_iframe = go.Figure(fig_gas_en)
##fig_gas_en_monitor_iframe = fig_gas_en_monitor_iframe.update_layout(legend = {'y':-.7},
##                                    title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
##fig_gas_en_monitor_iframe.write_html("docs/figures/monitor/landing/fig_gas_en.html",
##                                    include_plotlyjs="directory")
#
###### 3-monthly gas imports ########################################################################
#
#fig_gas_3month_de_monitor_iframe = go.Figure(fig_gas_3month_de)
#fig_gas_3month_de_monitor_iframe = fig_gas_3month_de_monitor_iframe.update_layout(
#                                    title = "", margin=dict(l=0, r=0, t=30, b=0, pad=0))
#fig_gas_3month_de_monitor_iframe.write_html("docs/figures/export/blog3_fig_gas_3month_de.html", 
#                                    include_plotlyjs="directory")
#
## Export as png
#
#fig_gas_3month_de_monitor_png = fig_gas_3month_de_monitor_iframe
#fig_gas_3month_de_monitor_png.update_layout(
#    width = 860, height = 430,
#    legend = {'y':0.5})
#fig_gas_3month_de_monitor_png.write_image("docs/figures/export/blog3_fig_gas_3month_de.png", scale = 5)
#
##fig_gas_3month_en_monitor_iframe = go.Figure(fig_gas_3month_en)
##fig_gas_3month_en_monitor_iframe = fig_gas_3month_en_monitor_iframe.update_layout(legend = {'y':.2},
##                                    title = "", margin=dict(l=0, r=0, t=0, b=0, pad=0))
##fig_gas_3month_en_monitor_iframe.write_html("docs/figures/monitor/landing/fig_gas_3month_en.html",
##                                    include_plotlyjs="directory")
#
## Alex: still not super fine tunbed; and English is missing for the next two, not sure we need it?
#
#
###### "gas tracker": weekly gas consumption and gradtagszahlen #####################################
#
#
#fig_gas_tracker_de_monitor_iframe = go.Figure(fig_gas_tracker_de)
#fig_gas_tracker_de_monitor_iframe = fig_gas_tracker_de_monitor_iframe.update_layout(
#    title = "", 
#    margin=dict(l=0, r=0, t=30, b=0, pad=0),
#    legend = {'y':-.9}
#    )
#fig_gas_tracker_de_monitor_iframe.write_html("docs/figures/export/blog3_fig_gas_tracker_de.html",
#                                    include_plotlyjs="directory")
#
## Export as png
#
#fig_gas_tracker_de_monitor_png = fig_gas_tracker_de_monitor_iframe
#fig_gas_tracker_de_monitor_png.update_layout(
#    width = 860, height = 430,
#    legend = {'y':-.7})
#fig_gas_tracker_de_monitor_png.write_image("docs/figures/export/blog3_fig_gas_tracker_de.png", scale = 5)
#
###### "gas tracker": temperature
#
#fig_gas_tracker_temp_de_monitor_iframe = go.Figure(fig_gas_tracker_temp_de)
#fig_gas_tracker_temp_de_monitor_iframe = fig_gas_tracker_temp_de_monitor_iframe.update_layout(
#    title = "", margin=dict(l=0, r=0, t=30, b=0, pad=0),
#    legend = {'y':-.1}
#    )
#fig_gas_tracker_temp_de_monitor_iframe.write_html("docs/figures/export/blog3_fig_gas_tracker_temp_de.html",
#                                    include_plotlyjs="directory")
#
## Export as png
#
#fig_gas_tracker_temp_de_monitor_png = fig_gas_tracker_temp_de_monitor_iframe
#fig_gas_tracker_temp_de_monitor_png.update_layout(
#    width = 860, height = 430,
#    legend = {'y':-0.1})
#fig_gas_tracker_temp_de_monitor_png.write_image("docs/figures/export/blog3_fig_gas_tracker_temp_de.png", scale = 5)


####################################################################################################
##### add <!DOCTYPE html> in front of every HTML ###################################################
####################################################################################################

#file_list = glob.glob(os.path.join(export_path,"*.html"))
#
#def line_prepender(filename, line):
#    with open(filename, 'r+') as f:
#        content = f.read()
#        f.seek(0, 0)
#        f.write(line.rstrip('\r\n') + '\n' + content)
#
#for file in file_list:
#    line_prepender(file,"<!DOCTYPE html> <style> html, body { height: 100% } </style>")

#%% ################################################################################################