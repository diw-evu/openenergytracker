#%% Import packages

import os
import sys
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
import datetime
import sys

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser
import statsmodels.api as sm

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%%

# import custom packages

import scripts.colors as colors
import scripts.util as oet

import re

#%% Import data
#os.chdir(os.path.realpath(__file__))

am_solar_path = os.path.join("scripts","static","am","am_solar")

bnetza_eeg_data   = pd.read_csv(os.path.join(am_solar_path,"data","pv_bnetza_eeg_data.csv"))

# Create plotly figure 
totals = bnetza_eeg_data.groupby("Lage").agg({"Netto":"sum"}).reset_index()
subtotals = bnetza_eeg_data.groupby(["Subventionstyp","Lage"]).agg({"Netto":"sum"}).reset_index()
totals["Subventionstyp"] = ""
totals = totals[["Subventionstyp","Lage","Netto"]]
totals = pd.concat([totals,subtotals],axis=0).reset_index()
totals["Subventionstyp"] = totals["Subventionstyp"].replace({"Gesetzliche EEG-Förderung":"Feste Einspeisevergütung","Ungeförderte Anlagen *":"Ungefördert"})
totals["ids"] = np.where(totals["Subventionstyp"] == "",totals["Lage"],totals["Lage"]+" - "+totals["Subventionstyp"])
totals["parents"] = np.where(totals["Subventionstyp"]=="","",totals["Lage"])
totals["labels"] = np.where(totals["Subventionstyp"]=="",totals["Lage"],totals["Subventionstyp"])

sb_fig = go.Figure()
sb_fig.add_trace(go.Sunburst(labels=totals.labels,ids = totals.ids,parents =totals.parents,values=totals.Netto/1e3,branchvalues="total",marker = dict(colors=["#00786b","#6fc8b6","#5e7c8f"]),hovertemplate = '<b>%{label} </b> <br> Zubau: %{value:.1f} GW'))

sb_fig.update_layout(
    title = "Netto-Zubau in den letzten 12 Monaten nach EEG-Förderungsart", 
    template = "simple_white",
    margin=dict(l=0,r=0,b=0,t=50,pad=0),
    font=dict(family = "'sans-serif','arial'", size=12, color='#000000'))

#sb_fig.update_layout(
#    template = "simple_white",
#    showlegend = False,
#    #width = 800,   
#    height = 400,
#    margin = dict(l=0, r=90, t=30, b=0, pad = 0),
#    font = dict(size = 14, family = "Arial"),
#)

sb_fig.write_html(os.path.join(am_solar_path,"figures","fig_am_solar3_eeg.html"),include_plotlyjs="directory")
sb_fig.write_image(os.path.join(am_solar_path,"figures","fig_am_solar3_eeg.png"), scale=5)

sb_fig.show()

# %%
