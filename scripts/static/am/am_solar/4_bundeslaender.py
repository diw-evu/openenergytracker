#%% Bundeslaender

import os
import sys
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
import datetime

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser
import statsmodels.api as sm

# import custom packages

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%%

import scripts.colors as colors
import scripts.util as oet

import re


#%% import data

am_solar_path = os.path.join("scripts","static","am","am_solar")

mastr_data   = pd.read_csv(os.path.join(am_solar_path,"data","pv_mastr_data.csv"))
mastr_data = mastr_data.assign(date=pd.to_datetime(dict(year=mastr_data.year,month=mastr_data.month,day= 1)))

potential_data = pd.read_csv(os.path.join(am_solar_path,"data","potential_data.csv"))
potential_data = potential_data.set_index("bundesland")
# %%

def build_wb_bundesland_chart(mastr_data,potential_data):
    # compute net reductions
    tp = mastr_data
    tp['reductions'] = tp['reductions'].fillna(0)
    tp['net_additions'] = tp['additions'] - tp['reductions']
    # cutoff end of last month
   # curr_mon = date.today().month
   # mon = curr_mon -1 if curr_mon -1 > 0 else 11 + (curr_mon -1)
   # yr = date.today().year if mon < 11 else date.today().year - 1
   # max_date = date(yr,mon+1,1) if mon<11 else date(yr+1,1,1)
   # min_date = date(yr-1,mon+1,1) if mon < 11 else date(yr,1,1)
   # tp = tp.query(f"date<'{max_date}'")
    tp_gm = tp.query("Lage=='Freifläche'")
    tp_gm = tp_gm.groupby(['Bundesland']).agg({'net_additions':"sum"})/1e6
    #tp = tp.query(f"date>='{min_date}'")
    tp_rt = tp.query("Lage=='Bauliche Anlagen (Hausdach, Gebäude und Fassade)' or Lage=='Bauliche Anlagen (Sonstige)' ")
    tp_rt = tp_rt.groupby(['Bundesland']).agg({'net_additions':"sum"})/1e6
    tp = pd.merge(tp_gm,tp_rt,left_index=True,right_index=True,suffixes=('_gm','_rt'))
    tp["total"] = tp["net_additions_gm"]+tp["net_additions_rt"]
    tp = tp.query('index!="Ausschließliche Wirtschaftszone"')

    # compute capacity potential (conversion factor in MW/ha)
    tp["potential_gm"] = potential_data["gm capa ARIADNE"]
    tp["potential_rt"] = potential_data["rt capa ARIADNE"]

    tp['build_out_gm'] = tp["net_additions_gm"]/tp["potential_gm"]
    tp['build_out_rt'] = tp["net_additions_rt"]/tp["potential_rt"]
    
    tp = tp.sort_values(by=["total"])
    #tp2 = tp.sort_values(by=["net_additions_rt"])
        
    fig = make_subplots(rows=1,cols=3,
                        specs=[[{},{"type":"Choropleth"},{"type":"Choropleth"}]],
                        column_widths=[0.3,0.35,0.35],
                        shared_xaxes=False,
                        shared_yaxes=False,
                        horizontal_spacing=0.0,
                        subplot_titles=["Installierte Leistung [GW] <br>","Leistung/Potenzial [%] <br> Freifläche","Leistung/Potenzial [%] <br> Bauliche Anlagen"])

    fig.add_trace(go.Bar(
        y = tp.index, x = tp["net_additions_rt"],
        name = "Auf/an baulichen Anlagen",
        marker_color = "#00786b",
        orientation = "h",
        offsetgroup = 1,
#        marker_pattern_shape = "+",
        legendgroup = "rooftop",
        hovertemplate = "%{x:.2f} GW"),
    row = 1, col = 1)
    
    fig.add_trace(go.Bar(
        y = tp.index, x = tp["net_additions_gm"],
        name = "Freifläche",
        marker_color = "#6fc8b6",
        orientation = "h",
        offsetgroup = 1,
        legendgroup="area",
        hovertemplate="%{x:.2f} GW"),
    row = 1, col = 1)

    with urlopen("https://raw.githubusercontent.com/isellsoap/deutschlandGeoJSON/main/2_bundeslaender/1_sehr_hoch.geo.json") as response:
        states = json.load(response)

    fig.add_trace(go.Choropleth(
        locations = tp.index,
        featureidkey = 'properties.name',
        geojson=states,
        z=100*tp['build_out_gm'],
        colorscale =  "RdBU",#["#FFFFFF",colors.standard["second"]],
        #colorbar_title = "Bestand relativ zum Potenzial, %",
        coloraxis = "coloraxis",
        #colorbar = dict(orientation="h",thickness=10,y=-1.0),
        #orientation = "h",
        #hoverinfo= "location+z",
        hovertemplate = "<extra> %{location} </extra> %{z:.2f} %"
        #projection = "mercator"
        #hoverlabel = tp_total["Bundesland"]
    ),1,2)
   
    fig.add_trace(go.Choropleth(
        locations = tp.index,
        featureidkey = 'properties.name',
        geojson=states,
        z=100*tp['build_out_rt'],
        colorscale = "RdBU",#["#FFFFFF",colors.standard["first"]],
        #colorbar = dict(orientation="h",thickness=10,y=-1.0),
        coloraxis = "coloraxis",
        #colorbar_title = "Bestand relativ zum Potenzial, %",
        #orientation = "h",
        #colorbar_x = 0.45,
        #hoverinfo= "location+z",
        hovertemplate = "<extra> %{location} </extra> %{z:.2f} %"
        #projection = "mercator"
        #hoverlabel = tp_total["Bundesland"]
    ),1,3)

    fig.update_layout(coloraxis={'colorscale':["#FFFFFF","#dfdc09","#f0323c"],'cmin':0,'cmax':40,'cmid': 10},coloraxis_colorbar=dict(orientation="h",thickness=10,y=-.25))

    fig.update_geos(
        fitbounds="locations",
        visible=False
        )
    fig.update_geos(projection_type="mercator")
    #fig.update_traces({'fill':{"colorbar":{"orientation":"h","y":-1.0}}})
    fig.update_coloraxes(colorbar = dict(orientation="h",thickness=10,y=-1.0),row=1,col=2)
    fig.update_coloraxes(colorbar = dict(orientation="h",thickness=10,y=-1.0),row=1,col=3)
#    fig.update_layout(autosize=false,margin=dict(l=0,r=0,b=0,t=0,pad=0),row=1,col=2)

    
    fig.update_xaxes(title_text=None,row=1,col=1, range=[0,22])
    #fig.update_xaxes(title_text=None,row=1,col=2,range=[0.29,0.00],tickformat = ',.0%')
    #fig.update_xaxes(title_text=None,row=1,col=3,range=[0.00,0.29],tickformat = ',.0%')
    fig.update_yaxes(dtick=1,row=1,col=1)
    #fig.update_yaxes(visible=False,row=1,col=2)
    #fig.update_yaxes(visible=False,row=1,col=3)
    
    fig.update_xaxes(showgrid=True, gridwidth=0.1, gridcolor='Lightgray',row=1,col=1)
    
    #fig.add_annotation(
    #    text = "Installierte Leistung [GW]", x = 0.5, y = 1.08, col = 1, row = 1, 
    #    xref = "x domain", yref = "y domain", xanchor = "center",
    #    showarrow = False)
    #fig.add_annotation(
    #    text = "Installierte Leistung relativ zu Potenzial [%]", x = 0, y = 1.08, col = 3, row = 1, 
    #    xref = "x domain", yref = "y domain", xanchor = "center",
    #    showarrow = False)
    
    fig.update_layout(
        title = {"text": "Photovoltaik in den Bundesländern", "y": 0.99},
        margin = dict(l=0,r=0,b=0,t=60,pad=0),
        font = dict(family = "'sans-serif','arial'", size=10, color='#000000'),
        hovermode = "y unified",
        barmode = "stack",
        template = "simple_white",
        legend={"yanchor":"bottom","xanchor":"left","y":0.05,"x":0.05,})
    
    return fig

fig = build_wb_bundesland_chart(mastr_data,potential_data)

fig.update_annotations(font_size=11)

fig.update_layout(
    template = "simple_white",
    margin = dict(l=0, r=90, t=40, b=0, pad = 10),
    font = dict(size = 10, family = "Arial"),
)

fig.write_html(os.path.join(am_solar_path,"figures","fig_am_solar4_bl.html"),include_plotlyjs="directory")
fig.write_image(os.path.join(am_solar_path,"figures","fig_am_solar4_bl.png"), scale=5)

fig.show()
# %%
