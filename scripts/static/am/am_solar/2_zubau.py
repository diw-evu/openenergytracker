# %%

import os
import sys
import pandas as pd
import numpy as np
import plotly.express as px
from plotly.subplots import make_subplots
from plotly import graph_objects as go
from pandas.tseries.offsets import *

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%%

import scripts.util as oet

# %% Load data

bestand_oet = oet.prepare_data('https://gitlab.com/diw-evu/oet/openenergytracker/-/raw/main/docs/germany/data/pv.csv',"months")
bestand_oet = oet.calculate_trend(bestand_oet,"actual","12-months")
bestand_oet = bestand_oet[["date","plan","plan_linear"]]
bestand_oet.date += MonthEnd()

#%% MASTR

am_solar_path = os.path.join("scripts","static","am","am_solar")

#mastr = pd.read_csv(os.path.join(am_solar_path,"data","pv_mastr_data.csv"))
mastr = pd.read_csv(os.path.join('https://gitlab.com/diw-evu/oet/openenergytracker/-/raw/main/docs/germany/data/pv_mastr_data.csv'))

# Create date column
mastr["date"] = pd.to_datetime(dict(year=mastr.year, month=mastr.month, day=15))

#%% Bestand

today = pd.Timestamp.today().strftime("%Y-%m-%d")

bestand = mastr.groupby("date").agg({'net_additions':'sum'}).reset_index()
bestand["actual"] = bestand["net_additions"].cumsum()
bestand = bestand.query("date >= '2017-01-01'").reset_index(drop=True)
bestand["actual"] = bestand["actual"] / (1e6)
bestand["actual"] = np.where(bestand["date"] > today,np.nan,bestand["actual"])
bestand_ext = pd.concat([bestand,pd.DataFrame({'date': pd.date_range(start=bestand.date.iloc[-1], periods=72, freq='ME')})], ignore_index=True)

bestand_ext.date += MonthEnd()
bestand_ext = oet.calculate_trend(bestand_ext,"actual","12-months")
bestand_ext = bestand_ext.merge(bestand_oet, on="date", how="left")

bestand_ext["net_additions"] = bestand_ext["net_additions"].replace({0:np.nan})

#%% Zubau: After 2017
zubau = mastr.query("date >= '2017-01-01'")

# Rename
zubau["Lage"] = zubau["Lage"].replace(
    {'Bauliche Anlagen (Hausdach, Gebäude und Fassade)' : 'Bauliche Anlage',
     'Bauliche Anlagen (Sonstige)' : 'Bauliche Anlage', 
     'Freifläche' : 'Freifläche',
     'Gewässer' : 'Freifläche',
     'Großparkplatz' : 'Freifläche',
     'Steckerfertige Solaranlage (sog. Balkonkraftwerk)' : 'Balkonkraftwerk'})

#zubau['bins'] = np.where(
#   (zubau['bins'] == '(0.0, 10.0]') | (zubau['bins'] == '(10.0, 25.0]'), "bis 25 kW", "größer 25 kW")

zubau["bins"] = np.where(zubau["bins"] == "(0.0, 10.0]","bis 10 kW","größer 10 kW")

zubau_grouped = zubau.groupby(["date","Lage","bins"]).agg(
    {'net_additions': 'sum'}).reset_index()

zubau_grouped["groups"] = zubau_grouped["Lage"] + " (" + zubau_grouped["bins"] + ")"

zubau_grouped["groups"] = zubau_grouped["groups"].replace(
    {"Balkonkraftwerk (bis 10 kW)":"Balkonkraftwerk",
     "Balkonkraftwerk (größer 10 kW)":"Balkonkraftwerk",
     "Freifläche (bis 10 kW)":"Freifläche",
     "Freifläche (größer 10 kW)":"Freifläche",
     })

zubau_grouped2 = zubau_grouped.groupby(["date","groups"]).agg({'net_additions':'sum'}).reset_index()

zubau_grouped2["net_additions"] = zubau_grouped2["net_additions"] / (1e6)

zubau_pivot = zubau_grouped2.pivot(index="date",columns="groups",values="net_additions").reset_index()

#%%

data_figure = bestand_ext.query("date <= '2030-12-31'")

subfig = make_subplots(
    column_titles = ["Bestand","Zubau"],
    rows=1, cols=2,
    vertical_spacing=0.1,)

# Figure 1: PV Bestand
colnr = 1
rownr = 1

subfig.add_scatter(
    x=data_figure["date"],
    y=data_figure["actual"], 
    name = "Bestand",
    fill='tozeroy',
    fillcolor = "rgba(252, 186, 3, 0.50)",
    line_color = "rgba(252, 186, 3, 1)",
    hovertemplate='%{y:.1f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=data_figure["date"],
    y=data_figure["trend_12months"],
    name = "Trend",
    fill='tozeroy',
    fillcolor = "rgba(252, 186, 3, 0.1)",
    line_color = "rgba(252, 186, 3, 0.25)",
    hovertemplate='%{y:.1f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=data_figure["date"],
    y=data_figure["plan_linear"], 
    name = "Regierungsziel (linear)",
    line_color = "rgba(0, 0, 0, 0.5)",
    line_dash = "dot",
    mode = "lines",
    line_width = 1.5,
    hovertemplate='%{y:.1f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=data_figure["date"],
    y=data_figure["plan"], 
    name = "Regierungsziel (Jahr)",
    text = data_figure["plan"].apply(lambda x: '{0:1.0f} GW'.format(x)),
    mode = "markers+text",
    textposition="top left",
    line_color = "rgba(0, 0, 0, 0.5)",
    hovertemplate='%{y:.1f} GW',
    col=colnr, row=rownr)

subfig.add_annotation(text="Ist", row = 1, col = 1,
                        x="2022",y=25,
                        arrowhead=2,
                        showarrow=False,
                        #xshift=15,yshift=-15,
                        #ax = 5,
                        arrowcolor = "rgba(0, 0, 0,1)",
                        arrowwidth = 1,
                        font=dict(size=14,family="Arial",
                                  color = "rgb(0, 0, 0)"))

subfig.add_annotation(text="Trend", row = 1, col = 1,
                        x="2028",y=25,arrowhead=2,
                        #xshift=15,yshift=-50,
                        ay = 25,
                        arrowwidth = 1,
                        showarrow=False,
                        #ax = 5,ay =-15,
                        arrowcolor = "rgba(136, 136, 136, 1)",
                        font=dict(size=14,family="Arial",
                                  color = "rgba(136, 136, 136, 1)"))

subfig.add_annotation(text="Regierungsziele", row = 1, col = 1,
                        x="2027",y=120,arrowhead=2,
                        textangle=0,
                        showarrow=True,
                        xshift=5,
                        #yshift=30,
                        ax = 35, 
                        ay = 35,
                        arrowwidth = 1.5,
                        arrowcolor = "rgba(0, 0, 0, .5)",
                        font=dict(size=12,family="Arial",
                                  color = "rgba(0, 0, 0,.5)"))

subfig.add_annotation(text="Juli 2024: <br> 90,8 GW", row = 1, col = 1,
                        x="2024-01",y=90,arrowhead=2,
                        textangle=0,
                        align="left",
                        showarrow=True,
                        #xshift=30,
                        #yshift=30,
                        ax = -75, 
                        ay = 10,
                        arrowwidth = 1.5,
                        arrowcolor = "rgba(252, 186, 3, 1)",
                        font=dict(size=12,family="Arial",
                                  color = "rgba(252, 186, 3, 1)"))

# Figure 2: PV Zubau
colnr = 2
rownr = 1

subfig.add_scatter(
    x=zubau_pivot["date"],
    y=zubau_pivot["Balkonkraftwerk"], 
    name = "Balkon",
    stackgroup = "one",
    line=dict(width=0),
    fillcolor = "rgba(252, 186, 3, .25)",
    line_color = "rgba(252, 186, 3, .25)",
    hovertemplate='%{y:.2f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=zubau_pivot["date"],
    y=zubau_pivot["Bauliche Anlage (bis 10 kW)"], 
    name = "Bauliche Anlage (bis 10 kW)",
    line=dict(width=0),
    stackgroup = "one",
    fillcolor = "rgba(252, 186, 3, 1)",
    line_color = "rgba(252, 186, 3, 1)",
    hovertemplate='%{y:.2f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=zubau_pivot["date"],
    y=zubau_pivot["Bauliche Anlage (größer 10 kW)"], 
    name = "Bauliche Anlage (größer 10 kW)",
    line=dict(width=0),
    stackgroup = "one",
    fillcolor = "rgba(176, 130, 2, 1)",
    line_color = "rgba(176, 130, 2, 1)",
    hovertemplate='%{y:.2f} GW',
    col=colnr, row=rownr)

subfig.add_scatter(
    x=zubau_pivot["date"],
    y=zubau_pivot["Freifläche"], 
    name = "Freifläche und Sonstige",
    line=dict(width=0),
    stackgroup = "one",
    fillcolor = "rgba(17, 174, 157,1)",
    line_color = "rgba(17, 174, 157,1)",
    hovertemplate='%{y:.2f} GW',
    col=colnr, row=rownr)

x_loc = 1.01

subfig.add_annotation(text="Bauliche Anlange<br>(bis 10 kW)",
                      xref='paper', x=x_loc,
                      yref='paper', y=0.10,
                      arrowhead=2,
                      textangle=0,
                      showarrow=True,
                      #xshift=30,
                      #yshift=30,
                      ax = 50, 
                      ay = -30,
                      arrowwidth = 1.5,
                      arrowcolor = "rgba(252, 186, 3, 1)",
                      font=dict(size=12,family="Arial",
                                  color = "rgba(252, 186, 3, 1)"))

subfig.add_annotation(text="Bauliche Anlange<br>(größer 10 kW)",
                      xref='paper', x=x_loc,
                      yref='paper', y=0.30,
                      arrowhead=2,
                      textangle=0,
                      showarrow=True,
                      #xshift=30,
                      #yshift=30,
                      ax = 50, 
                      ay = -20,
                      arrowwidth = 1.5,
                      arrowcolor = "rgba(176, 130, 2, 1)",
                      font=dict(size=12,family="Arial",
                                  color = "rgba(176, 130, 2, 1)"))

subfig.add_annotation(text="Freifläche<br>und Sonstige",
                      xref='paper', x=x_loc,
                      yref='paper', y=0.70,
                      arrowhead=2,
                      textangle=0,
                      showarrow=True,
                      #xshift=30,
                      #yshift=30,
                      ax = 50, 
                      #ay = 10,
                      arrowwidth = 1.5,
                      arrowcolor = "rgba(17, 174, 157,1)",
                      font=dict(size=12,family="Arial",
                                  color = "rgba(17, 174, 157,1)"))

subfig.add_annotation(text="Balkon",
                      xref='paper', x=x_loc,
                      yref='paper', y=0.01,
                      arrowhead=2,
                      textangle=0,
                      showarrow=True,
                      #xshift=30,
                      #yshift=30,
                      ax = 30, 
                      ay = -10,
                      arrowwidth = 1.5,
                      arrowcolor = "rgba(120, 120, 120, 1)",
                      font=dict(size=12,family="Arial",
                                  color = "rgba(120, 120, 120, 1)"))

subfig.update_yaxes(range=[0,250], row=1, col=1, title = "Gigawatt", dtick = 50, titlefont=dict(size=13,family="Arial"))
subfig.update_yaxes(range=[0,1.5], row=1, col=2, title = None, dtick = 0.250, titlefont=dict(size=13,family="Arial"))
subfig.update_xaxes(range=["2017","2031-03-31"],tickformat="%Y",dtick="M24",col=1)#,dtick=1)
subfig.update_xaxes(range=["2017",today],tickformat="%Y",dtick="M12",col=2)#,dtick=1)
subfig.update_xaxes(hoverformat='%b %Y')

subfig.update_layout(
    template = "simple_white",
    showlegend = False,
    #width = 700,   height = 300,
    margin = dict(l=0, r=100, t=25, b=0, pad = 0),
    font = dict(size = 12, family = "Arial"),
    hovermode = "x unified",
)

subfig.write_html(os.path.join(am_solar_path,"figures","fig_am_solar2_DE.html"),include_plotlyjs="directory")
subfig.write_image(os.path.join(am_solar_path,"figures","fig_am_solar2_DE.png"), scale=5)

subfig.show()

#%% 