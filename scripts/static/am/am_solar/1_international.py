#%% Import packages

import os
import sys
import pandas as pd
import numpy as np
import plotly.express as px
from plotly.subplots import make_subplots

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

# %% Load data

am_solar_path = os.path.join("scripts","static","am","am_solar")

pv_imp  = pd.read_excel(os.path.join(am_solar_path,"data","IRENA_Stats_Extract_ 2024_H1_V1.xlsx"),sheet_name='All Data')

pv_imp = pv_imp.rename({
    'Region': 'region',
    'Country': 'n',
    'Sub-Technology': 'tech',
    'Year': 'year',
    'Electricity Installed Capacity (MW)': 'value',
    #'Electricity Generation (GWh)': 'value',
    },
                             axis = 1)

pop_imp = pd.read_csv(os.path.join(am_solar_path,"data","World Bank Population Data.csv"))
pop_imp = pop_imp.drop(["Series Name","Series Code","Country Code"], axis=1)

# %% Aggregated

pv_region = pv_imp[['region','tech','year','value']]
pv_region = pv_region.query("tech == ['Off-grid Solar photovoltaic','On-grid Solar photovoltaic', 'Solar mini-grids']")
pv_region = pv_region.groupby(['region','year']).agg({'value':'sum'}).reset_index()
pv_region["value"] = pv_region["value"] / 1000

pv_region.loc[~pv_region["region"].isin(['Asia','Europe','North America']), "region"] = "Andere"

pv_region["region"] = pv_region["region"].replace({
    "Asia":"Asien",
    "Europe":"Europa",
    "North America": "Nordamerika"
})

pv_region = pv_region.groupby(["year","region"]).agg({"value":"sum"}).reset_index()

pv_region_pivot = pv_region.pivot(index="year",columns="region",values="value").reset_index()

# China und Deutschland

china = pv_imp.query("n=='China' & tech == ['Off-grid Solar photovoltaic','On-grid Solar photovoltaic', 'Solar mini-grids']").groupby("year").agg({"value":"sum"}).reset_index()
china["value"] = china["value"] / 1000

deutschland = pv_imp.query("n=='Germany' & tech == ['Off-grid Solar photovoltaic','On-grid Solar photovoltaic', 'Solar mini-grids']").groupby("year").agg({"value":"sum"}).reset_index()
deutschland["value"] = deutschland["value"] / 1000

pv_region_pivot["Asien (ohne China)"] = pv_region_pivot["Asien"] - china["value"]
pv_region_pivot["Europa (ohne Deutschland)"] = pv_region_pivot["Europa"] - deutschland["value"]

pv_region_pivot["Deutschland"] = deutschland["value"]
pv_region_pivot["China"] = china["value"]

pv_region_long = pv_region_pivot.melt(id_vars=["year"], value_vars=["Europa (ohne Deutschland)","Deutschland","Nordamerika","Asien (ohne China)","China","Andere"], var_name="region")

# %% Per Capita

pop     = pop_imp.melt(id_vars=["Country Name"])
pop["variable"] = pop["variable"].str[:4].astype(int)
pop = pop.dropna()
pop = pop.rename({
    "Country Name": "n",
    "variable": "year",
}, axis = 1)

pv_cap = pv_imp[['n','tech','year','value']]
pv_cap = pv_cap.query("tech == ['Off-grid Solar photovoltaic','On-grid Solar photovoltaic', 'Solar mini-grids']")
pv_cap = pv_cap.groupby(['n','year']).agg({'value':'sum'}).reset_index()

pv_cap['n'] = pv_cap['n'].replace({
    'Netherlands (Kingdom of the)':'Netherlands'})

pv_merge = pd.merge(pv_cap, pop, on = ['n','year'])
pv_merge = pv_merge.rename({
    'value_x': 'value_pv',
    'value_y': 'value_pop'
}, axis = 1)

pv_merge["value"] = pv_merge["value_pv"]*1000 / pv_merge["value_pop"].astype(int)

data_figure = pv_merge.query("n == ['Germany','Netherlands','Belgium','United States','China', 'Italy', 'Spain', 'France', 'Australia','Poland']")

data_figure["n"] = data_figure["n"].replace({
    "Australia":"Australien",
    "Germany":"Deutschland",
    "Netherlands":"Niederlande",
    "Belgium":"Belgien",
    "United States":"USA",
    "China":"China",
    "Italy":"Italien",
    "Spain":"Spanien",
    "France":"Frankreich",
    "Poland":"Polen"})

# %%

colnr = 1
rownr = 1

hovertemp1 = '%{x:.0f}: '+'%{y:.1f} GW'
hovertemp2 = '%{x:.0f}: '+'%{y:.2f} kW'

subfig = make_subplots(
    column_titles = ["Gesamt","Pro Kopf"],
    rows=1, cols=2,
    horizontal_spacing=0.1)

subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["Deutschland"],
    name = "Deutschland",
    legend = "legend1",
    marker_color = "rgba(31, 119, 180,1)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)
subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["Europa (ohne Deutschland)"],
    name = "Europa (ohne Deutschland)",
    legend = "legend1",
    marker_color = "rgba(31, 119, 180,.5)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)
subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["Nordamerika"], 
    name = "Nordamerika",
    legend = "legend1",
    marker_color = "rgb(255, 127, 14)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)
subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["China"], 
    name = "China",
    legend = "legend1",
    marker_color = "rgba(0,128,0,1)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)
subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["Asien (ohne China)"], 
    name = "Asien (ohne China)",
    legend = "legend1",
    marker_color = "rgba(0,128,0,.5)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)
subfig.add_bar(
    x=pv_region_pivot["year"],
    y=pv_region_pivot["Andere"], 
    name = "Andere",
    legend = "legend1",
    marker_color = "rgba(150,150,150,1)",
    hovertemplate=hovertemp1,
    col=colnr, row=rownr)

pv_region_2023 = pv_region_long.query("year == 2023")
pv_region_2023["cumsum"] = pv_region_2023["value"].cumsum()

#for n in pv_region_2023["region"].unique():
#    data_temp = pv_region_2023.query("region == @n")
#    shift = 100
#    if n == "Asien (ohne China)":
#        n = "Asien <br> (ohne China)"
#    if n == "Europa (ohne Deutschland)":
#        n = "Europa <br> (ohne Deutschland)"
#        shift = 200
#    if n == "China":
#        shift = 200
#    if n == "Andere":
#        shift = 50
#    subfig.add_annotation(
#        xref='paper', x=0.47,
#        yref='paper', y=((data_temp["cumsum"]-shift) / 1500).values[0],
#        text = n,
#        align = "center",
#        showarrow=False)

colorsequence = px.colors.qualitative.T10

countrylist = data_figure["n"].unique()

for i in range(1,len(countrylist)+1):
    n    = countrylist[i-1]
    data = data_figure.query("n == @n")
    subfig.add_scatter(
        x = data["year"],
        y = data["value"],
        showlegend=False,
        mode="lines",
        name = n,
        #legend = "legend2",
        line = dict(color = colorsequence[i-1],
                    width = 2.5),
        hovertemplate=hovertemp2,
        col = 2, row = 1)
        
    # labeling the right_side of the plot
    if n == "China":
        shift =  -1
        ashift = -5
    elif n == "Polen":
        shift = -5
        ashift = 5
    else:
        shift = 0
        ashift = 0
    subfig.add_annotation(
        xref='paper', 
        yref='paper',
        x=1.01, 
        y=data["value"].iloc[-1] / 1.5,
        xshift = -15,
        yshift = shift,
        ax = 10, 
        ay = ashift,
        text=n,
        xanchor='left', yanchor='middle',
        showarrow=True,
        arrowcolor=colorsequence[i-1],
        arrowhead=0,
        arrowwidth=2,
        #col = 2, row = 1
    )
    
    #annotations.append(dict(xref='paper', x=0.95, y=y_trace[11],
    #                              xanchor='left', yanchor='middle',
    #                              text='{}%'.format(y_trace[11]),
    #                              font=dict(family='Arial',
    #                                        size=16),
    #                              showarrow=False))

subfig.update_yaxes(range=[0,1500], row=1, col=1, title = "Gigawatt", dtick = 250, titlefont=dict(size=13,family="Arial"), title_standoff = 5)
subfig.update_yaxes(range = [0,1.5], dtick = 0.25, row=1, col=2, title = "Kilowatt", titlefont=dict(size=13,family="Arial"), title_standoff = 0)
subfig.update_xaxes(dtick = 5, range = [2005, 2024])
#subfig["data"][10].update(line_color = "rgba(0,0,0,1)",line_width = 3)

subfig.update_layout(template = "simple_white",
                     barmode = "stack",
                     title = None,
                     margin = dict(l=0, r=80, t=25, b= 50, pad = 0),
                     font = dict(size = 12, family = "Arial"),
                     legend1 = dict(title = None, orientation = "v", y = 1, x = 0, 
                                    xanchor = "left", yanchor = "top"), 
                     #height = 430
                     )

subfig.write_html(os.path.join(am_solar_path,"figures","fig_am_solar1_international.html"), include_plotlyjs="directory")
subfig.write_image(os.path.join(am_solar_path,"figures","fig_am_solar1_international.png"), scale=5)

subfig.show()

#%%