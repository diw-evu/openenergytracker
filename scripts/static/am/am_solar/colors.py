#%%
#import met_brewer
#
#pal = met_brewer.met_brew(name="Demuth", n=12, brew_type="continuous")
#pal

# "#591c19", 
# "#9b332b", 
# "#b64f32",
# "#d39a2d",
# "#f7c267",
# "#b9b9b8",
# "#8b8b99",
# "#5d6174",
# "#41485f",
# "#262d42"
#%%
#print(met_brewer.export(name = "Demuth"))

#%%

standard = {
    "first"               : "#3c435a",
    "first_light"         : "#9da1ac",
    "plan_target"         : "#ac452f",
    "plan_target_low"     : "#ac452f",
    "plan_target_high"    : "#dda53d",
    "plan_target_belfort" : "#2b614e",
    "plan_linear"         : "#ac452f",
    "plan_linear_low"     : "#ac452f",
    "plan_linear_high"    : "#dda53d",
    "plan_log"            : "#c37130",
    "trend1"              : "#bfbab1",
    "trend2"              : "#9898a1",
    "ar_main"             : "#d39a2d",
    "ar_main_inter"       : "#53586c",
    "ar_background"       : "rgba(211,154,45,0.25)",
    "ar_background2"      : "rgba(191, 186, 177, 0.5)",
    "second"              : "#2A6A89",
    "third"               : "#0094A4",
    "fourth"              : "#18BCA0",
    "fifth"               : "#8BE087",
    "misc"                : "#262d42",
    }

#color_standard = {
#    'first'         : '#262d42',
#    'first_light'   : '#8f2f28',
#    'second'        : '#ac452f',
#    'plan_target'   : '#c37130',
#    'plan_linear'   : '#dda53d',
#    'plan_log'      : '#f1c16e',
#    'trend1'        : '#bfbab1',
#    'trend2'        : '#9898a1',
#    'ar_main'       : '#727485',
#    'ar_main_inter' : '#53586c',
#    'ar_background' : '#3c435a',
#    'misc'          : '#262d42'
#    }

#%% DIW inspired

standard2 = {
    "first"         : "#13c0b0",
    "first_light"   : "#8f2f28",
    "second"        : "#ac452f",
    "plan_target"   : "#9b332b",
    "plan_linear"   : "#9b332b",
#    "plan_linear"   : "#dda53d",
    "plan_log"      : "#f1c16e",
    "trend1"        : "#bfbab1",
    "trend2"        : "#9898a1",
#   "ar_main"       : "#727485",
    "ar_main"       : "#d39a2d",
    "ar_main_inter" : "#53586c",
#   "ar_background" : "#3c435a",
    "ar_background" : "rgba(211, 154, 45, 0.25)",
    "misc"          : "#262d42"
    }


diw = {
    'first'         : 'rgba(0,120,107,1)',
    'first_light'   : 'rgba(0,120,107,0.5)',
    'second'        : 'rgba(111,200,182,1)',
    'plan_target'   : 'rgba(105,70,135,1)',
    'plan_linear'   : 'rgba(105,70,135,1)',
    'plan_log'      : 'rgba(25,90,150,0.75)',
    'trend1'        : 'rgba(94,124,143,1)',
    'trend2'        : 'rgba(23,38,36,1)',
    'ar_main'       : 'rgba(236,102,8,1)',
    'ar_main_inter' : 'rgba(236,102,8,1)',
    'ar_background' : 'rgba(236,102,8,0.25)',
    'third'         : 'rgba(110,183,75,1)',
    'fourth'        : 'rgba(105,75,54,1)',
    'fifth'         : 'rgba(174,57,63,1)',
    }