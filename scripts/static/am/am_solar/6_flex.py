#%% Import packages

import os
import sys
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
import datetime

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser
import statsmodels.api as sm

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%%

import scripts.colors as colors
import scripts.util as oet

import re

#%% Import data	
# Import data

am_solar_path = os.path.join("scripts","static","am","am_solar")

mv_rel = pd.read_csv(os.path.join(am_solar_path,"data","market_values_relative.csv"))
gen_shares = pd.read_csv(os.path.join(am_solar_path,"data","gen_shares.csv"))

mv_rel['Date'] = pd.to_datetime(mv_rel['Date'],dayfirst=True)
gen_shares['Date'] = pd.to_datetime(gen_shares['Date'],dayfirst=True)

hourly_elec = pd.read_csv(os.path.join(am_solar_path,"data","hourly_electr_all.csv"))

#%% Create figure
types = ['Wind onshore','Wind offshore','Solar']
titel = ['Wind and Land','Wind auf See','Photovoltaik']
colors = ["#5e7c8f","#195a96","#dfdc09"]

fig2 = make_subplots(rows=2,cols=2,specs=[[{"colspan":2},None],[{},{}]],subplot_titles=("Erzeugungsanteil vs. Marktwertfaktor","Winter","Sommer"))
for k in range(len(types)):
    #fig2.add_trace(go.Scatter(x=100*mv_rel[types[k]],y=100*gen_shares[types[k]],mode='none',name=types[k],customdata = gen_shares["Date"],hovertemplate='{customdata}'))
    fig2.add_trace(go.Scatter(x=100*gen_shares[types[k]],y=100*mv_rel[types[k]],mode='markers',name=titel[k],hovertemplate='Erzeugungsanteil: %{x:.2f}<br>Marktwertfaktor: %{y:.2f}',
    marker=dict(color=colors[k]),legendgroup = types[k]),row=1,col=1)
    X = sm.add_constant(100*gen_shares[types[k]])
    y = 100*mv_rel[types[k]]
    mod = sm.OLS(y, X)
    res = mod.fit()
    preds = res.predict()
    fig2.add_trace(go.Scatter(x=100*gen_shares[types[k]],y=preds,mode='lines',name=titel[k]+' regression',legendgroup = types[k],line=dict(color=colors[k]),showlegend=False),row=1,col=1)
    if k == 2:
        text = "Marktwertfaktor (%) = "+str(res.params[0].round(2))+str(res.params[1].round(2))+" x Erzeugung (%)"
        #fig2.add_annotation(x=15,y=110,text=text,font=dict(color="#FFFFFF"),showarrow=False)
        #fig2.update_annotations(bgcolor=colors[k])
fig2.update_xaxes(title='Erzeugungsanteil, %',linecolor='black',ticks='outside',range=[0,50],row=1,col=1)
fig2.update_yaxes(title='Marktwertfaktor, %',linecolor='black',ticks='outside',range=[50,150],row=1,col=1)
fig2.update_layout(
    plot_bgcolor='white',
    hovermode         = "closest",
    showlegend=True,
    template = "simple_white",
    legend=dict(
    x=.8,
    y=1,
    title_font_family='arial',
    font=dict(
        family="arial",
        size=14,
        color="black"
    ),
    borderwidth=1
)
)


hourly_elec = hourly_elec[['Date','Value']]
hourly_elec.Date = pd.to_datetime(hourly_elec.Date)
hourly_elec["year"] = hourly_elec.Date.dt.year

hourly_elec["month"] = hourly_elec.Date.dt.month

hourly_elec["season"] = np.select([(hourly_elec["month"] >= 4) & (hourly_elec["month"] <= 9)], ["summer"], default="winter")
hourly_elec["hour"] = hourly_elec.Date.dt.hour

avgs = hourly_elec.groupby(["year","season","hour"]).agg({"Value":"mean"}).reset_index()
seas_avg = hourly_elec.groupby(["year","season"]).agg({"Value":"mean"}).reset_index()
avgs = avgs.merge(seas_avg, on = ["year","season",], suffixes = ("","_seas"))


avgs["factor"] = avgs["Value"]/avgs["Value_seas"]


avgs["Id"] = avgs["year"].astype(str)+"_"+avgs["season"].astype(str)

winter = avgs[avgs["season"]=="winter"]
summer = avgs[avgs["season"]=="summer"]

summer = summer[["year","hour","factor"]].pivot(index="hour",columns="year",values="factor")
winter = winter[["year","hour","factor"]].pivot(index="hour",columns="year",values="factor")

cols = ['#E0E0E0','#E0E0E0','#E0E0E0','#E0E0E0', '#C0C0C0', '#A0A0A0', '#808080', '#606060','#404040','#00786b']
cols = dict(zip(summer.columns,cols))

for y in winter.columns:
    
    fig2.add_trace(go.Scatter(x=winter.index,y=winter.loc[:,y],mode='lines',marker = dict(color=cols[y]),name=str(y),legendgroup=str(y),showlegend=False),row=2,col=1)
    fig2.add_trace(go.Scatter(x=summer.index,y=summer.loc[:,y],mode='lines',marker = dict(color=cols[y]),name=str(y),legendgroup=str(y),showlegend=False),row=2,col=2)

fig2.update_yaxes(title='Preis relativ zum <br> Tagesdurchschnitt',linecolor='black',ticks='outside',range=[0,2],row=2,col=1)
fig2.update_yaxes(title='',linecolor='black',ticks='outside',range=[0,2],row=2,col=2)
fig2.update_xaxes(title='Stunde des Tages',linecolor='black',ticks='outside',row=2,col=1)


fig2.update_layout(template = "simple_white",showlegend=True)
fig2.add_annotation(x=20,y=summer[2024].iloc[-4],text="2024",row=2,col=2,font = dict(color=cols[2024],size=14),arrowcolor=cols[2024],arrowhead=1)
fig2.add_annotation(x=11,y=summer[2023].iloc[11],text="2023",row=2,col=2,font = dict(color=cols[2023],size=14),arrowcolor=cols[2023],arrowhead=1)


fig2.update_layout(
    template = "simple_white",
    margin = dict(l=0, r=0, t=25, b=0, pad = 0),
    font = dict(size = 12, family = "Arial"),
)

fig2.write_html(os.path.join(am_solar_path,"figures","fig_am_solar6_markt.html"),include_plotlyjs="directory")
fig2.write_image(os.path.join(am_solar_path,"figures","fig_am_solar6_markt.png"), scale=5)

fig2.show()

# %%
