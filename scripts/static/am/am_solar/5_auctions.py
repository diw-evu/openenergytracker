#%% Import packages

import os
import sys
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
import datetime

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser
import statsmodels.api as sm

# import custom packages

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%%

import scripts.colors as colors
import scripts.util as oet

import re

#%%

am_solar_path = os.path.join("scripts","static","am","am_solar")


# Function to download auction data
def get_table_from_url(url):
    skip = 5 if 'Solar2' in url else 6
    skip = 7 if 'solar1' in url else skip
    footer = 3 if 'solar1' in url else 2
    data = pd.read_excel(url,sheet_name='Übersicht',skiprows=skip,skipfooter=footer)
    df = pd.DataFrame({'second' : data.iloc[0,]}).reset_index()
    # Replace 'Unnamed' in 'index' column by previous entry
    df['index'] = df['index'].where(~df['index'].str.contains('Unnamed'), df['index'].shift())
    df['index'] = df['index'].where(~df['index'].str.contains('Unnamed'), df['index'].shift())


    # Append 'second' to 'index' if 'second' is not NaN or NaT
    df['index'] = df.apply(lambda row: row['index'] + ' ' + str(row['second']) if pd.notnull(row['second']) else row['index'],axis=1)
    df['index'] = df['index'].apply(lambda row: re.sub(r'[0-9]','',row))
    df['index'] = df['index'].str.replace('.', '')
    df['index'] = df['index'].str.replace('\n', '')
    data.columns = df['index']
    data.drop(axis=0,index=0,inplace=True)

    # Deal with duplicate columns
    test = data['Zuschlagsmenge (kW)']
    if len(test.columns) > 2:
        test.columns = ['Initial','Mit Sicherung','Nachtrag']
    else:
        test.columns = ['Mit Sicherung','Nachtrag']
    test['Zuschlagsmenge (kW)'] = test["Mit Sicherung"] + test['Nachtrag'].fillna(0)
    data['Zuschlagsvolumen (kW)'] = test['Zuschlagsmenge (kW)']

    return data

# Function to build auction chart

def create_auction_plot(data, technology,cpi_data):
    fig_dta = data[['Gebotstermin','Ausschreibungsvolumen (kW)','Zuschlagsvolumen (kW)','Zuschlagswert (ct/kWh) Gew Mittel','Höchstwert (ct/kWh)','Preisregel','Gebotswerte mit Zuschlag (ct/kWh) Min','Gebotswerte mit Zuschlag (ct/kWh) Max']]
    fig_dta['Gebotstermin'] = pd.to_datetime(fig_dta['Gebotstermin'])
    fig_dta = fig_dta.set_index('Gebotstermin').merge(cpi_data,how='left',left_index=True,right_index=True)
    fig_dta = fig_dta.reset_index()
    fig_dta['CPI'] = fig_dta['CPI']/100
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Höchstwert (ct/kWh)'],
                                mode='none',
                            customdata=fig_dta['Preisregel'],
                            showlegend=False,
                            name = 'Preisregel',
                                hovertemplate='%{customdata}',
                                marker=dict(color="#c37130",symbol='x',size=10)),
                                secondary_y=True,
                                )
    fig.add_trace(go.Bar(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Ausschreibungsvolumen (kW)']/1e3,
                            customdata=fig_dta['Preisregel'],
                            name='Auschreibungsmenge',
                            marker=dict(color="#3c435a",),
                            hovertemplate='%{y:.2f} MW'),
                            secondary_y=False)
    fig.add_trace(go.Bar(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Zuschlagsvolumen (kW)']/1e3,
                            name='Zuschlagsmenge',
                            marker=dict(color="#2A6A89"),
                            hovertemplate='%{y:.2f} MW'),
                            secondary_y=False)
    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Höchstwert (ct/kWh)']/fig_dta['CPI'],
                                mode='markers',
                            customdata=fig_dta['Preisregel'],
                                name='Höchstwert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kWh',
                                marker=dict(color="#c37130",symbol='x',size=10),visible = "legendonly"),
                                secondary_y=True)

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Zuschlagswert (ct/kWh) Gew Mittel']/fig_dta['CPI'],
                            mode='markers',
                            name='Gew. Zuschlagswert, 2020ct/kWh',
                            hovertemplate='%{y:.2f} ct/kWh',
                            marker=dict(color="#ac452f",size=10)),
                            secondary_y=True)

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Gebotswerte mit Zuschlag (ct/kWh) Min']/fig_dta['CPI'],
                                mode='markers',
                            #customdata=fig_dta['Preisregel'],
                            name = 'Minimum Zuschlagswert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kW',
                                marker=dict(size=5,color="#ac452f",opacity=0.7),visible = "legendonly"),
                                secondary_y=True)    
    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Gebotswerte mit Zuschlag (ct/kWh) Max']/fig_dta['CPI'],
                                mode='markers',
                            #customdata=fig_dta['Preisregel'],
                            name = 'Maximum Zuschlagswert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kWh',
                                marker=dict(size=5,color="#ac452f",opacity=0.7),visible = "legendonly"),
                                secondary_y=True)                                      
    fig.update_yaxes(title=technology+", MW",linecolor='black',ticks='outside',range = [0,3500],secondary_y=False)
    fig.update_yaxes(title="2020ct/kWh",linecolor='black',ticks='outside',range = [0,12],secondary_y=True,showgrid=False)

    fig.update_layout(
        xaxis= dict(title='',tickangle=-45, tickmode='array',tickfont=dict(color='black'),tickvals=fig_dta['Gebotstermin'].tolist(),ticktext=fig_dta['Gebotstermin'].dt.strftime('%b %Y').tolist(),linecolor='black',ticks='outside'),
        #yaxis = dict(title='',linecolor='black',ticks='outside'),
        plot_bgcolor='white',
        hovermode         = "x unified",
        legend = {'x': 0 , 'y': 1},
    )
    leg_dict = {"show"   : True,
                      "x0"     : "2021-12",
                      "x1"     : "2025-11", 
                      "text"   : "Legislaturperiode",
                      "color"  : "first",
                      "opacity": 0.1,
                      }
    fig.add_vrect(x0                   = leg_dict["x0"],
                x1                   = leg_dict["x1"],
                fillcolor            = "#3c435a",
                opacity              = 0.1,
                layer                = "below",
                annotation_text      = "Legislaturperiode",
                annotation_font_size = 12,
                annotation_position  = "top right",
                row                  = "all",
                col                  = "all")

    fig.update_layout(
        legend=dict(
            x=0.01,
            y=1,
            title_font_family='arial',
            font=dict(
                family="arial",
                size=14,
                color="black"
            ),
            borderwidth=1
        )
    )
    return fig


def weighted_mean(data, weights):
    return (data * weights).sum() / weights.sum()

def create_annual_auction_vols(data,technology):
    fig_dta = data[['Gebotstermin','Ausschreibungsvolumen (kW)','Zuschlagsvolumen (kW)','Zuschlagswert (ct/kWh) Gew Mittel','Höchstwert (ct/kWh)','Preisregel','Gebotswerte mit Zuschlag (ct/kWh) Min','Gebotswerte mit Zuschlag (ct/kWh) Max']]
    fig_dta['Gebotstermin'] = pd.to_datetime(fig_dta['Gebotstermin'])
    fig_dta = fig_dta.set_index('Gebotstermin').merge(cpi_data,how='left',left_index=True,right_index=True)
    fig_dta = fig_dta.reset_index()
    fig_dta['CPI'] = fig_dta['CPI']/100
    fig_dta['Zuschlagswert (ct/kWh) Gew Mittel'] = fig_dta['Zuschlagswert (ct/kWh) Gew Mittel']/fig_dta['CPI'] 
    fig_dta['Höchstwert (ct/kWh)'] = fig_dta['Höchstwert (ct/kWh)']/fig_dta['CPI']

    fig_dta['year'] = fig_dta['Gebotstermin'].dt.year
    fig_dta = fig_dta.groupby('year').apply(
    lambda x: pd.Series({
        'Ausschreibungsvolumen (GW)': x['Ausschreibungsvolumen (kW)'].sum() / 1e6,
        'Zuschlagsvolumen (GW)': x['Zuschlagsvolumen (kW)'].sum() / 1e6,
        'Zuschlagswert (ct/kWh) Gew Mittel': weighted_mean(x['Zuschlagswert (ct/kWh) Gew Mittel'], x['Zuschlagsvolumen (kW)']),
        'Höchstwert (ct/kWh)': weighted_mean(x['Höchstwert (ct/kWh)'], x['Ausschreibungsvolumen (kW)']),
    })
    )
    fig_dta['Höchstwert (ct/kWh)'].iloc[-1] = fig_dta['Höchstwert (ct/kWh)'].iloc[-1]*sum(fig_dta['Ausschreibungsvolumen (GW)'])/fig_dta['Ausschreibungsvolumen (GW)'].iloc[-1]
    #fig_dta['Zuschlagsvolumen_2024'] = 0
    #fig_dta['Zuschlagsvolumen_2024'].iloc[-1] = fig_dta['Zuschlagsvolumen (GW)'].iloc[-1]
    #fig_dta['Zuschlagsvolumen (GW)'].iloc[-1] = np.nan
    fig_dta["pattern"] = ""
    fig_dta["pattern"].iloc[-1] = "/"
    fig_dta = fig_dta.reset_index()
    fig_dta = fig_dta.rename(columns={'Ausschreibungsvolumen (GW)':'Ausschreibungsmengen (GW)','Zuschlagsvolumen (GW)':'Zuschlagsmengen (GW)'})
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(go.Bar(x=fig_dta['year'],y=fig_dta['Ausschreibungsmengen (GW)'],
    #text = fig_dta['Ausschreibungsmengen (GW)'],
     name='Ausschreibungsmengen, GW',marker_color='#3c435a',hovertemplate='%{y:.2f} GW'),secondary_y=False)
    fig.add_trace(go.Bar(x=fig_dta['year'],y=fig_dta['Zuschlagsmengen (GW)'],
    #text=fig_dta['Zuschlagsmengen (GW)'],
    name='Zuschlagsmengen, GW',marker_color='#2A6A89',marker_pattern_shape = fig_dta["pattern"],hovertemplate='%{y:.2f} GW'),secondary_y=False)
    #fig.add_trace(go.Bar(x=fig_dta['year'],y=fig_dta['Zuschlagsvolumen_2024'],
    #text=fig_dta['Zuschlagsmengen (GW)'],
    #name='Zuschlagsmengen, GW',marker_color='#2A6A89',marker_pattern_shape = "/",hovertemplate='%{y:.2f} GW',showlegend=False),secondary_y=False)

    fig.add_trace(go.Scatter(x=fig_dta['year'],
                            y=fig_dta['Zuschlagswert (ct/kWh) Gew Mittel'],
                            mode='markers',
                            #text = fig_dta['Zuschlagswert (ct/kWh) Gew Mittel'],
                            name='Gew. Zuschlagswert, 2020ct/kWh',
                            hovertemplate='%{y:.2f} ct/kWh',
                            marker=dict(color="#ac452f",size=10)),
                            secondary_y=True)
    fig.add_trace(go.Scatter(x=fig_dta['year'], 
                                y=fig_dta['Höchstwert (ct/kWh)'],
                                mode='markers',
                                name='Höchstwert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kWh',
                                marker=dict(color="#c37130",symbol='x',size=10)),
                                secondary_y=True)

    
    fig.update_yaxes(title="GW",linecolor='black',ticks='outside',range = [0,10],secondary_y=False)
    fig.update_yaxes(title="2020ct/kWh",linecolor='black',ticks='outside',range = [0,12],secondary_y=True,showgrid=False)

    #fig.add_annotation(x=2015.9,y=fig_dta['Ausschreibungsmengen (GW)'][1]/10,text='Ausschreibungsmengen, GW',showarrow=True,font=dict(size=12))
    #fig.add_annotation(x=2017.1,y=fig_dta['Zuschlagsmengen (GW)'][2]/10,text='Zuschlagsmengen, GW',showarrow=True,font=dict(size=12))

    #fig.update_traces(textposition='outside',textfont_size=16,texttemplate='%{text:.1f}')
    fig.update_layout(
        xaxis= dict(title='',tickangle=-45, tickmode='array',tickfont=dict(color='black'),linecolor='black',ticks='outside',tickformat = '%{y:0f}',showgrid=False),
        #yaxis = dict(title='',linecolor='black',ticks='outside'),
        plot_bgcolor='white',
        hovermode         = "x unified",
        legend = {'x': 1 , 'y': 1, 'xanchor': 'left'},
    )
    leg_dict = {"show"   : True,
                      "x0"     : "2021-12",
                      "x1"     : "2025-11", 
                      "text"   : "Legislaturperiode",
                      "color"  : "first",
                      "opacity": 0.1,
                      }

    fig.update_layout(
        legend=dict(
            x=.9,
            y=1.2,
            xanchor='right',
            title_font_family='arial',
            font=dict(
                family="arial",
                size=14,
                color="black"
            ),
            borderwidth=1
        )
    )
    
    return fig

# Download auction data
url_wind_on ="https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Onshore.xlsx?__blob=publicationFile&v=2"
url_wind_on = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Onshore.xlsx?__blob=publicationFile&v=3"
url_pv_fr = "https://data.bundesnetzagentur.de/Bundesnetzagentur/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/statistik_solar1.xlsx"
url_pv_fr = os.path.join(am_solar_path,"data","statistik_solar1.xlsx")
url_pv_ad = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Solar2.xlsx?__blob=publicationFile&v=2"
url_pv_ad = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Solar2.xlsx?__blob=publicationFile&v=3"

wind_on = get_table_from_url(url_wind_on)
pv_fr = get_table_from_url(url_pv_fr)
pv_ad = get_table_from_url(url_pv_ad)

# Download CPI data (this needs to be automated at some point)
cpi_data = pd.read_csv(os.path.join(am_solar_path,'data','cpi.csv'))
cpi_data.Date = pd.to_datetime(cpi_data.Date,format='%d/%m/%Y')
cpi_data = cpi_data.set_index('Date')

# Create auction plots

fig=create_annual_auction_vols(pv_fr,'PV Freifläche')

fig.update_layout(
    template = "simple_white",
    showlegend = False,
    #width = 1000,   height = 600,
    margin = dict(l=0, r=90, t=25, b=0, pad = 0),
    font = dict(size = 12, family = "Arial"),
)

fig.write_html(os.path.join(am_solar_path,"figures","fig_am_solar5_auktion.html"),include_plotlyjs="directory")
fig.write_image(os.path.join(am_solar_path,"figures","fig_am_solar5_auktion.png"), scale=5)

fig.show()

#%%