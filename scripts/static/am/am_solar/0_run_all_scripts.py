#%% Import library

import os
import runpy

#%% Change working directory

# Change to correct folder
working_dir = os.path.dirname(os.getcwd())
working_folder = os.path.split(working_dir)[1]

if working_folder == 'am':
    os.chdir('../../../../')
else:
    sys.path.append('.')

print(os.getcwd())

#%% Run all scripts

runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','1_international.py'))
runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','2_zubau.py'))
runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','3_eeg.py'))
runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','4_bundeslaender.py'))
runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','5_auctions.py'))
runpy.run_path(path_name=os.path.join('scripts','static','am','am_solar','6_flex.py'))
