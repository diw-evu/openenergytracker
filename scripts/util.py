#%%

import os
import requests
import pandas as pd
import numpy as np
import plotly as plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from sklearn.linear_model import LinearRegression
from datetime import timedelta
from datetime import datetime

#%%

import scripts.colors as colors

###### Data ########################################################################################

def prepare_data(rel_url,date_type):
    """_summary_

    Args:
        rel_url (_type_): _description_

    Returns:
        _type_: _description_
    """
    data               = pd.read_csv(rel_url, sep = ",")
    if 'months' in data.columns:
        data['months']     = data['months'].astype(str)
    elif 'quarters' in data.columns:
        data['quarters']   = data['quarters'].astype(str)
    else:
        pass
    
    if date_type == "months":
        data['date']       = pd.to_datetime(data['date'], format='%d.%m.%Y')#.dt.strftime('%b %y')
    if date_type == "quarters":
        data['date']   = pd.to_datetime(data['date'], format='%d.%m.%Y')#.dt.strftime('%b %y')
    if date_type == "years":
        data['date']       = pd.to_datetime(data['date'], format='%Y')#.dt.strftime('%b %y')
    else:
        pass
    
    return data

def calculate_trend(data,variable,type,start_date=None,end_date=None):
    
    # Create index
    data['X'] = data.index
    
    if type == "12-months":
        reg2_data = data[['date',variable,'X']].dropna()
        reg2_data = reg2_data.tail(13)[1:12] # Takes last 13 months, but discards last one
        reg2_x    = np.array(reg2_data['X']).reshape((-1, 1))
        reg2      = LinearRegression().fit(reg2_x, reg2_data[variable])

        predict2 = data[['date','X',variable]]
        predict2 = predict2[(predict2['date'] >= reg2_data.iloc[-1]['date'])].reset_index()
        predict2['index'] = predict2.index
        predict2['trend_12months'] = predict2.loc[0,variable] + predict2['index'] * reg2.coef_
        predict2 = predict2[['date','trend_12months']]

        # merge
        data = data.merge(predict2, how='left', on='date')

    if type == "4-quarters":
        reg2_data = data[['date',variable,'X']].dropna()
        reg2_data = reg2_data.tail(4)
        reg2_x    = np.array(reg2_data['X']).reshape((-1, 1))
        reg2      = LinearRegression().fit(reg2_x, reg2_data[variable])

        predict2 = data[['date','X',variable]]
        predict2 = predict2[(predict2['date'] >= reg2_data.iloc[-1]['date'])].reset_index()
        predict2['index'] = predict2.index
        predict2['trend_12months'] = predict2.loc[0,variable] + predict2['index'] * reg2.coef_
        predict2 = predict2[['date','trend_12months']]

        # merge
        data = data.merge(predict2, how='left', on='date')
    
    if type == "5-years":
        reg1_data        = data[['date','X',variable]].dropna()
        last_data_point  = reg1_data.iloc[-1]['date']
        first_data_point = last_data_point - timedelta(days=5*365) # 5 years ago

        reg1_data = reg1_data.query("date >= @first_data_point")
        reg1_x    = np.array(reg1_data['X']).reshape((-1, 1))
        reg1      = LinearRegression().fit(reg1_x, reg1_data[variable])

        predict1 = data[['date','X',variable]]
        predict1 = predict1[(predict1['date'] >= reg1_data.iloc[-1]['date'])].reset_index()
        predict1['index'] = predict1.index
        predict1['trend_5years'] = predict1.loc[0,variable] + predict1['index'] * reg1.coef_
        predict1 = predict1[['date','trend_5years']]
        
        # merge
        data = data.merge(predict1, how='left', on='date')

    return data

###### Default RES capacity figure #################################################################

def create_fig(data_dict):

    # Check whether subplot
    if "subplots" in data_dict["base"]:
        fig = make_subplots(rows = data_dict["base"]["subplots"]["rows"],
                            cols = data_dict["base"]["subplots"]["cols"],
                            row_heights = data_dict["base"]["subplots"]["row_heights"],
                            vertical_spacing=0.05,
                            shared_xaxes=True)
    else:
        fig = go.Figure()

    # Create figure dictionary in loop
    for key in data_dict:

        if "figure" in key:
            
            # Create exception for "custom" & range (because adding by dictionary doesn't work, see below)
            if "custom" in data_dict[key]:
                if "range" in data_dict[key]["custom"]:
                    
                    temp_dict = dict(
                        x = pd.concat([data_dict["base"]["data"][data_dict[key]["x"]],
                                       data_dict["base"]["data"][data_dict[key]["x"]][::-1]]),
                        y = pd.concat([data_dict["base"]["data"][data_dict[key]["y"][0]],
                                       data_dict["base"]["data"][data_dict[key]["y"][1]][::-1]]),
                        name          = data_dict[key]["name"],
                        fill          = data_dict[key]["fill"],
                        fillcolor     = data_dict["base"]["color_dict"][data_dict[key]["fillcolor"]],
                        line          = data_dict[key]["line"],
                        hovertemplate = data_dict[key]["hover"],
                        legendgroup   = data_dict[key]["group"],
                        visible       = data_dict[key]["visible"],
                        hoverinfo     = data_dict[key]["hoverinfo"])
                    
                    if "title_group" in data_dict[key]:
                        temp_dict["legendgrouptitle_text"] = data_dict[key]["title_group"]
                                                            
                    if "subplots" in data_dict["base"]:
                        fig.add_trace(temp_dict, row = data_dict[key]["row"], col = data_dict[key]["col"])
                    else:
                        fig.add_trace(temp_dict)
                else:
                    pass
                pass
            
            # Totally custom dictionaries:
            elif "custom" in key:
                fig.add_trace(data_dict[key])
                
            # Add figures with dictionary
            else:
                temp_dict = {}
                                
                # Custom dictionary
                if "custom" in data_dict[key]:
                #    if "range" in data_dict[key]["custom"]:
                #        temp_dict["x"] = pd.concat([data_dict["base"]["data"][data_dict[key]["x"]],
                #                                    data_dict["base"]["data"][data_dict[key]["x"]][::-1]]),
                #        temp_dict["y"] = pd.concat([data_dict["base"]["data"][data_dict[key]["y"][0]],
                #                                    data_dict["base"]["data"][data_dict[key]["y"][1]][::-1]])
                #    
                #    else:
                #        pass
                    pass       
                # Standard dictionary
                else:
                    temp_dict["x"] = data_dict["base"]["data"][data_dict[key]["x"]]
                    temp_dict["y"] = data_dict["base"]["data"][data_dict[key]["y"]]

                # Standard dictionary
                temp_dict["x"] = data_dict["base"]["data"][data_dict[key]["x"]]
                temp_dict["y"] = data_dict["base"]["data"][data_dict[key]["y"]]

                # Fill dictionary with info  
                temp_dict["name"] = data_dict[key]["name"]
                temp_dict["type"] = data_dict[key]["type"]

                if "color" in data_dict[key]: 
                    temp_dict["marker_color"] = data_dict["base"]["color_dict"][data_dict[key]["color"]]
                if "hover" in data_dict[key]:
                    temp_dict["hovertemplate"] = data_dict[key]["hover"]
                if "group" in data_dict[key]:
                    temp_dict["legendgroup"] = data_dict[key]["group"]
                if "title_group" in data_dict[key]:
                    temp_dict["legendgrouptitle_text"] = data_dict[key]["title_group"]
                if "legendrank" in data_dict[key]:
                        temp_dict["legendrank"] = data_dict[key]["legendrank"]
                if "showlegend" in data_dict[key]:
                    temp_dict["showlegend"] = data_dict[key]["showlegend"]

                # Exception for scatter
                if data_dict[key]["type"] == "scatter":
                    temp_dict["mode"] = data_dict[key]["mode"]
                    if data_dict[key]["mode"] == "lines":
                        temp_dict["line"] = data_dict[key]["line"]
                    if data_dict[key]["mode"] == "markers":
                        temp_dict["marker"] = data_dict[key]["marker"]
                    if data_dict[key]["mode"] == "lines+markers":
                        temp_dict["connectgaps"] = data_dict[key]["connectgaps"]
                        temp_dict["line"]        = data_dict[key]["line"]
                    if "stackgroup" in data_dict[key]:
                        temp_dict["stackgroup"] = data_dict[key]["stackgroup"]
                    if "text" in data_dict[key]:
                        temp_dict["text"] = data_dict[key]["text"]
                    if "textposition" in data_dict[key]:
                        temp_dict["textposition"] = data_dict[key]["textposition"]
                else:
                    pass
                    
                # Add fill:
                if "fill" in data_dict[key]:
                    temp_dict["fill"]      = data_dict[key]["fill"]
                    temp_dict["fillcolor"] = data_dict["base"]["color_dict"][data_dict[key]["fillcolor"]]
                else:
                    pass
                
                # Add "visible":
                if "visible" in data_dict[key]:
                    temp_dict["visible"] = data_dict[key]["visible"]
                else:
                    pass
                
                # Hoverinfo
                if "hoverinfo" in data_dict[key]:
                    temp_dict["hoverinfo"] = data_dict[key]["hoverinfo"]

                # If customdata is in dictionary
                if "customdata" in data_dict[key]:
                    temp_dict["customdata"] = data_dict["base"]["data"][data_dict[key]["customdata"]]
                else:
                    pass

                # Add all traces to correct subplot
                if "subplots" in data_dict["base"]:
                    fig.add_trace(temp_dict, row = data_dict[key]["row"], col = data_dict[key]["col"])
                else:
                    fig.add_trace(temp_dict)

        else:
            pass

    # Add legislative period background
    if "legislative_period" in data_dict:
        if data_dict["legislative_period"]["show"] == True:
            fig.add_vrect(x0                   = data_dict["legislative_period"]["x0"],
                          x1                   = data_dict["legislative_period"]["x1"],
                          fillcolor            = data_dict["base"]["color_dict"][data_dict["legislative_period"]["color"]],
                          opacity              = 0.1,
                          layer                = "below",
                          annotation_text      = data_dict["legislative_period"]["text"],
                          annotation_font_size = 12,
                          annotation_position  = "top right",
                          row                  = "all",
                          col                  = "all")
            if "subplots" in data_dict["base"]:
                # Remove lower "Legislaturperiode"
                fig['layout']['annotations'][1]['text'] = ""
            else:
                pass
        else:
            pass
    else:
        pass

    # Add buttons
    if "buttons" in data_dict["base"]:   
        if "subplots" in data_dict["base"]:
            if "xaxis_range_2030" in data_dict["base"]["buttons"]:
                arg1 ={'xaxis.range' : data_dict["base"]["buttons"]["xaxis_range_2030"],
                       'xaxis2.range': data_dict["base"]["buttons"]["xaxis_range_2030"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2030"]]}
            else:
                arg1 ={'xaxis.range' : ["2017-01", "2031-03"],
                       'xaxis2.range': ["2017-01", "2031-03"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2030"]]}
                
            if "xaxis_range_2045" in data_dict["base"]["buttons"]:
                arg2 ={'xaxis.range' : data_dict["base"]["buttons"]["xaxis_range_2045"],
                       'xaxis2.range': data_dict["base"]["buttons"]["xaxis_range_2045"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2045"]]}
            else:
                arg2 = {'xaxis.range' : ["2017-01", "2046-06"],
                        'xaxis2.range': ["2017-01", "2046-06"],
                        'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2045"]]}

        elif "custom_button1" in data_dict["base"]["buttons"]:
            arg1 = [{'visible': data_dict["base"]["buttons"]['custom_traces1']},
                    {'yaxis': {'range': [0, data_dict["base"]["buttons"]["y_max_custom1"]],
                                          'title': data_dict["base"]["buttons"]["y_title_1"]}}]
            arg2 = [{'visible': data_dict["base"]["buttons"]['custom_traces2']},
                    {'yaxis': {'range': [0, data_dict["base"]["buttons"]["y_max_custom2"]],
                                          'title': data_dict["base"]["buttons"]["y_title_2"]}}]
        
        
        else:
            if "xaxis_range_2030" in data_dict["base"]["buttons"]:
                arg1 ={'xaxis.range' : data_dict["base"]["buttons"]["xaxis_range_2030"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2030"]]}
            else:
                arg1 ={'xaxis.range' : ["2017-01", "2031-03"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2030"]]}
                
            if "xaxis_range_2045" in data_dict["base"]["buttons"]:
                arg2 ={'xaxis.range' : data_dict["base"]["buttons"]["xaxis_range_2045"],
                       'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2045"]]}
            else:
                arg2 = {'xaxis.range' : ["2017-01", "2046-06"],
                        'yaxis.range' : [0, data_dict["base"]["buttons"]["y_max_2045"]]}
    

        if "position" in data_dict["base"]["buttons"]:
            x_pos = data_dict["base"]["buttons"]["position"]["x"]
            y_pos   = data_dict["base"]["buttons"]["position"]["y"]

        elif "custom_button1" in data_dict["base"]["buttons"]:
            x_pos = 0.01
            y_pos = 1.09

        else:
            x_pos = 0.01
            y_pos = 1.13
        
        
        if "button2030" in data_dict["base"]["buttons"]:
            fig.update_layout(
                updatemenus=[
                    dict(type="buttons", direction="left", active=0, 
                         showactive=True,
                         pad={"r": 10, "t": 0},
                    xanchor="left", yanchor="top", x = x_pos, y = y_pos,
                    buttons=list([
                        dict(label=data_dict["base"]["buttons"]["button2030"],
                             method="relayout",
                             args=[arg1]),
                        dict(label=data_dict["base"]["buttons"]["button2045"],
                             method="relayout",
                             args=[arg2])
                        ]),
                    )
                    ]
                )
        #mit dicts für jede Anpassung funktioniert default nicht      
        elif "custom_button1" in data_dict["base"]["buttons"]:
            fig.update_layout(
                updatemenus=[
                    dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
                        xanchor="left", yanchor="top", x = x_pos, y = y_pos,
                        buttons=list([
                            dict(label=data_dict["base"]["buttons"]["custom_button1"],
                                method="update",
                                args=[
                                    {"visible": data_dict["base"]["buttons"]['custom_traces1']},
                                    {"yaxis": {"range": [0, data_dict["base"]["buttons"]["y_max_custom1"]], 
                                               "title": data_dict["base"]["buttons"]["y_title_1"]}}    
                                ]
                                ),
                            dict(label=data_dict["base"]["buttons"]["custom_button2"],
                                method="update",
                                args=[
                                    {"visible": data_dict["base"]["buttons"]['custom_traces2']},
                                    {"yaxis": {"range": [0, data_dict["base"]["buttons"]["y_max_custom2"]], 
                                               "title": data_dict["base"]["buttons"]["y_title_2"]}}  
                                ]
                                )
                        ])
                    )
                ]
            )


        else:
            pass
    else:
        pass

    # X-Axis
    fig.update_xaxes(
        title = data_dict["base"]["x_axis_title"],
        range = data_dict["base"]["x_axis_range"])
    
    # Y-Axis
    if "buttons" not in data_dict["base"]:
        fig.update_yaxes(title = data_dict["base"]["y_axis_title"])
        fig["layout"]["yaxis"]["range"] = data_dict["base"]["y_axis_range"]

    elif "custom_button1" in data_dict["base"]["buttons"]:
        fig.update_yaxes(title = data_dict["base"]["y_axis_title"])
        fig["layout"]["yaxis"]["range"] = data_dict["base"]["y_axis_range"]

    else:
        fig.update_yaxes(title = data_dict["base"]["y_axis_title"])
        fig["layout"]["yaxis"]["range"] = data_dict["base"]["y_axis_range"]
        
    if "subplots" in data_dict["base"]:
        fig["layout"]["xaxis"]["showticklabels"]  = False
        fig["layout"]["yaxis2"]["range"] = data_dict["base"]["y_axis2_range"]
        fig["layout"]["yaxis2"]["title"] = data_dict["base"]["y_axis2_title"]
    
    if "title_y" in data_dict["base"]:
        y_title_position = data_dict["base"]["title_y"]
    else:
        y_title_position = 0.99
    
    if "margin" in data_dict["base"]:
        custom_margin = data_dict["base"]["margin"]
    else:
        custom_margin = dict(l=0, r=0, t=70, b=40, pad=0)
    
    if "showlegend" in data_dict["base"]:
        showlegend = data_dict["base"]["showlegend"]
    else:
        showlegend = True
    
    template_dict = dict(
        title = {"text": data_dict["base"]["title"], "y": y_title_position},
        template = "simple_white",
        font=dict(family  = "'sans-serif','arial'",size=12, color="#000000"),
        uniformtext_mode  = "hide",
        margin            = custom_margin,
        hovermode         = "x unified",
        hoverlabel        = dict(font_size=12),
        showlegend        = showlegend,
        legend = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 0.01,
            "y"              : 1,
            "yanchor"        : "top",
            "xanchor"        : "left",
            "borderwidth"    : 1,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "groupclick"     : "toggleitem"},
        modebar_remove=["lasso","autoScale"]
    )
    
    if "margin" in data_dict["base"]:
        template_dict["margin"] = data_dict["base"]["margin"]
    if "hovermode" in data_dict["base"]:
        template_dict["hovermode"] = data_dict["base"]["hovermode"]
    if "barmode" in data_dict["base"]:
        template_dict["barmode"] = data_dict["base"]["barmode"]
    if "legend_dict" in data_dict["base"]:
        template_dict["legend"] = data_dict["base"]["legend_dict"]
    if "legend_trace_order" in data_dict["base"]:
        template_dict["legend"]["traceorder"] = data_dict["base"]["legend_trace_order"]
    if "source_file" in data_dict["base"]:
        # Get last updated date
        date = get_last_updated_date(data_dict["base"]["source_file"])

        if "language" in data_dict["base"]:
            if data_dict["base"]["language"] == "de":
                date = "Letztes Update: " + date
            if data_dict["base"]["language"] == "en":
                date = "Last Update: " + date
            if data_dict["base"]["language"] == "fr":
                date = "Dernière mise à jour: " + date
        else:
            date = "Last Update: " + date
            
        fig.add_annotation(text=date, xref="paper", yref="paper", x=1.0, y=-.11, showarrow=False, font=dict(size=12,family="Arial",
                                  color = "lightgray"))
    
    # Add annotations
    for key in data_dict:
        if "annotation" in key:
            if fig["layout"]["annotations"] != ():
                current_annotations  = fig.layout.annotations
                current_annotations += (data_dict[key],)
                fig.update_layout(annotations=current_annotations)
            else:
                fig.update_layout(annotations=(data_dict[key]))
        else:
            pass
    
    # General template
    fig.update_layout(template_dict)

    return fig

#####

def update_color(old_data_dict,new_color_dict):
    new_dict = old_data_dict.copy()
    new_dict["base"]["color_dict"] = new_color_dict
    return new_dict

####### Default RES dictionaries ###################################################################

base_default = {
    "title"         : "Installierte Leistung Photovoltaik",
    "data"          : None,
    "color_dict"    : colors.standard,
    "x_axis_title"  : None,
    "x_axis_range"  : ["2017-01", "2031-03"],
    "y_axis_title"  : "Bestand [GW]",
    "y_axis_range"  : [0,250],
    "buttons"  : {
        "y_max_2030"  : 250,
        "y_max_2045"  : 500,
        "button2030"  : "bis 2030",
        "button2045"  : "bis 2045"
        },
    "source_file"     : "",
    "language"        : "de"
    }

legislative_period = {"show"   : True,
                      "x0"     : "2021-12",
                      "x1"     : "2024-12", 
                      "text"   : "Ampel-Koalition",
                      "color"  : "first",
                      "opacity": 0.1,
                      }

actual = {"name"       : "Installiert",
          "type"       : "bar",
          "x"          : "date",
          "y"          : "actual",
          "color"      : "first",
          "hover"      : "%{y:.2f} GW",
          "group"      : "reached",
          "title_group": None,
          "row"        : 1,
          "col"        : 1,
        }

trend = {"name"       : "12-Monats-Trend",
         "type"       : "scatter",
         "x"          : "date",
         "y"          : "trend_12months",
         "mode"       : "lines",
         "line"       : {"dash" : "dash"},
         "color"      : "trend1",
         "hover"      : "%{y:.2f} GW",
         "group"      : "reached",
         "title_group": None,
         "visible"    : "legendonly",
         "row"        : 1,
         "col"        : 1,
        }

trend_months = {"name": "12-Monats-Trend",
         "type"       : "scatter",
         "x"          : "date",
         "y"          : "trend_12months",
         "mode"       : "lines",
         "line"       : {"dash" : "dash"},
         "color"      : "trend1",
         "hover"      : "%{y:.2f} GW",
         "group"      : "reached",
         "title_group": None,
         "visible"    : "legendonly",
         "row"        : 1,
         "col"        : 1,
        }

trend_years = {"name"       : "5-Jahres-Trend",
         "type"       : "scatter",
         "x"          : "date",
         "y"          : "trend_5years",
         "mode"       : "lines",
         "line"       : {"dash" : "dash"},
         "color"      : "trend2",
         "hover"      : "%{y:.2f} GW",
         "group"      : "reached",
         "title_group": None,
         "visible"    : "legendonly",
         "row"        : 1,
         "col"        : 1,
        }

goal = {"name"        : "Ziele der Koalition",
        "type"        : "scatter",
        "x"           : "date",
        "y"           : "plan",
        "mode"        : "markers",
        "marker"      : {"size" : 12, "symbol": "x-dot"},
        "color"       : "plan_target",
        "hover"       : "%{y:.2f} GW",
        "group"       : "gov",
        "title_group" : "<i>Regierungsziele</i>",                    
        "showlegend"  : True,
        "row"         : 1,
        "col"         : 1,
        }

goal_low = {"name"    : "Untere Ziele der PPE",
        "type"        : "scatter",
        "x"           : "date",
        "y"           : "plan_low",
        "mode"        : "markers",
        "marker"      : {"size" : 12, "symbol": "x-dot"},
        "color"       : "plan_target_low",
        "hover"       : "%{y:.2f} GW",
        "group"       : "gov",
        "title_group" : "<i>Regierungsziele</i>",
        "row"         : 1,
        "col"         : 1,
        }

goal_high = {"name"   : "Oberes Ziel der PPE",
        "type"        : "scatter",
        "x"           : "date",
        "y"           : "plan_high",
        "mode"        : "markers",
        "marker"      : {"size" : 12, "symbol": "x-dot"},
        "color"       : "plan_target_high",
        "hover"       : "%{y:.2f} GW",
        "group"       : "gov",
        "title_group" : "<i>Regierungsziele</i>",
        "row"         : 1,
        "col"         : 1,
        }

goal_lin = {"name"      : "Linearisierter Verlauf",
           "type"       : "scatter",
           "x"          : "date",
           "y"          : "plan_linear",
           "mode"       : "lines",
           "line"       : {"dash": "dot"},
           "color"      : "plan_target",
           "hover"      : "%{y:.2f}GW",
           "group"      : "gov",
           "showlegend" : True,
           "row"        : 1,
           "col"        : 1,
           }

goal_lin_low = {"name"  : "Linearisierter Verlauf (Untere Ziele)",
           "type"       : "scatter",
           "x"          : "date",
           "y"          : "plan_low_linear",
           "mode"       : "lines",
           "line"       : {"dash": "dot"},
           "color"      : "plan_target_low",
           "hover"      : "%{y:.2f} GW",
           "group"      : "gov",
           "row"        : 1,
           "col"        : 1,
           }

goal_lin_high = {"name" : "Linearisierter Verlauf (Oberes Ziel)",
           "type"       : "scatter",
           "x"          : "date",
           "y"          : "plan_high_linear",
           "mode"       : "lines",
           "line"       : {"dash": "dot"},
           "color"      : "plan_target_high",
           "hover"      : "%{y:.2f} GW",
           "group"      : "gov",
           "row"        : 1,
           "col"        : 1,
           }

ar_lead = {"name"       : "Leitmodel",
           "type"       : "scatter",
           "x"          : "date",
           "y"          : "ariadne_lead_points",
           "mode"       : "lines+markers",
           "connectgaps": True,
           "line"       : {"dash": "dot", "width" : 2},
           "color"      : "ar_main",
           "hover"      : "%{y:.2f} GW",
           "group"      : "ariadne",
           "title_group": "<i>Ariadne-Szenarien</i>",
           "visible"    : "legendonly",
           "row"        : 1,
           "col"        : 1,
           }

ar_corr = {"custom"     : "range",
           "type"       : "scatter",
           "name"       : "Korridor",
           "x"          : "date",
           "y"          : ["ariadne_max","ariadne_min"],
           "mode"       : "lines",
           "fill"       : "toself",
           "fillcolor"  : "ar_background",
           "line"       : {"dash": "dot", "width" : 0},
           "hover"      : "%{y:.2f} GW",
           "group"      : "ariadne",
           "visible"    : "legendonly",
           "hoverinfo"  : "skip",
           "col"        : 1,
           "row"        : 1,
         }

futurs50_corr = {"custom"     : "range",
                "type"       : "scatter",
                "name"       : "Futurs2050-Scenariokorridor",
                "x"          : "date",
                "y"          : ["futurs50_max","futurs50_min"],
                "mode"       : "lines",
                "fill"       : "toself",
                "fillcolor"  : "ar_background2",
                "line"       : {"dash": "dot", "width" : 0},
                "hover"      : "%{y:.2f} GW",
                "title_group": "<i>Szenarien</i>",
                "group"      : "scenarios",
                "visible"    : "legendonly",
                "hoverinfo"  : "skip",
                "col"        : 1,
                "row"        : 1,
                }

op_corr = {"custom"     : "range",
           "type"       : "scatter",
           "name"       : "openENTRANCE-Scenariokorridor",
           "x"          : "date",
           "y"          : ["opentrance_max","opentrance_min"],
           "mode"       : "lines",
           "fill"       : "toself",
           "fillcolor"  : "ar_background",
           "line"       : {"dash": "dot", "width" : 0},
           "hover"      : "%{y:.2f} GW",
           "title_group": "<i>Szenarien</i>",
           "group"      : "scenarios",
           "visible"    : "legendonly",
           "hoverinfo"  : "skip",
           "col"        : 1,
           "row"        : 1,
         }

additions = {"name"       : "Hinzugefügt",
             "type"       : "bar",
             "x"          : "date",
             "y"          : "add_gw",
             "color"      : "first",
             "hover"      : "%{y:.2f} GW",
             "group"      : None,
             "showlegend" : False,
             "row"        : 2,
             "col"        : 1,
             }

additions_needed = {"name"       : "Benötigt",
                    "type"       : "scatter",
                    "x"          : "date",
                    "y"          : "add_plan_linear",
                    "mode"       : "lines",
                    "line"       : {"dash": "dot", "width" : 2},
                    "color"      : "plan_target",
                    "hover"      : "%{y:.2f} GW",
                    "group"      : None,
                    "showlegend" : False,
                    "row"        : 2,
                    "col"        : 1,
                    }

additions_needed_low = {"name"    : "Benötigt (Unteres Ziel)",
                    "type"       : "scatter",
                    "x"          : "date",
                    "y"          : "add_plan_linear_low",
                    "mode"       : "lines",
                    "line"       : {"dash": "dot", "width" : 2},
                    "color"      : "plan_target_low",
                    "hover"      : "%{y:.2f} GW",
                    "group"      : None,
                    "showlegend" : False,
                    "row"        : 2,
                    "col"        : 1,
                    }

additions_needed_high = {"name"   : "Benötigt (Oberes Ziel)",
                    "type"       : "scatter",
                    "x"          : "date",
                    "y"          : "add_plan_linear_high",
                    "mode"       : "lines",
                    "line"       : {"dash": "dot", "width" : 2},
                    "color"      : "plan_target_high",
                    "hover"      : "%{y:.2f} GW",
                    "group"      : None,
                    "showlegend" : False,
                    "row"        : 2,
                    "col"        : 1,
                    }

# Function to receive last updated date ############################################################

def get_last_updated_date(file_path):
    
    # GitLab API and project details
    GITLAB_API_URL = "https://gitlab.com/api/v4"
    PROJECT_ID = "31699759"
    FILE_PATH = file_path

    # GitLab API endpoint to get the last commit for a file
    url = f"{GITLAB_API_URL}/projects/{PROJECT_ID}/repository/commits"
    params = {
        "path": FILE_PATH,
        "per_page": 1  # Limit to one commit (the latest one)
    }

    # Make the request
    response = requests.get(url, params=params)

    # Check for successful response
    if response.status_code == 200:
        commit_data = response.json()
        if commit_data:
            last_commit = commit_data[0]  # The first (latest) commit
            last_commit_date = last_commit['committed_date']
            print(f"The file was last modified on: {last_commit_date}")
            
            # Convert to date
            date = last_commit_date[:10]
            # Convert to German date format
            date = datetime.strptime(date, "%Y-%m-%d").strftime('%d.%m.%Y')
            
            return date
        
        else:
            print("No commits found for the specified file.")
            date = "unknown"
            return date
    else:
        print(f"Failed to retrieve commits. Status code: {response.status_code}")
        date = "unknown"
        return date
# %%
