# Open Energy Tracker

[![Latest Release](https://gitlab.com/diw-evu/openenergytracker/-/badges/release.svg)](https://gitlab.com/diw-evu/openenergytracker/-/releases) [![pipeline status](https://gitlab.com/diw-evu/openenergytracker/badges/main/pipeline.svg)](https://gitlab.com/diw-evu/openenergytracker/-/commits/main)

## An open data platform for monitoring energy policy targets.

With the [Open Energy Tracker](https://openenergytracker.org/en/), we provide an open data platform for monitoring and visualizing energy policy goals. Initially, the focus was on Germany and the goals of the current federal government. We want to extend this data platform to other countries, initially to selected European countries. France has already been included and further countries will follow. Beyond that, we are open to extensions of other countries and regions, also outside Europe, ideally collaboratively with respective regional experts.

We graphically depict governmental targets, most of which are specified for the year 2030, and compare them with the current status. We focus on renewable energy expansion, electric mobility and electrolysis, but cover also other indicators. All figures, explanations, and links to the underlying data can be found on the [main page of the project](https://openenergytracker.org/de/).

## "Ampel-Monitor Energiewende", formerly known as "KoaVTracker": monitoring energy goals of the new German government

In their [coalition agreement](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) of 24.11.2021, the coalition parties of the new German government set themselves various quantitative targets in the energy sector. We have been pursuing these since the end of 2021, initially under the name **"KoaVTracker "**. Since then, however, the federal government has increasingly set itself further targets that go beyond the coalition agreement, so we have decided to rename our tool. It is now called **"Ampel-Monitor Energiewende "**. From July 2022, this traffic light monitor will also have its own [DIW Berlin website](https://www.diw.de/ampel-monitor) and regular publications. The complete set indicators will, including all countries, is available on the [homepage of the Open Energy Tracker](https://openenergytracker.org/en/).

## About us

The Open Energy Tracker was developed by [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), [Alexander Roth](https://www.diw.de/cv/en/aroth),  [Adeline Guéret](https://www.diw.de/cv/en/agueret), and [Felix Schmidt](https://www.diw.de/cv/en/fschmidt) of the [Transformation of the Energy Economy](https://twitter.com/transenerecon) research unit of the [Energy, Transport, Environment](https://www.diw.de/de/diw_01.c.604205.de/abteilung_energie__verkehr__umwelt.html) department at [DIW Berlin](https://www.diw.de/de). In particular, the parts of the tracker in which we report as "Ampel-Monitor Energiewende" were created in the context of the BMBF-funded Copernicus project [Ariadne](https://ariadneprojekt.de/).

## Contact

Please see the [about](https://openenergytracker.org/en/docs/about/) page on the Open Energy Tracker.