# %%
# Import required packages
import os
import shutil
import pandas as pd
import numpy as np
import datetime as datetime
import plotly.express as px
from plotly import graph_objects as go
from open_mastr import Mastr
from open_mastr.soap_api.download import MaStRDownload
from open_mastr.soap_api.download import MaStRAPI
from sqlalchemy import create_engine

# %%
# Import functions
import mastr_functions as mf

# %%
user = os.getlogin()

#dynamisch
db_path = r"sqlite:///C:/Users/"+user+r"/OneDrive - DIW Berlin/Open Energy Tracker and Ampel-Monitor/OET/input/MaStR/.open-MaStR/data/sqlite/open-mastr.db"

#statisch, wenn das dynamische nicht klappt, bitte fschmidt.DIW-BERLIN-DE ersetzen.
#db_path = "sqlite:///C:\\Users\\fschmidt.DIW-BERLIN-DE\\OneDrive - DIW Berlin\\Open Energy Tracker and Ampel-Monitor\\OET\\input\\MaStR\\.open-MaStR\\data\\sqlite\\open-mastr.db"
db_path = "sqlite:///C:/Users/u0175660/OneDrive - DIW Berlin/Studium/PhD/4_Projects/OET - AM/Open Energy Tracker and Ampel-Monitor/OET/input/MaStR/.open-MaStR/data/sqlite/open-mastr.db"

print(db_path)

engine_sqlite = create_engine(db_path)
path          = create_engine(db_path)
db            = Mastr(engine=path)

# %% [markdown]
# Es reicht wenn die open-mastr.db geladen wurde und dann die zelle bei Code all ausgeführt wird, die anderen zellen sind eher zum ausprobieren beim programmieren das nicht immer die beiden db geladen und gejoint werden müssen.

# %%
from sqlalchemy import inspect

inspect(engine_sqlite).get_table_names()

# %% [markdown]
# # Download

# %%
# Keep commented out unless you want to download the data from the API; takes around 2-3 h

#db.download()

# %% [markdown]
# # Load data

# %%
# Import extended table
storage_extended = pd.read_sql(sql="storage_extended",con=db.engine).drop(columns=["NutzbareSpeicherkapazitaet"])

# %%
# Import main table
storage_main     = pd.read_sql(sql="storage_units",con=db.engine)[["MastrNummer","NutzbareSpeicherkapazitaet"]]

# %%
# Merge the two tables
storage_merge = storage_main.merge(storage_extended, left_on='MastrNummer', right_on='SpeMastrNummer', how='inner')

# %%
# # Batteries  #####################################################################################

# Get subset of the data

batteries_df = storage_merge[[
    'MastrNummer',
    'Registrierungsdatum',
    'NutzbareSpeicherkapazitaet',
    'SpeMastrNummer',
    'Inbetriebnahmedatum',
    'EinheitBetriebsstatus',
    'Batterietechnologie',
    'NetzbetreiberpruefungStatus',
    'Nettonennleistung',
    'Bruttoleistung',
    'AnlagenbetreiberMastrNummer',]]

# %%

# Filter data

#filter 1 Batterietechnologie
merged_df_filter_1 = batteries_df.dropna(subset=['Batterietechnologie'])

#filter2 Energie-zu-Leistung-Verhältnis) muss zwischen 6 Minuten und 15 Stunden liegen.
merged_df_filter_2 = merged_df_filter_1[(merged_df_filter_1['NutzbareSpeicherkapazitaet'] / merged_df_filter_1['Nettonennleistung']).between(0.1, 15)]

#filter3 Nach Monat sortieren
merged_df_filter_2['Inbetriebnahmedatum'] = pd.to_datetime(merged_df_filter_2['Inbetriebnahmedatum'])
merged_df_filter_2 = merged_df_filter_2.sort_values(by='Inbetriebnahmedatum').reset_index(drop=True)

#filter 4 falsche werte aussortieren, hier sollen alle werte raus wo die NutzebarenKapazitäten größer 1000kw sind und es noch nicht vom Netzbetreiber geprüft ist
# Wenn dieser Filter angewendet wrid, wird ein groß speicher rausgefiltert, 
filtered_data_to_remove = merged_df_filter_2[
    (merged_df_filter_2['NutzbareSpeicherkapazitaet'] > 1000) & (merged_df_filter_2['NetzbetreiberpruefungStatus'] == '0')]

merged_df_filter_3 = merged_df_filter_2[~((merged_df_filter_2['NutzbareSpeicherkapazitaet'] > 1000) & 
                                            (merged_df_filter_2['NetzbetreiberpruefungStatus'] == '0'))]

# Sonder Ausnahmen, eine Batterie wird nach prüfung wieder mit rein genommen
specific_mastr_number = 'SSE978006940074'

# Filtern Sie den gewünschten Datensatz aus filtered_data_to_remove
restored_data = filtered_data_to_remove[filtered_data_to_remove['MastrNummer'] == specific_mastr_number]

# Fügen die wiederhergestellten Daten wieder zu den ursprünglichen Daten hinzu
merged_df_filter_3 = pd.concat([merged_df_filter_3, restored_data], ignore_index=True)

# %%

# Add categories
merged_df_filter_3.loc[:,'Category'] = pd.cut(
    merged_df_filter_3['NutzbareSpeicherkapazitaet'], # war mal Brutto
    bins=[-np.inf, 30, 1000, np.inf], 
    labels=['Heimspeicher', 'Gewerbespeicher', 'Großspeicher'])

# %%

##### Create monthly dataset

# Nur in operation
battery_monthly = merged_df_filter_3.query("EinheitBetriebsstatus == 'In Betrieb'").reset_index(drop=True)

# Change data format to monthly
battery_monthly.loc[:,"date"] = battery_monthly.loc[:,"Inbetriebnahmedatum"].dt.strftime("%Y-%m")

# Aggregate data
battery_monthly = battery_monthly.groupby(["date","Category"]).agg(
    {
        "Nettonennleistung":"sum",
        "Bruttoleistung":"sum",
        "NutzbareSpeicherkapazitaet":"sum"
    }).reset_index(drop=False)

battery_monthly_melt = battery_monthly.melt(id_vars=["date","Category"],value_vars=["Nettonennleistung","Bruttoleistung","NutzbareSpeicherkapazitaet"],var_name="Variable",value_name="Value")

# Aggregate data before 2019 to one month
battery_monthly_pre2019 = battery_monthly_melt.query("date < '2019-01'")
battery_monthly_post2019 = battery_monthly_melt.query("date >= '2019-01'")

battery_monthly_pre2019 = battery_monthly_pre2019.groupby(["Category","Variable"]).agg({"Value":"sum"}).reset_index(drop=False)
battery_monthly_pre2019["date"] = "2018-12"

battery_monthly_consolidated = pd.concat([battery_monthly_pre2019,battery_monthly_post2019]).reset_index(drop=True)

# Create cumulative values
battery_monthly_consolidated["value_cum"] = battery_monthly_consolidated.groupby(["Variable","Category"])["Value"].cumsum()

battery_monthly_consolidated_total = battery_monthly_consolidated.groupby(["date","Variable"]).agg({"Value":"sum","value_cum":"sum"}).reset_index(drop=False)
battery_monthly_consolidated_total["Category"] = "Total"

battery_monthly_consolidated_final = pd.concat([battery_monthly_consolidated,battery_monthly_consolidated_total]).reset_index(drop=True)

# Make GW / GWh

battery_monthly_consolidated_final["Value"] /= 1e3
battery_monthly_consolidated_final["value_cum"] /= 1e6

# Rename

battery_monthly_consolidated_final = battery_monthly_consolidated_final.rename(columns={"Value":"value"})

# (below copied from old code, to be implemented later?)
# Die bestände von vor 2019 müssen händisch hinzugefügt werden, hier berufe ich mich auf https://www.sciencedirect.com/science/article/pii/S2352152X19309442
#    cumulative_df.at[index_to_update, 'Heimspeicher_bl'] += 415000
#    cumulative_df.at[index_to_update, 'Großspeicher_bl'] += 400000
#    cumulative_df.at[index_to_update, 'Gewerbespeicher_bl'] += 1000
#    cumulative_df.at[index_to_update, 'Heimspeicher_kp'] += 930000
#    cumulative_df.at[index_to_update, 'Großspeicher_kp'] += 550000
#    cumulative_df.at[index_to_update, 'Gewerbespeicher_kp'] += 1000

# Energy ###########################################################################################

batteries_data_energy = battery_monthly_consolidated_final.query("Variable == 'NutzbareSpeicherkapazitaet'").pivot_table(index="date", columns="Category", values=["value","value_cum"] ).reset_index()

batteries_data_energy.columns = ['_'.join(col).rstrip('_') for col in [c[::-1] for c in batteries_data_energy.columns.values]]
batteries_data_energy = batteries_data_energy.rename(columns={"_date":"date"})

batteries_data_energy.to_csv(
    os.path.join("..","docs","germany","data","batteries_energy.csv"),index=False)

# Power ############################################################################################

batteries_data_power = battery_monthly_consolidated_final.query("Variable == 'Nettonennleistung'").pivot_table(index="date", columns="Category", values=["value","value_cum"] ).reset_index()

batteries_data_power.columns = ['_'.join(col).rstrip('_') for col in [c[::-1] for c in batteries_data_power.columns.values]]
batteries_data_power = batteries_data_power.rename(columns={"_date":"date"})

batteries_data_power.to_csv(
    os.path.join("..","docs","germany","data","batteries_power.csv"),index=False)

# %% 

# Pumped hydro #####################################################################################

# %%
# Filter for pumped hydro storage
pumped_hydro = storage_merge.query("Pumpspeichertechnologie.notnull()")

# Only keep pumped hydro plants that are in operation
pumped_hydro = pumped_hydro.query("EinheitBetriebsstatus == 'In Betrieb'")

# %%
# Choose columns 
"""
pumped_hydro = pumped_hydro[
    [
        "EinheitMastrNummer",
        "Inbetriebnahmedatum",
        "PumpbetriebLeistungsaufnahme",
        "Pumpspeichertechnologie",
        "Bruttoleistung",
        "Nettonennleistung",
        "NutzbareSpeicherkapazitaet",
        "Land",
        "Bundesland"]
].reset_index(drop=True)

# %%
pumped_hydro["date"] = pumped_hydro["Inbetriebnahmedatum"].dt.strftime("%Y-%m")
pumped_hydro = pumped_hydro.drop(columns=["Inbetriebnahmedatum"])

# %%
pumped_hydro.head()

# %%
pumped_hydro     = pumped_hydro.sort_values("date").reset_index(drop=True)
pumped_hydro_sum = pumped_hydro.groupby(["date","Pumpspeichertechnologie"]).agg(
    {
        "EinheitMastrNummer":"count",
        "Bruttoleistung":"sum",
        "Nettonennleistung":"sum",
        "PumpbetriebLeistungsaufnahme":"sum",
        "NutzbareSpeicherkapazitaet":"sum"
        }).reset_index(drop=False)

# %%
months1 = pd.DataFrame({'date': pd.date_range("1926-01","2024-11",freq="ME").strftime("%Y-%m")})
months2 = pd.DataFrame({'date': pd.date_range("1926-01","2024-11",freq="MS").strftime("%Y-%m")})
months1["Pumpspeichertechnologie"] = "Pumpspeicheranlage mit natürlichem Zufluss"
months2["Pumpspeichertechnologie"] = "Pumpspeicheranlage ohne natürlichen Zufluss"
months = pd.concat([months1,months2]).reset_index(drop=True)

pumped_hydro_sum_complete = pd.merge(months,pumped_hydro_sum,how="left",on=["date","Pumpspeichertechnologie"])
pumped_hydro_sum_complete["Pumpspeichertechnologie"] = pumped_hydro_sum_complete["Pumpspeichertechnologie"].ffill()
pumped_hydro_sum_complete = pumped_hydro_sum_complete.fillna(0)

# %%
pumped_hydro_sum_complete["EinheitMastrNummer_cum"]         = pumped_hydro_sum_complete.groupby("Pumpspeichertechnologie")["EinheitMastrNummer"].cumsum()
pumped_hydro_sum_complete["Bruttoleistung_cum"]             = pumped_hydro_sum_complete.groupby("Pumpspeichertechnologie")["Bruttoleistung"].cumsum()
pumped_hydro_sum_complete["Nettonennleistung_cum"]          = pumped_hydro_sum_complete.groupby("Pumpspeichertechnologie")["Nettonennleistung"].cumsum()
pumped_hydro_sum_complete["PumpbetriebLeistung_cum"]        = pumped_hydro_sum_complete.groupby("Pumpspeichertechnologie")["PumpbetriebLeistungsaufnahme"].cumsum()
pumped_hydro_sum_complete["NutzbareSpeicherkapazitaet_cum"] = pumped_hydro_sum_complete.groupby("Pumpspeichertechnologie")["NutzbareSpeicherkapazitaet"].cumsum()

# %%
px.area(pumped_hydro_sum_complete,x="date",y="Nettonennleistung_cum",color="Pumpspeichertechnologie",title="Nettonennleistung in MW")

# %%
px.area(pumped_hydro_sum_complete,x="date",y="Bruttoleistung_cum",color="Pumpspeichertechnologie",title="Bruttoleistung in MW")

# %%
px.area(pumped_hydro_sum_complete,x="date",y="PumpbetriebLeistung_cum",color="Pumpspeichertechnologie",title="Bruttoleistung in MW")

# %%
px.area(pumped_hydro_sum_complete,x="date",y="NutzbareSpeicherkapazitaet_cum",color="Pumpspeichertechnologie",title="Bruttoleistung in MW")

# %%
px.area(pumped_hydro_sum_complete,x="date",y="EinheitMastrNummer_cum",color="Pumpspeichertechnologie",title="Number of units")



"""