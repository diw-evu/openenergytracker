import os
import pandas as pd
import numpy as np
import datetime as datetime

# function to compile pv aggregate dataset
def compile_pv_net_additions_dataset(df,bins_vec):
    # create bins
    df = df.assign(bins = pd.cut(df.Bruttoleistung,bins_vec))
    # begin with additions
    add = df
    add['date'] = pd.to_datetime(add['Inbetriebnahmedatum'])
    add = add.set_index('date')
    add = add.assign(month = add.index.month,year = add.index.year)
    add = add.groupby(['month','year','Lage','Bundesland','bins']).agg({'Bruttoleistung':'sum'})

    # now with reductions
    red = df
    red['date'] = pd.to_datetime(red['DatumEndgueltigeStilllegung'])
    red = red.set_index('date')
    red = red.assign(month = red.index.month,year = red.index.year)
    red = red.groupby(['month','year','Lage','Bundesland','bins']).agg({'Bruttoleistung':'sum'})

    # merge
    tmp = add.merge(red,how='outer',on=['year','month','Lage','Bundesland','bins',])
    tmp = tmp.rename(columns={'Bruttoleistung_x':'additions','Bruttoleistung_y':'reductions'})
    tmp['additions'] = tmp['additions'].fillna(0)
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp = tmp.assign(net_additions = tmp.additions - tmp.reductions)
    tmp = tmp.reset_index()
    
    return tmp


def compile_wind_input_df(df,bins_vec):
    df = df.assign(bins = pd.cut(df.Bruttoleistung,bins_vec))
    #df = df[df['EinheitBetriebsstatus'] != 'In Planung']
    df = df[df['Lage']=="Windkraft an Land"]
    df = df[["EinheitMastrNummer",
             "EinheitBetriebsstatus",
             "Inbetriebnahmedatum",
             "DatumEndgueltigeStilllegung",
             "Bundesland",
             "bins","Bruttoleistung",
             "Rotordurchmesser",
            "Nabenhoehe"
            ]]
    #df = df.reset_index()
    return df

def compile_attributes_df(df):
    wind_df = df
    wind_df['month'] = wind_df['Inbetriebnahmedatum'].dt.month
    wind_df['year'] = wind_df['Inbetriebnahmedatum'].dt.year
    wind_df['bl_dia'] = wind_df['Rotordurchmesser']*wind_df['Bruttoleistung']
    wind_df['bl_nh'] = wind_df['Nabenhoehe']*wind_df['Bruttoleistung']
    tmp = wind_df.groupby(['year','month']).agg({'Bruttoleistung':["sum","count"],'bl_dia':'sum','bl_nh':'sum'}).reset_index(col_level=1)
    tmp.columns = ['year','month','sum','count','bl_dia','bl_nh']
    tmp['rotor_diameter'] = tmp['bl_dia']/tmp['sum']
    tmp['hub_height'] = tmp['bl_nh']/tmp['sum'] 
    tmp['average_power'] = tmp['sum']/tmp['count']/1e3
    tmp = tmp.query('year>1998')
    tmp[['rotor_diameter','hub_height','average_power']]= tmp[['rotor_diameter','hub_height','average_power']].rolling(window=12).mean()
    tmp = tmp.query('year>1999')
    tmp = tmp.rename(columns={'rotor_diameter':'Rotor diameter','hub_height':'Hub height','average_power':'Average power'})
    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    tmp = tmp[['date','Rotor diameter','Hub height','Average power']]
    return tmp

def create_size_dfs(df):
    # additions
    adds = df
    adds['month'] = adds['Inbetriebnahmedatum'].dt.month
    adds['year'] = adds['Inbetriebnahmedatum'].dt.year
    adds = adds.groupby(['year','month','bins']).agg({'Bruttoleistung':'sum'}).reset_index()
    adds = adds.rename(columns={'Bruttoleistung':'additions'})
    # reductions
    reds = df
    reds['month'] = reds['DatumEndgueltigeStilllegung'].dt.month
    reds['year'] = reds['DatumEndgueltigeStilllegung'].dt.year
    reds = reds.groupby(['year','month','bins']).agg({'Bruttoleistung':'sum'}).reset_index()
    reds = reds.rename(columns={'Bruttoleistung':'reductions'})

    tmp = adds.merge(reds,how='outer',on=['year','month','bins'])
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp['net_additions'] = tmp['additions']-tmp['reductions']
    

    tmp['cumsum'] = tmp.groupby('bins')['net_additions'].cumsum()

    tmp2 = tmp.groupby(['year','month'])['net_additions'].sum().reset_index()
    tmp2['cumsum'] = tmp2['net_additions'].cumsum()
    tmp2 = tmp2[['year','month','cumsum']]
    tmp2 = tmp2.rename(columns={'cumsum':'total'})
    tmp = tmp.merge(tmp2,on=['year','month'])
    tmp['share'] = tmp['cumsum']/tmp['total']

    size_cum = tmp[['year','month','bins','share']]
    size_cum['date'] = pd.to_datetime(size_cum[['year','month']].assign(day=15))
    size_cum = size_cum.query('year>1999')
    size_cum = size_cum.pivot(index=['date'],columns='bins',values='share').reset_index()

    size_monthly = tmp[['year','month','bins','net_additions']]
    size_monthly = size_monthly.query('year>2016')
    size_monthly['net_additions'] = size_monthly['net_additions']/1e3
    size_monthly['date'] = pd.to_datetime(size_monthly[['year','month']].assign(day=15))
    size_monthly = size_monthly.pivot(index=['date'],columns='bins',values='net_additions').reset_index()
    
    size_monthly.columns = size_monthly.columns.map(str)
    size_cum.columns = size_cum.columns.map(str)
    size_monthly = size_monthly.rename(columns=
    {'(0.0,1000.0]':'0-1 MW',
    '(1000.0,2000.0]':'1-2 MW',
    '(2000.0,3000.0]':'1-2 MW',
    '(3000.0,4000.0]':'3-4 MW',
    '(4000.0,5000.0]':'4-5 MW',
    '(5000.0,1000000.0]':'>5 MW'}
    )

    size_cum = size_cum.rename(columns=
    {'(0.0,1000.0]':'0-1 MW',
    '(1000.0,2000.0]':'1-2 MW',
    '(2000.0,3000.0]':'1-2 MW',
    '(3000.0,4000.0]':'3-4 MW',
    '(4000.0,5000.0]':'4-5 MW',
    '(5000.0,1000000.0]':'>5 MW'}
    )

    
    return size_cum,size_monthly

def create_bundeslaender_dfs(df,scen_data):
    # additions
    adds = df
    adds['month'] = adds['Inbetriebnahmedatum'].dt.month
    adds['year'] = adds['Inbetriebnahmedatum'].dt.year
    adds = adds.groupby(['year','month','Bundesland']).agg({'Bruttoleistung':'sum'}).reset_index()
    adds = adds.rename(columns={'Bruttoleistung':'additions'})
    # reductions
    reds = df
    reds['month'] = reds['DatumEndgueltigeStilllegung'].dt.month
    reds['year'] = reds['DatumEndgueltigeStilllegung'].dt.year
    reds = reds.groupby(['year','month','Bundesland']).agg({'Bruttoleistung':'sum'}).reset_index()
    reds = reds.rename(columns={'Bruttoleistung':'reductions'})

    tmp = adds.merge(reds,how='outer',on=['year','month','Bundesland'])
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp['additions'] = tmp['additions'].fillna(0)
    tmp['net_additions'] = tmp['additions']-tmp['reductions']

    tmp = tmp.set_index(['year','month','Bundesland']).T.stack().unstack().T.reset_index()
    tmp = tmp.fillna(0)

    l12 = tmp
    total = tmp
    total = total.groupby('Bundesland').agg({'net_additions':'sum'}).reset_index()
    total['total'] = total['net_additions']/1e6 
    total = total.drop(columns=['net_additions'])
    total = total.merge(scen_data,on='Bundesland')
    total['share'] = total['total']/total['Potential']
    total = total.drop(columns=['Potential'])
    total = total.rename(columns={'total':'Installiert','share':'Anteil am Scenario'})

    tmp['cumsum'] = tmp.groupby('Bundesland')['net_additions'].cumsum()

    tmp2 = tmp.groupby(['year','month'])['net_additions'].sum().reset_index()
    tmp2['cumsum'] = tmp2['net_additions'].cumsum()
    tmp2 = tmp2[['year','month','cumsum']]
    tmp2 = tmp2.rename(columns={'cumsum':'total'})
    tmp = tmp.merge(tmp2,on=['year','month'])
    tmp['share'] = tmp['cumsum']/tmp['total']

    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    
    cum = tmp.pivot(index=['date'],columns='Bundesland',values='share').reset_index()
    cum = cum.query('date>"1999-12-31"')

    tmp = l12
    current_date = datetime.datetime.today()
    end_date = current_date.replace(day=1) - datetime.timedelta(days=1)
    end_date = end_date.replace(day=15)
    start_date = end_date - datetime.timedelta(days=365)

    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    tmp = tmp.query('date>=@start_date & date<=@end_date')
    tmp = tmp.groupby('Bundesland').agg({'net_additions':'sum'}).reset_index()
    tmp['Last 12M'] = tmp['net_additions']/1e3
    tmp['Last 12M rel'] = tmp['Last 12M']/tmp['Last 12M'].sum()
    l12 = tmp.drop(columns=['net_additions'])


    return cum, l12, total