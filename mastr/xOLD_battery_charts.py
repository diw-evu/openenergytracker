import pandas as pd
from open_mastr import Mastr
import numpy as np
import datetime as datetime
import plotly.express as px



def filternMaSTRBattery():
    db = Mastr()
    #db laden
    storage_main = pd.read_sql(sql="storage_units",con=db.engine)
    storage_extended = pd.read_sql(sql="storage_extended",con=db.engine)

    # db join
    storage_main=storage_main.loc[:, ['MastrNummer', 'Registrierungsdatum', 'NutzbareSpeicherkapazitaet']]
    storage_extended=storage_extended.loc[:, ['Registrierungsdatum','SpeMastrNummer', 'Inbetriebnahmedatum','Batterietechnologie','NetzbetreiberpruefungStatus','Nettonennleistung','Bruttoleistung','AnlagenbetreiberMastrNummer','Pumpspeichertechnologie']]

    merged_df = pd.merge(storage_main, storage_extended, left_on='MastrNummer', right_on='SpeMastrNummer', how='inner')


    #filter 1 Batterietechnologie
    merged_df_filter_1 = merged_df.dropna(subset=['Batterietechnologie'])

    #filter2 Energie-zu-Leistung-Verhältnis) muss zwischen 6 Minuten und 15 Stunden liegen.
    merged_df_filter_2 = merged_df_filter_1[(merged_df_filter_1['NutzbareSpeicherkapazitaet'] / merged_df_filter_1['Nettonennleistung']).between(0.1, 15)]



    #filter3 Nach Monat sortieren
    merged_df_filter_2['Inbetriebnahmedatum'] = pd.to_datetime(merged_df_filter_2['Inbetriebnahmedatum'])




    #filter 4 falsche werte aussortieren, hier sollen alle werte raus wo die NutzebarenKapazitäten größer 1000kw sind und es noch nicht vom Netzbetreiber geprüft ist
    # Wenn dieser Filter angewendet wrid, wird ein groß speicher rausgefiltert, 
    filtered_data_to_remove = merged_df_filter_2[(merged_df_filter_2['NutzbareSpeicherkapazitaet'] > 1000) & 
                                                (merged_df_filter_2['NetzbetreiberpruefungStatus'] == '0')]



    merged_df_filter_3 = merged_df_filter_2[~((merged_df_filter_2['NutzbareSpeicherkapazitaet'] > 1000) & 
                                            (merged_df_filter_2['NetzbetreiberpruefungStatus'] == '0'))]

    

    # Sonder Ausnahmen, eine Batterie wird nach prüfung wieder mit rein genommen
    specific_mastr_number = 'SSE978006940074'

    # Filtern Sie den gewünschten Datensatz aus filtered_data_to_remove
    restored_data = filtered_data_to_remove[filtered_data_to_remove['MastrNummer'] == specific_mastr_number]

    # Fügen die wiederhergestellten Daten wieder zu den ursprünglichen Daten hinzu
    merged_df_filter_3 = pd.concat([merged_df_filter_3, restored_data], ignore_index=True)






    start_date = pd.to_datetime('2019-03-01')
    end_date = pd.to_datetime('now')  # Aktuelles Datum


    # Erstellen eine leere Liste, um die einzelnen DataFrames für jeden Monat zu speichern
    result_frames = []

    pivot_result = pd.DataFrame(columns=['Month', 'Heimspeicher', 'Gewerbespeicher', 'Großspeicher', 'Count_Heimspeicher', 'Count_Gewerbespeicher', 'Count_Großspeicher', 'Brutto_Heimspeicher', 'Brutto_Gewerbespeicher', 'Brutto_Großspeicher'])

    # Iterieren über jeden Monat im Zeitraum
    for single_date in pd.date_range(start_date, end_date, freq='MS'):
        # Definieren das Enddatum des Monats
        end_of_month = single_date + pd.offsets.MonthEnd(0)
        
        # Filtern den Datensatz für den aktuellen Monat
        merged_df_filter_date = merged_df_filter_3[(merged_df_filter_3['Inbetriebnahmedatum'] >= single_date) & 
                                                (merged_df_filter_3['Inbetriebnahmedatum'] <= end_of_month)]
        
        # Füge die Spalte 'Month' hinzu
        merged_df_filter_date['Month'] = merged_df_filter_date['Inbetriebnahmedatum'].dt.to_period('M')
        
        # Füge eine Spalte 'Category' hinzu, basierend auf der NutzbarenSpeicherkapazitaet
        merged_df_filter_date['Category'] = pd.cut(merged_df_filter_date['NutzbareSpeicherkapazitaet'], # war mal Brutto
                                                bins=[-np.inf, 30, 1000, np.inf], 
                                                labels=['Heimspeicher', 'Gewerbespeicher', 'Großspeicher'])
        #print(merged_df_filter_date)
        
        # Gruppieren nach Monat und Kategorie und aggregieren Sie die Summe und die Anzahl der Datensätze
        grouped_result = merged_df_filter_date.groupby(['Month', 'Category'])['NutzbareSpeicherkapazitaet'].agg(['sum', 'count']).reset_index()
    
        
        # Fügen das aktuelle DataFrame zur Liste hinzu
        result_frames.append(grouped_result)



    final_result = pd.concat(result_frames, ignore_index=True)


    pivot_result = final_result.pivot(index='Month', columns='Category', values=['sum', 'count']).reset_index()

    # Benennen die Spalten um
    pivot_result.columns = ['Month', 'Heimspeicher', 'Gewerbespeicher', 'Großspeicher', 'Count_Heimspeicher', 'Count_Gewerbespeicher', 'Count_Großspeicher']

    pivot_result['Summe'] = pivot_result[['Heimspeicher', 'Gewerbespeicher', 'Großspeicher']].sum(axis=1)




    cumulative_df = pivot_result.copy()



    index_to_update = cumulative_df.index[cumulative_df['Month'] == '2019-03'][0] 

    # Die bestände von vor 2019 müssen händisch hinzugefügt werden, hier berufe ich mich auf https://www.sciencedirect.com/science/article/pii/S2352152X19309442
    cumulative_df.at[index_to_update, 'Heimspeicher'] += 415000
    cumulative_df.at[index_to_update, 'Großspeicher'] += 400000
    cumulative_df.at[index_to_update, 'Gewerbespeicher'] += 1000


    cumulative_df

    # Berechnen die kumulative Summe für jede Kategorie über die Monate
    cumulative_df['Cumulative_Heimspeicher'] = cumulative_df['Heimspeicher'].cumsum()
    cumulative_df['Cumulative_Gewerbespeicher'] = cumulative_df['Gewerbespeicher'].cumsum()
    cumulative_df['Cumulative_Großspeicher'] = cumulative_df['Großspeicher'].cumsum()

    # Berechnen die kumulative Gesamtsumme über die Monate
    cumulative_df['Cumulative_Summe'] = cumulative_df['Summe'].cumsum()
    
    # als CSV speichern
    cumulative_df.to_csv('docs/germany/data/batterycharts.csv')




def plotbatterchart():

    battery_df = pd.read_csv('docs/germany/data/batterycharts.csv')


    # Annahme: cumulative_df ist Ihr DataFrame mit den kumulierten Daten
    battery_df['Month'] = battery_df['Month'].astype(str)

    # Definieren die gewünschte Reihenfolge der Kategorien
    category_order = ['Cumulative_Großspeicher', 'Cumulative_Gewerbespeicher', 'Cumulative_Heimspeicher']

    new_category_names = {'Cumulative_Großspeicher': 'Großspeicher', 
                      'Cumulative_Gewerbespeicher': 'Gewerbespeicher', 
                      'Cumulative_Heimspeicher': 'Heimspeicher'}

    # Erstellen ein Plotly-Figure-Objekt für ein gestapeltes Balkendiagramm mit der gewünschten Reihenfolge
    fig_power = px.bar(battery_df, x='Month', y=category_order,
                title='Kumulative Summe der Bruttoleistung über die Monate',
                labels={'value': 'Kumulative Summe Bruttoleistung (kw)', 'variable': 'Kategorie'},
                barmode='stack',
                hover_data={'Month': True, 'variable': True, 'value': True},  
                
                color_discrete_map={
                "Cumulative_Großspeicher": colors.standard["first"],
                "Cumulative_Gewerbespeicher": colors.standard["second"],
                "Cumulative_Heimspeicher": colors.standard["third"],
                }
                )

    #fig_power.update_layout(width=800)
    fig_power.for_each_trace(lambda t: t.update(name = new_category_names[t.name],
                                      legendgroup = new_category_names[t.name],
                                      hovertemplate = t.hovertemplate.replace(t.name, new_category_names[t.name])
                                     )
                  )
    

    



    # Definieren Sie die gewünschte Reihenfolge der Kategorien
    category_order = ['Großspeicher', 'Gewerbespeicher', 'Heimspeicher']
    new_category_names = {'Großspeicher': 'Großspeicher', 
                      'Gewerbespeicher': 'Gewerbespeicher', 
                      'Heimspeicher': 'Heimspeicher'}


    # entferne den ersten Datensatz aus battery_df, da der auch die wärte von vor 2019 hat
    battery_df_filtered = battery_df.iloc[1:]

    # Erstellen Sie ein Plotly-Figure-Objekt für ein gestapeltes Balkendiagramm mit der gewünschten Reihenfolge
    fig_energy = px.bar(battery_df_filtered, x='Month', y=category_order,
                title='Zubau Speicherkapazität über die Monate',
                labels={'value': 'NutzbareSpeicherkapazität (kwh)', 'variable': 'Kategorie'},
                barmode='stack',
                hover_data={'Month': True, 'variable': True, 'value': True},  # Hier wird Hovertext konfiguriert
        
                color_discrete_map={
                "Großspeicher": colors.standard["first"],
                "Gewerbespeicher": colors.standard["second"],
                "Heimspeicher": colors.standard["third"],
                }
                )


    # Zeigen Sie das Balkendiagramm an
    #fig_energy.update_layout(width=800)
    fig_energy.for_each_trace(lambda t: t.update(name = new_category_names[t.name],
                                      legendgroup = new_category_names[t.name],
                                      hovertemplate = t.hovertemplate.replace(t.name, new_category_names[t.name])
                                     )
                  )


    fig_energy.update_layout(legend={'traceorder':'reversed'},
                    hovermode = 'x unified',
                    template = "simple_white",
                    barmode="relative",                    
                    )
    fig_power.update_layout(legend={'traceorder':'reversed'},
                    hovermode = 'x unified',
                    template = "simple_white",
                    barmode="relative",                    
                    )

    
    fig_power.update_yaxes(title_text="Bruttoleistung (GW)", tickvals=[0, 1000000, 2000000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000,], ticktext=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    fig_energy.update_yaxes(title_text="NutzbareSpeicherkapazität (MWh)", tickvals=[0, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000,], ticktext=[0, 100, 200, 300, 400, 500, 600, 700, 800, 900])



    fig_energy.write_html("docs/germany/figures/battery_energy_DE.html", include_plotlyjs="cdn")
    fig_power.write_html("docs/germany/figures/battery_power_DE.html", include_plotlyjs="cdn")

    


filternMaSTRBattery()
#plotbatterchart()