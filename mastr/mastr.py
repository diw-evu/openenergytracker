#%% Import required packages

import os
import pandas as pd
import numpy as np
import datetime as datetime

import mastr_functions as mf

from open_mastr import Mastr
from open_mastr.soap_api.download import MaStRDownload
from open_mastr.soap_api.download import MaStRAPI
from sqlalchemy import create_engine
import shutil 


#%% Functions

def info_date(db):
    
    storage_main = pd.read_sql(sql="storage_units",con=db.engine)
    print('Under "DateDownload" you can see when the last update was made')
    print(storage_main.head())


def update_mastr(db):
    db.download()


def filter_solar(solar):
    solar = solar.assign(powerpermodule = solar['Bruttoleistung']/solar['AnzahlModule'])
    solar = solar.assign(gross_net = solar['Bruttoleistung']/solar['Nettonennleistung'])
    solar["factor"] = 1.0
    solar['factor'] = np.where((solar.Nutzungsbereich == "Haushalt") & (solar.Bruttoleistung > 500),1/1e3,1.0)
    solar['factor'] = np.where((solar.AnzahlModule > 10) & (solar.powerpermodule > 5),1/1e3,1.0)
    solar['factor'] = np.where((solar.Lage != "Freifläche") & (solar.Bruttoleistung > 10000),1/1e3,1.0)
    solar['factor'] = np.where((solar.Lage == "Steckerfertige Solaranlage (sog. Balkonkraftwerk)") & (solar.Bruttoleistung > 2),1/1e3,1.0)
    solar['factor'] = np.where((solar.gross_net > 10) & (solar.Bruttoleistung > 1000),1/1e3,1.0)
    #solar.loc[(solar.Nutzungsbereich == "Haushalt") & (solar.Bruttoleistung > 500)]['factor'] = 1/1e3
    #solar.loc[(solar.AnzahlModule > 20) & (solar.powerpermodule > 5)]['factor'] = 1/1e3
    #solar.loc[(solar.Lage != "Freifläche") & (solar.Bruttoleistung > 10000)]['factor'] = 1/1e3
    #solar.loc[(solar.Lage == "Steckerfertige Solaranlage (sog. Balkonkraftwerk)") & (solar.Bruttoleistung > 10)]["factor"] = 1/1e3
    #solar.loc[(solar.gross_net > 100) & (solar.Bruttoleistung > 1000)]['factor'] = 1/1e3
    solar['Bruttoleistung_old'] = solar["Bruttoleistung"]
    solar["Bruttoleistung"] = solar["Bruttoleistung"]*solar["factor"]
    return solar


def compute_average_size(solar):
    df  = solar
    df["date"] = pd.to_datetime(df.Inbetriebnahmedatum)
    df.set_index("date",inplace=True)
    df["year"] = df.index.year
    df = df.groupby(["year","Lage"])["Bruttoleistung"].agg(avg = "mean", count = "count", sum = "sum", max = "max",median = "median").reset_index()
    df = df.query('year > 2016')
    return df


def compile_pv_net_additions_dataset(df,bins_vec):
    # create bins
    df = df.assign(bins = pd.cut(df.Bruttoleistung,bins_vec))
    # begin with additions
    add = df
    add['date'] = pd.to_datetime(add['Inbetriebnahmedatum'])
    add = add.set_index('date')
    add = add.assign(month = add.index.month,year = add.index.year)
    add = add.groupby(['month','year','Lage','Bundesland','bins']).agg({'Bruttoleistung':'sum'})

    # now with reductions
    red = df
    red['date'] = pd.to_datetime(red['DatumEndgueltigeStilllegung'])
    red = red.set_index('date')
    red = red.assign(month = red.index.month,year = red.index.year)
    red = red.groupby(['month','year','Lage','Bundesland','bins']).agg({'Bruttoleistung':'sum'})

    # merge
    tmp = add.merge(red,how='outer',on=['year','month','Lage','Bundesland','bins',])
    tmp = tmp.rename(columns={'Bruttoleistung_x':'additions','Bruttoleistung_y':'reductions'})
    tmp['additions'] = tmp['additions'].fillna(0)
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp = tmp.assign(net_additions = tmp.additions - tmp.reductions)
    tmp = tmp.reset_index()
    
    return tmp


def compile_wind_input_df(df,bins_vec):
    df = df.assign(bins = pd.cut(df.Bruttoleistung,bins_vec))
    #df = df[df['EinheitBetriebsstatus'] != 'In Planung']
    df = df[df['Lage']=="Windkraft an Land"]
    df = df[["EinheitMastrNummer",
             "EinheitBetriebsstatus",
             "Inbetriebnahmedatum",
             "DatumEndgueltigeStilllegung",
             "Bundesland",
             "bins","Bruttoleistung",
             "Rotordurchmesser",
            "Nabenhoehe"
            ]]
    df = df.reset_index()
    return df


def compile_attributes_df(df):
    wind_df = df
    wind_df['month'] = wind_df['Inbetriebnahmedatum'].dt.month
    wind_df['year'] = wind_df['Inbetriebnahmedatum'].dt.year
    wind_df['bl_dia'] = wind_df['Rotordurchmesser']*wind_df['Bruttoleistung']
    wind_df['bl_nh'] = wind_df['Nabenhoehe']*wind_df['Bruttoleistung']
    tmp = wind_df.groupby(['year','month']).agg({'Bruttoleistung':["sum","count"],'bl_dia':'sum','bl_nh':'sum'}).reset_index(col_level=1)
    tmp.columns = ['year','month','sum','count','bl_dia','bl_nh']
    tmp['rotor_diameter'] = tmp['bl_dia']/tmp['sum']
    tmp['hub_height'] = tmp['bl_nh']/tmp['sum'] 
    tmp['average_power'] = tmp['sum']/tmp['count']/1e3
    tmp = tmp.query('year>1998')
    tmp[['rotor_diameter','hub_height','average_power']]= tmp[['rotor_diameter','hub_height','average_power']].rolling(window=12).mean()
    tmp = tmp.query('year>1999')
    tmp = tmp.rename(columns={'rotor_diameter':'Rotor diameter','hub_height':'Hub height','average_power':'Average power'})
    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    tmp = tmp[['date','Rotor diameter','Hub height','Average power']]
    return tmp


def create_size_dfs(df):
    # additions
    adds = df
    adds['month'] = adds['Inbetriebnahmedatum'].dt.month
    adds['year'] = adds['Inbetriebnahmedatum'].dt.year
    adds = adds.groupby(['year','month','bins']).agg({'Bruttoleistung':'sum'}).reset_index()
    adds = adds.rename(columns={'Bruttoleistung':'additions'})
    # reductions
    reds = df
    reds['month'] = reds['DatumEndgueltigeStilllegung'].dt.month
    reds['year'] = reds['DatumEndgueltigeStilllegung'].dt.year
    reds = reds.groupby(['year','month','bins']).agg({'Bruttoleistung':'sum'}).reset_index()
    reds = reds.rename(columns={'Bruttoleistung':'reductions'})

    tmp = adds.merge(reds,how='outer',on=['year','month','bins'])
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp['net_additions'] = tmp['additions']-tmp['reductions']
    

    tmp['cumsum'] = tmp.groupby('bins')['net_additions'].cumsum()

    tmp2 = tmp.groupby(['year','month'])['net_additions'].sum().reset_index()
    tmp2['cumsum'] = tmp2['net_additions'].cumsum()
    tmp2 = tmp2[['year','month','cumsum']]
    tmp2 = tmp2.rename(columns={'cumsum':'total'})
    tmp = tmp.merge(tmp2,on=['year','month'])
    tmp['share'] = tmp['cumsum']/tmp['total']

    size_cum = tmp[['year','month','bins','share']]
    size_cum['date'] = pd.to_datetime(size_cum[['year','month']].assign(day=15))
    size_cum = size_cum.query('year>1999')
    size_cum = size_cum.pivot(index=['date'],columns='bins',values='share').reset_index()

    size_monthly = tmp[['year','month','bins','net_additions']]
    size_monthly = size_monthly.query('year>2016')
    size_monthly['net_additions'] = size_monthly['net_additions']/1e3
    size_monthly['date'] = pd.to_datetime(size_monthly[['year','month']].assign(day=15))
    size_monthly = size_monthly.pivot(index=['date'],columns='bins',values='net_additions').reset_index()
    
    size_monthly.columns = size_monthly.columns.map(str)
    size_cum.columns = size_cum.columns.map(str)
    size_monthly = size_monthly.rename(columns=
    {'(0.0,1000.0]':'0-1 MW',
    '(1000.0,2000.0]':'1-2 MW',
    '(2000.0,3000.0]':'1-2 MW',
    '(3000.0,4000.0]':'3-4 MW',
    '(4000.0,5000.0]':'4-5 MW',
    '(5000.0,1000000.0]':'>5 MW'}
    )

    size_cum = size_cum.rename(columns=
    {'(0.0,1000.0]':'0-1 MW',
    '(1000.0,2000.0]':'1-2 MW',
    '(2000.0,3000.0]':'1-2 MW',
    '(3000.0,4000.0]':'3-4 MW',
    '(4000.0,5000.0]':'4-5 MW',
    '(5000.0,1000000.0]':'>5 MW'}
    )

    
    return size_cum,size_monthly


def create_bundeslaender_dfs(df,scen_data):
    # additions
    adds = df
    adds['month'] = adds['Inbetriebnahmedatum'].dt.month
    adds['year'] = adds['Inbetriebnahmedatum'].dt.year
    adds = adds.groupby(['year','month','Bundesland']).agg({'Bruttoleistung':'sum'}).reset_index()
    adds = adds.rename(columns={'Bruttoleistung':'additions'})
    # reductions
    reds = df
    reds['month'] = reds['DatumEndgueltigeStilllegung'].dt.month
    reds['year'] = reds['DatumEndgueltigeStilllegung'].dt.year
    reds = reds.groupby(['year','month','Bundesland']).agg({'Bruttoleistung':'sum'}).reset_index()
    reds = reds.rename(columns={'Bruttoleistung':'reductions'})

    tmp = adds.merge(reds,how='outer',on=['year','month','Bundesland'])
    tmp['reductions'] = tmp['reductions'].fillna(0)
    tmp['additions'] = tmp['additions'].fillna(0)
    tmp['net_additions'] = tmp['additions']-tmp['reductions']

    tmp = tmp.set_index(['year','month','Bundesland']).T.stack().unstack().T.reset_index()
    tmp = tmp.fillna(0)

    l12 = tmp
    total = tmp
    total = total.groupby('Bundesland').agg({'net_additions':'sum'}).reset_index()
    total['total'] = total['net_additions']/1e6 
    total = total.drop(columns=['net_additions'])
    total = total.merge(scen_data,on='Bundesland')
    total['share'] = total['total']/total['Potential']
    total = total.drop(columns=['Potential'])
    total = total.rename(columns={'total':'Installiert','share':'Anteil am Scenario'})

    tmp['cumsum'] = tmp.groupby('Bundesland')['net_additions'].cumsum()

    tmp2 = tmp.groupby(['year','month'])['net_additions'].sum().reset_index()
    tmp2['cumsum'] = tmp2['net_additions'].cumsum()
    tmp2 = tmp2[['year','month','cumsum']]
    tmp2 = tmp2.rename(columns={'cumsum':'total'})
    tmp = tmp.merge(tmp2,on=['year','month'])
    tmp['share'] = tmp['cumsum']/tmp['total']

    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    
    cum = tmp.pivot(index=['date'],columns='Bundesland',values='share').reset_index()
    cum = cum.query('date>"1999-12-31"')

    tmp = l12
    current_date = datetime.datetime.today()
    end_date = current_date.replace(day=1) - datetime.timedelta(days=1)
    end_date = end_date.replace(day=15)
    start_date = end_date - datetime.timedelta(days=365)

    tmp['date'] = pd.to_datetime(tmp[['year','month']].assign(day=15))
    tmp = tmp.query('date>=@start_date & date<=@end_date')
    tmp = tmp.groupby('Bundesland').agg({'net_additions':'sum'}).reset_index()
    tmp['Last 12M'] = tmp['net_additions']/1e3
    tmp['Last 12M rel'] = tmp['Last 12M']/tmp['Last 12M'].sum()
    l12 = tmp.drop(columns=['net_additions'])


    return cum, l12, total


def start_engine(path='0'):
    if path == '0':
        user= os.getlogin()
        db_path = r"sqlite:///C:/Users/"+user+r"/OneDrive - DIW Berlin/Open Energy Tracker and Ampel-Monitor/OET/input/MaStR/.open-MaStR/data/sqlite/open-mastr.db"
    else:
        db_path= path

    path= create_engine(db_path)
    db = Mastr(engine=path)
    return db


def solar_csv(db):
    solar = pd.read_sql(sql="solar_extended",con=db.engine)
    # exclude units that are not in operation yet
    solar = solar.query('EinheitBetriebsstatus!="In Planung"')
    solar = filter_solar(solar)
    solar.query("Bruttoleistung != Bruttoleistung_old").to_csv("drop_solar.csv")
    compute_average_size(solar).to_csv("avg_size_solar.csv",index=False)
    pv_data = compile_pv_net_additions_dataset(solar,[0,10,25,100,250,500,750,1e3,1e7])
    pv_data.reset_index().to_csv('pv_mastr_data.csv',index=False)



    df = solar.query('Nutzungsbereich == "Haushalt"').groupby(['year','Einspeisungsart']).agg({'Bruttoleistung':'sum'}).reset_index()
    df.groupby('year').agg({'Bruttoleistung':'sum'}).reset_index()/1e6
    df.query('(year==2024) & (Einspeisungsart=="Volleinspeisung")')['Bruttoleistung']/df.query('(year==2024)')['Bruttoleistung'].sum()


    solar["GemeinsamerWechselrichterMitSpeicher"].unique()


    solar = solar.assign(date = pd.to_datetime(solar.Inbetriebnahmedatum))
    solar = solar.assign(year = solar.date.dt.year)
    solar.query('Lage == "Bauliche Anlagen (Hausdach, Gebäude und Fassade)"').query('Bruttoleistung > 1000').sort_values('Bruttoleistung',ascending=False).to_csv("large_rooftop.csv",index=False)

def wind_on_csv(db):
    wind = pd.read_sql(sql="wind_extended",con=db.engine)
    wind_df = compile_wind_input_df(wind,[0,1000,2000,3000,4000,5000,1e6])
    wind_df.to_csv("wind_on_mastr_data.csv")



#%%
def start_update():
    db=start_engine()
    
    #update_mastr(db)       # Update   
    info_date(db)          # check the date of the new verson
    solar_csv(db)




# %%
# Konte ich nicht erfolgreich testen, da beim Test der PC abgeschmiert ist
#start_update()