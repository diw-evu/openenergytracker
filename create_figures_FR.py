#%% Import packages

from pathlib import Path
from sklearn.linear_model import LinearRegression

import plotly.graph_objs as go
from plotly.subplots import make_subplots

import numpy as np
import pandas as pd
import glob
import copy
import scripts.colors as colors
import scripts.util as oet

import shutil

# Create path

Path("docs/france/figures").mkdir(parents=True, exist_ok=True)


#%% France - PV ####################################################################################
# Prepare data
pv_data = oet.prepare_data("docs/france/data/pv.csv","quarters")
pv_data = oet.calculate_trend(pv_data,"actual","4-quarters")
pv_data = oet.calculate_trend(pv_data,"actual","5-years")


#%% Define PV dictionary ###########################################################################

base_pv = {"title"         : "Installierte Leistung Photovoltaik",
           "data"          : pv_data,
           "source_file"   : "docs/france/data/pv.csv",
           "language"      : "de",
           "color_dict"    : colors.standard,
           "x_axis_title"  : None,
           "x_axis_range"  : ["2008-12", "2031-03"],
           "y_axis_title"  : "Bestand [GW]",
           "y_axis_range"  : [0,70],
           "y_axis2_range" : [0,2.5],
           "y_axis2_title" : "Zubau [GW]",
           "subplots" : {"rows"        : 2,
                         "cols"        : 1,
                         "row_heights" : [0.75,0.25]},
           "buttons"  : {"y_max_2030"  : 50,
                         "xaxis_range_2030" : ["2008-12", "2031-12"],
                         "y_max_2045"  : 215,
                         "xaxis_range_2045" : ["2008-12", "2051-12"],
                         "button2030"  : "bis 2030",
                         "button2045"  : "bis 2050"
                         },
            "legend_dict" : {"title" : {"text":None},
                            "traceorder"     :"grouped",
                            "orientation"    :"h",
                            "x"              : 0.01,
                            "y"              : 1,
                            "yanchor"        : "top",
                            "xanchor"        : "left",
                            "borderwidth"    : 1,
                            "font"           : {"size": 11},
                            "grouptitlefont" : {"size": 11},
                            "groupclick"     : "toggleitem"}
           }

dict_pv_de = {
    "base"                     : base_pv.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend_months"      : oet.trend_months.copy(),
    "figure_trend_years"       : oet.trend_years.copy(),
    "figure_goal_low"          : oet.goal_low.copy(),
    "figure_goal_high"         : oet.goal_high.copy(),
    "figure_goal_low_ppe3"     : oet.goal_low.copy(),
    "figure_goal_high_ppe3"    : oet.goal_high.copy(),
    "figure_goal_belfort"      : oet.goal.copy(),
    "figure_goal_lin_low"      : oet.goal_lin_low.copy(),
    "figure_goal_lin_high"     : oet.goal_lin_high.copy(),
    "figure_goal_lin_belfort"  : oet.goal_lin.copy(),
    "figure_futurs50_corr"     : oet.futurs50_corr.copy(),
    "figure_op_corr"           : oet.op_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_low"     : oet.additions_needed_low.copy(),
    "figure_additions_high"    : oet.additions_needed_high.copy(),
    "figure_additions_belfort" : oet.additions_needed_high.copy(),
}

dict_pv_de["figure_goal_belfort"]["y"] = "plan_belfort"
dict_pv_de["figure_goal_belfort"]["group"] = "bel"
dict_pv_de["figure_goal_belfort"]["title_group"] = "<i>Belfort-Rede</i>"
dict_pv_de["figure_goal_belfort"]["name"] = "Ziel der Belfort-Rede"
dict_pv_de["figure_goal_belfort"]["color"] = "plan_target_belfort"
dict_pv_de["figure_goal_lin_belfort"]["y"] = "plan_belfort_linear"
dict_pv_de["figure_goal_lin_belfort"]["showlegend"] = False
dict_pv_de["figure_goal_lin_belfort"]["name"] = "Linearisierter Verlauf"
dict_pv_de["figure_goal_lin_belfort"]["color"] = "plan_target_belfort"
dict_pv_de["figure_additions_belfort"]["y"] = "add_plan_belfort"
dict_pv_de["figure_additions_belfort"]["color"] = "plan_target_belfort"
dict_pv_de["figure_goal_low_ppe3"]["y"] = "plan_low_ppe3"
dict_pv_de["figure_goal_low_ppe3"]["name"] = "Untere Ziele der PPE3"
dict_pv_de["figure_goal_high_ppe3"]["y"] = "plan_high_ppe3"
dict_pv_de["figure_goal_high_ppe3"]["name"] = "Obere Ziele der PPE3"
dict_pv_de["figure_goal_low_ppe3"]["showlegend"] = False
dict_pv_de["figure_goal_high_ppe3"]["showlegend"] = False

#%%

dict_pv_en = copy.deepcopy(dict_pv_de)

# English
dict_pv_en["base"]["title"]                = "Installed PV capacity"
dict_pv_en["base"]["y_axis_title"]         = "Stock [GW]"
dict_pv_en["base"]["y_axis2_title"]        = "Additions [GW]"
dict_pv_en["base"]["buttons"]["button2030"] = "until 2030"
dict_pv_en["base"]["buttons"]["button2045"] = "until 2050"
dict_pv_en["base"]["language"]              = "en"

dict_pv_en["figure_actual"]["name"]                = "Installed"
dict_pv_en["figure_trend_months"]["name"]          = "12-month-trend"
dict_pv_en["figure_trend_years"]["name"]           = "5-year-trend"
dict_pv_en["figure_goal_low"]["title_group"]       = "<i>Government goals</i>"
dict_pv_en["figure_goal_low"]["name"]              = "PPE (low)"
dict_pv_en["figure_goal_high"]["title_group"]      = "<i>Government goals</i>"
dict_pv_en["figure_goal_high"]["name"]             = "PPE (high)"
dict_pv_en["figure_goal_lin_low"]["name"]          = "Linearized progression (low)"
dict_pv_en["figure_goal_lin_high"]["name"]         = "Linearized progression (high)"
dict_pv_en["figure_goal_belfort"]["title_group"]   = "<i>Belfort Address</i>"
dict_pv_en["figure_goal_belfort"]["name"]          = "Target of the Belfort Address"
dict_pv_en["figure_goal_lin_belfort"]["name"]      = "Linearized progression"
dict_pv_en["figure_futurs50_corr"]["title_group"]  = "<i>Scenarios</i>"
dict_pv_en["figure_futurs50_corr"]["name"]         = "Futurs2050 scenario corridor"
dict_pv_en["figure_op_corr"]["title_group"]        = "<i>Scenarios</i>"
dict_pv_en["figure_op_corr"]["name"]               = "openENTRANCE scenario corridor"
dict_pv_en["figure_additions"]["name"]             = "Additions"
dict_pv_en["figure_additions_low"]["name"]         = "Additions needed (low)"
dict_pv_en["figure_additions_high"]["name"]        = "Additions needed (high)"
dict_pv_en["figure_additions_belfort"]["name"]     = "Additions needed (Belfort)"

dict_pv_fr = copy.deepcopy(dict_pv_de)

dict_pv_fr["base"]["title"]                 = "Capacité photovoltaïque installée"
dict_pv_fr["base"]["y_axis_title"]          = "Stock [GW]"
dict_pv_fr["base"]["y_axis2_title"]         = "Ajouts [GW]"
dict_pv_fr["base"]["buttons"]["button2030"] = "jusqu'en 2030"
dict_pv_fr["base"]["buttons"]["button2045"] = "jusqu'en 2050"
dict_pv_fr["base"]["language"]              = "fr"

dict_pv_fr["figure_actual"]["name"]                = "Déjà installée"
dict_pv_fr["figure_trend_months"]["name"]          = "12 derniers mois"
dict_pv_fr["figure_trend_years"]["name"]           = "5 dernières années"
dict_pv_fr["figure_goal_low"]["title_group"]       = "<i>Réglementation</i>"
dict_pv_fr["figure_goal_low"]["name"]              = "PPE (bas)"
dict_pv_fr["figure_goal_high"]["title_group"]      = "<i>Réglementation</i>"
dict_pv_fr["figure_goal_high"]["name"]             = "PPE (haut)"
dict_pv_fr["figure_goal_lin_low"]["name"]          = "Progression linéarisée (bas)"
dict_pv_fr["figure_goal_lin_high"]["name"]         = "Progression linéarisée (haut)"
dict_pv_fr["figure_goal_belfort"]["title_group"]   = "<i>Discours de Belfort</i>"
dict_pv_fr["figure_goal_belfort"]["name"]          = "Objectif"
dict_pv_fr["figure_goal_lin_belfort"]["name"]      = "Progression linéarisée"
dict_pv_fr["figure_futurs50_corr"]["title_group"]  = "<i>Scénarios</i>"
dict_pv_fr["figure_futurs50_corr"]["name"]         = "Couloir de scénarios Futurs2050"
dict_pv_fr["figure_op_corr"]["title_group"]        = "<i>Scénarios</i>"
dict_pv_fr["figure_op_corr"]["name"]               = "Couloir de scénarios openENTRANCE"
dict_pv_fr["figure_additions"]["name"]             = "Ajouts"
dict_pv_fr["figure_additions_low"]["name"]         = "Ajouts requis (bas)"
dict_pv_fr["figure_additions_high"]["name"]        = "Ajouts requis (haut)"
dict_pv_fr["figure_additions_belfort"]["name"]     = "Ajouts requis (Belfort)"


# Create figures

fig_pv_de  = oet.create_fig(dict_pv_de)
fig_pv_en  = oet.create_fig(dict_pv_en)
fig_pv_fr  = oet.create_fig(dict_pv_fr)

# Write figures

fig_pv_de.write_html("docs/france/figures/pv.de.html", include_plotlyjs="directory")
fig_pv_en.write_html("docs/france/figures/pv.en.html", include_plotlyjs="directory")
fig_pv_fr.write_html("docs/france/figures/pv.fr.html", include_plotlyjs="directory")

#%% France - Wind onshore ####################################################################################

# Prepare data
windon_data = oet.prepare_data("docs/france/data/wind_onshore.csv","quarters")
windon_data = oet.calculate_trend(windon_data,"actual","4-quarters")
windon_data = oet.calculate_trend(windon_data,"actual","5-years")


#%% Define wind onshore dictionary ###########################################################################

base_windon = {"title"         : "Installierte Leistung Windkraft an Land",
           "data"          : windon_data,
           "source_file"   : "docs/france/data/wind_onshore.csv",
           "language"      : "de",
           "color_dict"    : colors.standard,
           "x_axis_title"  : None,
           "x_axis_range"  : ["2008-12", "2031-03"],
           "y_axis_title"  : "Bestand [GW]",
           "y_axis_range"  : [0,50],
           "y_axis2_range" : [0.0,1],
           "y_axis2_title" : "Zubau [GW]",
           "subplots" : {"rows"        : 2,
                         "cols"        : 1,
                         "row_heights" : [0.75,0.25]},
           "buttons"  : {"y_max_2030"  : 50,
                         "xaxis_range_2030" : ["2008-12", "2031-12"],
                         "y_max_2045"  : 135,
                         "xaxis_range_2045" : ["2008-12", "2050-12"],
                         "button2030"  : "bis 2030",
                         "button2045"  : "bis 2050"
                         },
            "legend_dict" : {"title" : {"text":None},
                            "traceorder"     :"grouped",
                            "orientation"    :"h",
                            "x"              : 0.01,
                            "y"              : 1,
                            "yanchor"        : "top",
                            "xanchor"        : "left",
                            "borderwidth"    : 1,
                            "font"           : {"size": 11},
                            "grouptitlefont" : {"size": 11},
                            "groupclick"     : "toggleitem"}
           }

dict_windon_de = {
    "base"                     : base_windon.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend_months"      : oet.trend_months.copy(),
    "figure_trend_years"       : oet.trend_years.copy(),
    "figure_goal_low"          : oet.goal_low.copy(),
    "figure_goal_high"         : oet.goal_high.copy(),
    "figure_goal_lin_low"      : oet.goal_lin_low.copy(),
    "figure_goal_lin_high"     : oet.goal_lin_high.copy(),
    "figure_futurs50_corr"     : oet.futurs50_corr.copy(),
    "figure_op_corr"           : oet.op_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_low"     : oet.additions_needed_low.copy(),
    "figure_additions_high"    : oet.additions_needed_high.copy(),
}

#%%

dict_windon_en = copy.deepcopy(dict_windon_de)

# English
dict_windon_en["base"]["title"]                 = "Installed wind onshore capacity"
dict_windon_en["base"]["y_axis_title"]          = "Stock [GW]"
dict_windon_en["base"]["y_axis2_title"]         = "Additions [GW]"
dict_windon_en["base"]["buttons"]["button2030"] = "until 2030"
dict_windon_en["base"]["buttons"]["button2045"] = "until 2050"
dict_windon_en["base"]["language"]              = "en"

dict_windon_en["figure_actual"]["name"]               = "Installed"
dict_windon_en["figure_trend_months"]["name"]         = "12-month-trend"
dict_windon_en["figure_trend_years"]["name"]          = "5-year-trend"
dict_windon_en["figure_goal_low"]["title_group"]      = "<i>Government goals</i>"
dict_windon_en["figure_goal_low"]["name"]             = "PPE (low)"
dict_windon_en["figure_goal_high"]["title_group"]     = "<i>Government goals</i>"
dict_windon_en["figure_goal_high"]["name"]            = "PPE (high)"
dict_windon_en["figure_goal_lin_low"]["name"]         = "Linearized progression (low)"
dict_windon_en["figure_goal_lin_high"]["name"]        = "Linearized progression (high)"
dict_windon_en["figure_op_corr"]["title_group"]       = "<i>Scenarios</i>"
dict_windon_en["figure_op_corr"]["name"]              = "openENTRANCE scenario corridor"
dict_windon_en["figure_futurs50_corr"]["title_group"] = "<i>Scenarios</i>"
dict_windon_en["figure_futurs50_corr"]["name"]        = "Futurs2050 scenario corridor"
dict_windon_en["figure_additions"]["name"]            = "Additions"
dict_windon_en["figure_additions_low"]["name"]        = "Additions needed (low)"
dict_windon_en["figure_additions_high"]["name"]       = "Additions needed (high)"

dict_windon_fr = copy.deepcopy(dict_windon_de)

dict_windon_fr["base"]["title"]                 = "Capacité éolien terrestre installée"
dict_windon_fr["base"]["y_axis_title"]          = "Stock [GW]"
dict_windon_fr["base"]["y_axis2_title"]         = "Ajouts [GW]"
dict_windon_fr["base"]["buttons"]["button2030"] = "jusqu'en 2030"
dict_windon_fr["base"]["buttons"]["button2045"] = "jusqu'en 2050"
dict_windon_fr["base"]["language"]              = "fr"

dict_windon_fr["figure_actual"]["name"]                = "Déjà installée"
dict_windon_fr["figure_trend_months"]["name"]          = "12 derniers mois"
dict_windon_fr["figure_trend_years"]["name"]           = "5 dernières années"
dict_windon_fr["figure_goal_low"]["title_group"]       = "<i>Réglementation</i>"
dict_windon_fr["figure_goal_low"]["name"]              = "PPE (bas)"
dict_windon_fr["figure_goal_high"]["title_group"]      = "<i>Réglementation</i>"
dict_windon_fr["figure_goal_high"]["name"]             = "PPE (haut)"
dict_windon_fr["figure_goal_lin_low"]["name"]          = "Progression linéarisée (bas)"
dict_windon_fr["figure_goal_lin_high"]["name"]         = "Progression linéarisée (haut)"
dict_windon_fr["figure_op_corr"]["title_group"]        = "<i>Scénarios</i>"
dict_windon_fr["figure_op_corr"]["name"]               = "Couloir de scénarios openENTRANCE"
dict_windon_fr["figure_futurs50_corr"]["title_group"]  = "<i>Scénarios</i>"
dict_windon_fr["figure_futurs50_corr"]["name"]         = "Couloir de scénarios Futurs2050"
dict_windon_fr["figure_additions"]["name"]             = "Ajouts"
dict_windon_fr["figure_additions_low"]["name"]         = "Ajouts requis (bas)"
dict_windon_fr["figure_additions_high"]["name"]        = "Ajouts requis (haut)"

# Create figures

fig_windon_de  = oet.create_fig(dict_windon_de)
fig_windon_en  = oet.create_fig(dict_windon_en)
fig_windon_fr  = oet.create_fig(dict_windon_fr)

# Write figures

fig_windon_de.write_html("docs/france/figures/windon.de.html", include_plotlyjs="directory")
fig_windon_en.write_html("docs/france/figures/windon.en.html", include_plotlyjs="directory")
fig_windon_fr.write_html("docs/france/figures/windon.fr.html", include_plotlyjs="directory")

#%% France - Wind offshore ####################################################################################

# Prepare data
windoff_data = oet.prepare_data("docs/france/data/wind_offshore.csv","quarters")
windoff_data = oet.calculate_trend(windoff_data,"actual","4-quarters")
windoff_data = oet.calculate_trend(windoff_data,"actual","5-years")


#%% Define wind offshore dictionary ###########################################################################

base_windoff = {"title"         : "Installierte Leistung Windkraft auf See",
           "data"          : windoff_data,
           "source_file"   : "docs/france/data/wind_offshore.csv",
           "language"      : "de",
           "color_dict"    : colors.standard,
           "x_axis_title"  : None,
           "x_axis_range"  : ["2008-12", "2031-03"],
           "y_axis_title"  : "Bestand [GW]",
           "y_axis_range"  : [0.0,40],
           "y_axis2_range" : [0.0,1],
           "y_axis2_title" : "Zubau [GW]",
           "subplots" : {"rows"        : 2,
                         "cols"        : 1,
                         "row_heights" : [0.75,0.25]},
           "buttons"  : {"y_max_2030"  : 10,
                         "xaxis_range_2030" : ["2008-12", "2030-12"],
                         "y_max_2045"  : 65,
                         "xaxis_range_2045" : ["2008-12", "2051-12"],
                         "button2030"  : "bis 2030",
                         "button2045"  : "bis 2050"
                         },
            "legend_dict" : {"title" : {"text":None},
                            "traceorder"     :"grouped",
                            "orientation"    :"h",
                            "x"              : 0.01,
                            "y"              : 1,
                            "yanchor"        : "top",
                            "xanchor"        : "left",
                            "borderwidth"    : 1,
                            "font"           : {"size": 11},
                            "grouptitlefont" : {"size": 11},
                            "groupclick"     : "toggleitem"}
           }

dict_windoff_de = {
    "base"                     : base_windoff.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend_months"      : oet.trend_months.copy(),
    "figure_trend_years"       : oet.trend_years.copy(),
    "figure_goal_low"          : oet.goal_low.copy(),
    "figure_goal_high"         : oet.goal_high.copy(),
    "figure_goal_belfort"      : oet.goal.copy(),
    "figure_goal_lin_low"      : oet.goal_lin_low.copy(),
    "figure_goal_lin_high"     : oet.goal_lin_high.copy(),
    "figure_goal_lin_belfort"  : oet.goal_lin.copy(),
    "figure_futurs50_corr"     : oet.futurs50_corr.copy(),
    "figure_op_corr"           : oet.op_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_low"     : oet.additions_needed_low.copy(),
    "figure_additions_high"    : oet.additions_needed_high.copy(),
    "figure_additions_belfort" : oet.additions_needed_high.copy(),
}

dict_windoff_de["figure_goal_belfort"]["y"] = "plan_belfort"
dict_windoff_de["figure_goal_belfort"]["group"] = "bel"
dict_windoff_de["figure_goal_belfort"]["title_group"] = "<i>Belfort-Rede</i>"
dict_windoff_de["figure_goal_belfort"]["name"] = "Ziel der Belfort-Rede"
dict_windoff_de["figure_goal_belfort"]["color"] = "plan_target_belfort"
dict_windoff_de["figure_goal_lin_belfort"]["y"] = "plan_belfort_linear"
dict_windoff_de["figure_goal_lin_belfort"]["showlegend"] = False
dict_windoff_de["figure_goal_lin_belfort"]["name"] = "Linearisierter Verlauf"
dict_windoff_de["figure_goal_lin_belfort"]["color"] = "plan_target_belfort"
dict_windoff_de["figure_additions_belfort"]["y"] = "add_plan_belfort"
dict_windoff_de["figure_additions_belfort"]["color"] = "plan_target_belfort"
#%%

dict_windoff_en = copy.deepcopy(dict_windoff_de)

# English
dict_windoff_en["base"]["title"]                = "Installed wind offshore capacity"
dict_windoff_en["base"]["y_axis_title"]         = "Stock [GW]"
dict_windoff_en["base"]["y_axis2_title"]        = "Additions [GW]"
dict_windoff_en["base"]["buttons"]["button2030"] = "until 2030"
dict_windoff_en["base"]["buttons"]["button2045"] = "until 2050"
dict_windoff_en["base"]["language"]              = "en"

dict_windoff_en["figure_actual"]["name"]         = "Installed"
dict_windoff_en["figure_trend_months"]["name"]          = "12-month-trend"
dict_windoff_en["figure_trend_years"]["name"]          = "5-year-trend"
dict_windoff_en["figure_goal_low"]["title_group"]    = "<i>Government goals</i>"
dict_windoff_en["figure_goal_low"]["name"]           = "PPE (low)"
dict_windoff_en["figure_goal_high"]["title_group"]    = "<i>Government goals</i>"
dict_windoff_en["figure_goal_high"]["name"]           = "PPE (high)"
dict_windoff_en["figure_goal_lin_low"]["name"]       = "Linearized progression (low)"
dict_windoff_en["figure_goal_lin_high"]["name"]       = "Linearized progression (high)"
dict_windoff_en["figure_goal_belfort"]["title_group"] = "<i>Belfort Address</i>"
dict_windoff_en["figure_goal_belfort"]["name"] = "Target of the Belfort Address"
dict_windoff_en["figure_goal_lin_belfort"]["name"] = "Linearized progression"
dict_windoff_en["figure_op_corr"]["title_group"]       = "<i>Scenarios</i>"
dict_windoff_en["figure_op_corr"]["name"]              = "openENTRANCE scenario corridor"
dict_windoff_en["figure_futurs50_corr"]["title_group"] = "<i>Scenarios</i>"
dict_windoff_en["figure_futurs50_corr"]["name"]        = "Futurs2050 scenario corridor"
dict_windoff_en["figure_additions"]["name"]            = "Additions"
dict_windoff_en["figure_additions_low"]["name"]        = "Additions needed (low)"
dict_windoff_en["figure_additions_high"]["name"]       = "Additions needed (high)"
dict_windoff_en["figure_additions_belfort"]["name"]    = "Additions needed (Belfort)"

dict_windoff_fr = copy.deepcopy(dict_windoff_de)

dict_windoff_fr["base"]["title"]                 = "Capacité éolien en mer installée"
dict_windoff_fr["base"]["y_axis_title"]          = "Stock [GW]"
dict_windoff_fr["base"]["y_axis2_title"]         = "Ajouts [GW]"
dict_windoff_fr["base"]["buttons"]["button2030"] = "jusqu'en 2030"
dict_windoff_fr["base"]["buttons"]["button2045"] = "jusqu'en 2050"
dict_windoff_fr["base"]["language"]              = "fr"

dict_windoff_fr["figure_actual"]["name"]         = "Déjà installée"
dict_windoff_fr["figure_trend_months"]["name"]          = "12 derniers mois"
dict_windoff_fr["figure_trend_years"]["name"]          = "5 dernières années"
dict_windoff_fr["figure_goal_low"]["title_group"]    = "<i>Réglementation</i>"
dict_windoff_fr["figure_goal_low"]["name"]           = "PPE (bas)"
dict_windoff_fr["figure_goal_high"]["title_group"]    = "<i>Réglementation</i>"
dict_windoff_fr["figure_goal_high"]["name"]           = "PPE (haut)"
dict_windoff_fr["figure_goal_lin_low"]["name"]       = "Progression linéarisée (bas)"
dict_windoff_fr["figure_goal_lin_high"]["name"]       = "Progression linéarisée (haut)"
dict_windoff_fr["figure_goal_belfort"]["title_group"] = "<i>Discours de Belfort</i>"
dict_windoff_fr["figure_goal_belfort"]["name"] = "Objectif"
dict_windoff_fr["figure_goal_lin_belfort"]["name"] = "Progression linéarisée"
dict_windoff_fr["figure_op_corr"]["title_group"]        = "<i>Scénarios</i>"
dict_windoff_fr["figure_op_corr"]["name"]               = "Couloir de scénarios openENTRANCE"
dict_windoff_fr["figure_futurs50_corr"]["title_group"]  = "<i>Scénarios</i>"
dict_windoff_fr["figure_futurs50_corr"]["name"]         = "Couloir de scénarios Futurs2050"
dict_windoff_fr["figure_additions"]["name"]             = "Ajouts"
dict_windoff_fr["figure_additions_low"]["name"]         = "Ajouts requis (bas)"
dict_windoff_fr["figure_additions_high"]["name"]        = "Ajouts requis (haut)"
dict_windoff_fr["figure_additions_belfort"]["name"]     = "Ajouts requis (Belfort)"

# Create figures

fig_windoff_de  = oet.create_fig(dict_windoff_de)
fig_windoff_en  = oet.create_fig(dict_windoff_en)
fig_windoff_fr  = oet.create_fig(dict_windoff_fr)

# Write figures

fig_windoff_de.write_html("docs/france/figures/windoff.de.html", include_plotlyjs="directory")
fig_windoff_en.write_html("docs/france/figures/windoff.en.html", include_plotlyjs="directory")
fig_windoff_fr.write_html("docs/france/figures/windoff.fr.html", include_plotlyjs="directory")

#%% Hydropower in France
hydro_france_data          = pd.read_csv("docs/france/data/hydro.csv", sep = ",")
hydro_france_data['date']  = pd.to_datetime(hydro_france_data['date'], format='%Y') #'%d.%m.%Y'

# Create hypower figure for France

def create_hydro_france(data, title,
                        actual, actual_group,
                        actual_ror, actual_rsvr_small, actual_rsvr_big, actual_phs,
                        goal_low, goal_high, goal_group,
                        unit):
    """
    Create hydro power capacity figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data[data['date']<'2023']['date'], y = data[data['date']<'2023']['actual'],
                         name = actual,
                         marker = dict(color= 'rgba(60, 67, 90, 0.7)', 
                                       line=(dict(width=1, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.2f}GW',
                         legendgroup = 'actual',
                         legendgrouptitle_text=actual_group))
    fig.add_trace(go.Bar(x = data[data['date']=='2023']['date'], y = data[data['date']=='2023']['actual_ror'],
                         name = actual_ror,
                         marker = dict(color= 'rgba(42, 106, 137, 0.7)', 
                                       line=(dict(width=1, color='rgba(42, 106, 137, 1)'))),
                         hovertemplate = '%{y:.1f}GW',
                         legendgroup = 'actual',
                         legendgrouptitle_text=actual_group))
    fig.add_trace(go.Bar(x = data[data['date']=='2023']['date'], y = data[data['date']=='2023']['actual_rsvr_small'],
                         name = actual_rsvr_small,
                         marker = dict(color= 'rgba(0, 148, 164, 0.7)', 
                                       line=(dict(width=1, color='rgba(0, 148, 164, 1)'))),
                         hovertemplate = '%{y:.1f}GW',
                         legendgroup = 'actual',
                         legendgrouptitle_text=actual_group))
    fig.add_trace(go.Bar(x = data[data['date']=='2023']['date'], y = data[data['date']=='2023']['actual_rsvr_big'],
                         name = actual_rsvr_big,
                         marker = dict(color= 'rgba(24, 188, 160, 0.7)', 
                                       line=(dict(width=1, color='rgba(24, 188, 160, 1)'))),
                         hovertemplate = '%{y:.1f}GW',
                         legendgroup = 'actual',
                         legendgrouptitle_text=actual_group))
    fig.add_trace(go.Bar(x = data[data['date']=='2023']['date'], y = data[data['date']=='2023']['actual_phs'],
                         name = actual_phs,
                         marker = dict(color= 'rgba(139, 224, 135, 0.7)', 
                                       line=(dict(width=1, color='rgba(139, 224, 135, 1)'))),
                         hovertemplate = '%{y:.1f}GW',
                         legendgroup = 'actual',
                         legendgrouptitle_text=actual_group))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_low'],
                             mode = "markers", name = goal_low,
                             marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "rgba(172, 69, 47, 1)",
                             hovertemplate = '%{y:.1f}GW',
                             legendgroup = 'steps',
                             legendgrouptitle_text=goal_group))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_high'],
                             mode = "markers", name = goal_high,
                             marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "rgba(221, 165, 61, 1)",
                             hovertemplate = '%{y:.1f}GW',
                             legendgroup = 'steps'))
    fig.update_yaxes(matches = None, title = unit, range = [0,28])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2006-09","2031-01"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            barmode = 'stack',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.75,
                                    "y"              : 0.18,
                                    "yanchor"        : "bottom",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 0.8,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))
    
    return fig

# Create figures 
fig_hydro_france_de = create_hydro_france(hydro_france_data, 
                                          "Installierte Leistung Wasserkraft", 
                                          "Alle Technologien", 
                                          "<i>Installiert</i>", 
                                          "Laufwasserkraftwerke", "Speicherkraftwerke (klein)", "Speicherkraftwerke (groß)", "Pumpspeicherkraftwerke",
                                          "PPE (Unteres Ziel)", "PPE (Oberes Ziel)", "<i>Ziele</i>", 
                                          "GW")
fig_hydro_france_en = create_hydro_france(hydro_france_data, 
                                          "Hydropower installed capacity", 
                                          "All technologies", 
                                          "<i>Installed</i>", 
                                          "Run-of-river", "Reservoirs (small)", "Reservoirs (large)", "Pumped-hydro storage",
                                          "PPE (low)", "PPE (high)", "<i>Targets</i>", 
                                          "GW")
fig_hydro_france_fr = create_hydro_france(hydro_france_data, 
                                          "Capacité hydroélectrique installée", 
                                          "Toutes technologies confondues", 
                                          "<i>Installé</i>", 
                                          "Au fil de l'eau", "Éclusées", "Lacs", "STEP",
                                          "PPE (bas)", "PPE (haut)", "<i>Objectifs</i>", 
                                          "GW")
# Export to html
fig_hydro_france_de.write_html("docs/france/figures/hydro_france.de.html", include_plotlyjs="directory")
fig_hydro_france_en.write_html("docs/france/figures/hydro_france.en.html", include_plotlyjs="directory")
fig_hydro_france_fr.write_html("docs/france/figures/hydro_france.fr.html", include_plotlyjs="directory")

#%% RES share in final gross energy consumption in France ##########################################

res_france_data          = pd.read_csv("docs/france/data/resshares_cons.csv", sep = ",")
res_france_data['date']  = pd.to_datetime(res_france_data['date'], format='%Y') #'%d.%m.%Y'

# Create indexm
res_france_data['X']     = res_france_data.index

#%% trend 2017-2021
res_reg1_data = res_france_data[['date','X','actual']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2019') & (res_reg1_data['date'] <= '2023')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual'])

predict1_rs = res_france_data[['date','X','actual']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2023')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2019_2023'] = predict1_rs.loc[0,'actual'] + predict1_rs[
    'index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2019_2023']]

#%% merge
res_france_data = res_france_data.merge(predict1_rs, how='left', on='date')

#%% create res share figure for France

def create_resshares_france(data,title,
                            observed_share_total, observed_share_hydro, observed_share_wind, 
                            observed_share_respower_other, observed_share_biomass, observed_share_hp, 
                            observed_share_rescool, observed_share_resheat_other, observed_share_biofuels, 
                            trend_2018_2022, 
                            achieved_group_name, trends_group_name,target_group_name,
                            linear,goal_ce,goal_lte,step_group_legend,goal_ppe_low,goal_ppe_high,percentage,
                            scenarios_group_name, opentrance_span):
    """
    Create res share figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_hydro'],
                         name = observed_share_hydro,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(70, 30, 247, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_wind'],
                         name = observed_share_wind,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(50, 150, 168, 0.5)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_respower_other'],
                         name = observed_share_respower_other,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(188, 225, 232, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_biomass'],
                         name = observed_share_biomass,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(166, 96, 12, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_hp'],
                         name = observed_share_hp,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(247, 153, 30, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_rescool'],
                         name = observed_share_rescool,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(21, 176, 153, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_resheat_other'],
                         name = observed_share_resheat_other,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(150, 130, 2, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data[data['date']<'2024']['date'], y = data[data['date']<'2024']['actual_biofuels'],
                         name = observed_share_biofuels,
                         mode = 'lines',
                         stackgroup='tech',
                         marker_color = 'rgba(2, 150, 76, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['actual'],
                         name = observed_share_total,
                         mode = 'lines',
                         marker_color = 'rgba(60, 67, 90, 1)',
                         hovertemplate = '%{y:.1f}%',
                         legend = "legend1",
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2019_2023'],
                             name = trend_2018_2022,
                             line = dict(color="rgba(60, 67, 90, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legend = "legend2",
                             legendgroup = 'trends',
                             legendgrouptitle_text = trends_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="rgba(43, 97, 78, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legend = "legend2",
                             legendgroup = 'trends'
                             ))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ce'],
                             mode = "markers", name = goal_ce,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 0.6)",
                             hovertemplate = '%{y:.0f}%',
                             legend = "legend2",
                             legendgroup = 'target',
                             legendgrouptitle_text= target_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_lte'],
                             mode = "markers", name = goal_lte,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 1)",
                             hovertemplate = '%{y:.0f}%',
                             legend = "legend2",
                             legendgroup = 'target'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ppe_low'],
                             mode = "markers", name = goal_ppe_low,
                             marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "rgba(172, 69, 47, 1)",
                             hovertemplate = '%{y:.0f}%',
                             legend = "legend2",
                             legendgroup = 'steps',
                             legendgrouptitle_text=step_group_legend))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ppe_high'],
                             mode = "markers", name = goal_ppe_high,
                             marker_size=12, marker_symbol = "x-dot", 
                             marker_color = "rgba(221, 165, 61, 1)",
                             hovertemplate = '%{y:.0f}%',
                             legend = "legend2",
                             legendgroup = 'steps'))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['opentrance_max'], data['opentrance_min'][::-1]]),
                             name = opentrance_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legend = "legend2",
                             legendgroup="opentrance",
                             legendgrouptitle_text=scenarios_group_name,
                             hoverinfo='skip'
                             ))

    fig.update_yaxes(matches = None, title = percentage, range = [0,40])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2004-06","2031-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend1 = {'traceorder':'grouped',
                                      'x': 1.15, 'y': 0.2,
                                      'yanchor': 'bottom', 'xanchor': 'right', 'xref' : 'container',
                                      'font': {'size': 13, 'color': '#000000'}},
                            legend2 = {'traceorder':'grouped', 'orientation' : 'h',
                                        #'entrywidth' : 70,
                                        'x': 0.0, 'y': -0.2,
                                        'yanchor': 'top', 'xanchor': 'left', 
                                        'font': {'size': 13, 'color': '#000000'}},
                            margin=dict(l=0, r=10, t=130, b=0, pad=0),
                            #height=550, width=50,
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_resshares_france_de = create_resshares_france(res_france_data, "Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch",
                                                "Total", "Wasserkraft", "Windkraft", "Andere EE Strom",
                                                "Biomasse", "Wärmepumpen", "Erneuerbare Kälte", "Andere EE Wärme & Kälte", "Biokraftstoffe", 
                                                "2019 - 2023",
                                                "Erreicht", "Trends", "Ziele",          
                                                "Linear Verlauf",
                                                "Ziel der LEC",
                                                "Ziel der LEC",
                                                "Indikative Meilensteine",
                                                "Meilenstein PPE (unten)",
                                                "Meilenstein PPE (oben)",
                                                "Prozent",
                                                "openENTRANCE Szenarien", "Szenariokorridor")

fig_resshares_france_en = create_resshares_france(res_france_data,
                             "Share of renewable energy in gross final energy consumption",
                             "Total", "Hydropower", "Wind", "Other RES power",
                             "Biomass", "Heat pumps", "Renewable Cooling", "Other RES H&C", "Biofuels", 
                             "2019 - 2023",
                             "Achieved","Trends","Targets",           
                             "Linear trend",
                             "Target of the CE",
                             "Target of LEC",
                             "Indicative milestones",
                             "Milestone PPE (low)",
                             "Milestone PPE (high)",
                             "Percentage",
                             "openENTRANCE scenarios", "Scenario corridor")

fig_resshares_france_fr = create_resshares_france(res_france_data,
                             "Part des EnR dans la consommation finale brute d'énergie",
                             "Total", "Hydraulique", "Éolien", "Autres fillières électriques",
                             "Biomasse solide", "Pompes à chaleur", "Froid renouvelable", "Autres filières chaleur", "Biocarburants", 
                             "2019 - 2023",
                             "Réalisé", "Tendances", "Objectifs",
                             "Progression linéaire",
                             "Objectif du CE",
                             "Objectif de la LEC",
                             "Jalons indicatifs",
                             "Jalon de la PPE (bas)",
                             "Jalon de la PPE (haut)",
                             "Pourcentage",
                             "Scénarios openENTRANCE", "Intervalle de scénarios")

fig_resshares_france_de.write_html("docs/france/figures/resshares.de.html", include_plotlyjs="directory")
fig_resshares_france_en.write_html("docs/france/figures/resshares.en.html", include_plotlyjs="directory")
fig_resshares_france_fr.write_html("docs/france/figures/resshares.fr.html", include_plotlyjs="directory")

#%% Res share in final gross power consumption in France ########################################################

respower_france_data          = pd.read_csv("docs/france/data/resshares_power.csv", sep = ",")
respower_france_data['date']  = pd.to_datetime(respower_france_data['date'], format='%Y') #'%d.%m.%Y'

# Create indexm
respower_france_data['X']     = respower_france_data.index

#%% trend 2017-2021
res_reg1_data = respower_france_data[['date','X','actual']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2019') & (res_reg1_data['date'] <= '2023')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual'])

predict1_rs = respower_france_data[['date','X','actual']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2023')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2019_2023'] = predict1_rs.loc[0,'actual'] + predict1_rs['index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2019_2023']]

#%% merge
respower_france_data = respower_france_data.merge(predict1_rs, how='left', on='date')

#%% create res power share figure for France

def create_respower_france(data, title,
                           observed_share, observed_group,
                           trend_2018_2022, plan_group,
                           linear, 
                           goal, target_group,
                           percentage):
    """
    Create res power share figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = observed_share,
                         marker = dict(color= 'rgba(60, 67, 90, 0.7)', 
                                       line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.2f}%',
                         legendgroup = 'observed',
                         legendgrouptitle_text=observed_group))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_lte'],
                             mode = "markers", 
                             name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 1)",
                             hovertemplate = '%{y:.0f}%',
                             legendgroup = 'target',
                             legendgrouptitle_text=target_group))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2019_2023'],
                             name = trend_2018_2022,
                             mode = "lines",
                             line = dict(color="rgba(60, 67, 90, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             visible='legendonly',
                             legendgroup = 'plan',
                             legendgrouptitle_text=plan_group
                             ))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="rgba(43, 97, 78, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'plan',
                             legendgrouptitle_text=plan_group
                             ))

    fig.update_yaxes(matches = None, title = percentage, range = [0,50])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2011-06","2031-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 1,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_respower_france_de = create_respower_france(respower_france_data,
                             "Anteil der erneuerbaren Energien an der Stromerzeugung",
                             "Tatsächlich erreicht", "Erreicht",    
                             "2019 - 2023", "Verlauf",       
                             "Linear Verlauf", 
                             "Ziel der LEC", "Ziele",
                             "Prozent")

fig_respower_france_en = create_respower_france(respower_france_data,
                             "Share of renewable energy in power production",
                             "Actually reached", "Reached",        
                             "2019 - 2023", "Trends",     
                             "Linear trend", 
                             "Target of the LEC", "Targets",
                             "Percentage")

fig_respower_france_fr = create_respower_france(respower_france_data,
                             "Part des EnR dans la production d'électricité",
                             "Part réalisée", "Réalisé",
                             "2019 - 2023", "Tendances",
                             "Progression linéaire", 
                             "Objectif de la LEC", "Objectifs",
                             "Pourcentage")

fig_respower_france_de.write_html("docs/france/figures/respower.de.html", include_plotlyjs="directory")
fig_respower_france_en.write_html("docs/france/figures/respower.en.html", include_plotlyjs="directory")
fig_respower_france_fr.write_html("docs/france/figures/respower.fr.html", include_plotlyjs="directory")

#%% Res share in final gross heat consumption in France ############################################

resheat_france_data          = pd.read_csv("docs/france/data/resshares_heat.csv", sep = ",")
resheat_france_data['date']  = pd.to_datetime(resheat_france_data['date'], format='%Y') #'%d.%m.%Y'

# Create indexm
resheat_france_data['X']     = resheat_france_data.index

#%% trend 2017-2021
res_reg1_data = resheat_france_data[['date','X','actual']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2019') & (res_reg1_data['date'] <= '2023')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual'])

predict1_rs = resheat_france_data[['date','X','actual']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2023')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2019_2023'] = predict1_rs.loc[0,'actual'] + predict1_rs['index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2019_2023']]

#%% merge
resheat_france_data = resheat_france_data.merge(predict1_rs, how='left', on='date')


#%% create heat share figure for France

def create_resheat_france(data,title,observed_share,
                          trend_2018_2022,
                          achieved_group_name, trends_group_name,target_group_name,
                          linear,goal_ltecv,
                          step_group_legend,goal_ppe_low,goal_ppe_high,percentage,
                          scenarios_group_name, opentrance_span):
    
    """
    Create heat share figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = observed_share,
                         #mode = "lines",
                         marker = dict(color= 'rgba(60, 67, 90, 0.7)', 
                         line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.1f}%',
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2019_2023'],
                             name = trend_2018_2022,
                             line = dict(color="rgba(60, 67, 90, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legendgroup = 'trends',
                             legendgrouptitle_text = trends_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="rgba(43, 97, 78, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'trends'
                             ))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ltecv'],
                             mode = "markers", name = goal_ltecv,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 1)",
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'target',
                             legendgrouptitle_text= target_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ppe_low'],
                             mode = "markers", name = goal_ppe_low,
                             marker_size=12, marker_symbol = "x-dot",
                             marker_color = "rgba(172, 69, 47, 1)",
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'steps',
                             legendgrouptitle_text=step_group_legend))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ppe_high'],
                             mode = "markers", name = goal_ppe_high,
                             marker_size=12, marker_symbol = "x-dot",
                             marker_color = "rgba(221, 165, 61, 1)",
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'steps'))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['opentrance_max'], data['opentrance_min'][::-1]]),
                             name = opentrance_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="opentrance",
                             legendgrouptitle_text=scenarios_group_name,
                             hoverinfo='skip'
                             ))

    fig.update_yaxes(matches = None, title = percentage, range = [0,50])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2004-06","2031-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"h",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 1,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_resheat_france_de = create_resheat_france(resheat_france_data,
                             "Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch für Wärme",
                             "Tatsächlich erreicht",
                             "2019 - 2023",
                             "Erreicht", "Trends", "Ziele",             
                             "Linear Verlauf",
                             "LEC",
                             "Indikative Meilensteine",
                             "PPE (unten)",
                             "PPE (oben)",
                             "Prozent",
                             "openENTRANCE Szenarien", "Szenariokorridor")

fig_resheat_france_en = create_resheat_france(resheat_france_data,
                             "Share of renewable energy in gross final energy consumption for heat",
                             "Actually reached",
                             "2019 - 2023",
                             "Achieved","Trends","Targets",                   
                             "Linear trend",
                             "LEC",
                             "Indicative milestone",
                             "PPE (low)",
                             "PPE (high)",
                             "Percentage",
                             "openENTRANCE scenarios", "Scenario corridor")

fig_resheat_france_fr = create_resheat_france(resheat_france_data,
                             "Part des EnR dans la consommation finale brute d'énergie pour la chaleur",
                             "Part réalisée",
                             "2019 - 2023",
                             "Réalisé", "Tendances", "Objectifs",
                             "Progression linéaire",
                             "LEC",
                             "Jalons indicatifs",
                             "PPE (bas)",
                             "PPE (haut)", 
                             "Pourcentage",
                             "Scénarios openENTRANCE", "Intervalle de scénarios")

fig_resheat_france_de.write_html("docs/france/figures/resheat.de.html", include_plotlyjs="directory")
fig_resheat_france_en.write_html("docs/france/figures/resheat.en.html", include_plotlyjs="directory")
fig_resheat_france_fr.write_html("docs/france/figures/resheat.fr.html", include_plotlyjs="directory")

#%% Heat pump stock in France (2011-2021)
hpstock_france_data          = pd.read_csv("docs/france/data/hp.csv", sep = ",")
hpstock_france_data['date']  = pd.to_datetime(hpstock_france_data['date'], format='%Y')

hpstock_france_data['X']     = hpstock_france_data.index
res_reg1_data = hpstock_france_data[['date','X','actual_total_stock']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2017') & (res_reg1_data['date'] <= '2021')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual_total_stock'])

predict1_rs = hpstock_france_data[['date','X','actual_total_stock']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2021')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2017_2021'] = predict1_rs.loc[0,'actual_total_stock'] + predict1_rs['index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2017_2021']]

hpstock_france_data = hpstock_france_data.merge(predict1_rs, how='left', on='date')

# Define a function that creates the figure
def create_hpstock_france(data,title,
                          observed_stock_aero, observed_stock_geo, observed_total,
                          achieved_group_name,
                          trend_group_name, trend_2017_2021,
                          unit):
    
    """
    Create heat pump stock figure
    """

    fig = go.Figure()
    
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_aero_hp'],
                        name = observed_stock_aero,
                        marker = dict(color= 'rgba(242, 200, 143, 1)', 
                        line=(dict(width=1, color='rgba(242, 200, 143, 1)'))),
                        hovertemplate = '%{y:.1f}',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_geo_hp'],
                        name = observed_stock_geo,
                        marker = dict(color= 'rgba(185, 86, 63, 1)', 
                        line=(dict(width=1, color='rgba(185, 86, 63, 1)'))),
                        hovertemplate = '%{y:.1f}',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['actual_total_stock'],
                        name = observed_total,
                        mode = 'lines', 
                        marker = dict(color= 'rgba(60, 67, 90, 1)', 
                        line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                        hoverinfo='skip',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['actual_total_stock'],
                            text=data['actual_total_stock'].round(1),
                            mode='text',
                            hoverinfo='skip',
                            textposition='top center',
                            textfont=dict(size=12),
                            showlegend=False))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2017_2021'].round(2),
                        name = trend_2017_2021,
                        mode = 'lines', 
                        line_dash = 'dot',
                        marker = dict(color= 'rgba(152, 152, 161, 1)', 
                        line=(dict(width=2, color='rgba(152, 152, 161, 1)'))),
                        legendgroup = 'trend',
                        legendgrouptitle_text = trend_group_name))
    
    fig.update_yaxes(matches = None, title = unit, range = [0,16])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2010-06","2031-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            barmode = 'stack',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"h",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 1,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))
    return fig

# Create figures
fig_hpstock_france_de = create_hpstock_france(hpstock_france_data,
                             "Bestand an Wärmepumpen nach Technologien",
                             "WP Luft-X", "WP Sole-X", "Gesamt",
                             "<i>Installierte</i>",
                             "<i>Trend</i>", "2017 - 2021",
                             "Million Geräte")
fig_hpstock_france_en = create_hpstock_france(hpstock_france_data,
                             "Stock of heat pumps by technology",
                             "Aerothermal heat pump", "Geothermal heat pump", "Total",
                             "<i>Installed</i>",
                             "<i>Trends</i>", "2017 - 2021",
                             "Million devices")
fig_hpstock_france_fr = create_hpstock_france(hpstock_france_data,
                             "Stock de pompes à chaleur par technologie",
                             "PAC aérothermique", "PAC géothermique", "Total",
                             "<i>Installées</i>",
                             "<i>Tendances</i>", "2017 - 2021",
                             "En millions d'appareils")

# Export to html
fig_hpstock_france_de.write_html("docs/france/figures/hpstock.de.html", include_plotlyjs="directory")
fig_hpstock_france_en.write_html("docs/france/figures/hpstock.en.html", include_plotlyjs="directory")
fig_hpstock_france_fr.write_html("docs/france/figures/hpstock.fr.html", include_plotlyjs="directory")


#%% Heat pumps sales and production of renewable heat from heat pumps in France
hpsales_france_data          = pd.read_csv("docs/france/data/hp.csv", sep = ",")
hpsales_france_data['date']  = pd.to_datetime(hpsales_france_data['date'], format='%Y')

hpsales_france_data['X']     = hpsales_france_data.index

res_reg1_data = hpsales_france_data[['date','X','actual_total_sales']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2018') & (res_reg1_data['date'] <= '2022')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual_total_sales'])

predict1_rs = hpsales_france_data[['date','X','actual_total_sales']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2022')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2018_2022'] = predict1_rs.loc[0,'actual_total_sales'] + predict1_rs['index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2018_2022']]

hpsales_france_data = hpsales_france_data.merge(predict1_rs, how='left', on='date')

# Define a function that creates the figure
def create_hpsales_france(data,title,
                          observed_sales_airair, observed_sales_airwater, observed_sales_geo, observed_sales_total,
                          achieved_group_name,
                          trend_group_name, trend_2018_2022,
                          hpprod_group_name, hp_heat_generation, hp_heat_generation_wa,
                          targets_group_name, target_low, target_high, target_low_ppe3, target_high_ppe3,
                          unit_y, unit_y2):
    
    """
    Create heat pump sales figure
    """

    fig = make_subplots(specs=[[{"secondary_y": True}]])
    
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_sales_airair_hp'],
                        name = observed_sales_airair,
                        marker = dict(color= 'rgba(242, 200, 143, 1)', 
                        line=(dict(width=1, color='rgba(242, 200, 143, 1)'))),
                        hovertemplate = '%{y:.1f}',
                        legend='legend1',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name),
                        secondary_y=False)
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_sales_airwater_hp'],
                        name = observed_sales_airwater,
                        marker = dict(color= 'rgba(230, 156, 107, 1)', 
                        line=(dict(width=1, color='rgba(230, 156, 107, 1)'))),
                        hovertemplate = '%{y:.1f}',
                        legend='legend1',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name),
                        secondary_y=False)
    fig.add_trace(go.Bar(x = data['date'], y = data['actual_sales_geo_hp'],
                        name = observed_sales_geo,
                        marker = dict(color= 'rgba(185, 86, 63, 1)', 
                        line=(dict(width=1, color='rgba(185, 86, 63, 1)'))),
                        hovertemplate = '%{y:.1f}',
                        legend='legend1',
                        legendgroup = 'observed',
                        legendgrouptitle_text = achieved_group_name),
                        secondary_y=False)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['trend_2018_2022']).round(2),
                        name = trend_2018_2022,
                        mode = 'lines', 
                        line_dash = 'dot',
                        marker = dict(color= 'rgba(152, 152, 161, 1)', 
                        line=(dict(width=1, color='rgba(152, 152, 161, 1)'))),
                        legend='legend1',
                        legendgroup = 'trend',
                        legendgrouptitle_text = trend_group_name),
                        secondary_y=False)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['actual_renewable_heat_hp']).round(1),
                        name = hp_heat_generation,
                        mode = 'lines', 
                        #line_dash = 'dot',
                        marker = dict(color= 'rgba(125, 176, 234, 1)', 
                        line=(dict(width=1, color='rgba(125, 176, 234, 1)'))),
                        hovertemplate = '%{y:.4f}',
                        legend='legend2',
                        legendgroup = 'Production de chaleur',
                        legendgrouptitle_text = hpprod_group_name),
                        secondary_y=True)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['actual_renewable_heat_hp_weatheradj']).round(1),
                        name = hp_heat_generation_wa,
                        mode = 'lines', 
                        line_dash = 'dash',
                        marker = dict(color= 'rgba(34, 91, 178, 1)', 
                        line=(dict(width=1, color='rgba(34, 91, 178, 1)'))),
                        hovertemplate = '%{y:.5f}',
                        legend='legend2',
                        legendgroup = 'Production de chaleur',
                        legendgrouptitle_text = hpprod_group_name),
                        secondary_y=True)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['plan_renewable_heat_hp_low']),
                        name = target_low,
                        mode = 'markers',
                        marker_symbol = 'x', 
                        marker = dict(color= 'rgba(30, 90, 70, 1)', 
                        line=(dict(width=1, color='rgba(30, 90, 70, 1)'))),
                        legend='legend2',
                        legendgroup = 'Targets',
                        legendgrouptitle_text = targets_group_name),
                        secondary_y=True)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['plan_renewable_heat_hp_high']),
                        name = target_high,
                        mode = 'markers', 
                        marker_symbol = 'x',
                        marker = dict(color= 'rgba(117, 136, 75, 1)', 
                        line=(dict(width=1, color='rgba(117, 136, 75, 1)'))),
                        legend='legend2',
                        legendgroup = 'Targets',
                        legendgrouptitle_text = targets_group_name),
                        secondary_y=True)
    fig.add_trace(go.Scatter(x = data['date'], y = (data['plan_renewable_heat_hp_low_ppe3']),
                        name = target_low_ppe3,
                        mode = 'markers', 
                        marker_symbol = 'circle-open',
                        marker = dict(color= 'rgba(92, 37, 180, 1)', 
                        line=(dict(width=1, color='rgba(92, 37, 180, 1)'))),
                        legend='legend2',
                        legendgroup = 'Targets',
                        legendgrouptitle_text = targets_group_name),
                        secondary_y=True),
    fig.add_trace(go.Scatter(x = data['date'], y = (data['plan_renewable_heat_hp_high_ppe3']),
                        name = target_high_ppe3,
                        mode = 'markers', 
                        marker_symbol = 'circle-open',
                        marker = dict(color= 'rgba(92, 37, 180, 0.5)', 
                        line=(dict(width=1, color='rgba(92, 37, 180, 0.5)'))),
                        legend='legend2',
                        legendgroup = 'Targets',
                        legendgrouptitle_text = targets_group_name),
                        secondary_y=True)
    
    fig.update_yaxes(matches = None, title = unit_y, range = [0,2100], secondary_y=False)
    fig.update_yaxes(matches = None, range = [0,110],
                     title     = dict(text = unit_y2, font = dict(color="rgba(125, 176, 234, 1)")),
                     tickfont  = dict(color="rgba(125, 176, 234, 1)"),
                     tickcolor = "rgba(125, 176, 234, 1)", 
                     linecolor = "rgba(125, 176, 234, 1)",
                     secondary_y=True)
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2004-06","2036-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            barmode = 'stack',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend1 = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 0,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            legend2 = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.72,
                                    "y"              : 0.6,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 0,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=13,color='#000000'))
    return fig

# Create figure
fig_hpsales_france_de = create_hpsales_france(hpsales_france_data,
                             "Jährlicher Absatz von Wärmepumpen und jährliche erneuerbare Wärmeerzeugung",
                             "Luft-Luft Wärmepumpen", "Luft-Wasser Wärmepumpen", "Erdwärmepumpen", "Gesamt",
                             "<i>Verkauft</i>",
                             "<i>Trends</i>", "2018 - 2022",
                             "<i>Erneuerbare Wärmeerzeugung</i>", "Nicht witterungsbereinigt", "Witterungsbereinigt",
                             "<i>Ziele</i>", "PPE2 (Unteres Ziel)", "PPE2 (Oberes Ziel)", "PPE3 (Unteres Ziel)", "PPE3 (Oberes Ziel)",
                             "Tausend Geräte", "Terawattstunden")
fig_hpsales_france_en = create_hpsales_france(hpsales_france_data,
                             "Annual heat pump sales and annual renewable heat production",
                             "Air-air heat pumps", "Air-water heat pumps", "Geothermal heat pumps", "Total",
                             "<i>Sold</i>",
                             "<i>Trends</i>", "2018 - 2022",
                             "<i>Renewable heat production</i>", "Non weather-adjusted", "Weather-adjusted",
                             "<i>Targets</i>", "PPE2 (low)", "PPE2 (high)", "PPE3 (low)", "PPE3 (high)",
                             "Thousand devices", "TWh")
fig_hpsales_france_fr = create_hpsales_france(hpsales_france_data,
                             "Ventes annuelles de pompes à chaleur et production annuelle de chaleur renouvelable",
                             "PAC air-air", "PAC air-eau", "PAC géothermique", "Total",
                             "<i>Vendues</i>",
                             "<i>Tendances</i>", "2018 - 2022",
                             "<i>Production de chaleur renouvelable</i>", "Non ajustée des var. clim.", "Ajustée des var. clim.",
                             "<i>Objectifs</i>", "PPE2 (bas)", "PPE2 (haut)", "PPE3 (bas)", "PPE3 (haut)",
                             "En milliers d'appareils", "En TWh")

# Export to html
fig_hpsales_france_de.write_html("docs/france/figures/hpsales.de.html", include_plotlyjs="directory")
fig_hpsales_france_en.write_html("docs/france/figures/hpsales.en.html", include_plotlyjs="directory")
fig_hpsales_france_fr.write_html("docs/france/figures/hpsales.fr.html", include_plotlyjs="directory")

#%% Res share in final gross consumption for transport in France 
restransp_france_data          = pd.read_csv("docs/france/data/resshares_transport.csv", sep = ",")
restransp_france_data['date']  = pd.to_datetime(restransp_france_data['date'], format='%Y') #'%d.%m.%Y'
# Create indexm
restransp_france_data['X']     = restransp_france_data.index
# trend 2017-2021
res_reg1_data = restransp_france_data[['date','X','actual']].dropna()
res_reg1_data = res_reg1_data[(res_reg1_data['date'] >= '2019') & (res_reg1_data['date'] <= '2023')]
res_reg1_x    = np.array(res_reg1_data['X']).reshape((-1, 1))
res_reg1      = LinearRegression().fit(res_reg1_x, res_reg1_data['actual'])

predict1_rs = restransp_france_data[['date','X','actual']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2023')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2019_2023'] = predict1_rs.loc[0,'actual'] + predict1_rs['index'] * res_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2019_2023']]
# merge
restransp_france_data = restransp_france_data.merge(predict1_rs, how='left', on='date')
#%% create transport res share figure for France

def create_restransp_france(data,title,observed_share,
                            trend_2018_2022,
                            achieved_group_name, trends_group_name,target_group_name,
                            linear,goal_ltecv,
                            percentage):
    
    """
    Create transport res share figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = observed_share,
                         #mode = "lines",
                         marker = dict(color= 'rgba(60, 67, 90, 0.7)', 
                         line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.1f}%',
                         legendgroup = 'observed',
                         legendgrouptitle_text = achieved_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2019_2023'],
                             name = trend_2018_2022,
                             line = dict(color="rgba(60, 67, 90, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%', visible='legendonly',
                             legendgroup = 'trends',
                             legendgrouptitle_text = trends_group_name))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="rgba(43, 97, 78, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'trends'
                             ))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ltecv'],
                             mode = "markers", name = goal_ltecv,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 1)",
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'target',
                             legendgrouptitle_text= target_group_name))

    fig.update_yaxes(matches = None, title = percentage, range = [0,30])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2004-06","2031-09"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 1,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_restransp_france_de = create_restransp_france(restransp_france_data,
                             "Anteil der erneuerbaren Energien am Endenergieverbrauch von Kraftstoffen",
                             "Tatsächlich erreicht",
                             "2019 - 2023",
                             "Erreicht", "Trends", "Ziele",             
                             "Linear Verlauf",
                             "LTECV",
                             "Prozent")

fig_restransp_france_en = create_restransp_france(restransp_france_data,
                             "Share of renewable energy in final consumption of transport fuels",
                             "Actually reached",
                             "2019 - 2023",
                             "Achieved","Trends","Targets",                   
                             "Linear trend",
                             "LTECV",
                             "Percentage")

fig_restransp_france_fr = create_restransp_france(restransp_france_data,
                             "Part des EnR dans la consommation finale de carburants",
                             "Part réalisée",
                             "2019 - 2023",
                             "Réalisé", "Tendances", "Objectifs",
                             "Progression linéaire",
                             "LTECV",
                             "Pourcentage")

fig_restransp_france_de.write_html("docs/france/figures/restransp.de.html", include_plotlyjs="directory")
fig_restransp_france_en.write_html("docs/france/figures/restransp.en.html", include_plotlyjs="directory")
fig_restransp_france_fr.write_html("docs/france/figures/restransp.fr.html", include_plotlyjs="directory")

#%% BEV and PHEV stock #############################################################################

bev_stock_data           = pd.read_csv("docs/france/data/bev.csv", sep = ",")
bev_stock_data['months'] = bev_stock_data['months'].astype(str)
bev_stock_data['date']   = pd.to_datetime(bev_stock_data['date'], format='%d.%m.%Y')
bev_stock_data['actual_bev'] = bev_stock_data['BEV_stock'] 
bev_stock_data['actual_phev'] = bev_stock_data['PHEV_stock'] 

bev_stock_data['X']          = bev_stock_data.index

#%% dynamic trend last 12 months
#BEV
bev_stock_reg2_data = bev_stock_data[['date','actual_bev','X']].dropna()
bev_stock_reg2_data = bev_stock_reg2_data.tail(12)
bev_stock_reg2_x    = np.array(bev_stock_reg2_data['X']).reshape((-1, 1))
bev_stock_reg2      = LinearRegression().fit(bev_stock_reg2_x, bev_stock_reg2_data['actual_bev'])

predict2_bev_stock = bev_stock_data[['date','X','actual_bev']]
predict2_bev_stock = predict2_bev_stock[(predict2_bev_stock['date'] >= bev_stock_reg2_data.iloc[-1]['date'])].reset_index()
predict2_bev_stock['index'] = predict2_bev_stock.index
predict2_bev_stock['trend_bev_last_12months'] = predict2_bev_stock.loc[0,'actual_bev'] + predict2_bev_stock['index'] * bev_stock_reg2.coef_
predict2_bev_stock = predict2_bev_stock[['date','trend_bev_last_12months']]
#PHEV
phev_stock_reg2_data = bev_stock_data[['date','actual_phev','X']].dropna()
phev_stock_reg2_data = phev_stock_reg2_data.tail(12)
phev_stock_reg2_x    = np.array(phev_stock_reg2_data['X']).reshape((-1, 1))
phev_stock_reg2      = LinearRegression().fit(phev_stock_reg2_x, phev_stock_reg2_data['actual_phev'])

predict2_phev_stock = bev_stock_data[['date','X','actual_phev']]
predict2_phev_stock = predict2_phev_stock[(predict2_phev_stock['date'] >= phev_stock_reg2_data.iloc[-1]['date'])].reset_index()
predict2_phev_stock['index'] = predict2_phev_stock.index
predict2_phev_stock['trend_phev_last_12months'] = predict2_phev_stock.loc[0,'actual_phev'] + predict2_phev_stock['index'] * phev_stock_reg2.coef_
predict2_phev_stock = predict2_phev_stock[['date','trend_phev_last_12months']]

#%% merge
bev_stock_data = bev_stock_data.merge(predict2_bev_stock, how='left', on='date')
bev_stock_data = bev_stock_data.merge(predict2_phev_stock, how='left', on='date')


# %% create bev_adm figure

def create_bev_stock(data,title,
              group_title_achieved,
              bev_stock_bev,bev_stock_phev,
              group_title_goals, goal_bev, goal_phev,
              linear_bev, linear_phev,
              group_title_trends, trend_bev_12_months, trend_phev_12_months,
              y_axis):

    """
    Create bev_adm figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['BEV_stock'],
                         name = bev_stock_bev,
                         marker_color = 'rgba(60, 67, 90, 1)', hovertemplate = '%{y:.3s}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['PHEV_stock'],
                         name = bev_stock_phev,
                         marker_color = 'rgba(0, 148, 164, 1)', hovertemplate = '%{y:.3s}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_bev'],
                             name = goal_bev,
                             mode = "markers",
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(172, 69, 47, 1)",
                             hoverinfo='none',
                             #hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_phev'],
                             name = goal_phev,
                             mode = "markers",
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(195, 113, 48, 1)",
                             hoverinfo='none',
                             #hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_bev_linear'],
                             name = linear_bev,
                             line = dict(color="rgba(172, 69, 47, 1)", dash='dot'), 
                             hovertemplate = '%{y:.3s}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_phev_linear'],
                             name = linear_phev,
                             line = dict(color="rgba(195, 113, 48, 1)", dash='dot'), 
                             hovertemplate = '%{y:.3s}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_bev_last_12months'],
                            name = trend_bev_12_months, line = dict(color="rgba(152, 152, 161, 1)", dash='dot'),
                            #hovertemplate = '%{y:.3s}',
                            hoverinfo='none',
                            legendgroup="trends",
                            legendgrouptitle_text=group_title_trends))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_phev_last_12months'],
                            name = trend_phev_12_months, line = dict(color="rgba(152, 152, 161, 1)", dash='dot'),
                            #hovertemplate = '%{y:.3s}',
                            hoverinfo='none',
                            legendgroup="trends",
                            legendgrouptitle_text=group_title_trends))


    fig.update_yaxes(matches = None, title = y_axis, range = [0,3200000])
    fig.update_xaxes(title=None, range = ["2021-05", "2030-07"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', 
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", x=0.01, y=1,
                                       orientation = "v",borderwidth=1),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         #barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x"
                         )
    return fig

#def create_bev_adm(data,title,legislative_period,
#              group_title_achieved,bev_adm_normal,bev_adm_fast,
#              group_title_trends,trend_2017_2021,trend_12_months,
#              button_legislative_period,button_til_2030,
#              y_axis):

fig_bev_stock_de = create_bev_stock(bev_stock_data,"Bestand wiederaufladebare Fahrzeuge",
                                "Tatsächlich erreicht",
                                "Batterieelektrische Fahrzeuge", "Plug-in-Hybrid Fahrzeuge",
                                "Regierungsziele", "Batterieelektrische", "Plug-in-Hybrid",
                                "Linear Verlauf Batterieelektrische", "Linear Verlauf Plug-in-Hybrid",
                                "12-Monats-Trends", "Batterieelektrische", "Plug-in-Hybrid",
                                "Anzahl")

fig_bev_stock_en = create_bev_stock(bev_stock_data,"Stock of rechargeable light vehicles",
                                "Actually achieved",
                                "Battery-electric vehicles", "Plug-in hybrid vehicles",
                                "Government goals", "BEV", "PHEV",
                                "Linear progression for BEV", "Linear progression for PHEV",
                                "Trends over the last 12 months", "BEV", "PHEV",
                                "Units")

fig_bev_stock_fr = create_bev_stock(bev_stock_data,"Stock des véhicules rechargeables",
                                "Réalisé",
                                "Véhicules 100% électriques", "Véhicules hybrides rechargeables",
                                "Objectifs du gouvernement", "100% électrique", "Hybride rechargeable",
                                "Progression linéaire 100% électrique", "Progression linéaire hybride rechargeable",
                                "Tendance sur les 12 derniers mois", "100% électrique", "Hybride rechargeable",
                                "Unités")

fig_bev_stock_de.write_html("docs/france/figures/bev_stock.de.html", include_plotlyjs="directory")
fig_bev_stock_en.write_html("docs/france/figures/bev_stock.en.html", include_plotlyjs="directory")
fig_bev_stock_fr.write_html("docs/france/figures/bev_stock.fr.html", include_plotlyjs="directory")

#%% BEV new admissions #############################################################################

bev_adm_data           = pd.read_csv("docs/france/data/bev.csv", sep = ",")
bev_adm_data['months'] = bev_adm_data['months'].astype(str)
bev_adm_data['date']   = pd.to_datetime(bev_adm_data['date'], format='%d.%m.%Y')
bev_adm_data['actual'] = bev_adm_data['total_ms'] 

bev_adm_data['X']          = bev_adm_data.index

#%% dynamic trend last 12 months
bev_adm_reg2_data = bev_adm_data[['date','actual','X']].dropna()
bev_adm_reg2_data = bev_adm_reg2_data.tail(12)
bev_adm_reg2_x    = np.array(bev_adm_reg2_data['X']).reshape((-1, 1))
bev_adm_reg2      = LinearRegression().fit(bev_adm_reg2_x, bev_adm_reg2_data['actual'])

predict2_bev_adm = bev_adm_data[['date','X','actual']]
predict2_bev_adm = predict2_bev_adm[(predict2_bev_adm['date'] >= bev_adm_reg2_data.iloc[-1]['date'])].reset_index()
predict2_bev_adm['index'] = predict2_bev_adm.index
predict2_bev_adm['trend_last_12months'] = predict2_bev_adm.loc[0,'actual'] + predict2_bev_adm['index'] * bev_adm_reg2.coef_
predict2_bev_adm = predict2_bev_adm[['date','trend_last_12months']]

#%% merge
bev_adm_data = bev_adm_data.merge(predict2_bev_adm, how='left', on='date')

# %% create bev_adm figure

def create_bev_adm(data,title,
              group_title_achieved,
              bev_adm_bevp,bev_adm_bevld,bev_adm_phev,
#              group_title_trends,trend_2018_2021,trend_12_months,
              y_axis):

    """
    Create bev_adm figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['BEVp_ms'],
                         name = bev_adm_bevp,
                         marker_color = 'rgba(60, 67, 90, 1)', hovertemplate = '%{y:.r}%',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['PHEVp_ms'],
                         name = bev_adm_phev,
                         marker_color = 'rgba(0, 148, 164, 1)', hovertemplate = '%{y:.r}%',
                         legendgroup = 'actual'))


    fig.update_yaxes(matches = None, title = y_axis, range = [0,70])
    fig.update_xaxes(title=None, range = ["2021-05", "2025-07"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', 
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", x=0.01, y=1,
                                       orientation = "v",borderwidth=1),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         barmode='stack',
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x"
                         )
    return fig

#def create_bev_adm(data,title,legislative_period,
#              group_title_achieved,bev_adm_normal,bev_adm_fast,
#              group_title_trends,trend_2017_2021,trend_12_months,
#              button_legislative_period,button_til_2030,
#              y_axis):

fig_bev_adm_de = create_bev_adm(bev_adm_data,"Anteile an den monatlichen Neuzulassungen",
                                "Pkw-Typ",
                                "Batterieelektrische", "Batterieelektrische Nutzfahrzeuge", "Plug-in-Hybrid",
                                "Prozent")

fig_bev_adm_en = create_bev_adm(bev_adm_data,"Shares in monthly new registrations",
                                "Type of passenger car",
                                "Battery-electric", "Battery-electric light commercial vehicles", "Plug-in hybrid",
                                "Percent")

fig_bev_adm_fr = create_bev_adm(bev_adm_data,"Parts des nouvelles immatriculations mensuelles",
                                "Type de voiture particulière",
                                "100% électrique", "Voitures utilitaires électriques", "Hybride rechargeable",
                                "Pourcentage")

fig_bev_adm_de.write_html("docs/france/figures/bev_adm.de.html", include_plotlyjs="directory")
fig_bev_adm_en.write_html("docs/france/figures/bev_adm.en.html", include_plotlyjs="directory")
fig_bev_adm_fr.write_html("docs/france/figures/bev_adm.fr.html", include_plotlyjs="directory")

#%% Charging stations in France ##############################################################################

cs_data           = pd.read_csv("docs/france/data/cs.csv", sep = ",")
cs_data['months'] = cs_data['months'].astype(str)
cs_data['date']   = pd.to_datetime(cs_data['date'], format='%d.%m.%Y')
cs_data['actual'] = cs_data['actual'] 

cs_data['X']          = cs_data.index

#%% dynamic trend last 12 months
cs_reg2_data = cs_data[['date','actual','X']].dropna()
cs_reg2_data = cs_reg2_data.tail(12)
cs_reg2_x    = np.array(cs_reg2_data['X']).reshape((-1, 1))
cs_reg2      = LinearRegression().fit(cs_reg2_x, cs_reg2_data['actual'])

predict2_cs = cs_data[['date','X','actual']]
predict2_cs = predict2_cs[(predict2_cs['date'] >= cs_reg2_data.iloc[-1]['date'])].reset_index()
predict2_cs['index'] = predict2_cs.index
predict2_cs['trend_last_12months'] = predict2_cs.loc[0,'actual'] + predict2_cs['index'] * cs_reg2.coef_
predict2_cs = predict2_cs[['date','trend_last_12months']]

#%% merge
cs_data = cs_data.merge(predict2_cs, how='left', on='date')

# %% create cs figure

def create_cs(data,title,
              goal,linear, linear2,
              group_title_achieved,cs,
              group_title_goals, group_title_trends, trend_12_months,
              y_axis):

    """
    Create CS figure
    """

    fig = go.Figure()


    fig.add_trace(go.Bar(x = data['date'], y = data['actual']/1000,
                         name = cs,
                         marker_color = 'rgba(60, 67, 90, 1)', hovertemplate = '%{y:.r}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan']/1000,
                             name = goal,
                             mode = "markers",
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(172, 69, 47, 1)",
                             hoverinfo= 'none',
                             #hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan']/1000,
                             name = linear,
                             line = dict(color="rgba(172, 69, 47, 1)", dash='dot'), hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['linear_plan2']/1000,
                             name = linear2,
                             line = dict(color="rgba(172, 69, 47, 1)", dash='dot'), hovertemplate = '%{y:.2r}',
                             legendgroup = 'goals',
                             legendgrouptitle_text = group_title_goals))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_last_12months']/1000,
                            name = trend_12_months, line = dict(color="rgba(152, 152, 161, 1)", dash='dot'),
                            hoverinfo = 'none',
                            #hovertemplate = '%{y:.3s}',
                            legendgroup="trends",
                            legendgrouptitle_text=group_title_trends))

    fig.update_yaxes(matches = None, title = y_axis, range = [0,425])
    fig.update_xaxes(title=None, range = ["2017-12", "2031-07"])
    #fig.update_traces(width=1, selector=dict(type='bar'))
    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', 
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", x=0.01, y=1,
                                       orientation = "v",borderwidth=1),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         xaxis_hoverformat='%b %Y',
                         hovermode = "x"
                         )

    return fig

fig_cs_de = create_cs(cs_data,"Ladepunkte in Betrieb",
                      "Ziele der Regierung","Linearer Verlauf bis 2023", "Linearer Verlauf bis 2030",
                      "Tatsächlich erreicht","Ladepunkte",
                      "Regierungsziele", "Trends","Letzte 12 Monate",
                      "Tausend Ladepunkte")

fig_cs_en = create_cs(cs_data,"Charging points in operation",
                      "Target of the government","Linear progression until 2023", "Linear progression until 2030",
                      "Actually achieved", "Charging points",
                      "Government goals", "Trends","Last 12 months",
                      "Thousand charging points")
fig_cs_fr = create_cs(cs_data,"Points de charge en opération",
                      "Objectif du gouvernement","Progression linéaire jusqu'en 2023", "Progression linéaire jusqu'en 2030",
                      "Réalisé","Points de charge",
                      "Objectifs","Tendances","12 derniers mois",
                      "Em milliers de points de charge")

fig_cs_de.write_html("docs/france/figures/cs.de.html", include_plotlyjs="directory")
fig_cs_en.write_html("docs/france/figures/cs.en.html", include_plotlyjs="directory")
fig_cs_fr.write_html("docs/france/figures/cs.fr.html", include_plotlyjs="directory")

#%% BEV per charging point #########################################################################

# Define bev_per_cp_data by using bev_data
bev_per_cp_data = pd.read_csv("docs/france/data/bev.csv", sep = ",")

# Define variables of interest
bev_per_cp_data['BEV_cs']    = bev_per_cp_data['BEV_stock'] / bev_per_cp_data['cs_total']
bev_per_cp_data['PHEV_cs']   = bev_per_cp_data['PHEV_stock'] / bev_per_cp_data['cs_total']
bev_per_cp_data['total_cs']  = bev_per_cp_data['total_stock'] / bev_per_cp_data['cs_total']
bev_per_cp_data['date']      = pd.to_datetime(bev_per_cp_data['date'], format='%d.%m.%Y') #'%d.%m.%Y'


bev_per_cp_data['X']      = bev_per_cp_data.index

# %% create bev_per_cp figure

def create_bev_per_cp(data,title,
              group_title_achieved,bev_per_cs,phev_per_cs,total_per_cs,
              y_axis):

    """
    Create bev_per_cp figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['BEV_cs'],
                         name = bev_per_cs,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="rgba(60, 67, 90, 1)", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}',
                         legendgrouptitle_text = group_title_achieved))

    fig.add_trace(go.Scatter(x = data['date'], y = data['PHEV_cs'],
                         name = phev_per_cs,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="rgba(0, 148, 164, 1)", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'))
                        
    fig.add_trace(go.Scatter(x = data['date'], y = data['total_cs'],
                         name = total_per_cs,
                         mode = 'lines',
                         line_shape="spline",
                         line = dict(color="black", dash = "solid", width=3),
                         legendgroup = "actual",
                         hovertemplate = '%{y:.2f}'))

    fig.update_yaxes(matches = None, title = y_axis, range = [0,25])
    fig.update_xaxes(title=None, range = ["31.05.2021", "31.05.2025"])
    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', 
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(yanchor="top", xanchor="left", x=0.01, y=1,
                                       orientation = "v",borderwidth=1),
                         legend_groupclick = "toggleitem",
                         margin=dict(l=0, r=0, t=40, b=0, pad=0),
                         xaxis_hoverformat='%Y',
                         hovermode = "x"
                         )

    return fig

fig_bev_per_cp_de = create_bev_per_cp(bev_per_cp_data,
                                      "Wiederaufladbare Fahrzeuge pro öffentlichem Ladepunkt",
                                      "Bisher erreicht",
                                      "BEV pro Ladepunkt",
                                      "PHEV pro Ladepunkt",
                                      "BEV+PHEV pro Ladepunkt",
                                      "")

fig_bev_per_cp_en = create_bev_per_cp(bev_per_cp_data,
                                      "Plug-in cars per public charging point",
                                      "Actually achieved",
                                      "BEV per charging point",
                                      "PHEV per charging point",
                                      "BEV+PHEV per charging point",
                                      "")

fig_bev_per_cp_fr = create_bev_per_cp(bev_per_cp_data,
                                      "Voitures rechargeables par borne de recharge publique",
                                      "Réalisé",
                                      "Voitures 100% électriques par borne de recharge",
                                      "Hybriques rechargeables par borne de recharge",
                                      "Voitures rechargeables par borne de recharge",
                                      "")

fig_bev_per_cp_de.write_html("docs/france/figures/bev_per_cp.de.html", include_plotlyjs="directory")
fig_bev_per_cp_en.write_html("docs/france/figures/bev_per_cp.en.html", include_plotlyjs="directory")
fig_bev_per_cp_fr.write_html("docs/france/figures/bev_per_cp.fr.html", include_plotlyjs="directory")


#%% Nuclear share in power production France #######################################################

nuclear_france_data          = pd.read_csv("docs/france/data/nuclear_share.csv", sep = ",")
nuclear_france_data['date']  = pd.to_datetime(nuclear_france_data['date'], format='%Y') #'%d.%m.%Y'

#%% create nuclear share figure for France

def create_nuclear_share(data,title,observed_share,
                         linear,goal,percentage,
                         y_max_2030, y_max_2050, til_2030, til_2050,
                         scenarios_group_name, opentrance_span):
    """
    Create nuclear share figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['share_nuclear'],
                         name = observed_share,
                         marker = dict(color= 'rgba(60, 67, 90, 0.7)', 
                                       line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.2f}%',
                         legendgroup = 'observed'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['target'],
                             mode = "markers", name = goal,
                             marker_size=12, marker_symbol = "x-dot", marker_color = "rgba(43, 97, 78, 1)",
                             hovertemplate = '%{y:.0f}%',
                             legendgroup = 'target'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear,
                             mode = "lines",
                             line = dict(color="rgba(43, 97, 78, 0.8)", dash='dot'),
                             hovertemplate = '%{y:.1f}%',
                             legendgroup = 'plan'
                             ))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['opentrance_max'], data['opentrance_min'][::-1]]),
                             name = opentrance_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="opentrance",
                             legendgrouptitle_text=scenarios_group_name,
                             hoverinfo='skip'
                             ))
    
    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2005", "2031"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2005", "2050"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_yaxes(matches = None, title = percentage, range = [0,100])
    fig.update_xaxes(title=None, tickmode = 'array', range = ["2011-06","2036-09"],
                     tickvals = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040],
                     ticktext = ['2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040'])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"h",
                                    "x"              : 0.01,
                                    "y"              : 1,
                                    "yanchor"        : "top",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 1,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))

    return fig

fig_nuclear_france_de = create_nuclear_share(nuclear_france_data, 
                             "Anteil der Atomkraft an der Stromerzeugung",
                             "Erfasster Anteil",              
                             "Lineare Reduzierung",
                             "Ziel der LEC (entfernt)",
                             "Prozent",
                             100, 100, 
                             "Bis 2030", "Bis 2050", 
                             "openENTRANCE Szenarien", "Szenariokorridor")

fig_nuclear_france_en = create_nuclear_share(nuclear_france_data,
                             "Share of nuclear energy in electricity generation",
                             "Observed share",                    
                             "Linear decrease",
                             "Target of the LEC (removed)",
                             "Percentage",
                             100, 100, 
                             "Until 2030", "Until 2050",
                             "openENTRANCE scenarios", "Scenario corridor")

fig_nuclear_france_fr = create_nuclear_share(nuclear_france_data,
                             "Part de l'énergie nucléaire dans la production d'électricité",
                             "Part du nucléaire observée",
                             "Réduction linéaire",
                             "Objectif de la LEC (supprimé)",
                             "Pourcentage",
                             100, 100, 
                             "Jusqu'en 2030", "Jusqu'en 2050",
                             "Scénarios openENTRANCE", "Intervalle de scénarios")

fig_nuclear_france_de.write_html("docs/france/figures/nuclear.de.html", include_plotlyjs="directory")
fig_nuclear_france_en.write_html("docs/france/figures/nuclear.en.html", include_plotlyjs="directory")
fig_nuclear_france_fr.write_html("docs/france/figures/nuclear.fr.html", include_plotlyjs="directory")
#%% Greenhouse gas emissions in France

ghs_france_data               = pd.read_csv("docs/france/data/emissions.csv", sep = ",")
ghs_france_data['date']       = pd.to_datetime(ghs_france_data['date'], format='%Y') #'%d.%m.%Y'

# Define a function: create the figure
def create_ghs_france(data, title, 
                      group_actual, actual_total, actual_energy, actual_industry, actual_waste, actual_buildings, 
                      actual_agriculture, actual_transport, actual_lulucf,
                      group_plan, plan_snbc1, plan_snbc2, 
                      group_plan_future, plan_snbc3, plan_ndc2020,
                      group_annual_plan, plan_snbc2_annual, 
                      group_plan_sectors, plan_snbc2_energy, plan_snbc2_industry, plan_snbc2_waste, plan_snbc2_buildings, plan_snbc2_agriculture, plan_snbc2_transport, plan_snbc2_lulucf,
                      unit):
    fig = go.Figure()

    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_energy'], 
                         name = actual_energy,
                         marker = dict(color= 'rgba(153, 191, 243, 0.7)', 
                                       line=(dict(width=1, color='rgba(153, 191, 243, 1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_industry'], 
                         name = actual_industry,
                         marker = dict(color= 'rgba(44, 81, 191,0.7)', 
                                       line=(dict(width=1, color='rgba(44, 81, 191,1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_waste'], 
                         name = actual_waste,
                         marker = dict(color= 'rgba(161, 180, 204,0.7)', 
                                       line=(dict(width=1, color='rgba(161, 180, 204,1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_buildings'], 
                         name = actual_buildings,
                         marker = dict(color= 'rgba(243, 218, 29, 0.7)', 
                                       line=(dict(width=1, color='rgba(243, 218, 29, 1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_agriculture'], 
                         name = actual_agriculture,
                         marker = dict(color= 'rgba(160, 217, 188,0.7)', 
                                       line=(dict(width=1, color='rgba(160, 217, 188,1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_transport'], 
                         name = actual_transport,
                         marker = dict(color= 'rgba(235, 126, 30, 0.7)', 
                                       line=(dict(width=1, color='rgba(235, 126, 30, 1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Bar(x=data[data['date']<'2024']['date'], y=data[data['date']<'2024']['actual_lulucf'], 
                         name = actual_lulucf,
                         marker = dict(color= 'rgba(206, 217, 152,0.7)', 
                                       line=(dict(width=1, color='rgba(206, 217, 152,1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['actual_total'], 
                         name = actual_total,
                         marker = dict(color= 'rgba(60, 67, 90, 1)', 
                                       line=(dict(width=1, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.2f}MtCO2e',
                         legend = 'legend1',
                         legendgroup = 'actual',
                         legendgrouptitle_text=group_actual
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['plan_snbc1'], 
                         name = plan_snbc1,
                         mode = 'lines',
                         line_dash="dash",
                         marker = dict(color= 'rgba(172, 6, 47, 1)', 
                                       line=(dict(width=1, color='rgba(172, 6, 47, 1)'))),
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan',
                         legendgrouptitle_text=group_plan
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['plan_snbc2'], 
                         name = plan_snbc2,
                         marker = dict(color= 'rgba(172, 6, 47, 1)', 
                                       line=(dict(width=1, color='rgba(172, 6, 47, 1)'))),
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan',
                         legendgrouptitle_text=group_plan
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['plan_snbc3'], 
                         name = plan_snbc3,
                         mode = 'lines',
                         line_dash="dash",
                         marker = dict(color= 'rgba(243, 93, 0, 1)', 
                                       line=(dict(width=1, color='rgba(243, 93, 0, 1)'))),
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan',
                         legendgrouptitle_text=group_plan
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['plan_ndc2020'], 
                         name = plan_ndc2020,
                         mode="markers",
                         marker_symbol='diamond',
                         marker = dict(color= 'rgba(121, 176, 72,0.8)', 
                                       line=(dict(width=1, color='rgba(121, 176, 72,1)'))),
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan_future',
                         legendgrouptitle_text=group_plan_future
                         ))
    fig.add_trace(go.Scatter(x=data['date'], y=data['plan_snbc2_annual'], 
                         name = plan_snbc2_annual,
                         mode="lines",
                         line_dash = 'dot',
                         marker = dict(color= 'rgba(172, 6, 47, 1)', 
                                       line=(dict(width=1, color='rgba(172, 6, 47, 1)'))),
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan_annual',
                         legendgrouptitle_text=group_annual_plan
                         ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_energy'], 
    #                     name = plan_snbc2_energy,
    #                     marker_color='rgba(153, 191, 243, 0.4)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_industry'], 
    #                     name = plan_snbc2_industry,
    #                     marker_color='rgba(44, 81, 191,0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_waste'], 
    #                     name = plan_snbc2_waste,
    #                     marker_color='rgba(161, 180, 204,0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_buildings'], 
    #                     name = plan_snbc2_buildings,
    #                     marker_color='rgba(243, 218, 29, 0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_agriculture'], 
    #                     name = plan_snbc2_agriculture,
    #                     marker_color='rgba(160, 217, 188,0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_transport'], 
    #                     name = plan_snbc2_transport,
    #                     marker_color= 'rgba(235, 126, 30, 0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    #fig.add_trace(go.Bar(x=data[data['date']=='2023']['date'], y=data[data['date']=='2023']['plan_snbc2_lulucf'], 
    #                     name = plan_snbc2_lulucf,
    #                     marker_color= 'rgba(206, 217, 152,0.7)',
    #                     marker_pattern=dict(shape='/'),
    #                     hovertemplate = '%{y:.0f}MtCO2e',
    #                     showlegend=False,
    #                     legend = 'legend2',
    #                     legendgroup = 'plan_sectors',
    #                     legendgrouptitle_text=group_plan_sectors
    #                     ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_energy'], 
                         name = plan_snbc2_energy,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(153, 191, 243, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_industry'], 
                         name = plan_snbc2_industry,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(44, 81, 191,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_waste'], 
                         name = plan_snbc2_waste,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(161, 180, 204,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_buildings'], 
                         name = plan_snbc2_buildings,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(243, 218, 29, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend3',
                         legendgroup = 'plan_sectors',
                         #legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_agriculture'], 
                         name = plan_snbc2_agriculture,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(160, 217, 188,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend3',
                         legendgroup = 'plan_sectors',
                         #legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_transport'], 
                         name = plan_snbc2_transport,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color= 'rgba(235, 126, 30, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend3',
                         legendgroup = 'plan_sectors',
                         #legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2023') & (data['date']<'2029')]['date'], y=data[(data['date']>'2023') & (data['date']<'2029')]['plan_snbc2_lulucf'], 
                         name = plan_snbc2_lulucf,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color= 'rgba(206, 217, 152,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc2_neg',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend3',
                         legendgroup = 'plan_sectors',
                         #legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_energy'], 
                         name = plan_snbc2_energy,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(153, 191, 243, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_industry'], 
                         name = plan_snbc2_industry,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(44, 81, 191,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_waste'], 
                         name = plan_snbc2_waste,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(161, 180, 204,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_buildings'], 
                         name = plan_snbc2_buildings,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(243, 218, 29, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_agriculture'], 
                         name = plan_snbc2_agriculture,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color='rgba(160, 217, 188,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_transport'], 
                         name = plan_snbc2_transport,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color= 'rgba(235, 126, 30, 0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))
    fig.add_trace(go.Scatter(x=data[(data['date']>'2028') & (data['date']<'2034')]['date'], y=data[(data['date']>'2028') & (data['date']<'2034')]['plan_snbc2_lulucf'], 
                         name = plan_snbc2_lulucf,
                         mode="lines",
                         line_dash = 'dot',
                         marker_color= 'rgba(206, 217, 152,0.7)',
                         fillpattern= dict(shape='/'),
                         stackgroup='bc3_neg',
                         hovertemplate = '%{y:.0f}MtCO2e',
                         legend = 'legend2',
                         showlegend=False,
                         legendgroup = 'plan_sectors',
                         legendgrouptitle_text=group_plan_sectors
                         ))

    fig.update_yaxes(matches = None, title = unit, range = [-50,600])
    fig.add_hline(y=0, line_width=1, line_color='rgba(0, 0, 0,1)')
    fig.update_xaxes(title=None, tickmode = 'array', linecolor='rgb(255, 255, 255)', tickcolor='rgb(255, 255, 255)', range = ["1989-06","2051-06"])
    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                            barmode = 'relative',
                            legend_title_text = '',
                            legend_groupclick = "toggleitem",
                            legend1 = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"v",
                                    "x"              : 0.85,
                                    "y"              : 0.3,
                                    "yanchor"        : "bottom",
                                    "xanchor"        : "left",
                                    "borderwidth"    : 0.8,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            legend2 = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"h",
                                    "x"              : 0.05,
                                    "y"              : -1,
                                    "yanchor"        : "bottom",
                                    "xanchor"        : "left",
                                    "xref"           : 'container',
                                    "yref"           : 'container',
                                    "borderwidth"    : 0,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},
                            legend3 = {"title" : {"text":None},
                                    "traceorder"     :"grouped",
                                    "orientation"    :"h",
                                    "x"              : 0.65,
                                    "y"              : -1,
                                    "yanchor"        : "bottom",
                                    "xanchor"        : "left",
                                    "xref"           : 'container',
                                    "yref"           : 'container',
                                    "borderwidth"    : 0,
                                    "font"           : {"size": 11},
                                    "grouptitlefont" : {"size": 11},
                                    "groupclick"     : "toggleitem"},                            margin=dict(l=0, r=0, t=130, b=0, pad=0),
                            xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                            font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))
    return fig
    
# Generate figure
fig_ghs_france_de = create_ghs_france(ghs_france_data, 
                                      "Treibhausgasemissionen", 
                                       "<i>Historische Emissionen</i>", "Gesamt (ohne LULUCF)", "Energiewirthschaft", "Industrie", "Abfall", "Gebäude", "Landwirtschaft", "Verkehr", "LULUCF",
                                       "<i>CO2-Budgets</i>", "SNBC-1", "SNBC-2", 
                                       "<i>Ziele</i>", "SNBC-3", "NDC2020", 
                                       "<i>Jährliche CO2-Budgets</i>", "Alle Sektoren (ohne LULUCF)", 
                                       "<i>CO2-Budgets nach Sektoren</i>", "Energiewirthschaft", "Industrie", "Abfall", "Gebäude", "Landwirtschaft", "Verkehr", "LULUCF",
                                       "Mio t CO2-äquivalent")
fig_ghs_france_en = create_ghs_france(ghs_france_data, 
                                      "Greenhouse gas emissions", 
                                       "<i>Historical emissions</i>", "Total", "Energy", "Industry", "Waste", "Buildings", "Agriculture", "Transport", "LULUCF",
                                       "<i>Carbon budgets</i>", "SNBC-1", "SNBC-2", 
                                       "<i>Future targets</i>", "SNBC-3", "NDC2020", 
                                       "<i>Annual carbon budgets</i>", "All sectors (excl. LULUCF)", 
                                       "<i>Sectoral carbon budgets</i>", "Energy", "Industry", "Waste", "Buildings", "Agriculture", "Transport", "LULUCF",
                                       "Mio t CO2eq")
fig_ghs_france_fr = create_ghs_france(ghs_france_data, 
                                      "Émissions de gaz à effet de serre", 
                                       "<i>Émissions historiques</i>", "Total (hors UTCATF)", "Énergie", "Industrie", "Déchets", "Bâtiments", "Agriculture", "Transport", "UTCATF",
                                       "<i>Budgets carbone</i>", "SNBC-1", "SNBC-2", 
                                       "<i>Cibles futures</i>", "SNBC-3", "NDC2020", 
                                       "<i>Budgets carbone annuels</i>", "Tous secteurs (hors UTCATF)", 
                                       "<i>Budgets carbone par secteur</i>", "Énergie", "Industrie", "Déchets", "Bâtiments", "Agriculture", "Transport", "UTCATF",
                                       "MtCO2-équivalent")
# Export to html
fig_ghs_france_de.write_html("docs/france/figures/ghs_france.de.html", include_plotlyjs="directory")
fig_ghs_france_en.write_html("docs/france/figures/ghs_france.en.html", include_plotlyjs="directory")
fig_ghs_france_fr.write_html("docs/france/figures/ghs_france.fr.html", include_plotlyjs="directory")
# %% Fossil energy primary consumption for France ##################################################

pec_france_data         = pd.read_csv("docs/france/data/pec.csv", sep = ",")
pec_france_data['date'] = pd.to_datetime(pec_france_data['date'], format='%Y')

# Create indexm
pec_france_data['X']     = pec_france_data.index

#%% trend 2019-2023
pec_reg1_data = pec_france_data[['date','X','actual_total']].dropna()
pec_reg1_data = pec_reg1_data[(pec_reg1_data['date'] >= '2019') & (pec_reg1_data['date'] <= '2023')]
pec_reg1_x    = np.array(pec_reg1_data['X']).reshape((-1, 1))
pec_reg1      = LinearRegression().fit(pec_reg1_x, pec_reg1_data['actual_total'])

predict1_rs = pec_france_data[['date','X','actual_total']]
predict1_rs = predict1_rs[(predict1_rs['date'] >= '2023')].reset_index()
predict1_rs['index'] = predict1_rs.index
predict1_rs['trend_2019_2023'] = predict1_rs.loc[0,'actual_total'] + predict1_rs['index'] * pec_reg1.coef_
predict1_rs = predict1_rs[['date','trend_2019_2023']]

#%% merge
pec_france_data = pec_france_data.merge(predict1_rs, how='left', on='date')

#%% create fossil energy primary consumption figure for France

def create_pec_france_figure(data,title,
                             legend_group_achieved, oil, coal, natural_gas, total,
                             legend_group_plan, plan_lec,linear_reduction,
                             legend_group_plan_ppe, plan_ppe_oil, plan_ppe_coal, plan_ppe_gas,
                             legend_group_trends, trend_five_years,
                             y_axis_title,
                             y_max_2030, y_max_2050, til_2030, til_2050,
                             scenarios_group_name, opentrance_span):

    """
    Create primary energy consumption figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['oil'],
                             name = oil,
                             marker_color = "rgba(26, 55, 77, .80)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             legendgrouptitle_text=legend_group_achieved,
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['natural_gas'],
                             name = natural_gas,
                             marker_color = "rgba(205, 182, 153, .50)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['coal'],
                             name = coal,
                             marker_color = "rgba(26, 55, 77, .60)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Scatter(x=data['date'], y=data['actual_total'], 
                         name = total,
                         marker = dict(color= 'rgba(60, 67, 90, 1)', 
                                       line=(dict(width=1, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.2f} TWh',
                         legendgroup = 'fossils'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_lec'],
                             name = plan_lec,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "rgba(143, 47, 40, 1)",
                             hovertemplate = '%{y:.2f} TWh',
                             legendgroup = 'plan',
                             legendgrouptitle_text=legend_group_plan))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear_reduction,
                             line = dict(color="rgba(143, 47, 40, 1)", dash='dot', width = 1),
                             legendgroup = 'plan',
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_oil'],
                             name = plan_ppe_oil,
                             marker_color = "rgba(26, 55, 77, .50)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             legendgrouptitle_text=legend_group_plan_ppe,
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_gas'],
                             name = plan_ppe_gas,
                             marker_color = "rgba(205, 182, 153, .20)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_coal'],
                             name = plan_ppe_coal,
                             marker_color = "rgba(26, 55, 77, .30)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_2019_2023'],
                             name = trend_five_years,
                             line = dict(color="rgba(152, 152, 161, 1)", dash='dot', width = 1),
                             legendgroup = 'trends',
                             legendgrouptitle_text=legend_group_trends,
                             hovertemplate = '%{y:.0f} TWh'))

    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['opentrance_max'], data['opentrance_min'][::-1]]),
                             name = opentrance_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="opentrance",
                             legendgrouptitle_text=scenarios_group_name,
                             hoverinfo='skip'
                             ))
    
    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2005", "2031"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2005", "2050"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )


    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,2000])
    fig.update_xaxes(matches = None,                       range = ['1989-06','2032'])

    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                                barmode = 'stack',
                                legend_title_text = '',
                                legend_groupclick = "toggleitem",
                                legend = {"title" : {"text":None},
                                        "traceorder"     :"grouped",
                                        "orientation"    :"h",
                                        "x"              : 0,
                                        "y"              : -0.2,
                                        "yanchor"        : "top",
                                        "xanchor"        : "left",
                                        "borderwidth"    : 0.8,
                                        "font"           : {"size": 11},
                                        "grouptitlefont" : {"size": 11},
                                        "groupclick"     : "toggleitem"},
                                margin=dict(l=0, r=0, t=130, b=0, pad=0),
                                xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                                font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))
    return fig

fig_pec_france_figure_de = create_pec_france_figure(pec_france_data,"Fossiler Primärenergieverbrauch",
                                  "<i>Erreicht</i>", "Mineralöl", "Kohle","Erdgas", "Gesamt",
                                  "<i>Ziele</i>", "Ziel der LEC", "Linearer Verlauf",
                                  "<i>Ziele der PPE</i>", "Mineralöl", "Kohle","Erdgas",
                                  "<i>Trends</i>", "5-Jahre-Trend",
                                  "Terawattstunden (TWh)",
                                  15000, 4000, 
                                  "Bis 2030", "Bis 2050", 
                                  "openENTRANCE Szenarien", "Szenariokorridor")

fig_pec_france_figure_en = create_pec_france_figure(pec_france_data,"Fossil primary energy consumption",
                                  "<i>Achieved</i>", "Oil","Coal","Natural gas", "Total",
                                  "<i>Targets</i>", "Target of the LEC", "Linear trend",
                                  "<i>Targets of the PPE</i>", "Oil","Coal","Natural gas",
                                  "<i>Trends</i>", "5-years-trend",
                                  "Terawatthour (TWh)",
                                  15000, 4000, 
                                  "Until 2030", "Until 2050",
                                  "openENTRANCE scenarios", "Scenario corridor")

fig_pec_france_figure_fr = create_pec_france_figure(pec_france_data,"Consommation d'énergie primaire fossile",
                                  "<i>Atteint</i>","Pétrole","Charbon","Gaz naturel", "Total",
                                  "<i>Objectifs</i>", "Objectif de la LEC", "Progression linéaire", 
                                  "<i>Objectifs de la PPE</i>", "Pétrole","Charbon","Gaz naturel",
                                  "<i>Tendance</i>", "5 dernières années",
                                  "Térawattheure (TWh)",
                                  15000, 4000, 
                                  "Jusqu'en 2030", "Jusqu'en 2050",
                                 "Scénarios openENTRANCE", "Intervalle de scénarios")

fig_pec_france_figure_de.write_html("docs/france/figures/fig_pec.de.html", include_plotlyjs="directory")
fig_pec_france_figure_en.write_html("docs/france/figures/fig_pec.en.html", include_plotlyjs="directory")
fig_pec_france_figure_fr.write_html("docs/france/figures/fig_pec.fr.html", include_plotlyjs="directory")

# %% Final energy consumption for France ###########################################################

fec_france_data         = pd.read_csv("docs/france/data/fec.csv", sep = ",")
fec_france_data['date'] = pd.to_datetime(fec_france_data['date'], format='%Y')

#%% create final energy consumption figure for France

def create_fec_france_figure(data,title,
                             legend_group_achieved, buildings,transport,industry,agriculture,others,total,
                             legend_group_plan, plan_lec, linear_reduction,
                             legend_group_plan_ppe, plan_ppe_buildings, plan_ppe_transport, plan_ppe_industry, plan_ppe_agriculture,
                             y_axis_title,
                             y_max_2030, y_max_2050, til_2030, til_2050,
                             scenarios_group_name, opentrance_span):

    """
    Create final energy consumption figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['buildings'],
                             name = buildings,
                             marker = dict(color= 'rgba(243, 218, 29, 0.7)', 
                                       line=(dict(width=1, color='rgba(243, 218, 29, 1)'))),
                             legendgroup="achieved",
                             legendgrouptitle_text=legend_group_achieved,
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['transport'],
                             name = transport,
                             marker = dict(color= 'rgba(235, 126, 30, 0.7)', 
                                       line=(dict(width=1, color='rgba(235, 126, 30, 1)'))),
                             legendgroup="achieved",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['industry'],
                             name = industry,
                             marker = dict(color= 'rgba(44, 81, 191,0.7)', 
                                       line=(dict(width=1, color='rgba(44, 81, 191,1)'))),
                             legendgroup="achieved",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['agriculture'],
                             name = agriculture,
                             marker = dict(color= 'rgba(160, 217, 188,0.7)', 
                                       line=(dict(width=1, color='rgba(160, 217, 188,1)'))),
                             legendgroup="achieved",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['others'],
                             name = others,
                             marker_color = "#996600",
                             marker_line_width = 0,
                             legendgroup="achieved",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Scatter(x=data['date'], y=data['total'], 
                         name = total,
                         marker = dict(color= 'rgba(60, 67, 90, 1)', 
                                       line=(dict(width=1, color='rgba(60, 67, 90, 1)'))),
                         hovertemplate = '%{y:.0f} TWh',
                         legendgroup = 'achieved'))
    #fig.add_trace(go.Scatter(x = data['date'], y = data['plan_ppe'],
    #                         name = plan_ppe,
    #                         mode = "markers", marker_size=12, marker_symbol = "x-dot",
    #                         marker_color = "rgba(143, 47, 40, 0.6)",
    #                         hovertemplate = '%{y:.0f} TWh',
    #                         legendgroup = 'plan_ppe',
    #                         legendgrouptitle_text=legend_group_plan))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_buildings'],
                             name = plan_ppe_buildings,
                             marker_color = "rgba(243, 218, 29, 0.5)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             legendgrouptitle_text=legend_group_plan_ppe,
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_transport'],
                             name = plan_ppe_transport,
                             marker_color = "rgba(235, 126, 30, 0.5)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             legendgrouptitle_text=legend_group_plan_ppe,
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_industry'],
                             name = plan_ppe_industry,
                             marker_color = "rgba(44, 81, 191,0.5)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             legendgrouptitle_text=legend_group_plan_ppe,
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Bar(x = data[(data['date']=='2024') | (data['date']=='2028')]['date'], y = data[(data['date']=='2024') | (data['date']=='2028')]['plan_ppe_agriculture'],
                             name = plan_ppe_agriculture,
                             marker_color = "rgba(160, 217, 188,0.5)",
                             marker_line_width = 0,
                             marker_pattern=dict(shape='/'),
                             legendgroup="plan_ppe",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_lec'],
                             name = plan_lec,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             marker_color = "rgba(143, 47, 40, 1)",
                             hovertemplate = '%{y:.0f} TWh',
                             legendgroup = 'plan',
                             legendgrouptitle_text=legend_group_plan))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear_reduction,
                             line = dict(color="rgba(143, 47, 40, 1)", dash='dot', width = 1),
                             legendgroup = 'plan',
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['opentrance_max'], data['opentrance_min'][::-1]]),
                             name = opentrance_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="opentrance",
                             legendgrouptitle_text=scenarios_group_name,
                             hoverinfo='skip'
                             ))
    
    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.15,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["1990", "2032"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["1990", "2051"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )


    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,2000])
    fig.update_xaxes(matches = None,                       range = ['1990','2032'])

    fig.update_layout(title = title, template = "simple_white", uniformtext_mode = 'hide',
                                barmode = 'stack',
                                legend_title_text = '',
                                legend_groupclick = "toggleitem",
                                legend = {"title" : {"text":None},
                                        "traceorder"     :"grouped",
                                        "orientation"    :"h",
                                        "x"              : 0,
                                        "y"              : -0.1,
                                        "yanchor"        : "top",
                                        "xanchor"        : "left",
                                        "borderwidth"    : 0.8,
                                        "font"           : {"size": 11},
                                        "grouptitlefont" : {"size": 11},
                                        "groupclick"     : "toggleitem"},
                                margin=dict(l=0, r=0, t=130, b=0, pad=0),
                                xaxis_hoverformat='%Y', xaxis_tickformat='%Y', hovermode = "x",
                                font=dict(family = "'sans-serif','arial'",size=14,color='#000000'))
    return fig

fig_fec_france_figure_de = create_fec_france_figure(fec_france_data,"Endenergieverbrauch",
                                  "<i>Erreicht</i>", "Gebäude", "Verkehr","Industrie","Landwirtschaft und Fischerei", "Nicht zugeordnet", "Gesamt",
                                  "<i>Ziele</i>", "Ziele der LTECV", "Linearisierter Verlauf", 
                                  "<i>Ziele der PPE</i>", "Gebäude", "Verkehr","Industrie","Landwirtschaft und Fischerei", 
                                  "Terawattstunden (TWh)",
                                  2000, 2000, "Bis 2030", "Bis 2050", 
                                  "openENTRANCE Szenarien", "Szenariokorridor")

fig_fec_france_figure_en = create_fec_france_figure(fec_france_data,"Final energy consumption",
                                  "<i>Reached</i>", "Buildings","Transport","Industry","Agriculture and fishing", "Undetermined", "Total",
                                  "<i>Targets</i>", "Targets of the LTECV", "Linearized trend", 
                                  "<i>Targets of the PPE </i>", "Buildings","Transport","Industry","Agriculture and fishing",
                                  "Terawatt-hour (TWh)",
                                  2000, 2000, "Until 2030", "Until 2050",
                                  "openENTRANCE scenarios", "Scenario corridor")

fig_fec_france_figure_fr = create_fec_france_figure(fec_france_data,"Consommation finale d'énergie",
                                  "<i>Atteint</i>", "Bâtiments","Transports","Industrie","Agriculture et pêche", "Non affecté", "Total",
                                  "<i>Objectifs</i>",  "Objectifs de la LTECV", "Trajectoire linéarisée", 
                                  "<i>Objectifs de la PPE</i>", "Bâtiments","Transports","Industrie","Agriculture et pêche",
                                  "Térawattheure (TWh)",
                                  2000, 2000, "Jusqu'en 2030", "Jusqu'en 2050",
                                  "Scénarios openENTRANCE", "Intervalle de scénarios")

fig_fec_france_figure_de.write_html("docs/france/figures/fig_fec.de.html", include_plotlyjs="directory")
fig_fec_france_figure_en.write_html("docs/france/figures/fig_fec.en.html", include_plotlyjs="directory")
fig_fec_france_figure_fr.write_html("docs/france/figures/fig_fec.fr.html", include_plotlyjs="directory")

# %% Power trade France- Germany/Belgien ###########################################################
# Data from CSV
trade_france_data = pd.read_csv("docs/france/data/power_trade.csv", sep=";")
# function for data filter years
def filter_last_n_years(df, n):
    return df[df['Date'].dt.year >= (df['Date'].dt.year.max() - n + 1)]
# Exports
trade_france_data_AB_Ex = trade_france_data[(trade_france_data['frontiere'].str.strip() == 'Belgium/Germany') & (trade_france_data['Filière'].str.strip() == 'Exports')]
trade_france_data_AB_Ex['Date'] = pd.to_datetime(trade_france_data_AB_Ex['Date'])
trade_france_data_AB_Ex['Valeur (TWh)'] = trade_france_data_AB_Ex['Valeur (TWh)'].str.replace(',', '.').astype(float)
df_last_5_years_EX = filter_last_n_years(trade_france_data_AB_Ex, 5)
df_last_10_years_EX = filter_last_n_years(trade_france_data_AB_Ex, 10)
df_last_15_years_EX = filter_last_n_years(trade_france_data_AB_Ex, 15)
# Imports
trade_france_data_AB_IM = trade_france_data[(trade_france_data['frontiere'].str.strip() == 'Belgium/Germany') & (trade_france_data['Filière'].str.strip() == 'Imports')]
trade_france_data_AB_IM['Date'] = pd.to_datetime(trade_france_data_AB_IM['Date'])
trade_france_data_AB_IM['Valeur (TWh)'] = trade_france_data_AB_IM['Valeur (TWh)'].str.replace(',', '.').astype(float)
df_last_5_years_IM = filter_last_n_years(trade_france_data_AB_IM, 5)
df_last_10_years_IM = filter_last_n_years(trade_france_data_AB_IM, 10)
df_last_15_years_IM = filter_last_n_years(trade_france_data_AB_IM, 15)
# Balance-Daten filtern
trade_france_data_AB_BA = trade_france_data.loc[ (trade_france_data['frontiere'].str.strip() == 'Belgium/Germany') & (trade_france_data['Filière'].str.strip() == 'Balance')]
trade_france_data_AB_BA['Date'] = pd.to_datetime(trade_france_data_AB_BA['Date'])
trade_france_data_AB_BA['Valeur (TWh)'] = trade_france_data_AB_BA['Valeur (TWh)'].str.replace(',', '.').astype(float)
df_last_5_years_BA = filter_last_n_years(trade_france_data_AB_BA, 5)
df_last_10_years_BA = filter_last_n_years(trade_france_data_AB_BA, 10)
df_last_15_years_BA = filter_last_n_years(trade_france_data_AB_BA, 15)
# Combine DataFrames for button functionality
df_combined_5_years = pd.concat([df_last_5_years_EX, df_last_5_years_IM])
df_combined_10_years = pd.concat([df_last_10_years_EX, df_last_10_years_IM])
df_combined_15_years = pd.concat([df_last_15_years_EX, df_last_15_years_IM])

# Plotly figure
def create_fig_france_trade(title_name, y_axis_name, 
                        exports_name, imports_name, balance_name,
                        label_button_5y, label_button_10y, label_button_15y, 
                        exports_color= 'rgba(0,120,107,0.7)', exports_line= 'rgba(0,120,107,1)',
                        imports_color= 'rgba(105,70,135,0.7)', imports_line= 'rgba(105,70,135,1)', 
                        balance_color= 'rgba(236,102,8,1)'):
    fig_france_trade = go.Figure()
    # Add Bar trace for start with 5 years
    fig_france_trade.add_trace(go.Bar(x=df_combined_5_years['Date'],
                                    y=df_combined_5_years[df_combined_5_years['Filière'] == 'Exports']['Valeur (TWh)'],
                                    name=exports_name, 
                                    marker = dict(color= exports_color, 
                                                  line=(dict(width=2, color=exports_line))))
                            )
    # Add Bar trace for start with 5 years
    fig_france_trade.add_trace(go.Bar(x=df_combined_5_years['Date'],
                                    y=df_combined_5_years[df_combined_5_years['Filière'] == 'Imports']['Valeur (TWh)'],
                                    name=imports_name,  
                                    marker = dict(color= imports_color, 
                                                  line=(dict(width=2, color=imports_line)))
                                    )
                            )
    fig_france_trade.add_hline(y=0, line_width=1, line_color='rgba(0, 0, 0,1)')
    # Add Scatter trace for Balance
    fig_france_trade.add_trace(go.Scatter(x=df_last_5_years_BA['Date'],
                                        y=df_last_5_years_BA['Valeur (TWh)'],
                                        mode='lines',
                                        name=balance_name,
                                        line=dict(color=balance_color))
                            )
    # Update layout with buttons
    fig_france_trade.update_layout(
        title = title_name,
        template="simple_white",
        uniformtext_mode = 'hide',
        font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
        barmode = "relative",hovermode='x',
        updatemenus=[
            dict(
                type="buttons",
                direction="right",
                buttons=[dict(label=label_button_5y,
                            method="update",
                            args=[{"x": [df_combined_5_years['Date']],
                                    "y": [df_combined_5_years[df_combined_5_years['Filière'] == 'Exports']['Valeur (TWh)'], 
                                        df_combined_5_years[df_combined_5_years['Filière'] == 'Imports']['Valeur (TWh)'],
                                        df_last_5_years_BA['Valeur (TWh)']
                                        ],
                                    "marker.color": [exports_color, imports_color, balance_color]
                                    }]
                            ),
                        dict(label=label_button_10y,
                            method="update",
                            args=[{"x": [df_combined_10_years['Date']],
                                    "y": [df_combined_10_years[df_combined_10_years['Filière'] == 'Exports']['Valeur (TWh)'],
                                        df_combined_10_years[df_combined_10_years['Filière'] == 'Imports']['Valeur (TWh)'],
                                        df_last_10_years_BA['Valeur (TWh)']
                                        ],
                                    "marker.color": [exports_color, imports_color, balance_color]
                                    }]
                            ),
                    dict(
                        label=label_button_15y,
                        method="update",
                        args=[
                            {"x": [df_combined_15_years['Date']],
                            "y": [
                                df_combined_15_years[df_combined_15_years['Filière'] == 'Exports']['Valeur (TWh)'],
                                df_combined_15_years[df_combined_15_years['Filière'] == 'Imports']['Valeur (TWh)'],
                                df_last_15_years_BA['Valeur (TWh)']
                            ],
                            "marker.color": [exports_color, imports_color, balance_color]
                            }]
                    )
                ],
                pad={"r": 0, "t": 25},
                showactive=True,
                x=0.0,
                xanchor='left',
                yanchor='top',
                y=1.2
            )
        ],
        yaxis=dict(gridcolor='lightgray', title=y_axis_name),  
        showlegend=True,
        legend_groupclick = "toggleitem",
        legend = {'orientation':'h', 'y':-0.25,
                'yanchor': 'bottom', 'xanchor': 'left'}
    )
    return fig_france_trade

fig_trade_france_de = create_fig_france_trade("Stromhandel von Frankreich mit Deutschland und Belgien", "TWh", 
                                              "Exporte", "Importe", "Saldo", 
                                              "Letzte 5 Jahre", "Letzte 10 Jahre", "Letzte 15 Jahre")
fig_trade_france_en = create_fig_france_trade("Power trade of France with Germany and Belgium", "TWh", 
                                              "Exports", "Imports", "Balance", 
                                              "Last 5 years", "Last 10 years", "Last 15 years")
fig_trade_france_fr = create_fig_france_trade("Échanges commerciaux d'électricité de la France avec l'Allemagne et la Belgique", "TWh", 
                                              "Exportations", "Importations", "Solde", 
                                              "5 dernières années", "10 dernières années", "15 dernières années")

fig_trade_france_de.write_html("docs/france/figures/powertrade.de.html", include_plotlyjs="directory")
fig_trade_france_en.write_html("docs/france/figures/powertrade.en.html", include_plotlyjs="directory")
fig_trade_france_fr.write_html("docs/france/figures/powertrade.fr.html", include_plotlyjs="directory")


# %% Carbon intensity of power generation for France ###########################################################
# load Data
df_carbon_intensity = pd.read_csv("docs/france/data/carbonemissions.csv",encoding='utf-8', sep=",")
# Sort data by date
df_carbon_intensity['Date'] = pd.to_datetime(df_carbon_intensity['Date'], format='%Y-%m')
df_carbon_intensity = df_carbon_intensity.sort_values(by='Date')
# Plot figure

def create_fig_france_cbintensity(title_name, 
                                  name_total_emissions, name_carbon_intensity, 
                                  unit_total_emissions, unit_power_intensity):
    fig_carbon_intensity = make_subplots(specs=[[{"secondary_y": True}]])
    fig_carbon_intensity.add_trace(go.Scatter(x=df_carbon_intensity['Date'], 
                                              y=df_carbon_intensity['tax_co2_mean'], 
                                              mode='lines', 
                                              marker_color = 'rgba(0, 148, 164, 1)',
                                              name=name_carbon_intensity,
                                              hovertemplate = '%{y:.0f} g/kWh'), 
                                              secondary_y=True)
    fig_carbon_intensity.add_trace(go.Bar(x=df_carbon_intensity['Date'], 
                                          y=df_carbon_intensity['decarb_mt_sum'], 
                                          name=name_total_emissions,
                                          marker = dict(color='rgba(60, 67, 90, 0.7)', 
                                                        line=(dict(width=2, color='rgba(60, 67, 90, 1)'))),
                                          hovertemplate = '%{y:.2f} Mt'), 
                                          secondary_y=False)
    fig_carbon_intensity.update_yaxes(title_text=unit_power_intensity, 
                                    secondary_y=True)
    fig_carbon_intensity.update_yaxes(title_text=unit_total_emissions,
                                    secondary_y=False)

    fig_carbon_intensity.update_layout(title = title_name,
                                    template="simple_white",
                                    uniformtext_mode = 'hide',
                                    barmode='stack',
                                    hovermode='x',
                                    font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                                    yaxis2=dict(title = dict(font=dict(color='rgba(0, 148, 164, 1)')),
                                                tickfont=dict(color='rgba(0, 148, 164, 1)'),
                                                showline=True, linecolor='rgba(0, 148, 164, 1)'),
                                    showlegend=True,
                                    legend_groupclick = "toggleitem",
                                    legend = {'orientation':'h', 'y':-0.25, 'yanchor': 'bottom', 'xanchor': 'left'})
    return fig_carbon_intensity

fig_carbonintensity_france_de = create_fig_france_cbintensity("CO<sub>2</sub>-Emissionen der Stromerzeugung", 
                                                              "Emissionen", "Emissionsfaktor", 
                                                              "Millionen Tonnen", "g/kWh")
fig_carbonintensity_france_en = create_fig_france_cbintensity("CO<sub>2</sub> emissions of electricity generation", 
                                                              "Emissions", "Emission factor", 
                                                              "Million tons", "g/kWh")
fig_carbonintensity_france_fr = create_fig_france_cbintensity("CO<sub>2</sub> émis par la production d'électricité", 
                                                              "Émissions", "Facteur d'émissions", 
                                                              "Million de tonnes", "g/kWh")

fig_carbonintensity_france_de.write_html("docs/france/figures/carbonintensity.de.html", include_plotlyjs="directory")
fig_carbonintensity_france_en.write_html("docs/france/figures/carbonintensity.en.html", include_plotlyjs="directory")
fig_carbonintensity_france_fr.write_html("docs/france/figures/carbonintensity.fr.html", include_plotlyjs="directory")

# %% Duplicate plotly.min.js for the EN and FR #####################################################

# Specify the source file path
source_file = "docs/france/figures/plotly.min.js"

# Specify the destination file path with a different name
destination_file_en = "docs/france/figures/plotly.min.en.js"
destination_file_fr = "docs/france/figures/plotly.min.fr.js"

# Copy the file to the destination with a different name
shutil.copyfile(source_file, destination_file_en)
shutil.copyfile(source_file, destination_file_fr)

#%% ################################################################################################
