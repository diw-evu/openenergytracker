---
hide:
  - navigation
  - toc
---

## Journals 

- [Schill, W. P., Roth, A., Guéret, A. & Schmidt, F. (2024). Ampel-Monitor Energiewende: ambitionierte Ziele, aber zu geringe Dynamik. *Wirtschaftsdienst*, vol.104, no.6, pp.427-430. DOI:10.2478/wd-2024-0110](https://www.wirtschaftsdienst.eu/inhalt/jahr/2024/heft/6/beitrag/ampel-monitor-energiewende-ambitionierte-ziele-aber-zu-geringe-dynamik.html)

## DIW Wochenbericht (Deutsch)

- [Schmidt, F., Roth, A. & Schill, W. P. (2024). Ausbau der Solarenergie: viel Licht, aber auch Schatten. *DIW Wochenbericht* 33, 507-517. DOI:10.18723/diw_wb:2024-33-1](https://www.diw.de/de/diw_01.c.911061.de/publikationen/wochenberichte/2024_33_1/ausbau_der_solarenergie__viel_licht__aber_auch_schatten.html)
  
- [Guéret, A. & Schill, W. P. (2024). Energiewende in Frankreich: Ausbau Erneuerbarer stockt, gute Fortschritte bei Wärmepumpen. *DIW Wochenbericht* 4, 51-61. DOI:10.18723/diw_wb:2024-4-1](https://www.diw.de/de/diw_01.c.890547.de/publikationen/wochenberichte/2024_04_1/energiewende_in_frankreich__ausbau_erneuerbarer_stockt__gute_fortschritte_bei_waermepumpen.html)

- [Schill, W. P., Roth, A., Guéret, A. & Schmidt, F. (2023). Gemischte Halbzeitbilanz für Ampel bei Energiewende – Gute Fortschritte bei Photovoltaik, schwache Dynamik bei Elektromobilität und Windenergie. *DIW Aktuell* 90.](https://www.diw.de/de/diw_01.c.887074.de/publikationen/diw_aktuell/2023_0090/gemischte_halbzeitbilanz_fuer_ampel_bei_energiewende_____gut___taik__schwache_dynamik_bei_elektromobilitaet_und_windenergie.html)
  
- [Schill, W. P., Roth, A., & Guéret, A. (2022). Ampel-Monitor zeigt: Energiewende muss deutlich beschleunigt werden. *DIW Wochenbericht* 27, 371-379. DOI:10.18723/diw_wb:2022-27-1.](https://www.diw.de/de/diw_01.c.844911.de/publikationen/wochenberichte/2022_27_1/ampel-monitor_zeigt__energiewende_muss_deutlich_beschleunigt_werden.html)

## DIW Weekly Report (English)

- [Guéret, A. & Schill, W. P. (2024). The Energy Transition in France: Expansion of Renewables Stalling, Good Progress on Heat Pumps. *DIW Weekly Report* 3/4, 31-41. DOI:10.18723/diw_dwr:2024-4-1](https://www.diw.de/de/diw_01.c.892073.de/publikationen/weekly_reports/2024_04_1/the_energy_transition_in_france__expansion_of_renewables_stalling__good_progress_on_heat_pumps.html)
  
- [Schill, W. P., Roth, A., & Guéret, A. (2022). Ampel-Monitor Energiewende shows the pace of the energy transition must be accelerated significantly. *DIW Weekly Report* 26/27/28, 171-179. DOI:10.18723/diw_dwr:2022-26-1.](https://www.diw.de/de/diw_01.c.845846.de/publikationen/weekly_reports/2022_26_1/ampel-monitor_energiewende_shows_the_pace_of_the_energy_transition_must_be_accelerated_significantly.html)

## DIW Ampel-Monitor Blogs (Deutsch)

- [Roth, A. & Schill W. P. (2023). Ampel-Monitor Energiewende #8: Aktuelle Zahlen zu Elektroautos. *DIW Blog.*](https://www.diw.de/de/diw_01.c.883435.de/nachrichten/ampel-monitor_energiewende__8__aktuelle_zahlen_zu_elektroautos.html)

- [Roth, A., Schill W. P. & Schmidt, F. (2023). Ampel-Monitor Energiewende #7: Deep Dive zur Windkraft an Land. *DIW Blog.*](https://www.diw.de/de/diw_01.c.878342.de/nachrichten/ampel-monitor_energiewende__7__deep_dive_zur_windkraft_an_land.html)
  
- [Roth, A., Schill W. P. & Schmidt, F. (2023). Ampel-Monitor Energiewende #6: Deep Dive zur Photovoltaik. *DIW Blog.*](https://www.diw.de/de/diw_01.c.876338.de/nachrichten/ampel-monitor_energiewende__6__deep_dive_zur_photovoltaik.html)
  
- [Schill, W. P. (2023). Ampel-Monitor Energiewende #5: Eine Million Elektrofahrzeuge. *DIW Blog.*](https://www.diw.de/de/diw_01.c.862674.de/nachrichten/ampel-monitor_energiewende__5__eine_million_elektrofahrzeuge__zulassungsrekord_im_dezember.html)

- [Roth, A., Schmidt, F. (2022). Ampel-Monitor Energiewende #4: Haushalte und Gewerbe sparen Erdgas. *DIW Blog.*](https://www.diw.de/de/diw_01.c.858819.de/nachrichten/ampel-monitor_energiewende__4__haushalte_und_gewerbe_sparen_erdgas.html)

- [Roth, A., Schill, W. P. (2022). Ampel-Monitor Energiewende #3: Aktuelle Daten zum Erdgasverbrauch. *DIW Blog.*](https://www.diw.de/de/diw_01.c.856100.de/nachrichten/ampel-monitor_energiewende__3__aktuelle_daten_zum_erdgasverbrauch.html)

- [Schill, W. P., Roth, A. (2022). Ampel-Monitor Energiewende #2: Der Stand der Dinge am 24. August 2022. *DIW Blog.*](https://www.diw.de/de/diw_01.c.850169.de/nachrichten/ampel-monitor_energiewende__2__der_stand_der_dinge_am_24._august_2022.html)

- [Schill, W. P., Roth, A. (2022). Ampel-Monitor Energiewende #1: Der Stand der Dinge am 6. Juli 2022. *DIW Blog.*](https://www.diw.de/de/diw_01.c.844650.de/nachrichten/ampel-monitor_energiewende__1__der_stand_der_dinge_am_6._juli_2022.html)

## DIW Ampel-Monitor Blogs (English)

- [Schill, W. P., Roth, A. (2022). Ampel-Monitor Energiewende #2: The state of affairs on August 24, 2022. *DIW Blog.*](https://www.diw.de/en/diw_01.c.850169.en/nachrichten/ampel-monitor_energiewende__2__the_state_of_affairs_on_august_24__2022.html)
