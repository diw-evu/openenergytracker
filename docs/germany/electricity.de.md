---
hide:
#  - navigation
#  - toc
---

# Erneuerbarer Strom

=== "Autor*Innen"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)
	
## :fontawesome-solid-solar-panel: Photovoltaik

*Zusätzliche Informationen sind im [Hintergrund zur Photovoltaik](../germany/background/pv.de.md) zu finden.*

### Gesamtbestand und Ziele

Für die Photovoltaik hat sich die Bundesregierung im Rahmen des [Erneuerbaren-Energien-Gesetzes (EEG)](https://www.gesetze-im-internet.de/eeg_2014/) ein Ausbauziel von 215 GW bis zum Jahr 2030 gesetzt. Danach soll die PV-Leistung weiter kontinuierlich wachsen, auf 400 GW im Jahr 2040 (folgende Abbildung, oberer Teil). Der geplante Ausbau steigt im Zeitverlauf an (Abbildung, unterer Teil). Dies ergibt sich aus im EEG formulierten Zwischenzielen sowie der [Photovoltaik-Strategie](https://www.bmwk.de/Redaktion/DE/Publikationen/Energie/photovoltaik-stategie-2023.pdf)) der Bundesregierung. Im Jahr 2023 sollten netto, also unter Berücksichtigung des Abgangs von Altanlagen, 9 GW dazu kommen, in den Jahren 2024 und 2025 sind es 13 bzw. 18 GW. Von 2026 an sollen dann 22 GW pro Jahr zugebaut werden. Bis ungefähr 2022 war das Ausbautempo deutlich geringer, zuletzt hat es aber stark angezogen und lag sogar leicht über dem Zielpfad.

<iframe title = "Abbildung installierte Leistung Photovoltaik" src="../../../docs/germany/figures/pv.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das PV-Ziel für das Jahr 2030 liegt am oberen Ende des von den Ariadne-Szenarien aufgespannten Korridors (hier ohne das Modell TIMES). Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 praktisch auf dem Ziel der der Koalition. Nach 2030 steigt die PV-Leistung in den Ariadne-Szenarien weiter an. Die von der Bundesregierung langfristig angestrebten 400 GW werden allerdings nur in manchem Szenarien und auch erst nach dem Jahr 2040 erreicht.

---

## :material-wind-turbine: Windkraft an Land

*Zusätzliche Informationen sind im [Hintergrund zur Windkraft an Land](../germany/background/wind.de.md) zu finden.*

### Gesamtbestand und Ziele

Für die Windkraft an Land hat sich die Bundesregierung im [Erneuerbare-Energien-Gesetz (EEG)](https://www.gesetze-im-internet.de/eeg_2014/) ein Ziel von 115 GW installierter Leistung im Jahr 2030 gesetzt. Danach strebt sie einen weiteren Ausbau auf 160 GW im Jahr 2040 an (folgende Abbildung, oberer Teil). Auch bei der Windkraft soll der Zubau bis 2030 nicht linear erfolgen, sondern im Zeitverlauf ansteigen (Abbildung, unterer Teil). Für die Jahre 2023 und 2024 lassen sich Netto-Ausbauziele (d.h. unter Berücksichtigung des Abgangs von Altanlagen) von rund 4 bzw. 6,5 GW ableiten; danach sollen bis 2028 7,5 GW pro Jahr zugebaut werden, ab 2031 sogar 8,4 GW pro Jahr. In den letzten Jahren war das Ausbautempo deutlich geringer, und der Zielpfad wird derzeit deutlich verfehlt.

<iframe title = "Abbildung installierte Leistung Windkraft an Land" src="../../../docs/germany/figures/wind_onshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Die Ziele für den Ausbau der Windkraft an Land liegen ungefähr in der Mitte des von allen Ariadne-Szenarien aufgespannten Korridors, der breiter ist als der Zielkorridor bei der Photovoltaik. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* dauerhaft etwas über dem politischen Ziel.

### Ziele für Flächen

Die Bundesregierung hat sich auch Ziele für die Ausweisung von Flächen für die Windenergie gesetzt. Im [Gesetz zur Erhöhung und Beschleunigung des Ausbaus von Windenergieanlagen an Land](https://dip.bundestag.de/vorgang/gesetz-zur-erh%C3%B6hung-und-beschleunigung-des-ausbaus-von-windenergieanlagen-an/288780) ist festgelegt, dass Ende des Jahres 2027 im Mittel aller Bundesländer ein Anteil von 1,4 Prozent erreicht werden soll, Ende 2032 dann zwei Prozent. Dabei gelten für die einzelnen Bundesländer spezifische Zielvorgaben. Die drei Stadtstaaaten müssen bis 2032 nur 0,5 Prozent ihrer Landesfläche für die Nutzung der Windkraft ausweisen; bei den südlichen Flächenländer Bayern und Baden-Württemberg sind es je 1,8 Prozent, in einigen mitteldeutschen Ländern wie Hessen, Thüringen und Sachsen-Anhalt sind es 2,2 Prozent. Bundesweit waren einem früheren [Bericht](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) des Bund-Länder-Kooperationsausschusses zufolge Ende 2020 nur 0,70 Prozent der Fläche rechtswirksam für Windenergie an Land ausgewiesen (unterer Korridor ohne Doppelzählungen, Flächenfestlegungen entweder ausschließlich auf Ebene der Raumordnung oder auf Bauleitplanebene). Bis zum Jahr 2023 hat sich dieser Wert dem jüngsten [Bericht](https://www.bmwk.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2024/bericht-bund-laender-kooperationsausschuss-2024.pdf?__blob=publicationFile&v=2) zufolge auf 0,92 Prozent erhöht. Die Ausweisung von Flächen für die Windkraft liegt jedoch unter einem linear interpolierten Zielpfad.

<iframe title = "Abbildung ausgewiesene Landesflächen für Windkraft an Land" src="../../../docs/germany/figures/wind_areas.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

---

## Windkraft auf See

Für die Windkraft auf See ist im [Windenergie-auf-See-Gesetzes](https://www.gesetze-im-internet.de/windseeg/) eine Leistung von mindestens 30 GW im Jahr 2030 als Ziel genannt. Danach soll die installierte Leistung weiter stark wachsen, auf mindestens 40 GW im Jahr 2035 und 70 GW im Jahr 2045. Aufgrund der Größe einzelner Projekte bzw. ihrer Netzanschlüsse ist der Zielpfad weniger gleichmäßig als bei der Windkraft an Land. Aus dem [Offshore-Flächenentwicklungsplan 2023](https://www.bsh.de/DE/THEMEN/Offshore/Meeresfachplanung/Flaechenentwicklungsplan/_Anlagen/Downloads/FEP_2023_1/Flaechenentwicklungsplan_2023.pdf?__blob=publicationFile&v=1) lassen sich für die Jahre 2026-2032 verschiedene Ausbaustufen ableiten; wo das nicht möglich war, wurde in der Abbildung linear interpoliert.

<iframe title = "Abbildung installierte Leistung Windkraft auf See"src="../../../docs/germany/figures/wind_offshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel für den Ausbau der Windkraft auf See im Jahr 2030 liegt über dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 nur gut halb so hoch wie das Ziel der der Koalition. Der Ausbau der Windenergie auf See nimmt in den Ariadne-Szenarien erst nach 2025 Fahrt auf. Bis 2045 steigt die auf See installierte Windkraftleistung dann sehr unterschiedlich an, im Extremfall auf bis zu 80 GW im Jahr 2045. Die Ziele der Bundesregierung liegen durchgängig im oberen Bereich des Ariadne-Szenarienkorridors.

---

## Anteile im Stromsektor

Dem [Erneuerbare-Energien-Gesetz (EEG)](https://www.gesetze-im-internet.de/eeg_2014/) zufolge soll der Anteil erneuerbarer Energien am Bruttostrombedarf (bzw. Bruttostromverbrauch, BSV) bis zum Jahr 2030 auf mindestens 80 Prozent gesteigert werden. 

<iframe title = "Abbildung Anteil erneuerbare Energien Strom" src="../../../docs/germany/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Statistische Daten zu diesem Indikator werden von der AGEE-Stat grundsätzlich jährlich bereit gestellt, z.B. [hier](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen). Im Jahr 2020 betrug der Anteil 45,5 Prozent, im Jahr 2021 waren es nur noch 41,5 Prozent. Dieser Rückgang dürfte weitgehend durch den im Jahr 2020 pandemiebedingt deutlich niedrigeren Stromverbrauch und durch ein relativ schlechtes Windjahr 2021 bedingt sein. Seitdem stieg der Anteil wieder deutlich. Zur Erreichung des 2030-Ziels muss der Anteil ab 2021 im Durchschnitt um über vier Prozentpunkte pro Jahr wachsen. Nach der Vollendung des Kohleausstiegs wird laut [Erneuerbare-Energien-Gesetz (EEG)](https://www.gesetze-im-internet.de/eeg_2014/) eine treibhausgasneutrale Stromerzeugung angestrebt, was wir in der Abbildung mit einem Anteil erneuerbarer Energien von 95 Prozent ab dem Jahr 2035 illustrieren.

Ergänzend können in der Abbildung auch die Anteile erneuerbarer Energien an der Nettostromerzeugung (NSE) dargestellt werden (durch Klick auf den entsprechenden Eintrag in der Legende). Sie werden im Rahmen der [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) des Fraunhofer ISE regelmäßig aktualisiert und liegen somit sehr viel früher vor als der im Koalitionsvertrag genannte Indikator. Die Abbildung zeigt die vorläufigen Daten, die zum Zeitpunkt der letzten Aktualisierung dieser Webseite verfügbar waren, d.h. im Lauf des Jahres kann sich der Anteil aufgrund saisonaler Effekte noch deutlich verändern (*). Die Anteile erneuerbarer Energien an der NSE und am BSV unterscheiden sich unter anderem deshalb, da der Nenner im ersten Fall aufgrund des Kraftwerkseigenverbrauchs geringer ist. 

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für den Anteil der erneuerbaren Energien im Jahr 2030 liegt in dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für den Ausbau erneuerbarer Energien, REMIND, liegt im *Technologiemix-Szenario* 2030 mit 87 Prozent sogar über dem Ziel der Koalition von 80 Prozent. Nach 2030 steigt der Anteil in den meisten Ariadne-Szenarien auf über 90 Prozent an.