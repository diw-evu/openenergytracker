---
description: Energie-Ziele der deutschen Bundesregierung - und wo wir heute stehen

hide:
#  - navigation
#  - toc
---

# Deutschland

=== "Autor*Innen"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md)

!!! Success "Neuigkeiten"
    
	* Neue Abbildungen zu [**Energiepreisen**](../../../docs/germany/prices)
		
Im Open Energy Tracker stellen wir verschiedene energiepolitische Ziele Deutschlands grafisch dar und vergleichen sie regelmäßig mit dem aktuell tatsächlich erreichten Stand. Viele dieser Ziele stammen von der Ampel-Regierung und sind für das Jahr 2030 spezifiziert, einige auch darüber hinaus. Im Fokus stehen dabei der Ausbau erneuerbarer Energien im Strom- und Wärmebereich, die Elektromobilität sowie grüner Wasserstoff. Ergänzend werden wesentliche Entwicklungen bei Energieverbrauch, Energiepreisen und Treibhausgasemissionen dargestellt. Für manche Indikatoren sind monatlich aktualisierte Daten verfügbar, für andere nur Jahresdaten. Aus rein illustrativen Gründen wird meist ein linearer Verlauf zwischen dem heutigen Stand und dem Zieljahr (meist Dezember 2030) dargestellt. Die genauen Pfade wurden von der Bundesregierung für viele Indikatoren nicht spezifiziert. Zusätzlich stellen wir lineare Projektionen aktueller Trends dar.

<center>
<iframe title = "Aktueller Stand gegenüber Zielen für 2030" src="../../../docs/germany/figures/fig_reached.html" width="75%" height= "400px" frameBorder="0" loading = "lazy"></iframe>
</center>

Bei praktisch allen wichtigen Indikatoren besteht eine große Lücke zwischen dem aktuellen Stand und den Regierungszielen für das Jahr 2030. Diese Lücke ist bei der installierten Elektrolyseleistung sowie bei der Elektromobilität besonders groß. Auch die installierte Leistung der Windkraft auf See sowie der Bestand an Wärmepumpen sind noch relativ weit von ihren Zielen für das Jahr 2030 entfernt.

Die Regierungsziele und bisherigen Verläufe können bei einigen Indikatoren auch mit Ergebnissen der [Ariadne-Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) verglichen werden. Dabei handelt es sich um Szenarienanalysen, die im vom BMBF geförderten Kopernikus-Projekt [Ariadne](https://ariadneprojekt.de/) durchgeführt wurden und unterschiedliche Wege zur Klimaneutralität Deutschlands im Jahr 2045 aufzeigen. Wir stellen dabei sowohl die von allen Ariadne-Szenarien aufgespannte Korridore dar, als auch die Ergebnisse des jeweiligen Ariadne-Leitmodells im sogenannten *Technologiemix-Szenario* (8Gt_Bal). Bei den Ariadne-Szenarien sind Daten grundsätzlich nur in Fünfjahresschritten verfügbar (2020, 2025, 2030, ...). Zwischen diesen Stützjahren haben wir linear interpoliert. Die Daten der Ariadne-Szenarien liegen bis zum Jahr 2045 vor und erlauben somit einen längerfristigen Blick auf plausible Entwicklungen bei den jeweiligen Indikatoren. Weitere Visualisierungen sowie Daten zu allen Ariadne-Szenarien stehen auf externen Webseiten im [Pathfinder](https://pathfinder.ariadneprojekt.de/) sowie im [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne) zur Verfügung.
