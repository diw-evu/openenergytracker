---
hide:
# - navigation
  - toc
---
# Consommation actuelle de gaz naturel dans les secteurs résidentiel et commercial

=== "Auteurs"

    [Alexander Roth](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

=== "Suggestion de citation"

    ``Alexander Roth & Felix Schmidt (2023) - "Consommation actuelle de gaz naturel dans les secteurs résidentiel et commercial". Publié sur OpenEnergyTracker.org. Récupéré de : 'https://openenergytracker.org/fr/docs/germany/naturalgas/' [site web]``

    ou

    [``Roth, Alexander, and Felix Schmidt. "Not only a mild winter: German consumers change their behavior to save natural gas." Joule 7, no. 6 (2023): 1081-1086.``](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

!!! Success ""
    
    Les données de cette section sont mises à jour tous les jeudis.


Afin d'éviter une pénurie de gaz au cours de l'hiver 2022/23 après l'arrêt des livraisons de gaz russe, l'Allemagne a dû non seulement trouver d'autres fournisseurs, mais aussi des moyens de réduire la demande de gaz. Cette page montre la consommation hebdomadaire et cumulée de gaz naturel dans les secteurs résidentiel et commercial, ainsi que les économies réalisées par rapport à 2018/21. L'encadré en fin de page présente les données et la méthodologie utilisées pour l'analyse présentée.

## Consommation hebdomadaire de gaz naturel

Une comparaison directe des niveaux de consommation entre les années ne permet pas de déterminer le niveau des économies (comportementales), en particulier pour les consommateurs résidentiels et commerciaux dont la consommation dépend fortement des températures extérieures. Une évaluation opportune des efforts d'économie nécessite donc une correction statistique des données de consommation en fonction des conditions météorologiques.

Sur la base des données de consommation et des données météorologiques de 2018/21, nous estimons la relation entre les températures extérieures et la consommation de gaz naturel résidentielle et commerciale à l'aide d'une méthode de forêt linéaire (voir l'encadré méthodologique ci-dessous). Nous utilisons le modèle estimé pour prédire la demande de gaz naturel attendue compte tenu des températures actuelles si les consommateurs n'avaient pas modifié leur comportement par rapport à la période 2018/21. La comparaison entre la demande prédite et la consommation réelle permet de déduire les niveaux d'économie de gaz. Nous nous concentrons sur les périodes représentatives de septembre à juin, car le gaz est principalement utilisé pour le chauffage dans les secteurs d'intérêt. 

<iframe title = "Figure Consommation hebdomadaire de gaz" src="../../../docs/germany/figures/fig_gas_consumption.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

La figure montre la consommation de gaz naturel prévue (ligne pointillée) et la consommation de gaz naturel réelle (ligne noire). Chaque fois que la ligne pointillée est supérieure à la ligne noire, les ménages et les clients commerciaux **consomment actuellement moins de gaz** que ce qui aurait été prévu aux températures actuelles sur la base des données 2018/21. Au cours de la saison de chauffage 2022/23, nous avons enregistré des économies pour presque toutes les semaines.

## Économies : composantes météorologiques et comportementales

<iframe title = "Figure Comportement en matière d'économies de gaz et conditions météorologiques" src="../../../docs/germany/figures/fig_gas_savings_abs.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

En utilisant la consommation "prévue" estimée, nous pouvons décomposer les économies de gaz naturel du secteur résidentiel et commercial en 2022 en une "composante météorologique" et une "composante comportementale". Contrairement à une version antérieure de cette page, nous montrons maintenant une décomposition des économies *absolues*, car celles-ci sont pertinentes pour éviter une pénurie de gaz. En particulier pendant les semaines d'hiver froides, nous observons de petites économies relatives mais de grandes économies absolues (cf. [Roth et Schmidt (2023)] (https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)). Le panneau de gauche de la figure montre une variation significative des économies entre les semaines calendaires au cours de l'hiver 2022/23. La vague de froid de décembre 2022 se distingue particulièrement. Le panneau de droite montre les économies totales par saison de chauffage. Il est évident que les économies réalisées au cours de l'hiver 2022/23 sont principalement dues à des changements de comportement. Le temps plus chaud y a également contribué, mais de manière mineure.

## Consommation cumulée

<iframe title = "Figure Consommation cumulée de gaz" src="../../../docs/germany/figures/fig_gas_reached.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

La figure ci-dessus montre la consommation cumulée pour la période allant de septembre à juin. La consommation est restée très inférieure aux niveaux précédents tout au long de la saison 2022/23.

!!! info ""

    Nos données sont également présentées par les deux journaux allemands dans leurs tableaux de bord énergétiques respectifs :
    
    - [Süddeutsche Zeitung (SZ)](https://www.sueddeutsche.de/projekte/artikel/wirtschaft/erdgas-versorgung-ernergiekrise-daten-e426783/#einsparungen)
    - [Stuttgarter Zeitung](https://stuttgarter-zeitung.de/energiedaten) 

!!! info ""
    
    Les auteurs de cette page ont publié un article sur le sujet dans la revue académique [Joule](https://www.cell.com/joule/) en juin 2023. Les petites divergences entre les résultats présentés ici et dans l'article proviennent de différences mineures dans la méthodologie et les données : [Roth, Alexander et Felix Schmidt. "Not only a mild winter : Les consommateurs allemands changent de comportement pour économiser le gaz naturel". Joule 7, no. 6 (2023) : 1081-1086.](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

??? info "Méthodologie et données"

    === "Méthodologie"

        En utilisant une "forêt linéaire", une méthode d'apprentissage automatique, nous estimons la consommation de gaz résidentielle et commerciale sur la base des températures journalières moyennes, maximales et minimales pour le jour en cours et les trois derniers jours. Nous agrégeons les données de centaines de stations météorologiques à travers l'Allemagne en utilisant la médiane pour éviter les biais dus aux points de données extrêmes, tels que ceux du Zugspitze. En outre, nous contrôlons le mois calendaire en cours et les week-ends ou jours fériés pour tenir compte de la saisonnalité inhérente à la consommation de gaz (au-delà des variations de température) et des différents comportements de consommation les jours non ouvrés.

        Nous entraînons notre modèle en utilisant des données quotidiennes entre juillet 2018 et décembre 2021. Nous obtenons les données de température auprès du service météorologique allemand et les données de consommation de gaz auprès de Trading Hub Europe. Nous avons validé la qualité de la prédiction du modèle par rapport aux niveaux de consommation réels entre janvier et juin 2018 afin de déterminer le modèle le plus performant. Nous avons ainsi comparé une série de modèles d'apprentissage automatique, notamment un réseau neuronal, un estimateur LASSO, des arbres linéaires, le gradient boosting, des forêts aléatoires et des forêts linéaires. La spécification de la forêt linéaire présentait l'erreur quadratique moyenne la plus faible et a donc donné les meilleurs résultats.

        Nous utilisons le modèle obtenu pour prévoir la consommation en 2022. Il n'est pas évident de déterminer la date limite à partir de laquelle on peut s'attendre à ce que la consommation réelle de gaz naturel s'écarte de la consommation estimée sur la base des années précédentes. Des publications comparables commencent à établir des prévisions dès septembre 2021, lorsque les prix du gaz ont commencé à augmenter dans les centres de négoce européens. Une autre approche consisterait à choisir le début de la guerre de la Russie contre l'Ukraine. Nous ne prenons en compte que les ménages et les clients commerciaux, qui ne sont généralement affectés par les évolutions du marché de gros qu'avec un long retard. Par conséquent, nous estimons qu'il est plausible que l'inclusion de données d'entraînement jusqu'en décembre 2021 n'introduise pas de biais dans les prévisions.

        Nous comparons nos prévisions avec les données de consommation de l'Agence fédérale allemande des réseaux en résolution hebdomadaire, car elle fournit des données plus récentes que le Trading Hub Europe.

        De plus amples détails sur la sélection et l'estimation du modèle sont disponibles [dans ce carnet](https://gitlab.com/diw-evu/oet/openenergytracker/-/blob/main/scripts/static/gas_savings_analysis.ipynb).

        Nous convertissons les économies cumulées de gaz basées sur les changements de comportement dans la consommation en expéditions de GNL en supposant une capacité moyenne de navire de [170 000 mètres cubes](https://www.igu.org/resources/world-lng-report-2022/). Cela correspond à une énergie de près de 1,2 TWh (sans tenir compte des pertes pendant le transport et la regazéification).

    === "Données"

        - **Consommation de gaz naturel** : [Bundesnetzagentur - Consommation de gaz des clients résidentiels et commerciaux en GWh/jour, moyenne hebdomadaire](https://www.bundesnetzagentur.de/DE/Gasversorgung/aktuelle_gasversorgung/_svg/GasverbrauchSLP_woechentlich/Gasverbrauch_SLP_W_2023.html)
        - **Consommation de gaz naturel des années précédentes** : [Trading Hub Europe - Aggregate Consumption Data Publication - Consumption Data SLP Area](https://www.tradinghub.eu/de-de/Ver%C3%B6ffentlichungen/Transparenz/Aggregierte-Verbrauchsdaten)
        - **Température** : [Deutscher Wetterdienst Open Daten Portal](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/historical/) ; Mesures quotidiennes de la température moyenne, minimale et maximale de l'air à 2 m en °C.
