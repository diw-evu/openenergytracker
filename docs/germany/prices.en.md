---
hide:
#  - navigation
#  - toc
---

# Energy prices

=== "Autors"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Nicolas Aichner](../../docs/about/authors.md), [Jan Czimmek](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Household electricity prices
The affordability of energy, and of electricity in particular, has been an important energy policy goal of all federal German governments in recent years. Here we show how the average electricity price for households has developed since the liberalization of the German electricity market. We draw on the [BDEW electricity price analysis](https://www.bdew.de/service/daten-und-grafiken/bdew-strompreisanalyse/). Average prices for a household with an annual consumption of 3500 kWh are shown, with fixed price components also allocated to consumption. We first show the data in current prices as provided by BDEW. In addition, we present the prices adjusted for inflation (price base 2023, deflated with consumer price index).

<iframe title = "Figure household electricity prices nominal and real" src="../../../docs/germany/figures/hh_elec_tariff.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Electricity prices for households are significantly higher than wholesale market prices because they include various other price components. These include costs for distribution and meters, regulated network charges for the operation of transmission and distribution networks, and various taxes, levies, and surcharges. In the years 2013 to 2021, taxes, levies and surcharges accounted for more than half of the household electricity price. In the wake of the energy price crisis, however, their share has fallen to just under one-third, as procurement costs have risen sharply due to high wholesale prices. The EEG levy, which has been charged since 2000 to finance the expansion of renewable energies, was abolished on July 1, 2022. However, this relief was more than offset by the increase in procurement costs in 2022 and 2023. In 2024, procurement costs and therefore prices fell significantly again.

## Wholesale electricity prices
In addition to the development of household electricity prices, that of wholesale prices is also of interest. The following figure shows the distribution of hourly wholesale prices for one megawatt hour (MWh) of electricity in the market area of Germany based on data from the [Federal Network Agency](https://www.smard.de/home/downloadcenter/download-marktdaten/). Until 2018, Germany, Austria and Luxembourg shared one market area for electricity; since 01.10.2018, Austria forms its own market area, while Luxembourg remains in a market area with Germany. Data for the current year (*) are always updated weekly.

Wholesale prices rose to unprecedented highs in the context of the energy price crisis in 2022. In addition to generation shortfalls in hydroelectric power in Europe and nuclear power in France, this was mainly due to a sharp rise in natural gas prices. These had a direct impact on electricity prices, as natural gas power plants set the price in many hours. The average price was just over 30 euros/MWh from 2015 to 2020; it rose to almost 100 euros/MWh in 2021 and to 235 euros/MWh in 2022. Hourly maximum prices and the price spread between individual hours have also risen sharply. After reaching a peak in late summer 2022, prices have since fallen again significantly. However, they are still about three times higher in the current year than during pre-crisis levels. In 2023, in a single hour, there was also an extreme negative price equal to the negative day-ahead price limit of -500 euros/MWh (zoom out by double-clicking on the figure).

<iframe title = "Figure wholesale electricity prices" src="../../../docs/germany/figures/wholesale_electricity.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Distribution over the day

The following figures show the distribution of average wholesale prices over the course of the day, broken down by summer (April-September) and winter half-year (October-March). In the summer half-year in particular, a trend known as the “duck curve” has become increasingly apparent in recent years: prices fall sharply around midday, as this is when electricity generation from photovoltaics is concentrated (the belly of the duck). At the same time, prices rise in the shoulder periods in the morning and especially in the evening (head of the duck). One reason for this is that increasingly expensive peak-load power plants are used during these hours.

<iframe title = "Figure average wholesale prices over the course of the day Summer" src="../../../docs/germany/figures/fig_duckcuve_sum.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

This effect is much less pronounced in the winter months, as solar power generation is significantly lower here.

<iframe title = "Figure Average wholesale prices over the course of the day Winter" src="../../../docs/germany/figures/fig_duckcuve_win.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

### Negative prices

Negative prices occur on the wholesale market when the supply of electricity exceeds demand. This can be due to inflexibly operated conventional or renewable electricity generation. Subsidized renewable energy plants partly have economic incentives to produce electricity even in hours with negative prices, and partly there is not even a technical possibility of switching off during such hours. Since 2015, the number of negative prices has tended to increase - with an interruption in 2021 and 2022 due to the sharp rise in electricity prices in the meantime. There have never been as many negative prices as in 2024. The data for the current year (*) is updated on an ongoing basis, also in the following figures. 

<iframe title = "Figure negative cumulative prices" src="../../../docs/germany/figures/negative_price_cumulated.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following figure shows the hours with negative prices per year, sorted in ascending order according to the level of negative prices. After the crisis year 2022, not only did the number of hours with negative prices increase significantly, but there was also an increase in more negative price levels. Since 2023, there have also been prices below -€100/MWh, but so far only in very few hours.

<iframe title = "Figure negative price-duration curve" src="../../../docs/germany/figures/negative_price_duration.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

In addition, the following figure shows the number of hours with negative prices over the course of the day. Negative prices have recently occurred mainly during the day when electricity generation from photovoltaic systems was high, particularly in the hours around midday. This goes hand in hand with the recent sharp rise in solar energy output. This chart also shows the increased occurrence of negative prices since 2015, with the exception of 2021 and 2022.

<iframe title = "Figure negative prices over the course of the day" src="../../../docs/germany/figures/negative_price_hourly.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following histogram shows the frequency of hours with different negative price levels. Most negative prices have been between €0 and -10/MWh in all years to date. Prices lower than -100€/MWh are very rare and are summarized in the last category.

<iframe title = "Figure negative prices histogram" src="../../../docs/germany/figures/negative_price_histo.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following chart shows how often there have been consecutive hours with negative electricity market prices of various lengths in recent years. The longest interval to date was 36 consecutive hours in 2023.

<iframe title = "Figure negative prices histogramm" src="../../../docs/germany/figures/negative_price_consecutive.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following figure shows for 2024 which technologies were used to generate electricity in hours with negative prices in Germany, broken down by different price levels (up to -10 €/MWh, up to -20 €/MWh, etc.). Photovoltaics has by far the largest share, which also tends to increase slightly with more negative prices. This is likely to be mainly due to rooftop systems, which are subsidized via constant feed-in tariffs and have no incentives - and often no technical possibility - of being curtailed in times of negative prices. The share of other technologies in electricity generation in times of negative prices is lower and is likely to be indicated by various types of “must-run” conditions (e.g. technical minimum load of thermal power plants, combined heat and power generation, provision of balancing reserves). The figure is again based on data from the [Federal Network Agency](https://www.smard.de/home/downloadcenter/download-marktdaten/).

<iframe title = "Figure slider" src="../../../docs/germany/figures/neg_price_generation_shares.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Market values and capture rate of renewable energies

In addition to wholesale prices, the market values of solar energy and wind power are also of interest. The market value corresponds to the average price, weighted by the respective electricity generation, that all plants of a certain technology achieve on average on the electricity market. It can also be interpreted as the average revenue of a technology. Monthly market values are particularly important for the direct marketing of renewable electricity. If the market value is set in relation to the (unweighted) average price on the electricity market, the so-called market value factor, also known as the yield rate or *capture rate*, is obtained. The figure shows the monthly market value factors of wind power and solar energy in Germany based on data from the [grid transparency portal](https://www.netztransparenz.de/de-de/Erneuerbare-Energien-und-Umlagen/EEG/Transparenzanforderungen/Marktpr%C3%A4mie/Marktwert%C3%BCbersicht) and compares them with the respective market shares of the technologies. We calculate the latter on the basis of data from the [Federal Network Agency](https://www.smard.de/home).

<iframe title = "Figure Market values time course" src="../../../docs/germany/figures/capture_rate.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

The following chart does not show the monthly market value factors over time, but as a function of the respective market share. This tends to show the so-called cannibalization effect: if the market share of wind power or solar energy increases, their market value factor can fall while conditions in the electricity sector remain otherwise unchanged. Additional flexibility in the electricity sector, e.g. in the form of energy storage, can counteract this cannibalization effect, as described [here](http://www.annualreviews.org/eprint/WSMW2UJJFWMJXX6FD7PU/full/10.1146/annurev-resource-101620-081246).

<iframe title = "Figure Market values scatter" src="../../../docs/germany/figures/capture_rate_scatter.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Revenues from photovoltaics

The following figure shows the average shares of individual hours in the total revenue from photovoltaics over the course of the day, differentiated by summer and winter half-year. It is assumed that all PV electricity is marketed on the day-ahead wholesale market. It can be seen that photovoltaics has recently been able to generate less and less revenue in the midday hours in the summer half-year, i.e. in the hours of its highest electricity generation. Instead, higher revenue shares are shifting to the morning and evening hours. This is due to the cannibalization effect described above and indicates that flexibility in the electricity system has recently not kept pace with the expansion of PV. This effect is much less pronounced in the winter months, as solar power generation is significantly lower.

<iframe title = "Figure PV revenue distribution over the course of the day" src="../../../docs/germany/figures/pv_revs_year.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

The decline in PV electricity market revenues in the midday hours is also partly due to the feed-in and marketing of PV electricity in hours with negative prices (see above, section [Negative prices](#negative-prices)). The following shows how PV revenues would have been distributed on the electricity market last year and in the current year if prices had not become negative, but had fallen to a minimum of EUR 0 during negative price phases. This counterfactual assumption could be made plausible, for example, by stopping remuneration payments to PV systems in hours with negative prices, or by additional electric loads that are switched on in times of negative electricity prices. It can be seen that negative prices already have a significant impact on the distribution of PV revenues in 2023, but even more so in 2024. In particular, they strongly influence the decline in revenue at midday.

<iframe title = "Figure PV revenue distribution over the course of the day counterfactual" src="../../../docs/germany/figures/pv_revs_cf.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

In addition, the following figure shows the development of the absolute monthly PV market values since January 2023. The influence of negative wholesale prices is also shown here by counterfactually assuming that the price never falls below EUR 0 per MWh. In the summer months, there is a relatively clear influence of negative prices of over €6 per MWh in some cases (May 2024).

<iframe title = "Illustration of monthly PV revenues counterfactual" src="../../../docs/germany/figures/pv_monthly_avg_cf.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>