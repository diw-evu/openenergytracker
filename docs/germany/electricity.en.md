---
hide:
#  - navigation
#  - toc
---

# Renewable electricity

=== "Authors"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

## :fontawesome-solid-solar-panel: Solar PV

*Additional information can be found in the [background on photovoltaics](background/pv.en.md).*

For solar PV, the government coalition has specified a capacity expansion target of 215 GW by 2030 in [Renewable Energy Sources Act (EEG)](https://www.gesetze-im-internet.de/eeg_2014/). 

<iframe title = "Figure installed pv solar capacity" src="../../../docs/germany/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

According to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat), the installed capacity in Germany in November 2021 was nearly 60 GW. To achieve the target, an average of approx. 1.4 GW per month must be added by the end of 2030 (including intermediate targets specified in EEG and federal [PV strategy](https://www.bmwk.de/Redaktion/DE/Publikationen/Energie/photovoltaik-stategie-2023.pdf)). This is the required net capacity deployment, i.e. also taking into account old plants that will be taken off the grid over time. Over the last years, the expansion was significantly lower than that. If PV capacity additions were to follow the trend of the years 2017-2021, only around 92 GW could be reached by the end of 2030. In the last twelve months, the pace of expansion was only slightly higher, so a continuation of this trend would also fall significantly short of the target. To reach the 215 GW target in 2030, the expansion must therefore be around three times faster than the trend of the last twelve months. After 2030, the PV capacity is set to grow further, reaching 400 GW in 2040.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be shown in the figure. The PV target of the coalition for the year 2030 is at the upper end of the corridor of all Ariadne scenarios (here without the model TIMES). The Ariadne lead model for the expansion of renewable energy sources, REMIND, is more or less similar to the coalition target in the *Technology Mix Scenario* for 2030. After 2030, PV capacity increases further in the Ariadne scenarios. Yet, the 400 GW targeted by the government are only reached in some scenarios, and only after 2040.

---

## :material-wind-turbine: Wind energy 

*Additional information can be found in the [background on onshore wind energy](../germany/background/wind.en.md).*

### Onshore

For onshore wind power, the German government coalition is aiming for an installed capacity of 115 GW in 2030 according to the [Renewable Energy Sources Act (EEG)](https://www.gesetze-im-internet.de/eeg_2014/). 

<iframe title = "Figure onshore wind capacity" src="../../../docs/germany/figures/wind_onshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

In the month of November 2021, the installed capacity in Germany was just under 56 GW, according to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat). To achieve the target, therefore, an average of a good 0.5 GW per month (again net) must be added by the end of 2030. After 2030, the government aims to further expand capacity to 160 GW in 2040.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's targets for the expansion of onshore wind power are roughly in the middle of the corridor formed by all Ariadne scenarios, which is wider than the respective target corridor for photovoltaic. The Ariadne lead model for renewable energy expansion, REMIND, is permanently slightly above the coalition's target in the *Technology Mix Scenario* in 2030.

#### Land use

The federal government has also set itself targets for the designation of areas for wind energy. The [Act to Increase and Accelerate the Expansion of Onshore Wind Energy Systems](https://dip.bundestag.de/vorgang/gesetz-zur-erh%C3%B6hung-und-beschleunigung-des-ausbaus-von-windenergieanlagen-an/288780) stipulates that an average share of 1.4 percent should be achieved by the end of 2027 across all federal states, and then two percent by the end of 2032. Specific targets apply to the individual federal states. The three city states only have to designate 0.5 percent of their land area for the use of wind power by 2032; in the southern states of Bavaria and Baden-Württemberg it is 1.8 percent each, and in some central German states such as Hesse, Thuringia and Saxony-Anhalt it is 2.2 percent. According to an earlier [report](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) by the Federal Government-Länder Cooperation Committee, only 0.70 percent of the area nationwide was legally designated for onshore wind energy at the end of 2020 (lower corridor without double counting, area designations either exclusively at the regional planning level or at the urban land-use plan level). According to the latest [report](https://www.bmwk.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2024/bericht-bund-laender-kooperationsausschuss-2024.pdf?__blob=publicationFile&v=2), this figure has increased to 0.92 percent by 2023. However, the designation of areas for wind power is below a linear interpolated target path.

<iframe title = "Figure area designated to wind onshore", src="../../../docs/germany/figures/wind_areas.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

---

### Offshore

For offshore wind power, the government coalition is aiming for a capacity of at least 30 GW in 2030 according to the [Wind Energy at Sea Act](https://www.gesetze-im-internet.de/windseeg/). In the month of November 2021, the installed capacity in German waters was 7.8 GW, according to [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat). 

<iframe title = "Figure installed offshore wind capacity" src="../../../docs/germany/figures/wind_offshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

In order to achieve the expansion target, an average of approx. 0.2 GW per month (net) must be added to meet the 2030 target. After that, installed capacity is expected to continue to grow strongly, to at least 40 GW in 2035 and 70 GW in 2045. Due to the size of individual projects and their grid connections, the target path is less smooth than for onshore wind power. Various expansion stages can be derived from the [Offshore Area Development Plan 2023](https://www.bsh.de/DE/THEMEN/Offshore/Meeresfachplanung/Flaechenentwicklungsplan/_Anlagen/Downloads/FEP_2023_1/Flaechenentwicklungsplan_2023.pdf?__blob=publicationFile&v=1) for the years 2026-2032; where this was not possible, linear interpolation was used in the figure.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's target for the expansion of offshore wind power in 2030 is above the Ariadne scenario corridor. In the Ariadne lead model for the expansion of renewable energy sources, REMIND, the installed capacity is only about half as high as the coalition's target in the *Technology Mix Scenario* in 2030. The expansion of offshore wind energy in the Ariadne scenarios only picks up speed after 2025. Until 2045, the installed wind power capacity at sea then increases very differently, in the extreme case up to 80 GW in 2045. The targets of the federal government are consistently in the upper range of the Ariadne scenario corridor.

---

## Shares in the power sector

The coalition aims to increase the share of renewable energy in gross electricity consumption to 80 percent by 2030. Statistical data on this indicator are provided by AGEE-Stat on an annual basis, e.g., [here](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen). 

<iframe title = "Figure share of renewables in electricity" src="../../../docs/germany/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

In 2020, the share was 45.5 percent; in 2021, it was only 41.5 percent. This decline is likely to be largely caused by significantly lower electricity consumption in 2020 due to the pandemic and a relatively poor wind year in 2021. Since then, the share has risen again significantly. To reach the 2030 target, the share must grow by more than four percentage points per year on average from 2021 onwards. According to the [Renewable Energy Sources Act (EEG)](https://www.gesetze-im-internet.de/eeg_2014/), once the coal phase-out has been completed, the aim is to achieve a greenhouse gas-neutral power generation, which we illustrate in the figure with a renewable energy share of 95 percent from 2035.

In addition, the shares of renewable energy in net electricity generation can also be shown in the figure (by clicking on the corresponding entry in the legend). These shares are regularly updated by Fraunhofer ISE's [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=en&c=DE&interval=year) and are thus available much earlier than the consumption-based indicator mentioned in the coalition agreement. The graph shows the data available at the time of the last update of this website, i.e. the share can still change significantly over the course of the current year due to seasonal effects (*). The shares of renewable energy in net electricity generation and gross electricity consumption differ, among other reasons, because the denominator is lower in the first case due to own consumption of power plants. 

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The coalition's target for the share of renewable energies in 2030 lies within the Ariadne scenario corridor. The Ariadne lead model for renewable energy expansion, REMIND, even exceeds the coalition's target of 80 percent in the *Technology Mix Scenario* in 2030 with a share of 87 percent. After 2030, the share increases beyond 90 percent in most Ariadne scenarios.