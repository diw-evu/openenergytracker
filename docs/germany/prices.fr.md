---
hide:
#  - navigation
#  - toc
---

# Prix de l'énergie

=== "Auteurs"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Prix de l'électricité pour les ménages

L'accord de coalition ne mentionne pas d'objectif quantitatif pour l'évolution des prix de l'électricité pour les ménages. Cependant, l'accessibilité financière de l'énergie, et en particulier de l'électricité, a été un objectif important de la politique énergétique de tous les gouvernements fédéraux de ces dernières années. Nous montrons ici comment le prix moyen de l'électricité pour les ménages a évolué depuis la libéralisation du marché de l'électricité. Pour ce faire, nous nous basons sur l'analyse [BDEW-Strompreisanalyse](https://www.bdew.de/service/daten-und-grafiken/bdew-strompreisanalyse/). Les prix moyens pour un ménage avec une consommation annuelle de 3500 kWh sont représentés, les éléments de prix fixes étant également répartis sur la consommation. Nous présentons tout d'abord les données mises à disposition par le BDEW en prix courants, et en plus une variante corrigée de l'inflation (base de prix du 2023, déflatée par l'indice des prix à la consommation). 

<iframe title = "Figure " src="../../../docs/germany/figures/hh_elec_tariff.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>


Les prix de l'électricité pour les ménages sont nettement plus élevés que les prix du marché de gros, car ils comprennent divers autres éléments de prix. Il s'agit notamment des coûts de distribution et de comptage, des tarifs réglementés pour l'exploitation des réseaux de transport et de distribution, ainsi que de divers impôts, taxes et prélèvements. Entre 2013 et 2021, les impôts, taxes et prélèvements représentaient plus de la moitié du prix de l'électricité pour les ménages. Dans le cadre de la crise des prix de l'énergie, leur part a toutefois diminué à près d'un tiers, car les coûts d'approvisionnement ont fortement augmenté en raison des prix de gros élevés. La taxe EEG, prélevée depuis 2000 pour financer le développement des énergies renouvelables, a été supprimée au 1er juillet 2022. Cet allègement a toutefois été nettement plus que compensé par la hausse des coûts d'approvisionnement en 2022 et 2023. En 2024, les coûts d'approvisionnement, et donc les prix, ont à nouveau nettement baissé.

## Prix de l'électricité en gros

Outre l'évolution des prix de l'électricité pour les ménages, celle des prix de gros est également intéressante. La figure suivante montre la répartition des prix de gros horaires pour un mégawattheure (MWh) d'électricité dans la zone de marché Allemagne sur la base des données de [Bundesnetzagentur](https://www.smard.de/home/downloadcenter/download-marktdaten/). Jusqu'en 2018, l'Allemagne, l'Autriche et le Luxembourg se partageaient une zone de marché pour l'électricité ; depuis le 01.10.2018, l'Autriche forme sa propre zone de marché, tandis que le Luxembourg reste dans une zone de marché avec l'Allemagne. Les données pour l'année en cours (*) sont toujours actualisées à la fin du mois.

Dans le contexte de la crise des prix de l'énergie, les prix de gros ont atteint en 2022 des sommets jamais atteints auparavant. La raison en est, outre les interruptions de la production hydroélectrique en Europe ainsi que de la production nucléaire en France, la forte hausse des prix du gaz naturel. Ces derniers ont eu un impact direct sur les prix de l'électricité, car les centrales électriques au gaz naturel fixent les prix à de nombreuses heures. Le prix moyen était d'un peu plus de 30 euros/MWh entre 2015 et 2020 ; il est passé à près de 100 euros/MWh en 2021 et à 235 euros/MWh en 2022. Les prix maximaux horaires et l'écart de prix entre certaines heures ont également fortement augmenté. Après avoir atteint un pic à la fin de l'été 2022, les prix ont entre-temps de nouveau nettement baissé. Cette année, ils sont toutefois encore environ trois fois plus élevés que lors du niveau d'avant la crise. En 2023, un prix extrêmement négatif a également été enregistré en une seule heure, à hauteur de la limite de prix négatif day-ahead de -500 euros/MWh (dézoomer en double-cliquant sur l'illustration).

<iframe title = "Illustration des prix de gros de l'électricité" src="../../../docs/germany/figures/wholesale_electricity.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>.

## Valeurs de marché et facteurs de valeur de marché des énergies renouvelables

Outre les prix de gros, les valeurs de marché de l'énergie solaire et de l'énergie éolienne sont également intéressantes. La valeur de marché correspond au prix moyen, pondéré par la production d'électricité correspondante, que toutes les installations d'une technologie donnée obtiennent en moyenne sur le marché de l'électricité. Elle peut également être interprétée comme le revenu moyen d'une technologie. Les valeurs de marché mensuelles jouent un rôle particulièrement important pour la commercialisation directe de l'électricité renouvelable. Si l'on met la valeur de marché en relation avec le prix moyen (non pondéré) sur le marché de l'électricité, on obtient ce que l'on appelle le facteur de valeur de marché, également appelé taux de rendement ou *capture rate*. L'illustration montre les facteurs de valeur marchande mensuels de l'énergie éolienne et de l'énergie solaire en Allemagne sur la base des données du [portail de transparence du réseau](https://www.netztransparenz.de/de-de/Erneuerbare-Energien-und-Umlagen/EEG/Transparenzanforderungen/Marktpr%C3%A4mie/Marktwert%C3%BCbersicht) et les compare aux parts de marché respectives des technologies. Nous calculons ces dernières sur la base des données de l'[Agence fédérale des réseaux](https://www.smard.de/home).

<iframe title = "Illustration des valeurs de marché dans le temps" src="../../../docs/germany/figures/capture_rate.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Le graphique suivant présente les facteurs de valeur marchande mensuels non pas au fil du temps, mais en fonction de la part de marché respective. La tendance est à l'effet de cannibalisation : si les parts de marché de l'énergie éolienne ou solaire augmentent, leur facteur de valeur marchande peut baisser, toutes choses étant égales par ailleurs dans le secteur de l'électricité. Une flexibilité supplémentaire dans le secteur de l'électricité, par exemple sous forme de stockage d'énergie, peut contrecarrer cet effet de cannibalisation, comme décrit [ici](http://www.annualreviews.org/eprint/WSMW2UJJFWMJXX6FD7PU/full/10.1146/annurev-resource-101620-081246).

<iframe title = "Figure Valeurs de marché scatter" src="../../../docs/germany/figures/capture_rate_scatter.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

