---
hide:
#  - navigation
#  - toc
---

# Chaleur renouvelable

=== "Auteurs"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md)

## Pompes à chaleur

Les pompes à chaleur permettent d'utiliser la chaleur de l'environnement. Cela joue un rôle important dans de nombreux scénarios, notamment pour le chauffage des bâtiments. Dans son accord de coalition, le gouvernement n'a pas mentionné d'objectif pour le déploiement des pompes à chaleur : le terme "pompe à chaleur" n'est pas utilisé une seule fois dans tout le document. Toutefois, le [Bilan d'ouverture sur la protection du climat](https://www.bmwk.de/Redaktion/FR/Downloads/E/bilan-douverture-sur-la-protection-du-climat.html) du Ministère fédéral allemand de l'Économie et de la Protection du Climat (BMWK) mentionne une fourchette allant de "4,1 à 6 millions de pompes à chaleur" en 2030, ce chiffre se rapportant probablement à la fourniture de chaleur ambiante dans les bâtiments individuels (sans les pompes à chaleur de grande puissance dans les réseaux de chaleur et sans les pompes à chaleur haute température). Dans le cadre du [2e sommet sur les pompes à chaleur en novembre 2022](https://www.bmwk.de/Redaktion/DE/Downloads/Energie/2-waermepumpen-gipfel-eckpunktepapier.html), le BMWK a ensuite indiqué un objectif d'environ six millions de pompes à chaleur en 2030 ainsi qu'une construction supplémentaire d'au moins 500 000 pompes à chaleur par an à partir de 2024. Il n'existe pas de données publiques mises à jour mensuellement concernant le parc de pompes à chaleur pour le chauffage en Allemagne. Comme source de données jusqu'à fin 2022, nous utilisons [AGEE-Stat](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Entwicklung/entwicklung-der-erneuerbaren-energien-in-deutschland.html) ainsi que, pour la marge actuelle, les chiffres de [BDH](https://www.bdh-industrie.de/start/marktdaten). Actuellement, le développement des pompes à chaleur est bien en deçà de la trajectoire visée.

À titre de comparaison, les [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/) peuvent également être représentés sur le graphique. Le modèle de référence pour les pompes à chaleur (REMod) prévoit dans le *Scénario Mix Technologique* un parc de 5,3 millions de pompes à chaleur en 2030 (dans les bâtiments individuels), ce qui est inférieur à l'objectif du gouvernement. D'ici 2045, ce parc de pompes à chaleur passera à environ 15 millions. En outre, les trajectoires de développement des pompes à chaleur peuvent être comparées à celles de quatre autres études qui, avec les scénarios Ariadne, ont été surnommées les "Cinq Grandes" (ndlt. "Big 5" en anglais), à savoir "Neutralité climatique de l'Allemagne 2045" (Agora KND 2045) d'[Agora Energiewende](https://www.agora-energiewende.de/veroeffentlichungen/klimaneutrales-deutschland-2045/), "Trajectoires climatiques 2.0" (Klimapfade 2.0) du [BDI](https://bdi.eu/artikel/news/klimapfade-2-0-deutschland-braucht-eine-klima-aufbruch/), "Étude pilote : vers la neutralité climatique" (scénario KN100) de [Dena](https://www.dena.de/en/newsroom/news/dena-pilot-study-towards-climate-neutrality/), ainsi que les "Scénarios de long-terme 3" (scénario TN-Strom) du [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/). Jusqu'en 2045, le déploiement de pompes à chaleur est particulièrement inportant dans les scénarios à long terme du BMWK, alors qu'elle est la plus faible dans l'étude de Dena.

<iframe title = "Figure pompes à chaleur" src="../../../docs/germany/figures/hp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Part dans le secteur de la chaleur

La coalition vise une part "très élevée" d'énergies renouvelables dans le secteur de la chaleur. Pourtant, l'accord de coalition ne prévoit qu'un objectif quantitatif pour la chaleur "neutre en émissions", à savoir 50 % de l'offre totale de chaleur d'ici 2030. Étant donné que d'autres options, telles que l'importation d'hydrogène neutre en carbone ou le captage et le stockage du carbone, semblent peu plausibles dans le secteur allemand de la chaleur d'ici 2030, nous interprétons cet objectif quantitatif comme un objectif en termes d'énergies renouvelables, en nous écartant légèrement de la formulation exacte de l'accord de coalition. 

En 2021, selon les données de [AGEE-Stat](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen), la part des énergies renouvelables dans la consommation finale d'énergie pour le chauffage et le refroidissement (y compris le chauffage urbain) était de 15,7 %. D'ici 2030, cette part devra donc augmenter de près de quatre points de pourcentage par an, soit beaucoup plus que ces dernières années, pour atteindre l'objectif. Or, l'évolution actuelle est très nettement inférieure à cette trajectoire cible.

<iframe title = "Part des énergies renouvelables dans la chaleur" src="../../../docs/germany/figures/resshare_heat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
