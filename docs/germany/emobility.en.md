---
hide:
#  - navigation
#  - toc
---

# Electric Mobility

=== "Authors"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)

## Road transport

### Battery electric passenger cars

The government coalition has set a target of at least 15 million fully electric passenger cars by 2030, which we equate here with purely battery electric vehicles. At the end of November 2021, there were slightly below 0.6 million such passenger cars in Germany, according to the [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html). To achieve the target, an average of 0.13 million vehicles per month must be added by 2030. Such a linear progression is shown in the figure, but it is purely illustrative. In reality, a logistic pathway is more likely. Based on a [study by Fraunhofer ISI](https://www.isi.fraunhofer.de/content/dam/isi/dokumente/sustainability-innovation/2022/WP05-2022_Markthochlaufszenarien_E-Fahrzeuge_GNT-final.pdf), a logistic path can be derived that will achieve exactly 15 million electric cars in 2030 (thanks to [Patrick Plötz](https://twitter.com/PatrickPlotz) and colleagues). According to this, the current development of the vehicle stock is only slightly below the target path. Note: In addition to new registrations, the KBA also publishes the actual stock data (including deregistrations that have taken place in the meantime) every three months. In the two months in between, deregistrations are estimated on the basis of the most recent ratios of new registrations and deregistrations. Accordingly, the data for the last few months may be subject to slight changes.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be shown in the figure. The coalition's target of 15 million fully electric passenger cars in the year 2030 lies within the corridor provided by all Ariadne scenarios. The Ariadne lead model for mobility, VECTOR21, is only slightly below the coalition's target in the *Technology Mix Scenario*. By 2045, the fleet grows to nearly 42 million electric vehicles in the lead model. It should be noted that the Ariadne scenarios also include some plug-in hybrid electric vehicles, and not only fully electric ones as formulated in the coalition target.

<iframe title = "Figure stock of battery electric vehicles" src="../../../docs/germany/figures/bev.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

In addition, the following figure shows the shares of purely battery-electric cars and plug-in hybrids in new car registrations per month. Although the coalition has not formulated a specific target for this, this indicator illustrates the dynamics of what is happening better than the stock indicator shown above. At the turn of the year there were several breaks due to changing regulation and funding measures. This was particularly pronounced at the end of 2022 due to cuts in the [*Umweltbonus*](https://www.bafa.de/DE/Energie/Energieeffizienz/Elektromobilitaet/Neuen_Antrag_stellen/neuen_antrag_stellen.html).

<iframe title = "Figure new electric car registrations" src="../../../docs/germany/figures/bev_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Total passenger car fleet

In contrast to the expansion of the fully electric passenger car fleet, the governing coalition has not set a target for reducing the stock of cars with internal combustion engines. The following figure shows the development of the total passenger car fleet in Germany by drive or fuel type. The data is provided by the Kraftfahrt-Bundesamt, from 2013 to the end of 2017 as [annual data](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Jahrebilanz_Bestand/fz_b_jahresbilanz_node.html) and from mid-2018 as [quarterly data](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Vierteljaehrlicher_Bestand/viertelj%C3%A4hrlicher_bestand_node.html). Overall, the fleet continues to grow to just under 49 million passenger cars at last count. Since the beginning of 2020, the number of passenger cars with gasoline or diesel engines has been declining slightly; however, they still account for over 90 percent of the passenger car fleet. Fuel cell passenger cars play virtually no role in the overall fleet, with only around 2,000 vehicles (0.004 percent).

<iframe title = "Figure stock of passenger cars" src="../../../docs/germany/figures/cars_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Charging infrastructure

The traffic light coalition has set a target of one million publicly and non-discriminatorily accessible charging points by 2030, with a focus on fast charging infrastructure. As of 1 December 2021, according to [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html), nearly 54,000 charging points were in operation, of which around 46,000 were normal charging points and 8000 were fast charging points. To achieve the target, an average of around 8,700 new charging points must go into operation every month until 2030. The graph shows a corresponding linear path. In addition, a logistic path is available that follows the course of the logistic stock development of the battery-electric passenger car fleet shown above.

<iframe title = "Figure charging infrastructure" src="../../../docs/germany/figures/cs.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

In addition, the following figure shows the development of the charging capacity available at public charging points. The government has not yet formulated any explicit targets for this. The total installed charging capacity has recently risen sharply, primarily due to the growth in fast charging points. The average charging capacity per charging point also grew, but at a much slower rate.

<iframe title = "Figure charging capacity" src="../../../docs/germany/figures/fig_cs_capa.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

The following figure shows the ratio of battery electric cars and publicly accessible charging points. The government has not formulated an explicit target for this. The fleet and charging infrastructure targets for 2030 imply a ratio of 15 battery electric cars per charging point, although the breakdown between fast and normal charging points is unclear. In particular, the number of electric vehicles that mathematically share a publicly accessible fast charging point has increased significantly since 2020. The figure additionally shows the average installed charging capacity per electric car. It has decreased significantly since 2020.

<iframe title = "Figure BEV per charging point" src="../../../docs/germany/figures/bev_per_cp.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

## Rail transport

### Electrification of the network

The government coalition plans to electrify 75 percent of the German rail network by 2030. According to [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html), 61.7% of the federally owned rail network was electrified in 2020. To achieve the target, the share must increase by an average of almost 1.5 percentage points per year until 2030. Currently, the electrification speed is significantly lower.

<iframe title = "Figure electrification of the rail network" src="../../../docs/germany/figures/electrification.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
