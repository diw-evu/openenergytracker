---
hide:
#  - navigation
#  - toc
---

# Data

## Ariadne scenarios

[Ariadne-Pathfinder](https://pathfinder.ariadneprojekt.de/) and [Ariadne Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne)

## Renewable energy

### Capacity expansion

Solar PV: [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat) 

Onshore wind: [AGEE-Stat](https://umweltbundesamt.de/sites/default/files/medien/372/dokumente/11-2021_agee-stat_monatsbericht_final.pdf)

Offshore wind: [AGEE-Stat](https://umweltbundesamt.de/sites/default/files/medien/372/dokumente/11-2021_agee-stat_monatsbericht_final.pdf)

### Areas

Onshore wind: [Report of the Bund/Länder Cooperation Committee](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) 

### Shares in the electricity sector

Share of gross electricity consumption: [BMWi](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html)

Share of net electricity generation: [energy-charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=de&c=DE&interval=year) 

### Heat pumps

Historical development of the stock: [EurObserv'ER](https://www.eurobserv-er.org/online-database/#) for 2011-2020, [BWP](https://www.waermepumpe.de/presse/zahlen-daten/) for 2021

Governmental target: [Germany's current climate action status](https://www.bmwk.de/Redaktion/EN/Downloads/E/germany-s-current-climate-action-status.html)

Stock in future scenarios:

* "Towards a climate-neutral Germany by 2045" [Agora Energiewende](https://www.agora-energiewende.de/en/publications/towards-a-climate-neutral-germany-2045-executive-summary/)
* "Climate Paths 2.0" [BDI](https://english.bdi.eu/publication/news/climate-paths-2-0-a-program-for-climate-and-germanys-future-development/)
* "Pilot study: Towards Climate Neutrality" [Dena](https://www.dena.de/en/newsroom/news/dena-pilot-study-towards-climate-neutrality/)
* "Langfristszenarien 3" [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/)

### Shares in the heating sector

Share of renewable energy in final energy consumption for heating and cooling: [AGEE-Stat](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Zeitreihen/zeitreihen.html)

## Electric mobility

### Road transport

Battery electric passenger cars: [Kraftfahrt-Bundesamt](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html)

Charging infrastructure: [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html), spreadsheet "Ladeinfrastruktur in Zahlen"; linear interpolation between historic quarterly data.

### Rail transport

Electrification of the network: [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html)

## Hydrogen

Electrolysis capacity: [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database)

## Energy consumption

### Primary energy consumption

[BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html) and [AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/)

### Security of supply

Monthly data on gross flows of natural gas at individual border points: [IEA](https://www.iea.org/data-and-statistics/data-product/gas-trade-flows#gas-trade-flows)

## Greenhouse gas emissions

Sectoral greenhouse gas emissions: [German Federal Environmental Agency (UBA)](https://www.umweltbundesamt.de/sites/default/files/medien/361/dokumente/2022_03_15_trendtabellen_thg_nach_sektoren_v1.0.xlsx)

CO<sub>2</sub> emissions from electricity generation: [Federal Environment Agency (UBA)](https://www.umweltbundesamt.de/publikationen/entwicklung-der-spezifischen-kohlendioxid-8)