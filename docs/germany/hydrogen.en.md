---
hide:
#  - navigation
#  - toc
---

# Hydrogen

=== "Authors"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

The coalition agreement mentions an electrolysis capacity of around 10 gigawatts in 2030 as a target. According to the [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database), electrolysers with an electrical capacity of around 60 megawatts were in operation in Germany at the beginning of October 2021. To achieve the target, an average of around 90 megawatts per month must be added by the end of 2030. According to data from the [Electrolysis Monitor](https://www.wasserstoff-kompass.de/), installed capacity has barely increased to date. If you exclude a large chlor-alkali plant in Bavaria, where hydrogen is more of a by-product, the electrical output of all electrolysers in Germany is still less than 100 MW, i.e. less than one percent of the target for 2030.

Although the electrolysis capacity already in operation is still very small, there are a large number of projects that are to be developed over the next few years. These are shown in the following figure. According to the [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database), further electrolysers with an electrical output of just under 500 megawatts are currently under construction or in the final investment decision (FID) phase. In addition, there are a further 8.3 gigawatts for which feasibility studies are underway. In addition, there are (less concrete) concepts for further projects with a total capacity of 13.2 gigawatts. A specific year of planned commissioning is not specified for all projects (see column on the right). In order to achieve the target of 10 gigawatts in 2030, not only would all projects with a concrete start date that are currently at least at the feasibility study level actually have to be realized, but also a good portion of the less mature conceptual projects.

<iframe title = "Figure cumulative capacity of German hydrogen projects by status" src="../../../docs/germany/figures/iea_h2.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>