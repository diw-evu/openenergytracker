---
hide:
#  - navigation
  - toc
---
# Aktueller Erdgasverbrauch und Einsparungen von Haushalten und Gewerbe

=== "Autor*Innen"

    [Alexander Roth](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

=== "Zitiervorschlag"

    ``Alexander Roth & Felix Schmidt (2023) - "Aktueller Erdgasverbrauch und Einsparungen von Haushalten und Gewerbe". Veröffentlicht auf OpenEnergyTracker.org. Abgerufen von: 'https://openenergytracker.org/de/docs/germany/naturalgas/' [Webseite]``

    oder

    [``Roth, Alexander, and Felix Schmidt. "Not only a mild winter: German consumers change their behavior to save natural gas." Joule 7, no. 6 (2023): 1081-1086.``](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

!!! Success ""
    
    Die Daten dieses Bereichs werden jeden Donnerstag aktualisiert.

Um eine Gasmangellage im Winter 2022-23 nach dem Lieferstopp von russischem Gas zu vermeiden, musste Deutschland nicht nur neue Lieferquellen erschließen, sondern auch die Nachfrage reduzieren. Diese Seite zeigt den wöchentlichen und kumulativen Erdgasverbrauch von Haushalten und Gewerben, sowie die Einsparungen im Vergleich zum Zeitraum 2018-21. Die Daten und Methodik, die diesen Abbildungen zugrunde liegen, werden am Ende dieser Seite erläutert.

## Wöchentlicher Erdgasverbrauch 

Die Analyse des reinen Erdgasverbrauchs lässt keine Rückschlüsse auf Einsparungen zu. Dies gilt insbesondere für den Verbrauch im Haushalts- und Gewerbebereich, der im Vergleich zu dem der Industriekund\*innen stark von der Außentemperatur abhängt. Nur durch eine Witterungsbereinigung lässt sich unterscheiden, welche Einsparungen von Haushalten und Gewerben auf Verhaltensänderungen und welche auf Witterungseinflüsse zurückgehen. Für eine zeitnahe Beurteilung der Einsparerfolge ist es daher von Interesse, einen witterungsbereinigten Erdgasverbrauch zu ermitteln.

Basierend auf Verbrauchs- und Temperaturdaten der Jahre 2018-21 ermitteln wir mit Hilfe einer Linear-Forest-Methode (siehe Methodik-Kasten unten) einen Zusammenhang zwischen der Außentemperatur und dem Erdgasverbrauch von Haushalts- und Gewerbekund\*innen. Auf dieser Basis schätzen wir den aktuellen Verbrauch ab, der angsichts der aktuellen Wetterbedingungen zu erwarten wäre, wenn sich das Verhalten der Erdgaskund\*innen gegenüber den letzten Jahren nicht geändert hätte. Die Differenz zwischen diesem "erwarteten" und dem tatsächlichen Verbrauch erlaubt Schlussfolgerungen zur Frage, wie stark die Einsparbemühungen in Haushalten und Gewerbe bisher greifen. Dabei liegt der Fokus auf den Monaten September bis Juni.

<iframe title = "Abbildung Gas Verbrauch Haushalte" src="../../../docs/germany/figures/fig_gas_consumption.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Die Abbildung zeigt den erwartete Erdgasverbrauch (gepunktete Linie) und den tatsächlichen Erdgasverbrauch (schwarze Linie). Wenn der erwartete Erdgasverbrauch über dem tatsächlichen liegt, bedeutet das, dass die Haushalte und Gewerbekund\*innen **weniger Gas verbrauchen**, als dies bei den gemessen Temperaturen und dem Vorkrisenverhalten (2018-2021) zu erwarten wäre. In der Heizsaison 2022-23 wurde fast in jeder Woche Einsparungen erzielt.

## Einsparungen: Witterungs- und Verhaltenkomponenten

<iframe title = "Abbildung absolute Gaseinsparungen Verhalten und Wetter" src="../../../docs/germany/figures/fig_gas_savings_abs.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Mit Hilfe des geschätzten, "erwarteten" Verbrauchs können wir die Erdgaseinsparungen von Haushalten und Gewerbe in eine "Witterungskomponente" und eine "Verhaltenskomponente" zerlegen. Im Gegensatz zu einer früheren Version dieser Abbildung zeigen wir die *absoluten* Einsparungen, da diese - im Gegennsatz zu *relativen* Einsparungen - für die Vermeidung einer Gasmangellage relevant sind. Gerade in kalten Winterwochen realisieren sich oft geringe relative, aber große absolute Einsparungen (vgl. [Roth und Schmidt (2023)](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)). Wie im linken Teil der Abbildung oben gut zu erkennen ist, haben die Einsparungen im Winter 2022-23 gegenüber dem Durchschnitt der Jahre 2018-21 zwischen den Wochen beträchtlich geschwankt. Gut zu erkennen ist die Kältewelle Mitte Dezember 2022.

Der rechte Teil der Abbildung zeigt die gesamten Einsparungen im Zeitraum September bis Juni. Für den Winter 2022-23 ist gut zu erkennen, dass der überwiegende Teil der Einsparungen auf Verhaltensänderungen zurückzuführen ist, während das Wetter nur eine untergeordnete Rolle gespielt hat.

## Kumulativer Verbrauch

<iframe title = "Abbildung benötigte Gaseinsparungen" src="../../../docs/germany/figures/fig_gas_reached.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Die obige Abbildung zeigt den gesamten Erdgasverbrauch in den Monaten September bis Juni. Gut zu sehen ist, dass der Verbrauch in der Heizsaison 2022-23 deutlich unter dem Verbrauch der vergangenen Jahre lag.

!!! info ""

    Unsere Daten werden auch in den jeweiligen Energiedashbords folgender deutscher Zeitungen gezeigt:

    - [Süddeutsche Zeitung (SZ)](https://www.sueddeutsche.de/projekte/artikel/wirtschaft/erdgas-versorgung-ernergiekrise-daten-e426783/#einsparungen)  
    - [Stuttgarter Zeitung](https://stuttgarter-zeitung.de/energiedaten) 

!!! info ""

    Die Autoren dieser Seite haben einen Fachartikel in der Zeitschrift [Joule](https://www.cell.com/joule/) im Juni 2023 veröffentlicht, der sich der selben Fragestellung widmet. Da dort leicht andere Daten und Methoden genutzt wurden, können sich die Ergebnisse ein wenig von diesen hier gezeigten unterscheiden: [Roth, Alexander, and Felix Schmidt. "Not only a mild winter: German consumers change their behavior to save natural gas." Joule 7, no. 6 (2023): 1081-1086.](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

??? info "Methodik und Daten"

    === "Methodik"

        Mit einem "Linear Forest", einer Methode des maschinellen Lernens, schätzen wir den Gasverbrauch von Haushalten und Gewerbe auf Basis von Durchschnitts-, Maximal- und Minimaltagestemperaturen für den aktuellen Tag und die jeweils letzten drei Tage. Wir aggregieren die Daten von hunderten Wetterstationen in ganz Deutschland mittels des Medians, um einer Verzerrung durch extreme Datenpunkte, wie beispielsweise von der Zugspitze, vorzubeugen. Zusätzlich kontrollieren wir für den aktuellen Kalendermonat und Wochenend- bzw. Feiertage, um der dem Gasverbrauch inhärenten Saisonalität (über die Temperaturschwankungen hinaus) und unterschiedlichem Verhalten an arbeitsfreien Tagen Rechnung zu tragen.

        Wir trainieren unser Modell auf Basis von täglichen Daten zwischen Juli 2018 und Dezember 2021. Temperaturdaten beziehen wir vom Deutschen Wetterdienst und Gasverbrauchsdaten von Trading Hub Europe. Mithilfe der Periode von Januar 2018 bis Juni 2018 haben wir die Qualität der Vorhersage des Models gegenüber den tatsächlich eingetretenen Verbrauchsständen validiert und so das performanteste Modell ermittelt. Auf diese Weise haben wir eine ganze Reihe von Machine-Learning-Modellen verglichen, u.a. ein neurales Netzwerk, einen LASSO-Schätzer, Linear Trees, Gradient Boosting, Random Forests und Linear Forests. Eine Linear-Forest-Spezifikation wies dabei den geringsten mittleren quadratischen Fehler auf und schnitt somit am Besten ab.

        Wir nutzen das resultierende Modell für die Vorhersage der aktuellen Verbräuche. Es ist nicht eindeutig, ab wann eine Abweichung des tatsächlichen Erdgasverbrauchs von dem auf Basis der vorhergegangenen Jahre geschätzten Gasverbrauch zu erwarten ist. Vergleichbare Modellierungsansätze beginnen mit der Prognose bereits ab September 2021, als die Gaspreise an den europäischen Gashandelsplätzen begannen zu steigen. Ein anderer Ansatz wäre den Startzeitpunkt auf den Beginn des russischen Angriffskrieges gegen die Ukraine zu datieren. Wir betrachten lediglich Haushalte und Gewerbekund\*innen, für welche sich die Entwicklung an den Großhandelsbörsen in der Regel erst mit großer Verzögerung im vertraglichen Arbeitspreis niederschlagen. Daher halten wir es für plausibel, dass ein Einbezug von Trainingsdaten bis Dezember 2021 nicht zu Verzerrungen bei der Vorhersage führt.

        Wir vergleichen unsere Prognose in wöchentlicher Auflösung auf Basis der Verbrauchsdaten der Bundesnetzagentur, da diese aktuellere Daten zu Verfügung stellt als der Trading Hub Europe.

        Mehr Details zur Auswahl und Schätzung des Models sind [hier](https://gitlab.com/diw-evu/oet/openenergytracker/-/blob/main/scripts/static/gas_savings_analysis.ipynb) zu finden.
    
    === "Daten"

        - **Erdgasverbrauch**: [Bundesnetzagentur - Gasverbrauch der Haushalts- und Gewerbekunden in GWh/Tag, wöchentlicher Mittelwert](https://www.bundesnetzagentur.de/DE/Gasversorgung/aktuelle_gasversorgung/_svg/GasverbrauchSLP_woechentlich/Gasverbrauch_SLP_W_2023.html)
        -  **Erdgasverbrauch Vorjahre**: [Trading Hub Europe - Veröffentlichung der aggregierten Verbrauchsdaten - Verbrauchdaten SLP Bereich](https://www.tradinghub.eu/de-de/Ver%C3%B6ffentlichungen/Transparenz/Aggregierte-Verbrauchsdaten)
        - **Temperatur**: [Deutscher Wetterdienst Open Daten Portal](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/historical/); Tägliche Stationsmessungen der mittleren, minimalen und maximalen Lufttemperatur in 2 m Höhe in °C.
