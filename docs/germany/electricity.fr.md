---
hide:
#  - navigation
#  - toc
---

# Électricité renouvelable

=== "Auteurs"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

## :fontawesome-solid-solar-panel: Photovoltaïque

Pour le solaire photovoltaïque, la coalition s'est fixé l'objectif d'atteindre une capacité totale de 215 GW d'ici 2030 dans la [Loi sur les Énergies Renouvelables (EEG)](https://www.gesetze-im-internet.de/eeg_2014/). 

<iframe title = "Figure capacité solaire photovoltaïque installée" src="../../../docs/germany/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Selon [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat), le groupe de travail sur les statistiques des énergies renouvelables, la capacité installée en Allemagne en novembre 2021 était d'environ 60 GW. Atteindre l'objectif supposerait donc d'accroître d'environ 1,4 GW par mois en moyenne la capacité du solaire photovoltaïque d'ici fin 2030 (comprenant les objectifs intermédiaires spécifiés dans la EEG et la [Photovoltaik-Strategie](https://www.bmwk.de/Redaktion/DE/Publikationen/Energie/photovoltaik-stategie-2023.pdf)). Il s'agit du déploiement net requis, c'est-à-dire qu'il tient également compte des anciennes centrales qui seront progressivement retirées du réseau. Ces dernières années, l'expansion a été nettement inférieure à ce chiffre. Si le développement de la capacité photovoltaïque suivait la tendance des années 2017-2021, seulement 92 GW seraient opération d'ici à la fin de 2030. Au cours des douze derniers mois, le rythme d'expansion n'a été que légèrement supérieur, de sorte qu'une poursuite de cette tendance mènerait également à une capacité totale nettement inférieure à l'objectif. Pour atteindre l'objectif de 215 GW en 2030, l'expansion doit ainsi être environ trois fois plus rapide que la tendance des douze derniers mois. Après 2030, la capacité photovoltaïque devrait encore augmenter, pour atteindre 400 GW en 2040.

À titre de comparaison, les [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/) sont également représentés sur le graphique. L'objectif de la coalition pour le photovoltaïque en 2030 se situe dans la partie supérieure du couloir de tous les scénarios Ariadne (ici sans le modèle TIMES). Le résultat du modèle de référence d'Ariadne (REMIND) pour les sources d'énergie renouvelables en 2030 dans le scénario *Mix Technologique* est plus ou moins similaire à la cible de la coalition. Après 2030, la capacité photovoltaïque continue d'augmenter dans les scénarios Ariadne. Pourtant, les 400 GW visés par le gouvernement ne sont atteints que dans certains scénarios, et seulement après 2040.

---

## :material-wind-turbine: Énergie éolienne

### Terrestre

Pour l'éolien terrestre, la coalition gouvernementale allemande vise une capacité installée de 115 GW en 2030 dans la [Loi sur les Énergies Renouvelables (EEG)](https://www.gesetze-im-internet.de/eeg_2014/). Au mois de novembre 2021, la capacité installée en Allemagne était légèrement inférieure à 56 GW, selon [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat). 

<iframe title = "Figure capacité éolienne terrestre installée" src="../../../docs/germany/figures/wind_onshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Pour atteindre l'objectif, il faut donc ajouter en moyenne un bon 0,5 GW par mois (toujours net) d'ici à la fin de 2030. Après 2030, le gouvernement vise à augmenter encore la capacité pour atteindre 160 GW en 2040.

Par rapport aux [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/), les objectifs de la coalition pour l'expansion de l'énergie éolienne terrestre se situent à peu près au milieu du couloir formé par l'ensemble desdits scénarios, qui est plus large que le couloir cible pour le photovoltaïque. Dans le scénario *Mix Technologique*, les résultats du modèle de référence d'Ariadne pour l'expansion de l'éolien se situent systématiquement un peu au-dessus de l'objectif de la coalition.

#### Utilisation des terres

Le gouvernement fédéral s'est également fixé des objectifs en matière d'affectation de surfaces à l'énergie éolienne. La [loi sur l'augmentation et l'accélération du développement des installations d'énergie éolienne à terre](https://dip.bundestag.de/vorgang/gesetz-zur-erh%C3%B6hung-und-beschleunigung-des-ausbaus-von-windenergieanlagen-an/288780) stipule que la moyenne de tous les Länder doit atteindre 1,4 pour cent à la fin de l'année 2027, puis 2 pour cent à la fin de l'année 2032. Des objectifs spécifiques sont fixés pour chaque Land. Les trois villes-États ne doivent consacrer que 0,5 % de leur territoire à l'énergie éolienne d'ici 2032 ; les Länder du sud, la Bavière et le Bade-Wurtemberg, doivent consacrer chacun 1,8 % de leur territoire à l'énergie éolienne, et certains Länder du centre de l'Allemagne, comme la Hesse, la Thuringe et la Saxe-Anhalt, 2,2 %. Dans l'ensemble de l'Allemagne, selon un [rapport](https://www.bmwi.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2021/bericht-bund-laender-kooperationsausschuss-2021.pdf?__blob=publicationFile&v=4) antérieur du comité de coopération entre l'État fédéral et les Länder, seulement 0,70 % de la surface était légalement désignée pour l'énergie éolienne à terre à la fin de l'année 2020 (corridor inférieur sans double comptage, détermination des surfaces soit exclusivement au niveau de l'aménagement du territoire, soit au niveau des plans d'urbanisme). Selon le dernier [rapport](https://www.bmwk.de/Redaktion/DE/Downloads/E/EEG-Kooperationsausschuss/2024/bericht-bund-laender-kooperationsausschuss-2024.pdf?__blob=publicationFile&v=2), cette valeur est passée à 0,92 % d'ici 2023. La désignation de surfaces pour l'énergie éolienne se situe toutefois en dessous d'une trajectoire cible interpolée de manière linéaire.

<iframe title = "Figure zone désignée pour l'éolien terrestre" src="../../../docs/germany/figures/wind_areas.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

---

### En mer

Pour l'éolien offshore, la coalition gouvernementale vise une capacité d'au moins 30 GW en 2030 dans la [Windenergie-auf-See-Gesetz](https://www.gesetze-im-internet.de/windseeg/). Au mois de novembre 2021, la capacité installée dans les eaux allemandes était de 7,8 GW, selon [AGEE-Stat](https://www.umweltbundesamt.de/themen/klima-energie/erneuerbare-energien/erneuerbare-energien-in-zahlen/monats-quartalsdaten-der-agee-stat). 

<iframe title = "Figure capacité éolienne offshore installée" src="../../../docs/germany/figures/wind_offshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Afin d'atteindre l'objectif fixé en 2030, environ 0,2 GW par mois (net) en moyenne doivent être construits. Ensuite, la capacité installée devrait continuer à croître fortement, pour atteindre au moins 40 GW en 2035 et 70 GW en 2045. En raison de la taille des différents projets ou de leurs raccordements au réseau, la trajectoire cible est moins régulière que pour l'énergie éolienne terrestre. Le [Offshore-Flächenentwicklungsplan 2023](https://www.bsh.de/DE/THEMEN/Offshore/Meeresfachplanung/Flaechenentwicklungsplan/_Anlagen/Downloads/FEP_2023_1/Flaechenentwicklungsplan_2023.pdf?__blob=publicationFile&v=1) permet de déduire différentes étapes de développement pour les années 2026-2032 ; lorsque cela n'a pas été possible, une interpolation linéaire a été effectuée dans la figure.

Comparé aux [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/), l'objectif de la coalition pour l'éolien en mer en 2030 se trouve au-dessus de l'intervalle formé par l'ensemble des scénarios Ariadne. Dans le modèle de référence d'Ariadne (REMIND), selon le scénario *Mix Technologique*, la capacité installée en 2030 serait moitié moins élevée que l'objectif de la coalition. L'expansion de l'énergie éolienne en mer dans les scénarios Ariadne ne s'accélère qu'à partir de 2025. En 2045, la capacité installée augmente de manière différente selon le scénario Ariadne considéré et atteint 80 GW dans le cas le plus extrême. Les objectifs du gouvernement fédéral se situent systématiquement dans la fourchette supérieure du couloir des scénarios Ariadne.

---

## Part des EnR dans le secteur de l'électricité

La coalition a pour objectif de faire passer la part des énergies renouvelables dans la consommation brute d'électricité à 80% d'ici 2030. 

<iframe title = "Figure la part des énergies renouvelables dans l'électricité" src="../../../docs/germany/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Les données statistiques relatives à cet indicateur sont fournies chaque année par AGEE-Stat, par exemple [ici](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen). En 2020, la part était de 45,5% ; en 2021, elle n'était plus que de 41,5 %. Cette baisse est probablement due en grande partie à une consommation d'électricité nettement inférieure en 2020 en raison de la pandémie et à une année de vent relativement faible en 2021. Depuis, la proportion a de nouveau augmenté significative. Pour atteindre l'objectif de 2030, cette part doit augmenter de plus de quatre points de pourcentage par an en moyenne à partir de 2021. Après l'achèvement de la sortie du charbon, la [loi sur les énergies renouvelables (EEG)](https://www.gesetze-im-internet.de/eeg_2014/) prévoit une production d'électricité neutre en termes d'émissions de gaz à effet de serre, ce que nous illustrons dans le graphique avec une part d'énergies renouvelables de 95 % à partir de 2035.

En outre, les parts des énergies renouvelables dans la production nette d'électricité peuvent également être affichées sur le graphique (en cliquant sur l'entrée correspondante dans la légende). Ces données sont régulièrement mises à jour sur la plateforme en ligne [Energy-Charts](https://energy-charts.info/charts/renewable_share/chart.htm?l=fr&c=DE&interval=year) du Fraunhofer ISE et sont donc disponibles bien plus tôt que l'indicateur basé sur la consommation mentionné dans l'accord de coalition. Le graphique montre les données disponibles au moment de la dernière mise à jour de ce site web, ce qui signifie que la part peut encore changer de manière significative au cours de l'année actuelle en raison des effets saisonniers (*). Les parts des énergies renouvelables dans la production nette d'électricité et la consommation brute d'électricité diffèrent, entre autres, parce que le dénominateur est plus faible dans le premier cas en raison de la consommation propre des centrales électriques.

À titre de comparaison, l'objectif de la coalition concernant la part des énergies renouvelables en 2030 se situe dans l'intervalle des [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/). Pour 2030, les projections faites d'après le modèle de référence d'Ariadne (REMIND) dépassent l'objectif de la coalition (de 80 %) dans le scénario *Mix Technologique* avec une part de 87 %. Après 2030, la part dépasse 90 % dans la plupart des scénarios Ariadne.