---
hide:
#  - navigation
#  - toc
---

# Greenhouse gas emissions

=== "Authors"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)


!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Sectoral greenhouse gas emissions

The government coalition has repeatedly committed itself to the goal of climate neutrality in 2045. In the short and medium term, however, it has not set any new targets for the reduction of greenhouse gases. Sectoral emission reduction targets are already set by 2030 under the [Federal Climate Protection Act](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&start=//*%5b@attr_id=%27bgbl121s3905.pdf%27%5d#__bgbl__%2F%2F*%5B%40attr_id%3D%27bgbl121s3905.pdf%27%5D__1658147992705), which was last amended on August 18, 2021. Thereafter, the law sets annual greenhouse gas emissions targets without sectoral resolution through 2040, with net greenhouse gas neutrality to be achieved by 2045. The figure shows sectoral emissions since 1990 and medium- and longer-term targets, based on data from the [German Federal Environment Agency (UBA)](https://www.umweltbundesamt.de/themen/klima-energie/treibhausgas-emissionen). In 2020, greenhouse gas emissions were more than 40 percent below those of the 1990 reference year, thus achieving a longer-term climate policy goal. However, the significant pandemic-related reduction in economic activity in 2020 contributed significantly to this. The strong emissions reductions in 2020 also shape the trend for 2017-2021 shown in the figure. Emissions were rising again in 2021 as the economy recovered, but have decreased since then.

<iframe title = "Sectorale greenhouse gas emissions" src="../../../docs/germany/figures/emissions_sect.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## CO<sub>2</sub> emissions of electricity generation

There is no explicit target set by the German government for the CO<sub>2</sub> emissions of electricity generation. The [draft of the EEG 2023](https://dserver.bundestag.de/btd/20/016/2001630.pdf) contained a target of "almost complete greenhouse gas neutrality" of electricity generation by 2035; however, this wording is not included in the final Bundestag resolution. Previously, at the G7 summit in Elmau, it was already [decided](https://www.g7germany.de/resource/blob/974430/2057942/9a53b78596a343132101870daa868f34/2022-06-28-kommunique-g7-zusammenfassung-data.pdf) to "fully or predominantly decarbonize" the power sector by 2035. The figure therefore shows an indicative target value of zero CO<sub>2</sub> emissions in 2035.

The absolute CO<sub>2</sub> emissions (in millions of tons) and the CO<sub>2</sub> emission intensity (in g/kWh, excluding upstream chain emissions) of German electricity generation are calculated annually by the [German Federal Environment Agency (UBA)](https://www.umweltbundesamt.de/publikationen/entwicklung-der-spezifischen-kohlendioxid-10). Emissions roughly halved between 1990 and 2020. However, the values for 2020 were particularly low due to the pandemic. Emissions rose again in 2021 and 2022, initially due to a recovering electricity consumption and then to the increased use of coal-fired power plants in the context of the European electricity price crisis. In 2023, absolute CO<sub>2</sub> emissions in the electricity sector fell to their lowest level since the data was recorded and are currently even below the target pathway.

<iframe title = "CO2 emissions of electricity generation" src="../../../docs/germany/figures/emissions_electr.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>