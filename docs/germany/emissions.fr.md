---
hide:
#  - navigation
#  - toc
---

# Émissions de gaz à effet de serre

=== "Auteurs"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.
	
## Émissions sectorielles de gaz à effet de serre

La coalition gouvernementale s'est engagée à plusieurs reprises à atteindre l'objectif de la neutralité climatique en 2045. Mais à court et moyen terme, elle ne s'est pas fixé de nouveaux objectifs de réduction des gaz à effet de serre. Les objectifs sectoriels de réduction des émissions sont déjà fixés jusqu'en 2030 par la [loi fédérale sur la protection climatique](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&start=//*%5b@attr_id=%27bgbl121s3905.pdf%27%5d#__bgbl__%2F%2F*%5B%40attr_id%3D%27bgbl121s3905.pdf%27%5D__1658147992705), dont la dernière modification en date remonte au 18 août 2021. La loi fixe également des objectifs annuels en matière d'émissions de gaz à effet de serre jusqu'en 2040, sans résolution sectorielle. Par ailleurs, le pays doit atteindre l'objectif de zéro emission nette d'ici 2045. La figure montre les émissions sectorielles depuis 1990 et les objectifs à moyen et long terme, sur la base des données de l'[Agence fédérale de l'environnement (UBA)](https://www.umweltbundesamt.de/themen/klima-energie/treibhausgas-emissionen). En 2020, les émissions de gaz à effet de serre étaient inférieures de plus de 40 % à celles de l'année de référence 1990, une réduction des émissions compatible avec l'objectif de politique climatique de long terme. La nette réduction des activités économiques en 2020 due à la pandémie y a toutefois largement contribué. La forte réduction des émissions en 2020 caractérise également la tendance des années 2017-2021 présentée dans la figure. Dans le cadre de la reprise économique, les émissions ont de nouveau augmenté en 2021, mais elles ont de nouveau diminué depuis.

<iframe title = "Sectorale greenhouse gas emissions" src="../../../docs/germany/figures/emissions_sect.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Émissions de CO<sub>2</sub> de la production d'électricité

Il n'existe pas d'objectif explicite du gouvernement fédéral pour l'intensité des émissions de CO<sub>2</sub> de la production d'électricité. Dans le [projet de loi sur les Énergies Renouvelables (EEG) 2023](https://dserver.bundestag.de/btd/20/016/2001630.pdf), il était question d'une "neutralité quasi-totale en matière de gaz à effet de serre" de la production d'électricité d'ici 2035 ; cette formulation ne figure toutefois pas dans la décision finale du Bundestag. Auparavant, le sommet du G7 à Elmau avait déjà [décidé](https://www.g7germany.de/resource/blob/974430/2057942/9a53b78596a343132101870daa868f34/2022-06-28-kommunique-g7-zusammenfassung-data.pdf) de "décarboniser totalement ou principalement" le secteur de l'électricité d'ici 2035. Le graphique présente donc une valeur cible indicative de zéro émission de CO<sub>2</sub> en 2035.

Les émissions absolues de CO<sub>2</sub> (en millions de tonnes) ainsi que l'intensité des émissions de CO<sub>2</sub> (en g/kWh, sans les émissions en amont) de la production d'électricité allemande sont calculées chaque année par l'[Agence fédérale de l'environnement (UBA)](https://www.umweltbundesamt.de/publikationen/entwicklung-der-spezifischen-kohlendioxid-10). Les émissions ont diminué de moitié environ entre les années 1990 et 2020. Toutefois, les valeurs de l'année 2020 étaient particulièrement faibles en raison de la pandémie. En 2021 et 2022, les émissions ont de nouveau augmenté, d'abord en raison de la reprise de la consommation d'électricité, puis d'un recours accru aux centrales à charbon dans le contexte de la crise européenne des prix de l'électricité. En 2023, les émissions absolues de CO<sub>2</sub> du secteur de l'électricité sont tombées à leur niveau le plus bas depuis la collecte des données et sont même actuellement inférieures à la trajectoire cible.

<iframe title = "CO2 emissions of electricity generation" src="../../../docs/germany/figures/emissions_electr.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
