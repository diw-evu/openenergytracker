---
description: Energy targets in the coalition agreement of the German government vs. where we stand today

hide:
#  - navigation
#  - toc
---

# Germany

=== "Authors"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md)

In their [coalition agreement](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) of November 24, 2021, the parties of the so-called "traffic light" coalition set themselves various quantitative targets for the German energy sector. We have been tracking these here since the end of 2021 - initially under the name **KoaVTracker** (KoaV for coalition agreement, in German "Koalitionsvertrag"). Since then, however, the German government has increasingly set itself additional goals that go beyond the coalition agreement, and we would also like to cover other countries here in the future, so we decided to rename our data platform. Since June 2022, it has been called **Open Energy Tracker**.

!!! info "Ampel-Monitor Energiewende by DIW Berlin, based on the Open Energy Tracker"
    
    Since July 2022, there is a DIW product called "[Ampel-Monitor Energiewende](https://www.diw.de/de/diw_01.c.841560.de/ampel-monitor_energiewende.html)", based on data gathered in the Open Energy Tracker. This includes a dedicated website of DIW Berlin as well as a series of publications.

In the Open Energy Tracker, we graphically depict some of the government targets, most of which are specified for the year 2030, and regularly compare them with the current status actually achieved. We initially focus on targets in the area of renewable energy expansion, electric mobility and electrolysis. In addition, for current reasons, we monitor the gas import situation. For these indicators, updated data is available partly on a monthly basis, but also partly only on an annual basis. For purely illustrative reasons, a linear progression between the current status and the target year (usually December 2030) is usually shown. The exact paths have not been specified by the federal government for most indicators. In addition, we present projections of current trends based on the least squares (OLS) method. 

<center>
<iframe title = "Current status of indicators comparing to 2030 goals" src="../../../docs/germany/figures/fig_reached.html" width="75%" height= "400px" frameBorder="0" loading = "lazy"></iframe>
</center>

For practically all important indicators, there is a large gap between the current status and the government's targets for 2030. This gap is particularly large for installed electrolysis capacity and the stock of battery-electric cars and public charging points. The installed capacity of offshore wind power and photovoltaic power as well as the stock of heat pumps is currently only about a quarter of the targets for 2030.

The coalition's goals and previous trajectories can also be compared with the results of the [Ariadne scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) for some indicators. These are scenario analyses carried out in the BMBF-funded Copernicus project [Ariadne](https://ariadneprojekt.de/) and show different paths to climate neutrality for Germany in 2045. We present both the corridors spanned by all Ariadne scenarios and the results of the respective Ariadne lead model in the so-called *Technology Mix Scenario* (8Gt_Bal). In the Ariadne scenarios, data are generally only available in five-year steps (2020, 2025, 2030, ...). We have interpolated linearly between these base years. The data of the Ariadne scenarios are available until 2045, and thus allow a longer-term view of plausible developments of the respective indicators. Further visualizations and data on all Ariadne scenarios are available on external websites in the [Pathfinder](https://pathfinder.ariadneprojekt.de/) and in the [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne). In the future, we plan to add more indicators and time trends.
