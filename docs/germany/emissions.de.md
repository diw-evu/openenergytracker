---
hide:
#  - navigation
#  - toc
---

# Treibhausgasemissionen

=== "Autor*Innen"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md), [Fernanda Ballesteros](../../docs/about/authors.md)

## Sektorale Treibhausgasemissionen

Die Ampelkoalition hat sich mehrfach zum Ziel der Klimaneutralität im Jahr 2045 bekannt. Kurz- und mittelfristig hat sie sich aber keine neuen Ziele für die Minderung von Treibhausgasen gesetzt. Die sektoralen Emissionsminderungsziele sind bis zum Jahr 2030 bereits durch das zuletzt am 18. August 2021 geänderte [Bundes-Klimaschutzgesetz](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&start=//*%5b@attr_id=%27bgbl121s3905.pdf%27%5d#__bgbl__%2F%2F*%5B%40attr_id%3D%27bgbl121s3905.pdf%27%5D__1658147992705) vorgegeben. Danach legt das Gesetz bis zum Jahr 2040 die jährlichen Ziele für Treibhausgasemissionen ohne sektorale Auflösung fest; bis 2045 soll dann Netto-Treibhausgasneutralität erreicht werden. Die Abbildung zeigt die sektoralen Emissionen seit 1990 und die mittel- und längerfristigen Ziele, basierend auf Daten des [Umweltbundesamts](https://www.umweltbundesamt.de/themen/klima-energie/treibhausgas-emissionen). Im Jahr 2020 lagen die Treibhausgasemissionen mehr als 40 Prozent unter denen des Referenzjahres 1990, womit ein längerfristiges klimapolitisches Ziel erreicht wurde. Hierzu trug allerdings die deutliche pandemiebedingte Verminderung der wirtschaftlichen Aktivitäten im Jahr 2020 erheblich bei. Die starken Emissionsminderungen des Jahres 2020 prägen auch den in der Abbildung ausgewiesenen Trend der Jahre 2017-2021. Im Zuge der wirtschaftlichen Erholung stiegen die Emissionen im Jahr 2021 wieder an, seitdem sanken sie wieder.

<iframe title = "Sektorale Treibhausgasemissionen" src="../../../docs/germany/figures/emissions_sect.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Für einen weiteren Blick in die Zukunft über das Jahr 2030 hinaus, können Klimaneutralitätsstudien herangezogen werden, die die Realisierbarkeit von Klimaneutralität im Jahr 2045 für Deutschland untersuchen. Die untenstehende Grafik zeigt die Spannweite sowie den Durchschnitt der Studien von Agora, Ariadne, BDI sowie dena. Gemäß der Szenarien müssen die meisten Treibhausgasemissionsreduktionen bis 2045 in den Sektoren Energie, Verkehr, Industrie und Gebäude erfolgen. Zudem ist die Spannweite der Modelle in den Sektoren Energie, Industrie und Verkehr am größten, während Szenarien zu ähnlicheren Emissionswerten etwa für den Gebäudesektor kommen. Zu beachten ist, dass nicht alle Studien für die hier ausgewählten Jahre und Sektoren Emissionswerte zur Verfügung stellen. Fehlende Jahreswerte für die einzelnen Sekoten wurden nicht interpoliert, sodass sowohl die Range als auch der Durchschnitt teilweise "Knicke" aufweisen, da für bestimmte Jahre nicht alle Modelle mit in die Range bzw. den Durschnitt einfließen. Die Werte des Klimaschutzgesetzes sind als Vergleich auch in der Abbildung einblendbar (KSG).

<iframe title = "Sektorale Treibhausgasemissionen" src="../../../docs/germany/figures/emissions_scenarios.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## CO<sub>2</sub>-Emissionen der Stromerzeugung

Für die CO<sub>2</sub>-Emissionen der Stromerzeugung gibt es kein explizites Ziel der Bundesregierung. Im [Entwurf des EEG 2023](https://dserver.bundestag.de/btd/20/016/2001630.pdf) war von einer "nahezu vollständigen Treibhausgasneutralität" der Stromerzeugung bis zum Jahr 2035 die Rede; diese Formulierung ist im finalen Bundestagsbeschluss allerdings nicht enthalten. Zuvor wurde auf dem G7-Gipfel in Elmau bereits [beschlossen](https://www.g7germany.de/resource/blob/974430/2057942/9a53b78596a343132101870daa868f34/2022-06-28-kommunique-g7-zusammenfassung-data.pdf), den Stromsektor bis 2035 "vollständig oder überwiegend zu dekarbonisieren". In der Abbildung ist daher ein indikativer Zielwert von Null CO<sub>2</sub>-Emissionen im Jahr 2035 dargestellt.

Die absoluten CO<sub>2</sub>-Emissionen (in Millionen Tonnen) sowie die CO<sub>2</sub>-Emissionsintensität (in g/kWh, ohne Vorketten-Emissionen) der deutschen Stromerzeugung werden jährlich vom [Umweltbundesamt](https://www.umweltbundesamt.de/publikationen/entwicklung-der-spezifischen-kohlendioxid-10) berechnet. Die Emissionen haben sich zwischen den Jahren 1990 und 2020 ungefähr halbiert. Allerdings waren die Werte des Jahres 2020 pandemiebedingt besonders niedrig. In den Jahren 2021 und 2022 stiegen die Emissionen wieder an, was zunächst am wieder anziehenden Stromverbrauch und dann an einer verstärkten Nutzung von Kohlekraftwerken im Kontext der europäischen Strompreiskrise lag. Im Jahr 2023 sind die absoluten CO<sub>2</sub>-Emissionen des Stromsektors auf den tiefsten Wert seit Erfassung der Daten gesunken und liegen aktuell sogar unterhalb des Zielpfades.

<iframe title = "CO2-Emissionen der Stromerzeugung" src="../../../docs/germany/figures/emissions_electr.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>