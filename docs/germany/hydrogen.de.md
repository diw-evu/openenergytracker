---
hide:
#  - navigation
#  - toc
---

# Wasserstoff

=== "Autor*Innen"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

Im Koalitionsvertrag wird "eine Elektrolysekapazität von rund 10 Gigawatt im Jahr 2030" als Ziel genannt. Laut [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database) waren Anfang Oktober 2021 in Deutschland Elektrolyseure mit einer elektrischen Leistung von gut 60 Megawatt in (Demonstrations-)Betrieb. Zur Erreichung des Ziels müssen bis Ende 2030 im Durchschnitt etwa 90 Megawatt pro Monat zugebaut werden. Bis heute stieg die installierte Leistung laut Daten des [Elektrolyse-Monitors](https://www.wasserstoff-kompass.de/) kaum. Wenn man eine große Chlor-Alkali-Anlage in Bayern herausrechnet, bei der Wasserstoff eher ein Nebenprodukt ist, liegt die elektrische Leistung aller Elektrolyseure in Deutschland erst etwas über 100 MW, also bei rund einem Prozent des Ziels für das Jahr 2030.

Die bereits in Betrieb befindliche Elektrolyseleistung ist zwar noch sehr klein, aber es gibt eine Vielzahl von Projekten, die in den nächsten Jahren entwickelt werden sollen. Diese sind in der folgenden Abbildung dargestellt. Laut [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database) sind derzeit weitere Elektrolyseure mit einer elektrischen Leistung von über einem Gigawatt im Bau oder im Bereich der finalen Investitionsentscheidung (FID). Hinzu kommen mehr als 7 Gigawatt, für die Machbarkeitsstudien laufen. Darüber hinaus gibt es (weniger konkrete) Konzepte für weitere Projekte mit einer Leistung von knapp 14 Gigawatt. Dabei wird nicht bei allen Projekten ein konkretes Jahr der geplanten Inbetriebnahme angegeben (vgl. Säule rechts). Um das Ziel von 10 Gigawatt im Jahr 2030 zu erreichen, müssten nicht nur alle Projekte mit konkretem Startdatum, die derzeit mindestens auf Ebene der Machbarkeitsstudien sind, auch tatsächlich verwirklicht werden, sondern auch noch ein guter Teil der weniger ausgereiften konzeptionellen Projekte.

<iframe title = "Abbildung kumulierte Kapazität von Wasserstoffprojekten in Deutschland nach Status" src="../../../docs/germany/figures/iea_h2.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
