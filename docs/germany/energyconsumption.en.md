---
hide:
#  - navigation
#  - toc
---

# Energy Consumption

=== "Authors"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Fossil energy primary consumption

The coalition agreement mentions the goal of climate neutrality by 2045 in several places; in addition, the energy infrastructure is to be allowed to run only on non-fossil fuels beyond 2045. From goals, it can be deduced that the coalition is aiming to entirely end the consumption fossil energy sources by 2045. However, a precise time path is not specified for this target. According to data from the [BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html) and the [AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/), consumption of mineral oil, lignite and hard coal has declined significantly in recent years. In contrast, the consumption of natural gas has hardly decreased, with significant annual fluctuations. In 2021, fossil primary energy consumption totaled 2660 TWh, with mineral oil still accounting for the largest share. In 2020, consumption was even lower, caused by the pandemic. Assuming a linear reduction path, fossil primary energy consumption would have to fall by just under 40 percent to below 1700 TWh by 2030. Consumption actually fell in 2022-2024 and is currently slightly below the indicative linear reduction path.

<iframe title = "Fossil primary energy consumption" src="../../../docs/germany/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

While the BMWK data is only updated annually, the [AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/) also provides quarterly updated data on primary energy consumption for the current year. In 2024, fossil primary energy consumption was almost the same as in the previous year at 2249 TWh. Natural gas consumption even increased slightly by three percent. The consumption of hard coal and lignite fell by 12% and 11% respectively compared to the previous year and is now at its lowest level for decades. The reason for this is the significant drop in coal-fired power generation. In contrast, the consumption of mineral oil fell by only 1%.

<iframe title = "Fossil primary energy consumption 3 months" src="../../../docs/germany/figures/fig_pec_3month.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
