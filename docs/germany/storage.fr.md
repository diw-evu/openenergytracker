---
hide:
#  - navigation
#  - toc
---

# Stockage d'énergie

=== "Auteurs"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

!!! Warning "Page en construction"
    
    * Pour l'instant, cette page ne contient que des accumulateurs à batterie dont les données ne sont pas tout à fait à jour.
    * Les données seront actualisées en temps voulu et complétées par d'autres types de stockage d'énergie.

## :fontawesome-solid-battery-full: Stockage sur batterie

### Capacité de stockage

Le tableau ci-dessous présente le stock total et les installations mensuelles de stockage d'énergie par batterie en Allemagne. La base de données est le registre des données de base du marché. Le traitement et la catégorisation en accumulateurs domestiques, commerciaux et de grande capacité suivent en grande partie la méthodologie de [Battery Charts der RWTH Aachen](https://battery-charts.rwth-aachen.de/). Les accumulateurs domestiques sont actuellement surtout utilisés pour optimiser l'autoconsommation des installations PV (prosumers), mais pas pour l'arbitrage sur le marché de l'électricité. Il devrait en être de même pour les grands systèmes de stockage professionnels. Parmi les grands accumulateurs, une partie importante devrait être active sur le marché de la puissance de réglage. La capacité de stockage domestique a récemment connu une forte croissance, parallèlement à la forte croissance du photovoltaïque sur toiture (voir [Deep dive photovoltaics](../germany/background/pv.en.md)).

<iframe title = "Abbildung installierte Speicherenergie" src="../../../docs/germany/figures/battery_energy.html" width="100%" height="650px" frameBorder="0" loading = "lazy"></iframe>

---

### Puissance de stockage 

La situation de la puissance de stockage est similaire à celle de la capacité de stockage.

<iframe title = "Abbildung installierte Speicherleistung" src="../../../docs/germany/figures/battery_power.html" width="100%" height= "650px" frameBorder="0" loading = "lazy"></iframe>

