---
hide:
#  - navigation
#  - toc
---

# Consommation d'énergie

=== "Auteurs"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)
	
!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Consommation d'énergie primaire d'origine fossile

L'accord de coalition mentionne à plusieurs reprises l'objectif de la neutralité climatique d'ici 2045. Il est par ailleurs fait mention du fait que l'infrastructure énergétique ne devra plus être alimentée qu'avec des combustibles non fossiles après 2045. On peut donc en déduire que la coalition vise un arrêt complet de l'utilisation des énergies primaires fossiles d'ici 2045. Cependant, aucune échéance précise n'est spécifiée. Selon les données du ministère fédéral de l'Économie et de la Protection du Climat (en allemand, [Bundesministerium für Wirtschaft und Klimaschutz (BMWK)](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html)) et du groupe d'études sur le bilan énergétique (en allemand, [Arbeitsgemeinschat Energie Bilanz (AGEB)](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/)), la consommation de pétrole, de lignite et de charbon a nettement diminué en moyenne au cours des dernières années. En revanche, la consommation de gaz naturel n'a guère diminué, avec des variations annuelles significatives. Au début de la coalition en 2021, la consommation totale d'énergie primaire fossile s'élevait à 2660 TWh, le pétrole continuant à représenter la plus grande part. Dans l'hypothèse d'une trajectoire de réduction linéaire, la consommation d'énergie primaire fossile devrait diminuer de près de 40 % d'ici 2030 pour passer sous la barre des 1700 TWh. En 2022-2024, la consommation a effectivement diminué et elle est actuellement légèrement inférieure à la trajectoire indicative de réduction linéaire.

<iframe title = "Consommation d'énergie primaire" src="../../../docs/germany/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Alors que les données du BMWK ne sont actualisées qu'une fois par an, l'[AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/) met également à disposition des données trimestrielles actualisées sur la consommation d'énergie primaire de l'année en cours. En 2024, la consommation d'énergie primaire fossile était presque aussi élevée que l'année précédente, avec 2249 TWh. La consommation de gaz naturel a même légèrement augmenté de trois pour cent. La consommation de charbon et de lignite a diminué de 12 et 11 pour cent par rapport à l'année précédente et se situe désormais à son niveau le plus bas depuis des décennies. Cela s'explique par la nette diminution de la production d'électricité à partir du charbon. En revanche, la consommation de pétrole n'a diminué que de 1 %.

<iframe title = "Fossiler Primärenergieverbrauch 3 Monate" src="../../../docs/germany/figures/fig_pec_3month.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
