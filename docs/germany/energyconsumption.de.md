---
hide:
#  - navigation
#  - toc
---

# Energieverbrauch

=== "Autor*Innen"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Fossiler Primärenergieverbrauch

Im Koalitionsvertrag wird an mehreren Stellen das Ziel der Klimaneutralität bis zum Jahr 2045 genannt; zudem soll die Energieinfrastruktur über das Jahr 2045 hinaus nur noch mit nicht-fossilen Brennstoffen betrieben werden dürfen. Daraus lässt sich ableiten, dass die Ampel-Koalition eine vollständige Beendigung der Nutzung fossiler Primärenergieträger bis zum Jahr 2045 avisiert. Ein genauer Zeitpfad ist hierfür jedoch nicht spezifiziert. Daten des [BMWK](https://www.bmwi.de/Redaktion/DE/Artikel/Energie/energiedaten-gesamtausgabe.html) sowie der [AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/) zufolge war der Verbrauch von Mineralöl, Braunkohle und Steinkohle im Trend der letzten Jahre deutlich rückläufig. Dagegen hat sich der Verbrauch von Erdgas kaum verringert, wobei es deutliche jährliche Schwankungen gab. Zum Start der Ampel-Koalition im Jahr 2021 betrug der Verbrauch fossiler Primärenergie insgesamt 2661 TWh, wobei Mineralöl nach wie vor den größten Anteil hatte. Im Jahr 2020 lag er pandemiebedingt noch etwas niedriger. Unter der Annahme eines linearen Reduktionspfads bis 2045 müsste der fossile Primärenergieverbrauch bis zum Jahr 2030 um knapp 40 Prozent auf unter 1700 TWh sinken. In den Jahren 2022-2024 ging der Verbrauch jeweils zurück, und er liegt aktuell leicht unter dem indikativen linearen Reduktionspfad.

<iframe title = "Fossiler Primärenergieverbrauch" src="../../../docs/germany/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Während die Daten des BMWK nur jährlich aktualisiert werden, stellt die [AGEB](https://ag-energiebilanzen.de/daten-und-fakten/primaerenergieverbrauch/) auch quartalsweise aktualisierte Daten zum Primärenergieverbrauch des jeweils laufenden Jahres bereit. Im Jahr 2024 war der fossile Primärenergieverbrauch mit 2249 TWh fast gleich hoch wie im Vorjahr. Dabei hat der Erdgasverbrauch sogar leicht um drei Prozent zugenommen. Der Verbrauch von Stein- bzw. Braunkohle ging gegenüber dem Vorjahr um 12 bzw. 11 Prozent zurück und liegt jetzt auf dem niedrigsten Niveau seit Jahrzehnten. Grund hierfür ist die deutlich gesunkene Kohleverstromung. Dagegen ging der Verbrauch von Mineralöl um nur 1 Prozent zurück.

<iframe title = "Fossiler Primärenergieverbrauch 3 Monate" src="../../../docs/germany/figures/fig_pec_3month.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
