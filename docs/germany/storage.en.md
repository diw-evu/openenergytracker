---
hide:
#  - navigation
#  - toc
---

# Energy storage

=== "Authors"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

!!! Warning "Page under construction"
    
    * So far this page only contains battery storage with slightly outdated data status.
    * The data will be updated soon and other types of energy storage systems will be added.
    
## :fontawesome-solid-battery-full: Battery storage

### Storage energy

The total stock and monthly additions of battery storage systems in Germany are shown below. The data is based on the Federal Network Agency's market master data register. The preparation and categorization into home, industrial and large-scale storage systems largely follows the methodology of the [Battery Charts of RWTH Aachen University](https://battery-charts.rwth-aachen.de/). Home storage systems are currently mainly used to optimize the self-consumption of PV systems (prosumers), but not for arbitrage in the electricity market. The same is likely to apply to larger industrial storage systems. A significant proportion of large-scale storage systems are likely to be active in the balancing power market. Home storage capacity has recently grown strongly in parallel with the strong growth in rooftop photovoltaics (see [Deep dive photovoltaics](../germany/background/pv.en.md)).

<iframe title = "Abbildung installierte Speicherenergie" src="../../../docs/germany/figures/battery_energy.html" width="100%" height="650px" frameBorder="0" loading = "lazy"></iframe>

---

### Storage power

The picture for storage power is similar to that for storage capacity.

<iframe title = "Abbildung installierte Speicherleistung" src="../../../docs/germany/figures/battery_power.html" width="100%" height= "650px" frameBorder="0" loading = "lazy"></iframe>

