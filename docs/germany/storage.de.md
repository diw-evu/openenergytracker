---
hide:
#  - navigation
#  - toc
---

# Energiespeicher

=== "Autor*Innen"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

!!! Warning "Seite im Aufbau"
    
	* Bisher enthält diese Seite nur Batteriespeicher mit einem nicht ganz aktuellen Datenstand.
    * Die Daten werden zeitnah aktualisiert und um andere Arten von Energiespeichern ergänzt.

## :fontawesome-solid-battery-full: Batteriespeicher

### Speicherkapazität

Im Folgenden sind der Gesamtbestand und die monatlichen Zubauten von Batteriespeichern in Deutschland dargestellt. Datengrundlage ist das Marktstammdatenregister der Bundesnetzagentur. Die Aufbereitung und Kategorisierung in Heim-, Gewerbe- und Großsspeicher folgt weitgehend der Methodik der [Battery Charts der RWTH Aachen](https://battery-charts.rwth-aachen.de/). Heimspeicher werden derzeit vor allem zur Optimierung des Eigenverbrauchs von PV-Anlagen (Prosumer) verwendet, jedoch nicht für Arbitrage im Strommarkt. Ähnliches dürfte für die größeren Gewerbespeicher gelten. Von den Großspeicher dürfte ein erheblicher Teil im Regelleistungsmarkt aktiv sein. Die Heimspeicher-Kapazität ist zuletzt parallel zum starken Wachstum der Aufdach-Photovoltaik (vgl. [Hintergrund zur Photovoltaik](../germany/background/pv.de.md)) stark gewachsen.

<iframe title = "Abbildung installierte Speicherenergie" src="../../../docs/germany/figures/battery_energy.html" width="100%" height="650px" frameBorder="0" loading = "lazy"></iframe>

---

### Speicherleistung

Bei der Speicherleistung zeigt sich ein ähnliches Bild wie bei der Speicherkapazität.

<iframe title = "Abbildung installierte Speicherleistung" src="../../../docs/germany/figures/battery_power.html" width="100%" height= "650px" frameBorder="0" loading = "lazy"></iframe>

