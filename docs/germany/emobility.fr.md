---
hide:
#  - navigation
#  - toc
---

# Électromobilité

=== "Auteurs"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)

## Transport routier

### Voitures particulières électriques à batterie

La coalition gouvernementale a fixé un objectif d'au moins 15 millions de voitures particulières entièrement électriques d'ici 2030, que nous assimilons ici aux véhicules purement électriques à batterie (i.e. nous négligeons les véhicules électriques à hydrogène et les véhicules hybrides). Fin novembre 2021, on comptait près de 0,6 million de voitures particulières de ce type en Allemagne, selon l'[Office fédéral des véhicules à moteur](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html) (*Kraftfahrt-Bundesamt*). Pour atteindre l'objectif, il faut ajouter en moyenne 0,13 million de véhicules par mois d'ici 2030. Une progression linéaire est représentée dans la figure, mais elle a un caractère purement illustratif. En réalité, il faut plutôt s'attendre à une évolution logistique. Sur la base d'une [étude du Fraunhofer ISI](https://www.isi.fraunhofer.de/content/dam/isi/dokumente/sustainability-innovation/2022/WP05-2022_Markthochlaufszenarien_E-Fahrzeuge_GNT-final.pdf), il est possible de déduire une trajectoire logistique permettant d'atteindre exactement 15 millions de voitures électriques en 2030 (merci à [Patrick Plötz](https://twitter.com/PatrickPlotz) et à ses collègues). Selon cette étude, l'évolution actuelle du parc automobile n'est que légèrement inférieure à la trajectoire visée. Remarque : en plus des nouvelles immatriculations, le KBA publie tous les trois mois les données effectives du parc (y compris les radiations intervenues entre-temps). Au cours des deux mois intermédiaires, les radiations sont estimées sur la base des derniers rapports disponibles entre les nouvelles immatriculations et les radiations. Par conséquent, les données des derniers mois peuvent encore être légèrement modifiées.

À titre de comparaison, les [scénarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) du [projet Ariadne](https://ariadneprojekt.de/) peuvent également être représentés sur la figure. L'objectif de la coalition, à savoir 15 millions de voitures particulières entièrement électriques en 2030, se situe dans l'intervalle formé par tous les scénarios Ariadne. L'objectif de la coalition n'est que légèrement inférieur au résultat du scénario *Mix Technologique* du modèle de référence de mobilité d'Ariadne (VECTOR21). En 2045, le parc automobile atteint près de 42 millions de véhicules électriques dans le modèle de référence. Il convient de noter que les scénarios d'Ariadne comprennent également des véhicules électriques hybrides rechargeables, et pas seulement des véhicules entièrement électriques comme le prévoit l'objectif de la coalition.

<iframe title = "Figure stock de véhicules électriques à batterie" src="../../../docs/germany/figures/bev.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

En complément, nous montrons dans le graphique suivant la part des voitures purement électriques à batterie et des véhicules hybrides rechargeables dans les immatriculations mensuelles de nouvelles voitures. Bien que la coalition n'ait pas formulé d'objectif spécifique à ce sujet, cet indicateur illustre mieux la dynamique à l'œuvre que l'indicateur de stock présenté ci-dessus. Au début de l'année, il y a eu plusieurs ruptures en raison de l'évolution des conditions-cadres dans la réglementation et les mesures de financement. Cette situation était particulièrement marquée fin 2022 en raison des réductions du [*Umweltbonus*](https://www.bafa.de/DE/Energie/Energieeffizienz/Elektromobilitaet/Neuen_Antrag_stellen/neuen_antrag_stellen.html).

<iframe title = "Figure nouvelles immatriculations mensuelles de voitures" src="../../../docs/germany/figures/bev_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Parc total de voitures

Contrairement au développement du parc de voitures entièrement électriques, la coalition gouvernementale ne s'est pas fixé d'objectif de réduction du parc de voitures à moteur thermique. Le graphique suivant montre l'évolution de l'ensemble du parc de voitures particulières en Allemagne par type de propulsion ou de carburant. Les données sont fournies par le Kraftfahrt-Bundesamt, de 2013 à fin 2017 sous forme de [données annuelles](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Jahrebilanz_Bestand/fz_b_jahresbilanz_node.html) et à partir de mi-2018 sous forme de [données trimestrielles](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Vierteljaehrlicher_Bestand/viertelj%C3%A4hrlicher_bestand_node.html). Au total, la flotte continue de croître pour atteindre récemment près de 49 millions de voitures particulières. Depuis le début de l'année 2020, le nombre de voitures à essence ou diesel diminue légèrement, mais elles représentent toujours plus de 90 % du parc de voitures. Les voitures à pile à combustible ne jouent pratiquement aucun rôle dans la flotte totale, avec seulement environ 2000 véhicules (0,004 %).

<iframe title = "Figure parc total de voitures" src="../../../docs/germany/figures/cars_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Bornes de chargement

La coalition en feu tricolore a fixé l'objectif d'un million de bornes de chargement publiquement accessibles d'ici 2030, en mettant l'accent sur les bornes de recharge rapide. Au 1er decembre 2021, selon la [Agence fédérale du réseau électrique](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html) (*Bundesnetzagentur*), environ 54 000 bornes de recharge étaient en service, dont 46 000 bornes de chargement normal et 8 000 bornes de chargement rapide. Pour atteindre l'objectif, environ 8 700 nouvelles bornes de chargement doivent être mises en service en moyenne chaque mois jusqu'en 2030. La figure présente une courbe linéaire correspondante. Un chemin logistique est également disponible, qui suit l'évolution logique du parc de voitures électriques à batterie représentée ci-dessus.

<iframe title = "Figure infrastructure de recharge" src="../../../docs/germany/figures/cs.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

En complément, la figure suivante présente l'évolution de la puissance de charge disponible aux points de charge publics. Le gouvernement n'a pas encore formulé d'objectifs explicites à ce sujet. La puissance de charge totale installée a récemment fortement augmenté, principalement en raison de la croissance des points de charge rapide. La puissance de charge moyenne par point de charge a également augmenté, mais beaucoup plus lentement.

<iframe title = "Illustration de la puissance de charge" src="../../../docs/germany/figures/fig_cs_capa.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>.

Le graphique suivant montre le rapport entre les voitures électriques à batterie et les points de recharge accessibles au public. Le gouvernement n'a pas formulé d'objectif explicite à ce sujet. Les objectifs en matière de parc automobile et de bornes de recharge pour 2030 donnent une valeur de 15 voitures électriques à batterie par borne de recharge, la répartition entre bornes de recharge rapide et normale n'étant pas claire. C'est surtout le nombre de véhicules électriques qui se partagent mathématiquement un point de recharge rapide accessible au public qui a nettement augmenté depuis 2020. La figure montre en outre la puissance de charge installée moyenne par voiture électrique. Elle a nettement baissé depuis 2020.

<iframe title = "Figure BEV per point de recharge" src="../../../docs/germany/figures/bev_per_cp.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

## Transport ferroviaire

### Électrification du réseau

La coalition gouvernementale prévoit d'électrifier 75 % du réseau ferroviaire allemand d'ici 2030. Selon la [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html), 61,7 % du réseau ferroviaire appartenant au gouvernement fédéral était électrifié en 2020. Pour atteindre l'objectif, cette part doit augmenter en moyenne de près de 1,5 point de pourcentage par an jusqu'en 2030. Actuellement, la vitesse d'électrification est nettement plus faible.

<iframe title = "Figure électrification du réseau ferroviaire" src="../../../docs/germany/figures/electrification.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>
