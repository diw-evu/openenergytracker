---
hide:
#  - navigation
#  - toc
---

# Renewable heat

=== "Authors"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md)

## Heat pumps

Heat pumps can be used to harness environmental heat. They play a major role in many future scenarios, especially for space heating. The German government did not mention a target for the expansion of heat pumps in its coalition agreement - the term "heat pump" does not appear a single time in the entire document. However, the [*Eröffnungsbilanz Klimaschutz*](https://www.bmwk.de/Redaktion/EN/Downloads/E/germany-s-current-climate-action-status.html) of the Federal Ministry for Economic Affairs and Climate Action (BMWK) mentions a corridor of "4.1 to 6 million heat pumps" in 2030. This figure is likely to refer to the provision of space heating in individual buildings (excluding large-scale heat pumps in heating networks and high-temperature heat pumps). In the context of the [2nd Heat Pump Summit in November 2022](https://www.bmwk.de/Redaktion/DE/Downloads/Energie/2-waermepumpen-gipfel-eckpunktepapier.html), the BMWK then stated a target of around six million heat pumps in 2030 and an addition of at least 500,000 heat pumps per year each from 2024. There is no monthly updated public data on the number of heating heat pumps in Germany. We use [AGEE-Stat](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Entwicklung/entwicklung-der-erneuerbaren-energien-in-deutschland.html) as a data source up to the end of 2022 and figures from the [BDH](https://www.bdh-industrie.de/start/marktdaten) for the current year. The expansion of heat pumps is currently lagging far behind the target path.

For comparison, [scenarios](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) of the [Ariadne project](https://ariadneprojekt.de/) can also be added to the figure. The lead model for heat pumps (REMod) identifies a stock of 5.3 million heat pumps in 2030 (in individual buildings) in the *Technology mix scenario*, which is below the government target. By 2045, this heat pump inventory grows to about 15 million. In addition, the heat pump expansion paths of four other studies can be compared, which together with the Ariadne scenarios have been dubbed the "Big 5" scenarios, namely "Climate Neutral Germany 2045" by [Agora Energiewende](https://www.agora-energiewende.de/en/publications/towards-a-climate-neutral-germany-2045-executive-summary/), "Climate Paths 2.0" (scenario Zielpfad) by [BDI](https://english.bdi.eu/publication/news/climate-paths-2-0-a-program-for-climate-and-germanys-future-development/), "Towards climate neutrality" (*Leitstudie Aufbruch Klimaneutralität*, scenario KN100) by [Dena](https://www.dena.de/en/newsroom/news/dena-pilot-study-towards-climate-neutrality/), and "Long-term scenarios 3" (scenario TN-Strom) by [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/). Up to 2045, heat pump additions are particularly high in the BMWK long-term scenarios, and lowest in the Dena lead study.

<iframe title = "Abbildung Wärmepumpen" src="../../../docs/germany/figures/hp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Shares in the heating sector

The coalition aims for a "very high" share of renewables in the heating sector. Yet, a quantitative target is only provided for "climate neutral" heat in the coalition agreement, which should reach 50 percent of the total heat supply by 2030. As other options, such as imported climate-neutral hydrogen or carbon capture and storage appear to be implausible in the German heating sector by 2030, we interpret the quantiative target in fact as a renewable energy target, slightly deviating from the exact wording of the coalition agreement. 

In 2021, the share of renewable energies in final energy consumption for heating and cooling (including district heating consumption) amounted to 15.7% according to [AGEE-Stat](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen) data. By 2030, this share must therefore increase by almost four percentage points per year and thus much more than in previous years in order to achieve the target. However, development is currently well below this target path.

<iframe title = "Figure share of renewables in heat" src="../../../docs/germany/figures/resshare_heat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
