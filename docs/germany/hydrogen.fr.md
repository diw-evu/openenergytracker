---
hide:
#  - navigation
#  - toc
---

# Hydrogène

=== "Auteurs"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

Le contrat de coalition mentionne comme objectif "une capacité d'électrolyse d'environ 10 gigawatts en 2030". Selon [la base de données de l'AIE sur les projets hydrogène](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database) (*IEA Hydrogen Projects Database*), début octobre 2021, des électrolyseurs d'une puissance électrique de 61 mégawatts étaient en service en Allemagne. Pour atteindre l'objectif, il faudra ajouter environ 90 mégawatts par mois en moyenne d'ici fin 2030. Jusqu'à présent, la puissance installée n'a guère augmenté selon les données du [Elektrolyse-Monitor](https://www.wasserstoff-kompass.de/). Si l'on exclut une grande usine de chlore et de soude en Bavière, dont l'hydrogène est plutôt un sous-produit, la puissance électrique de tous les électrolyseurs en Allemagne est toujours inférieure à 100 MW, soit moins d'un pour cent de l'objectif fixé pour 2030.

La capacité d'électrolyse déjà en service est certes encore très faible. Ceux-ci sont représentés dans la figure ci-dessous. Selon [IEA Hydrogen Projects Database](https://www.iea.org/data-and-statistics/data-product/hydrogen-projects-database), d'autres électrolyseurs d'une puissance électrique de près de 500 mégawatts sont actuellement en construction ou font l'objet d'une décision d'investissement finale (FID). S'y ajoutent 8,3 gigawatts supplémentaires pour lesquels des études de faisabilité sont en cours. En outre, il existe des concepts (moins concrets) pour d'autres projets d'une puissance totale de 13,2 gigawatts. Pour tous les projets, l'année concrète de mise en service prévue n'est pas indiquée (voir colonne de droite). Pour atteindre l'objectif de 10 gigawatts en 2030, il faudrait non seulement que tous les projets avec une date de lancement concrète, qui se trouvent actuellement au moins au niveau des études de faisabilité, soient effectivement réalisés, mais aussi qu'une bonne partie des projets conceptuels moins aboutis le soient également.

<iframe title = "Figure Capacité cumulée des projets d'hydrogène en Allemagne par statut" src="../../../docs/germany/figures/iea_h2.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>