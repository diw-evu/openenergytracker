---
hide:
#  - navigation
#  - toc
---

# Elektromobilität

=== "Autor*Innen"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Nicolas Aichner](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md), [Alexander Roth](../../docs/about/authors.md)
	
## Pkw

### Batterieelektrische Pkw

Die Ampel-Regierungs hat sich ein Ziel von "mindestens 15 Millionen vollelektrische(n) Pkw bis 2030" gesetzt, was wir hier mit rein batterieelektrischen Fahrzeugen gleichsetzen. Ende November 2021 gab es laut [Kraftfahrt-Bundesamt (KBA)](https://kba.de/DE/Statistik/Fahrzeuge/fahrzeuge_node.html) knapp 0,6 Millionen solcher Pkw in Deutschland. Zur Erreichung des Ziels müssen bis zum Jahr 2030 im Durchschnitt 0,13 Millionen Fahrzeuge pro Monat hinzu kommen. Ein solcher linearer Verlauf ist in der Abbildung dargestellt, hat aber nur illustrativen Charakter. In Wirklichkeit ist eher mit einem logistischen Verlauf zu rechnen. Auf Basis einer [Studie des Fraunhofer ISI](https://www.isi.fraunhofer.de/content/dam/isi/dokumente/sustainability-innovation/2022/WP05-2022_Markthochlaufszenarien_E-Fahrzeuge_GNT-final.pdf) lässt sich ein solcher Pfad ableiten, mit dem genau 15 Millionen Elektro-Pkw im Jahr 2030 erreicht werden (Dank an [Patrick Plötz](https://twitter.com/PatrickPlotz) und Kollegen). Demnach liegt die aktuelle Bestandsentwicklung deutlich unter diesem Zielpfad. Hinweis: das KBA veröffentlicht neben den Neuzulassungen alle drei Monate auch die tatsächlichen Bestandsdaten (inklusive zwischenzeitlich erfolgter Abmeldungen). In den jeweils dazwischen liegenden beiden Monaten werden die Abmeldungen auf Basis der jeweils zuletzt vorliegenden Verhältnisse von Neuzulassungen und Abmeldungen abgeschätzt. Demnach können sich die Daten der letzten Monate teils noch leicht ändern.

Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Ziel der Koalition für vollelektrische Pkw im Jahr 2030 liegt mitten in dem von allen Ariadne-Szenarien aufgespannten Korridor. Das Ariadne-Leitmodell für Mobilität, VECTOR21, liegt im *Technologiemix-Szenario* nur knapp unter dem Ziel der Koalition. Bis zum Jahr 2045 wächst die Flotte im Leitmodell auf knapp 42 Millionen Fahrzeuge an. Zu beachten ist hier jedoch, dass die gezeigten Ariadne-Szenarien auch einen kleineren Anteil Plug-in-Hybridfahrzeuge beinhalten, also nicht nur vollelektrische Fahrzeuge wie beim formulierten Koalitionsziel.

<iframe title = "Abbildung Bestand batterieelektrische Pkw" src="../../../docs/germany/figures/bev.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Ergänzend zeigen wir in der folgenden Abbildung die Anteile von rein batterieelektrischen Pkw sowie von Plug-in-Hybriden an den monatlichen Neuzulassungen. Hierfür hat die Ampel-Koalition zwar kein spezifischen Ziel formuliert; dieser Indikator verdeutlicht die Dynamik des Geschehens jedoch besser als der oben gezeigte Bestandsindikator. Zum Jahreswechsel gab es mehrfach Brüche wegen sich ändernder Randbedingungen bei der Regulierung bzw. bei den Fördermaßnahmen. Dies war Ende 2022 wegen Kürzungen beim [Umweltbonus](https://www.bafa.de/DE/Energie/Energieeffizienz/Elektromobilitaet/Neuen_Antrag_stellen/neuen_antrag_stellen.html) besonders ausgeprägt.

<iframe title = "Abbildung Neuzulassungen elektrische Pkw" src="../../../docs/germany/figures/bev_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Pkw-Gesamtbestand

Im Gegensatz zum Ausbau der vollelektrischen Pkw-Flotte hat sich die Regierungskoalition kein Ziel für eine Reduktion des Bestands von Pkw mit Verbrennungsmotoren gesetzt. Die folgende Abbildung zeigt die Entwicklung der gesamten Pkw-Flotte in Deutschland nach Antriebs- bzw. Kraftstoffarten. Die Daten werden vom Kraftfahrt-Bundesamt bereitgestellt, von 2013 bis Ende 2017 als [Jahresdaten](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Jahrebilanz_Bestand/fz_b_jahresbilanz_node.html) und ab Mitte 2018 als [Quartalsdaten](https://www.kba.de/DE/Statistik/Fahrzeuge/Bestand/Vierteljaehrlicher_Bestand/viertelj%C3%A4hrlicher_bestand_node.html). Insgesamt wächst die Flotte weiterhin auf zuletzt knapp 49 Millionen Pkw. Seit Anfang 2020 nimmt der Bestand von Pkw mit Benzin- oder Dieselmotoren leicht ab; sie machen aber immer noch über 90 Prozent der Pkw-Flotte aus. Brennstoffzelle-Pkw spielen mit nur rund 2000 Fahrzeugen (0,004 Prozent) in der Gesamtflotte praktisch keine Rolle.

<iframe title = "Abbildung Bestand Pkw" src="../../../docs/germany/figures/cars_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Nutzfahrzeuge

Bei den Nutzfahrzeugen unterscheidet die Statistik Lastkraftwagen (Lkw), Zugmaschinen sowie Kraftomnibusse. Diese unterscheiden sich durch Anwendungszweck und Aufbau. Lkw sind Nutzfahrzeuge mit in der Regel festem Aufbau, die für den Transport von Gütern bestimmt sind. Daneben gibt es die sogenannten Zugmaschinen, welche durch ihre Bauart überwiegend zum Mitführen von Anhängefahrzeugen bestimmt sind. Zu dieser Kategorie gehören auch die sogenannten Sattelzugmaschinen, die umgangssprachlich oft auch als "Sattelschlepper" oder ebenfalls als "Lkw" bezeichnet werden. Neben den Sattelzugmaschinen gehören zur Kategorie der Zugmaschinen auch land- und forstwirtschaftliche Zugfahrzeuge, welche im Folgenden nicht näher betrachtet werden. Kraftomnibusse sind Fahrzeuge, die durch Einrichtung und Bauart zur Beförderung von mehr als neun Personen (inkl. Fahrzeugführer*in) bestimmt sind.

 Das [Klimaschutzprogramm 2030](https://www.bundesregierung.de/resource/blob/974430/1679914/c8724321decefc59cca0110063409b50/2019-10-09-klima-massnahmen-data.pdf) der Bundesregierung sieht vor, dass bis zum Jahr 2030 etwa ein Drittel der Fahrleistung im schweren Straßengüterverkehr elektrisch oder auf Basis strombasierter Kraftstoffe erbracht werden soll. Konkrete Ziele für Neuzulassungen oder Bestand elektrischer Lkw gibt es jedoch bisher nicht. Bei Stadtbussen wird im Klimaschutzprogramm degegen für 2030 ein Ziel von 50 Prozent elektrischen Antrieben im Bestand genannt. Reise- und Überlandsbusse sind hiervon jedoch nicht erfasst. Auf [europäischer Ebene](https://www.consilium.europa.eu/en/press/press-releases/2024/01/18/heavy-duty-vehicles-council-and-parliament-reach-a-deal-to-lower-co2-emissions-from-trucks-buses-and-trailers/) wurden zuletzt neue Flottengrenzwerte für CO<sub>2</sub>-Emissionen von schweren Nutzfahrzeugen beschlossen, die voraussichtlich nur mit einer schnellen Elektrifizierung der Fahrzeugflotten erreichbar sind. Demnach sollen im Jahr 2030 neu zugelassene Lastkraftwagen und Zugmaschinen über 7,5t sowie Überlandbusse 45 Prozent geringere Emissionen gegenüber 2019 aufweisen. In den Jahren 2035 bzw. 2040 steigt dieser Wert auf 65 bzw. 90 Prozent. Ausnahmen gibt es nur für Kleinserienhersteller, für Bergbau, Land- oder Forstwirtschaft, sowie Fahrzeuge für Militär, Zivilschutz, öffentliche Sicherheit und medizinische Versorgung. Bei Stadtbussen gelten nochmals strengere Vorgaben mit dem Ziel vollständig emissionsfreier neu zugelassener Busse bereits im Jahr 2035 und einem Zwischenziel von 90 Prozent bis zum Jahr 2030. Von den neuen Regeln werden 92% der verkauften Nutzfahrzeuge (Stand 2023) erfasst.

Im Folgenden werden die aktuellen Entwicklungen bei alternativen Antrieben für verschiedene Typen von Nutzfahrzeugen dargestellt. Die Daten zu [monatlichen Neuzulassungen](https://www.kba.de/DE/Statistik/Produktkatalog/produkte/Fahrzeuge/fz28/fz28_gentab.html) sowie zum [quartalsweisen Fahrzeugbestand](https://www.kba.de/DE/Statistik/Produktkatalog/produkte/Fahrzeuge/fz27_b_uebersicht.html) werden vom Kraftfahrtbundesamt bereit gestellt.

### Lastkraftwagen

Zu Beginn der Legislaturperiode im Oktober 2021 betrug der Anteil elektrisch betriebener Lkw am Gesamtbestand der in Deutschland zugelassenen Lkw 1,1 Prozent. Einen genauso hohen Anteil hatten Erdgasfahrzeuge. Zwei Jahre später war der Bestand an batterieelektrischen Fahrzeugen bereits doppelt so hoch wie der Bestand an Gas-Lkw. Der Anteil von Lkw mit Wasserstoff-Brennstoffzellen ist zuletzt ebenfalls gewachsen, allerdings auf sehr niedrigem Niveau.

<iframe title = "Abbildung Bestand LKW gesamt" src="../../../docs/germany/figures/trucks_gen_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Dies spiegelt sich auch in der Zahl der Neuzulassungen wider. Das Segment der batterieelektrischen Fahrzeuge zeigt mit teilweise sprunghaft angestiegenen Anteilen von über 17 Prozent die größte Dynamik, während Erdgasantriebe lediglich einen konstant niedrigen Anteil von meist einem Prozent der Neuzulassungen haben. Hybrid- und Wasserstoffantriebe machen bisher weniger als ein Prozent der Neuzulassungen aus.

<iframe title = "Abbildung monatliche Neuzulassungen LKW gesamt" src="../../../docs/germany/figures/trucks_gen_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Sattelzugmaschinen

Im Bereich der Sattelzugmaschinen ist Erdgas nach wie vor der relevanteste alternative Antrieb mit einem Anteil von rund einem Prozent am Bestand. Zuletzt kamen einige batterieelektrische Sattelzugmaschinen dazu, ihr Anteil am Bestand ist aber noch deutlich geringer als ein Prozent.

<iframe title = "Abbildung Bestand Sattelschlepper" src="../../../docs/germany/figures/trucks_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Bei den Neuzulassungen zeigt sich ein kontinuierlicher Rückgang der Gasantriebe bei gleichzeitigem Hochlauf der batterieelektrischen Fahrzeuge seit Mitte des Jahres 2022. Auch hier spielen andere Antriebsarten bisher kaum eine Rolle.

<iframe title = "Abbildung monatliche Neuzulassungen Sattelschlepper" src="../../../docs/germany/figures/trucks_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Kraftomnibusse

Bei den Bussen ist der Diesel bereits deutlich stärker auf dem Rückzug. Die relevantesten alternativen Antriebsarten sind hier rein batterieelektrische sowie Hybridbusse ohne Stecker. Im Gegensatz zu den Lkw ist der Anteil der Gasantriebe seit Oktober 2021 sogar gesunken. Innerhalb der ersten zwei Jahre der Koalition hat sich der Bestand an Hybridantrieben ohne Stecker sowie der Bestand an batterieelektrischen Fahrzeugen jeweils ungefähr verdoppelt. Unter anderem durch die [Förderung alternativer Antriebe](https://www.bmwk.de/Redaktion/DE/Wasserstoff/Foerderung-National/009-alternative-antriebe-personenverkehr.html) (batterieelektrisch, Wasserstoffbrennstoffzelle, Biomethan) beginnend im September 2021 wurde der Markthochlauf zusätzlich finanziell gefördert.

<iframe title = "Abbildung Bestand Kraftomnibusse" src="../../../docs/germany/figures/busses_stock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Bei den Neuzulassungen sind hybride und batterieelektrische Antriebe ebenfalls die dominante Kategorie innerhalb der alternativen Antriebe. Auch hier ist ein Anstieg der Anteile an den monatlichen Neuzulassungen zu beobachten. Im Oktober 2021 betrugen die Anteile von rein batterieelektrischen Bussen sowie von Hybridbussen jeweils rund 13 Prozent. Zusammen machten sie in manchen Monaten des Jahres 2023 bereits knapp 50 Prozent der neu zugelassenen Fahrzeuge aus.

<iframe title = "Abbildung monatliche Neuzulassungen Kraftomnibusse" src="../../../docs/germany/figures/busses_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Ladeinfrastruktur

### Anzahl Ladepunkte

Die Ampel hat sich ein Ziel von "einer Million öffentlich und diskriminierungsfrei zugänglichen Ladepunkten bis 2030 mit Schwerpunkt auf Schnellladeinfrastruktur" gesetzt. Am 1. Dezember 2021 waren laut [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/E-Mobilitaet/start.html) knapp 54.000 Ladepunkte in Betrieb, davon ca. 46.000 Normalladepunkte und 8000 Schnellladepunkte. Zur Erreichung des Ziels müssen bis 2030 im Durchschnitt monatlich rund 8700 neue Ladepunkte in Betrieb gehen. In der Abbildung ist ein entsprechender linearer Verlauf dargestellt. Außerdem ist ein logistischer Pfad hinterlegt, der dem Verlauf der oben dargestellten logistischen Bestandsentwicklung der batterieelektrischen Pkw-Flotte folgt.

<iframe title = "Abbildung Ladeinfrastruktur" src="../../../docs/germany/figures/cs.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

### Ladeleistung

Ergänzend wird in der folgenden Abbildung die Entwicklung der an öffentlichen Ladepunkten verfügbaren Ladeleistung dargestellt. Hierfür hat die Regierung bisher keine expliziten Ziele formuliert. Die insgesamt installierte Ladeleistung ist zuletzt vor allem aufgrund des Wachstums bei den Schnellladepunkten stark gestiegen. Die durchschnittliche Ladeleistung pro Ladepunkt wuchs ebenfalls, aber deutlich langsamer.

<iframe title = "Abbildung Ladeleistung" src="../../../docs/germany/figures/fig_cs_capa.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

Die folgende Abbildung zeigt das Verhältnis von batterieelektrischen Pkw und öffentlich zugänglichen Ladepunkten. Hierfür hat die Regierung kein explizites Ziel formuliert. Aus den Bestands- und Ladeinfrastrukturzielen für 2030 ergibt sich ein Wert von 15 batterieelektrischen Pkw pro Ladepunkt, wobei die Aufteilung auf Schnell- und Normalladepunkte unklar ist. Vor allem die Zahl der Elektrofahrzeuge, die sich rechnerisch einen öffentlich zugänglichen Schnellladepunkt teilen, ist seit dem Jahr 2020 deutlich angestiegen. Die Abbildung zeigt zusätzlich die durchschnittlich installierte Ladeleistung pro elektischem Pkw. Sie ist seit 2020 deutlich gesunken.

<iframe title = "Abbildung BEV pro Ladepunkt" src="../../../docs/germany/figures/bev_per_cp.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

## Schienenverkehr

### Elektrifizierung des Netzes

Die Ampelkoalition plant, bis zum Jahr 2030 75 Prozent des deutschen Schienennetzes zu elektrifizieren. Nach Angaben der [Deutsche Bahn AG](https://www.eba.bund.de/DE/Themen/Finanzierung/LuFV/IZB/izb_node.html) waren im Jahr 2021 61,7% der Strecken-Kilometer des bundeseigenen Schienennetzes elektrifiziert. Zur Erreichung des Ziel muss der Anteil bis 2030 im Durchschnitt um knapp 1,5 Prozentpunkte pro Jahr steigen. Aktuell ist die Elektrifizierungs-Geschwindigkeit deutlich geringer.

<iframe title = "Abbildung Elektrifizierung des Schienennetzes" src="../../../docs/germany/figures/electrification.html" width="100%" height= "500px" frameBorder="0"  loading = "lazy"></iframe>

