---
hide:
#  - navigation
#  - toc
---

# Energiepreise

=== "Autor*Innen"

    [Wolf-Peter Schill](../../docs/about/authors.md), [Nicolas Aichner](../../docs/about/authors.md), [Jan Czimmek](../../docs/about/authors.md), [Lars Felder](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)


!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Haushalts-Strompreise

Die Bezahlbarkeit von Energie, und insbesondere von Strom, war ein wichtiges energiepolitisches Ziel aller Bundesregierungen der letzten Jahre. Hier zeigen wir, wie sich der durchschnittliche Strompreis für Haushalte seit der Liberalisierung des Strommarkts entwickelt hat. Dabei greifen wir auf die [BDEW-Strompreisanalyse](https://www.bdew.de/service/daten-und-grafiken/bdew-strompreisanalyse/) zurück. Dargestellt sind durchschnittliche Preise für einen Haushalt mit einem Jahresverbrauch von 3500 kWh, wobei fixe Preisbestandteile ebenfalls auf den Verbrauch umgelegt wurden. Wir zeigen zunächst die vom BDEW zur Verfügung gestellten Daten in laufenden Preisen, und zusätzlich eine inflationsbereinigte Variante (Preisbasis 2023, deflationiert mit Verbraucherpreisindex). 

<iframe title = "Abbildung Haushalts-Strompreise nominal und real" src="../../../docs/germany/figures/hh_elec_tariff.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Die Strompreise für Haushalte sind deutlich höher als Großhandelsmarktpreise, da sie diverse weitere Preisbestandteile enthalten. Dazu gehören Kosten für Vertrieb und Zähler, regulierte Netzentgelte für den Betrieb der Übertragungs- und Verteilnetze sowie diverse Steuern, Abgaben und Umlagen. In den Jahren 2013 bis 2021 machten Steuern, Abgaben und Umlagen über die Hälfte des Haushaltsstrompreises aus. Im Zuge der Energiepreiskrise ist ihr Anteil aber auf knapp ein Drittel zurückgegangen, da die Beschaffungskosten wegen der hohen Großhandelspreise stark gestiegen sind. Die EEG-Umlage, die seit dem Jahr 2000 zur Finanzierung des Ausbaus erneuerbarer Energien erhoben wurde, ist zum 1. Juli 2022 abgeschafft worden. Diese Entlastung wurde jedoch vom Anstieg der Beschaffungskosten in den Jahren 2022 und 2023 deutlich überkompensiert. Im Jahr 2024 sind die Beschaffungskosten und damit auch die Preise wieder deutlich gesunken.

## Großhandels-Strompreise

Im Folgenden wird die Entwicklung der stündlichen Großhandelspreise für Strom in Deutschland dargestellt. Neben der Verteilung der stündlichen Preise innerhalb eines Jahres sind auch die Entwicklung der Preise im Tagesverlauf sowie Stunden mit negativen Preisen von Interesse. Die folgenden Abbildungen basieren auf Daten der [Bundesnetzagentur](https://www.smard.de/home/downloadcenter/download-marktdaten/) zu stündlichen Großhandelspreise für eine Megawattstunde (MWh) Strom im Marktgebiet Deutschland (Day-Ahead). Bis zum Jahr 2018 teilten sich Deutschland, Österreich und Luxemburg ein Marktgebiet für Strom; seit dem 01.10.2018 bildet Österreich ein eigenes Marktgebiet, während Luxemburg in einem Marktgebiet mit Deutschland verbleibt. Die Daten für das laufende Jahr (*) werden laufend aktualisiert.

### Jährliche Verteilung
Die Großhandelspreise sind im Kontext der Energiepreiskrise im Jahr 2022 auf zuvor nie erreichte Höchststände gestiegen. Grund hierfür waren neben Erzeugungsausfällen bei der Wasserkraft in Europa sowie bei der Atomkraft in Frankreich vor allem stark gestiegene Erdgaspreise. Diese haben sich direkt auf die Strompreise ausgewirkt, da Erdgaskraftwerke in vielen Stunden den Preis setzen (mehr Details dazu in der DIW-Publikation [Strommarkt erklärt](https://www.diw.de/de/diw_01.c.858018.de/publikationen/politikberatung_kompakt/2022_0184/strommarkt_erklaert__preisbildung__preiskrise_und_die____str___trag_zur_aktuellen_debatte_ueber_eingriffe_in_den_strommarkt.html)). Der Durchschnittspreis lag in den Jahren 2015 bis 2020 bei gut 30 Euro/MWh; er stieg im Jahr 2021 auf fast 100 Euro/MWh und im Jahr 2022 auf 235 Euro/MWh. Auch die stündlichen Maximalpreise und die Preisspreizung zwischen einzelnen Stunden sind stark gestiegen. Nachdem im Spätsommer 2022 ein Höchststand erreicht wurde, sind die Preise mittlerweile wieder deutlich gesunken - aber liegen immer noch deutlicht über dem Vorkrisen-Niveaus. Im Jahr 2023 kam es in in einer einzelnen Stunde zu einem extremen negativen Preis in Höhe des negativen Day-Ahead-Preislimits von -500 Euro/MWh (herauszoomen durch Doppelklick auf die Abbildung).

<iframe title = "Abbildung Großhandels-Strompreise" src="../../../docs/germany/figures/wholesale_electricity.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Verteilung über den Tag

Die folgenden Abbildungen zeigt die Verteilung der durchschnittlichen Großhandelpreise im Tagesverlauf, differenziert nach Sommer- (April-September) und Winterhalbjahr (Oktober-März). Vor allem im Sommerhalbjahr zeigt sich im Verlauf der letzten Jahre zunehmend eine Entwicklung, die im englischsprachigen Raum als "duck curve" bezeichnet wird: die Preise sinken rund um die Mittagszeit stark, da hier die Stromerzeugung aus Photovoltaik konzentriert ist (Bauch der "Ente"). Gleichzeitig steigen die Preise in den Schulter-Perioden morgens und vor allem abends (Kopf der "Ente"). Ein Grund hierfür ist, dass in diesen Stunden zunehmend teurere Spitzenlastkraftwerke zum Einsatz kommen.

<iframe title = "Abbildung durchschnittlichen Großhandelpreise im Tagesverlauf Sommer" src="../../../docs/germany/figures/fig_duckcuve_sum.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

Im Winterhalbjahr ist dieser Effekt deutlich weniger ausgeprägt, da hier die Solarstromerzeugung deutlich geringer ist.

<iframe title = "Abbildung durchschnittlichen Großhandelpreise im Tagesverlauf Winter" src="../../../docs/germany/figures/fig_duckcuve_win.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

### Negative Preise

Negative Preise treten am Großhandelsmarkt auf, wenn das Stromangebot die Nachfrage übersteigt. Gründe hierfür können unflexibel betriebene konventionelle oder erneuerbare Stromerzeugungsanlagen sein. Bei geförderten Erneuerbaren-Energien-Anlagen gibt es zudem teils ökonomische Anreize, auch in Stunden mit negativen Peisen Strom zu produzieren, teils aber auch gar keine technische Möglichkeit der Abschaltung in solchen Stunden. Seit dem Jahr 2015 ist die Anzahl der negativen Preise tendenziell gestiegen - mit einer Unterbrechung in den Jahren 2021 und 2022 aufgrund des zwischenzeitlich stark gestiegenen Strompreisniveaus. So viele negative Preise wie im Jahr 2024 gab es bisher noch nie. Die Daten für das aktuelle Jahr (*) werden laufend aktualisiert, auch in den folgenden Abbildungen.

<iframe title = "Abbildung negative Preise kumuliert" src="../../../docs/germany/figures/negative_price_cumulated.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung zeigt die Stunden mit negativen Preisen pro Jahr, jeweils sortiert in aufsteigender Reihenfolge nach Höhe der negativen Preise. Nach dem Krisenjahr 2022 ist nicht nur die Anzahl der Stunden mit negativen Preisen deutlich gestiegen, sondern es kam auch vermehrt zu stärker negativen Preisniveaus. Seit 2023 kam es auch zu Preisen unterhalb von -100€/MWh, bisher aber nur in sehr wenigen Stunden. 

<iframe title = "Abbildung negative Preis-Dauer-Kurve" src="../../../docs/germany/figures/negative_price_duration.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Ergänzend dazu zeigt die folgende Abbildung die Anzahl der Stunden mit negativen Preisen im Tagesverlauf. Negative Preise sind zuletzt vor allem tagsüber aufgetreten, wenn die Stromerzeugung aus Photovoltaikanlagen hoch war, insbesondere in den Stunden um die Mittagszeit. Dies geht mit der zuletzt stark gestiegenen Leistung der Solarenergie einher. Auch in dieser Darstellung zeigt sich das verstärkte Auftreten negativer Preise seit 2015, mit der Ausnahme der Jahre 2021 und 2022.

<iframe title = "Abbildung negative Preise im Tagesverlauf" src="../../../docs/germany/figures/negative_price_hourly.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Das folgende Histogramm stellt die Häufigkeit von Stunden mit verschiedenen negativen Preisniveaus dar. Die meisten negativen Preise lagen bisher in allen Jahren zwischen 0 und -10 Euro/MWh. Preise kleiner als -100€/MWh sind sehr selten und in der letzten Kategorie zusammengefasst.

<iframe title = "Abbildung negative Preise Histogramm" src="../../../docs/germany/figures/negative_price_histo.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Ergänzend zeigt die folgende Abbildung, wie häufig es in den vergangenen Jahren zu zusammenhängenden Stunden mit negativen Strommarktpreisen verschiedener Längen kam. Das bisher längste Intervall waren 36 aufeinander folgende Stunden im Jahr 2023.

<iframe title = "Abbildung negative Preise Histogramm" src="../../../docs/germany/figures/negative_price_consecutive.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung zeigt für das Jahr 2024, mit welchen Technologien in Stunden mit negativen Preisen in Deutschland Strom erzeugt wurde, aufgeschlüsselt nach verschiedenen Preisniveaus (bis -10 €/MWh, bis -20€/MWh, etc.). Die Photovoltaik hat mit Abstand den größten Anteil, der mit stärker negativen Preisen tendenziell auch noch etwas steigt. Dies dürfte vor allem an Aufdachanlagen liegen, die über konstante Einspeisetarife gefördert werden und keine Anreize - und oft auch keine technische Möglichkeit - zur Abregelung in Zeiten negativer Preise haben. Der Anteil anderer Technologien an der Stromerzeugung in Zeiten negativer Preise ist geringer und dürfte durch verschiedene Arten von "Must-Run"-Randbedingungen hindeuten (z.B. technische Mindestlast thermischer Kraftwerke, Kraft-Wärme-Kopplung, Bereitstellung von Regelleistung). Grundlage der Abbildung sind wiederum Daten der [Bundesnetzagentur](https://www.smard.de/home/downloadcenter/download-marktdaten/).

<iframe title = "Abbildung slider" src="../../../docs/germany/figures/neg_price_generation_shares.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Marktwerte erneuerbarer Energien

### Monatliche Marktwertfaktoren

Neben den Großhandelspreisen sind auch die Marktwerte von Solarenergie und Windkraft von Interesse. Der Marktwert entspricht dem mit der jeweiligen Stromerzeugung gewichteten Durchschnittspreis, den alle Anlagen einer bestimmten Technologie im Mittel am Strommarkt erzielen. Er kann auch als durchschnittlicher Erlös einer Technologie interpretiert werden. Monatliche Marktwerte spielen insbesondere für die Direktvermarktung von erneuerbarem Strom eine Rolle. Setzt man den Marktwert ins Verhältnis zum (ungewichteten) Durchschnittspreis am Strommarkt, erhält man den sogenannten Marktwertfaktor, auch Ertragsrate oder *capture rate* genannt. Die Abbildung zeigt die monatlichen Marktwertfaktoren von Windkraft und Solarenergie in Deutschland basierend auf Daten vom [Netztransparenzportal](https://www.netztransparenz.de/de-de/Erneuerbare-Energien-und-Umlagen/EEG/Transparenzanforderungen/Marktpr%C3%A4mie/Marktwert%C3%BCbersicht) und stellt sie den jeweiligen Marktanteilen der Technologien gegenüber. Letztere berechnen wir auf Basis von Daten der [Bundesnetzagentur](https://www.smard.de/home).

<iframe title = "Abbildung monatliche Marktwerte Zeitverlauf" src="../../../docs/germany/figures/capture_rate.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung stellt die monatlichen Marktwertfaktoren nicht im Zeitverlauf, sondern in Abhängigkeit des jeweiligen Marktanteils dar. Dabei zeigt sich in der Tendenz der sogenannte Kannibalisierungseffekt: bei steigenden Marktanteilen von Windkraft oder Solarenergie kann ihr Marktwertfaktor bei ansonsten unveränderten Bedingungen im Stromsektor sinken. Zusätzliche Flexibilität im Stromsektor, z.B. in Form von Energiespeichern kann diesem Kannibalisierungseffekt entgegenwirken, wie [hier](http://www.annualreviews.org/eprint/WSMW2UJJFWMJXX6FD7PU/full/10.1146/annurev-resource-101620-081246) beschrieben.

<iframe title = "Abbildung monatliche Marktwerte scatter" src="../../../docs/germany/figures/capture_rate_scatter.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

### Erlöse der Photovoltaik

Die folgende Abbildung zeigt die durchschnittlichen Anteile einzelner Stunden an den Gesamterlösen der Photovoltaik im Tagesverlauf, differenziert nach Sommer- und Winterhalbjahr. Angenommen wird dabei, dass der gesamte PV-Strom am Day-Ahead-Großhandelsmarkt vermarktet wird. Es zeigt sich, dass die Photovoltaik in den Mittagsstunden im Sommerhalbjahr, also in den Stunden ihrer höchsten Stromerzeugung, zuletzt immer weniger erlösen konnte. Dafür verlagern sich höhere Erlösanteile in die Morgen- und Abendstunden. Dies liegt am oben beschriebenen Kannibalisierungseffekt und deutet darauf hin, dass die Flexibilität im Stromsystem zuletzt nicht mit dem PV-Zubau Schritt gehalten hat. Im Winterhalbjahr ist dieser Effekt viel weniger ausgeprägt, da die Solarstromerzeugung deutlich geringer ist.

<iframe title = "Abbildung PV-Erlösverteilung im Tagesverlauf" src="../../../docs/germany/figures/pv_revs_year.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

Der Rückgang der PV-Strommarkterlöse in den Mittagsstunden liegt teilweise auch an der Einspeisung und Vermarktung von PV-Strom in Stunden mit negativen Preisen (vgl. oben, Abschnitt [Negative Preise](#negative-preise)). Im Folgenden ist dargestellt, wie sich die Verteilung er PV-Erlöse am Strommarkt im letzten und im laufenden Jahr dargestellt hätte, wenn die Preise nicht negativ geworden wären, sondern in Negativpreisphasen auf minimal 0 Euro gesunken wären. Plausibilisieren ließe sich diese kontrafaktische Annahme beispielsweise durch einen Stop von Vergütungszahlungen an PV-Anlagen in Stunden mit negativen Preisen, oder auch durch zusätzliche Stromlasten, die sich in Zeiten negativer Strompreise zuschalten. Es zeigt sich, dass die negativen Preise bereits im Jahr 2023, aber mehr noch im laufenden Jahr 2024 einen erheblichen Einfluss auf die Verteilung der PV-Erlöse haben. Insbesondere prägen sie den Erlösrückgang zur Mittagszeit stark.

<iframe title = "Abbildung PV-Erlösverteilung im Tagesverlauf kontrafaktisch" src="../../../docs/germany/figures/pv_revs_cf.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>

Ergänzend dazu zeigt die folgende Abbildung die Entwicklung der absoluten monatlichen PV-Marktwerte seit Januar 2023. Auch hier ist der Einfluss der negativen Großhandelspreise dargestellt, indem kontrafaktisch angenommen wird, dass der Preis nie unter 0 Euro pro MWh sinkt. Hier zeigt sich in den Sommermonaten ein relativ deutlicher Einfluss der negativen Preise von teils über 6 Euro pro MWh (Mai 2024).

<iframe title = "Abbildung monatliche PV-Erlöse kontrafaktisch" src="../../../docs/germany/figures/pv_monthly_avg_cf.html" width="100%" height= "450px" frameBorder="0" loading = "lazy"></iframe>