---
hide:
#  - navigation
#  - toc
---

# Hintergrund Windenergie 

=== "Autor*Innen"

    [Felix Schmidt](../../../../docs/about/authors.md), [Wolf-Peter Schill](../../../../docs/about/authors.md)

## Größe der Anlagen

Die in Deutschland neu installierten Windkraftanlagen wurden im Lauf der letzten Jahre immer größer und leistungsstärker. Die Nabenhöhe und der Rotordurchmesser neuer Anlagen haben sich seit dem Jahr 2000 mehr als verdoppelt (vgl. folgende Abbildung, 12-monatige gleitende Durchschnitte). Die elektrische Leistung hat sich sogar fast verfünffacht: Anfang des Jahres 2000 hatten neu installierte Windkraftanlagen eine durchschnittliche Leistung von unter einem Megawatt. Aktuell liegt der Wert im Durchschnitt bei über vier Megawatt pro Anlage.

<iframe title = "Abbildung Größenentwicklung" src="../../../../docs/germany/figures/wind_on_dd_attributes.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Dieses Größenwachstum der neu installierten Anlagen spiegelt sich auch im Bestand wider (linke Abbildung). Bis Ende 2016 dominierten Windkraftanlagen mit einer Leistung unter 2 Megawatt. Heute machen Anlagen mit einer Leistung von 3 MW oder mehr bereits ein gutes Drittel des Gesamtbestandes aus. Große Anlagen mit Leistungen über 4 MW haben bisher nur einen relativ kleinen Anteil am Gesamtbestand; sie dominieren aber bereits die monatlichen Neuinstallationen (rechte Abbildung). Zurückgebaut werden bisher fast ausschließlich Anlagen mit einer Leistung unter 2 MW.

<iframe title = "Abbildung Größenklassen" src="../../../../docs/germany/figures/wind_on_dd_size_shares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Verteilung über Bundesländer

Die Anteile der einzelnen Bundesländer an der insgesamt installierten Windkraftleistung in Deutschland haben sich in den letzten 20 Jahren nicht stark verändert. Am meisten Windkraft ist bisher in Niedersachsen installiert, mit einigem Abstand folgen Brandenburg, Schleswig-Holstein, Nordrhein-Westfalen und Sachsen-Anhalt. In diesen fünf Bundesländern sind derzeit ungefähr zwei Drittel der gesamten Windkraftleistung Deutschlands installiert. Große Flächenländern wie Bayern oder Baden-Württemberg tragen bisher erst wenig zur Nutzung der Windkraft in Detuschland bei. Dieses Bild ändert sich auch nicht, wenn man auf den Ausbau am aktuellen Rand blickt. Im Gegenteil: am Zubau der letzten zwölf Monate haben die genannten "Top-5-Bundesländer" sogar einen noch größeren Anteil als am Gesamtbestand. Mit Abstand am stärksten war der Zubau zuletzt im vergleichsweise kleinen Land Schleswig-Holstein.

<iframe title = "Abbildung Anteile Bundesländer" src="../../../../docs/germany/figures/wind_on_dd_state_shares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Die unterschiedliche Verteilung der installierten Leistung über die Bundesländer hängt nicht nur damit zusammen, das die Länder unterschiedlich große Potenziale für die Windkraftnutzung haben, sondern auch damit, dass sie diese Potenziale unterschiedlich gut ausnutzen. Die folgenden Abbildung zeigt die derzeit installierte Leistung in den Ländern im Vergleich zum langfristig möglichen und nötigen Ausbau in einem "Bürgerszenario" des Ariadne-Projekts ([Gerhardt et al. (2023)](https://ariadneprojekt.de/media/2023/11/Ariadne-Kurzdossier_EE_Potenziale-Akzeptanz_November2023.pdf)), das die Länder-spezifischen Potenziale berücksichtigt. Abgesehen vom Stadtstaat Bremen, der sein insgesamt sehr kleines Potenzial demnach bereits vollständig ausschöpft, nutzen die oben genannten "Top-5-Länder" ihre Potenziale auch schon besonders gut aus. Allen voran geht Schleswig-Holstein, das bereits über Drittel der im Ariadne-Szenario vorgesehenen Leistung realisiert hat. Besonders weit zurück bei der Erschließung ihrer Potenziale hängen dagegen Bayern, Sachsen und Baden-Württemberg.

<iframe title = "Abbildung Bestand und Potenziale Bundesländer" src="../../../../docs/germany/figures/wind_on_dd_butterfly_wind.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Ausschreibungen

Die folgende Abbildung gibt einen Überblick über die Auktionen von Erzeugungsleistung für Windkraft an Land seit Beginn der Ausschreibungen im Jahr 2017 auf Basis von Daten der [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/start.html). Dargestellt sind ausgeschriebene Mengen, bezuschlagte Mengen sowie Zuschlagswerte. Letztere sind maßgeblich für die Marktprämien, die die Anlagen erhalten und können als durchschnittlich erforderliche Mindesterlöse pro kWh über die Laufzeit interpretiert werden. Wir stellen die Zuschlagswerte zur besseren Vergleichbarkeit hier in realen Preisen dar, deflationiert mit dem Verbraucherpreisindex. Die ausgeschriebene Leistung hat sich im Zeitverlauf erhöht, allerdings weniger stark als bei den PV-Anlagen. Zuletzt waren die meisten Ausschreibungen jedoch unterzeichnet, einige davon deutlich. Das heißt, es gab deutlich zu wenige Gebote. Die (realen) Zuschlagswerte sind in den letzten Jahren kaum gesunken. Bei den zuletzt unterzeichneten Auktionen lagen sie jeweils auf Höhe des maximal möglichen Zuschlagswerts.

<iframe title = "Abbildung Auktion Windkraft an Land" src="../../../../docs/germany/figures/wind_on_auction.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung zeigt die jährlichen Ausschreibungs- und Zuschlagsmengen noch einmal in aggregierter Form.

<iframe title = "Abbildung Auktion Windkraft an Land jährlich" src="../../../../docs/germany/figures/wind_on_auction_annual.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>