# Deep dives

Currently, there are background articles on the following topics:

<div class="grid cards" markdown>

- :fontawesome-solid-solar-panel: [Photovoltaics](pv.en.md)
- :material-wind-turbine: [Wind Energy](wind.en.md)

</div>