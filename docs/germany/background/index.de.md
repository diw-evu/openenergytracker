# Hintergrund

Aktuell gibt es Hintergrundartikel zu folgenden Themen:

<div class="grid cards" markdown>

- :fontawesome-solid-solar-panel: [Photovoltaik](pv.de.md)
- :material-wind-turbine: [Windenergie](wind.de.md)

</div>