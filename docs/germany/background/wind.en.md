---
hide:
#  - navigation
#  - toc
---

# Deep wind energy

=== "Authors"

    [Felix Schmidt](../../../../docs/about/authors.md), [Wolf-Peter Schill](../../../../docs/about/authors.md)

## Turbine size

Onshore wind turbines installed in Germany have been getting larger and more powerful over the years. Hub heights and rotor diameters more than doubled since 2000 (cf. figure below for 12 month rolling averages). Average power has even almost quintupled. Currently, the average newly added turbine has a power capacity of over 4 megawatts.

<iframe title = "Figure Size Development" src="../../../../docs/germany/figures/wind_on_dd_attributes.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

The aforementioned size growth is also apparent in the turbine stock (left figure). Until the end of 2016 turbines with less than 2 MW dominated. Today
turbines with at least 3 MW represent more than a third of the stock. The share of larger installations with over 4 MW of capacity is still relatively small but make up the largest share of recent monthly capacity additions. Deconstructed installations are almost exclusively smaller than 2 MW.
ebaut werden bisher fast ausschließlich Anlagen mit einer Leistung unter 2 MW.

<iframe title = "Figure Size Classes" src="../../../../docs/germany/figures/wind_on_dd_size_shares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Distribution across German states

The total capacity shares among the German states have not changed much in the last 20 years. Lower Saxony has the most installed capacity followed at some distance by Brandenburg, Schleswig-Holstein, North Rhine-Westphalia and Saxony-Anhalt. These five states make up for two thirds of the total capacity installed in Germany. States large in area such as Bavaria or Baden-Württemberg contribute little to the total capacity. This assessment does not change if one concentrates on recent additions. On the contrary, the aforementioned top-5 states have an even larger share. The most capacity additions by far in the last 12 months have happened in the comparably small state of Schleswig-Holstein. 

<iframe title = "Figure Shares States" src="../../../../docs/germany/figures/wind_on_dd_state_shares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

The differences in installed capacities across the German states does not merely depend on the differences in capacity potentials but also on the way these potentials are used. The following figure shows the currently installed capacity in each state in comparison to a scenario of the Ariadne project reflecting the necessary capacities by state for 2045 under considerations of the state-specific potentials. Apart from small city state Bremen which has already met its small potential for onshore wind energy, it is again the top-5 states that already make good use of their potential. Especially so Schleswig-Holstein, having already realized two thirds of the capacity envisaged in the Ariadne scenario. Bayern, Saxony and Baden-Württemberg are particularly far behind the capacity goals suggested in the scenario.   

<iframe title = "Figure Stocks and Potentials States" src="../../../../docs/germany/figures/wind_on_dd_butterfly_wind.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>