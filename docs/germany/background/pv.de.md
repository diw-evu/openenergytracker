---
hide:
#  - navigation
#  - toc
---

# Hintergrund Photovolatik 

=== "Autor*Innen"

    [Felix Schmidt](../../../docs/about/authors.md), [Wolf-Peter Schill](../../../docs/about/authors.md)

## Netto-Zubau

### ... nach Anlagentyp

Laut der im Mai 2023 veröffentlichten [Photovoltaik-Strategie](https://www.bmwk.de/Redaktion/DE/Publikationen/Energie/photovoltaik-stategie-2023.pdf) soll ab dem Jahr 2026 der weitere PV-Ausbau ungefähr je zur Hälfte auf Freiflächenanlagen und Anlagen auf Gebäuden entfallen. Die folgende Abbildung zeigt, analog zum oben gezeigten Gesamtbestand, den monatlichen Netto-Zubau nach Anlagentyp seit dem Jahr 2017. Datengrundlage hierfür ist das [Marktstammdatenregister (MaStR)](https://www.marktstammdatenregister.de/MaStR/Einheit/Einheiten/OeffentlicheEinheitenuebersicht). Neben dem grundsätzlich ansteigenden Trend zeigt die Abbildung, dass der Ausbau in fast allen Monaten stark vom Segment der "Baulichen Anlagen" getragen wurde, was überwiegend Aufdach-Anlagen sind. Die Abbildung zeigt zudem, dass das Segment der kleinen Balkon-PV-Anlagen ("Steckerfertige Erzeugungsanlagen") zuletzt stark gewachsen ist, allerdings von einem sehr niedrigen Niveau aus (Doppelklick auf entsprechenden Legenden-Eintrag). Zu beachten ist dabei, dass nur ein Teil der Balkon-Anlagen korrekt im Marktstammdatenregister erfasst wird. Befragungsergebnissen der [HTW Berlin](file:///C:/Users/wschill/Downloads/PRAETORIUS-2022-Marktentwicklung-und-technisch-rechtliche-Bedingungen-fuer-Steckersolargeraete.pdf) zufolge wurden 2022 nur rund ein Drittel der Anlagen vollständig und korrekt im MaStR angemeldet.

<iframe title = "Abbildung Netto-Zubau nach Anlagentyp" src="../../../../docs/germany/figures/pv_type_split_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung zeigt ergänzend die relativen Anteile des Zubaus verschiedener PV-Anlagentypen in jährlicher Auflösung seit dem Jahr 2000. Demnach wurde bisher noch in keinem Jahr ein Anteil von 50 Prozent Freiflächenanlagen am Netto-Zubau erreicht, wie es ab 2026 angestrebt wird. In den letzten Jahren lag der Anteil von Freiflächenanlagen, mit einigen Schwankungen, im Bereich von ungefähr einem Drittel.

<iframe title = "Abbildung Netto-Zubau nach Anlagentyp (relativ)" src="../../../../docs/germany/figures/pv_type_split_rel_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... nach Anlagengröße

Der Zubau lässt sich auch nach Anlagengrößen unterscheiden (Auswahl einzelner Größenklassen durch Klick auf die Legende). Bei den baulichen PV-Anlagen machten zuletzt kleine Anlagen bis 30 kW meist den Großteil der monatlichen Netto-Zubauten aus. Diese Anlagen dürften überwiegend auf Dächern privater Haushalte installiert worden sein. Das Segment der vermutlich gewerblichen Gebäude-PV-Anlagen über 1000 kW ist deutlich kleiner, es ist zuletzt aber ebenfalls gewachsen. Bei den Freiflächenanlagen dominieren klar Anlagen, die größer als 1000 kW (1 MW) sind.

<iframe title = "Abbildung Netto-Zubau nach Anlagengroesse" src="../../../../docs/germany/figures/pv_size_split_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... Förderungsart

Alternativ kann der PV-Ausbau nach Förderungsart differenziert werden. Diese Daten stellt die [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/ErneuerbareEnergien/ZahlenDatenInformationen/start.html;jsessionid=0A5198EA6A2B8EB25296A3F1B9279634) bereit. Die Abbildung zeigt, dass der Netto-Zubau der letzten zwölf Monate zu einem großen Teil von Anlagen in der gesetzlichen EEG-Förderung getragen wurde, d.h. von Anlagen mit einer Einspeisevergütung. Dabei handelt es sich weitgehend um kleine Aufdachanlagen. Dagegen war der Beitrag der über Ausschreibungen zugebauten Anlagen - dies sind überwiegend Freiflächenanlagen, die eine gleitende Marktprämie erhalten - nicht einmal halb so groß. Das Segment der ungeförderten Anlagen (über 750 kW) ist noch vergleichsweise klein. Hierbei handelt es sich z.B. um Anlagen, die über Power Purchase Agreements vermarktet werden oder dem reinen Eigenverbrauch in Gewerbe oder Industrie dienen. Es kann sich aber auch um teilweise noch ungeprüfte Marktstammdatenregister-Einträge seitens der Bundesnetzagentur handeln. Mieterstrom-Anlagen bleiben bisher eine vernachlässigbar keine Nische.

<iframe title = "Abbildung Netto-Zubau in den letzten 12 Monaten nach EEG" src="../../../../docs/germany/figures/pv_eeg_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

## Auf Bundesländerebene

### ... Bestand und Potenzialnutzung

Ergänzend zeigt folgende Abbildung den bisher installierten Gesamtbestand von PV-Anlagen nach Bundesländern. Der linke Teil zeigt, dass insgesamt in Bayern am meisten PV-Leistung installiert ist, mit einem Freiflächenanteil von rund einem Drittel. Mit deutlichem Abstand folgen Baden-Württemberg und Nordrhein-Westfalen, gefolgt von Niedersachsen und Brandenburg. Von diesen Ländern hat nur Brandenburg einen größeren Anteil an Freiflächenanlagen. Sehr gering ist die installierte PV-Leistung in den drei Stadtstaaten.

Allerdings unterscheiden sich auch die Potenziale der Bundesländer zur Nutzung der Solarenergie aufgrund von Größen-, Landschafts- und Strukturunterschieden deutlich. Der rechte Teil der Abbildung setzt daher die installierte Leistung ins Verhältnis zu den länderspezifischen Potenzialen, wie sie im Ariadne-Projekt von [Gerhardt et al. (2023)](https://ariadneprojekt.de/media/2023/11/Ariadne-Kurzdossier_EE_Potenziale-Akzeptanz_November2023.pdf) ermittelt wurden. Bei Photovoltaik auf bzw. an baulichen Anlagen unterscheidet sich die Potenzialnutzung zwischen den meisten Ländern nicht sehr stark. Das heißt, der absolut betrachtet sehr hohe Ausbaugrad der Gebäude-PV in Bayern geht teilweise auch auf ein besonders hohes Potenzial in diesem Bundesland zurück. Manche Länder, die in Hinblick auf die absolut installierte Leistung bei der Gebäude-PV eher im unteren Mittelfeld liegen, nutzen ihre Potenziale relativ betrachtet schon recht gut aus, beispielsweise Mecklenburg-Vorpommern oder Sachsen-Anhalt. Die Stadtstaaten Berlin und Hamburg bilden auch bei der Potenzialausnutzung die Schlusslichter, was unter anderem durch Koordinationsprobleme von Aufdachanlagen in Mehrfamilienhäusern erklärt werden kann.

Bei der Potenzialausnutzung der Freiflächenanlagen gibt es deutlich größere Unterschiede zwischen den Bundesländern. Zugrunde gelegt sind hier die Potenziale nach neuem EEG, inklusive geeigneter Flächen im Abstand von 500 Metern von Autobahnen und Schienenwegen (Abb. 16 in [Gerhardt et al. (2023)](https://ariadneprojekt.de/media/2023/11/Ariadne-Kurzdossier_EE_Potenziale-Akzeptanz_November2023.pdf)). Besonders gut schneidet hier das Saarland ab, gefolgt von Brandenburg und Bayern. Flächenländer wie Baden-Württemberg, Nordrhein-Westfalen und Niedersachsen nutzen ihre Freiflächenpotenziale dagegen bisher noch relativ wenig.

<iframe title = "Abbildung PV Bestand nach Bundesland" src="../../../../docs/germany/figures/fig_pv_butterfly.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... Gesamtbestand und Trend, normiert mit Potenzialen

Die folgenden Karten zeigen den mit den jeweiligen Länder-Potenzialen normierten Gesamtbestand an Photovoltaik (Summe von Freiflächen und Gebäuden, links) sowie den ebenfalls auf Potenziale bezogenen Ausbautrend der letzten zwölf Monate. Dabei fällt auf, dass die Länder, die ihr Ausbaupotenzial insgesamt schon am meisten ausgenutzt haben, aktuell auch mit die höchste Zubaudynamik aufweisen. Weit vorne liegen in beiden Bereichen das Saarland und Bayern. Dagegen haben beispielsweise Thüringen und Sachsen-Anhalt ihre Potenziale bisher vergleichsweise wenig erschlossen, und auch die aktuelle Ausbaudynamik ist dort am geringsten. Die Stadtstaaten Hamburg und Berlin haben ihre Potenziale bisher vergleichsweise wenig genutzt, aber bei der Ausbaudynamik schneiden sie etwas besser ab. Die Daten für Bremen sind nur unter Vorbehalt vergleichbar, da dort kein Potenzial an/auf baulichen Anlagen ausgewiesen ist.

<iframe title = "Abbildung Geographische Verteilung von Bestand und Ausbau in den letzten 12 Monaten" src="../../../../docs/germany/figures/pv_maps.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

## Ausschreibungen

Die folgenden Abbildungen geben einen Überblick über die Auktionen von PV-Leistung auf Freiflächen sowie Dächern seit Beginn der jeweiligen Ausschreibungen auf Basis von Daten der [Bundesnetzagentur](https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/Ausschreibungen/start.html). Dargestellt sind ausgeschriebene Mengen, bezuschlagte Mengen sowie Zuschlagswerte. Letztere sind maßgeblich für die Marktprämien, die die Anlagen erhalten und können als durchschnittlich erforderliche Mindesterlöse pro kWh über die Laufzeit interpretiert werden. Wir stellen die Zuschlagswerte zur besseren Vergleichbarkeit hier in realen Preisen dar, deflationiert mit dem Verbraucherpreisindex. 

Im Bereich der Freiflächenanlagen hat sich die ausgeschriebene Leistung seit Beginn der Auktionen im Jahr 2015 deutlich erhöht, vor allem seit Beginn der Ampel-Regierung. Nur wenige Ausschreibungen waren unterzeichnet. Das heißt, es gab nur selten zu wenige Gebote. Dies war vor allem im Jahr 2022 der Fall. Die (realen) Zuschlagswerte sind im Zeitverlauf deutlich gesunken.

<iframe title = "Abbildung PV-Auktion Freifläche" src="../../../../docs/germany/figures/pv_fr_auction.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Die folgende Abbildung zeigt die jährlichen Ausschreibungs- und Zuschlagsmengen für Freiflächenanlagen noch einmal in aggregierter Form.

<iframe title = "Abbildung PV-Auktion Freifläche jährlich" src="../../../../docs/germany/figures/pv_fr_auction_annual.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Ausschreibungen von Aufdachanlagen sind ein vergleichsweise kleines Segment. Sie finden erst seit dem Jahr 2021 statt. Sie waren im Jahr 2022 ebenfalls unterzeichnet. Die Zuschlagswerte lagen immer über denen der Freiflächenanlagen.

<iframe title = "Abbildung PV-Auktion Dachanlagen" src="../../../../docs/germany/figures/pv_ad_auction.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

## Freiflächenphotovoltatik

<iframe title = "Abbildung PV-Auktion Dachanlagen" src="../../../../docs/germany/figures/pv_util_stock.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

Die obige Abbildung weißt den aktuellen Bestand und Trend bei der Freiflächenphotovoltaik (FFPV) aus, und zeigt die die Regierungsziele. Angenommen wird, dass FFPV-Anlagen 50% der Gesamtziele der Bundesregierung erfüllen werden.

<iframe title = "Abbildung PV-Auktion Dachanlagen" src="../../../../docs/germany/figures/pv_util_additions.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

In den letzten Jahren hat sich die Größe der PV-Anlage, die in der Freifläche verbaut werden, deutlich nach oben entwickelt.