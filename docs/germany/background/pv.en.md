---
hide:
#  - navigation
#  - toc
---

# Deep dive photovoltaics

=== "Authors"

    [Felix Schmidt](../../../docs/about/authors.md), [Wolf-Peter Schill](../../../docs/about/authors.md)

## Net additions

### ... by installation type

According to the [Photovoltaic Strategy](https://www.bmwk.de/Redaktion/DE/Publikationen/Energie/photovoltaik-stategie-2023.pdf) published in May 2023, the expansion of PV from 2026 onwards will be divided roughly equally between ground-mounted systems and systems on buildings. The following figure shows, analogously to the total stock shown above, the monthly net additions by system type since 2017. The data basis for this is the [*Marktstammdatenregister*(MaStR)](https://www.marktstammdatenregister.de/MaStR/Einheit/Einheiten/OeffentlicheEinheitenuebersicht). Apart from a generally increasing trend, the figure shows that the "buildings" segment, which predominantly consists of rooftop installations, has been driving net additions. The figure also shows that the segment of small balcony PV systems ("plug-in generation systems") has grown strongly recently, albeit from a very low level (double-click on the corresponding legend entry). It should be noted that only a fraction of the balcony systems are correctly recorded in the *Marktstammdatenregister*. According to survey results of [HTW Berlin](file:///C:/Users/wschill/Downloads/PRAETORIUS-2022-Marktentwicklung-und-technisch-rechtliche-Bedingungen-fuer-Steckersolargeraete.pdf), only about one third of the database entries for these installations is correct and complete.

<iframe title = "Figure Net additions by installation type" src="../../../../docs/germany/figures/pv_type_split_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

The following figure additionally shows the share of total annual net additions by installation type since 2000. So far ground-mounted installation have never reached a share of 50% of total additions as envisioned by the Photovoltaic Strategy from 2026. In recent years, the share fluctuated around about one third.

<iframe title = "Figure Net addtions by installation type (relative)" src="../../../../docs/germany/figures/pv_type_split_rel_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... by installation size

Net additions are also differentiable by installation size (use the legend to view particular size classes in isolation). Within the buildings segment it is predominantly the smaller installations up to 30 kW that drive the increase in additions. These installations are most likely situated on private rooftops. While the segment of commercial building installations north of 1000 kW is significantly smaller, it has grown recently as well. The ground is dominated by large installations with a capacity of over 1000 kW (1 MW).

<iframe title = "Figure Net additions by installation type" src="../../../../docs/germany/figures/pv_size_split_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... by subsidy type

The [German Network Agency](https://www.bundesnetzagentur.de/DE/Fachthemen/ElektrizitaetundGas/ErneuerbareEnergien/ZahlenDatenInformationen/start.html;jsessionid=0A5198EA6A2B8EB25296A3F1B9279634) provides data on net additions by subsidy type in the last twelve months. The following figure shows that the lion share of additions receive feed-in tariffs or premiums in accordance to the German Renewable Energy Sources Act. In most cases additions are small rooftop installations. Not even half as many additions receive subsidies resulting from a reverse auction, in which the lowest subsidy bids by installation owners win. Installations participating in these auctions are mostly large-scale ground-mounted plants. The share of installations not receiving any subsidies is rather small at this point. Such installations are e.g. marketed under power purchasing agreements (PPAs) or serve own consumption of larger industrial or commercial consumers. Lastly, this segment also includes some preliminary or unverified entries into the database. The Mieterstrom (tenant electricity) segment has been rather niche so far. 

<iframe title = "Figure Net additions by subsidy type" src="../../../../docs/germany/figures/pv_eeg_plt.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

## German state level

### ... installed capacity absolute and relative to potential

This section splits additions and installed capacity by German states. In the following figure, the left panel shows that Bavaria has the most installed capacity hosting roughly a third of all ground-mounted capacity. Far behind follow Baden-Württemberg, North Rhine-Westphalia, Lower Saxony und Brandenburg. Among these states only Brandenburg has a larger share of ground-mounted installations. The overall capacity in the city-states is negligible. 

However, comparing absolute capacities is somewhat misleading as the states differ substantially  in terms of landscape, building density and other factors that drive PV capacity potential. In order to admit a like-for-like comparison we express installed capacities as a share of the potential, as recently estimated by the Adriadne-Project, for each state by installation type. For the buildings segment there is not much variation across states. Consequently, large Bavarian capacities in this sector derive from a large potential. On the other hand, some states that seem to trail behind on an absolute basis actually use their potential similarly to states hosting much more capacity in absolute terms. Also in terms of use of potentials the city-states fall behind. Part of the reason is that they have historically faced issues in coordinating deployment for multi-family homes.

For ground-mounted installations, there is significantly more variation across states in terms of use of potential. While Saarland, Brandenburg and Bavaria are doing comparatively well, Baden-Württemberg, North Rhine-Westphalia and Lower Saxony have only used a fraction of their potential so far.

<iframe title = "Figure Installed PV capacity by German state" src="../../../../docs/germany/figures/fig_pv_butterfly.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>

### ... installed capacity and trend as a share of potentials

The following maps show the installed capacity by German state as a share of their respective potential both for ground-mounted and building installations in the left panel. The right panel shows the net additions as a share of the potential for the last twelve months until the end of last month. It is clear that installed capacity and installation dynamics (measured in terms of additions in the last 12 months) correlate. Bayern and Saarland are in the lead here, while Thuringia and Saxony-Anhalt which have been slow to add capacity in the last twelve months have only used a small share of their overall capacity so far. City-states are far behind in terms of installed capacity. However, their trend places them better. Data for Bremen are rather unreliable as the estimated potential seems very small compared to recent additions. 

<iframe title = "Figure Geographic distribution of installed capacity and additons in the last 12 months" src="../../../../docs/germany/figures/pv_maps.html" width="100%" height="500px" frameBorder="0" loading = "lazy"></iframe>