---
hide:
#  - navigation
#  - toc
---

# Erneuerbare Wärme

=== "Autor*Innen"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md), [Fernanda Ballesteros](../../docs/about/authors.md)

## Wärmepumpen

Mit Wärmepumpen kann Umweltwärme nutzbar gemacht werden. Dies spielt in vielen Zukunftsszenarien insbesondere für den Raumwärmebereich eine große Rolle. Die Regierung hat in ihrem Koalitionsvertrag kein Ziel für den Ausbau von Wärmepumpen genannt - der Begriff "Wärmepumpe" wird im gesamten Dokument kein einziges mal verwendet. In der [Eröffnungsbilanz Klimaschutz](https://www.bmwi.de/Redaktion/DE/Downloads/Energie/220111_eroeffnungsbilanz_klimaschutz.html) des BMWK wird ein Korridor von "4,1 bis 6 Millionen Wärmepumpen" im Jahr 2030 genannt, wobei diese Zahl sich auf die Bereitstellung von Raumwärme in Einzelgebäuden beziehen dürfte (ohne Großwärmepumpen in Wärmenetzen und ohne Hochtemperaturwärmepumpen). Im Kontext des [2. Wärmepumpengipfels im November 2022](https://www.bmwk.de/Redaktion/DE/Downloads/Energie/2-waermepumpen-gipfel-eckpunktepapier.html) wurde vom BMWK dann ein Ziel von rund sechs Millionen Wärmepumpen im Jahr 2030 sowie ein Zubau von je mindestens 500.000 Wärmepumpen pro Jahr ab 2024 genannt. Monatlich aktualisierte öffentliche Daten zum Bestand von Heizungswärmepumpen in Deutschland gibt es nicht. Als Datenquelle bis Ende 2022 nutzen wir [AGEE-Stat](https://www.erneuerbare-energien.de/EE/Navigation/DE/Service/Erneuerbare_Energien_in_Zahlen/Entwicklung/entwicklung-der-erneuerbaren-energien-in-deutschland.html) sowie am aktuellen Rand Zahlen des [BDH](https://www.bdh-industrie.de/start/marktdaten). Derzeit liegt der Ausbau von Wärmepumpen weit hinter dem Zielpfad zurück.
 
Zum Vergleich können in der Abbildung auch [Szenarien](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/) des [Ariadne-Projekts](https://ariadneprojekt.de/) eingeblendet werden. Das Leitmodell für Wärmepumpen, REMod, ermittelt im *Technologiemix-Szenario* einen Bestand von 5,3 Million Wärmepumpen im Jahr 2030 (in Einzelgebäuden), was unter dem Regierungsziel liegt. Bis 2045 wächst dieser Wärmepumpen-Bestand auf rund 15 Millionen an. Außerdem können die Wärmepumpen-Ausbaupfade vier weiterer Studien verglichen werden, die gemeinsam mit den Ariadne-Szenarien als "Big 5"-Szenarien bezeichnet wurden, nämlich "Klimaneutrales Deutschland 2045" von [Agora Energiewende](https://www.agora-energiewende.de/veroeffentlichungen/klimaneutrales-deutschland-2045/), "Klimapfade 2.0" (Zielpfad) des [BDI](https://bdi.eu/artikel/news/klimapfade-2-0-deutschland-braucht-einen-klima-aufbruch/), "Leitstudie Aufbruch Klimaneutralität" (Szenario KN100) der [Dena](https://www.dena.de/newsroom/meldungen/dena-leitstudie-aufbruch-klimaneutralitaet/), sowie die "Langfristszenarien 3" (Szenario TN-Strom) des [BMWK](https://www.langfristszenarien.de/enertile-explorer-de/). Bis 2045 ist der Wärmepumpen-Zubau in den BMWK-Langfristszenarien besonders hoch, in der Dena-Leitstudie ist er am niedrigsten.

<iframe title = "Abbildung Wärmepumpen" src="../../../docs/germany/figures/hp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Anteile im Wärmebereich

Die Ampel-Regierung strebt "einen sehr hohen Anteil Erneuerbarer Energien bei der Wärme" an. Als quantiatives Ziel für 2030 enthält der Koalitionsvertrag jedoch kein explizites Ziel für erneuerbare Energien, sondern die Formulierung, dass bis zum Jahr 2030 50 Prozent der Wärme "klimaneutral" erzeugt werden müssen. Da andere Optionen wie importierter klimaneutraler Wasserstoff oder CO<sub>2</sub>-Abscheidung im Wärmebereich bis 2030 jedoch eher unrealistisch erscheinen, interpretieren wir das genannte Ziel hier daher leicht abweichend vom Wortlaut als Erneuerbare-Energien-Ziel.

Im Jahr 2021 betrug der Anteil der erneuerbaren Energien am Endenergieverbrauch für Wärme und Kälte (einschließlich Fernwärmeverbrauch) den Daten der [AGEE-Stat](https://www.bmwk.de/Redaktion/DE/Dossier/erneuerbare-energien#entwicklung-in-zahlen) zufolge 15,7 Prozent. Bis 2030 muss dieser Anteil demnach jährlich um fast vier Prozentpunkte steigen und damit sehr viel stärker als in den vergangenen Jahren, um das Ziel zu erreichen. Derzeit liegt die Entwicklung jedoch sehr deutlich unter diesem Zielpfad.

<iframe title = "Abbildung Anteil erneuerbare Energien Wärme" src="../../../docs/germany/figures/resshare_heat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### ... in verschiedenen Klimaneutralitätsstudien

Für einen weiteren Blick in die Zukunft über das Jahr 2030 hinaus, können Klimaneutralitätsstudien herangezogen werden, die die Realisierbarkeit von Klimaneutralität im Jahr 2045 für Deutschland untersuchen. Die untenstehende Grafik zeigt die Studien von Agora, Ariadne, BDI sowie McKinsey für den Wärme- und Gebäudesektor. 

<iframe title = "Abbildung Anteil Wärme Studien" src="../../../docs/germany/figures/heat_resshares_scenarios.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Die Ariadne-Studie weist den Anteil erneuerbarer Energien im Verlauf für den Wärmesektor auf, der von etwa 13 Prozent im Jahr 2020 auf etwa 55 Prozent im Jahr 2030 und im Jahr 2045 auf rund 100 Prozent steigt. Die Szenarien von Agora, BDI und McKinsey beziehen sich auf den Gebäudesektor und differenzieren zusätzlich nach Wärmeerzeugern. In den Szenarien ist der Anteil von Wärmepumpen am größten, der im Jahr 2030 bereits auf 20 beziehungsweise 25 Prozent ansteigt und im Jahr 2045 mit rund 50 Prozent die dominierende Technologie ist.

## Emissionen

Mittels der Emissionsdaten für den Gebäudesektor sowie Annahmen zur Flächenentwicklung kann zudem die Intensität der Treibhausgasemissionen (THG-Emissionen) pro Quadratmeter abgeleitet werden, die auf dem Weg zur Klimaneutralität eine hilfreiche Bezugsgröße sein kann. Die unstehende Grafik zeigt die sektoralen THG-Emissionen in Kilogramm (kg) pro Quadrameter (m2) nach den Klimaneutralitätszenarien von Agora, Ariadne, BDI und dena. Für die Flächenentwicklung wurde der Durchschnitt der Ariadne und Agora Szenarien verwendet. Die THG-Intensität sinkt in den Szenarien für private Haushalte sowie den Sektor Gewerbe, Handel und Dienstleistungen (GHD) von durchschnittlich 22kg/m2 im Zeitraum 2018-20 auf 13 kg/m2 im Jahr 2030 und auf etwa 0,45 kg/m2 im Jahr 2045.

<iframe title = "Abbildung Emissionen Gebäude Sektor" src="../../../docs/germany/figures/emissions_buildings_scenarios.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Für die THG-Intensität für private Haushalte kann auch ein historischer Vergleich angestellt werden. Die untenstehende Grafik zeigt die hisotrischen Treihbausgasemissionen pro Quadratmeter für die historische Entwicklung seit 1990 auf Basis von Daten des Umweltbundesamts sowie die Entwicklung gemäß der Szeanrien für Klimaneutralität im Jahr 2045 von Agora und Ariadne. Wie der Grafik zu entnehmen ist, ist die THG-Intensität im Zeitraum von 2015 bis 2020 mit etwa 23 kg/m2 unverändert geblieben. Allerdings sinkt sie gemäß der Szenarien für KN 2045 bereits im Jahr 2025 auf etwa 18 kg/m2 und im Jahr 2040 auf unter 1 kg/m2.Für die Emissionsdaten ausschließlich privater Haushalte für Klimaneutralität im Jahr 2045 wurden die Ariadne-Daten genutzt.Für die Flächenentwicklung für private Haushalte Klimaneutralität im Jahr 2045 wurden die Agora-Daten genutzt.

<iframe title = "Abbildung Emissionen Intensität Gebäude Sektor" src="../../../docs/germany/figures/emissions_buildings_persqm_scenarios.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
