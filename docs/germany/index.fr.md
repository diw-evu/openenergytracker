---
description: Objectifs énergétiques dans l'accord de coalition du gouvernement allemand par rapport à la situation actuelle

hide:
#  - navigation
#  - toc
---

# Allemagne

=== "Auteurs"

    [Alexander Roth](../../docs/about/authors.md), [Wolf-Peter Schill](../../docs/about/authors.md)

Dans leur [accord de coalition](https://www.spd.de/fileadmin/Dokumente/Koalitionsvertrag/Koalitionsvertrag_2021-2025.pdf) du 24 novembre 2021, les partis de la coalition dite "en feu tricolore" (ndlt. en allemand "Ampel-Koalition") se sont fixé divers objectifs quantitatifs pour le secteur énergétique allemand. Nous les suivons ici depuis fin 2021 - initialement sous le nom de **KoaVTracker** (KoaV pour accord de coalition, en allemand "Koalitionsvertrag"). Toutefois, le gouvernement allemand s'est fixé de plus en plus d'objectifs supplémentaires qui vont au-delà de l'accord de coalition. De plus, à l'avenir, nous aimerions couvrir d'autres pays. C'est pourquoi nous avons décidé de renommer notre plateforme de données. Depuis juin 2022, elle s'appelle **Open Energy Tracker**.

!!! info "Ampel-Monitor Energiewende par le DIW Berlin, basé sur l'Open Energy Tracker"

    Depuis juillet 2022, un produit appelé "[Ampel-Monitor Energiewende](https://www.diw.de/de/diw_01.c.841560.de/ampel-monitor_energiewende.html)", basé sur les données recueillies dans l'Open Energy Tracker est accessible sur le site du DIW Berlin. Ce site web dédié s'accompagne d'une série de publications.

Dans l'Open Energy Tracker, nous représentons graphiquement certains des objectifs gouvernementaux, dont la plupart sont spécifiés pour l'année 2030, et nous les comparons régulièrement avec ce qui est effectivement réalisé. Nous nous concentrons en premier lieu sur les objectifs dans le domaine de l'expansion des énergies renouvelables, de la mobilité électrique et de l'électrolyse. En outre, au vu de l'actualité, nous suivons la situation des importations de gaz. Pour ces indicateurs, des données actualisées sont disponibles en partie à fréquence mensuelle, en partie à fréquence annuelle uniquement. Pour des raisons purement illustratives, et dans la mesure où les trajectoires exactes n'ont pas été spécifiées par le gouvernement fédéral pour la plupart des indicateurs, une progression linéaire entre l'état actuel et l'année cible (généralement décembre 2030) est représentée dans la plupart des graphiques. Nous présentons par ailleurs des projections des tendances actuelles calculées en utilisant la méthode des moindres carrés ordinaires (MCO). 

<center>
<iframe title = "Situation actuelle comparée aux objectifs pour 2030" src="../../../docs/germany/figures/fig_reached.html" width="75%" height= "400px" frameBorder="0" loading = "lazy"></iframe>
</center>

Pour pratiquement tous les indicateurs principaux, il existe un écart important entre la situation actuelle et les objectifs du gouvernement pour 2030. Cet écart est particulièrement grand en ce qui concerne la puissance installée de l'électrolyse et le parc de voitures électriques à batterie et de bornes de chargement publiques. De même, la puissance installée de l'énergie éolienne en mer et de l'énergie photovoltaïque d'une part et le parc de pompes à chaleur d'autre part ne représentent actuellement chacun qu'un peu plus d'un quart des objectifs fixés pour l'année 2030.

Les objectifs de la coalition et les résultats obtenus jusqu'à présent peuvent également être comparés, pour certains indicateurs, aux résultats des [scénarios Ariadne](https://ariadneprojekt.de/publikation/deutschland-auf-dem-weg-zur-klimaneutralitat-2045-szenarienreport/). Il s'agit d'analyses de scénarios réalisées dans le cadre du projet Copernicus [Ariadne](https://ariadneprojekt.de/) financé par le ministère fédéral allemand de l'Enseignement et de la Recherche (*Bundesministerium für Bildung und Forschung*, BMBF). Ces analyses montrent différentes trajectoires vers la neutralité climatique pour l'Allemagne à horizon 2045. Nous représentons de surcroît le corridor défini par tous les scénarios Ariadne ainsi que les résultats du modèle de référence d'Ariadne correspondant au scénario *Mix Technologique* (en allemand: *Technologiemix-Szenario* 8Gt_Bal). Pour les scénarios Ariadne, les données ne sont en principe disponibles que par incrément de cinq ans (2020, 2025, 2030, ...). Nous relions les années de référence en supposant une trajectoire linéaire. Les données des scénarios Ariane sont disponibles jusqu'en 2045, ce qui permet d'avoir une vision à plus long terme des évolutions plausibles des différents indicateurs. D'autres visualisations ainsi que des données sur tous les scénarios Ariadne sont disponibles sur des sites web externes tels que [Pathfinder](https://pathfinder.ariadneprojekt.de/) et [Scenario Explorer](https://data.ece.iiasa.ac.at/ariadne). À l'avenir, de nouveaux indicateurs et des trajectoires supplémentaires sont susceptibles d'être mis en ligne.
