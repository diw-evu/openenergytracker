---
hide:
# - navigation
  - toc
---
# Current natural gas consumption in the residential and commercial sectors

=== "Authors"

    [Alexander Roth](../../docs/about/authors.md), [Felix Schmidt](../../docs/about/authors.md)

=== "Citation suggestion"

    ``Alexander Roth & Felix Schmidt (2023) - "Current natural gas consumption and savings by residential and commercial sectors". Published on OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/de/docs/germany/naturalgas/' [website]``

    or

    [``Roth, Alexander, and Felix Schmidt. "Not only a mild winter: German consumers change their behavior to save natural gas." Joule 7, no. 6 (2023): 1081-1086.``](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

!!! Success ""
    
    The data of this section is updated every Thursday.


In order to avoid a gas shortage in the winter of 2022/23 after the delivery stop of Russian gas, Germany not only had to find alternative suppliers but also had to find ways to curb gas demand. This page shows weekly and cumulative natural gas consumption in residential and commercial sectors as well as savings relative to 2018/21. The info box at the end of the page discusses data and methodology used for the presented analysis. 

## Weekly natural gas consumption

A direct comparison of consumption levels between years does not allow determining the level of (behavioural) savings, especially so for residential and commercial consumers whose consumption is highly depedent on outside temperatures. A timely evaluation of saving efforts therefore requires a statistical weather correction of consumption data.

Based on consumption and weather data from 2018/21, we estimate the relationship between outside temperatures and residential and commercial natural gas consumption using a linear forest method (see methodology box below). We use the estimated model to predict the expected natural gas demand given current temperatures if consumers had not changed their behaviour relative to the 2018/21 period. Comparing the predicted demand to actual consumption allows infering levels of gas savings. We focus on the representive periods from September to June, as gas is mainly used for heating in the sectors of interest. 

<iframe title = "Figure Gas Consumption Households" src="../../../docs/germany/figures/fig_gas_consumption.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

The figure shows that the expected natural gas consumption (dotted line) and the actual natural gas consumption (black line). Whenever the dotted line is above the black, households and commercial customers currently **consume less gas** than would have been expected at current temperatures based on 2018/21 data. In the heating season 2022/23, we recorded savings for almost every week.

## Savings: weather and behaviour components

<iframe title = "Figure Gas Savings Behavior and Weather" src="../../../docs/germany/figures/fig_gas_savings_abs.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

Using the estimated "expected" consumption, we can decompose natural gas savings of the residential and commercial sector in 2022 into a "weather component" and a "behaviour component". In contrast to an earlier version of this page, we now show a decomposition of *absolute* savings, as these are relevant towards the avoidance of a gas shortage. Especially during cold winter weeks, we observe small relative but large absolute savings (cf. [Roth and Schmidt (2023)](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)). The left panel of the figure shows significant variation in savings between calendar weeks during the winter 2022/23. The cold spell in December 2022 stands out particularly. The right panel shows total savings by heating season. It is evident that savings during the winter of 2022/23 were predominantly down to behavioural changes. Warmer weather also contributed but only in a minor capacity.

## Cumulative consumption

<iframe title = "Figure needed gas savings" src="../../../docs/germany/figures/fig_gas_reached.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

The figure above shows cumulative consumption for the period from September to June. Consumption remained far below previous levels throughout the 2022/23 season.

!!! info ""

    Our data is also shown by the two German newspapers in their respective energy dashboards:
    
    - [Süddeutsche Zeitung (SZ)](https://www.sueddeutsche.de/projekte/artikel/wirtschaft/erdgas-versorgung-ernergiekrise-daten-e426783/#einsparungen)
    - [Stuttgarter Zeitung](https://stuttgarter-zeitung.de/energiedaten) 

!!! info ""
    
    The authors of this page have published an article on the topic in the academic journal [Joule](https://www.cell.com/joule/) in June 2023. Small descrepancies between the results shown here and in the article arise from minor differences in methodology and data: [Roth, Alexander, and Felix Schmidt. "Not only a mild winter: German consumers change their behavior to save natural gas." Joule 7, no. 6 (2023): 1081-1086.](https://www.cell.com/joule/pdf/S2542-4351(23)00173-3.pdf)

??? info "Methodology and Data"

    === "Methodology"

        Using a "linear forest", a machine learning method, we estimate residential and commercial gas consumption based on average, maximum, and minimum daily temperatures for the current and the last three days. We aggregate data from hundreds of weather stations across Germany using the median to prevent bias from extreme data points, such as from the Zugspitze. In addition, we control for the current calendar month and weekend or public holidays to account for the seasonality inherent in gas consumption (beyond temperature variations) and different consumption behavior on non-working days.

        We train our model using daily data between July 2018 and December 2021. We obtain temperature data from the German Weather Service and gas consumption data from Trading Hub Europe. We validated the quality of the model's prediction against the actual consumption levels between January and June 2018 to determine the best-performing model. In this way, we compared a range of machine learning models, including a neural network, a LASSO estimator, linear trees, gradient boosting, random forests, and linear forests. A linear forest specification had the lowest mean squared error and thus performed the best.

        We use the resulting model to predict consumption in 2022. It is not trivial to determine the cutoff time after which we would expect actual natural gas consumption to deviate from the consumption estimated based on previous years. Comparable publications start forecasting as early as September 2021, when gas prices began to rise at European gas trading hubs. Another approach would be to select the beginning of the Russian war against Ukraine. We only consider households and commercial customers who are usually only affected by wholesale market developments with a long delay. Therefore, we deem it plausible that including training data through to December 2021 will not introduce bias into the forecast.

        We compare our forecast with consumption data from the German Federal Network Agency in weekly resolution, as it provides more up-to-date data than the Trading Hub Europe.

        More details on model selection and estimation can be found [in this notebook](https://gitlab.com/diw-evu/oet/openenergytracker/-/blob/main/scripts/static/gas_savings_analysis.ipynb).

        We convert the cumulative gas savings based on behavioral changes in consumption to LNG shipments by assuming an average vessel capacity of [170 000 cubic metres](https://www.igu.org/resources/world-lng-report-2022/). This corresponds to an energy of nearly 1.2 TWh (not considering losses during transportation and regasification).

    === "Data"

        - **Natural Gas Consumption**: [Bundesnetzagentur - Gas consumption of household and commercial customers in GWh/day, weekly average](https://www.bundesnetzagentur.de/DE/Gasversorgung/aktuelle_gasversorgung/_svg/GasverbrauchSLP_woechentlich/Gasverbrauch_SLP_W_2023.html)
        - **Previous years' natural gas consumption**: [Trading Hub Europe - Aggregate Consumption Data Publication - Consumption Data SLP Area](https://www.tradinghub.eu/de-de/Ver%C3%B6ffentlichungen/Transparenz/Aggregierte-Verbrauchsdaten)
        - **Temperature**: [Deutscher Wetterdienst Open Daten Portal](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/historical/); Daily station measurements of mean, minimum, and maximum air temperature at 2 m in °C.
