---
hide:
#  - navigation
#  - toc
---

# Daten
## Legislativer Rahmen 

*Gesetz zur Energiewende für grünes Wachstum*: [LTECV](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) (veröffentlicht im Amtsblatt am 18. August 2015) 

*Mehrjährige Energiepläne* (PPE): 

* [PPE1](https://www.ecologie.gouv.fr/sites/default/files/PPE%20int%C3%A9gralit%C3%A9.pdf), veröffentlicht im Amtsblatt am 28. Oktober 2016, für die Zeiträume 2016-2018 und 2019-2013.

* [PPE2](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), veröffentlicht im Amtsblatt am 21. April 2020, für die Zeiträume 2019-2023 und 2023-2028.

*Nationale Strategie für geringen Kohlenstoffausstoß*: [SNBC](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc), die 2015 (SNBC-1) verabschiedet und 2018-2019 (SNBC-2) überarbeitet wurde, um das Ziel der Klimaneutralität im Einklang mit dem Pariser Abkommen aufzunehmen.

*Energie-Klimagesetz*: [LEC](https://www.ecologie.gouv.fr/loi-energie-climat), verabschiedet am 8. November 2019.

*Energiegesetz*: verfügbar unter [Légifrance](https://www.legifrance.gouv.fr/codes/texte_lc/LEGITEXT000023983208/2011-07-31)

## Erneuerbare Energien
### Installierte Leistung erneuerbarer Energien

Photovoltaik: [Dashboard: solar PV](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690) des Ministeriums für den ökologischen Wandel. Ausgabe für das dritte Quartal 2024 (November 2024).

Windkraft an Land: [Dashboard: Windenergie](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) des Ministeriums für den ökologischen Wandel. Ausgabe für das dritte Quartal 2024 (November 2024).

Windkraft auf See: [Dashboard : Windenergie](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/5689) des Ministeriums für den ökologischen Wandel. Ausgabe für das dritte Quartal 2024 (November 2024).

Wasserkraft:

* [Entwicklung des installierten Parks der Stromerzeugung in Frankreich nach Sparten](https://analysesetdonnees.rte-france.com/production/synthese), Daten des Réseau de Transport d'Électricité (RTE) (Januar 2024). 

* [Verteilung des Bestands und der durchschnittlichen Wasserkraftproduktion nach Anlagentypen](https://www.ecologie.gouv.fr/hydroelectricite), Daten des Ministeriums für den ökologischen Übergang und die Kohäsion der Territorien (April 2024).


### Anteile erneuerbarer Energien

Anteil am Bruttoendenergieverbrauch: [Kennzahlen für erneuerbare Energien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), Ausgabe 2024 (August 2024).

Anteil am Endenergieverbrauch für Wärme: [Kennzahlen für erneuerbare Energien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), Ausgabe 2024 (August 2024).

## Erneuerbare Wärme 

Anteil der erneuerbaren Energien am Bruttoendverbrauch von Wärme: [Kennzahlen für erneuerbare Energien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), Ausgabe 2024 (August 2024).

Wärmepumpen : 

* Bestand an in Betrieb befindlichen Wärmepumpen: Online-Datenbank von [EurObserv'ER](https://www.eurobserv-er.org/online-database/).

* Jährlicher Absatz von Wärmepumpen und Wärmeerzeugung: [Kennzahlen für erneuerbare Energien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), Ausgabe 2024 (August 2024).

## Elektromobilität 

Ladeinfrastruktur: [Nationales Barometer der öffentlich zugänglichen Ladeinfrastrukturen](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-recharge)

Neuzulassungen und Marktanteile: [Zulassungsbarometer](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-immatriculations)

Geschätzte Anzahl privater Ladepunkte in Frankreich: [Enedis Open Data](https://data.enedis.fr/pages/points-de-charge/)

## Atomkraft 

Anteil der Atomkraft an der Stromerzeugung: [Jährliche nationale Erzeugung nach Sektoren](https://odre.opendatasoft.com/explore/dataset/prod-national-annuel-filiere/information/) von [Réseau de Transport d'Électricité (RTE France)](https://www.rte-france.com/), Daten verfügbar auf der Plattform [Open Data Réseaux Énergies (Odré)](https://opendata.reseaux-energies.fr/).

## Energieverbrauch 

Fossiler Primärenergieverbrauch: [Energiekennzahlen - Ausgabe 2024](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (September 2024).

Endenergieverbrauch: [Energiekennzahlen - Ausgabe 2024](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (September 2024).

## Treibhausgasemissionen

Treibhausgasemissionen und Budgets: Secten-Daten aus [CITEPA](https://www.citepa.org/fr/secten/), Ausgabe 2024 (Juni 2024).

Treibhausgasemissionen aus der Stromerzeugung: [RTE-Daten](https://analysesetdonnees.rte-france.com/emission/emission-ges), monatliche Auflösung, alle Sektoren zusammen (Dezember 2024). 

Kohlenstoffintensität der Stromerzeugung: [RTE-Daten](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs) (Dezember 2024). 

## Außenhandel

Elektrizitätsimporte und -exporte in Frankreich: [RTE-Daten](https://analysesetdonnees.rte-france.com/marche/echanges-commerciaux), für die Grenze Belgien/Deutschland, monatliche Auflösung (Dezember 2024). 
