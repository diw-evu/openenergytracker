---
hide:
#  - navigation
#  - toc
---

# Nuclear power

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

??? info "Scenario Corridor"

    === "GENeSYS-MOD"

        For comparison, each graphic shows the corridor of scenarios developed by a team of researchers from the Technical University of Berlin using the [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) model as part of the European [openENTRANCE](https://openentrance.eu/about-openentrance/) project.

    === "Scenarios"

        For a given indicator, the scenario corridor displays the minimum and maximum values of projections at five-year intervals between 2025 and 2050 among four scenarios. The scenarios considered are: _Directed Transition_, _Gradual Development_, _Societal Commitment_ and _Techno-Friendly_.

        More information on the definition of the scenarios can be found [here](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).


## Share of nuclear energy in electricity production

France has 56 nuclear reactors located at 18 sites, with a total installed capacity of 61.4 GW. According to the [2022 electricity balance data](https://analysesetdonnees.rte-france.com/bilan-electrique-production) published by the Réseau de Transport d'Électricité [(RTE)](https://www.rte-france.com/eco2mix) in France, nuclear generation accounted for 62.7% of electricity production in France in 2022.

The Energy Transition Law for Green Growth [(LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte), adopted in August 2015, aimed to diversify France's electricity mix by reducing the share of nuclear power in electricity production to 50% by 2025. However, the energy-climate law [(LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) in November 2019 postponed this objective to 2035. The article concerning this target was repealed by the Law of 22 June 2023 (Law No. 2023-491), along with  another article that set a cap on the total nuclear power generation capacity of 63.2 GW. As a result, there is no longer any cap in force. This legislative change reflects the French government's vision for the energy transition, as detailed by President Emmanuel Macron in his Belfort speech on 10 February 2022. The plan includes extending existing nuclear reactors and constructing new ones, making nuclear power a key component of the transition.

The [Pluriannual Energy Programme](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) (PPE2) specified the closure of 14 nuclear reactors in France by 2035. However, the draft of the next version of the PPE (PPE3) proposes extending the operation of existing reactors beyond 50 or 60 years and constructing six new EPR2 reactors, with work starting by 2025. Finally, there are plans to construct eight additional reactors with a total capacity of 13 GW. A final decision on this matter will be made by 2026. Overall, it is expected that nuclear power generation will represent between 360 TWh and 400 TWh by 2030.

Despite the removal of this target for reducing the share of nuclear power in France's electricity generation, we are keeping the corresponding graph in order to monitor the development of this indicator, which we consider to be of interest.

<iframe title = "Figure nuclear share in the power production" src="../../../docs/france/figures/nuclear.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

