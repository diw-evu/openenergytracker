---
hide:
#  - navigation
#  - toc
---

# Déploiement des énergies renouvelables (EnR)

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

??? info "Corridor de scénarios"

    === "Futurs énergétiques 2050"

        Afin de mettre les objectifs du gouvernement en perspective, nous ajoutons sur certains graphique le corridor des scénarios [*Futurs énergétiques 2050*](https://rte-futursenergetiques2050.com/) développés par le Réseau de Transport d'Électricité français (RTE). 
        
        Le corridor ici visualisé correspond aux valeurs minimales et maximales parmi les six scénarios reposant sur la trajectoire de référence pour la consommation. Ces six scénarios envisagent des mix de production d'électricité variés, allant du 100% renouvelable au 50% nucléaire/50% renouvelables. 
        
        L'exercice de modélisation de RTE offre des valeurs pour certaines variables (e.g. capacité photovoltaïque installée) à horizon 2030, 2040 et 2050. Nous supposons une trajectoire linéaire entre ces horizons pour former le corridor. 

    === "GENeSYS-MOD"

        Pour comparaison, chaque graphique permet de visualiser le corridor des scénarios développés par une équipe de chercheurs de l'université technique de Berlin à l'aide du modèle [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) dans le cadre du projet européen [openENTRANCE](https://openentrance.eu/about-openentrance/).

        Pour un indicateur donné, le corridor de scénarios permet de visualiser les valeurs minimales et maximales des projections à intervalle de cinq ans entre 2025 et 2050 parmi quatre scénarios. Les scénarios considérés sont les suivants : _Directed Transition_, _Gradual Development_, _Societal Commitment_ et _Techno-Friendly_.

        Plus d'informations sur la définition des scénarios sont à trouver [ici](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

        
## Photovoltaïque

La [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), dont la dernière révision en date a été adoptée en avril 2020, fixe un objectif de 20,1 GW de capacité photovoltaïque installée en 2023 ainsi qu'un objectif bas (resp. haut) pour 2030 de 35,1 GW (resp. 44 GW). La PPE fournit également la répartition de la capacité photovoltaïque installée entre panneaux au sol et panneaux sur toitures. Pour 2023, les panneaux au sol devraient atteindre une capacité installée de 11,6 GW (resp. 8,5 GW pour les panneaux sur toitures). En 2028, les panneaux au sol (resp. panneaux sur toitures) représenteraient entre 20,6 et 25 GW (resp. entre 14,5 et 19 GW) soit une surface estimée entre 330 et 400 km<sup>2</sup> (resp. entre 150 et 200 km<sup>2</sup>). 

La [Stratégie Française pour l'Énergie et le Climat](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) fournit également des objectifs pour 2030 et 2035. Ces objectifs sont susceptibles d'être ceux retenus dans la PPE3 mais n'ont pas encore officiellement été adoptés par voie législative. En 2030, la SFEC prévoit fixe un objectif de capacité installée entr 54 et 60 GW et entre 75 et 100 GW pour 2035. Les objectifs 2030 se situent au-dessus du corridor des scénarios *Futurs énergétiques 2050* ainsi que l'objectif haut pour 2035. 

D'après les [données et études statistiques du Ministère de la Transition Écologique](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690), la capacité photovoltaïque installée en France à la fin du troisième trimestre 2024 s'élève à 22,9 GW, soit 0,6 GW de plus que ce que prévoirait un déploiement linéaire des nouvelles installations entre décembre 2023 et décembre 2028. Par ailleurs, la tendance sur la dernière année écoulée montre qu'il y a une nette accélération des nouvelles installations photovoltaïques par rapport à la tendance sur les cinq dernières années. Une telle dynamique, si elle était maintenue sur les cinq années à venir permettrait presque d'atteindre l'objectif haut 2028.

Dans son [discours de Belfort](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique), en date du 10 février 2022, le président de la République francaise, Emmanuel Macron, a annoncé vouloir atteindre une capacité installée photovoltaïque de 100 GW en 2050. Atteindre un tel objectif supposerait de raccorder chaque trimestre 740 MW additionnels, c'est-à-dire garder le même rythme que celui nécessaire à atteindre les objectifs bas de la PPE entre 2023 et 2028, sous l'hypothèse supplémentaire que l'objectif bas de la PPE en 2028 sera atteint à temps. Précisons toutefois que cet objectif n'a pas valeur légale pour l'instant. 

<iframe title = "Figure capacité solaire photovoltaïque installée en France" src="../../../docs/france/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Énergie éolienne

### Terrestre

Dans la dernière révision en date de la [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), adoptée en avril 2020, l'objectif pour la capacité installée d'énergie éolienne terrestre s'élève à 24,1 GW en 2023 et entre 33,2 GW et 34,7 GW en 2028. 

La [Stratégie Française pour l'Énergie et le Climat](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) fournit également des objectifs pour 2030 et 2035. Les objectifs proposés pour 2030 semblent reprendre les objectifs de 2028, repoussant ainsi de deux ans l'atteinte des objectifs prévus par la PPE2. Les objectifs pour 2035 prévoient entre 40 et 45 GW de capacité installée, ce qui correspond plus ou moins au même rythme de construction prévu par la PPE2 sur la période 2023-2028. L'objectif haut pour 2035 se trouve au-dessus du corridor de scénarios *Futurs énergétiques 2050* tandis que l'objectif bas se trouve au milieu du corridor.

D'après les [données et études statistiques du Ministère de la Transition Écologique](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689), au troisième trimestre 2024, la puissance installée d'éolien terrestre en France était de 22,9 GW contre les 25,6 GW prévus par une progression linéaire entre décembre 2023 et décembre 2028, soit un retard de 2,5 GW. À en croire la comparaison de la tendance sur les quatre derniers trimestres avec la tendance sur les cinq dernières années (2018-2022), le déploiement de l'éolien terrestre en France a récemment ralenti. En continuant au rythme des douze derniers mois, l'objectif bas pour 2028 (33,2 GW installés) serait manqué d'environ 6 GW. Une accélération du déploiement de l'éolien terrestre est donc absolument nécessaire.

<iframe title = "Figure capacité éolien terrestre installée en France" src="../../../docs/france/figures/windon.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### En mer 

En ce qui concerne l'éolien en mer, la France affiche un retard grandissant par rapport aux objectifs fixés par la dernière version en date de la [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) adoptée en avril 2020. En effet, selon la PPE 2,4 GW d'éolien en mer devraient être posés d'ici fin 2023 et entre 5,2 GW et 6,2 GW d'ici fin 2028. 

La [Stratégie Française pour l'Énergie et le Climat](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) fournit également des objectifs pour 2030 et 2035, bien qu'elle ne fournisse qu'un objectif pour chaque échéance et non une fourchette comme c'est le cas pour le photovoltaique et l'éolien terrestre. L'objectif pour 2030 est de 4 GW, ce qui en-dessous des objectifs fixés par la PPE2 pour 2028. L'objectif pour 2035 est de 18 GW, requiérant une accélération massive du raccordement de l'éolien terrestre sur la période 2030-2035 avec en moyenne 700 MW par mois. Cet objectif 2035 se trouve en haut du corridor des scénarios *Futurs énergétiques 2050*.

D'après les [données et études statistiques du Ministère de la Transition Écologique](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689), il y avait à la fin du troisième trimestre 2024 1,48 GW d'éolien posé en mer. La première ferme éolienne en mer française au large de Saint-Nazaire a été inaugurée en septembre 2022. Ce premier parc est constitué de 80 éoliennes. Au cours du mois de janvier 2023, le parc éolien en mer de Saint-Nazaire a produit plus de 180 000 MWh. En mai 2023, un deuxième parc éolien en mer a commencé sa phase d'installation en mer de Bretagne, dans la baie de Saint-Brieuc. Ce parc est constitué de 62 éoliennes d'une capacité totale de 496 MW. Un troisième parc, le parc éolien de Fécamp, compte 71 éoliennes pour une puissance totale de 497 MW a également été mis en service en 2023. 

Si l'installation de ces deux premiers parcs éoliens en mer est encourageant et semble aller dans le bon sens, la France accuse toujours un retard important en la matière y compris comparé aux objectifs fixés par la PPE. En dépit des 1.48 GW installés en 2022-2023, le déficit de capacité installée reste considérable (environ 1.3 GW) comparé à ce que prévoirait une trajectoire linéaire entre les objectifs de décembre 2023 et décembre 2028 car aucune nouvelle installation n'a été raccordée depuis le deuxième trimestre 2023.

Dans son [discours de Belfort](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique), en date du 10 février 2022, le président de la République francaise, Emmanuel Macron, a annoncé vouloir atteindre une capacité installée d'éolien en mer de 40 GW en 2050. Atteindre un tel objectif supposerait de raccorder chaque trimestre 400 MW additionnels, soit légèrement moins que le parc éolien en mer de Saint-Nazaire (480 MW), sous l'hypothèse supplémentaire que l'objectif bas de la PPE en 2028 sera atteint à temps. Précisons toutefois que cet objectif n'a pas valeur légale pour l'instant. 

<iframe title = "Figure capacité éolien en mer installée en France" src="../../../docs/france/figures/windoff.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Hydraulique

L'énergie hydraulique est la première source d'électricité renouvelable en France et la deuxième technologie de production électrique après le nucléaire, avec une capacité installée d'environ 25,7 GW en 2023, sans compter les énergies marémotrices, d'après les [données de RTE](https://analysesetdonnees.rte-france.com/production/synthese). À l'heure actuelle, les lacs représentent la technologie la plus utilisée, suivie des technologies dites "au fil de l'eau" d'après les [statistiques](https://www.ecologie.gouv.fr/hydroelectricite) du ministère de la Transition Énergétique.  L'objectif fixé pour 2023 de 25,7 GW (énergies marémotrices inclues) a été atteint. Bien que les possibilités d'expansion de l'hydroélectricité en France semblent limitées, la PPE2 prévoit un léger accroissement des capacités installées à horizon 2028 pour atteindre entre 26,4 et 26,7 GW (énergies marémotrices inclues). Ces objectifs sont cohérents avec les projections des scénarios Futurs 2050 de RTE, qui prévoient tous une capacité hydraulique installée de 26,5 GW en 2030. Ces scénarios reposent également sur un accroissement des capacités hydroélectriques pour atteindre 28,2 GW en 2040 et 30,1 GW en 2050.

<iframe title = "Figure capacité hydraulique installée en France" src="../../../docs/france/figures/hydro_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Part des EnR

### dans la consommation finale brute d'énergie

Le [code de l'énergie (CE)](https://www.legifrance.gouv.fr/codes/id/LEGITEXT000023983208/) fixe à 23% la part des énergies renouvelables dans la consommation finale brute d'énergie en 2020. Par ailleurs, la [Loi énergie-climat (LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) fixe cette part à au moins 33% en 2030. 

D'après les [données et études statistiques du Ministère de la Transition Écologique](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), la France n'a pas atteint l'objectif fixé pour 2020 de près de 4 points de pourcentage. Les énergies renouvelables représentaient en 2023 22,2% de la consommation finale d'énergie brute en France, soit une augmentation de 1,7 points de pourcentage sur l'année 2023 par rapport à 2022. Toutefois, le jalon de 24% fin 2023 fixé par la PPE n'a donc pas été atteint. Le retard en matière de part des EnR dans la consommation finale brute d'énergie se maintient donc, étant en 2023 inférieur de 0,8 points de pourcentage au niveau prévu par un accroissement linéaire entre 2020 et 2030, d'ampleur légèrement moindre que le retard constaté en 2022. Cela signifie qu'une certaine accélération a eu lieu en 2023, bien que cette accélération n'ait pas été suffisante pour combler les retards accumulés dans le passé. Notons par ailleurs que la progression sur les cinq dernières années semble avoir été plus lente que la progression linéaire permettant d'atteindre l'objectif de 2030.

Dans la version adoptée en avril 2020 de la [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), la consommation finale brute d'énergie est estimée à 1637 TWh en 2023 et 1489 TWh en 2028. Si les mesures prévues dans la PPE sont effectivement mises en œuvre, la part des énergies renouvelables dans la consommation finale brute d'énergie devrait être de 389 TWh en 2023 (environ 24%) et entre 477 et 529 TWh en 2028 (environ 32 à 35%), ce qui signifierait que la France atteindrait probablement l'objectif fixé pour 2030.

<iframe title = "Figure part des EnR conso en France" src="../../../docs/france/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### dans la production d'électricité

La loi énergie-climat (LEC) de 2019 maintient l'objectif introduit dans la loi relative à la transition énergétique pour la croissance verte (LTECV) concernant la part des énergies renouvelables dans la production d'électricité. Cette part devra être de 40% à horizon 2030 en France. D'après [Les chiffres clés des énergies renouvelables - Édition 2024](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), publiés par le Ministère de la Transition énergétique, cette part s'élevait à la fin de l'année 2023 à environ 30%. La plus grande source d'électricité renouvelable reste l'hydraulique, représentant plus de 42% de l'électricité d'origine renouvelable en France en 2023. L'éolien terrestre quant à lui représente environ 33% de cette production d'origine renouvelable et le solaire photovoltaique environ 16%.

Les perspectives de déploiement des capacités hydrauliques étant limitées, la grande partie de l'accroissement futur de la part des renouvelables dans la production d'électricité devra provenir d'autres sources. Ceci étant, il semble pour l'heure que l'évolution de cet indicateur soit aligné avec les objectifs fixés par la législation en vigueur. 

<iframe title = "Figure share RES in power production" src="../../../docs/france/figures/respower.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>