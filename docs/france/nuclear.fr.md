---
hide:
#  - navigation
#  - toc
---

# Énergie nucléaire

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

??? info "Corridor de scénarios"

    === "GENeSYS-MOD"

        Pour comparaison, chaque graphique permet de visualiser le corridor des scénarios développés par une équipe de chercheurs de l'université technique de Berlin à l'aide du modèle [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) dans le cadre du projet européen [openENTRANCE](https://openentrance.eu/about-openentrance/).

    === "Scénarios"

        Pour un indicateur donné, le corridor de scénarios permet de visualiser les valeurs minimales et maximales des projections à intervalle de cinq ans entre 2025 et 2050 parmi quatre scénarios. Les scénarios considérés sont les suivants : _Directed Transition_, _Gradual Development_, _Societal Commitment_ et _Techno-Friendly_.

        Plus d'informations sur la définition des scénarios sont à trouver [ici](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Part du nucléaire dans la production d'électricité

La France compte actuellement 56 réacteurs nucléaires répartis sur 18 sites pour une capacité installée totale de 61,4 GW.- D'après les [données du bilan électrique 2022](https://analysesetdonnees.rte-france.com/bilan-electrique-production) publiées par le [Réseau de Transport d'Électricité (RTE)](https://www.rte-france.com/eco2mix) en France, la production nucléaire a représenté 62,7% de la production d'électricité en France en 2022.

Afin de poursuivre l'objectif de diversification du mix électrique en France, la [Loi de Transition Énergétique pour la Croissance Verte (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) adoptée en août 2015 avait fixé l'objectif de réduire la part du nucléaire dans la production d'électricité à 50% d'ici 2025. En novembre 2019, la [loi énergie-climat (LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) a acté le report de cet objectif à 2035 au lieu de 2025. Finalement, l’article concernant cet objectif a été abrogé par la loi du 22 juin 2023 (loi n°2023-491).  Cette loi a également abrogé un autre article fixant un plafond à la capacité totale de production d’électricité d’origine nucléaire de 63,2 GW. Il n'y a donc à présent plus de plafond en vigueur. Cette évolution législative reflète la vision de la transition énergétique du président de la République, Emmanuel Macron, et de son gouvernement, vision qu’il a notamment détaillée dans son discours de Belfort, prononcé le 10 février 2022, qui fait du nucléaire un pilier de la transition énergétique à la française en prévoyant le prolongement des réacteurs existant ainsi que la construction de nouveaux réacteurs.

Alors qu'il était précisé dans la [Programmation Pluriannuelle de l'Énergie (PPE2)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) que cet objectif impliquerait notamment de fermer 14 réacteurs nucléaires en France d'ici 2035, il semblerait que l'ébauche de la prochaine version de la PPE (PPE3) prévoit le prolongement de l'exploitation des réacteurs existants au-delà de 50 ou 60 ans ainsi que la construction de six nouveaux réacteurs EPR2 (avec un début des travaux à horizon 2025). Enfin, la construction de huit réacteurs supplémentaires d'une capacité totale de 13 GW est également à l'étude et devrait faire l'objet de travaux supplémentaires dans les années à venir afin de prendre une décision finale d'ici 2026. Au total, la production d'électricité d'origine nucléaire devrait représenter entre 360 TWh et 400 TWh en 2030.

En dépit de la suppression de cet objectif de réduction de la part du nucléaire dans la production d'électricité en France, nous conservons le graphique correspondant afin de suivre l'évolution de cet indicateur que nous jugeons intéressant.

<iframe title = "Figure capacité nucléaire installée en France" src="../../../docs/france/figures/nuclear.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>


