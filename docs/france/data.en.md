---
hide:
#  - navigation
#  - toc
---

# Data

## Legislative framework 

*Energy Transition Law for Green Growth*: [LTECV](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) (published in the Official Journal on August 18, 2015) 

*Multi-year energy plans* (PPE): 

* [PPE1](https://www.ecologie.gouv.fr/sites/default/files/PPE%20int%C3%A9gralit%C3%A9.pdf) published in the Official Journal on October 28, 2016, for the periods 2016-2018 and 2019-2013.

* [PPE2](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) published in the Official Journal on April 21, 2020, for the periods 2019-2023 and 2023-2028.

*National Low Carbon Strategy*: [SNBC](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc) adopted in 2015 (SNBC-1) and revised in 2018-2019 (SNBC-2) to include the objective of carbon neutrality in accordance with the Paris Agreement.

*Energy-Climate Law*: [LEC](https://www.ecologie.gouv.fr/loi-energie-climat) adopted on November 8, 2019.

*Energy Code*: available at [Légifrance](https://www.legifrance.gouv.fr/codes/texte_lc/LEGITEXT000023983208/2011-07-31)

## Renewable energy
### Renewable energy deployment

Photovoltaic: [Dashboard: solar PV](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690) from the Ministry of Ecological Transition. Third quarter 2024 edition (November 2024).

Onshore wind: [Dashboard : wind energy](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) from the Ministry of Ecological Transition. Third quarter 2024 edition (November 2024).

Offshore wind: [Dashboard : wind energy](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) from the Ministry of Ecological Transition. Third quarter 2024 edition (November 2024).

Hydropower: 

* [Evolution of installed electricity generation capacity in France by type of plant](https://analysesetdonnees.rte-france.com/production/synthese), data from Réseau de Transport d'Électricité (RTE) (January 2024). 

* [Breakdown of installed capacity and average hydroelectric production by type of facility](https://www.ecologie.gouv.fr/hydroelectricite), data from the French Ministry for Ecological Transition and Territorial Cohesion (April 2024).

### Share of renewable energy

Share in gross final energy consumption: [Key figures for renewable energy](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), 2024 edition (August 2024).

Share in final heat consumption: [Key figures for renewable energy](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), 2024 edition (August 2024).

## Renewable heat 

Share of renewable energy in gross final consumption of heat: [Key figures for renewable energy](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), 2024 edition (August 2024).

Heat pumps : 

* Stock of heat pumps in operation: [EurObserv'ER online database](https://www.eurobserv-er.org/online-database/).

* Annual sales of heat pumps and heat production: [Key figures for renewable energy](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), 2024 edition (August 2024).

## Emobility

Charging infrastructure: [National barometer of charging infrastructures open to the public](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-recharge)

New registrations and market shares: [Registration Barometer](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-immatriculations)

Estimation of private charge points in France: [Enedis Open Data](https://data.enedis.fr/pages/points-de-charge/)

## Nuclear energy 

Share of nuclear power in electricity production: [Annual national production by sector](https://odre.opendatasoft.com/explore/dataset/prod-national-annuel-filiere/information/) from [Réseau de Transport d'Électricité (RTE France)](https://www.rte-france.com/), data available on the [Open Data Réseaux Énergies (Odré)](https://opendata.reseaux-energies.fr/) platform.

## Energy consumption 

Primary fossil energy consumption: [Key energy figures - 2024 edition](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (September 2024).

Final energy consumption: [Key energy figures - 2024 edition](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (September 2024).

## Greenhouse gas emissions

Greenhouse gas emissions and budgets: Secten data from [CITEPA](https://www.citepa.org/fr/secten/), 2024 edition (June 2024).

Greenhouse gas emissions from electricity generation: [RTE data](https://analysesetdonnees.rte-france.com/emission/emission-ges), monthly resolution, all sectors combined (December 2024). 

Carbon intensity of electricity generation: [RTE data](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs) (December 2024). 

## Commercial exchange

Electricity imports and exports in France: [RTE data](https://analysesetdonnees.rte-france.com/marche/echanges-commerciaux), for the Belgium/Germany border, monthly resolution (December 2024). 
