---
hide:
#  - navigation
#  - toc
---

# Commercial trade

=== "Authors"

    Jan Czimmeck, [Adeline Guéret](../../docs/about/authors.md) 

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Germany and Belgium

Although France is traditionally a net exporter of electricity, trade fluctuates from month to month. According to [data](https://analysesetdonnees.rte-france.com/en/markets/imports-exports) from France's Réseau de Transport d'Électricité (RTE), France imports more electricity than it exports from Germany and Belgium during the autumn and winter, while the reverse is true in the spring and summer. The year 2022 is a notable exception. During that year, France was a net importer from Belgium and Germany throughout the year, including the spring and summer of 2022. This is probably a direct consequence of the shutdown of a large number of nuclear reactors due to technical problems and the historically low level of reservoirs connected to hydroelectric facilities. 

<iframe title = "Figure power trade" src="../../../docs/france/figures/powertrade.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>