---
hide:
#  - navigation
#  - toc
---

# Ausbau erneuerbarer Energie

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

??? info "Szenariokorridor"

    === "Futurs énergétiques 2050"

        Um die Ziele der Regierung zu verdeutlichen, fügen wir einigen Grafiken den Korridor der Szenarien [*Futurs énergétiques 2050*](https://rte-futursenergetiques2050.com/) hinzu, die vom französischen Übertragungsnetzbetreiber (*Réseau de Transport d'Électricité* auf Französisch, abgekürzt RTE) entwickelt wurden. 
        
        Der hier dargestellte Korridor entspricht den Minimal- und Maximalwerten der sechs Szenarien, die auf dem Referenzpfad für den Verbrauch basieren. Diese sechs Szenarien sehen unterschiedliche Erzeugungsmixe vor, die von 100 % erneuerbaren Energien bis zu 50 % Kernkraft/50 % erneuerbaren Energien reichen. 
        
        Die Modellierung von RTE liefert Werte für bestimmte Variablen (z. B. die installierte Photovoltaikkapazität) für 2030, 2040 und 2050. Wir gehen von einem linearen Verlauf zwischen diesen Zeithorizonten aus, um den Korridor zu bilden. 
    
    === "GENeSYS-MOD"

        Zum Vergleich zeigt jede Grafik den Korridor von Szenarien, die von einem Forscherteam der Technischen Universität Berlin mit dem Modell [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) im Rahmen des europäischen Projekts [openENTRANCE](https://openentrance.eu/about-openentrance/) entwickelt wurden.

        Der Szenariokorridor zeigt für einen bestimmten Indikator die minimalen und maximalen Werte der Projektionen in Fünfjahresintervallen zwischen 2025 und 2050 unter vier Szenarien an. Die betrachteten Szenarien sind: _Gerichteter Übergang_, _Graduale Entwicklung_, _Gesellschaftliches Engagement_ und _Techno-Friendly_.

        Weitere Informationen über die Definition der Szenarien finden Sie [hier](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

    
## Photovoltaik

Die [mehrjährige Programmplanung für Energie](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) (auf Französisch, PPE), dessen letzte Überarbeitung im April 2020 angenommen wurde, setzt ein Ziel von 20,1 GW installierter PV-Leistung im Jahr 2023 und ein niedriges (bzw. hohes) Ziel für 2030 von 35,1 GW (bzw. 44 GW). Die PPE enthält auch eine Aufschlüsselung der installierten PV-Kapazität nach Freiflächen- und Aufdachanlagen. Bis 2023 wird für Freiflächenanlagen eine installierte Leistung von 11,6 GW (bzw. 8,5 GW für Aufdachanlagen) erwartet. Im Jahr 2028 sollen Freiflächen- (bzw. Dach-) Module zwischen 20,6 und 25 GW (bzw. zwischen 14,5 und 19 GW) oder eine geschätzte Fläche zwischen 330 und 400 km<sup>2</sup> (bzw. zwischen 150 und 200 km<sup>2</sup>) ausmachen. 

Die [französische Energie- und Klimastrategie](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) (*Stratégie Française pour l'Énergie et le Climat*, abgekürzt SFEC) enthält ebenfalls Ziele für 2030 und 2035, die wahrscheinlich mit denen des PPE3 übereinstimmen, aber noch nicht formell gesetzlich verabschiedet wurden. Für 2030 sieht die SFEC ein Ziel für die installierte Kapazität zwischen 54 und 60 GW und für 2035 zwischen 75 und 100 GW vor. Die Ziele für 2030 liegen über dem Korridor der Szenarien *Futurs énergétiques 2050*, ebenso wie das hohe Ziel für 2035.

Nach Angaben des [Ministeriums für den ökologischen Wandel](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690) beläuft sich die in Frankreich installierte PV-Leistung am Ende des dritten Quartals 2024 auf 22,9 GW, d.h. 0,6 GW über dem Wert, der bei einer linearen Entwicklung der Neuinstallationen zwischen Dezember 2023 und Dezember 2028 zu erwarten wäre. Darüber hinaus zeigt der Trend des letzten Jahres, dass sich der Zubau von Photovoltaikanlagen im Vergleich zum Trend der letzten fünf Jahre deutlich beschleunigt hat. Setzt sich diese Dynamik in den nächsten fünf Jahren fort, wird das obere Ziel bis 2028 fast erreicht.

In seiner [Belfort-Rede](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique) vom 10. Februar 2022 kündigte der französische Staatspräsident Emmanuel Macron an, er wolle bis 2050 eine installierte Photovoltaikleistung von 100 GW erreichen. Um dieses Ziel zu erreichen, müssten pro Quartal 740 MW zusätzlich angeschlossen werden, d. h. es müsste das gleiche Tempo beibehalten werden, das für das Erreichen der niedrigen Ziele des PPE zwischen 2023 und 2028 erforderlich ist, wobei zusätzlich davon ausgegangen wird, dass das niedrige Ziel des PPE im Jahr 2028 pünktlich erreicht wird. Es sei jedoch darauf hingewiesen, dass dieses Ziel vorläufig keine Rechtskraft hat.

<iframe title = "Abbildung installierte Leistung Photovoltaik in Frankreich" src="../../../docs/france/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Windkraft

### an Land

Nach der letzten Überarbeitung der [mehrjährige Programmplanung für Energie](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) (PPE), die im April 2020 verabschiedet wurde, liegt das Ziel für die installierte Leistung der Windkraft and Land bei 24,1 GW im Jahr 2023 und zwischen 33,2 GW und 34,7 GW im Jahr 2028. 

Die [französische Energie- und Klimastrategie](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) enthält auch Ziele für 2030 und 2035. Die für 2030 vorgeschlagenen Ziele scheinen die gleichen wie die für 2028 zu sein, was eine Verschiebung um zwei Jahre gegenüber den in der PPE2 festgelegten Zielen bedeutet. Die Ziele für 2035 erfordern eine installierte Kapazität zwischen 40 und 45 GW, was in etwa der in der PPE2 für den Zeitraum 2023-2028 prognostizierten Zubaurate entspricht. Das hohe Ziel für 2035 liegt über dem Korridor des Szenarios *Futurs énergétiques 2050*, während das niedrige Ziel in der Mitte des Korridors liegt.

Nach Angaben des [Ministeriums für den ökologischen Wandel](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) lag die installierte Kapazität der Onshore-Windenergie in Frankreich im dritten Quartal 2024 bei 22,9 GW gegenüber dem Ziel von 25,5 GW, das bei einer linearen Entwicklung zwischen den Zielen von Dezember 2023 und Dezember 2028 erforderlich wäre, d.h. eine Verzögerung von 2,6 GW. Vergleicht man die Entwicklung der letzten vier Quartale mit der Entwicklung der letzten fünf Jahre (2018-2022), so hat sich der Ausbau der Onshore-Windenergie in Frankreich zuletzt verlangsamt. Bei einer Fortsetzung der Entwicklung der letzten zwölf Monate würde das niedrige Ziel für 2028 (33,2 GW installierte Leistung) um ungefähr 6 GW verfehlt. Der Ausbau der Onshore-Windenergie muss daher unbedingt beschleunigt werden.

<iframe title = "Abbildung installierte Leistung Windkraft an Land in Frankreich" src="../../../docs/france/figures/windon.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### auf See

Was die Windkraft auf See betrifft, so bleibt Frankreich immer weiter hinter den Zielen zurück, die in der letzten Fassung des im April 2020 verabschiedeten [Mehrjährigen Energieprogramms (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) festgelegt wurden. Dem PPE zufolge sollen bis Ende 2023 Windkraftanlagen auf See mit einer Leistung von 2,4 GW installiert werden; bis Ende 2028 soll eine Leistung zwischen 5,2 GW und 6,2 GW erreicht sein. 

Die [französische Energie- und Klimastrategie](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) legt ebenfalls Ziele für 2030 und 2035 fest, jedoch nur ein Ziel für jeden Zeitraum und keine Spanne wie bei der Photovoltaik und der Onshore-Windenergie. Das Ziel für 2030 liegt bei 4 GW und damit unter dem PPE2-Ziel für 2028. Das Ziel für 2035 liegt bei 18 GW, was eine massive Beschleunigung des Onshore-Windzubaus im Zeitraum 2030-2035 mit durchschnittlich 700 MW pro Monat erfordert. Das Ziel für 2035 liegt am oberen Ende des Korridors des Szenarios *Futurs énergétiques 2050*.

Laut den [Daten und statistischen Studien des französischen Ministeriums für den ökologischen Übergang](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) wurden Ende des ersten Quartals 2024 1,48 GW an Offshore-Windkraft installiert. Der erste französische Offshore-Windpark vor der Küste von Saint-Nazaire mit 80 Windkraftanlagen wurde im September 2022 eingeweiht.  Im Januar 2023 produzierte der Offshore-Windpark Saint-Nazaire mehr als 180.000 MWh. Im Mai 2023 begann die Installationsphase eines zweiten Offshore-Windparks im Meer vor der Bretagne, genauer gesagt in der Bucht von Saint-Brieuc. Der Windpark besteht aus 62 Windturbinen mit einer Gesamtkapazität von 496 MW. Dieser besteht aus 71 Windturbinen mit einer Gesamtleistung von 497 MW. Ebenfalls im Jahr 2023 wurde der Windpark Fécamp in Betrieb genommen.

Auch wenn die Installation der ersten beiden Offshore-Windparks ermutigend ist und in die richtige Richtung zu gehen scheint, liegt Frankreich in diesem Bereich immer noch weit hinter den in der PPE festgelegten Zielen zurück. Trotz der 1,48 GW, die in den Jahren 2022-2023 installiert werden sollen, ist die Lücke bei der installierten Kapazität immer noch beträchtlich (ca. 1,3 GW) im Vergleich zu dem, was ein linearer Pfad zwischen den Zielen von Dezember 2023 und Dezember 2028 vorsehen würde, da seit dem zweiten Quartal 2023 keine neuen Anlagen mehr angeschlossen wurden.

In seiner [Belfort-Rede](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique) vom 10. Februar 2022 kündigte der französische Präsident Emmanuel Macron an, er wolle bis 2050 eine installierte Offshore-Windkapazität von 40 GW erreichen. Das Erreichen eines solchen Ziels würde bedeuten, dass jedes Quartal zusätzlich 400 MW angeschlossen werden, also etwas weniger als die 480 MW des Offshore-Windparks Saint-Nazaire, unter der zusätzlichen Annahme, dass das niedrige Ziel des PPE im Jahr 2028 rechtzeitig erreicht wird. Dieses Ziel ist jedoch derzeit nicht rechtsverbindlich.

<iframe title = "Abbildung installierte Leistung Windkraft auf See in Frankreich" src="../../../docs/france/figures/windoff.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Wasserkraft

Wasserkraft ist in Frankreich die wichtigste erneuerbare Stromquelle und nach der Kernkraft die zweitwichtigste Stromerzeugungstechnologie. Laut den Daten von [RTE](https://analysesetdonnees.rte-france.com/production/synthese) beträgt die installierte Kapazität im Jahr 2023 etwa 25,7 GW, ohne Berücksichtigung der Gezeitenenergie. Derzeit sind Seen die am häufigsten eingesetzte Technologie, gefolgt von der sogenannten Laufwassertechnologie, wie aus den [Statistiken](https://www.ecologie.gouv.fr/hydroelectricite) des Ministeriums für Energiewende hervorgeht.  Das Ziel von 25,7 GW (einschließlich Gezeitenenergie) für das Jahr 2023 wurde erreicht. Obwohl die Möglichkeiten für den Ausbau der Wasserkraft in Frankreich begrenzt sind, sieht der PPE2 bis 2028 einen leichten Anstieg der installierten Kapazitäten auf 26,4 bis 26,7 GW (einschließlich Gezeitenenergie) vor. Diese Ziele stehen im Einklang mit den Projektionen der RTE-Szenarien "Futurs 2050", die alle von einer installierten Wasserkraftkapazität von 26,5 GW im Jahr 2030 ausgehen. Diese Szenarien gehen von einem Ausbau der Wasserkraftkapazitäten auf 28,2 GW im Jahr 2040 und 30,1 GW im Jahr 2050 aus.

<iframe title = "Figure installierte Leistung Wasserkraft Frankreich" src="../../../docs/france/figures/hydro_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Anteil der erneuerbaren Energien 

### am Bruttoendenergieverbrauch

Das [Energiegesetzbuch](https://www.legifrance.gouv.fr/codes/id/LEGITEXT000023983208/) (auf Französisch, *Code de l'Énergie* (CE)) legt den Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch auf 23 % im Jahr 2020 fest. Darüber hinaus setzt das [Energie- und Klimagesetz (LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) ein Ziel für diesen Anteil von mindestens 33 % im Jahr 2030. 

Laut [Daten und statistische Studien des franzÖsischen Ministeriums für den ökologischen Wandel](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024) hat Frankreich sein Ziel für 2020 um fast 4 Prozentpunkte verfehlt. Im Jahr 2023 betrug der Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch in Frankreich 22,2 %, was einem Anstieg von 1,7 Prozentpunkten im Jahr 2023 gegenüber 2022 entspricht. Das Ziel der PPE von 24 % bis Ende 2023 wurde jedoch noch nicht erreicht. Die Lücke beim Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch bleibt also bestehen und liegt 2023 um 0,8 Prozentpunkte unter dem Niveau, das sich bei einem linearen Anstieg zwischen 2020 und 2030 ergeben hätte, und ist damit etwas geringer als die Lücke im Jahr 2022. Dies deutet auf eine gewisse Beschleunigung im Jahr 2023 hin, die jedoch nicht ausreicht, um den in der Vergangenheit aufgelaufenen Rückstand aufzuholen. Darüber hinaus ist festzustellen, dass die Fortschritte in den letzten fünf Jahren langsamer zu sein scheinen als der lineare Anstieg, der erforderlich ist, um das Ziel für 2030 zu erreichen.

In der im April 2020 angenommenen Fassung der [Mehrjährigen Energieplanung (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) wird der Bruttoendenergieverbrauch auf 1637 TWh im Jahr 2023 und 1489 TWh im Jahr 2028 geschätzt. Wenn die im PPE vorgesehenen Maßnahmen tatsächlich umgesetzt werden, dürfte der Anteil der erneuerbaren Energien am Bruttoendenergieverbrauch 389 TWh im Jahr 2023 (ca. 24 %) und zwischen 477 und 529 TWh im Jahr 2028 (ca. 32 bis 35 %) betragen. Demnach würde Frankreich das für 2030 gesetzte Ziel wahrscheinlich erreichen.

<iframe title = "Abbildung Anteil erneuerbare Energien Bruttoverbrauch" src="../../../docs/france/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### an der Stromerzeugung

Das Energie- und Klimagesetz (*Loi sur l'énergie et le climat*, LEC) von 2019 hält an dem Ziel für den Anteil erneuerbarer Energien an der Stromerzeugung fest, das mit dem Gesetz zur Energiewende für grünes Wachstum (*Loi sur la transition énergétique pour un croissance verte*, LTECV) eingeführt wurde. Dieser Anteil soll in Frankreich bis 2030 40 % betragen. Laut [Daten](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024) des französischen Ministeriums für Energiewende lag der Anteil erneuerbarer Energien Ende 2023 bei etwa 30%. Wasserkraft ist momentan die bedeutendste Quelle für Strom aus erneuerbaren Energien in Frankreich und machte im Jahr 2023 mehr als 42% des Stroms aus erneuerbaren Energien aus. Onshore-Windkraftanlagen trugen etwa 33% und Photovoltaikanlagen etwa 16% zur Stromerzeugung aus erneuerbaren Energien bei.

Da die Aussichten für den Ausbau der Wasserkraft begrenzt sind, wird der größte Teil des künftigen Anstiegs des Anteils erneuerbarer Energien an der Stromerzeugung aus anderen Quellen kommen müssen. Dennoch scheint die Entwicklung dieses Indikators im Einklang mit den Zielen der geltenden Gesetzgebung zu stehen.

<iframe title = "Figure share RES in power production" src="../../../docs/france/figures/respower.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>