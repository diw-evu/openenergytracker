---
hide:
#  - navigation
#  - toc
---

# Energieverbrauch

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden

??? info "Szenariokorridor"

    === "GENeSYS-MOD"

        Zum Vergleich zeigt jede Grafik den Korridor von Szenarien, die von einem Forscherteam der Technischen Universität Berlin mit dem Modell [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) im Rahmen des europäischen Projekts [openENTRANCE](https://openentrance.eu/about-openentrance/) entwickelt wurden.

    === "Szenarien"

        Der Szenariokorridor zeigt für einen bestimmten Indikator die minimalen und maximalen Werte der Projektionen in Fünfjahresintervallen zwischen 2025 und 2050 unter vier Szenarien an. Die betrachteten Szenarien sind: _Gerichteter Übergang_, _Graduale Entwicklung_, _Gesellschaftliches Engagement_ und _Techno-Friendly_.

        Weitere Informationen über die Definition der Szenarien finden Sie [hier](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Fossiler Primärenergieverbrauch

Das [Energiegesetzbuch](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000023983208/LEGISCTA000023985174/?anchor=LEGIARTI000047717642#LEGIARTI000047717642) sieht bis 2030 eine Reduktion des fossilen Primärenergieverbrauchs um 40 % gegenüber 2012 vor. Dieses Ziel wird im [Mehrjährigen Energieprogramm](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/) in Unterziele nach Energieträgern für die Jahre 2023 und 2028 aufgeschlüsselt. Im Vergleich zum Referenzjahr 2012 soll der Primärölverbrauch bis 2023 um 19 %, der Erdgasverbrauch um 10 % und der Kohleverbrauch um 66 % gesenkt werden. Für 2028 liegen die Ziele bei -34 %, -22 % bzw. -80 %. 

Laut der Ausgabe 2024 der [Energiekennzahlen](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024), die von der Abteilung für [Daten und statistische Studien des Ministeriums für den ökologischen Wandel](https://www.statistiques.developpement-durable.gouv.fr/) veröffentlicht wurde, betrug der fossile Primärenergieverbrauch im Jahr 2023 1.176 TWh gegenüber 1.478 TWh im Jahr 2012, was einem Rückgang von 20,4 % im Zeitraum 2012 bis 2023 (302 TWh) entspricht. Der klimabereinigte Primärverbrauch von Erdöl, Erdgas und Kohle sinkt zwischen 2012 und 2023 um 16,0 %, 15,9 % bzw. 62,0 %. Damit werden die Ziele für den Erdgasverbrauch im Jahr 2023 erreicht. Bei Erdöl und Kohle werden die Ziele knapp verfehlt. Wenn sich die Fortschritte der letzten fünf Jahre fortsetzen, sollten die Ziele für 2030 erreicht werden.

<iframe title = "Fossiler Primärenergieverbrauch in Frankreich" src="../../../docs/france/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Endenergieverbrauch

Die [Mehrjährige Energieplanung (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), deren letzte Fassung im April 2020 verabschiedet wurde, setzt ein Ziel für die Verminderung des Endenergieverbrauchs für mehrere Zeiträume, nämlich 2023 und 2028. Genauer gesagt darf der Endenergieverbrauch aller Sektoren zusammengenommen im Jahr 2023 (bzw. 2028) 1525 TWh (bzw. 1378 TWh) nicht überschreiten, was einem Rückgang von fast 10% (bzw. 18%) im Vergleich zu 2012 entspricht. 

Darüber hinaus sieht das im August 2015 verabschiedete [Energiewendegesetz für grünes Wachstum (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) eine Reduzierung um 20 % im Jahr 2030 und um 50 % im Jahr 2050 im Vergleich zu den Werten von 2012 vor.

Laut der Ausgabe 2024 der [Schlüsselzahlen zum Thema Energie](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024), die vom [Ministerium für den ökologischen Wandel](https://www.statistiques.developpement-durable.gouv.fr/) veröffentlicht wurden, lag der Endenergieverbrauch im Jahr 2023 bei 1549 TWh. Dies liegt leicht über dem Ziel von 1525 TWh für 2023. Während die Sektoren Industrie und Gebäude die für 2023 gesetzten sektoralen Ziele erreicht haben, verbrauchten die Sektoren Verkehr und Landwirtschaft mehr Energie als gewünscht. Besonders deutlich ist der Unterschied beim Verkehr, wo 504 TWh statt der im Ziel prognostizierten 473 TWh verbraucht wurden, d. h. 7 % mehr als erwartet. 

<iframe title = "Endenergieverbrauch in Frankreich" src="../../../docs/france/figures/fig_fec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
