---
hide:
#  - navigation
#  - toc
---

# Émissions de gaz à effet de serre

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md), Jan Czimmeck

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Émissions sectorielles de gaz à effet de serre

Conformément aux dernières directives européennes en date, la France doit réduire ses émissions de gaz à effet de serre (GES) de 55% d'ici 2030 par rapport au niveau de 1990 et atteindre la neutralité carbone en 2050. L'objectif pour 2030 de -55% résulte de la réhausse de l'objectif de -40% lors de l'adoption de la loi climatique européenne à l'été 2021. À l'heure actuelle, la législation française n'a pas encore été mise en jour pour prendre en compte l'objectif de -55% et la réglementation en place se fonde sur l'objectif de -40% en 2030. Cela devrait être fait en 2024 avec la Loi de Programmation quinquennale sur l'Énergie et le Climat (LPEC).

Afin de pouvoir atteindre ces objectifs de long terme de réduction d'émissions de gaz à effet de serre, la Stratégie Nationale Bas Carbone (SNBC) fixe les budgets carbone de la France pour trois périodes successives de cinq années. La dernière SNBC en date (SNBC2) a été adoptée en 2020 et fixe les bugdets carbone des période 2019-2023, 2024-2028 et 2029-2033. Étant donné que la SNBC2 a été conçue avant la réhausse des ambitions de réduction des émissions de gaz à effet de serre de 40% à 55% en 2022, les budgets carbone devront être réajustés dans la prochaine SNBC (SNBC3) qui devrait être adoptée en 2024. 

Les budgets carbone signifient que la moyenne des émissions sur la période considérée doit être inférieure ou égale au budget fixé. À titre indicatif, des budgets annuels sont également fournis, ainsi que des budgets par secteurs. Ces budgets infra-quinquennats ou sectoriels ne sont pas contraignants et le moindre niveau d'émissions dans un secteur par rapport au budget sectoriel alloué peut tout à fait compenser un niveau plus élevé d'un autre secteur. Il en va de même entre les années au sein d'une période de cinq ans.

Pour la période 2019-2023, la France semble sur la bonne voie pour respecter le budget carbone qu'elle s'est fixé (421 MtCO2e) avec environ 373 MtCO2e émis en 2023 d'après les données du [CITEPA](https://www.citepa.org/fr/secten/). En ce qui concerne la répartition sectorielle, seul le secteur des déchets (14 MtCO2e) semble dépasser le budget indicatif alloué. Tous les autres secteurs en revanche semblent faire mieux que prévu. Notons toutefois que ces données doivent être prises avec prudence dans la mesure où les années 2020, 2021 et 2022 ont été fort inhabituelles, marquées par des crises mondiales ayant considérablement modifié les comportements de consommation et de production d'énergie. Toutefois, la baisse des émissions semble se confirmer en 2023 pour tous les secteurs, à l'exception du secteur des déchets qui a maintenu ses émissions constantes.

<iframe title = "Figure emissions de GES en France" src="../../../docs/france/figures/ghs_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Intensité carbone de la production d'électricité 

Du fait de rôle important du nucléaire dans le mix électrique français, la production d'électricité en France est une des plus décarbonées au monde. D'après les [données](https://analysesetdonnees.rte-france.com/emission/emission-ges) du Réseau de Transport d'Électricité (RTE), l'émission de gaz à effet de serre due à la production d'électricité varie de manière saisonnière. La production d'électricité génère plus de gaz à effet de serre en hiver, avec des pics entre novembre et janvier. Sur les dix dernières années, l'intensité carbone maximale fut d'environ 90 g/kWh (novembre 2016), bien que le pic hivernal se situe habituellement plutôt autour de 60 g/kWh depuis 2018. À l'inverse, le minimum, généralement atteint au début de l'été, se situe autour de 20-25 g/kWh et a pu atteindre jusqu'à 11 g/kWh (avril 2020). Cette variation concerne tant la production en valeur absolue (en million de tonnes émises) qu'en valeur relative (en grammes émis par kWh d'électricité produit). L'intensité carbone de la production d'électricité illustrée dans ce graphique correspond à la moyenne mensuelle de l'intensité carbone disponible pour chaque tranche demie-heure (les données brutes utilisées sont disponibles [ici](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs)).

<iframe title = "Figure carbon intensity power generation France" src="../../../docs/france/figures/carbonintensity.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>