---
description: L'Open Energy Tracker visualise les objectifs gouvernementaux et les avancées dans certains domaines de la transition énergétique.

hide:
#  - navigation
#  - toc
---
# France

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! Success "Nouveautés"

    * **Données mises à jour** pour toutes les sections 
    * Nouvelle section **Chaleur renouvelable**
    * Nouvelle section **Émissions de gaz à effet de serre**
    * Nouveaux indicateurs : capacité hydraulique installée, part des ENR dans la production d'électricité, la consommation de carburants, la consommation de chaleur. 
    * Nouveaux objectifs issus de la *Stratégie française pour l'énergie et le climat* et le *Plan National Intégré Climat-Énergie*

Les objectifs de la transition énergétique en France sont définis par la [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/programmations-pluriannuelles-lenergie-ppe) instaurée au même moment que la [Stratégie nationale bas carbone (SNBC)](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc) par la [loi relative à la transition énergétique pour la croissance verte (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) en août 2015. La [loi énergie-climat (LEC)](https://www.ecologie.gouv.fr/loi-energie-climat), promulguée en novembre 2019, est venue modifier certains objectifs adoptés dans la LTECV et doit être à présent prise comme référence pour la définition des objectifs dans les futures PPE. 

La PPE se concentre sur les indicateurs relatifs à la production et à la consommation d'énergie. La SNBC, quant à elle, définit les objectifs que doit atteindre la France en termes de réduction d'émissions de gaz à effet de serre. Ces deux outils de pilotage doivent être compatibles entre eux. Cela signifie notamment que les objectifs de réduction d'émissions s'imposent comme condition *sine qua non* aux objectifs de la PPE.  

À l'heure actuelle, la PPE définit les objectifs pour une période de dix ans allant de 2019 à 2028, séparée en deux périodes de cinq ans (2019-2023 et 2024-2028). Ainsi, des objectifs quantitatifs sont formulés pour 2023 et 2028. Par ailleurs, deux cibles sont parfois fournies pour 2028, à savoir une cible haute et une cible basse. 

La SNBC fournit la trajectoire d'émissions de gaz à effet de serre sur le long-terme i.e. la trajectoire permettant d'atteindre la neutralité carbone en 2050 conformément à l'Accord de Paris. Elle définit par ailleurs des objectifs intermédiaires, appelés budgets carbone. En avril 2020, les budgets carbone pour les périodes 2019-2023, 2024-2028 et 2029-2033 ont ainsi été fixés par la SNBC2. 

Tant la PPE que la SNBC ont vocation à être révisées tous les cinq ans afin d'ajuster les objectifs aux résultats obtenus lors de la période précédente. La prochaine révision (SNBC3 et PPE3) doit être adoptée au plus tard en juillet 2024. 

La loi Énergie-Climat de 2019 a introduit également l'obligation pour le gouvernement d'adopter une Loi quinquennale de Programmation sur l'Énergie et le Climat (LPEC), devant être révisée tous les cinq ans et dont la première devait être adoptée avant le 1er juillet 2023. Cette loi quinquennale "*détermine les objectifs et fixe les priorités d'action de la politique énergétique nationale pour répondre à l'urgence écologique et climatique*" (Article L100-1 du code de l'énergie). Le gouvernement d'Elisabeth Borne n'a pas respecté cette échéance.

En novembre 2023, un document stratétique, la [Stratégie Française pour l'Énergie et le Climat](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) (SFEC) a été publié et soumis à consultation publique. Ce document offre un aperçu des objectifs qui pourraient être proposés dans la PPE3, sans pour que cela soit pour l'instant définitif. 
Par ailleurs, fin décembre 2023, un avant-projet de loi sur la souveraineté énergétique a été soumis à examen à deux Conseils (le Cese et le CNTE) et pourrait être le texte de loi prévu par le gouvernement en guise de LPEC. 

Un suivi bien plus complet des objectifs de politiques énergétiques en France est accessible en ligne (en français et en anglais) sur le site de l'[Observatoire Climat-Énergie](https://www.observatoire-climat-energie.fr/) développé par le [Réseau Action Climat](https://reseauactionclimat.org/) et le [CLER-Réseau pour la transition énergétique](https://cler.org/). Le [Baromètre de la transition énergétique en France](https://energie-fr-de.eu/de/systeme-maerkte/nachrichten/leser/barometer-der-energiewende-in-frankreich.html) de l'[Office franco-allemand pour la transition énergétique (OFATE)](https://energie-fr-de.eu/de/startseite.html) fournit également des informations supplémentaires sur la transition énergétique en France en langue allemande. Notre but ici est aussi de rendre accessible en langue allemande le suivi de la politique énergétique française ainsi que de contribuer au développement d'une plateforme regroupant des ressources concernant la politique énergétique et ses avancées dans plusieurs pays. Toute collaboration et entraide est la bienvenue.
