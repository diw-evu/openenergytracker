---
hide:
#  - navigation
#  - toc
---

# Renewable energy expansion

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

??? info "Scenario Corridor"

    === "Futurs énergétiques 2050"

        To put the government's targets into perspective, we add to some of the graphs the corridor of the [*Futurs énergétiques 2050*](https://rte-futursenergetiques2050.com/) scenarios developed by the French Transmission System Operator (*Réseau de Transport d'Électricité* in French, shortened RTE). 
        
        The corridor shown here corresponds to the minimum and maximum values among the six scenarios based on the reference path for consumption. These six scenarios envisage different generation mixes, ranging from 100% renewable to 50% nuclear/50% renewable. 
        
        RTE's modelling provides values for certain variables (e.g. installed photovoltaic capacity) for 2030, 2040 and 2050. We assume a linear trajectory between these horizons to form the corridor.
    
    === "GENeSYS-MOD"

        For comparison, each graphic shows the corridor of scenarios developed by a team of researchers from the Technical University of Berlin using the [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) model as part of the European [openENTRANCE](https://openentrance.eu/about-openentrance/) project.

        For a given indicator, the scenario corridor displays the minimum and maximum values of projections at five-year intervals between 2025 and 2050 among four scenarios. The scenarios considered are: _Directed Transition_, _Gradual Development_, _Societal Commitment_ and _Techno-Friendly_.

        More information on the definition of the scenarios can be found [here](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Solar PV

The [Pluriannual Energy Programme](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) (in French, PPE), the latest revision of which was adopted in April 2020, sets a target of 20.1 GW of installed PV capacity in 2023 and a low (resp. high) target for 2030 of 35.1 GW (resp. 44 GW). The PPE also provides a breakdown of the installed PV capacity between ground-mounted and roof-mounted panels. By 2023, ground-mounted panels are expected to reach an installed capacity of 11.6 GW (resp. 8.5 GW for rooftop panels). In 2028, ground-mounted (resp. roof-mounted) panels would represent between 20.6 and 25 GW (resp. between 14.5 and 19 GW) or an estimated area of between 330 and 400 km<sup>2</sup> (resp. between 150 and 200 km<sup>2</sup>). 

The [French Energy and Climate Strategy](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) (*Stratégie Française pour l'Énergie et le Climat* in French, shortened SFEC) also includes targets for 2030 and 2035. These targets are likely to be those adopted in the PPE3, but have not yet been formally adopted by law. For 2030, the SFEC sets an installed capacity target between 54 and 60 GW and for 2035 between 75 and 100 GW. The 2030 targets are above the corridor of the *Futurs énergétiques 2050* scenarios, as is the high target for 2035.

According to the [data and statistical studies of the Ministry of Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690), installed photovoltaic capacity in France at the end of the third quarter of 2024 stood at 22.9 GW, i.e. 0.6 GW more than would be predicted by a linear deployment of new installations between December 2023 and December 2028. Furthermore, the trend over the past year shows that there has been a clear acceleration in new photovoltaic installations compared with the trend over the past five years. If this momentum is maintained over the next five years, the higher target will be almost within reach by 2028. 

In his [Belfort speech](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique) on 10 February 2022, the President of the French Republic, Emmanuel Macron, announced that he wanted to achieve installed photovoltaic capacity of 100 GW by 2050. Reaching such a target would mean connecting an additional 740 MW every quarter, i.e. maintaining the same pace as that needed to reach the low targets of the PPE between 2023 and 2028, under the additional assumption that the low target of the EPP in 2028 will be reached on time. It should be noted, however, that this target has no legal force for the time being.

<iframe title = "Figure installed pv capacity in France" src="../../../docs/france/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Wind energy
### Onshore

In the latest revision of the [Pluriannual Energy Programme (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), adopted in April 2020, the target for installed onshore wind power capacity is 24.1 GW in 2023 and between 33.2 GW and 34.7 GW in 2028. 

The [French Energy and Climate Strategy](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) also includes targets for 2030 and 2035. The proposed targets for 2030 appear to be the same as those for 2028, thus postponing by two years the achievement of the targets set out in the PPE2. The targets for 2035 require between 40 and 45 GW of installed capacity, which is more or less the same construction rate as forecast in the PPE2 for the period 2023-2028. The high target for 2035 is above the *Futurs énergétiques 2050* scenario corridor, while the low target is in the middle of the corridor.

According to the [data and statistical studies of the Ministry of Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689), in the third quarter of 2024, the installed capacity of onshore wind power in France was 22.9 GW compared with the 25.5 GW target that would be prescribed by a linear progression between the targets of december 2023 and december 2028, i.e. a delay of 2.6 GW. Comparing the trend over the last four quarters with the trend over the last five years (2018-2022), onshore wind deployment in France has recently slowed down. If we continue at the same pace as over the last twelve months, the low target for 2028 (33.2 GW installed) would be missed by about 6 GW. It is therefore absolutely essential to speed up the deployment of onshore wind power.

<iframe title = "Figure installed wind onshore capacity in France" src="../../../docs/france/figures/windon.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Offshore

As far as offshore wind is concerned, France is falling further and further behind the targets set in the latest version of the [Multiannual Energy Programme (MPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) adopted in April 2020. Indeed, according to the PPE, 2.4 GW of offshore wind should be installed by the end of 2023 and between 5.2 GW and 6.2 GW by the end of 2028. 

The [French Energy and Climate Strategy](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) also sets targets for 2030 and 2035, but only a target for each period, rather than a range as in the case of photovoltaics and onshore wind. The 2030 target is 4 GW, which is lower than the PPE2 target for 2028. The target for 2035 is 18 GW, which requires a massive acceleration of onshore wind connections in the period 2030-2035, with an average of 700 MW per month. This 2035 target is at the upper end of the *Futurs énergétiques 2050* scenario corridor.

According to the [data and statistical studies of the Ministry of Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689), 1.48 GW of offshore wind power was installed at the end of the third quarter of 2024. The first French offshore wind farm off the coast of Saint-Nazaire with 80 wind turbines was inaugurated in September 2022. In January 2023, the Saint-Nazaire offshore wind farm produced more than 180,000 MWh. In May 2023, the installation phase of a second offshore wind farm began in the sea off Brittany, more precisely in the Bay of Saint-Brieuc. The wind farm consists of 62 wind turbines with a total capacity of 496 MW. This consists of 71 wind turbines with a total capacity of 497 MW. The Fécamp wind farm was also commissioned in 2023.

Even if the installation of these first two offshore wind farms is encouraging and seems to be moving in the right direction, France is still lagging far behind in this area compared to the targets set by the PPE. Despite the 1.48 GW installed in 2022-2023, the shortfall in installed capacity remains significant (around 1.3 GW) compared to what would be expected on a linear trajectory between the December 2023 and December 2028 targets, as no new installations have been connected since the second quarter of 2023.

In his [Belfort speech](https://www.elysee.fr/emmanuel-macron/2022/02/10/reprendre-en-main-notre-destin-energetique), dated February 10, 2022, French President Emmanuel Macron announced that he wanted to achieve 40 GW of installed offshore wind capacity by 2050. Reaching such a target would mean connecting an additional 400 MW every quarter, slightly less than the 480 MW of the Saint-Nazaire offshore wind farm, under the additional assumption that the low target of the PPE in 2028 will be reached on time. However, this target is currently not legally binding.

<iframe title = "Figure installed wind offshore capacity in France" src="../../../docs/france/figures/windoff.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Hydropower

Hydropower is the primary source of renewable electricity in France and the second most significant electricity generation technology after nuclear. According to [RTE data](https://analysesetdonnees.rte-france.com/production/synthese), the installed capacity was approximately 25.7 GW in 2023, excluding tidal power. According to [statistics](https://www.ecologie.gouv.fr/hydroelectricite) from the French Ministry of Energy Transition, lakes are currently the most widely used hydropower technology, followed by 'run-of-river' technologies.  The target of 25.7 GW (including tidal power) set for 2023 has been achieved. Although the potential for expanding hydropower in France seems limited, the EPP2 predicts a slight increase in installed capacity by 2028 to between 26.4 and 26.7 GW (including tidal power). These targets are in line with the projections in RTE's Futurs 2050 scenarios. All scenarios predict an installed hydropower capacity of 26.5 GW by 2030, with an increase to 28.2 GW by 2040 and 30.1 GW by 2050.

<iframe title = "Figure hydropower installed capacity France" src="../../../docs/france/figures/hydro_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## RES share 

### In final gross energy consumption 

The [Energy Code (CE)](https://www.legifrance.gouv.fr/codes/id/LEGITEXT000023983208/) sets the share of renewable energy in gross final energy consumption at 23% in 2020. In addition, the [Energy and Climate Law (LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) sets this share at at least 33% in 2030. 

According to [data and statistical studies from the French Ministry for Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), France missed its 2020 target by almost 4 percentage points. In 2023, renewable energy accounted for 22.2% of gross final energy consumption in France, an increase of 1.7 percentage points in 2023 compared to 2022. However, the PPE's target of 24% by the end of 2023 has not yet been reached. The gap in the share of renewables in gross final energy consumption therefore remains and is 0.8 percentage points lower in 2023 than the level predicted by a linear increase between 2020 and 2030, slightly less than the gap observed in 2022. This means that there is some acceleration in 2023, but not enough to make up for the delays accumulated in the past. It should also be noted that progress over the last five years appears to have been slower than the linear progression required to reach the 2030 target.

In the version adopted in April 2020 of the [Pluriannual Energy Programming (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), gross final energy consumption is estimated at 1637 TWh in 2023 and 1489 TWh in 2028. If the measures foreseen in the PPE are effectively implemented, the share of renewable energies in the gross final energy consumption should be 389 TWh in 2023 (about 24%) and between 477 and 529 TWh in 2028 (about 32 to 35%), which would mean that France would probably reach the target set for 2030. The progress over the last five years is not fast enough to close the gap and reach the 2030 target.

<iframe title = "Figure RES shares in France" src="../../../docs/france/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## in power generation

The 2019 Energy-Climate Act (LEC) maintains the target introduced in the Energy Transition for Green Growth Act (LTECV) for the share of renewable energies in electricity generation. By 2030, 40% of France's electricity generation will have to come from renewable sources. As of the end of 2023, the share of renewable energy stood at approximately 30%, according to [data](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024) published by the French Ministry for Energy Transition. Hydroelectric power is currently the primary source of renewable electricity, accounting for over 42% of France's renewable electricity in 2023. Approximately 33% of the renewable production is provided by onshore wind power and solar photovoltaics account for about 16%.

As the prospects for deploying hydroelectric capacity are limited, most of the future increase in the share of renewables in electricity generation will have to come from other sources. That said, for the time being, it would appear that the trend in this indicator is in line with the targets set by current legislation.

<iframe title = "Figure share RES in power production" src="../../../docs/france/figures/respower.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>