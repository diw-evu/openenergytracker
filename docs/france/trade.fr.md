---
hide:
#  - navigation
#  - toc
---

# Échanges commerciaux

=== "Auteurs"

    Jan Czimmeck, [Adeline Guéret](../../docs/about/authors.md) 

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Allemagne et Belgique

Bien que la France soit traditionnellement une exportatrice nette d'électricité, les échanges commerciaux fluctuent au fil des mois. D'après les [données](https://analysesetdonnees.rte-france.com/marche/echanges-commerciaux) du Réseau de Transport d'Électricité (RTE) français, la France importe plus d'électricité qu'elle n'en exporte à l'Allemagne et la Belgique durant l'automne et l'hiver, tandis que l'inverse est vrai au printemps et en été. L'année 2022 fait figure d'exception notable. Durant cette année, la France a en effet été importatrice nette auprès de la Belgique et de l'Allemagne toute l'année durant, y compris au printemps et à l'été 2022. Cela est probablement la conséquence directe de la mise à l'arrêt d'un grand nombre de réacteurs nucléaires du fait de problèmes techniques ainsi que du niveau historiquement bas des réservoirs connectés aux installations hydroélectriques. 

<iframe title = "Figure échanges commerciaux" src="../../../docs/france/figures/powertrade.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>