---
hide:
#  - navigation
#  - toc
---

# Energy consumption

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

??? info "Scenario Corridor"

    === "GENeSYS-MOD"

        For comparison, each graphic shows the corridor of scenarios developed by a team of researchers from the Technical University of Berlin using the [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) model as part of the European [openENTRANCE](https://openentrance.eu/about-openentrance/) project.

    === "Scenarios"

        For a given indicator, the scenario corridor displays the minimum and maximum values of projections at five-year intervals between 2025 and 2050 among four scenarios. The scenarios considered are: _Directed Transition_, _Gradual Development_, _Societal Commitment_ and _Techno-Friendly_.

        More information on the definition of the scenarios can be found [here](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Fossil energy primary consumption

In terms of primary fossil energy consumption, the [Energy Code](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000023983208/LEGISCTA000023985174/?anchor=LEGIARTI000047717642#LEGIARTI000047717642) sets a target of a 40% reduction by 2030 compared to 2012. This target is broken down into sub-targets by energy vector for the years 2023 and 2028 in the [Pluriannual Energy Programme](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/). Compared to the reference year 2012, primary oil consumption in 2023 must be 19% lower, natural gas 10% lower and coal 66% lower. For 2028, the targets are -34%, -22% and -80% respectively. 

According to the 2024 edition of the [Key energy figures](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) published by the [Statistical data and studies from the French Ministry of Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/), primary fossil energy consumption amounted to 1176 TWh in 2023 compared to 1478 TWh in 2012, a decrease of 20.4% over the period 2012-2023 (302 TWh). Weather-adjusted primary consumption of oil, natural gas and coal decreased by 16.0%, 15.9% and 62.0%, respectively, between 2012 and 2023. The 2023 targets for natural gas consumption are therefore met. For oil and coal, the targets are missed by a small margin. The progress made over the past five years, if maintained, should ensure that the 2030 targets are met.

<iframe title = "Fossil primary energy consumption" src="../../../docs/france/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Final energy consumption

The [Pluriannual Energy Programming (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), the latest version of which was adopted in April 2020, sets a target for reducing final energy consumption for several time frames, namely 2023 and 2028. More specifically, all sectors combined, final energy consumption in 2023 (resp. 2028) must not exceed 1525 TWh (resp. 1378 TWh), which represents a decrease of about 10% compared to 2012 (resp. 18%). 

In addition, the [Energy Transition Law for Green Growth (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte), adopted in August 2015, provides for a reduction of 20% in 2030 and 50% in 2050 compared to 2012 levels.

According to the 2024 edition of the [Key energy figures](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) published by the [statistical data and studies from the French Ministry of Ecological Transition](https://www.statistiques.developpement-durable.gouv.fr/), final energy consumption stood at 1549 TWh in 2023. This is slightly above the target of 1525 TWh for 2023. While the industry and buildings sectors achieved the sectoral targets set for 2023, the transport and agriculture sectors consumed more energy than desired. The difference is particularly marked in the case of transport, where 504 TWh were consumed instead of the 473 TWh forecast in the target, i.e. 7% more than expected. 

<iframe title = "Final energy consumption" src="../../../docs/france/figures/fig_fec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
