---
hide:
#  - navigation
#  - toc
---

# Données

## Cadre législatif 

*Loi de Transition Énergétique pour la Croissance Verte* : [LTECV](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) (publiée au Journal Officiel le 18 août 2015) 

*Programmations Pluriannuelle de l'Énergie* (PPE) : 

* [PPE1](https://www.ecologie.gouv.fr/sites/default/files/PPE%20int%C3%A9gralit%C3%A9.pdf) publiée au Journal Officiel le 28 octobre 2016, pour les périodes 2016-2018 et 2019-2013.

*  [PPE2](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) publiée au Journal Officiel le 21 avril 2020, pour les périodes 2019-2023 et 2023-2028.

*Stratégie Nationale Bas Carbone* : [SNBC](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc) adoptée en 2015 (SNBC-1) puis révisée en 2018-2019 (SNBC-2) afin d'inclure l'objectif de neutralité carbone conformément à l'Accord de Paris.

*Loi énergie-climat* : [LEC](https://www.ecologie.gouv.fr/loi-energie-climat) adoptée le 8 novembre 2019.

*Stratégie française pour l'énergie et le climat* : [SFEC](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) publiée par le Ministère de la Transition Énergétique (MTE) en novembre 2023. 

*Code de l'Énergie* : disponible sur le site de [Légifrance](https://www.legifrance.gouv.fr/codes/texte_lc/LEGITEXT000023983208/2011-07-31)

## Électricité renouvelable
### Déploiement des énergies renouvelables

Photovoltaïque : [Tableau de bord : solaire photovoltaïque](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/690) du Ministère de la Transition Écologique. Édition du troisième trimestre 2024 (novembre 2024).

Éolien terrestre : [Tableau de bord : éolien](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) du Ministère de la Transition Écologique. Édition du troisième trimestre 2024 (novembre 2024).

Éolien en mer : [Tableau de bord : éolien](https://www.statistiques.developpement-durable.gouv.fr/publicationweb/689) du Ministère de la Transition Écologique. Édition du troisième trimestre 2024 (novembre 2024).

Hydraulique : 

* [Évolution du parc installé de production d'électricité en France par filière](https://analysesetdonnees.rte-france.com/production/synthese), données du Réseau de Transport d'Électricité (RTE) (janvier 2024). 

* [Répartition du parc et de la production hydroélectrique moyenne en fonction des types d'installations](https://www.ecologie.gouv.fr/hydroelectricite), données du Ministère de la Transition Écologique et de la Cohésion des Territoires (avril 2024).

### Part des énergies renouvelables

Part dans la consommation brute finale d'énergie : [Chiffres clés des énergies renouvelables](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), édition 2024 (août 2024).

Part dans la production d'électricité : [Chiffres clés des énergies renouvelables](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), édition 2024 (août 2024).


## Chaleur renouvelable 

Part des EnR dans la consommation finale brute de chaleur : [Chiffres clés des énergies renouvelables](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), édition 2024 (août 2024).

Pompes à chaleur : 

* Stock de pompes à chaleur en opération : Banque de données en ligne de l'[EurObserv'ER](https://www.eurobserv-er.org/online-database/).

* Ventes annuelles de pompes à chaleur et production de chaleur : [Chiffres clés des énergies renouvelables](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), édition 2024 (août 2024).

## Electromobilité

Nombre de points de charges ouverts au public : [Baromètre national des infrastructures de recharge ouvertes au public](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-recharge)

Nouvelles immatriculations et parts de marché : [Baromètre des immatriculations](https://www.avere-france.org/publications/?publication-type%5B%5D=barometres-immatriculations)

Estimation des points de charge privés en France : [Enedis Open Data](https://data.enedis.fr/pages/points-de-charge/)

## Énergie nucléaire 

Part du nucléaire dans la production d'électricité : [Production nationale annuelle par filière](https://odre.opendatasoft.com/explore/dataset/prod-national-annuel-filiere/information/) du [Réseau de Transport d'Électricité (RTE France)](https://www.rte-france.com/), données disponibles sur la plateforme [Open Data Réseaux Énergies (Odré)](https://opendata.reseaux-energies.fr/).

## Consommation d'énergie 

Consommation d'énergie primaire fossile : [Chiffres clés de l'énergie - Édition 2024](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (septembre 2024).

Consommation finale d'énergie : [Chiffres clés de l'énergie - Édition 2024](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) (septembre 2024).

## Émissions de gaz à effet de serre

Émissions de gaz à effet de serre et budgets : Données Secten du [CITEPA](https://www.citepa.org/fr/secten/), édition 2024 (juin 2024).

Émissions de gaz à effet de serre liées à la production d'électricité : [Données RTE](https://analysesetdonnees.rte-france.com/emission/emission-ges), résolution mensuelle, toutes fillières confondues (décembre 2024). 

Intensité carbone de la production d'électricité : [Données RTE](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs) (décembre 2024). 

## Échanges commerciaux

Importations et exportations d'électricité en France : [Données RTE](https://analysesetdonnees.rte-france.com/marche/echanges-commerciaux), pour la frontière Belgique/Allemagne, résolution mensuelle (décembre 2024). 