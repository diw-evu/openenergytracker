---
hide:
#  - navigation
#  - toc
---

# Consommation d'énergie

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

??? info "Corridor de scénarios"

    === "GENeSYS-MOD"

        Pour comparaison, chaque graphique permet de visualiser le corridor des scénarios développés par une équipe de chercheurs de l'université technique de Berlin à l'aide du modèle [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) dans le cadre du projet européen [openENTRANCE](https://openentrance.eu/about-openentrance/).

    === "Scénarios"

        Pour un indicateur donné, le corridor de scénarios permet de visualiser les valeurs minimales et maximales des projections à intervalle de cinq ans entre 2025 et 2050 parmi quatre scénarios. Les scénarios considérés sont les suivants : _Directed Transition_, _Gradual Development_, _Societal Commitment_ et _Techno-Friendly_.

        Plus d'informations sur la définition des scénarios sont à trouver [ici](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Consommation primaire d'énergie d'origine fossile

En matière de consommation d'énergie primaire fossile, est inscrit dans le [code de l'énergie](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000023983208/LEGISCTA000023985174/?anchor=LEGIARTI000047717642#LEGIARTI000047717642) un objectif de réduction de 40% en 2030 par rapport au niveau de 2012. Cet objectif est décomposé en sous-objectifs par vecteur d'énergie dans la [Programmation Pluriannuelle de l'Énergie](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/) pour les années 2023 et 2028. Par rapport à 2012, l'année de référence, la consommation primaire de pétrole doit être inférieure de 19% en 2023, de 10% pour le gaz naturel et de 66% pour le charbon. Pour l'année 2028, il s'agit respectivement de -34%, -22% et -80%. 

D'après l'édition 2024 des [Chiffres clés de l'énergie](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) publiés par le service des [données et études statistiques du Ministère de la Transition Écologique francais](https://www.statistiques.developpement-durable.gouv.fr/), la consommation primaire d'énergie fossile s'élève en 2023 à 1176 TWh contre 1478 TWh en 2012, soit une réduction de 20,4 % sur la période 2012-2023 (302 TWh). Les consommations primaires corrigées des variations climatiques de pétrole, gaz naturel et charbon ont respectivement décru de 16,0%, 15,9 % et 62,0 % entre 2012 et 2023. Les objectifs pour 2023 ont donc été atteints en ce qui concerne la réduction de la consommation de gaz naturel. En ce qui concerne le pétrole et le charbon, les objectifs ont été manqués de peu seulement. La progression sur les cinq dernières années, si elle est maintenue à ce rythme, devrait permettre d'atteindre les objectifs de 2030.

<iframe title = "Consommation d'énergie fossile primaire en France" src="../../../docs/france/figures/fig_pec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

## Consommation d'énergie finale

La [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), dont la dernière version en date a été adoptée en avril 2020, fixe un objectif de réduction de la consommation d'énergie finale pour plusieurs échéances, à savoir 2023 et 2028. Plus précisément, tous secteurs confondus, la consommation d'énergie finale en 2023 (resp. en 2028) ne devra pas dépasser 1525 TWh (resp. 1378 TWh), ce qui représente une baisse de près de 10% par rapport à 2012 (resp. 18%). La PPE prévoit aussi des objectifs spécifiques à chaque secteur. Pour 2023 (resp. 2028), la consommation d'énergie finale du secteur du bâtiment (résidentiel et tertiaire) ne devra pas dépasser 712 TWh (resp. 636 TWh). Pour le secteur des transports, de l'industrie et de l'agriculture-pêche, la consommation finale d'énergie ne devra pas être supérieure à 473 TWh (resp. 427 TWh), 291 TWh (resp. 269) et 49 TWh (resp. 46 TWh) respectivement.

Par ailleurs, la [Loi de Transition Énergétique pour la Croissance Verte (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte), adoptée en août 2015, prévoit une réduction de 20% en 2030 et de 50% en 2050 par rapport au niveau de 2012.

D'après l'édition 2024 des [Chiffres clés de l'énergie](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2024) publiés par le service des [données et études statistiques du Ministère de la Transition Écologique francais](https://www.statistiques.developpement-durable.gouv.fr/), la consommation finale d'énergie s'élevait à 1549 TWh en 2023. C'est légèrement au-dessus de la cible de 1525 TWh pour l'année 2023. Alors que le secteur de l'industrie et le secteur des bâtiments ont atteint les cibles sectorielles fixées pour 2023, le secteur des transports et celui de l'agriculture ont consommé plus d'énergie que souhaité. La différence est particulièrement forte pour les transports, avec 504 TWh consommés au lieu des 473 TWh prévus par la cible, soit 7% de plus qu'escompté. 

<iframe title = "Consommation finale d'énergie en France" src="../../../docs/france/figures/fig_fec.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>