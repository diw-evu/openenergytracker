---
description: Der Open Energy Tracker visualisiert Regierungsziele und Fortschritte für ausgewählte Bereiche der Energiewende.

hide:
#  - navigation
#  - toc
---

# Frankreich

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! Success "Neuigkeiten"

    * **Aktualisierte Daten** für alle Abschnitte 
    * Neuer Abschnitt **Erneuerbare Wärme**
    * Neuer Abschnitt **Treibhausgasemissionen** 
    * Neue Indikatoren: installierte Wasserkraftkapazität, Anteil der erneuerbaren Energien an der Stromerzeugung, Brennstoffverbrauch, Wärmeverbrauch
    * Neue Ziele aus der *Französischen Strategie für Energie und Klima* und dem *Integrierten Nationalen Klima-Energie-Plan*

Die Ziele der Energiewende in Frankreich werden in der [mehrjährige Programmplanung für Energie](https://www.ecologie.gouv.fr/programmations-pluriannuelles-lenergie-ppe) (auf Französisch *Programmation Pluriannuelle de l'Énergie*, abgekürzt PPE) festgelegt, die zur gleichen Zeit wie die [nationale Dekarbonisierungsstrategie](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc) (auf Französisch *Stratégie Nationale Bas Carbone*, abgekürzt SNBC) durch das [Gesetz zur Energiewende für grünes Wachstum](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) (auf Französisch *Loi relative à la Transition Énergétique pour la croissance verte*, LTECV) im August 2015 eingeführt wurde. Das im November 2019 verabschiedete [Energie- und Klimagesetz](https://www.ecologie.gouv.fr/loi-energie-climat) (auf Französisch *Loi énergie-climat*, abgekürzt LEC) hat einige der im LTECV verabschiedeten Ziele geändert und kann nun als Referenz für die Festlegung der Ziele in den künftigen mehrjährigen Programmplanungen für Energie herangezogen werden. 

Die mehrjährige Programmplanung für Energie (PPE) konzentriert sich auf Indikatoren für die Energieerzeugung und den Energieverbrauch. Die Dekarbonisierungsstrategie (SNBC) hingegen legt die Ziele fest, die Frankreich im Hinblick auf die Reduzierung der Treibhausgasemissionen erreichen muss. Diese beiden Programmen müssen miteinander kompatibel sein. Das bedeutet insbesondere, dass die Emissionsreduktionsziele als notwendige Bedingung für die Ziele des PPE gelten.  

Derzeit legt der PPE die Ziele für einen Zehnjahreszeitraum von 2019 bis 2028 fest, der in zwei Fünfjahreszeiträume (2019-2023 und 2024-2028) unterteilt ist. So werden quantitative Ziele für die Jahre 2023 und 2028 formuliert. Darüber hinaus werden manchmal zwei Ziele für 2028 angegeben, nämlich ein hohes und ein niedriges Ziel. 

Die SNBC liefert den langfristigen Treibhausgasemissionspfad, d.h. den Pfad zur Erreichung der Klimaneutralität im Jahr 2050, die von der EU-Ländern auf der Grundlage des Pariser Abkommens verabschiedet wurde. Darüber hinaus werden Zwischenziele festgelegt, die als Emissionsbudgets bezeichnet werden. Im April 2020 wurden die Emissionsbudgets für die Zeiträume 2019-2023, 2024-2028 und 2029-2033 in der SNBC2 festgelegt. 

Sowohl die PPE als auch die SNBC müssen alle fünf Jahre überarbeitet werden, um die Ziele an die in der vorangegangenen Periode erzielten Ergebnisse anzupassen. Die nächste Überarbeitung (SNBC3 und PPE3) muss bis zum 1. Juli 2024 verabschiedet werden, aber es scheint unwahrscheinlich, dass die Regierung diese Frist einhalten wird.

Mit dem Energie- und Klimagesetz 2019 wurde auch die Verpflichtung für die Regierung eingeführt, ein Fünfjahresgesetz zur Energie- und Klimaplanung (*Loi quinquennale de programmation pour l'Énergie et le Climat*, abgekürzt LPEC) zu verabschieden, das alle fünf Jahre überarbeitet wird, wobei die erste Überarbeitung vor dem 1. Juli 2023 verabschiedet werden musste. Dieses Fünfjahresgesetz *definiert die Ziele und legt die Handlungsprioritäten der nationalen Energiepolitik als Antwort auf die Umwelt- und Klimakrise fest*" (Artikel L100-1 des Energiegesetznuch, *Code de l'énergie* auf Französisch). Die Regierung von Elisabeth Borne hat diese Frist nicht eingehalten. 

Im November 2023 wurde ein strategisches Dokument, die [französische Energie- und Klimastrategie](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) (*Stratégie Française pour l'Énergie et le Climat* auf Französisch, abgekürzt SFEC), zur öffentlichen Konsultation veröffentlicht. Dieses Dokument gibt einen Überblick über die Ziele, die im Rahmen des PPE3 vorgeschlagen werden könnten, ist aber noch nicht endgültig. Darüber hinaus wurde zwei Räten (dem Cese und dem CNTE) ein Gesetzesentwurf zur Energiesouveränität vorgelegt, der bis Ende Januar 2024 geprüft werden soll und von der Regierung als Ersatz für den LPEC vorgesehen werden könnte.

Auf dieser Homepage werden ausgewählte energiepolitische Ziele Frankreichs dargestellt und mit dem aktuellen Stand verglichen. Ein weitaus umfassenderes Monitoring der energiepolitischen Ziele Frankreichs ist (in französischer und englischer Sprache) auf der Website des [Observatoire Climat-Énergie](https://www.observatoire-climat-energie.fr/) zugänglich, das vom [Réseau Action Climat](https://reseauactionclimat.org/) und dem [CLER-Réseau pour la transition énergétique](https://cler.org/) entwickelt wurde. Das [Barometer der Energiewende in Frankreich](https://energie-fr-de.eu/de/systeme-maerkte/nachrichten/leser/barometer-der-energiewende-in-frankreich.html) des [Deutsch-Französischen Büros für die Energiewende (DFBEW)](https://energie-fr-de.eu/de/startseite.html) bietet auch zusätzliche Informationen zur Energiewende in Frankreich in deutscher Sprache. Unser Open Energy Tracker ergänzt dies unter anderem dadurch, dass hiermit auch Informationen in deutscher Sprache bereit stehen. Außerdem sollen auf dieser Plattform künftig energiepolitische Ziele und Erfolge anderer Länder ergänzt werden. Hierbei sind wir offen für jegliche Form der Zusammenarbeit und freuen uns über gegenseitige Unterstützung.

