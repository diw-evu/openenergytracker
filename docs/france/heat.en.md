---
hide:
#  - navigation
#  - toc
---

# Renewable heat

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

??? info "Scenario Corridor"

    === "GENeSYS-MOD"

        For comparison, each graphic shows the corridor of scenarios developed by a team of researchers from the Technical University of Berlin using the [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) model as part of the European [openENTRANCE](https://openentrance.eu/about-openentrance/) project.

    === "Scenarios"

        For a given indicator, the scenario corridor displays the minimum and maximum values of projections at five-year intervals between 2025 and 2050 among four scenarios. The scenarios considered are: _Directed Transition_, _Gradual Development_, _Societal Commitment_ and _Techno-Friendly_.

        More information on the definition of the scenarios can be found [here](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).


## Share of RES in heat gross final consumption

With regard to final heat consumption, the [Energy Transition Law for Green Growth (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) set a target of 38% of renewable origin for 2030 in 2015.

If all the measures it provides for are effectively put in place, the [Pluriannual Energy Programme (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) estimates that 196 TWh of heating needs should be provided by renewable energies in 2023, i.e. 28% of final heat consumption. In 2028, this share should continue to increase to cover between 219 and 247 TWh of heat, i.e. a share of the final heat consumption amounting to between 34.3% and 38.9%. 

Based on [data and statistical studies](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024) from the Ministry of Energy Transition, renewable energy sources accounted for 29.6% of heat in 2023, which is an increase of almost 2.5 percentage points from 2022. In order to achieve the 2030 target on a linear trajectory, the share would need to be 29.1% in 2023 and 30.4% in 2023. France is therefore slightly ahead (by 0.5 points) of the level predicted by the linear trajectory and has significantly reduced the backlog accumulated in previous years. The 2030 target should be reached by maintaining the average rate of progress of the last five years.

In France, the main components of renewable heating and cooling are solid biomass (57%, including 37% of wood consumption by households) and heat pumps (around 28%). Heat pumps appear to have a growing share of renewable heat supply in France, with an increase of around 11% between 2022 and 2023. Biogas (+21%) is another technology that has developed particularly strongly between 2022 and 2023.

<iframe title = "Figure RES shares in heat in France" src="../../../docs/france/figures/resheat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Heat pumps

### Stock of operating heat pumps

In contrast to the 'traffic light' coalition in Germany, France does not have a quantified target for the stock of heat pumps to be in place by 2030. However, the indicator is still informative. According to the online database of [EurObserv'ER](https://www.eurobserv-er.org/online-database/), there were approximately 10.7 million heat pumps in France in 2023. This stock is primarily composed of aerothermal heat pumps, which derive their heat source from calories in the ambient air outside. In 2023, a bit more than 150,000 units were accounted for by ground-source heat pumps, which extract heat from the ground.

<iframe title = "Figure stock HP in France" src="../../../docs/france/figures/hpstock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Annual heat pump sales and renewable heat production

The 2024 edition of the [key figures for renewable energies](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), produced by the French Ministry of Energy Transition, reports that annual sales of all heat pump technologies exceeded one million in 2021, 2022 and 2023. Sales of air-to-air heat pumps increased slightly in 2023, while sales of air-to-water heat pumps stagnated at around 300,000 units sold. Annual sales of geothermal heat pumps increased slightly in 2023 compared with previous years.

This trend in heat pump sales explains the increase in total heat production by heat pumps, which is one of the targets set out in the Multiannual Energy Programme. The PPE2 sets production at 39.6 TWh for 2023 and between 44 and 52 TWh for 2028. In 2023, heat pump production amounted to 50 TWh (not adjusted for climatic variations).

<iframe title = "Figure sales heat pumps in France" src="../../../docs/france/figures/hpsales.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
