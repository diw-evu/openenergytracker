---
hide:
#  - navigation
#  - toc
---

# Erneuerbare Wärme

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

??? info "Szenariokorridor"

    === "GENeSYS-MOD"

        Zum Vergleich zeigt jede Grafik den Korridor von Szenarien, die von einem Forscherteam der Technischen Universität Berlin mit dem Modell [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) im Rahmen des europäischen Projekts [openENTRANCE](https://openentrance.eu/about-openentrance/) entwickelt wurden.

    === "Szenarien"

        Der Szenariokorridor zeigt für einen bestimmten Indikator die minimalen und maximalen Werte der Projektionen in Fünfjahresintervallen zwischen 2025 und 2050 unter vier Szenarien an. Die betrachteten Szenarien sind: _Gerichteter Übergang_, _Graduale Entwicklung_, _Gesellschaftliches Engagement_ und _Techno-Friendly_.

        Weitere Informationen über die Definition der Szenarien finden Sie [hier](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Anteil der EE am Bruttoendenergieverbrauch für Wärme

Das im Jahr 2015 erlassene [Gesetz über den Übergang zur Energie für grünes Wachstum (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) hat ein Ziel für einen Anteil erneuerbarer Energien am Endenergieverbrauch für Wärme von 38 % für das Jahr 2030 festgelegt.

Wenn alle im [Mehrjährigen Energieprogramm (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf), vorgesehenen Maßnahmen tatsächlich umgesetzt werden, sollten im Jahr 2023 196 TWh Wärme aus erneuerbaren Energien bereit gestellt werden, d.h. 28 % des Endenergieverbrauchs für Wärme. Bis zum Jahr 2028 soll dieser Anteil weiter ansteigen und zwischen 219 und 247 TWh Wärme abdecken, was einem Anteil am Endenergieverbrauch für Wärme zwischen 34,3 und 38,9 % entspricht. 

Auf der Grundlage von [Daten und statistischen Studien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024) des Ministeriums für Energiewende betrug der Anteil der erneuerbaren Energien an der Wärmeversorgung im Jahr 2023 29,6 %, was einem Anstieg von fast 2,5 Prozentpunkten gegenüber 2022 entspricht. Um das Ziel für 2030 bei einem linearen Verlauf zu erreichen, müsste der Anteil 2023 bei 29,1 % und 2023 bei 30,4 % liegen. Frankreich liegt damit leicht (um 0,5 Punkte) über dem nach dem linearen Verlauf vorhergesagten Niveau und hat den in den Vorjahren aufgelaufenen Rückstand deutlich abgebaut. Das Ziel für 2030 dürfte erreicht werden, wenn das durchschnittliche Fortschrittstempo der letzten fünf Jahre beibehalten wird.

In Frankreich sind die wichtigsten Komponenten der erneuerbaren Wärme- und Kälteerzeugung die feste Biomasse (57 %, einschließlich 37 % des Holzverbrauchs der Haushalte) und Wärmepumpen (etwa 28 %). Der Anteil der Wärmepumpen an der Wärmeversorgung aus erneuerbaren Energiequellen scheint in Frankreich mit einem Anstieg von rund 11 % zwischen 2022 und 2023 zuzunehmen. Biogas (+21 %) ist eine weitere Technologie, die sich zwischen 2022 und 2023 besonders stark entwickelt hat.

<iframe title = "Abbildung Anteil erneuerbare Energien Bruttowärmeverbrauch" src="../../../docs/france/figures/resheat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Wärmepumpen

### Bestand an Wärmepumpen 

Im Gegensatz zur Ampel-Koalition in Deutschland gibt es in Frankreich kein quantifiziertes Ziel für den Bestand an Wärmepumpen bis 2030. Der Indikator ist jedoch informativ. Laut der [Online-Datenbank von EurObserv'ER](https://www.eurobserv-er.org/online-database/) gab es in Frankreich im Jahr 2021 etwa 10,7 Millionen Wärmepumpen. Dieser Bestand setzt sich hauptsächlich aus aerothermischen Wärmepumpen zusammen, die ihre Wärmequelle aus den Kalorien der Außenluft beziehen. Im Jahr 2023 werden etwas mehr als 150.000 Einheiten von Erdwärmepumpen genutzt, um Wärme aus dem Erdreich zu gewinnen.

<iframe title = "Figure Bestand WP Frankreich" src="../../../docs/france/figures/hpstock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Jährlicher Absatz von Wärmepumpen und erneuerbare Wärmeerzeugung

Die Ausgabe 2024 der [Schlüsselzahlen für erneuerbare Energien](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), die vom französischen Ministerium für Energiewende erstellt wird, berichtet, dass der jährliche Absatz aller Wärmepumpentechnologien in den Jahren 2021, 2022 und 2023 über einer Million lag. Der Absatz von Luft-Luft-Wärmepumpen stieg 2023 leicht an, während der Absatz von Luft-Wasser-Wärmepumpen bei etwa 300.000 verkauften Einheiten stagnierte. Der jährliche Absatz von Erdwärmepumpen stieg 2023 im Vergleich zu den Vorjahren leicht an.

Diese Entwicklung des Wärmepumpenabsatzes erklärt den Anstieg der Gesamtwärmeerzeugung durch Wärmepumpen, der eines der Ziele des mehrjährigen Energieprogramms darstellt. In der PPE2 wird die Produktion für 2023 auf 39,6 TWh und für 2028 auf 44 bis 52 TWh festgelegt. Im Jahr 2023 belief sich die Wärmepumpenproduktion auf 50 TWh (nicht bereinigt um klimatische Schwankungen).

<iframe title = "Figure sales WP Frankreich" src="../../../docs/france/figures/hpsales.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
