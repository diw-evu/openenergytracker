---
hide:
#  - navigation
#  - toc
---

# Treibhausgasemissionen

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md), Jan Czimmeck

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden

## Sektorale Treibhausgasemissionen

Frankreich muss gemäß den neuesten europäischen Richtlinien bis 2030 seine Treibhausgasemissionen um 55% im Vergleich zum Stand von 1990 senken und bis 2050 Kohlenstoffneutralität erreichen. Das Ziel von -55% für 2030 ergibt sich aus der Anhebung des Ziels von -40% bei der Verabschiedung des europäischen Klimagesetzes im Sommer 2021. Die französische Gesetzgebung wurde bisher noch nicht an das -55%-Ziel angepasst, und die bestehenden Vorschriften basieren auf dem -40%-Ziel für 2030. Das Fünfjahresgesetz zur Energie- und Klimaprogrammierung (LPEC) wird voraussichtlich 2024 umgesetzt, um diese Ziele zu unterstützen.

Um die langfristigen Ziele zur Verringerung der Treibhausgasemissionen zu erreichen, hat Frankreich im Rahmen der nationalen Strategie für niedrige Kohlenstoffemissionen (SNBC) Kohlenstoffbudgets für drei aufeinanderfolgende Fünfjahreszeiträume festgelegt. Die jüngste SNBC (SNBC2) wurde im Jahr 2020 verabschiedet und legt die Kohlenstoffbudgets für die Zeiträume 2019-2023, 2024-2028 und 2029-2033 fest. Da der SNBC2 vor der Erhöhung der Ambitionen zur Reduzierung der Treibhausgasemissionen von 40 % auf 55 % im Jahr 2022 entworfen wurde, müssen die Kohlenstoffbudgets im kommenden SNBC (SNBC3), der voraussichtlich 2024 verabschiedet wird, neu angepasst werden.

Kohlenstoffbudgets setzen voraus, dass die durchschnittlichen Emissionen während des angegebenen Zeitraums das zugewiesene Budget nicht überschreiten. Die jährlichen und sektoralen Budgets dienen lediglich zu Informationszwecken und sind nicht verbindlich. Es ist möglich, dass niedrigere Emissionen in einem Sektor höhere Emissionen in einem anderen Sektor kompensieren. Dies gilt auch für Emissionen zwischen den Jahren innerhalb eines Fünfjahreszeitraums.

Für den Zeitraum 2019-2023 muss Frankreich sein Kohlenstoffbudget (421 MtCO2e) einhalten. Laut [CITEPA-Daten](https://www.citepa.org/fr/secten/) werden im Jahr 2023 etwa 373 MtCO2e emittiert. Der Abfallsektor (15 MtCO2e) übertrifft das zugewiesene indikative Budget, während alle anderen Sektoren besser abschneiden als erwartet. Diese Jahre waren von globalen Krisen geprägt, die sich erheblich auf den Energieverbrauch und die Produktionsmuster ausgewirkt haben. Aufgrund der außergewöhnlichen Umstände in den Jahren 2020, 2021 und 2022 ist bei der Interpretation dieser Zahlen Vorsicht geboten. Der Rückgang der Emissionen scheint sich jedoch 2023 für alle Sektoren zu bestätigen, mit Ausnahme des Abfallsektors, dessen Emissionen konstant bleiben.

<iframe title = "Figure part des EnR transport en France" src="../../../docs/france/figures/ghs_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>


## CO<sub>2</sub>-Emissionen der Stromerzeugung

Aufgrund der wichtigen Rolle der Kernenergie im französischen Strommix ist die Stromerzeugung in Frankreich eine der kohlenstoffärmsten der Welt. Nach [Angaben](https://analysesetdonnees.rte-france.com/en/emission/ghg-emissions) des Réseau de Transport d'Électricité (RTE) schwanken die Treibhausgasemissionen bei der Stromerzeugung saisonal. Die Stromerzeugung erzeugt im Winter mehr Treibhausgase, mit Spitzenwerten zwischen November und Januar. In den letzten zehn Jahren lag die maximale Kohlenstoffintensität bei etwa 90 g/kWh (November 2016), wobei die Winterspitze seit 2018 in der Regel bei etwa 60 g/kWh liegt. Umgekehrt liegt das Minimum, das in der Regel im Frühsommer erreicht wird, bei etwa 20-25 g/kWh und war so niedrig wie 11 g/kWh (April 2020). Diese Schwankungen betreffen sowohl die Produktion in absoluten Zahlen (in Millionen Tonnen emittiert) als auch in relativen Zahlen (in Gramm emittiert pro erzeugter kWh Strom). Die in diesem Schaubild dargestellte Kohlenstoffintensität der Stromerzeugung entspricht dem monatlichen Durchschnitt der Kohlenstoffintensität in 30-Minuten-Auflösung (die verwendeten Rohdaten sind [hier](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs) verfügbar).

<iframe title = "Figure carbon intensity power generation France" src="../../../docs/france/figures/carbonintensity.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>