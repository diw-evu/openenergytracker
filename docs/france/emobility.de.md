---
hide:
#  - navigation
#  - toc
---

# Mobilität

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

## Anteil erneuerbarer Energien am Kraftstoffverbrauch

In Frankreich ist Biodiesel der wichtigste Kraftstoff aus erneuerbaren Energiequellen. Im Jahr 2023 machte Biodiesel 72 % des Bruttoendverbrauchs von Energie aus erneuerbaren Quellen für Kraftstoffe aus. Biobenzin (oder Bioethanol) machte knapp ein Drittel (26 %) dieses Verbrauchs aus. 

Im Jahr 2015 wurde mit dem französischen Gesetz zur Energiewende für grünes Wachstum (*Loi sur la transition énergétique pour le croissance verte*, LTCEV) ein Ziel von 15 % für den Anteil erneuerbarer Energien am Bruttoendverbrauch von Kraftstoffen bis 2030 eingeführt. Nach Angaben des [Amt für statistische Daten und Studien (SDES)](https://www.statistiques.developpement-durable.gouv.fr/les-energies-renouvelables-en-france-en-2023-dans-le-cadre-du-suivi-de-la-directive-ue-20182001-0) des französischen Ministeriums für den ökologischen Wandel lag der Anteil der erneuerbaren Energien am Endverbrauch von Kraftstoffen im Jahr 2023 bei etwa 10 % und damit ungefähr 2 Prozentpunkte unter der linearen Entwicklung zwischen 2015 und 2030. 

Der Anteil erneuerbarer Kraftstoffe stagniert seit 2015 und scheint sogar leicht rückläufig zu sein. Dies spiegelt teilweise die Tatsache wider, dass die Strategie zur Dekarbonisierung des Verkehrs eher auf die Elektrifizierung von Fahrzeugen als auf die Verwendung von Biokraftstoffen setzt. Der leichte Rückgang könnte aber auch auf eine Änderung der Methodik im Jahr 2021 zurückzuführen sein. In jedem Fall deutet die derzeitige Dynamik darauf hin, dass das Ziel für 2030 ohne eine deutliche Beschleunigung nicht erreicht werden kann.

<iframe title = "Figure RES share in transport France" src="../../../docs/france/figures/restransp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
    
## Elektromobilität

### Elektrofahrzeuge

In der im April 2020 verabschiedeten [Mehrjährigen Energieplanung](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/) hat sich die französische Regierung das Ziel gesetzt, bis Ende 2023 660.000 und bis Ende 2028 3 Millionen Elektrofahrzeuge auf die Straße zu bringen. Ende November 2022 kündigte die Regierung außerdem [ein Ziel von 2 Millionen Fahrzeugen](https://www.gouvernement.fr/actualite/2-millions-de-voitures-electriques-en-2030) an, die bis 2030 in Frankreich produziert werden sollen, sowie eine finanzielle Unterstützung für die Automobilindustrie in Höhe von 100 Millionen Euro, um den technologischen Übergang des Sektors zur Elektrifizierung zu vollenden.

Laut [Avere France Registration Barometer](https://www.avere-france.org/publication/barometre-novembre-2024-37-231-vehicules-immatricules-en-baisse-de-242-par-rapport-a-novembre-2023/) gab es Ende November 2024 in Frankreich 2.002.654 Plug-in-Fahrzeuge, von denen 1.304.260 rein elektrisch waren. Die restlichen 698.394 Fahrzeuge sind Plug-in-Hybride. Die verfügbaren Statistiken definieren Fahrzeuge so, dass sie sowohl Personenkraftwagen als auch Nutzfahrzeuge umfassen. Nutzfahrzeuge sind nicht für den Personentransport ausgelegt, sondern für den Transport von Gütern. Dennoch handelt es sich um leichte Fahrzeuge mit einem Höchstgewicht von 3,5 Tonnen. Personenkraftwagen hingegen sind für die Personenbeförderung ausgelegt und haben neben dem Fahrersitz maximal 8 Sitzplätze.

<iframe title = "Figure BEV Bestand" src="../../../docs/france/figures/bev_stock.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

Die Entwicklung des Fahrzeugbestands kann auch anhand der monatlichen Neuzulassungen verfolgt werden. Die Daten des Zulassungsbarometers erlauben eine Unterscheidung zwischen rein elektrischen Pkw und rein elektrischen Nutzfahrzeugen. Im Juli 2024 machten die Neuzulassungen von 100% elektrischen Pkw mit 23.255 Zulassungen 17,4% des Marktanteils aus. Bei den 100 % elektrischen Personenkraftwagen verzeichnete das Renault Renault 5 die meisten Zulassungen (3.316 Neuzulassungen), gefolgt vom Tesla Model Y (3.175 Zulassungen) und dem Citroen E-C3 (1.239 Zulassungen).

<iframe title = "Figure Neuzulassungen" src="../../../docs/france/figures/bev_adm.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

### Ladeinfrastruktur

Laut dem [Nationalen Barometer der öffentlich zugänglichen Ladeinfrastruktur](https://www.avere-france.org/publication/barometre-152-887-points-de-recharge-ouverts-au-public-fin-novembre-2024/), das vom Ministerium für den ökologischen Übergang in Zusammenarbeit mit Areve-France auf der Grundlage der Daten von GIREVE erstellt wurde, gab es am 30. November 2024 in Frankreich 152.887 öffentliche Ladepunkte, davon 26.326 in der Region Paris. 48 % der Ladepunkte haben eine Leistung zwischen 7,4 und 22 kW und 33 % weniger als 7,4 kW. Ladepunkte mit einer Leistung von 22 kW oder mehr machen 19 % aller öffentlich zugänglichen Ladepunkte aus.

In der [Mehrjährigen Energieplanung](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/) wird das Ziel von 100.000 öffentlichen Stromzapfsäulen bis 2023 und von 7 Millionen öffentlichen und privaten Stromzapfsäulen bis 2030 genannt. Die Installation von Stromzapfsäulen hat sich in den letzten 12 Monaten deutlich beschleunigt, so dass das Ziel von 100.000 öffentlichen Stromzapfsäulen in Frankreich bis Ende Mai 2023 erreicht werden kann. Am 27. Oktober 2023 [gab die Regierung](https://www.gouvernement.fr/upload/media/content/0001/07/f74b604a7c975bf6f83c6d28656df3a96ee485b1.pdf) ein neues Ziel von 400.000 Stromzapfsäulen bis 2030 bekannt, darunter 50.000 Schnellzapfsäulen. Enedis, der französische Verteilernetzbetreiber, [schätzt](https://data.enedis.fr/pages/points-de-charge/), dass es in Frankreich Ende des dritten Quartals 2024 insgesamt 2.213.322 Ladepunkte (private, öffentliche und gewerbliche) gab.

<iframe title = "Abbildung Ladeinfrastruktur" src="../../../docs/france/figures/cs.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>

Aus den Daten zu den öffentlich zugänglichen Ladepunkten und dem Bestand an aufladbaren Fahrzeugen lässt sich die Anzahl der aufladbaren Fahrzeuge - insgesamt oder nur reine Elektro- oder Hybridfahrzeuge - pro öffentlich zugänglichem Ladepunkt berechnen. Im Juli 2024 gab es in Frankreich etwa 8 reine Elektrofahrzeuge pro öffentlich zugänglichem Ladepunkt. Hinter dieser Zahl können sich jedoch geografische Unterschiede verbergen, da die Verteilung der öffentlich zugänglichen Ladepunkte in einem Departement nicht unbedingt der Anzahl der dort aufladbaren Fahrzeuge entspricht.

<iframe title = "Abbildung Ladeinfrastruktur" src="../../../docs/france/figures/bev_per_cp.html" width="100%" height= "600px" frameBorder="0"  loading = "lazy"></iframe>



