---
hide:
#  - navigation
#  - toc
---

# Chaleur renouvelable

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

??? info "Corridor de scénarios"

    === "GENeSYS-MOD"

        Pour comparaison, chaque graphique permet de visualiser le corridor des scénarios développés par une équipe de chercheurs de l'université technique de Berlin à l'aide du modèle [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) dans le cadre du projet européen [openENTRANCE](https://openentrance.eu/about-openentrance/).

    === "Scénarios"

        Pour un indicateur donné, le corridor de scénarios permet de visualiser les valeurs minimales et maximales des projections à intervalle de cinq ans entre 2025 et 2050 parmi quatre scénarios. Les scénarios considérés sont les suivants : _Directed Transition_, _Gradual Development_, _Societal Commitment_ et _Techno-Friendly_.

        Plus d'informations sur la définition des scénarios sont à trouver [ici](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

## Part des EnR dans la consommation brute finale de chaleur 

En ce qui concerne la consommation finale de chaleur, la [Loi de transition énergétique pour la croissance verte (LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) a fixé en 2015 une cible de 38% d'origine renouvelable pour 2030.

Si toutes les mesures qu'elle prévoit sont effectivement mises en place, la [Programmation Pluriannuelle de l'Énergie (PPE)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) estime à 196 TWh les besoins de chaleur qui devraient être assurés par des énergies renouvelables en 2023, soit 28% de la consommation de chaleur finale. En 2028, cette part devrait continuer d'augmenter pour couvrir entre 219 et 247 TWh de chaleur, soit une part de la consommation finale de chaleur s'élevant entre 34,3% et 38,9%. 

D'après les [données et études statistiques du Ministère de la Transition Énergétique](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), la chaleur provenait à 29,6% de sources d'énergie renouvelables en 2023. Cela représente une hausse de près de 2,5 points de pourcentage par rapport à 2022. Pour tenir le cap vers l'objectif de 2030 en suivant une trajectoire linéaire, cette part devrait être de 29,1% en 2023 et de 30,4% en 2024. La France est donc légèrement en avance (0,5 point) par rapport au niveau prévu par la trajectoire linéaire et a significativement réduit son retard accumulé les années précédentes. En maintenant le rythme moyen de progression à l'oeuvre les cinq dernières années, l'objectif de 2030 devrait être atteint.

En France, les principales composantes de l'approvisionnement de chaleur et de froid de source renouvelable sont la biomasse solide (57%, dont 37% de consommation de bois par les ménages) et les pompes à chaleur (environ 28%). Ces dernières semblent représenter une part croissante de l'approvisionnement de chaleur de source renouvelable en France, avec une évolution d'environ 11% entre 2022 et 2023. Parmi les autres technologies s'étant particulièrement développées entre 2022 et 2023, on compte également le biogaz (+21%).

<iframe title = "Figure part des EnR chaleur en France" src="../../../docs/france/figures/resheat.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Pompes à chaleur

### Stock de pompes à chaleur en opération

Contrairement à la coalition "feu tricolore" en Allemagne, il n'existe pas en France d'objectif chiffré concernant le stock de pompes à chaleur devant être en place à horizon 2030. Cet indicateur reste néanmoins instructif. D'après la banque de données en ligne concernant les pompes à chaleur de [EurObserv'ER](https://www.eurobserv-er.org/online-database/), il y avait en 2023 en France environ 10,7 millions de pompes à chaleur. Ce stock est essentiellement constitué de pompes à chaleur aérothermiques, c'est-à-dire dont la source de chaleur provient des calories de l'air ambient extérieur. Les pompes à chaleur géothermiques, captant les calories dans le sol, ne représentaient en 2023 qu'un peu plus de 150 000 appareils. 

<iframe title = "Figure stock PAC en France" src="../../../docs/france/figures/hpstock.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

### Ventes annuelles de pompes à chaleur et production de chaleur

D'après l'édition 2024 des [chiffres clés des énergies renouvelables](https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-des-energies-renouvelables-edition-2024), produits par le Ministère de la Transition Énergétique, les ventes annuelles de pompes à chaleur toutes technologies confondues ont dépassé le million en 2021, 2022 et 2023. La vente des pompes à chaleur air-air a légèrement augmenté en 2023, tandis que les ventes de pompes à chaleur air-eau ont stagné autour de 300,000 unités vendues. Les ventes annuelles de pompes à chaleur géothermiques ont légèrement augmenté en 2023 par rapport aux années précédentes.

Cette évolution des ventes de pompes à chaleur explique l'augmentation de la production totale de chaleur par les pompes à chaleur, cette dernière figurant comme objectif de la Programmation Pluriannuelle de l'Énergie. La PPE2 fixe à 39.6 TWh la production pour 2023 et entre 44 et 52 TWh pour 2028. En 2023, la production de chaleur par pompes à chaleur s'est élevée à 50 TWh (non corrigée des variations climatiques).

<iframe title = "Figure ventes annuelles PAC en France" src="../../../docs/france/figures/hpsales.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe> 