---
hide:
#  - navigation
#  - toc
---
# Mobility 

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Share of renewable energy in fuel consumption

In France, the main fuel produced from renewable energy sources is biodiesel. In 2023, biodiesel accounts for 72% of the gross final consumption of energy from renewable sources used for fuels. Bio-gasoline (or bio-ethanol) accounts for just under a third (26%) of this consumption. 

In 2015, the French Energy Transition for Green Growth Act (LTCEV) introduced a target of 15% renewable energy in gross final fuel consumption by 2030. According to the [Statistical Data and Studies Department (SDES)](https://www.statistiques.developpement-durable.gouv.fr/les-energies-renouvelables-en-france-en-2023-dans-le-cadre-du-suivi-de-la-directive-ue-20182001-0) of the French Ministry for Ecological Transition, the share of renewable energy in final fuel consumption was about 10% in 2023, about 2 percentage points below the linear trajectory between 2015 and 2030. 

The share of renewable fuels has stagnated since 2015. In fact, the trend appears to be slightly downwards. This partly reflects the fact that the strategy for decarbonising transport is based more on vehicle electrification than on the use of biofuels. However, the slight decline may also be due to a change in methodology in 2021. In any case, the current momentum suggests that the 2030 target will not be met unless there is a strong acceleration.

<iframe title = "Figure RES share in transport France" src="../../../docs/france/figures/restransp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Electric mobility

### Plug-in vehicles 

In the [Pluriannual Energy Programming adopted in April 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/), the French government set a target of 660,000 electric vehicles on the road by the end of 2023 and 3 million by the end of 2028. The PPE also sets a target for the number of plug-in hybrid passenger cars of 500,000 by the end of 2023 and 1.8 million by the end of 2028. At the end of November 2022, the government also announced [a target of 2 million vehicles](https://www.gouvernement.fr/actualite/2-millions-de-voitures-electriques-en-2030) _produced_ in France by 2030, as well as financial support for the automotive industry of 100 million euros to complete the sector's technological transition to electrification.

According to the [Avere France Registration Barometer](https://www.avere-france.org/publication/barometre-novembre-2024-37-231-vehicules-immatricules-en-baisse-de-242-par-rapport-a-novembre-2023/), at the end of November 2024, there were 2,002,654 plug-in vehicles in France, of which 1,304,260 were 100% electric. The other 698,394 vehicles are plug-in hybrids. The available statistics define vehicles as including both passenger cars and commercial vehicles. Commercial vehicles are not designed to carry passengers but to transport goods. However, they are still light vehicles with a maximum weight of 3.5 tonnes. Passenger cars, on the other hand, are designed to carry passengers and have a maximum of 8 seats in addition to the driver's seat.

<iframe title = "Figure BEV stock" src="../../../docs/france/figures/bev_stock.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

The development of the plug-in vehicle fleet can also be tracked using the number of new registrations per month. The registration barometer data distinguishes between 100% electric passenger cars and 100% electric commercial vehicles. In November 2024, registrations of new 100% electric passenger cars accounted for 17.4% of market share, with 23,255 registrations. For 100% electric passenger cars, the Renault Renault 5 accounted for the highest number of registrations (3,316 new registrations), followed by the Tesla Model Y (3,175 registrations) and the Citroen E-C3 (1,239 registrations).

<iframe title = "Figure new registrations" src="../../../docs/france/figures/bev_adm.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

### Charging infrastructure

According to the [National barometer of recharging infrastructures open to the public](https://www.avere-france.org/publication/barometre-152-887-points-de-recharge-ouverts-au-public-fin-novembre-2024/), developed by the Ministry for Ecological Transition with Areve-France using data from GIREVE, there were 152,887 public charging points in France on 30 November 2024, including 26,326 in the Paris region. 48% of the charging points have a capacity between 7.4 and 22 kW and 33% less than 7.4 kW. Charging points with a capacity of 22 kW or more represent 19% of all charging points open to the public.

The [Pluriannual Energy Programming](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/) mentions a target of 100,000 public chargepoints by 2023 and 7 million public and private chargepoints by 2030. The deployment of chargepoints has accelerated significantly in the last 12 months, making it possible to reach the target of 100,000 public chargepoints in France by the end of May 2023. On 27 October 2023, the government [announced](https://www.gouvernement.fr/upload/media/content/0001/07/f74b604a7c975bf6f83c6d28656df3a96ee485b1.pdf) a new target of 400,000 chargepoints by 2030, including 50,000 fast chargepoints. Enedis, the French distribution network operator, [estimates](https://data.enedis.fr/pages/points-de-charge/) that there is a total of 2,213,322 charging points (private, public and business) in France by the end of the third quarter of 2024.

<iframe title = "Figure charging infrastructure" src="../../../docs/france/figures/cs.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>

Using data on public charging points and the stock of rechargeable vehicles, it is possible to calculate the number of rechargeable vehicles - total, 100% electric only or hybrids only - per public charging point. In July 2024, there were about 8 fully electric vehicles per public charging point in France. However, this figure may mask potential geographical disparities, as the distribution of public charging points in a given département does not necessarily correspond perfectly to the stock of rechargeable vehicles in the same département.

<iframe title = "Figure bev per charging infrastructure" src="../../../docs/france/figures/bev_per_cp.html" width="100%" height= "600px" frameBorder="0" loading = "lazy"></iframe>
