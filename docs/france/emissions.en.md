---
hide:
#  - navigation
#  - toc
---

# Greenhouse gas emissions

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md), Jan Czimmeck

!!! info "Hint"
    
    All our figures are interactive: in the upper-right corner of every figure, you find buttons for zooming in and out. By clicking on a time series in a legend, you can add or hide them from the figure.

## Sectoral greenhouse gas emissions

France is required to reduce its greenhouse gas (GHG) emissions by 55% by 2030 compared to 1990 levels, in accordance with the latest European directives. Additionally, carbon neutrality must be achieved by 2050. The -55% target for 2030 is a result of the -40% target being raised when the European climate law was adopted in the summer of 2021. Currently, French legislation has not been updated to reflect the -55% target, and existing regulations are based on the -40% target for 2030. The five-year Energy and Climate Programming Law (LPEC) is expected to be implemented in 2024 to support these targets.

In order to achieve the long-term greenhouse gas emission reduction targets, France has set carbon budgets for three successive five-year periods through the National Low Carbon Strategy (SNBC). The latest SNBC (SNBC2) was adopted in 2020, which sets the carbon budgets for the periods 2019-2023, 2024-2028, and 2029-2033. As the SNBC2 was designed prior to the increase in ambitions to reduce greenhouse gas emissions from 40% to 55% in 2022, the carbon budgets will need to be readjusted in the upcoming SNBC (SNBC3), which is expected to be adopted in 2024. 

Carbon budgets require that average emissions during the specified period do not exceed the allocated budget. Additionally, annual and sectoral budgets are provided for informational purposes only and are not binding. It is possible for lower emissions in one sector to compensate for higher emissions in another sector. This also applies to emissions between years within a five-year period.

For the 2019-2023 period, France is projected to meet its carbon budget (421 MtCO2e), with approximately 373 MtCO2e emitted in 2023 according to [CITEPA data](https://www.citepa.org/fr/secten/). The waste sector (14 MtCO2e) appear to exceed the indicative budget allocated, while all other sectors are performing better than expected. These years were marked by global crises that significantly impacted energy consumption and production patterns. It is important to exercise caution when interpreting these figures due to the exceptional circumstances of the years 2020, 2021, and 2022. However, the decrease in emissions seems to be confirmed in 2023 for all sectors except the waste sector, which keeps its emissions constant.

<iframe title = "Figure part des EnR transport en France" src="../../../docs/france/figures/ghs_france.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## CO<sub>2</sub> intensity of power generation

Because of the important role played by nuclear power in the French electricity mix, electricity generation in France is one of the most carbon-free in the world. According to [data](https://analysesetdonnees.rte-france.com/en/emission/ghg-emissions) from the Réseau de Transport d'Électricité (RTE), greenhouse gas emissions from electricity generation vary seasonally. Electricity production generates more greenhouse gases in winter, with peaks between November and January. Over the last ten years, the maximum carbon intensity has been around 90 g/kWh (November 2016), although the winter peak has usually been around 60 g/kWh since 2018. Conversely, the minimum, usually reached in early summer, is around 20-25 g/kWh and has been as low as 11 g/kWh (April 2020). This variation concerns both production in absolute terms (in million tonnes emitted) and in relative terms (in grams emitted per kWh of electricity produced). The carbon intensity of electricity generation shown in this graph corresponds to the monthly average of the carbon intensity available at the 30-minute resolution (the raw data used is available [here](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs)).

<iframe title = "Figure carbon intensity power generation France" src="../../../docs/france/figures/carbonintensity.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>