---
hide:
#  - navigation
#  - toc
---

# Kommerzieller Außenhandel

=== "Autor*Innen"

    Jan Czimmeck, [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.


## Deutschland und Belgien

Obwohl Frankreich traditionell ein Nettoexporteur von Strom ist, schwankt der Handel von Monat zu Monat. Nach den [Daten](https://analysesetdonnees.rte-france.com/en/markets/imports-exports) des französischen Réseau de Transport d'Électricité (RTE) importiert Frankreich im Herbst und Winter mehr Strom als es nach Deutschland und Belgien exportiert, während es im Frühjahr und Sommer umgekehrt ist. Das Jahr 2022 bildet eine bemerkenswerte Ausnahme. In diesem Jahr war Frankreich das ganze Jahr über ein Nettoimporteur aus Belgien und Deutschland, auch im Frühjahr und Sommer 2022. Dies ist wahrscheinlich eine direkte Folge der Abschaltung zahlreicher Kernreaktoren aufgrund technischer Probleme und des historisch niedrigen Niveaus der an die Wasserkraftwerke angeschlossenen Stauseen. 

<iframe title = "Abbildung Stromhandel" src="../../../docs/france/figures/powertrade.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>