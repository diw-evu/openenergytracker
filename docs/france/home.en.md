---
description: The Open Energy Tracker visualises governmental goals and progress for selected areas of the energy transition.

hide:
#  - navigation
#  - toc
---

# France

=== "Authors"

    [Adeline Guéret](../../docs/about/authors.md)

!!! Success "What's new"

    * **Updated data** for all sections 
    * New section **Renewable heat**
    * New section **Greenhouse gas emissions** 
    * New indicators: installed hydroelectric capacity, share of renewable energy in electricity generation, fuel consumption, heat consumption.
    * New indicators: installed hydroelectric capacity, share of renewable energy in electricity production, fuel consumption, heat consumption. 
    * New targets derived from the *French Strategy for Energy and Climate* and the *Integrated National Climate-Energy Plan*.
  
The objectives of the energy transition in France are defined by the [Pluriannual Energy Programming](https://www.ecologie.gouv.fr/programmations-pluriannuelles-lenergie-ppe) (in French *Programmation Pluriannuelle de l'Énergie* (PPE)), introduced at the same time as the [National Low Carbon Strategy](https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc) (in French, *Stratégie Nationale Bas Carbone* (SNBC)) by the [Law on the Energy Transition for Green Growth](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) (in French, *Loi de Transition Énergétique pour la Croissance Verte* (LTECV)) in August 2015. The [Energy and Climate Law](https://www.ecologie.gouv.fr/loi-energie-climat) (in French, *Loi énergie-climat* (LEC)), enacted in November 2019, has modified some of the targets adopted in the LTECV and should now be taken as a reference for the definition of targets in future PPEs. 

The PPE focuses on indicators related to energy production and consumption. The SNBC, on the other hand, defines the objectives to be achieved by France in terms of reducing greenhouse gas emissions. These two steering tools must be compatible with each other. This means, in particular, that the emission reduction targets are a "sine qua non" condition for the objectives of the EPP.  

At  the moment, the PPE defines the objectives for a ten-year period from 2019 to 2028, split into two five-year periods (2019-2023 and 2024-2028). Thus, quantitative targets are formulated for 2023 and 2028. In addition, sometimes two targets are provided for 2028, namely a high target and a low target. 

The SNBC provides the long-term greenhouse gas emissions trajectory, i.e. the trajectory to achieve carbon neutrality by 2050 in accordance with what EU countries decided together in order to meet the targets of the Paris Agreement. It also defines intermediate targets, called carbon budgets. In April 2020, the carbon budgets for the periods 2019-2023, 2024-2028 and 2029-2033 were set by the SNBC2. 

Both the PPE and the SNBC are intended to be revised every five years in order to adjust the targets to the results obtained in the previous period. The next revision (SNBC3 and PPE3) should have been adopted by July 2024 at the latest.

The 2019 energy and climate law also introduced the obligation for the government to adopt a five-year energy and climate programming law (LPEC), to be revised every five years, the first of which should be adopted before 1 July 2023. This five-year law "*defines the objectives and establishes the priorities for action of the national energy policy in response to the environmental and climate emergency*" (Article L100-1 of the Energy Code). The government of Elisabeth Borne failed to meet this deadline.

In November 2023, a strategic document, the [French Energy and Climate Strategy](https://www.ecologie.gouv.fr/sites/default/files/23242_Strategie-energie-climat.pdf) (*Stratégie Française pour l'Énergie et le Climat* in French, shortened SFEC), was published for public consultation. This document provides an overview of the targets that could be proposed in the PPE3, although it is not yet final. In addition, a draft law on energy sovereignty was submitted to two councils (the Cese and the CNTE) for consideration at the end of December 2023, which could be the legislative text intended by the government to replace the LPEC.

A much more comprehensive monitoring of energy policy targets in France is available online (in French) on the [Climate-Energy Observatory](https://www.observatoire-climat-energie.fr/) developed by [Climate Action Network](https://reseauactionclimat.org/) and [CLER-Réseau pour la transition énergétique](https://cler.org/). The [Barometer of the energy transition in France](https://energie-fr-de.eu/de/systeme-maerkte/nachrichten/leser/barometer-der-energiewende-in-frankreich.html) of the [Franco-German Office for Energy Transition (OFATE/DFBEW)](https://energie-fr-de.eu/de/startseite.html) also provides additional information on the energy transition in France in German. Our aim here is also to make the monitoring of French energy policy accessible in German and English and to contribute to the development of a platform that brings together resources on energy policy and its progress in several countries. Any collaboration and mutual assistance is welcome.

