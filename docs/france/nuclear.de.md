---
hide:
#  - navigation
#  - toc
---

# Atomkraft

=== "Autor*Innen"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Hinweis"
    
    Alle unsere Abbildungen sind interaktiv: im oberen rechten Bereich jeder Abbildung finden Sie Buttons, mit denen Sie zoomen können. Einzelne Zeitreihen können durch Klicken in der Legende ein- und ausgeblendet werden.

??? info "Szenariokorridor"

    === "GENeSYS-MOD"

        Zum Vergleich zeigt jede Grafik den Korridor von Szenarien, die von einem Forscherteam der Technischen Universität Berlin mit dem Modell [GENeSYS-MOD](https://openentrance.eu/2021/04/27/genesys-mod-tu-berlin/) im Rahmen des europäischen Projekts [openENTRANCE](https://openentrance.eu/about-openentrance/) entwickelt wurden.

    === "Szenarien"

        Der Szenariokorridor zeigt für einen bestimmten Indikator die minimalen und maximalen Werte der Projektionen in Fünfjahresintervallen zwischen 2025 und 2050 unter vier Szenarien an. Die betrachteten Szenarien sind: _Gerichteter Übergang_, _Graduale Entwicklung_, _Gesellschaftliches Engagement_ und _Techno-Friendly_.

        Weitere Informationen über die Definition der Szenarien finden Sie [hier](https://openentrance.eu/2022/07/06/quantitative-scenarios-for-low-carbon-futures-of-the-european-energy-system-oncountry-region-and-local-level/).

    
## Anteil der Atomkraft an der Stromerzeugung

Frankreich verfügt über 56 Kernreaktoren an 18 Standorten mit einer installierten Gesamtkapazität von 61,4 GW. Nach den vom Réseau de Transport d'Électricité [(RTE)](https://www.rte-france.com/eco2mix) veröffentlichten [Strombilanzdaten 2022](https://analysesetdonnees.rte-france.com/bilan-electrique-production) beträgt der Anteil der Kernenergie an der Stromerzeugung in Frankreich im Jahr 2022 62,7 %.

Das im August 2015 verabschiedete Gesetz über die Energiewende für grünes Wachstum [(LTECV)](https://www.ecologie.gouv.fr/loi-transition-energetique-croissance-verte) hatte zum Ziel, den französischen Strommix zu diversifizieren. Hierbei soll der Anteil der Kernenergie an der Stromerzeugung bis 2025 auf 50% reduziert werden. Mit dem Energie- und Klimagesetz [(LEC)](https://www.ecologie.gouv.fr/loi-energie-climat) vom November 2019 wurde dieses Ziel jedoch auf 2035 verschoben. Der Artikel zu diesem Ziel wurde durch das Gesetz Nr. 2023-491 vom 22. Juni 2023 aufgehoben. Ein weiterer Artikel, der eine Obergrenze für die Gesamtkapazität der Kernkraftwerke von 63,2 GW festlegte, wurde ebenfalls aufgehoben. Somit gibt es keine Obergrenze mehr. Diese Gesetzesänderung spiegelt die Vision der französischen Regierung für die Energiewende wider, die Präsident Emmanuel Macron in seiner Rede in Belfort am 10. Februar 2022 dargelegt hat. Der Plan sieht vor, bestehende Kernreaktoren auszubauen und neue zu bauen, um die Kernenergie zu einem zentralen Bestandteil der Energiewende zu machen.

Das Mehrjährige Energieprogramm [(PPE2)](https://www.ecologie.gouv.fr/sites/default/files/20200422%20Programmation%20pluriannuelle%20de%20l%27e%CC%81nergie.pdf) sah vor, dass 14 Kernreaktoren in Frankreich bis 2035 stillgelegt werden sollten. Der Entwurf der nächsten Version des PPE (PPE3) schlägt jedoch vor, den Betrieb der bestehenden Reaktoren um 50 oder 60 Jahre zu verlängern und sechs neue EPR2-Reaktoren zu bauen. Die Arbeiten sollen bis 2025 beginnen. Schließlich ist geplant, acht weitere Reaktoren mit einer Gesamtkapazität von 13 GW zu bauen. Eine endgültige Entscheidung wird in dieser Angelegenheit bis 2026 getroffen. Insgesamt wird erwartet, dass die Stromerzeugung aus Kernenergie bis 2030 zwischen 360 TWh und 400 TWh liegen wird.

Trotz der Streichung des Ziels, den Anteil der Kernenergie an der französischen Stromerzeugung zu verringern, behalten wir die entsprechende Grafik bei, um die Entwicklung dieses Indikators zu verfolgen.

<iframe title = "Abbildung anteil der Atomkraft an der Stromerzeugung in Frankreich" src="../../../docs/france/figures/nuclear.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

