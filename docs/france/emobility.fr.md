---
hide:
#  - navigation
#  - toc
---
# Transports 

=== "Auteurs"

    [Adeline Guéret](../../docs/about/authors.md)

!!! info "Astuce"
    
    Tous les graphiques sont interactifs. Vous trouverez en haut à droite de chaque graphique un bouton vous permettant d'agrandir ou de rétrécir l'image. Par ailleurs, il est possible de faire apparaître ou de supprimer une ou plusieurs série.s temporelle.s du graphique en cliquant sur l'élément correspondant dans la légende qui se situe en-dessous dudit graphique.

## Part des énergies renouvelables

En France, le carburant produit à partir d'énergies renouvelables est principalement du biogazole (ou biodiesel). En effet, le biogazole représentait en 2023 72% de la consommation finale brute d'énergie d'origine renouvelable utilisée pour les carburants. La bioessence (ou bioéthanol) représente un peu moins d'un tiers (26%) de cette consommation. 

La loi de transition énergétique pour la croissance verte (LTCEV) a introduit en 2015 l'objectif d'atteindre 15% d'énergies renouvelables dans la consommation finale de carburants en 2030. D'après les [données du service des données et études statistiques (SDES)](https://www.statistiques.developpement-durable.gouv.fr/les-energies-renouvelables-en-france-en-2023-dans-le-cadre-du-suivi-de-la-directive-ue-20182001-0) du Ministère de la Transition Écologique, cette part s'élève en 2023 à 10%, soit environ de 2 points de pourcentage en dessous de la trajectoire linéaire entre 2015 et 2030. 

La part des carburants produits à partir d'énergies renouvelables stagne depuis 2015. Il semblerait même que la tendance soit légèrement à la baisse. Cela reflète en partie le fait que la stratégie de décarbonation des transports repose davantage sur l'électrification des véhicules que le déploiements des biocarburants. Toutefois, le léger recul observé peut également être dû à un changement méthodologique intervenu en 2021. Quoiqu'il en soit, la dynamique actuelle laisse penser que l'objectif pour 2030 ne sera pas atteint, à moins d'accélérer drastiquement. 

<iframe title = "Figure part des EnR transport en France" src="../../../docs/france/figures/restransp.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>

## Électromobilité


### Voitures électriques

Dans la [Programmation Pluriannuelle de l'Énergie adoptée en avril 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/), le gouvernement français a fixé un objectif de 660 000 véhicules électriques en circulation d'ici fin 2023 et 3 millions d'ici fin 2028. La PPE fixe également un objectif pour le nombre de voitures particulières hybride rechargeable de 500 000 à fin 2023 et de 1,8 million à fin 2028. Fin novembre 2022, le gouvernement a également [annoncé l'objectif de 2 millions de véhicules](https://www.gouvernement.fr/actualite/2-millions-de-voitures-electriques-en-2030) _produits_ en France en 2030 ainsi que des soutiens financiers à l'industrie automobile à hauteur de 100 millions d'euros, afin de mener à bien la transition technologique du secteur vers l'électrique. 

D'après les données du [Baromètre des immatriculations de l'Avere France](https://www.avere-france.org/publication/barometre-novembre-2024-37-231-vehicules-immatricules-en-baisse-de-242-par-rapport-a-novembre-2023/), il y avait 2 002 654 véhicules rechargeables en circulation en France fin novembre 2024 dont 1 304 260 véhicules 100% électriques. Les 698 394 véhicules supplémentaires sont des véhicules hybrides rechargeables. Dans les statistiques disponibles, sont définis comme véhicules non seulement les véhicules particuliers mais également les véhicules utilitaires. Les véhicules utilitaires ne sont pas destinés au transport de passagers mais plutôt au transport de marchandises. Toutefois, il restent des véhicules légers, ne dépassant pas les 3,5 tonnes. Les véhicules particuliers quant à eux sont des voitures destinées au transport de passagers comportant au maximum 8 places assises en plus de la place conducteur.

<iframe title = "Figure stock voitures electriques" src="../../../docs/france/figures/bev_stock.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

Le développement de la flotte de véhicules rechargeables peut également être suivi à partir du nombre de nouvelles immatriculations mensuelles. Les données du baromètre des immatriculations permet de distinguer entre les voitures particulières 100% électriques et les véhicules utilitaires 100% électriques. Au mois de novembre 2024, les immatriculations de nouveaux véhicules particuliers 100% électriques ont représenté 17.4% des parts de marché avec 23 255 immatriculations. Pour les véhicules particuliers 100% électriques, la Renault Renault 5 a représenté le plus grand nombre d'immatriculations (3316 nouvelles immatriculations), suivie du modèle Y de Tesla (3175 immatriculations) et de la Citroen E-C3 (1239 immatriculations).

<iframe title = "Figure nouvelles immatriculations" src="../../../docs/france/figures/bev_adm.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

### Bornes de recharge

D'après le [Baromètre national des infrastructures de recharge ouvertes au public](https://www.avere-france.org/publication/barometre-152-887-points-de-recharge-ouverts-au-public-fin-novembre-2024/), développé par le ministère de la Transition écologique avec l'Areve-France à partir de données de GIREVE, il y a en France 152 887 points de charge ouverts au public au 30 novembre 2024, dont 26 326 en Ile-de-France. 48% des points de charge ont une puissance comprise entre 7,4 et 22 kW et 33% inférieure à 7,4 kW. Les points de charge d'une puissance supérieure ou égale à 22 kW représentent 19% du total des points de charge ouverts au public. 

Dans la [Programmation Pluriannuelle de l'Énergie](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000041814432/), il est fait mention d'un objectif de 100 000 points de charge ouverts au public en 2023 et de 7 millions de points de charge publics et privés d'ici 2030, sans préciser la répartion entre points de charge publics et privés pour ce dernier. Le déploiement des points de charge sur les 12 derniers mois a fortement accéléré, ce qui a permis d'atteindre l'objectif des 100 000 points de charge publics en France à la fin du mois de mai 2023. Le gouvernement a [annoncé](https://www.gouvernement.fr/upload/media/content/0001/07/f74b604a7c975bf6f83c6d28656df3a96ee485b1.pdf) le 27 octobre 2023 un nouvel objectif de 400 000 points de recharge d'ici 2030, dont 50 000 points de charge rapides. Par ailleurs, Enedis, le gestionnaire de réseau de distribution français, [estime](https://data.enedis.fr/pages/points-de-charge/) à 2 213 322 le nombre total de points de charge (particuliers, accessibles au public et entreprises) en France à la fin du troisième trimestre 2024.

<iframe title = "Figure infrastructure de recharge" src="../../../docs/france/figures/cs.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>

À partir des données sur les bornes de recharge accessibles au public et le stock de véhicules rechargeables, le nombre de véhicules rechargeables - au total, ou seulement 100% électriques ou seulement hybrides - par point de recharge accessible au public peut être calculé. En juillet 2024, il y avait environ 8 véhicules 100% électriques par point de recharge accessible au public en France. Ce nombre peut cependant masquer de potentielles disparités géographiques, la diffusion des points de recharge accessibles au public dans un département n'étant pas nécessairement parfaitement alignée sur le stock de véhicules rechargeables présents dans ce même département.

<iframe title = "Figure bev par points de recharge" src="../../../docs/france/figures/bev_per_cp.html" width="100%" height="600px" frameBorder="0" loading = "lazy"></iframe>