---
hide:
#- navigation
- toc
---

# Contributions (CRediT style)

* [Alexander Roth](https://www.diw.de/cv/en/aroth): Conceptualization, Methodology, Software, Investigation, Writing - Review & Editing, Visualization
* [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill): Conceptualization, Methodology, Investigation, Data Curation, Writing - Original draft, Funding Acquisition
  
<br />  
  
* Nicolas Aichner: Investigation, Data Curation, Visualization
* [Fernanda Ballesteros](https://www.diw.de/cv/en/fballesteros): Investigation, Data Curation, Visualization
* Jan Czimmeck: Investigation, Data Curation, Visualization
* Lars Felder: Investigation, Data Curation, Visualization
* [Adeline Guéret](https://www.diw.de/cv/en/agueret): Investigation, Data Curation, Visualization, Writing - Original draft (French section), Writing - Review & Editing
* [Felix Schmidt](https://www.diw.de/cv/en/fschmidt): Investigation, Data Curation, Visualization
* [Kelvin Say](https://findanexpert.unimelb.edu.au/profile/860565-kelvin-say): Investigation, Data Curation, Visualization, Writing - Original draft (entire Australian section)