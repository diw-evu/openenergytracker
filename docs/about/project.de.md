---
hide:
#- navigation
- toc
---

# Über uns

## Open Energy Tracker

<img src="../../../docs/assets/BMBF_gefördert_vom_deutsch.jpg" alt="Gefördert vom BMBF" style="float: right" width="20%" />

Der Open Energy Tracker wurde von [Adeline Guéret](https://www.diw.de/cv/de/agueret), [Alexander Roth](https://www.diw.de/cv/de/aroth), [Felix Schmidt](https://www.diw.de/cv/de/fschmidt) und [Wolf-Peter Schill](https://www.diw.de/cv/de/wschill) vom Forschungsbereich [Transformation der Energiewirtschaft](https://twitter.com/transenerecon) der Abteilung [Energie, Verkehr, Umwelt](https://www.diw.de/de/diw_01.c.604205.de/abteilung_energie__verkehr__umwelt.html) am [DIW Berlin](https://www.diw.de/de) entwickelt. Insbesondere die Teile des Trackers, über die wir als "Ampel-Monitor Energiewende" berichten, wurden im Kontext des vom BMBF geförderten Kopernikus-Projekts [Ariadne](https://ariadneprojekt.de/) erstellt.

## Zitiervorschlag

Sie können den Open Energy Tracker gerne zitieren:

!!! quote "Zitiervorschlag"

    Alexander Roth und Wolf-Peter Schill (2022): "Open Energy Tracker: Eine offene Datenplattform für das Monitoring von energiepolitischen Zielen". [https://openenergytracker.org/](https://openenergytracker.org/)

## Kontakt

* Allgemeine Anfragen & Deutschland: [Wolf-Peter Schill](https://www.diw.de/cv/de/wschill), [Alexander Roth](https://www.diw.de/cv/de/aroth)
* Frankreich: [Adeline Guéret](https://www.diw.de/cv/de/agueret)
* Australien: [Kelvin Say](https://findanexpert.unimelb.edu.au/profile/860565-kelvin-say)
* Technische Umsetzung: [Alexander Roth](https://www.diw.de/cv/de/aroth)
