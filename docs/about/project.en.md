---
hide:
#- navigation
- toc
---

# About us

## Open Energy Tracker

<img src="../../../docs/assets/BMBF_gefoerdert_2017_en.jpg" alt="Sponsored by the BMBF" style="float: right" width="20%" />

The Open Energy Tracker was created by [Adeline Guéret](https://www.diw.de/cv/en/agueret), [Alexander Roth](https://www.diw.de/cv/en/aroth), [Felix Schmidt](https://www.diw.de/cv/en/fschmidt) and [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), members of the research area [Transformation of the Energy Economy](https://twitter.com/transenerecon) in the Department [Energy, Transport, Environment](https://www.diw.de/en/diw_01.c.604205.en/energy__transportation__environment_department.html) at [DIW Berlin](https://www.diw.de/en). Especially those parts that are also published in the "Ampel-Monitor Energiewende" have been developed in the context of the BMBF-funded Copernicus Project [Ariadne](https://ariadneprojekt.de/). 

## Suggested citation

The Open Energy Tracker can be citet as:

!!! quote "Suggested citation"

    Alexander Roth and Wolf-Peter Schill (2022): "Open Energy Tracker: An open data platform to monitor energy policy targets". [https://openenergytracker.org/en/](https://openenergytracker.org/en/)

## Contact

* General inquiries & Germany: [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), [Alexander Roth](https://www.diw.de/cv/de/aroth)
* France: [Adeline Guéret](https://www.diw.de/cv/en/agueret)
* Australia: [Kelvin Say](https://findanexpert.unimelb.edu.au/profile/860565-kelvin-say)
* Technical implementation: [Alexander Roth](https://www.diw.de/cv/en/aroth)
