---
hide:
#- navigation
- toc
---

# Qui sommes-nous ?

## Open Energy Tracker

<img src="../../../docs/assets/BMBF_gefoerdert_2017_en.jpg" alt="Sponsored by the BMBF" style="float: right" width="20%" />

L'Open Energy Tracker a été créé par [Adeline Guéret](https://www.diw.de/cv/en/agueret), [Alexander Roth](https://www.diw.de/cv/en/aroth), [Felix Schmidt](https://www.diw.de/cv/en/fschmidt) et [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), membres du groupe de recherche [Transformation de l'économie de l'énergie](https://twitter.com/transenerecon) au sein du département [Énergie, Transport, Environnement](https://www.diw.de/en/diw_01.c.604205.en/energy__transportation__environment_department.html) du [DIW Berlin](https://www.diw.de/en). Les indicateurs et analyses qui sont aussi publiés sur l'espace du "Ampel-Monitor Energiewende" s'inscrivent dans le cadre du projet Copernicus [Ariadne](https://ariadneprojekt.de/) financé par le ministère fédéral allemand de l'Enseignement et de la Recherche (BMBF).

## Citation suggérée

L'Open Energy Tracker peut être cité en tant que tel :

!!! quote "Citation suggérée"

    Alexander Roth et Wolf-Peter Schill (2023): "Open Energy Tracker: Une plateforme de données ouvertes pour suivre les objectifs de la politique énergétique". [https://openenergytracker.org/fr/](https://openenergytracker.org/fr/)

## Contact

* Demandes générales & Allemagne: [Wolf-Peter Schill](https://www.diw.de/cv/en/wschill), [Alexander Roth](https://www.diw.de/cv/de/aroth)
* France: [Adeline Guéret](https://www.diw.de/cv/en/agueret)
* Australie: [Kelvin Say](https://findanexpert.unimelb.edu.au/profile/860565-kelvin-say)
* Réalisation technique: [Alexander Roth](https://www.diw.de/cv/en/aroth)
