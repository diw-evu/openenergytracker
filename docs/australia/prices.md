---
hide:
#  - navigation
#  - toc
---

# Energy prices

=== "Authors"

    [Kelvin Say](../../docs/about/authors.md)
  
=== "Suggested citation"
    
    ``` Kelvin Say (2024) - "Energy prices - Australia". Published online at OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/en/docs/australia/prices/' [Online Resource] ```

## Household electricity prices

Household electricity is typically sold through competitive retail markets. Retailers in turn pay for electricity generation in the wholesale electricity market, utilisation of the transmission and distribution network, and environmental policies. As a result, retailer revenue can be estimated as the difference (i.e. residual) between household electricity bills and the  costs of supplying this energy.

These retail electricity prices are the weighted average across each state and territory, and are prepared by the [Australian Energy Market Commission (AEMC)](https://www.aemc.gov.au/energy-system/retail). The data is monitored over the Australian financial year (e.g. 2021-22), which begins 1 July and ends 30 June of the following year.

Generally, network utilisation is the highest contributor to retail electricity prices, however present [volatility](https://www.aemc.gov.au/news-centre/media-releases/aemc-residential-electricity-price-trends-report-be-published-mid-2023) in the cost of gas and coal is expected to increase the relative contribution of wholesale prices in 2021-22. 

Environmental policies consist of payments for large-scale renewable energy target (LRET) and small-scale renewable energy scheme (SRES) certificates, jurisdictional and energy efficiency schemes.

Household electricity prices in the following figure are <em>nominal</em> and do not consider consumer price inflation. All prices are presented as cents per kilowatt-hour (kWh) in Australian dollars (AUD). 

<iframe title = "Figure household electricity prices (nominal)" src="../../../docs/australia/figures/hh_elec_tariff.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [AEMC](https://www.aemc.gov.au/market-reviews-advice/residential-electricity-price-trends-2021); [ABS](https://www.abs.gov.au/statistics/economy/price-indexes-and-inflation/consumer-price-index-australia/latest-release)

Under <em>real</em> prices, that take into consideration the [consumer price index (CPI)](https://www.abs.gov.au/statistics/economy/price-indexes-and-inflation/consumer-price-index-australia/latest-release), there has been a general decrease in the <em>real</em> cost of energy until 2021. With the global energy supply shocks in 2021-2022, anecdotally the residential electricity prices have subsequently increased. However, the electricity price reporting method is currently being [revised by the AEMC](https://www.aemc.gov.au/news-centre/media-releases/update-residential-electricity-prices-report) is due to be released in late 2024. 