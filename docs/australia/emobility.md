---
hide:
#  - navigation
#  - toc
---

# Electric Mobility

=== "Authors"

    [Kelvin Say](../../docs/about/authors.md)
  
=== "Suggested citation"
    
    ```Kelvin Say (2024) - "Electric mobility - Australia". Published online at OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/en/docs/australia/emobility/' [Online Resource]```

## Road transport

### Battery electric passenger cars

The federal government has recently released its first [National Electric Vehicle Strategy](https://www.dcceew.gov.au/energy/transport/national-electric-vehicle-strategy) with an aim for 3.8 million electric vehicles by 2030 in their [Powering Australia Plan](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf). However, The <em>Step Change</em> scenario from [AEMO](https://aemo.com.au/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp) considers a less aggressive trajectory.

The year 2023 saw a surge in battery electric vehicle sales with an increase of 161% over the previous year, as reported by the [Federal Chamber for Automotive Industries (FCAI)](https://data.aaa.asn.au/ev-index/). While there is an estimated 152,900 battery electric vehicles and 28,700 plug-in hybrids currently registered on Australian roads, a further 3.6 million electric vehicles (or 516,900/year) are required over the next 7 years to reach the 2030 federal government target.

<iframe title = "Electric car stock" src="../../../docs/australia/figures/bev.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [EV Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2022/10/State-of-EVs-October-2022.pdf); [FCAI](https://data.aaa.asn.au/ev-index/); [Reputex](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/iasr/detailed-electric-vehicle-databook.xlsx)

To better indicate how the stock of new vehicles is changing in Australia, the following figure shows the shares of purely battery electric and plug-in hybrids in new vehicle registrations per year based on [FCAI](https://data.aaa.asn.au/ev-index/) and [Electric Vehicle Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf) data. Federal government modelling in the [Powering Australia Plan](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf) estimates that electric vehicles could make 89% of new vehicle registrations by 2030.

<iframe title = "Figure new electric car registrations" src="../../../docs/australia/figures/bev_adm.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [EV Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2022/10/State-of-EVs-October-2022.pdf); [FCAI](https://data.aaa.asn.au/ev-index/); [Reputex](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf)

### Charging infrastructure

Presently, there are currently no explicit government targets for the number of public charging stations. However, federal government modelling in the [Powering Australia Plan](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf) estimates 1,800 public fast and ultra-fast charging stations will be needed by 2030. The [Electric Vehicle Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf) reports that there are 967 fast and ultra-fast chargers as of June 2023, which is an increase of 57% over the last 12 months. As the number of chargers are not officially tallied, the number of charging locations reported by the [EV Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf) and [Plugshare](https://www.plugshare.com/) are used to indicate the current status of EV charging infrastructure.

<iframe title = "Figure charging infrastructure" src="../../../docs/australia/figures/cs.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [EV Council](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf)