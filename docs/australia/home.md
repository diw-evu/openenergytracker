---
description: The Open Energy Tracker visualises governmental goals and progress for selected areas of the energy transition.

hide:
#  - navigation
#  - toc
---

# Australia

!!! Success "What's new"
    
	* January 2024 update
	* Added 'Cumulative' and 'per Year' switch to installed capacities
	* Added 'Nominal' and 'Real' switch for residential electricity prices 

The objectives of the energy transition in Australia are integrated with the federal government's commitment to climate neutrality in 2050 via an interim greenhouse gas (GHG) emissions reduction target of [43% by 2030](https://unfccc.int/sites/default/files/NDC/2022-06/Australias%20NDC%20June%202022%20Update%20%283%29.pdf) (based on 2005 levels). 

This is supported by the following national-level policies: 

* [Renewable Energy Target](https://www.cleanenergyregulator.gov.au/RET/About-the-Renewable-Energy-Target) (33 TWh of renewable energy generation required every year from 2020 until 2030)
* [Capacity Investment Scheme](https://www.dcceew.gov.au/energy/renewable/capacity-investment-scheme) (auctions to underwrite 23 GW of variable renewable and 9 GW of dispatchable capacity by 2030)
* [Safeguard Mechanism](https://www.cleanenergyregulator.gov.au/NGER/The-Safeguard-Mechanism) (an emissions trading scheme covering the electricity sector and industrial facilities emitting more than 100,000 tCO<sub>2</sub>e/year) 

In addition, various state governments have in place further GHG and renewable energy targets.

| State&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Relevant targets |
|-----------------|------------------|
| Victoria        | [GHG emissions reduction target](https://www.climatechange.vic.gov.au/victorian-government-action-on-climate-change) by 2025 of 28-33% (legislated), by 2030 of 45-50% (legislated), by 2035 of 75-80% (legislated), and by 2045 of net-zero (legislated) |
|                 | [Renewable energy target](https://www.energy.vic.gov.au/renewable-energy/victorian-renewable-energy-and-storage-targets) by 2025 of 40% (legislated), by 2030 of 50% (legislated) / 65% (pledged), and by 2035 of 95% (pledged) |
|                 | [Energy storage target](https://www.energy.vic.gov.au/renewable-energy/victorian-renewable-energy-and-storage-targets) by 2030 of 2.6 GW (legislated), and by 2035 of 6.3 GW (legislated) |
| New South Wales | [GHG emissions reduction target](https://www.energy.nsw.gov.au/nsw-plans-and-progress/government-strategies-and-frameworks/reaching-net-zero-emissions) by 2030 of 50% (legislated), and by 2035 of 70% (legislated) |
|                 | [Electricity Infrastructure Roadmap](https://www.energy.nsw.gov.au/nsw-plans-and-progress/major-state-projects/electricity-infrastructure-roadmap) to support an additional 12 GW of renewable generation and 2 GW of long duration storage by 2030 |
| Queensland      | [GHG emissions reduction target](https://www.des.qld.gov.au/climateaction/theplan/qld-climate-action-plan) by 2030 of 30% (legislated), and by 2035 of 75% (legislated) |
|                 | [Renewable energy target](https://www.des.qld.gov.au/climateaction/theplan/qld-climate-action-plan) by 2030 of 50%  (legislated), by 2032 of 70% (legislated), and by 2035 of 80% (legislated) |
| South Australia | [GHG emissions reduction target](https://www.environment.sa.gov.au/topics/climate-change/government-action-on-climate-change) by 2030 of 50% (legislated) |
|                 | [Renewable energy target](https://www.environment.sa.gov.au/topics/climate-change/government-action-on-climate-change) by 2030 of 100% (legislated) |
| Tasmania        | [GHG emissions reduction target](https://www.stategrowth.tas.gov.au/recfit/climate/reducing_our_emissions) by 2030 of net-zero (legislated) |
| Western Australia | [Government-only GHG emissions reduction target](https://www.wa.gov.au/service/environment/business-and-community-assistance/government-emissions-interim-target) by 2030 of 80% (committed) |

With respect to non-electricity energy sectors, industrial facilities emitting more than 100,000 tCO<sub>2</sub>e/year are subject to an emissions reduction policy (i.e. [Safeguard Mechanism](https://www.cleanenergyregulator.gov.au/NGER/The-Safeguard-Mechanism)), while remaining sectors (e.g. transport, heating, etc.) do not have any further sector-specific transition pathways. However, to reach economy-wide emissions reduction targets significant changes to Australia's energy infrastructure and processes are still required.

This information is aimed at monitoring the progress of Australia's energy transition and providing the background to production and consumption of energy in Australia.
