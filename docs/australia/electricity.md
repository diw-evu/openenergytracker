---
hide:
#  - navigation
#  - toc
---
# Renewable electricity

=== "Authors"

    [Kelvin Say](../../docs/about/authors.md)

=== "Suggested citation"

    ``Kelvin Say (2024) - "Renewable electricity - Australia". Published online at OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/en/docs/australia/electricity/' [Online Resource]``

Historically, the growth of renewable electricity in Australia was driven by the [Renewable Energy Target](https://www.cleanenergyregulator.gov.au/RET/About-the-Renewable-Energy-Target) which is a clean energy certificate scheme. This requires 33 TWh (or approximately 23.5%) of electricity generation annually from 2020 until 2030 to come from renewable energy sources. This requirement was met in 2020. To facilitate this market, large-scale generation certificates (LGCs) and small-scale technology certificates (STCs) are issued for each MWh of renewable energy generation. Wholesale purchasers of electricity (mainly electricity retailers) are required to purchase a sufficient number of these certificates to meet their renewable energy obligations. These LGCs and STCs can also be purchased by individuals or organisations on a secondary market to offset their energy emissions. 

To continue growing beyond the Renewable Energy Target, state governments have introduced further renewable energy and storage capacity targets to continue increasing the share of renewable energy generation in the electricity mix. The federal govenment has since committed additional policy support towards these state based targets through the [Capacity Investment Scheme](https://www.dcceew.gov.au/energy/renewable/capacity-investment-scheme). This is an underwriting scheme designed to bring forward 23 GW of wind and solar PV and 9 GW of dispatchable capacity.

In addition, companies that generate electricity remain liable under the [Safeguard Mechanism](https://www.cleanenergyregulator.gov.au/NGER/The-Safeguard-Mechanism) that requires their GHG emissions to fall inline with Australia's climate targets.

The integration of climate into the energy sector formally occurred in 2022, where the Energy Ministers of Australia agreed to [introduce an emissions reduction objective](https://www.energy.gov.au/government-priorities/energy-ministers/priorities/national-energy-transformation-partnership/incorporating-emissions-reduction-objective-national-energy-objectives) into the National Energy Objectives (NEO). This will integrate emissions reduction and energy policy into the national energy laws and Australia's energy market governance bodies. The Australian Energy Market Commission are in the process of [consultation](https://www.aemc.gov.au/market-reviews-advice/consultation-aemc-guide-applying-emissions-component-national-energy-objectives) before providing guidelines on how emissions will be applied to the [National Energy Objectives](https://www.aemc.gov.au/regulation/neo).

## Shares in the power sector

Generation by renewable energy technologies currently contributes 38% to Australia's electricity demand. Looking forward, the [2022 Integrated System Plan (ISP)](https://aemo.com.au/en/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp) by the Australian Energy Market Operator (AEMO) uses a [least-cost (and emissions constrained) model](https://aemo.com.au/en/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp/isp-methodology) to evaluate transition scenarios of the electricity system until 2050. The *Step Change* scenario involves a rapid initial transition to renewable energy that reaches 83% by 2030. It is also regarded as the most likely future scenario. [Federal government modelling](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf) to meet its interim 43% emissions reduction target by 2030 establishes a similar outcome with the share of renewable energy in the power sector needing to reach 82% by 2030.

<iframe title = "Figure share of renewables in electricity" src="../../../docs/australia/figures/resshares.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [OpenNEM](https://opennem.org.au/energy/au/?range=all&interval=1y&view=discrete-time)
## Installed Solar PV capacity

Solar PV is an major source of renewable energy generation in Australia with over [26 GW of solar PV capacity](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf) installed as of 2022.

Notably, a signficant proportion of installed solar PV capacity has come from small-scale generators, namely rooftop PV systems. As of November 2023, over [3.6 million or 34% of households](https://pv-map.apvi.org.au/historical) have installed a rooftop PV system.

The *Step Change* scenario in the [2022 Integrated System Plan (ISP)](https://aemo.com.au/en/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp) provides estimates of future solar PV capacity across three categories (utility, medium- and small-scale). Utility PV systems have a rated capacity above 5 MW. Medium-scale PV systems have a capacity between 100 kW and 5 MW. Small-scale PV systems have a rated capacity under 100 kW (typically rooftop PV on residential households). Both small- and medium-scale PV systems are considered as distributed PV as they are predominantly situated behind-the-meter on end-user premises.

<iframe title = "Figure installed pv solar capacity" src="../../../docs/australia/figures/pv.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Clean Energy Council](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/2022-documents/generation-outlook.zip?la=en)

At the end of 2022, 6.5 GW of utility PV capacity is in operation. The *Step Change* scenario considers utility PV capacity to remain generally below that of rooftop PV capacity, however expectations remain for significant growth after 2035 which allow it to reach approximately 70 GW by 2050.

### Utility PV

<iframe title = "Figure installed pv solar capacity" src="../../../docs/australia/figures/pv_util.html" width="100%" height= "350px" frameBorder="0" loading = "lazy"></iframe>
Source: [Clean Energy Council](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/2022-documents/generation-outlook.zip?la=en)

At the end of 2022, 20 GW of distributed PV capacity is in operation. As distributed energy resources (DER) situated behind-the-meter at customer premises, their continued growth and significant generation is driving governing institutions (i.e. [AEMO](https://aemo.com.au/en/initiatives/major-programs/nem-distributed-energy-resources-der-program/about-the-der-program) and [Energy Security Board](https://esb-post2025-market-design.aemc.gov.au/integration-of-distributed-energy-resources-der-and-flexible-demand)) to explore a transition towards a decentralised and two-way energy system. This would allow distributed PV and other behind-the-meter energy assets to improve their aggregate operational and market participation. The *Step Change* scenario estimates a generally linear growth trajectory to approximately 68 GW by 2050.

### Distributed PV

<iframe title = "Figure installed pv solar capacity" src="../../../docs/australia/figures/pv_distr.html" width="100%" height= "350px" frameBorder="0" loading = "lazy"></iframe>
Source: [Clean Energy Council](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/2022-documents/generation-outlook.zip?la=en)

## Installed wind energy capacity

While solar PV (combined) is the largest source of renewable energy in the power sector, wind turbines still generate more energy at the utility-scale. Australia has significant wind resources, which has led to over 10.5 GW of installed capacity as of 2022. The *Step Change* scenario expects significant growth in onshore wind capacity, which is more than double the current growth rate.

<iframe title = "Figure onshore wind capacity" src="../../../docs/australia/figures/wind_onshore.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Clean Energy Council](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/2022-documents/generation-outlook.zip?la=en)
