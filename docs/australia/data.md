---
hide:
#  - navigation
#  - toc
---

# Data

## Projections

Government modelling: 

* [Economic and model analysis of Powering Australia (Reputex)](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf)

Independent market operator modelling: 

* [Integrated System Plan 2022 (AEMO)](https://aemo.com.au/en/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp)

## Renewable energy

### Capacity expansion

Solar PV: 

* [Clean Energy Australia Report 2023 (Clean Energy Council)](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf)

Small-scale solar PV:

* [Clean Energy Regulator (Commonwealth of Australia)](https://www.cleanenergyregulator.gov.au/RET/Forms-and-resources/Postcode-data-for-small-scale-installations)
* [Australian PV Institute](http://pv-map.apvi.org.au/analyses)

Small-scale battery energy storage systems:

* [Clean Energy Regulator (Commonwealth of Australia)](https://www.cleanenergyregulator.gov.au/RET/Forms-and-resources/Postcode-data-for-small-scale-installations)

Onshore wind:

* [Clean Energy Australia Report 2023 (Clean Energy Council)](https://assets.cleanenergycouncil.org.au/documents/Clean-Energy-Australia-Report-2023.pdf) 

### Shares in the electricity sector

Share of gross and net electricity consumption:

* [OpenNEM](https://opennem.org.au/energy/au/?range=all&interval=1y)

## Electric mobility

### Road transport

Battery electric passenger cars: 

* [2022 Australian Electric Vehicle Industry Recap (Electric Vehicle Council)](https://electricvehiclecouncil.com.au/reports/2022-australian-electric-vehicle-industry-recap/)
* [State of Electric Vehicles July 2023 (Electric Vehicle Council)](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf)
* [Electric Vehicle Index (Federal Chamber of Automotive Industries)](https://data.aaa.asn.au/ev-index/)

Charging infrastructure: 

* [State of Electric Vehicles July 2023 (Electric Vehicle Council)](https://electricvehiclecouncil.com.au/wp-content/uploads/2023/07/State-of-EVs_July-2023_.pdf)
* [Economic and model analysis of Powering Australia (Reputex)](https://www.reputex.com/wp-content/uploads/2021/12/REPUTEX_The-economic-impact-of-the-ALPs-Powering-Australia-Plan_Summary-Report-1221.pdf)

## Energy consumption

Primary energy consumption: 

* [Australian Energy Update 2023 (Commonwealth of Australia)](https://www.energy.gov.au/publications/australian-energy-update-2022)

## Energy prices

Household electricity prices: 

* Residential Electricity Price Trends (AEMC) [2021](https://www.aemc.gov.au/market-reviews-advice/residential-electricity-price-trends-2021), [2020](https://www.aemc.gov.au/market-reviews-advice/residential-electricity-price-trends-2020), and [earlier](https://www.aemc.gov.au/our-work/market-reviews-and-advice).

## Greenhouse gas emissions

Sectoral greenhouse gas emissions: 

* [Paris Agreement inventory (Commonwealth of Australia)](https://greenhouseaccounts.climatechange.gov.au/)

CO<sub>2</sub> emissions from electricity generation: 

* [Clean Energy Regulator (Commonwealth of Australia)](https://www.cleanenergyregulator.gov.au/NGER/National%20greenhouse%20and%20energy%20reporting%20data/electricity-sector-emissions-and-generation-data/electricity-sector-emissions-and-generation-data-2021%E2%80%9322)