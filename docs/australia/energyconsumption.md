---
hide:
#  - navigation
#  - toc
---

# Energy Consumption

=== "Authors"

    [Kelvin Say](../../docs/about/authors.md)
  
=== "Suggested citation"
    
    ``` Kelvin Say (2024) - "Energy consumption - Australia". Published online at OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/en/docs/australia/energyconsumption/' [Online Resource] ```

## Fossil energy primary consumption

The federal government has committed itself to the goal of climate neutrality in 2050. However, there is no commitment to only using non-fossil fuel energy resources beyond 2050. Nonetheless, fossil fuel use in Australia's primary energy consumption must be reduced in order to meet government emissions reduction targets. This trajectory is represented by the 'linear trend to zero by 2050' line. The primary energy consumption equates to the <em>primary energy supply</em> in the [Australian Energy Flows](https://www.energy.gov.au/publications/australian-energy-update-2023) which excludes Australia's energy exports.

<iframe title = "Fossil primary energy consumption" src="../../../docs/australia/figures/fig_pec.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Commonwealth of Australia](https://www.energy.gov.au/publications/australian-energy-update-2023)

Australia currently exports more than twice the amount of energy it consumes. Australia is a significant exporter of (thermal and metallurgical) coal and liquid natural gas, but remains a net-importer of oil.

<iframe title = "Fossil primary energy trade" src="../../../docs/australia/figures/fig_pe_trade.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Commonwealth of Australia](https://www.energy.gov.au/publications/australian-energy-update-2023)
