---
hide:
#  - navigation
#  - toc
---

# Greenhouse gas emissions

=== "Authors"

    [Kelvin Say](../../docs/about/authors.md)
  
=== "Suggested citation"
    
    ``` Kelvin Say (2024) - "Greenhouse gas emissions - Australia". Published online at OpenEnergyTracker.org. Retrieved from: 'https://openenergytracker.org/en/docs/australia/emissions/' [Online Resource] ```

## Sectoral greenhouse gas emissions

The federal government has committed to climate neutrality in 2050 via an interim greenhouse gas (GHG) emissions reduction target of [43% by 2030](https://unfccc.int/sites/default/files/NDC/2022-06/Australias%20NDC%20June%202022%20Update%20%283%29.pdf) (based on 2005 levels). In addition, state governments have introduced further GHG reduction targets. Current plans rely heavily on the electricity sector to achieve significant GHG emission reductions by 2030. 

Another factor to consider is [land use, land use change and forestry (LULUCF)](https://unfccc.int/topics/land-use/workstreams/land-use-land-use-change-and-forestry-lulucf/reporting-and-accounting-of-lulucf-activities-under-the-kyoto-protocol) which accounts for the net impact of human activities on the effectiveness of terrestrial land sinks to absorb greenhouse gases. If we only consider Australia's sectoral GHG emissions without these terrestrial land sinks (i.e. without LULUCF) then much a faster emissions reduction trajectory is needed to meet the interim target. 
<!---Net changes to greenhouse gas uptake by terrestrial land sinks. --->

At its current trajectory, Australia needs to further accelerate its rate of emissions reduction in order to meet its interim emissions reduction target of 43% by 2030.

<iframe title = "Sectoral greenhouse gas emissions" src="../../../docs/australia/figures/emissions_sect.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Commonwealth of Australia](https://greenhouseaccounts.climatechange.gov.au/)

## CO<sub>2</sub> emissions of electricity generation

Most of Australia's reduction in greenhouse gas emissions to date have relied on the increasing share of renewable generation in the power sector. As more fossil fuel generators exit the energy market, the emissions intensity of electricity generation should continue to decrease. The emissions intensity is the average quantity of greenhouse gas equivalent (CO<sub>2</sub>e) emitted per kilowatt-hour (kWh). Given the [National Electricity Market](https://aemo.com.au/energy-systems/electricity/national-electricity-market-nem/about-the-national-electricity-market-nem) manages 90% of Australia's electricity generation, its [Step Change scenario](https://aemo.com.au/en/energy-systems/major-publications/integrated-system-plan-isp/2022-integrated-system-plan-isp) is extrapolated to provide a national perspective on electricity sector emissions. 

<iframe title = "CO2 emissions of electricity generation" src="../../../docs/australia/figures/emissions_electr.html" width="100%" height= "500px" frameBorder="0" loading = "lazy"></iframe>
Source: [Commonwealth of Australia](https://www.cleanenergyregulator.gov.au/NGER/National%20greenhouse%20and%20energy%20reporting%20data/electricity-sector-emissions-and-generation-data/electricity-sector-emissions-and-generation-data-2021%E2%80%9322); [AEMO](https://aemo.com.au/-/media/files/major-publications/isp/2022/2022-documents/generation-outlook.zip?la=en)

<!--Include a trend line that incorporates state government targets, and weighted to the energy generation in each state--> 