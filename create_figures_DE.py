#%% Import packages

import os
import numpy as np
import pandas as pd
import copy
from pathlib import Path
from datetime import date
from datetime import datetime

from sklearn.linear_model import LinearRegression
import plotly as plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px
import statsmodels.api as sm

from PIL import Image

from urllib.request import urlopen
import json

import requests
import shutil
import dateutil.parser

# import custom packages

import scripts.colors as colors
import scripts.util as oet

import re
import time

#%% Create directory

Path("docs/germany/figures").mkdir(parents=True, exist_ok=True)

####################################################################################################
# Define base dictionaries - to be re-used
####################################################################################################

base_default = {
    "title"         : "Installierte Leistung Photovoltaik",
    "data"          : None,
    "color_dict"    : colors.standard,
    "x_axis_title"  : None,
    "x_axis_range"  : ["2017-01", "2031-03"],
    "y_axis_title"  : "Bestand [GW]",
    "y_axis_range"  : [0,250],
    "buttons"  : {
        "y_max_2030"  : 250,
        "y_max_2045"  : 500,
        "button2030"  : "bis 2030",
        "button2045"  : "bis 2045"
        },
    #"source_file"     : "",
    #"language"        : "de"
    }

dict_default = {
    "base"                     : base_default.copy(),
    "legislative_period"       : oet.legislative_period.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend"             : oet.trend.copy(),
    "figure_goal"              : oet.goal.copy(),
    "figure_goal_lin"          : oet.goal_lin.copy(),
    "figure_ar_lead"           : oet.ar_lead.copy(),
    "figure_ar_corr"           : oet.ar_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_needed"  : oet.additions_needed.copy(),
}

#%%

####################################################################################################
# EMobility Data
################################################################################################

#Just used for calculation of trends and goals

mobility_url = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/kba_bna/"

bev_data = oet.prepare_data(mobility_url + "bev_stock_monthly.csv","months")
bev_data = oet.calculate_trend(bev_data,"actual","12-months")

cs_data           = oet.prepare_data(mobility_url +  "cs_quarterly.csv","quarters")
cs_data['actual'] = cs_data['actual_normal'] + cs_data['actual_fast']
cs_data = oet.calculate_trend(cs_data,"actual","4-quarters")


####################################################################################################
# Renewable Energy
####################################################################################################

#%% PV ---------------------------------------------------------------------------------------------

# Prepare data

pv_data = oet.prepare_data("docs/germany/data/pv.csv","months")
pv_data = oet.calculate_trend(pv_data,"actual","12-months")

# Define PV dictionary 

base_pv = {
    "title"         : "Installierte Leistung Photovoltaik",
    "data"          : pv_data,
    "color_dict"    : colors.standard,
    "x_axis_title"  : None,
    "x_axis_range"  : ["2017-01", "2031-03"],
    "y_axis_title"  : "Bestand [GW]",
    "y_axis_range"  : [0,250],
    "y_axis2_range" : [0,3],
    "y_axis2_title" : "Zubau [GW]",
    "subplots" : {
        "rows"        : 2,
        "cols"        : 1,
        "row_heights" : [0.75,0.25]},
    "buttons"  : {
        "y_max_2030"  : 250,
        "y_max_2045"  : 500,
        "button2030"  : "bis 2030",
        "button2045"  : "bis 2045"
        },
    "source_file"     : "docs/germany/data/pv.csv",
    "language"        : "de"
    }

base_pv_de = copy.deepcopy(base_pv)

dict_pv_de = {
    "base"                     : base_pv_de,
    "legislative_period"       : oet.legislative_period.copy(),
    "figure_actual"            : oet.actual.copy(),
    "figure_trend"             : oet.trend.copy(),
    "figure_goal"              : oet.goal.copy(),
    "figure_goal_lin"          : oet.goal_lin.copy(),
    "figure_ar_lead"           : oet.ar_lead.copy(),
    "figure_ar_corr"           : oet.ar_corr.copy(),
    "figure_additions"         : oet.additions.copy(),
    "figure_additions_needed"  : oet.additions_needed.copy(),
}

dict_pv_en = copy.deepcopy(dict_pv_de)

# English
dict_pv_en["base"]["title"]                = "Installed PV capacity"
dict_pv_en["base"]["y_axis_title"]         = "Stock [GW]"
dict_pv_en["base"]["y_axis2_title"]        = "Additions [GW]"
dict_pv_en["base"]["buttons"]["button2030"] = "until 2030"
dict_pv_en["base"]["buttons"]["button2045"] = "until 2045"
dict_pv_en["base"]["language"]              = "en"

dict_pv_en["legislative_period"]["text"]    = "Legislative period"
dict_pv_en["figure_actual"]["name"]         = "Installed"
dict_pv_en["figure_trend"]["name"]          = "12-month-trend"
dict_pv_en["figure_goal"]["title_group"]    = "<i>Government goals</i>"
dict_pv_en["figure_goal"]["name"]           = "Targets of the coalition"
dict_pv_en["figure_goal_lin"]["name"]       = "Linearized progression"
dict_pv_en["figure_ar_lead"]["title_group"] = "<i>Ariadne scenarios</i>"
dict_pv_en["figure_ar_lead"]["name"]        = "Lead model"
dict_pv_en["figure_ar_corr"]["name"]        = "Scenario corridor"

dict_pv_fr = copy.deepcopy(dict_pv_de)

dict_pv_fr["base"]["title"]                 = "Capacité photovoltaïque installée"
dict_pv_fr["base"]["y_axis_title"]          = "Stock [GW]"
dict_pv_fr["base"]["y_axis2_title"]         = "Ajouts [GW]"
dict_pv_fr["base"]["buttons"]["button2030"] = "jusqu'en 2030"
dict_pv_fr["base"]["buttons"]["button2045"] = "jusqu'en 2045"
dict_pv_fr["base"]["language"]              = "fr"

dict_pv_fr["legislative_period"]["text"]    = "Période du mandat"
dict_pv_fr["figure_actual"]["name"]         = "Déjà installée"
dict_pv_fr["figure_trend"]["name"]          = "12 derniers mois"
dict_pv_fr["figure_goal"]["title_group"]    = "<i>Réglementation</i>"
dict_pv_fr["figure_goal"]["name"]           = "Objectifs de la coalition"
dict_pv_fr["figure_goal_lin"]["name"]       = "Progression linéarisée"
dict_pv_fr["figure_ar_lead"]["title_group"] = "<i>Scénarios Ariadne</i>"
dict_pv_fr["figure_ar_lead"]["name"]        = "Modèle de référence"
dict_pv_fr["figure_ar_corr"]["name"]        = "Intervalle de scénarios"

# Create figures

fig_pv_de  = oet.create_fig(dict_pv_de)
fig_pv_en  = oet.create_fig(dict_pv_en)
fig_pv_fr  = oet.create_fig(dict_pv_fr)

# Write figures

fig_pv_de.write_html("docs/germany/figures/pv.de.html", include_plotlyjs="directory")
fig_pv_en.write_html("docs/germany/figures/pv.en.html", include_plotlyjs="directory")
fig_pv_fr.write_html("docs/germany/figures/pv.fr.html", include_plotlyjs="directory")

#%% Wind onshore -----------------------------------------------------------------------------------

windon_data = oet.prepare_data("docs/germany/data/wind_onshore.csv","months")
windon_data = oet.calculate_trend(windon_data,"actual","12-months")

# German

dict_windon_de = copy.deepcopy(dict_pv_de)

dict_windon_de["base"]["data"]                  = windon_data
dict_windon_de["base"]["source_file"]           = 'docs/germany/data/wind_onshore.csv'
dict_windon_de["base"]["title"]                 = "Installierte Leistung Windkraft an Land"
dict_windon_de["base"]["y_axis_range"]          = [0,130]
dict_windon_de["base"]["y_axis2_range"]         = [0,1]
dict_windon_de["base"]["buttons"]["y_max_2030"] = 130
dict_windon_de["base"]["buttons"]["y_max_2045"] = 180
dict_windon_de["base"]["source_file"]           = 'docs/germany/data/wind_onshore.csv'
dict_windon_de["base"]["language"]              = "de"

# English

dict_windon_en = copy.deepcopy(dict_pv_en)
dict_windon_en["base"]["data"]                  = windon_data
dict_windon_en["base"]["language"]              = "en"
dict_windon_en["base"]["title"]                 = "Installed capacity onshore wind power"
dict_windon_en["base"]["y_axis_range"]          = [0,130]
dict_windon_en["base"]["y_axis2_range"]         = [0,1]
dict_windon_en["base"]["buttons"]["y_max_2030"] = 130
dict_windon_en["base"]["buttons"]["y_max_2045"] = 180
dict_windon_en["base"]["language"]              = "en"

# French

dict_windon_fr = copy.deepcopy(dict_pv_fr)
dict_windon_fr["base"]["data"]                  = windon_data
dict_windon_fr["base"]["language"]              = "fr"
dict_windon_fr["base"]["title"]                 = "Capacité installée - éolien terrestre"
dict_windon_fr["base"]["y_axis_range"]          = [0,130]
dict_windon_fr["base"]["y_axis2_range"]         = [0,1]
dict_windon_fr["base"]["buttons"]["y_max_2030"] = 130
dict_windon_fr["base"]["buttons"]["y_max_2045"] = 180
dict_windon_fr["base"]["language"]              = "fr"

# Create figures

fig_windon_de  = oet.create_fig(dict_windon_de)
fig_windon_en  = oet.create_fig(dict_windon_en)
fig_windon_fr  = oet.create_fig(dict_windon_fr)

# Write figures

fig_windon_de.write_html("docs/germany/figures/wind_onshore.de.html", include_plotlyjs="directory")
fig_windon_en.write_html("docs/germany/figures/wind_onshore.en.html", include_plotlyjs="directory")
fig_windon_fr.write_html("docs/germany/figures/wind_onshore.fr.html", include_plotlyjs="directory")

#%% Wind offshore ----------------------------------------------------------------------------------

windoff_data = oet.prepare_data("docs/germany/data/wind_offshore.csv","months")
windoff_data = oet.calculate_trend(windoff_data,"actual","12-months")

# German

dict_windoff_de = copy.deepcopy(dict_windon_de)

dict_windoff_de["base"]["data"]                  = windoff_data
dict_windoff_de["base"]["source_file"]           = 'docs/germany/data/wind_offshore.csv'
dict_windoff_de["base"]["title"]                 = "Installierte Leistung Windkraft auf See"
dict_windoff_de["base"]["y_axis_range"]          = [0,35]
dict_windoff_de["base"]["y_axis2_range"]         = [0,0.75]
dict_windoff_de["base"]["buttons"]["y_max_2030"] = 35
dict_windoff_de["base"]["buttons"]["y_max_2045"] = 80
dict_windoff_de["base"]["source_file"]           = 'docs/germany/data/wind_offshore.csv'
dict_windoff_de["base"]["language"]              = "de"

# English

dict_windoff_en = copy.deepcopy(dict_windon_en)

dict_windoff_en["base"]["data"]                  = windoff_data
dict_windoff_en["base"]["language"]              = "en"
dict_windoff_en["base"]["title"]                 = "Installed capacity offshore wind power"
dict_windoff_en["base"]["y_axis_range"]          = [0,35]
dict_windoff_en["base"]["y_axis2_range"]         = [0,0.75]
dict_windoff_en["base"]["buttons"]["y_max_2030"] = 35
dict_windoff_en["base"]["buttons"]["y_max_2045"] = 80
dict_windoff_en["base"]["language"]              = "en"

# French

dict_windoff_fr = copy.deepcopy(dict_windon_fr)

dict_windoff_fr["base"]["data"]                  = windoff_data
dict_windoff_fr["base"]["language"]              = "fr"
dict_windoff_fr["base"]["title"]                 = "Capacité installée éolien en mer"
dict_windoff_fr["base"]["y_axis_range"]          = [0,35]
dict_windoff_fr["base"]["y_axis2_range"]         = [0,0.75]
dict_windoff_fr["base"]["buttons"]["y_max_2030"] = 35
dict_windoff_fr["base"]["buttons"]["y_max_2045"] = 80
dict_windoff_fr["base"]["language"]              = "fr"

# Create figures

fig_windoff_de  = oet.create_fig(dict_windoff_de)
fig_windoff_en  = oet.create_fig(dict_windoff_en)
fig_windoff_fr  = oet.create_fig(dict_windoff_fr)

# Write figures

fig_windoff_de.write_html("docs/germany/figures/wind_offshore.de.html", include_plotlyjs="directory")
fig_windoff_en.write_html("docs/germany/figures/wind_offshore.en.html", include_plotlyjs="directory")
fig_windoff_fr.write_html("docs/germany/figures/wind_offshore.fr.html", include_plotlyjs="directory")

#%% Wind onshore areas -----------------------------------------------------------------------------

# data

wind_areas_data          = pd.read_csv("docs/germany/data/wind_areas.csv", sep = ",")
wind_areas_data['date']  = pd.to_datetime(wind_areas_data['date'], format='%Y')

# German

base_wind_areas = copy.deepcopy(base_default)

dict_wind_areas_de = {
    "base"               : base_wind_areas,
    "legislative_period" : oet.legislative_period.copy(),
    "figure_actual"      : oet.actual.copy(),
    "figure_goal"        : oet.goal.copy(),
    "figure_goal_lin"    : oet.goal_lin.copy(),
}

dict_wind_areas_de["base"]["data"]          = wind_areas_data
dict_wind_areas_de["base"]["title"]         = "Anteil der für Windkraftanlagen ausgewiesenen Landesfläche"
dict_wind_areas_de["base"]["y_axis_title"]  = "%"
dict_wind_areas_de["base"]["y_axis_range"]  = [0,2.5]
dict_wind_areas_de["base"]["x_axis_range"]  = [2019,2035]
dict_wind_areas_de["base"]["x_axis_range"]  = [2019,2035]
dict_wind_areas_de["base"]["margin"]        = dict(l=0, r=0, t=30, b=0, pad=0)
dict_wind_areas_de["base"]["hovermode"]     = "x"

del dict_wind_areas_de["base"]["buttons"]

dict_wind_areas_de["figure_actual"]["name"]    = "Tatsächlich erreicht"
dict_wind_areas_de["figure_actual"]["hover"]   = "%{y:.2f}%"
dict_wind_areas_de["figure_actual"]["x"]       = "years"
dict_wind_areas_de["figure_goal"]["x"]         = "years"
dict_wind_areas_de["figure_goal"]["hover"]     = "%{y:.2f}%"
dict_wind_areas_de["figure_goal_lin"]["x"]     = "years"
dict_wind_areas_de["figure_goal_lin"]["hover"] = "%{y:.2f}%"

dict_wind_areas_de["legislative_period"]["x0"] = "2021"
dict_wind_areas_de["legislative_period"]["x1"] = "2025"

# English

dict_wind_areas_en = copy.deepcopy(dict_wind_areas_de)

dict_wind_areas_en["base"]["title"]              = "Share of the national area designated for wind power plants"
dict_wind_areas_en["figure_actual"]["name"]      = "Actually reached"
dict_wind_areas_en["figure_goal"]["name"]        = "Target of the coalition"
dict_wind_areas_en["figure_goal"]["title_group"]  = "<i>Government goals</i>"
dict_wind_areas_en["figure_goal_lin"]["name"]    = "Linearized progression"
dict_wind_areas_en["legislative_period"]["text"] = "Legislative period"

# French

dict_wind_areas_fr = copy.deepcopy(dict_wind_areas_de)
dict_wind_areas_fr["base"]["title"]              = "Part de la superficie nationale attribuée aux éoliennes"
dict_wind_areas_fr["figure_actual"]["name"]      = "Déjà attribuée"
dict_wind_areas_fr["figure_goal"]["name"]        = "Objectif de la coalition"
dict_wind_areas_fr["figure_goal"]["title_group"]  = "<i>Réglementation</i>"
dict_wind_areas_fr["figure_goal_lin"]["name"]    = "Progression linéarisée"
dict_wind_areas_fr["legislative_period"]["text"] = "Période du mandat"

# Create figures

fig_wind_areas_de  = oet.create_fig(dict_wind_areas_de)
fig_wind_areas_en  = oet.create_fig(dict_wind_areas_en)
fig_wind_areas_fr  = oet.create_fig(dict_wind_areas_fr)

# Write figures

fig_wind_areas_de.write_html("docs/germany/figures/wind_areas.de.html", include_plotlyjs="directory")
fig_wind_areas_en.write_html("docs/germany/figures/wind_areas.en.html", include_plotlyjs="directory")
fig_wind_areas_fr.write_html("docs/germany/figures/wind_areas.fr.html", include_plotlyjs="directory")

#%% Renewable share --------------------------------------------------------------------------------

# Data

rs_data = oet.prepare_data("docs/germany/data/resshare_elec.csv","years")
rs_data = oet.calculate_trend(rs_data,"actual_bsv","5-years")

### Figures

# German

dict_resshares_de = copy.deepcopy(dict_pv_de)

del dict_resshares_de["base"]["subplots"]
del dict_resshares_de["figure_additions"]
del dict_resshares_de["figure_additions_needed"]
del dict_resshares_de["figure_trend"]

dict_resshares_de["base"]["data"] = rs_data
dict_resshares_de["base"]["title"] = "Anteil erneuerbarer Energien im Stromsektor"
dict_resshares_de["base"]["x_axis_range"]  = ["2009-07","2031-03"]
dict_resshares_de["base"]["y_axis_title"]  = "%"
dict_resshares_de["base"]["y_axis_range"]  = [0,90]
dict_resshares_de["base"]["buttons"]["y_max_2030"] = 90
dict_resshares_de["base"]["buttons"]["y_max_2045"] = 100
dict_resshares_de["base"]["buttons"]["xaxis_range_2030"] = ["2009-07", "2031-03"]
dict_resshares_de["base"]["buttons"]["xaxis_range_2045"] = ["2009-07", "2046-06"]
dict_resshares_de["base"]["hovermode"] = "x"
dict_resshares_de["base"]["source_file"] = "docs/germany/data/resshare_elec.csv"

dict_resshares_de["figure_actual"]["y"]           = "actual_bsv"
dict_resshares_de["figure_actual"]["name"]        = "Bruttostromverbrauch"
dict_resshares_de["figure_actual"]["group"]       = "reached"
dict_resshares_de["figure_actual"]["title_group"] = "<i>Tatsächlich erreichter Anteil</i>"
dict_resshares_de["figure_actual"]["title_group"] = "<i>Tatsächlich erreichter Anteil</i>"

dict_resshares_de["figure_actual2"]            = dict_resshares_de["figure_actual"].copy()
dict_resshares_de["figure_actual2"]["y"]       = "actual_nse"
dict_resshares_de["figure_actual2"]["group"]   = "reached"
dict_resshares_de["figure_actual2"]["name"]    = "Nettostromerzeugung"
dict_resshares_de["figure_actual2"]["color"]   = "second"
dict_resshares_de["figure_actual2"]["visible"] = "legendonly"
del dict_resshares_de["figure_actual2"]["title_group"]

dict_resshares_de["figure_trend"] = oet.trend.copy()
dict_resshares_de["figure_trend"]["y"]     = "trend_5years"
dict_resshares_de["figure_trend"]["name"]  = "5-Jahres-Trend"
dict_resshares_de["figure_trend"]["group"] = "reached"
del dict_resshares_de["figure_trend"]["title_group"]

for key in dict_resshares_de:
    if key == "base":
        pass
    else:
        dict_resshares_de[key]["hover"] = "%{y:.2f}%"

# English

dict_resshares_en                                  = copy.deepcopy(dict_resshares_de)
dict_resshares_en["base"]["title"]                 = "Share of renewable energy in the power sector"
dict_resshares_en["base"]["buttons"]["button2030"] = "until 2030"
dict_resshares_en["base"]["buttons"]["button2045"] = "until 2045"
dict_resshares_en["figure_actual"]["title_group"]  = "<i>Actually achieved share</i>"
dict_resshares_en["figure_actual"]["name"]         = "Gross electricity consumption (GEC)"
dict_resshares_en["figure_actual2"]["name"]        = "Net electricity generation"
dict_resshares_en["figure_trend"]["name"]          = "5-year-trend"
dict_resshares_en["figure_goal"]["title_group"]    = "<i>Government goals</i>"
dict_resshares_en["figure_goal"]["name"]           = "Targets of the coalition"
dict_resshares_en["figure_goal_lin"]["name"]       = "Linearized progresion"
dict_resshares_en["figure_ar_lead"]["title_group"] = "<i>Ariadne scenarios</i>"
dict_resshares_en["figure_ar_lead"]["name"]        = "Lead model"
dict_resshares_en["figure_ar_corr"]["name"]        = "Scenario corridor"
dict_resshares_en["legislative_period"]["text"]    = "Legislative period"

# French

dict_resshares_fr                                  = copy.deepcopy(dict_resshares_de)
dict_resshares_fr["base"]["title"]                 = "Part de l'énergie renouvelable dans le secteur de l'électricité"
dict_resshares_fr["base"]["buttons"]["button2030"] = "jusqu'en 2030"
dict_resshares_fr["base"]["buttons"]["button2045"] = "jusqu'en 2045"
dict_resshares_fr["figure_actual"]["title_group"]  = "<i>Part d'électricité atteinte</i>"
dict_resshares_fr["figure_actual"]["name"]         = "Consommation brute (CB)"
dict_resshares_fr["figure_actual2"]["name"]        = "Production nette"
dict_resshares_fr["figure_trend"]["name"]          = "5 dernières années"
dict_resshares_fr["figure_goal"]["title_group"]    = "<i>Réglementation</i>"
dict_resshares_fr["figure_goal"]["name"]           = "Objectifs de la coalition"
dict_resshares_fr["figure_goal_lin"]["name"]       = "Progression linéarisée"
dict_resshares_fr["figure_ar_lead"]["title_group"] = "<i>Scénarios Ariadne</i>"
dict_resshares_fr["figure_ar_lead"]["name"]        = "Modèle de référence"
dict_resshares_fr["figure_ar_corr"]["name"]        = "Intervalle de scénarios"
dict_resshares_fr["legislative_period"]["text"]    = "Période du mandat"

# Create figures

fig_rs_de  = oet.create_fig(dict_resshares_de)
fig_rs_en  = oet.create_fig(dict_resshares_en)
fig_rs_fr  = oet.create_fig(dict_resshares_fr)

# Update x-axis

update_dict = dict(title=None, tickmode = 'array', range = ["2009-06","2031-03"],
                tickvals = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042,
                                2043, 2044, 2045],
                ticktext = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024*', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040', '2041',
                                '2042', '2043', '2044', '2045'])

fig_rs_de.update_xaxes(update_dict)
fig_rs_en.update_xaxes(update_dict)
fig_rs_fr.update_xaxes(update_dict)

# Write

fig_rs_de.write_html("docs/germany/figures/resshares.de.html", include_plotlyjs="directory")
fig_rs_en.write_html("docs/germany/figures/resshares.en.html", include_plotlyjs="directory")
fig_rs_fr.write_html("docs/germany/figures/resshares.fr.html", include_plotlyjs="directory")

####################################################################################################
# HEAT
####################################################################################################

#%% Heat Pumps -------------------------------------------------------------------------------------

#hp_data           = pd.read_csv("docs/germany/data/hp.csv", sep = ",")
#hp_data['date']   = pd.to_datetime(hp_data['date'], format='%Y')
#hp_data = oet.calculate_trend(hp_data,"actual","5-years")
#hp_data['X']      = hp_data.index

hp_data = oet.prepare_data("docs/germany/data/hp.csv","years")
hp_data = oet.calculate_trend(hp_data,"actual","5-years")

base_hp = copy.deepcopy(base_default)

dict_hp_de = dict(
    base                 = base_hp,
    legislative_period   = oet.legislative_period.copy(),
    figure_actual_custom = oet.actual.copy(),
    figure_actual2       = oet.actual.copy(),
    figure_actual3       = oet.actual.copy(),
    figure_actual4       = oet.actual.copy(),
    figure_goal          = oet.goal.copy(),
    figure_goal_lin      = oet.goal_lin.copy(),
    figure_ar_lead       = oet.ar_lead.copy(),
    figure_ar_corr       = oet.ar_corr.copy(),
    figure_trend         = oet.trend.copy(),
    figure_scen1         = oet.trend.copy(),
    figure_scen2         = oet.trend.copy(),
    figure_scen3         = oet.trend.copy(),
    figure_scen4         = oet.trend.copy(),
)

dict_hp_de["base"]["data"] = hp_data
dict_hp_de["base"]["color_dict"] = colors.standard
dict_hp_de["base"]["title"] = "Bestand an Wärmepumpen"
dict_hp_de["base"]["x_axis_range"]  = ["1999-07","2031-03"]
dict_hp_de["base"]["y_axis_title"]  = "Millionen Geräte"
dict_hp_de["base"]["y_axis_range"]  = [0,8]
dict_hp_de["base"]["buttons"]["y_max_2030"] = 8
dict_hp_de["base"]["buttons"]["y_max_2045"] = 20
dict_hp_de["base"]["buttons"]["xaxis_range_2030"] = ["1999-07", "2031-03"]
dict_hp_de["base"]["buttons"]["xaxis_range_2045"] = ["1999-07", "2046-06"]
dict_hp_de["base"]["buttons"]["position"] = {}
dict_hp_de["base"]["buttons"]["position"]["x"] = 0.25 
dict_hp_de["base"]["buttons"]["position"]["y"] = 1
dict_hp_de["base"]["hovermode"]                   = "x"
dict_hp_de["base"]["barmode"]                     = "stack"
dict_hp_de["base"]["legend_dict"] = {
            "title"          : {"text":None},
            "traceorder"     :"grouped",
            "orientation"    :"v",
            "x"              : 0.01,
            "y"              : 1,
            "yanchor"        : "top",
            "xanchor"        : "left",
            "borderwidth"    : 1,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "groupclick"     : "toggleitem"}
dict_hp_de["base"]["margin"] = dict(l=0, r=0, t=40, b=0, pad=0)

del dict_hp_de["figure_actual_custom"]["color"]
del dict_hp_de["figure_actual_custom"]["hover"]
del dict_hp_de["figure_actual_custom"]["row"]
del dict_hp_de["figure_actual_custom"]["col"]
del dict_hp_de["figure_actual_custom"]["group"]
del dict_hp_de["figure_actual_custom"]["title_group"]

dict_hp_de["figure_actual_custom"]["name"]              = "Gesamt"
dict_hp_de["figure_actual_custom"]["x"]                 = hp_data['date']
dict_hp_de["figure_actual_custom"]["y"]                 = hp_data['actual']
dict_hp_de["figure_actual_custom"]["type"]              = "scatter"
dict_hp_de["figure_actual_custom"]["legendgroup"]       = "reached"
dict_hp_de["figure_actual_custom"]["legendgrouptitle"]  = None
dict_hp_de["figure_actual_custom"]["text"]              = round(hp_data['actual'],1)
dict_hp_de["figure_actual_custom"]["textposition"]      = 'top center'
dict_hp_de["figure_actual_custom"]["textfont"]          = dict(color='black',size=10)
dict_hp_de["figure_actual_custom"]["mode"]              = 'text'
dict_hp_de["figure_actual_custom"]["showlegend"]        = False
dict_hp_de["figure_actual_custom"]["hoverinfo"]         = "skip" 

dict_hp_de["figure_actual2"]["y"]           = "solewater"
dict_hp_de["figure_actual2"]["name"]        = "Sole-Wasser"
dict_hp_de["figure_actual2"]["group"]       = "reached"
dict_hp_de["figure_actual2"]["title_group"] = None
dict_hp_de["figure_actual2"]["color"]       = "first"
dict_hp_de["figure_actual2"]["visible"]     = True

dict_hp_de["figure_actual3"]["y"]           = "waterwater"
dict_hp_de["figure_actual3"]["name"]        = "Wasser-Wasser"
dict_hp_de["figure_actual3"]["group"]       = "reached"
dict_hp_de["figure_actual3"]["title_group"] = None
dict_hp_de["figure_actual3"]["color"]       = "second"
dict_hp_de["figure_actual3"]["visible"]     = True

dict_hp_de["figure_actual4"]["y"]           = "airwater"
dict_hp_de["figure_actual4"]["name"]        = "Luft-Wasser"
dict_hp_de["figure_actual4"]["group"]       = "reached"
dict_hp_de["figure_actual4"]["title_group"] = None
dict_hp_de["figure_actual4"]["color"]       = "third"
dict_hp_de["figure_actual4"]["visible"]     = True

dict_hp_de["figure_trend"]["y"]           = "trend_5years"
dict_hp_de["figure_trend"]["name"]        = "5-Jahres Trend"
dict_hp_de["figure_trend"]["group"]       = "reached"
dict_hp_de["figure_trend"]["title_group"] = None

dict_hp_de["figure_scen1"]["y"]           = "agora_knd2045"
dict_hp_de["figure_scen1"]["name"]        = "Agora KND 2045"
dict_hp_de["figure_scen1"]["group"]       = "scenario"
dict_hp_de["figure_scen1"]["title_group"] = "<i>Andere Szenarien</i>"
dict_hp_de["figure_scen1"]["line"]        = {"dash" : "dashdot"}
dict_hp_de["figure_scen1"]["color"]       = "second"

dict_hp_de["figure_scen2"]                = dict_hp_de["figure_scen1"].copy()
dict_hp_de["figure_scen2"]["y"]           = "bdi_klimapfade2"
dict_hp_de["figure_scen2"]["name"]        = "BDI Klimapfade 2.0"
dict_hp_de["figure_scen2"]["color"]       = "third"

dict_hp_de["figure_scen3"]                = dict_hp_de["figure_scen1"].copy()
dict_hp_de["figure_scen3"]["y"]           = "dena_kn100"
dict_hp_de["figure_scen3"]["name"]        = "Dena KN100"
dict_hp_de["figure_scen3"]["color"]       = "fourth"

dict_hp_de["figure_scen4"]                = dict_hp_de["figure_scen1"].copy()
dict_hp_de["figure_scen4"]["y"]           = "bmwk_lfs_tnstrom"
dict_hp_de["figure_scen4"]["name"]        = "BMWK LFS TN-Strom"
dict_hp_de["figure_scen4"]["color"]       = "fifth"

for key in dict_hp_de:
    if key in ["base","figure_actual_custom"]:
        pass
    else:
        dict_hp_de[key]["hover"] = "%{y:.2f}"

# English

dict_hp_en = copy.deepcopy(dict_hp_de)

dict_hp_en["base"]["title"]                 = "Stock of heat pumps"
dict_hp_en["base"]["y_axis_title"]          = "Million units"
dict_hp_en["base"]["buttons"]["button2030"] = "Until 2030"
dict_hp_en["base"]["buttons"]["button2045"] = "Until 2045"
dict_hp_en["legislative_period"]["text"]    = "Legislative period"

dict_hp_en["figure_actual_custom"]["name"] = "Total"
dict_hp_en["figure_actual2"]["name"]       = "Brine-Water"
dict_hp_en["figure_actual3"]["name"]       = "Water-Water"
dict_hp_en["figure_actual4"]["name"]       = "Air-Water"

dict_hp_en["figure_trend"]["name"]         = "5-year-trend"
dict_hp_en["figure_goal"]["name"]          = "Targets of the coalition"
dict_hp_en["figure_goal"]["title_group"]   = "<i>Government goals</i>"
dict_hp_en["figure_goal_lin"]["name"]      = "Linearized progression"

dict_hp_en["figure_ar_lead"]["title_group"] = "<i>Ariadne scenarios</i>"
dict_hp_en["figure_ar_lead"]["name"]        = "Lead model"
dict_hp_en["figure_ar_corr"]["name"]        = "Scenario corridor"

dict_hp_en["figure_scen1"]["title_group"] = "<i>Other scenarios</i>"

# French

dict_hp_fr = copy.deepcopy(dict_hp_en)
dict_hp_fr["base"]["title"]                 = "Parc de pompes à chaleur"
dict_hp_fr["base"]["y_axis_title"]          = "Unités (en millions)"
dict_hp_fr["base"]["buttons"]["button2030"] = "Jusqu'en 2030"
dict_hp_fr["base"]["buttons"]["button2045"] = "Jusqu'en 2045"
dict_hp_fr["legislative_period"]["text"]    = "Période du mandat"

dict_hp_fr["figure_actual_custom"]["name"]  = "Installées"
dict_hp_fr["figure_actual2"]["name"]        = "Eau-glycolée"
dict_hp_fr["figure_actual3"]["name"]        = "Air-eau"
dict_hp_fr["figure_actual4"]["name"]        = "Air-Water"
dict_hp_fr["figure_trend"]["name"]          = "5 dernières années"
dict_hp_fr["figure_goal"]["name"]           = "Objectifs de la coalition"
dict_hp_fr["figure_goal"]["title_group"]    = "<i>Réglementation</i>"
dict_hp_fr["figure_goal_lin"]["name"]       = "Progression linéarisée"
dict_hp_fr["figure_ar_lead"]["title_group"] = "<i>Scénarios Ariadne</i>"
dict_hp_fr["figure_ar_lead"]["name"]        = "Modèle de référence"
dict_hp_fr["figure_ar_corr"]["name"]        = "Intervalle de scénarios"
dict_hp_fr["figure_scen1"]["title_group"]   = "<i>Autres scénarios</i>"

fig_hp_de  = oet.create_fig(dict_hp_de)
fig_hp_en  = oet.create_fig(dict_hp_en)
fig_hp_fr  = oet.create_fig(dict_hp_fr)

update_dict = dict(title=None, tickmode = 'array', range = ["1999-09","2031-03"],
                tickvals = [2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,
                                2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
                                2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                                2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042,
                                2043, 2044, 2045],
                ticktext = ['2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010',
                                '2011', '2012', '2013', '2014', '2015', '2016', '2017',
                                '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025',
                                '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033',
                                '2034', '2035', '2036', '2037', '2038', '2039', '2040', '2041',
                                '2042', '2043', '2044', '2045'])

fig_hp_de.update_xaxes(update_dict)
fig_hp_en.update_xaxes(update_dict)
fig_hp_fr.update_xaxes(update_dict)

fig_hp_de.write_html("docs/germany/figures/hp.de.html", include_plotlyjs="directory")
fig_hp_en.write_html("docs/germany/figures/hp.en.html", include_plotlyjs="directory")
fig_hp_fr.write_html("docs/germany/figures/hp.fr.html", include_plotlyjs="directory")

#%% resshare_heat ----------------------------------------------------------------------------------

rs_heat_data = oet.prepare_data("docs/germany/data/resshare_heat_components.csv","years")
rs_heat_data = oet.calculate_trend(rs_heat_data,"total","5-years")

# Date column as years
rs_heat_data['date'] = rs_heat_data['years'].astype(str) + "-12-31"

base_rs_heat = copy.deepcopy(base_default)

dict_rs_heat_de = dict(
    base                 = base_rs_heat,
    legislative_period   = oet.legislative_period.copy(),
    figure_actual_custom = oet.actual.copy(),
    figure_actual2       = oet.actual.copy(),
    figure_actual3       = oet.actual.copy(),
    figure_actual4       = oet.actual.copy(),
    figure_goal          = oet.goal.copy(),
    figure_goal_lin      = oet.goal_lin.copy(),
    figure_trend         = oet.trend.copy(),
)

del dict_rs_heat_de["base"]["buttons"]

dict_rs_heat_de["base"]["data"] = rs_heat_data
dict_rs_heat_de["base"]["title"]   = "Endenergieverbrauch für Wärme und Kälte <br><sup>Anteil erneuerbarer Energien</sup>"
dict_rs_heat_de["base"]["title_y"] = 0.95
dict_rs_heat_de["base"]["margin"] = dict(t=50,b=0,r=0,l=0,pad=0)
dict_rs_heat_de["base"]["x_axis_range"]  = ["1990-03","2031-09"]
dict_rs_heat_de["base"]["y_axis_range"]  = [0,60]
dict_rs_heat_de["base"]["y_axis_title"]  = "%"
dict_rs_heat_de["base"]["barmode"]       = "stack"

del dict_rs_heat_de["figure_actual_custom"]["color"]
del dict_rs_heat_de["figure_actual_custom"]["hover"]
del dict_rs_heat_de["figure_actual_custom"]["row"]
del dict_rs_heat_de["figure_actual_custom"]["col"]
del dict_rs_heat_de["figure_actual_custom"]["group"]
del dict_rs_heat_de["figure_actual_custom"]["title_group"]

dict_rs_heat_de["figure_actual_custom"]["name"]              = "Gesamt"
dict_rs_heat_de["figure_actual_custom"]["x"]                 = rs_heat_data['date']
dict_rs_heat_de["figure_actual_custom"]["y"]                 = rs_heat_data['total']
dict_rs_heat_de["figure_actual_custom"]["type"]              = "scatter"
dict_rs_heat_de["figure_actual_custom"]["legendgroup"]       = "reached"
dict_rs_heat_de["figure_actual_custom"]["legendgrouptitle"]  = None
dict_rs_heat_de["figure_actual_custom"]["text"]              = round(rs_heat_data['total'],1)
dict_rs_heat_de["figure_actual_custom"]["textposition"]      = 'top center'
dict_rs_heat_de["figure_actual_custom"]["textfont"]          = dict(color='black',size=10)
dict_rs_heat_de["figure_actual_custom"]["mode"]              = 'text'
dict_rs_heat_de["figure_actual_custom"]["showlegend"]        = False
dict_rs_heat_de["figure_actual_custom"]["hoverinfo"]         = "skip" 

dict_rs_heat_de["figure_actual2"]["y"]           = "biogen"
dict_rs_heat_de["figure_actual2"]["name"]        = "Biogene Brennstoffe"
dict_rs_heat_de["figure_actual2"]["group"]       = "reached"
dict_rs_heat_de["figure_actual2"]["title_group"] = None
dict_rs_heat_de["figure_actual2"]["color"]       = "first"
dict_rs_heat_de["figure_actual2"]["visible"]     = True

dict_rs_heat_de["figure_actual3"]["y"]           = "solar"
dict_rs_heat_de["figure_actual3"]["name"]        = "Solarthermie"
dict_rs_heat_de["figure_actual3"]["group"]       = "reached"
dict_rs_heat_de["figure_actual3"]["title_group"] = None
dict_rs_heat_de["figure_actual3"]["color"]       = "second"
dict_rs_heat_de["figure_actual3"]["visible"]     = True

dict_rs_heat_de["figure_actual4"]["y"]           = "environmental"
dict_rs_heat_de["figure_actual4"]["name"]        = "Umweltwärme (Wärmepumpen) und Sonstiges"
dict_rs_heat_de["figure_actual4"]["group"]       = "reached"
dict_rs_heat_de["figure_actual4"]["title_group"] = None
dict_rs_heat_de["figure_actual4"]["color"]       = "third"
dict_rs_heat_de["figure_actual4"]["visible"]     = True

dict_rs_heat_de["figure_trend"]["y"]           = "trend_5years"
dict_rs_heat_de["figure_trend"]["name"]        = "5-Jahres Trend"
dict_rs_heat_de["figure_trend"]["group"]       = "reached"
dict_rs_heat_de["figure_trend"]["title_group"] = None

for key in dict_rs_heat_de:
    if key in ["base","figure_actual_custom"]:
        pass
    else:
        dict_rs_heat_de[key]["hover"] = "%{y:.1f}%"

# English

dict_rs_heat_en                                 = copy.deepcopy(dict_rs_heat_de)
dict_rs_heat_en["base"]["title"]                = "Final energy consumption for heating and cooling<br><sup>Share of renewable energy</sup>"
dict_rs_heat_en["legislative_period"]["text"]   = "Legislative period"
dict_rs_heat_en["figure_actual_custom"]["name"] = "Total"
dict_rs_heat_en["figure_actual2"]["name"]       = "Biogenic fuels"
dict_rs_heat_en["figure_actual3"]["name"]       = "Solar thermal"
dict_rs_heat_en["figure_actual4"]["name"]       = "Environmental heat (heat pumps) and other"
dict_rs_heat_en["figure_trend"]["name"]         = "5-year-trend"
dict_rs_heat_en["figure_goal"]["name"]          = "Targets of the coalition"
dict_rs_heat_en["figure_goal"]["title_group"]   = "<i>Government goals</i>"
dict_rs_heat_en["figure_goal_lin"]["name"]      = "Linearized progression"

# French

dict_rs_heat_fr                                  = copy.deepcopy(dict_rs_heat_de)
dict_rs_heat_fr["base"]["title"]                 = "Consommation finale d'énergie pour la chaleur et le froid<br><sup>Part d'énergies renouvelables</sup>"
dict_rs_heat_fr["legislative_period"]["text"]    = "Période du mandat"
dict_rs_heat_fr["figure_actual_custom"]["name"]  = "Total"
dict_rs_heat_fr["figure_actual2"]["name"]        = "Combustibles biogènes"
dict_rs_heat_fr["figure_actual3"]["name"]        = "Solaire thermique"
dict_rs_heat_fr["figure_actual4"]["name"]        = "Chaleur ambiante (pompes à chaleur) et autres"
dict_rs_heat_fr["figure_trend"]["name"]          = "5 dernières années"
dict_rs_heat_fr["figure_goal"]["name"]           = "Objectifs de la coalition"
dict_rs_heat_fr["figure_goal"]["title_group"]    = "<i>Réglementation</i>"
dict_rs_heat_fr["figure_goal_lin"]["name"]       = "Progression linéarisée"

fig_rs_heat_de  = oet.create_fig(dict_rs_heat_de)
fig_rs_heat_en  = oet.create_fig(dict_rs_heat_en)
fig_rs_heat_fr  = oet.create_fig(dict_rs_heat_fr)

fig_rs_heat_de.write_html("docs/germany/figures/resshare_heat.de.html", include_plotlyjs="directory")
fig_rs_heat_en.write_html("docs/germany/figures/resshare_heat.en.html", include_plotlyjs="directory")
fig_rs_heat_fr.write_html("docs/germany/figures/resshare_heat.fr.html", include_plotlyjs="directory")

#%% ------------------------------------------------------------------------------------------------

# Graph 2: Anteil Erneuerbare Energien im Gebäudesektor und Wärmesektor nach Klimaneutralität
# Read data from CSV
KN45_share_RE = pd.read_csv("docs/germany/data/heat_resshares_scenarios.csv")

# create a figure object
KN45_bld_share_RE = go.Figure()

# add the first variable as a line to the figure
KN45_bld_share_RE.add_trace(go.Scatter(x=KN45_share_RE["Jahr"], y=KN45_share_RE["Kopernikus_REtotal"], name="EE gesamt", line=dict(color='purple'), legendgroup="Ariadne KN 2045"))

# add the second variable as a line to the figure
KN45_bld_share_RE.add_trace(go.Scatter(x=KN45_share_RE["Jahr"], y=KN45_share_RE["Prognos_REtotal"], name="EE gesamt", line=dict(color='darkorange'), legendgroup="Agora KN 2045"))

# create stacked bar charts for each year Prognos
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(
        x=KN45_building_ph_ghg_year["Jahr"], 
        y=KN45_building_ph_ghg_year["Prognos_Heatpumps"], 
        name="Wärmepumpen", showlegend=i==0, offsetgroup=0, width=0.4, 
        marker_color='rgb(255, 140, 0)', legendgroup="Agora KN 2045"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Districtheating"], 
                           name="Fernwärme", showlegend=i==0, offsetgroup=0, width=0.4, marker_color='rgb(255, 165, 0)', legendgroup="Agora KN 2045"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Biomass"], 
                           name="Biomasse", showlegend=i==0, offsetgroup=0, width=0.4, marker_color='rgb(255, 187, 51)', legendgroup="Agora KN 2045"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Solarthermal"], 
                           name="Solarthermie", showlegend=i==0, offsetgroup=0, width=0.4, marker_color='rgb(255, 223, 178)', legendgroup="Agora KN 2045"))

# create stacked bar charts for each year BCG
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_Heat pumps"], 
                           name="Wärmepumpen", showlegend=i==0, offsetgroup=2, width=0.4, marker_color='rgb(0, 100, 0)', legendgroup="BDI Klimapfade 2.0"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_District heating"], 
                           name="Fernwärme", showlegend=i==0, offsetgroup=2, width=0.4, marker_color='rgb(50, 205, 50)', legendgroup="BDI Klimapfade 2.0"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_GreenGases_Biomass"], 
                           name="Grüne Gase, Biomasse, Power-to-Liquids", showlegend=i==0, offsetgroup=2, width=0.4, marker_color='rgb(0, 255, 0)', legendgroup="BDI Klimapfade 2.0"))

# create stacked bar charts for each year McKinsey
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1 , y=KN45_building_ph_ghg_year["McKinsey_HeatPumps"], 
                           name="Wärmepumpen", showlegend=i==0, offsetgroup=3, width=0.4, marker_color='rgb(139, 0, 0)', legendgroup="McKinsey KN 2045"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1, y=KN45_building_ph_ghg_year["McKinsey_DistrictHeating "], 
                           name="Fernwärme", showlegend=i==0, offsetgroup=3, width=0.4, marker_color='rgb(255, 0, 0)', legendgroup="McKinsey KN 2045"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1, y=KN45_building_ph_ghg_year["McKinsey_BGasSolarH2O"], 
                           name="Biogas, Solarthermie, Wasserstoff", showlegend=i==0, offsetgroup=3, width=0.4, marker_color='rgb(255, 150, 150)', legendgroup="McKinsey KN 2045"))

# Create invisible traces with annotations for each Trace group in the legend
KN45_bld_share_RE.add_trace(go.Scatter(x=[None], y=[None], mode='markers', marker=dict(color='rgba(0,0,0,0)'), legendgroup="Ariadne KN 2045", showlegend=True, name="<b>Ariadne KN 2045<b>"))
KN45_bld_share_RE.add_trace(go.Scatter(x=[None], y=[None], mode='markers', marker=dict(color='rgba(0,0,0,0)'), legendgroup="Agora KN 2045", showlegend=True, name="<b>Agora KN 2045<b>"))
KN45_bld_share_RE.add_trace(go.Scatter(x=[None], y=[None], mode='markers', marker=dict(color='rgba(0,0,0,0)'), legendgroup="BDI Klimapfade 2.0", showlegend=True, name="<b>BDI Klimapfade 2.0<b>"))
KN45_bld_share_RE.add_trace(go.Scatter(x=[None], y=[None], mode='markers', marker=dict(color='rgba(0,0,0,0)'), legendgroup="McKinsey KN 2045", showlegend=True, name="<b>McKinsey KN 2045<b>"))

# layout
KN45_bld_share_RE.update_layout(
    title="Klimaneutralitätsstudien: Anteil erneuerbare Energien im Wärme- und Gebäudesektor<br><sup>Wohn- und Nichtwohngebäude</sup>",
    xaxis_title="Jahr",
    yaxis_title="Prozentualer Anteil",
    template="simple_white",
    barmode="stack",
    bargap = 0.01,
    uniformtext=dict(minsize=8, mode='hide'),
    margin=dict(l=0, r=0, t=70, b=0, pad=0),
    font=dict(family="sans-serif, arial", size=12, color='#000000'),
    yaxis=dict(tickformat="0%"),
    #legend=dict(
    #    title="Legende",
    #    traceorder='normal',
    #    itemsizing='constant',
    #    items=[dict(label=title) for title in legend_titles]
    # )
)

KN45_bld_share_RE.write_html("docs/germany/figures/heat_resshares_scenarios.de.html", include_plotlyjs="directory")

#%% ------------------------------------------------------------------------------------------------

# Graph 3: Darstellung der sektoralen Emissionen im Gebäudesektor pro qm (kg/qm) für Klimaneutralitätsstudien (private Haushalte und GHD Sektor)
# Titel: CO2-Intensität im Gebäudesektor

# Mittels der Emissionsdaten für den Gebäudesektor sowie Annahmen zur Flächenentwicklung kann zudem 
# die Intensität der Treibhausgasemissionen (THG-Emissionen) pro Quadratmeter abgeleitet werden, die auf dem Weg
# zur Klimaneutralität eine hilfreiche Bezugsgröße sein kann. 
# Die unstehende Grafik zeigt die sektoralen THG-Emissionen in Kilogramm (kg) pro Quadrameter (m2) nach den 
# Klimaneutralitätszenarien von Agora, Ariadne, BDI und dena. Für die Flächenentwicklung wurde der Durchschnitt der Ariadne und Agora Szenarien verwendet.
# Die THG-Intensität sinkt in den Szenarien für private Haushalte sowie den Sektor Gewerbe, Handel und Dienstleistungen (GHD) 
# von durchschnittlich 22kg/m2 im Zeitraum 2018-20 auf 13 kg/m2 im Jahr 2030 und auf etwa 0,45 kg/m2 im Jahr 2045.

# Read the CSV file into a DataFrame
KN45_building_ph_ghg = pd.read_csv('docs/germany/data/emissionspersqm.csv')

# Create figure
KN45_bld_ph_ghg = go.Figure()

# Add the average line with hovertext
KN45_bld_ph_ghg.add_trace(go.Scatter(
    x=KN45_building_ph_ghg['year'], 
    y=KN45_building_ph_ghg['average'],
    mode='lines+markers',
    name='Durchschnitt',
    line=dict(color='red', width=2, dash='dot'),
    marker=dict(color='red', size=6),
    hovertext=['Jahr: {}<br>Emissionen: {} kg CO2-Äqu. /m2'.format(x, y) for x, y in zip(KN45_building_ph_ghg['year'], KN45_building_ph_ghg['average'])]
))

# Add the minimum line as a dotted line with hovertext
KN45_bld_ph_ghg.add_trace(go.Scatter(
    x=KN45_building_ph_ghg['year'],
    y=KN45_building_ph_ghg['Ariadne'],
    mode='lines+markers',
    name='Ariadne KN 2045',
    line=dict(color='purple', width=2),
    marker=dict(color='purple', size=6),
    hovertext=['Jahr: {}<br>Emissionen: {} kg CO2-Äqu. /m2'.format(x, y) for x, y in zip(KN45_building_ph_ghg['year'], KN45_building_ph_ghg['Ariadne'])]
))

# Add BDI
KN45_bld_ph_ghg.add_trace(go.Scatter(
    x=KN45_building_ph_ghg['year'],
    y=KN45_building_ph_ghg['BDI'],
    mode='lines+markers',
    name='BDI Klimapfade 2.0',
    line=dict(color='green', width=2),
    marker=dict(color='green', size=6),
    hovertext=['Jahr: {}<br>Emissionen: {} kg CO2-Äqu. /m2'.format(x, y) for x, y in zip(KN45_building_ph_ghg['year'], KN45_building_ph_ghg['BDI'])]
))

# Add Agora
KN45_bld_ph_ghg.add_trace(go.Scatter(
    x=KN45_building_ph_ghg['year'],
    y=KN45_building_ph_ghg['Agora'],
    mode='lines+markers',
    name='Agora KN 2045',
    line=dict(color='orange', width=2),
    marker=dict(color='orange', size=6),
    hovertext=['Jahr: {}<br>Emissionen: {} kg CO2-Äqu. /m2'.format(x, y) for x, y in zip(KN45_building_ph_ghg['year'], KN45_building_ph_ghg['Agora'])]
))

# Add Dena
KN45_bld_ph_ghg.add_trace(go.Scatter(
    x=KN45_building_ph_ghg['year'],
    y=KN45_building_ph_ghg['Dena'],
    mode='lines+markers',
    name='Dena Leitstudie Klimaneutralität',
    line=dict(color='blue', width=2),
    marker=dict(color='blue', size=6),
    hovertext=['Jahr: {}<br>Emissionen: {} kg CO2-Äqu. /m2'.format(x, y) for x, y in zip(KN45_building_ph_ghg['year'], KN45_building_ph_ghg['Dena'])]
))

# Set the title and axis labels
KN45_bld_ph_ghg.update_layout(
    title='Klimaneutralitätsstudien: Treibhausgasemissionen im Gebäudesektor pro qm <br><sub>Wohn- und Nichtwohngebäude</sub>',
    template="simple_white",
    xaxis=dict(title='Jahr'),
    yaxis=dict(title='Emissionen in kg CO2-Äqu. /m2'),
    font=dict(family="'sans-serif','arial'", size=12, color='#000000'),
    legend = dict(xanchor = "right", x = 1, borderwidth = 1),
    margin=dict(l=0, r=0, t=70, b=0, pad=0),)

# Set hovermode to closest to show hover information of the nearest point
KN45_bld_ph_ghg.update_layout(hovermode="closest")

# Increase line thickness and marker size
KN45_bld_ph_ghg.update_traces(line=dict(width=3), marker=dict(size=8))

# Export as HTML
KN45_bld_ph_ghg.write_html("docs/germany/figures/emissions_buildings_scenarios.de.html", include_plotlyjs="directory")

#%% ------------------------------------------------------------------------------------------------

# Graph 4: Treibhausgasemissionen im Gebäudesektor pro qm (kg/qm) ausschließlich für private Haushalte 
# - historisch sowie für Klimaneutralitätsszenarien 

#Text for Graph
# Für die THG-Intensität für private Haushalte kann auch ein historischer Vergleich angestellt
# werden. 
# Die untenstehende Grafik zeigt die hisotrischen Treihbausgasemissionen pro Quadratmeter
# für die historische Entwicklung seit 1990 auf Basis von Daten des Umweltbundesamts sowie
# die Entwicklung gemäß der Szeanrien für Klimaneutralität im Jahr 2045 von Agora und Ariadne.
# Wie der Grafik zu entnehmen ist, ist die THG-Intensität im Zeitraum von 2015 bis 2020 mit etwa 23 kg/m2
#unverändert geblieben. Allerdings sinkt sie gemäß der Szenarien für KN 2045 bereits im Jahr 2025 auf etwa
#18 kg/m2 und im Jahr 2040 auf unter 1 kg/m2.
# Für die Emissionsdaten ausschließlich privater Haushalte für Klimaneutralität im Jahr 2045 wurden die Ariadne-Daten genutzt.
# Für die Flächenentwicklung für private Haushalte Klimaneutralität im Jahr 2045 wurden die Agora-Daten genutzt. 

#Read file
KN45_building_ph = pd.read_csv('docs/germany/data/historic_emissionpersqm.csv')

# Extract data from DataFrame
years = KN45_building_ph['Jahr']
historic_emissions = KN45_building_ph['HistorischeEmissionen']
future_emissions = KN45_building_ph['KN2045']

# Create figure
KN45_bld_ph = go.Figure()

# Add the historic emissions as blue bars using the 'bar' trace type
KN45_bld_ph.add_trace(go.Bar(x=years, y=historic_emissions, width = 1, marker_color='navy', name='Historische Emissionen - Umweltbundesamt', hovertemplate='Year: %{x}<br>Emissionen: %{y} kg CO2-Äqu. per m2'))

# Add the future emissions as a red line using the 'line' trace type
KN45_bld_ph.add_trace(go.Scatter(x=years, y=future_emissions, mode='lines+markers', 
                        line=dict(color='red'), marker=dict(symbol='circle', size=8), 
                        name='KN 2045 Szenarien - Ariadne & Agora', hovertemplate='Jahr: %{x}<br>Emissionen: %{y} kg CO2-Äqu. per m2'))

# Update chart title
KN45_bld_ph.update_layout(title_text='Treibhausgasemissionen im Gebäudesektor pro qm (kg/qm)<br><sup>Private Haushalte</sup>',
    template = "simple_white",
    xaxis_title="Jahr",
    xaxis=dict(dtick=5),
    yaxis_title="Emissionen in kg CO2-Äqu./m2",
    font=dict(family="'sans-serif','arial'", size=12, color='#000000'),
    margin=dict(l=0, r=0, t=70, b=0, pad=0),
    legend=dict(xanchor="right",x=1,borderwidth=1)
)

# Export as HTML
KN45_bld_ph.write_html("docs/germany/figures/emissions_buildings_persqm_scenarios.de.html", include_plotlyjs="directory")



####################################################################################################
##### HYDROGEN #####################################################################################
####################################################################################################

#%% Electrolysis figure ----------------------------------------------------------------------------

h2_data           = pd.read_csv("docs/germany/data/h2.csv", sep = ",")
h2_data['months'] = h2_data['months'].astype(str)
h2_data['date']   = pd.to_datetime(h2_data['date'], format='%d.%m.%Y')

#%% create electrolysis figure

def create_h2(data,title,actual,linear,goal,legislative_period,
              y_axis_title,
              only_legislative_period,til_2030,til_2050,
              legend_title_ariadne,ariadne_lead,
              ariadne_lead_points,ariadne_span,
              y_max_2025,y_max_2030,y_max_2050):
    """
    Create H2 figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-12", x1 = "2025-11",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top right")

    fig.add_trace(go.Bar(x = data['date'], y = data['actual'],
                         name = actual,
                         marker_color = '#ff7f0e', hovertemplate = '%{y:.2f}GW',
                         legendgroup = 'misc',
                         legendgrouptitle_text = None))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                         name = linear,
                         mode = 'lines', line = dict(color="#1f77b4", dash='dot'),
                         hovertemplate = '%{y:.2f}GW',
                         legendgroup = 'misc'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot",
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup = 'misc'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_points'],
                             name = ariadne_lead,
                             mode = "markers",
                             line = dict(color="#989C94", dash='dot', width = 2),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne",
                             legendgrouptitle_text=legend_title_ariadne))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ariadne_lead_linear'],
                             name = ariadne_lead_points,
                             line = dict(color="#989C94", dash='dot', width = 3),
                             hovertemplate = '%{y:.2f}GW', visible='legendonly',
                             legendgroup="ariadne"))
    fig.add_trace(go.Scatter(x = pd.concat([data['date'], data['date'][::-1]]),
                             y = pd.concat([data['ariadne_max'], data['ariadne_min'][::-1]]),
                             name = ariadne_span,
                             fill = "toself", mode='lines', visible='legendonly',
                             fillcolor = "rgba(152, 156, 148, 0.25)",
                             line = dict(color="#989C94", dash='dot', width = 0),
                             legendgroup="ariadne",
                             hoverinfo='skip'
                             )) 

    fig.update_layout(
        updatemenus=[
            dict(type="buttons", direction="left", active=0, showactive=True, pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0, y = 1.12,
            buttons=list([
                dict(label=til_2030,
                     method="relayout",
                     args=[{'xaxis.range': ["2020-12", "2031-03"],
                            'yaxis.range': [0, y_max_2030]}]),
                dict(label=til_2050,
                     method="relayout",
                     args=[{'xaxis.range': ["2020-12", "2046-06"],
                            'yaxis.range': [0, y_max_2050]}])
                ]),
            )
            ]
        )

    fig.update_xaxes(title=None, range = ["2020-12", "2031-03"])
    fig.update_yaxes(matches = None, title = y_axis_title, range = [0, y_max_2030])

    fig.update_layout(
        title = {'text': title, 'y': 1},
        template = "simple_white", uniformtext_mode = 'hide',
        font = dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        margin=dict(l=0, r=0, t=70, b=0, pad=0),
        xaxis_hoverformat='%Y', hovermode = "x",
        legend = {
            "title":None,
            "traceorder":"grouped",
            "groupclick":"toggleitem",
            "orientation":"v",
            "borderwidth":1,
            "x": 0.01, "y": 1,
            "yanchor": "top",
            "xanchor": "left",
            "font":{"size":11}})

    return fig

fig_h2_de = create_h2(h2_data, "Installierte Leistung Elektrolyse",
                      "Tatsächlich erreicht", "Linearer Verlauf","Ziel der Koalition",
                      "Legislaturperiode","Gigawatt<sub>elektrisch<sub>",
                      "Nur Legislaturperiode","bis 2030","bis 2045",
                      "Ariadne-Szenarien","Leitmodell",
                      "Leitmodell (interpoliert)","Szenariokorridor",
                      6,15,50)
fig_h2_en = create_h2(h2_data, "Installed electrolysis capacity",
                      "Actually achieved", "Linear progression","Target of the coalition",
                      "Legislative period","Gigawatt<sub>electric<sub>",
                      "Legislative period only","until 2030","until 2045",
                      "Ariadne scenarios","Lead model",
                      "Lead model (interpolated)","Scenario corridor",
                      6,15,50)
fig_h2_fr = create_h2(h2_data, "Capacité installée d'électrolyse",
                      "Déjà installée", "Progression linéaire","Objectif de la coalition",
                      "Période du mandat","Gigawatts<sub>electrique<sub>",
                      "Période du mandat", "Jusqu'en 2030", "Jusqu'en 2045",
                      "Scénarios Ariadne","Modèle de référence",
                      "Modèle de référence (tendance)","Intervalle de scénarios",
                      6,15,50)

fig_h2_de.write_html("docs/germany/figures/h2.de.html", include_plotlyjs="directory")
fig_h2_en.write_html("docs/germany/figures/h2.en.html", include_plotlyjs="directory")
fig_h2_fr.write_html("docs/germany/figures/h2.fr.html", include_plotlyjs="directory")

#%% IEA h2 project status ##########################################################################

# Function to build h2 project area/bar chart
def create_h2_projects_figure(
#    data_tuple,title,sp_title1,sp_title2,operational,under_construction,fid,feas_study,demo,
    data_tuple,title,sp_title1,sp_title2,operational,fid_construction,feas_study,demo,
    concept,total,linear,goal,start_year,status,pal,axis_title
    ):
    """
    Create H2 IEA figure
    """

    data = data_tuple[0][data_tuple[0]['date_online']>=start_year]
    tot_data = data_tuple[1]


    fig = make_subplots(rows=1,cols=2,column_widths=[0.9,0.1],shared_yaxes='rows',subplot_titles=(sp_title1,sp_title2),
    horizontal_spacing=0.00)
    #fig1 = go.Figure()
    #fig2 = go.Figure()

    fig.append_trace(go.Scatter(x = data["date_online"], y = data['operational'],
                             name = operational,
                             #text = data['operational'],
                             fillcolor= pal["operational"],
                             marker_color= pal["operational"],
                             legendgroup="status",
                             legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
    fig.append_trace(go.Scatter(x = data["date_online"], y = data['demo'],
                             name = demo,
                            # text = data['demo'],
                             fillcolor = pal["demo"],
                             marker_color = pal["demo"],
                             legendgroup="status",
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
#    fig.append_trace(go.Scatter(x = data["date_online"], y = data['under construction'],
#                             name = under_construction,
#                             #text = data['under construction'],
#                             fillcolor = pal["under_construction"],
#                             marker_color = pal["under_construction"],
#                             legendgroup="status",
#                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
    fig.append_trace(go.Scatter(x = data["date_online"], y = data['fid_construction'],
                             name = fid_construction,
                             #text = data['fid_construction'],
                             fillcolor = pal["fid"],
                             marker_color = pal["fid"],
                             legendgroup="status",
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
    fig.append_trace(go.Scatter(x = data["date_online"], y = data['feasibility study'],
                             name = feas_study,
                             #text = data['feasibility study'],
                             fillcolor = pal["feas_study"],
                             marker_color = pal["feas_study"],
                            legendgroup="status",
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
    fig.append_trace(go.Scatter(x = data["date_online"], y = data['concept'],
                             name = concept,
                            # text = data['concept'],
                             fillcolor = pal["concept"],
                             marker_color = pal["concept"],
                             legendgroup="status",
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',stackgroup='one'),row=1,col=1)
    fig.append_trace(go.Scatter(x = data["date_online"], y = data['total'],
                             name = total,
                             #text = data['total'],
                             textposition = "bottom right",
                             mode = 'lines+markers+text',
                             line_shape="linear",
                             line = dict(color="black", dash = "solid", width=3),
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>'),row=1,col=1)
    fig.add_trace(go.Scatter(x = data['date_online'], y = data['plan_linear'],
                            name = linear,
                            mode = 'lines', line = dict(color="#3c435a", dash='dot'),
                            hovertemplate = '%{y:.2f}GW',
                            legendgroup = 'misc'),row=1,col=1)
    fig.add_trace(go.Scatter(x = data['date_online'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "x-dot", marker_color="#3c435a",
                             hovertemplate = '%{y:.2f}GW',
                             legendgroup = 'misc'),row=1,col=1)

    # Total block
    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='operational']['cum_capacity'],
                             name = operational,
                             #text = data['operational'],
                             marker_color = pal["operational"],
                             legendgroup="status",
                             legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
                             #stackgroup="two",
                             showlegend=False
                             ),row=1,col=2)
    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='demo']['cum_capacity'],
                             #base = tot_data[tot_data['status']=='operational']['cum_capacity'],
                             name = demo,
                             #offsetgroup=0,
                             #text = data['operational'],
                             marker_color = pal["demo"],
                             legendgroup="status",
                             #legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
                             #stackgroup="two"
                             showlegend=False
                             ),row=1,col=2)
#    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='under construction']['cum_capacity'],
#                             #base = tot_data[tot_data['status']=='operational']['cum_capacity']+
#                             #tot_data[tot_data['status']=='demo']['cum_capacity'],
#                             name = under_construction,
#                             #offsetgroup=0,
#                             #text = data['operational'],
#                             marker_color = pal["under_construction"],
#                             legendgroup="status",
#                             #legendgrouptitle_text=status,
#                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
#                             #stackgroup="two"
#                             showlegend=False
#                             ),row=1,col=2)
    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='fid_construction']['cum_capacity'],
                             #base = tot_data[tot_data['status']=='operational']['cum_capacity']+
                             #tot_data[tot_data['status']=='demo']['cum_capacity']+
                             #tot_data[tot_data['status']=='under construction']['cum_capacity'],
                             name = fid_construction,
                             #offsetgroup=0,,
                             #text = data['operational'],
                             marker_color = pal["fid"],
                             legendgroup="status",
                             #legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
                             #stackgroup="two"
                             showlegend=False
                             ),row=1,col=2)
    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='feasibility study']['cum_capacity'],
                             #base = tot_data[tot_data['status']=='operational']['cum_capacity']+
                             #tot_data[tot_data['status']=='demo']['cum_capacity']+
                             #tot_data[tot_data['status']=='under construction']['cum_capacity']+
                             #tot_data[tot_data['status']=='fid']['cum_capacity'],
                             name = feas_study,
                             #offsetgroup=0,
                             #text = data['operational'],
                             marker_color = pal["feas_study"],
                             legendgroup="status",
                             #legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
                             #stackgroup="two"
                             showlegend=False
                             ),row=1,col=2)

    fig.append_trace(go.Bar(x = tot_data["total"], y = tot_data[tot_data['status']=='concept']['cum_capacity'],
                             name = concept,
                             #base = tot_data[tot_data['status']=='operational']['cum_capacity']+
                             #tot_data[tot_data['status']=='demo']['cum_capacity']+
                             #tot_data[tot_data['status']=='under construction']['cum_capacity']+
                             #tot_data[tot_data['status']=='fid']['cum_capacity']+
                             #tot_data[tot_data['status']=='feasibility study']['cum_capacity'],
                             #offsetgroup=0,
                             #text = data['operational'],
                             marker_color = pal["concept"],
                             legendgroup="status",
                             #legendgrouptitle_text=status,
                             hovertemplate = '%{y:.3f} GW<sub>el<sub>',
                             #stackgroup="two"
                             showlegend=False,

                             ),row=1,col=2)
    
    #fig.add_trace(fig1,row=1,col=1)
    #fig.add_trace(fig2,row=1,col=2)

    fig.update_yaxes(matches = None, title = axis_title,range =[0,22])

    fig.update_layout(title = title, template = "simple_white",
                     font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                     margin=dict(l=0, r=0, t=100, b=0, pad=0),
                     legend_groupclick = "toggleitem",
                     hovermode = 'x unified',
                     yaxis2 = dict(visible=False),
                     #Scattermode='relative'#
                     barmode = 'stack'
                     )
    fig.update_annotations(font_size=14)                 

    return fig


# Define path
dir = "docs/germany/data/"

# Define columns
columns = (
"ref","project_name","country","date_online","decommision_date","status",
"technology","tech_comments","tech_electricity","tech_renewables",
"product",
"end_use_refining","end_use_ammonia","end_use_methanol","end_use_iron_steel",
"end_use_other_ind","end_use_mobility","end_use_power","end_use_grid_inj",
"end_use_chp","end_use_dom_heat","end_use_biofuel",
"end_use_synfuel","end_use_ch4_grid_inj","end_use_ch4_mobility",
"announced_size","norm_capa_mw_el","norm_capa_nm3_h2_h",
"norm_capa_kt_h2_h","norm_capa_tco2_y","norm_capa_iea_nm3_h2_h","refs"
)

# Create color palette dict
palette = {"operational":"#008800","demo":"#880088","under_construction":"#ffff00","fid":"#ff8800","feas_study":"#888888","concept":"rgba(205, 182, 153, .50)"}

# Import data
tup = (pd.read_csv(dir + "iea_h2_by_year.csv"),pd.read_csv(dir + "iea_h2_total.csv"))

# Create English chart
#fig_iea_h2_en = create_h2_projects_figure(tup,start_year=2022,pal=palette,status="Project status",operational="Operational",under_construction="Under construction",fid="Final investment decision",demo="Demo",concept="Concept",feas_study="Feasibility study",
fig_iea_h2_en = create_h2_projects_figure(tup,start_year=2021,pal=palette,status="Project status",operational="Operational",fid_construction="Under construction/FID",demo="Demo",concept="Concept",feas_study="Feasibility study",
title = "Installed electrolysis capacity, sorted by project status",
sp_title1="Commissioning year<br> <sub> <sub>",
sp_title2="Total incl. undated:<br>" + str(round(tup[1]["cum_capacity"].sum(),1)) +" GW<sub>el<sub>",
total = "Total",linear = "Linear progression",goal = "Target of the coalition",axis_title='Gigawatt<sub>electric<sub>')

# Create German chart
#fig_iea_h2_de = create_h2_projects_figure(tup,start_year=2022,pal=palette,status="Projektstatus",operational="In Betrieb",under_construction="Im Bau",fid="Finale Investitionsentscheidung",demo="Demo-Betrieb",concept="Konzept",feas_study="Machbarkeitsstudie",
fig_iea_h2_de = create_h2_projects_figure(tup,start_year=2021,pal=palette,status="Projektstatus",operational="In Betrieb",fid_construction="Im Bau/Finale Investitionsentscheidung",demo="Demo-Betrieb",concept="Konzept",feas_study="Machbarkeitsstudie",
title = "Installierte Leistung Elektrolyse, nach Projektstatus",
sp_title1="Jahr der Inbetriebnahme<br> <sub> <sub>",
sp_title2="Gesamt inkl. undatierter Projekte:<br>" + str(round(tup[1]["cum_capacity"].sum(),1)) +" GW<sub>el<sub>",
total = "Gesamt",linear = "Linearer Verlauf",goal = "Ziel der Koalition",axis_title='Gigawatt<sub>elektrisch<sub>')

# Create French chart
fig_iea_h2_fr = create_h2_projects_figure(tup,start_year=2021,pal=palette,status="État du projet",operational="En service",fid_construction="En construction/décision finale d'investissement",demo="Fonctionnement de démonstration",concept="Concept",feas_study="Étude de faisabilité",
title = "Puissance installée en électrolyse, par état de projet",
sp_title1="Année de mise en service<br> <sub> <sub>",
sp_title2="Total, y compris les projets non datés:<br>" + str(round(tup[1]["cum_capacity"].sum(),1)) +" GW<sub>el<sub>",
total = "Total",linear = "Trajectoire linéaire",goal = "Objectif de la coalition",axis_title='Gigawatt<sub>elektrisch<sub>')

fig_iea_h2_de.write_html("docs/germany/figures/iea_h2.de.html",include_plotlyjs="directory")
fig_iea_h2_en.write_html("docs/germany/figures/iea_h2.en.html",include_plotlyjs="directory")
fig_iea_h2_fr.write_html("docs/germany/figures/iea_h2.fr.html",include_plotlyjs="directory")

# %% primary energy consumption (pec) ##############################################################

pec_data         = pd.read_csv("docs/germany/data/pec.csv", sep = ",")
pec_data['date'] = pd.to_datetime(pec_data['date'], format='%Y')
pec_data['X']    = pec_data.index

# trend 2017 - 2021
pec1_data = pec_data[['date','X','sum']].dropna()
pec1_data = pec1_data[(pec1_data['date'] >= '2017') &
                                  (pec1_data['date'] <= '2021')]
pec_reg1_x    = np.array(pec1_data['X']).reshape((-1, 1))
pec_reg1      = LinearRegression().fit(pec_reg1_x, pec1_data['sum'])

predict1_pec  = pec_data[['date','X','sum']]
predict1_pec  = predict1_pec[(predict1_pec['date'] >= '2021')].reset_index()
predict1_pec['index'] = predict1_pec.index
predict1_pec['trend_since_2017'] = predict1_pec.loc[0,'sum'] + predict1_pec['index'] * pec_reg1.coef_
predict1_pec = predict1_pec[['date','trend_since_2017']]

# merge
pec_data = pec_data.merge(predict1_pec, how='left', on='date')

#%% create primary energy consumption figure

def create_pec_figure(data,title,oil,hard_coal,lignite,natural_gas,
                      goal,linear_reduction,legend_group_title,y_axis_title,
                      legend_group_title_trends,trend_2017_2021):

    """
    Create primary energy consumption figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear'],
                             name = linear_reduction,
                             line = dict(color="#989C94", dash='dot', width = 1),
                             legendgroup = 'misc',
                             legendgrouptitle_text = " ",
                             hovertemplate = '%{y:.0f} TWh'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "arrow-down",
                             marker_color = "green",
                             hovertemplate = '%{y:.2f} TWh',
                             legendgroup = 'misc'))

    fig.add_trace(go.Bar(x = data['date'], y = data['oil'],
                             name = oil,
                             marker_color = "rgba(26, 55, 77, .80)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             legendgrouptitle_text = legend_group_title,
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['hard_coal'],
                             name = hard_coal,
                             marker_color = "rgba(26, 55, 77, .60)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['lignite'],
                             name = lignite,
                             marker_color = "rgba(26, 55, 77, .40)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             hovertemplate = '%{y:.2f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['natural_gas'],
                             name = natural_gas,
                             marker_color = "rgba(205, 182, 153, .50)",
                             marker_line_width = 0,
                             legendgroup="fossils",
                             hovertemplate = '%{y:.2f} TWh'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021,
                             mode = 'lines',
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.3s}',
                             visible='legendonly',
                             legendgroup='trends',
                             legendgrouptitle_text = legend_group_title_trends
                             ))

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,4000])
    fig.update_xaxes(matches = None,                       range = ['1990','2046'])

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide', legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=100, b=0, pad=0),
                      xaxis_hoverformat='%Y',
                      barmode='stack', bargap=0,
                      hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      legend_groupclick = "toggleitem",
                      legend_traceorder="reversed",
                      legend = {'traceorder':'grouped', 'orientation':'h',
                                'x': 0.0, 'y': -0.5,
                                'yanchor': 'bottom', 'xanchor': 'left'
                      })
    return fig

fig_pec_figure_de = create_pec_figure(pec_data,"Fossiler Primärenergieverbrauch",
                                  "Mineralöl", "Steinkohle","Braunkohle",
                                  "Erdgas",
                                  "Ziel der Koalition","Linearer Verlauf",
                                  "Energieträger", 
                                  "Terawattstunden",
                                  "Historische Trends", "2017-2021")

fig_pec_figure_en = create_pec_figure(pec_data,"Fossil primary energy consumption",
                                  "Oil","Hard coal","Lignite",
                                  "Natural gas",
                                  "Goal of the coalition", "Linear trend",
                                  "Energy carrier",
                                  "Terawatt-hour",
                                  "Historic trends", "2017-2021")

fig_pec_figure_fr = create_pec_figure(pec_data,"Consommation primaire d'énergie fossile",
                                  "Pétrole","Charbon anthracite","Houille brune",
                                  "Gaz naturel",
                                  "Objectif de la coalition","Trajectoire linéaire",
                                  "Vecteur d'énergie",
                                  "Térawattheure",
                                  "Tendance historique", "2017-2021")

fig_pec_figure_de.write_html("docs/germany/figures/fig_pec.de.html", include_plotlyjs="directory")
fig_pec_figure_en.write_html("docs/germany/figures/fig_pec.en.html", include_plotlyjs="directory")
fig_pec_figure_fr.write_html("docs/germany/figures/fig_pec.fr.html", include_plotlyjs="directory")

# %% primary energy consumption (pec) 3-monthly ####################################################

pec_3month_data         = pd.read_csv("docs/germany/data/pec_3month.csv", sep = ",")

#%% create 3-monthly primary energy consumption figure

def create_pec_3month_figure(data,title,oil,hard_coal,lignite,natural_gas,
                      y_axis_title):

    """
    Create 3-monthly primary energy consumption figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['oil'],
                             name = oil,
                             text = data['oil'],
                             marker_color = "rgba(26, 55, 77, .80)",
                             marker_line_width = 0,
                             hovertemplate = '%{y:.1f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['hard_coal'],
                             name = hard_coal,
                             text = data['hard_coal'],
                             marker_color = "rgba(26, 55, 77, .60)",
                             marker_line_width = 0,
                             hovertemplate = '%{y:.1f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['lignite'],
                             name = lignite,
                             text = data['lignite'],
                             marker_color = "rgba(26, 55, 77, .40)",
                             marker_line_width = 0,
                             hovertemplate = '%{y:.1f} TWh'))
    fig.add_trace(go.Bar(x = data['date'], y = data['natural_gas'],
                             name = natural_gas,
                             text = data['natural_gas'],
                             marker_color = "rgba(205, 182, 153, .50)",
                             marker_line_width = 0,
                             hovertemplate = '%{y:.1f} TWh'))

    fig.update_yaxes(matches = None, title = y_axis_title)

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide', legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      barmode='stack')
    return fig

fig_pec_3month_figure_de = create_pec_3month_figure(pec_3month_data,
                                  "Fossiler Primärenergieverbrauch, Quartale 1-4 ggü. Vorjahreszeitraum",
                                  "Mineralöl", "Steinkohle","Braunkohle",
                                  "Erdgas",
                                  "Terawattstunden")

fig_pec_3month_figure_en = create_pec_3month_figure(pec_3month_data,
                                  "Fossil primary energy consumption, quarters 1-4 vs. prior-year period",
                                  "Oil","Hard coal","Lignite",
                                  "Natural gas",
                                  "Terawatt-hours")

fig_pec_3month_figure_fr = create_pec_3month_figure(pec_3month_data,
                                  "Consommation primaire d'énergie fossile, trimestres 1-4 vs. année précédente",
                                  "Pétrole","Charbon anthracite","Houille brune",
                                  "Gas naturel",
                                  "Térawattheure")

fig_pec_3month_figure_de.write_html("docs/germany/figures/fig_pec_3month.de.html", include_plotlyjs="directory")
fig_pec_3month_figure_en.write_html("docs/germany/figures/fig_pec_3month.en.html", include_plotlyjs="directory")
fig_pec_3month_figure_fr.write_html("docs/germany/figures/fig_pec_3month.fr.html", include_plotlyjs="directory")


# %% gas imports ###################################################################################

gas_data           = pd.read_csv("docs/germany/data/gas.csv", sep = ",")
gas_data['date']   = pd.to_datetime(gas_data['date'], format='%d.%m.%Y') #'%d.%m.%Y'

#%% create gas figure

def create_gas_figure(data,title,net_imports,ru_nordstream,ru_poland,ru_cz,norway,netherlands,
                       other_import,overall_import, net_exports, pl_exports, cz_export,
                       other_export):
    """
    Create gas import figure
    """

    fig = go.Figure()

    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_nordstream'],
                             name = ru_nordstream,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .80)",
                             line=dict(width=1, color = 'rgba(26, 55, 77, 1)'),
                             legendgroup="imports",
                             legendgrouptitle_text=net_imports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_poland'],
                             name = ru_poland,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .60)",
                             line=dict(width=1, color = "rgba(26, 55, 77, .80)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['ru_via_cz'],
                             name = ru_cz,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(26, 55, 77, .40)",
                             line=dict(width=1, color = "rgb(26, 55, 77, .60)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['norway'],
                             name = norway,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(205, 182, 153, 0.5)",
                             line=dict(width=0, color = "rgba(205, 182, 153, 0.75)"),
                            legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['netherlands'],
                             name = netherlands,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(187, 100, 100, 0.5)",
                             line=dict(width=0, color = "rgba(187, 100, 100, 0.75)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['others_net_imports'],
                             name = other_import,
                             stackgroup = "one",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.15)",
                             line=dict(width=0, color = "rgba(180, 207, 176, .75)"),
                             legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['pl_net_exports'],
                             name = pl_exports,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.50)",
                             line=dict(width=0, color = "rgba(64, 104, 130, .75)"),
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['cz_net_exports'],
                             name = cz_export,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.30)",
                             line=dict(width=0, color = "rgba(255, 211, 101, .75)"),
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.2f}'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['others_net_exports'],
                             name = other_export,
                             stackgroup = "two",
                             line_shape="spline",
                             fillcolor = "rgba(5, 89, 91, 0.15)",
                             line=dict(width=0, color = "rgba(180, 207, 176, 0.75)"),
                             legendgroup="exports",
                             hovertemplate = '%{y:.2f}'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['total_net_imports'],
                             name = overall_import,
                             mode = 'lines',
                             line_shape="spline",
                             line = dict(color="black", dash = "solid", width=3),
                             #legendgroup="imports",
                             hovertemplate = '%{y:.2f}'))

    fig.update_yaxes(matches = None, title = 'TWh', range = [-75,150])
    fig.update_layout(title = title, template = "simple_white",
                     uniformtext_mode = 'hide', legend_title_text = '',
                     font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                     margin=dict(l=0, r=0, t=100, b=0, pad=0),
                     xaxis_hoverformat='%b %Y',
                     hovermode = "x unified",
                     hoverlabel = dict(namelength = -1),
                     legend_groupclick = "toggleitem",
                     legend_traceorder="reversed",
                     legend = {'traceorder':'grouped', 'orientation':'h',
                               'x': 0.0, 'y': -0.7,
                               'yanchor': 'bottom', 'xanchor': 'left'
                     })
    return fig

fig_gas_de = create_gas_figure(gas_data,"Monatliche Netto-Erdgasimporte nach Deutschland",
                                   "Netto-Importe",
                                   "Russland (via Nord Stream 1)","Russland (via Polen)",
                                   "Russland (via Tschechien)","Norwegen","Niederlande","Andere",
                                   "Gesamt",
                                   "Netto-Exporte","Polen","Tschechien","Andere")

fig_gas_en = create_gas_figure(gas_data,"Monthly net natural gas imports to Germany",
                                   "Net imports",
                                   "Russia (via Nord Stream 1)","Russia (via Poland)",
                                   "Russia (via Czech Republic)","Norway","Netherlands","Others",
                                   "Total",
                                   "Net exports","Poland","Czech Republic","Others")

fig_gas_fr = create_gas_figure(gas_data,"Importations mensuelles nettes de gaz naturel en Allemagne",
                               "Importations nettes",
                               "Russie (via Nord Stream 1)","Russie (via la Pologne)",
                               "Russie (via la République Tchèque)","Norvège","Pays-Bas",
                               "Autres",
                               "Total",
                               "Exportations nettes","Pologne","République Tchèque","Autres")

fig_gas_de.write_html("docs/germany/figures/gas.de.html", include_plotlyjs="directory")
fig_gas_en.write_html("docs/germany/figures/gas.en.html", include_plotlyjs="directory")
fig_gas_fr.write_html("docs/germany/figures/gas.fr.html", include_plotlyjs="directory")

# %% gas imports 3-monthly #########################################################################

gas_3month_data           = pd.read_csv("docs/germany/data/gas_3month.csv", sep = ",")

#%% create 3-monthly gas figure

def create_gas_3month_figure(data,title,net_imports,ru_nordstream,ru_poland,ru_cz,norway,netherlands,
                       other_import,overall_import, net_exports, pl_exports, cz_export,
                       other_export):
    """
    Create gas import figure
    """

    fig = go.Figure()

    fig.add_trace(go.Bar(x = data['date'], y = data['ru_via_nordstream'],
                             name = ru_nordstream,
                             text = data['ru_via_nordstream'],
                             marker_color = "rgba(26, 55, 77, .80)",
                             legendgroup="imports",
                             legendgrouptitle_text=net_imports,
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['ru_via_poland'],
                             name = ru_poland,
                             text = data['ru_via_poland'],
                             marker_color = "rgba(26, 55, 77, .60)",
                             legendgroup="imports",
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['ru_via_cz'],
                             name = ru_cz,
                             text = data['ru_via_cz'],
                             marker_color = "rgba(26, 55, 77, .40)",
                             legendgroup="imports",
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['norway'],
                             name = norway,
                             text = data['norway'],
                             marker_color = "rgba(205, 182, 153, 0.5)",
                            legendgroup="imports",
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['netherlands'],
                             name = netherlands,
                             text = data['netherlands'],
                             marker_color = "rgba(187, 100, 100, 0.5)",
                             legendgroup="imports",
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['others_net_imports'],
                             name = other_import,
                             text = data['others_net_imports'],
                             marker_color = "rgba(5, 89, 91, 0.15)",
                             legendgroup="imports",
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['pl_net_exports'],
                             name = pl_exports,
                             text = data['pl_net_exports'],
                             marker_color = "rgba(5, 89, 91, 0.50)",
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['cz_net_exports'],
                             name = cz_export,
                             text = data['cz_net_exports'],
                             marker_color = "rgba(5, 89, 91, 0.30)",
                             legendgroup="exports",
                             legendgrouptitle_text=net_exports,
                             hovertemplate = '%{y:.1f}'))
    fig.add_trace(go.Bar(x = data['date'], y = data['others_net_exports'],
                             name = other_export,
                             text = data['others_net_exports'],
                             marker_color = "rgba(5, 89, 91, 0.15)",
                             legendgroup="exports",
                             hovertemplate = '%{y:.1f}'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['total_net_imports'],
                             name = overall_import,
                             text = data['total_net_imports'],
                             textposition = "bottom right",
                             mode = 'lines+markers+text',
                             line_shape="spline",
                             line = dict(color="black", dash = "solid", width=3),
                             hovertemplate = '%{y:.1f}'))

    fig.update_yaxes(matches = None, title = 'TWh')

    fig.update_layout(title = title, template = "simple_white",
                     font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                     margin=dict(l=0, r=0, t=100, b=0, pad=0),
                     legend_groupclick = "toggleitem",
                     barmode='relative')

    return fig

fig_gas_3month_de = create_gas_3month_figure(gas_3month_data,
                                             "Netto-Erdgasimporte, letzte drei Monate ggü. Vorjahreszeitraum",
                                             "Netto-Importe",
                                             "Russland (via Nord Stream 1)","Russland (via Polen)",
                                             "Russland (via Tschechien)","Norwegen","Niederlande","Andere",
                                             "Gesamt",
                                             "Netto-Exporte","Polen","Tschechien","Andere")

fig_gas_3month_en = create_gas_3month_figure(gas_3month_data,
                                             "Net natural gas imports, last three months vs. prior-year period",
                                             "Net imports","Russia (via Nord Stream 1)",
                                             "Russia (via Poland)","Russia (via Czech Republic)",
                                             "Norway","Netherlands","Others","Total",
                                             "Net exports","Poland","Czech Republic","Others")

fig_gas_3month_fr = create_gas_3month_figure(gas_3month_data,
                                             "Importations nettes de gaz naturel, Mars-Mai 2022 vs. 2021",
                                             "Importations nettes","Russie (via Nord Stream 1)",
                                             "Russie (via la Pologne)",
                                             "Russie (via la République Tchèque)","Norvège",
                                             "Pays-Bas","Autres","Total",
                                             "Exportations nettes","Pologne","République Tchèque","Autres")

fig_gas_3month_de.write_html("docs/germany/figures/gas_3month.de.html", include_plotlyjs="directory")
fig_gas_3month_en.write_html("docs/germany/figures/gas_3month.en.html", include_plotlyjs="directory")
fig_gas_3month_fr.write_html("docs/germany/figures/gas_3month.fr.html", include_plotlyjs="directory")

####################################################################################################
# EMISSIONS ########################################################################################
####################################################################################################

# --------------------------------------------------------------------------------------------------
#%% sectoral emissions 
# --------------------------------------------------------------------------------------------------

emissions_sect_data           = pd.read_csv("docs/germany/data/emissions_sect.csv", sep = ",")
emissions_sect_data['date']   = pd.to_datetime(emissions_sect_data['date'], format='%Y')
emissions_sect_data['X']      = emissions_sect_data.index

# trend 2017-2021
emissions_sect_reg1_data = emissions_sect_data[['date','X','sum_wo_lulucf']].dropna()
emissions_sect_reg1_data = emissions_sect_reg1_data[(emissions_sect_reg1_data['date'] >= '2017') &
                            (emissions_sect_reg1_data['date'] <= '2021')]
emissions_sect_reg1_x    = np.array(emissions_sect_reg1_data['X']).reshape((-1, 1))
emissions_sect_reg1      = LinearRegression().fit(emissions_sect_reg1_x, emissions_sect_reg1_data['sum_wo_lulucf'])

predict1_emissions_sect = emissions_sect_data[['date','X','sum_wo_lulucf']]
predict1_emissions_sect = predict1_emissions_sect[(predict1_emissions_sect['date'] >= '2021')].reset_index()
predict1_emissions_sect['index'] = predict1_emissions_sect.index
predict1_emissions_sect['trend_since_2017'] = predict1_emissions_sect.loc[0,'sum_wo_lulucf'] + predict1_emissions_sect['index'] * emissions_sect_reg1.coef_
predict1_emissions_sect = predict1_emissions_sect[['date','trend_since_2017']]

# meerge
emissions_sect_data = emissions_sect_data.merge(predict1_emissions_sect, how='left', on='date')

# create sectoral emissions figure

def create_emissions_sect(data,title,legislative_period,
              plan_energy,plan_industry,plan_buildings,plan_transport,plan_agriculture,
              plan_waste_other,plan_sum,plan_linear_sum,
              group_title_planned,
              energy,industry,buildings,transport,agriculture,waste_other,lulucf,label_sum,
              group_title_achieved,
              group_title_trends,trend_2017_2021,
              y_axis):

    """
    Create sectoral emissions figure
    """

    fig = go.Figure()

    fig.add_vrect(x0 = "2021-12", x1 = "2025-11",
                  annotation_text = legislative_period,
                  fillcolor = "green", opacity = 0.1, layer = 'below',
                  annotation_position = "top left")

    fig.add_trace(go.Bar(x = data['date'], y = data['energy'],
                         name = energy,
                         marker_color = '#ff8800', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual',
                         legendgrouptitle_text = group_title_achieved))
    fig.add_trace(go.Bar(x = data['date'], y = data['industry'],
                         name = industry,
                         marker_color = '#ffff00', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['buildings'],
                         name = buildings,
                         marker_color = '#000088', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['transport'],
                         name = transport,
                         marker_color = '#880088', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['agriculture'],
                         name = agriculture,
                         marker_color = '#008800', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['waste_other'],
                         name = waste_other,
                         marker_color = '#880000', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Bar(x = data['date'], y = data['lulucf'],
                         name = lulucf,
                         marker_color = '#888888', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['sum_wo_lulucf'],
                         name = label_sum, mode = "lines",
                         marker_color = '#000000', hovertemplate = '%{y:.1f}',
                         legendgroup = 'actual'))
    
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_energy'],
                         name = plan_energy,
                         marker_color = '#ffc38d', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned',
                         legendgrouptitle_text = group_title_planned))
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_industry'],
                         name = plan_industry,
                         marker_color = '#ffff9e', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned'))
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_buildings'],
                         name = plan_buildings,
                         marker_color = '#967dc4', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned'))
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_transport'],
                         name = plan_transport,
                         marker_color = '#c78dc1', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned'))
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_agriculture'],
                         name = plan_agriculture,
                         marker_color = '#95c486', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned'))
    fig.add_trace(go.Bar(x = data['date'], y = data['plan_waste_other'],
                         name = plan_waste_other,
                         marker_color = '#cf8878', hovertemplate = '%{y:.1f}',
                         legendgroup = 'planned'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_sum'],
                             name = plan_sum,
                             mode = "lines", line = dict(dash="dot"),
                             marker_size=12, marker_symbol = "x-dot", marker_color = "#000000",
                             hovertemplate = '%{y:.2r}',
                             legendgroup = 'planned'))
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear_sum'],
                             name = plan_linear_sum,
                             line = dict(color="#1f77b4", dash='dot'), hovertemplate = '%{y:.1f}',
                             legendgroup = 'planned'))

    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021, line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.1f}', visible='legendonly',
                             legendgroup="trends",
                             legendgrouptitle_text = group_title_trends))

    fig.update_yaxes(matches = None, title = y_axis, range = [-50,1250])
    fig.update_xaxes(title=None, range = ["1989-01", "2046-01"])

    fig.update_layout(title = title, template = "simple_white",
                         uniformtext_mode = 'hide', #legend_title_text = 'Typ',
                         font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
                         legend = dict(traceorder="grouped+reversed",
                                       yanchor="top", xanchor="left", x=1, y=1,
                                       orientation = "v"),
                         legend_groupclick = "toggleitem",                        
                         margin=dict(l=0, r=0, t=50, b=0, pad=0),
                         barmode='relative',
                         xaxis_hoverformat='%Y',
                         hovermode = "x")

    return fig
    
fig_emissions_sect_de = create_emissions_sect(emissions_sect_data,"Sektorale Treibhausgasemissionen","Legislaturperiode",
                      "Energiewirtschaft","Industrie","Gebäude","Verkehr","Landwirtschaft","Abfallw. & Sonstiges","Gesamtemissionen","Linearer Verlauf",
                      "Regierungsziele",
                      "Energiewirtschaft","Industrie","Gebäude","Verkehr","Landwirtschaft",
                      "Abfallw. & Sonstiges","LULUCF","Gesamt (ohne LULUCF)",
                      "Tatsächlich erreicht",
                      "Trend (Gesamtemissionen)","2017-2021",
                      "Mio t CO2-äquivalent")

fig_emissions_sect_en = create_emissions_sect(emissions_sect_data,"Sectoral greenhouse gas emissions","Legislative period",
                      "Energy","Industry","Buildings","Transport","Agriculture","Waste and others",
                      "Total","Linear trend",
                      "Government targets",
                      "Energy","Industry","Buildings","Transport","Agriculture","Waste and others",
                      "LULUCF","Total (without LULUCF)",
                      "Actually achieved",
                      "Historic trend (total emissions)","2017-2021",
                      "Mio t CO2-equivalent")

# French translation pending
fig_emissions_sect_fr = create_emissions_sect(emissions_sect_data,"Émissions sectorielles de gaz à effet de serre","Période du mandat",
                      "Énergie","Industrie","Bâtiments","Transport","Agriculture","Déchets et autres",
                      "Émissions totales","Progression linéaire",
                      "Objectifs du gouvernement",
                      "Énergie","Industrie","Bâtiments","Transport","Agriculture","Déchets et autres",
                      "LULUCF","Totales (sans LULUCF)",
                      "Émissions passées",
                      "Tendances historiques (émissions totales)","2017-2021",
                      "Millions tCO2éq")

fig_emissions_sect_de.write_html("docs/germany/figures/emissions_sect.de.html", include_plotlyjs="directory")
fig_emissions_sect_en.write_html("docs/germany/figures/emissions_sect.en.html", include_plotlyjs="directory")
fig_emissions_sect_fr.write_html("docs/germany/figures/emissions_sect.fr.html", include_plotlyjs="directory")

# --------------------------------------------------------------------------------------------------
#%% Graph 1: Treibhausgasemissionen für Klimaneutralität 2045 im Szenarienvergleich
# --------------------------------------------------------------------------------------------------

#Text for graph
#Für einen weiteren Blick in die Zukunft über das Jahr 2030 hinaus, können 
# Klimaneutralitätsstudien herangezogen werden, die die Realisierbarkeit 
# von Klimaneutralität im Jahr 2045 für Deutschland untersuchen. Die untenstehende
# Grafik zeigt die Range sowie den Durchschnitt der Studien von Agora, Ariadne, BDI sowie dena.
# Gemäß der Szenarien müssen die meisten Treibhausgasemissionsreduktionen bis 2045 in den Sektoren Energie, Verkehr,
# Industrie und Gebäude erfolgen. 
# Zudem ist die Range der Modelle in den Sektoren Energie, Industrie und Verkehr am größten, während
#Szenarien zu ähnlicheren Emissionswerten etwa für den Gebäudesektor kommen.
# Zu beachten ist, dass nicht alle Studien für die hier ausgewählten Jahre und Sektoren Emissionswerte zur Verfügung stellen. 
# Fehlende Jahreswerte für die einzelnen Sekoten wurden nicht interpoliert, sodass sowohl die Range als auch der Durchschnitt 
# teilweise "Knicke" aufweisen, da für bestimmte Jahre nicht alle Modelle mit in die Range bzw. den Durschnitt einfließen.

# Read file
KN45_sec = pd.read_csv("docs/germany/data/emissions_scenarios.csv")

# Extract the indicators and their respective average, minimum and maximum columns
indicators = [col.split("_")[0] for col in KN45_sec.columns if col.endswith("_avg")]
avg_cols = [col for col in KN45_sec.columns if col.endswith("_avg")]
min_cols = [col for col in KN45_sec.columns if col.endswith("_min")]
max_cols = [col for col in KN45_sec.columns if col.endswith("_max")]
ksg_cols = [col for col in KN45_sec.columns if col.endswith("_KSG")]

# Define the colors for each indicator
colors_emissions = ["brown", "green", "purple", "blue", "yellow", "orange", "gray"]

KN45_sectors = go.Figure()

for i in range(len(indicators)):
    KN45_sectors.add_trace(
        go.Scatter(
            x=KN45_sec["Jahr"],
            y=KN45_sec[max_cols[i]],
            line=dict(color=colors_emissions[i], width=1, dash='dot'),
            name=indicators[i] + " Range (Max)",
            showlegend=True,
            legendgroup=indicators[i] + " Range",
            customdata=[[KN45_sec[max_cols[i]][j], KN45_sec[min_cols[i]][j]] for j in range(len(KN45_sec))]
        )
    )
    KN45_sectors.add_trace(
        go.Scatter(
            x=KN45_sec["Jahr"],
            y=KN45_sec[min_cols[i]],
            fill='tonexty',
            line=dict(color=colors_emissions[i], width=1, dash='dot'),
            name=indicators[i] + " Range (Min)",
            showlegend=False,
            legendgroup=indicators[i] + " Range"
        )
    )

    # Add the average line plot for each indicator
    KN45_sectors.add_trace(
        go.Scatter(
            x=KN45_sec["Jahr"],
            y=KN45_sec[avg_cols[i]],
            name=indicators[i] + " Durchschnitt",
            line=dict(color=colors_emissions[i], width=2),
        visible='legendonly'
        )
    )

# Add the KSG line plot for each indicator
    try:
        KN45_sectors.add_trace(
        go.Scatter(
            x=KN45_sec["Jahr"],
            y=KN45_sec[ksg_cols[i]],
            name=indicators[i] + " KSG",
            line=dict(color=colors_emissions[i], width=2),
            mode="lines+markers",
            marker=dict(
                symbol="square",
                size=7,
                color=colors_emissions[i],
                line=dict(width=1, color="black")
            ),
            visible='legendonly'
        )
    )
    except Exception:
        pass

# the layout of the plot
KN45_sectors.update_layout(
    title="Szenarienvergleich: Treibhausgasemissionen für Klimaneutralität im Jahr 2045",
    template="simple_white",
    xaxis_title="Jahr",
    yaxis_title="Emissionen in Mio. t CO2-Äquivalente",
    font=dict(
        family="'sans-serif','arial'",
        size=12,
        color='#000000'
    ),
    legend_title="Indikatoren", 
    # Disable default double-click behavior
    clickmode="event+select",
    margin=dict(l=0, r=0, t=50, b=0, pad=0),
    legend = dict(font = dict(size=10))
)

# Modify the legend labels to include both the minimum and maximum lines
for i in range(len(indicators)):
    for trace in KN45_sectors.data:
        if trace.name.startswith(indicators[i] + " Range"):
            trace.name = indicators[i] + " Range"


# Modify the legend labels for Durchschnitt and KSG to remove the sector name
for i in range(len(indicators)):
    for trace in KN45_sectors.data:
        if trace.name.startswith(indicators[i] + " Durchschnitt"):
            trace.name = "Durchschnitt" 
        elif trace.name.startswith(indicators[i] + " KSG"):
            trace.name = "KSG" 
        elif trace.name.endswith(" Range"):  
            trace.name = "<b>" + trace.name + "</b>"

# Export as HTML
KN45_sectors.write_html("docs/germany/figures/emissions_scenarios.de.html", include_plotlyjs="directory")

# %% electricity sector emissions ##################################################################

emissions_electr_data         = pd.read_csv("docs/germany/data/emissions_electr.csv", sep = ",")
emissions_electr_data['date'] = pd.to_datetime(emissions_electr_data['date'], format='%Y')
emissions_electr_data['X']    = emissions_electr_data.index

# trend 2017 - 2021
emissions_electr1_data = emissions_electr_data[['date','X','emissions']].dropna()
emissions_electr1_data = emissions_electr1_data[(emissions_electr1_data['date'] >= '2017') &
                                  (emissions_electr1_data['date'] <= '2021')]
emissions_electr_reg1_x    = np.array(emissions_electr1_data['X']).reshape((-1, 1))
emissions_electr_reg1      = LinearRegression().fit(emissions_electr_reg1_x, emissions_electr1_data['emissions'])

predict1_emissions_electr  = emissions_electr_data[['date','X','emissions']]
predict1_emissions_electr  = predict1_emissions_electr[(predict1_emissions_electr['date'] >= '2021')].reset_index()
predict1_emissions_electr['index'] = predict1_emissions_electr.index
predict1_emissions_electr['trend_since_2017'] = predict1_emissions_electr.loc[0,'emissions'] + predict1_emissions_electr['index'] * emissions_electr_reg1.coef_

predict1_emissions_electr = predict1_emissions_electr[['date','trend_since_2017']]

# merge
emissions_electr_data = emissions_electr_data.merge(predict1_emissions_electr, how='left', on='date')

#%% create electricity sector emissions figure

def create_emissions_electr_figure(data,title,emissions,emission_factor,
                      goal,linear_reduction,legend_group_title,y_axis_title, y2_axis_title,
                      legend_group_title_trends,trend_2017_2021):

    """
    Create primary electricity sector emissions figure
    """

    fig = go.Figure()

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_linear_emissions'],
                             name = linear_reduction,
                             line = dict(color="#1f77b4", dash='dash'),
                             legendgroup = 'misc',
                             legendgrouptitle_text = " ",
                             hovertemplate = '%{y:.0f} Mio. Tonnen'),
                             secondary_y=False,)
    fig.add_trace(go.Scatter(x = data['date'], y = data['plan_emissions'],
                             name = goal,
                             mode = "markers", marker_size=12, marker_symbol = "arrow-down",
                             marker_color = "green",
                             hovertemplate = '%{y:.0f} Mio. Tonnen',
                             legendgroup = 'misc'),
                             secondary_y=False)

    fig.add_trace(go.Bar(x = data['date'], y = data['emissions'],
                             name = emissions,
                             marker_color = "#ff7f0e",
                             marker_line_width = 0,
                             legendgroup="actual",
                             legendgrouptitle_text = legend_group_title,
                             hovertemplate = '%{y:.0f} Mio. Tonnen'),
                             secondary_y=False)

    fig.add_trace(go.Scatter(x = data['date'], y = data['emission_factor'],
                             name = emission_factor,
                             mode = 'lines',
                             line_shape="spline",
                             line = dict(color="#d62728", dash = "solid", width=3),
                             legendgroup="actual",
                             legendgrouptitle_text = legend_group_title,
                             hovertemplate = '%{y:.0f} g/kWh'),
                             secondary_y=True)
 
    fig.add_trace(go.Scatter(x = data['date'], y = data['trend_since_2017'],
                             name = trend_2017_2021,
                             mode = 'lines',
                             line = dict(color="#ff7f0e", dash='dot'),
                             hovertemplate = '%{y:.0f} Mio. Tonnen',
                             visible='legendonly',
                             legendgroup='trends',
                             legendgrouptitle_text = legend_group_title_trends
                             ),
                             secondary_y=False)

    fig.update_yaxes(matches = None, title = y_axis_title, range = [0,400],secondary_y=False)
    fig.update_yaxes(matches = None, title = y2_axis_title, range = [0,800],secondary_y=True)
    fig.update_xaxes(matches = None, range = ['1990','2036'])

    fig.update_layout(title = title, template = "simple_white",
                      uniformtext_mode = 'hide', legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=100, b=0, pad=0),
                      xaxis_hoverformat='%Y',
                      barmode='stack', bargap=0,
                      hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      legend_groupclick = "toggleitem",
                      legend_traceorder="reversed",
                      legend = {'traceorder':'grouped', 'orientation':'h',
                                'x': 0.0, 'y': -0.5,
                                'yanchor': 'bottom', 'xanchor': 'left'
                      })
    return fig

fig_emissions_electr_figure_de = create_emissions_electr_figure(emissions_electr_data,"CO<sub>2</sub>-Emissionen der Stromerzeugung",
                                  "Emissionen", "Emissionsfaktor",
                                  "Ziel der Koalition","Linearer Verlauf",
                                  "Tatsächlich erreicht", 
                                  "Millionen Tonnen", "g/kWh",
                                  "Historischer Trend", "2017-2021")

fig_emissions_electr_figure_en = create_emissions_electr_figure(emissions_electr_data,"CO<sub>2</sub> emissions of electricity generation",
                                  "Emissions","Emission factor",
                                  "Goal of the coalition", "Linear trend",
                                  "Actually reached",
                                  "Million tons", "g/kWh",
                                  "Historic trends", "2017-2021")

fig_emissions_electr_figure_fr = create_emissions_electr_figure(emissions_electr_data,"CO<sub>2</sub> émis par la production d'électricité",
                                  "Émissions","Intensité de CO2",
                                  "Objectif de la coalition","Trajectoire linéaire",
                                  "Niveaux observés",
                                  "Millions de tonnes", "g/kWh",
                                  "Tendance historique", "2017-2021")

fig_emissions_electr_figure_de.write_html("docs/germany/figures/emissions_electr.de.html", include_plotlyjs="directory")
fig_emissions_electr_figure_en.write_html("docs/germany/figures/emissions_electr.en.html", include_plotlyjs="directory")
fig_emissions_electr_figure_fr.write_html("docs/germany/figures/emissions_electr.fr.html", include_plotlyjs="directory")

#%% Create reached data

reached_windon = [
    "wind_on",
    windon_data.query("date == '2030-12-15'").plan.values[0],
    windon_data['actual'].fillna(method='ffill').iloc[-1]]

reached_pv = [
    "pv",
    pv_data.query("date == '2030-12-15'").plan.values[0],
    pv_data['actual'].fillna(method='ffill').iloc[-1]]

reached_windoff = [
    "windoff",
    windoff_data.query("date == '2030-12-15'").plan.values[0],
    windoff_data['actual'].fillna(method='ffill').iloc[-1]]

reached_hp = [
    "hp",
    hp_data.query("date == '2030-01-01'").plan.values[0],
    hp_data['actual'].fillna(method='ffill').iloc[-1]]

reached_hp[1] = reached_hp[1] * 1000000
reached_hp[2] = reached_hp[2] * 1000000

reached_bev = [
    "bev",
    bev_data.query("date == '2030-12-15'").plan.values[0],
    bev_data['actual'].fillna(method='ffill').iloc[-1]]

reached_cs = [
    "cs",
    cs_data.query("date == '2030-12-31'").plan.values[0],
    cs_data['actual'].fillna(method='ffill').iloc[-1]]

reached_h2 = [
    "h2",
    h2_data.query("date == '2030-12-15'").plan.values[0],
    h2_data['actual'].fillna(method='ffill').iloc[-1]]

reached_data_list = [reached_h2,reached_bev,reached_cs,reached_hp,reached_windoff,reached_windon,
                     reached_pv]

reached_data = pd.DataFrame (reached_data_list, columns = ['tech', 'plan', 'actual'])

reached_data['reached'] = reached_data['actual'] / reached_data['plan'] * 100
reached_data['to_be_reached'] = 100 - reached_data['reached']
reached_data['goal'] = 100

def create_reached_figure(data, title, reached_title, to_be_reached_title,
                          x_axis_title,y_axis_labels,
                          separators):

    #fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig = go.Figure()

    positions = ['outside','outside','outside','inside','inside','inside','inside']
    
    fig.add_bar(
        x=data['reached'],
        y=data['tech'],
        name=reached_title,
        text = data['actual'],
        textposition = positions,
        insidetextanchor="end",
        textangle = 0,
        texttemplate = '%{text:,.2f}',
        hovertemplate = '%{x:.1f}%',
        orientation='h',
        marker=dict(
            color='rgba(34, 87, 126, 0.75)',
            line=dict(color='rgba(34, 87, 126, 0.75)', width=0.5)
        )
    )

    fig.add_bar(
        x=reached_data['goal'],
        y=reached_data['tech'],
        name=to_be_reached_title,
        text = data['plan'],
        orientation='h',
        texttemplate = '%{text:,.0f}',
        hoverinfo='skip',
        marker=dict(
            color='rgba(34, 87, 126, 0.25)',
            line=dict(color='rgba(34, 87, 126, 0.75)', width=0.5)
        )
    )

    fig.update_xaxes(title = x_axis_title, range = [0,100],fixedrange = True)
    fig.update_yaxes(fixedrange = True)
    
    for idx in range(len(fig.data)):
        fig.data[idx].y = y_axis_labels

    fig.update_layout(title = dict(text=title,y=0.98),
                      barmode = 'overlay',
                      template = 'simple_white',
                      uniformtext_mode = 'hide', 
                      legend_title_text = '',
                      font = dict(family = "'sans-serif','arial'", size=14, color='#000000'),
                      margin=dict(l=0, r=0, t=40, b=0, pad=0),
                      #xaxis_hoverformat='%b %Y',
                      #hovermode = "x unified",
                      hoverlabel = dict(namelength = -1),
                      showlegend = False,
                      separators = separators,
                      )
    return fig

fig_reached_de = create_reached_figure(reached_data,
                                   "Aktueller Stand gegenüber Zielen für 2030",
                                   "bis jetzt erreicht", "noch zu erreichen",
                                   "Erreicht in %",
                                   ['Leistung Wasserstoffelektrolyse [GW]',
                                    'Elektroautos [Anzahl]','Ladepunkte [Anzahl]',
                                    'Wärmepumpen [Anzahl]','Wind auf See [GW]',
                                    'Wind an Land [GW]','Photovoltaik [GW]'],
                                    ',.')

fig_reached_en = create_reached_figure(reached_data,
                                   "Current status of indicators comparing to 2030 goals",
                                   "reached until now", "still to be reached",
                                   "Reached in %",
                                   ['Hydrogen electrolysis [GW]',
                                    'Battery electric vehicles [units]','Charging points [units]',
                                    'Heatpumps [units]','Offshore wind power[GW]',
                                    'Onshore wind power [GW]','Photovoltaik power [GW]'],
                                    '.,')

fig_reached_fr = create_reached_figure(reached_data,
                                   "Situation actuelle comparée aux objectifs pour 2030",
                                   "réalisé", "reste à réaliser",
                                   "Part réalisée en %",
                                   ["Électrolyse de l'hydrogène [GW]",
                                    'Voitures électriques [Unités]','Bornes de chargement [Unités]',
                                    'Pompes à chaleur [Unités]','Puissance éolien en mer [GW]',
                                    'Puissance éolien terrestre [GW]','Puissance photovoltaïque [GW]'],
                                    ', ')

# Make plot static
config = {'modeBarButtonsToRemove': ['select2d','lasso2d','toImage','resetScale'],
    'displayModeBar': False,
    #'scrollZoom': False,
    #'staticPlot': True
    }

fig_reached_de.write_html("docs/germany/figures/fig_reached.de.html", include_plotlyjs="directory", config=config)
fig_reached_en.write_html("docs/germany/figures/fig_reached.en.html", include_plotlyjs="directory", config=config)
fig_reached_fr.write_html("docs/germany/figures/fig_reached.fr.html", include_plotlyjs="directory", config=config)

####################################################################################################
# Natural gas consumption ##########################################################################
####################################################################################################

#%% Get data

url_gas_fig1     = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/gas/figures/data_gas_fig1.csv"
url_gas_fig2     = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/gas/figures/data_gas_fig2.csv"
url_gas_fig2_cum = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/gas/figures/data_gas_fig2cum.csv"
url_gas_fig3     = "https://gitlab.com/diw-evu/oet/oet-data/-/raw/main/data/DE/gas/figures/data_gas_fig3.csv"

data_gas_fig1     = pd.read_csv(url_gas_fig1,     sep = ",")
data_gas_fig2     = pd.read_csv(url_gas_fig2,     sep = ",")
data_gas_fig2_cum = pd.read_csv(url_gas_fig2_cum, sep = ",")
data_gas_fig3     = pd.read_csv(url_gas_fig3,     sep = ",")

data_gas_fig1['date'] = data_gas_fig1.apply(lambda x: date.fromisocalendar(x['year'].astype(int),x['week'].astype(int),1).strftime("%Y-%m-%d"), axis=1)
data_gas_fig1['date'] = pd.to_datetime(data_gas_fig1['date'])

data_gas_fig2['date'] = data_gas_fig2.apply(lambda x: date.fromisocalendar(x['year'].astype(int),x['week'].astype(int),1).strftime("%Y-%m-%d"), axis=1)
data_gas_fig2['date'] = pd.to_datetime(data_gas_fig2['date'])

data_gas_fig3['date'] = data_gas_fig3.apply(lambda x: date.fromisocalendar(x['year'],x['week'],1).strftime("%Y-%m-%d"), axis=1)
data_gas_fig3['date'] = pd.to_datetime(data_gas_fig3['date'])

# Get last edited date
fig1_last_modified = requests.head(url_gas_fig1).headers.get('Date')
if fig1_last_modified:
    fig1_last_modified = dateutil.parser.parse(fig1_last_modified)

fig2_last_modified = requests.head(url_gas_fig2).headers.get('Date')
if fig2_last_modified:
    fig2_last_modified = dateutil.parser.parse(fig2_last_modified)

fig3_last_modified = requests.head(url_gas_fig3).headers.get('Date')
if fig3_last_modified:
    fig3_last_modified = dateutil.parser.parse(fig3_last_modified)
    
#%% Figure weekly gas consumption ------------------------------------------------------------------

# helper data

data_gas_fig1['savings_late']           = data_gas_fig1['current']
data_gas_fig1['savings_late'].loc[0:34] = data_gas_fig1['expected'].loc[0:34]

fig_gas_consumption_de = go.Figure()

fig_gas_consumption_de.add_trace(go.Scatter(x=data_gas_fig1['date'],
                                            y=data_gas_fig1['current'],
                                            name="Tatsächlich",
                                            line = dict(color='rgba(0,0,0,1)', width=2),
                                            hovertemplate = '%{y:.1f} TWh',
                                            legendgroup="consumption",
                                            legendgrouptitle_text="Verbrauch"
                                            ))

fig_gas_consumption_de.add_trace(go.Scatter(x=data_gas_fig1['date'],
                                            y=data_gas_fig1['expected'],
                                            name="Erwartet (Linear Forest)",
                                            line = dict(
                                                color='rgba(178,34,34,1)', width=2, dash = 'dot'),
                                            hovertemplate = '%{y:.1f} TWh',
                                            legendgroup="consumption"
                                            ))

fig_gas_consumption_de.add_trace(go.Scatter(name='Verhalten',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['savings_late'],
                                            mode='lines',
                                            marker=dict(color="rgba(178,34,34,.5)"),
                                            fillcolor='rgba(178,34,34,.35)',
                                            fill='tonexty',
                                            #hover_data = data_gas_fig1['savings'],
                                            customdata = data_gas_fig1['savings'],
                                            hovertemplate = "%{customdata:.1f} TWh",
                                            legendgrouptitle_text="Einsparungen",
                                            legendgroup="savings",
                                            line=dict(width=0)))

fig_gas_consumption_de.add_trace(go.Scatter(x=data_gas_fig1['date'],
                                            y=data_gas_fig1['avg'],
                                            name="Durchschnitt",
                                            line = dict(color='gray', width=1, dash = 'solid'),
                                            hovertemplate = '%{y:.1f} TWh',
                                            legendgroup="consumption_hist",
                                            legendgrouptitle_text="Verbrauch 2018-21"
                                            ))

fig_gas_consumption_de.add_trace(go.Scatter(name='Maximum)',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['max'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            fill='none',
                                            line=dict(width=0),
                                            #hovertemplate = '%{y:.0f} TWh',
                                            showlegend=False,
                                            hoverinfo='skip'))

fig_gas_consumption_de.add_trace(go.Scatter(name='Maximum',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['max'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            fill='tonexty',
                                            line=dict(width=0),
                                            hovertemplate = '%{y:.1f} TWh',
                                            showlegend=False))

fig_gas_consumption_de.add_trace(go.Scatter(name='Spannweite',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['min'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            fill='tonexty',
                                            line=dict(width=0),
                                            #hovertemplate = '%{y:.0f} TWh',
                                            #showlegend=False
                                            legendgroup="consumption_hist",
                                            hoverinfo = 'skip'))

fig_gas_consumption_de.add_trace(go.Scatter(name='Minimum',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['min'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            fill='none',
                                            line=dict(width=0),
                                            hoverinfo='skip',
                                            #hovertemplate = '%{y:.0f} TWh',
                                            showlegend=False,
                                            legendgroup="consumption_hist"
                                            ))

fig_gas_consumption_de.add_trace(go.Scatter(name='Minimum',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['min'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            fill='tonexty',
                                            line=dict(width=0),
                                            #hoverinfo='skip',
                                            hovertemplate = '%{y:.1f} TWh',
                                            showlegend=False,
                                            legendgroup="consumption_hist"
                                            ))

fig_gas_consumption_de.add_trace(go.Scatter(name='helper',
                                            x=data_gas_fig1['date'],
                                            y=data_gas_fig1['expected'],
                                            mode='lines',
                                            marker=dict(color="#444"),
                                            fillcolor='rgba(68, 68, 68, 0.1)',
                                            line=dict(width=0),
                                            #hovertemplate = '%{y:.0f} TWh',
                                            showlegend=False,
                                            hoverinfo='skip'))

#fig_gas_consumption_de.add_vrect(x0 = "2022-01-01", x1 = "2022-12-31", 
#                                 fillcolor = "white", opacity = 0,
#                                 layer = 'below', annotation_text = "2022",
#                                 annotation_position = "top right")
#
#fig_gas_consumption_de.add_vrect(x0 = "2023-01-01", x1 = "2023-12-31", 
#                                 fillcolor = "gray", opacity = 0.10,
#                                 layer = 'below', annotation_text = "2023",
#                                 annotation_position = "top left")

fig_gas_consumption_de.layout.title.text = 'Wöchentlicher Erdgasverbrauch <br><sup>Haushalte und Gewerbe</sup>'

# y-axis
fig_gas_consumption_de.layout.title.y    = 0.97
fig_gas_consumption_de.update_yaxes(title = "Terawattstunden", range = [-15,30])
fig_gas_consumption_de.layout.yaxis.tickfont.size = 11

# x-axis
fig_gas_consumption_de.update_xaxes(anchor = "free", position = .335,
                                    #categoryorder = 'array',
                                    ticklabelmode= "period",
                                    dtick="M1",
                                    ticklen=15,
                                    tickformat = '%d.%m.\n%Y',
                                    minor=dict(
                                        ticklen=2,  
                                        dtick=7*24*60*60*1000,  
                                        tick0="2022-01-03", 
                                        griddash='dot', 
                                        gridcolor='white')
                                    #tickmode = 'array',
                                    #tickvals = data_gas_fig1['date'],
                                    #ticktext = data_gas_fig1['week']
                                    )
fig_gas_consumption_de.layout.xaxis.range = ['2023-08-16','2024-06-01']
fig_gas_consumption_de.layout.xaxis.tickfont.size = 11

# Add text update
fig_gas_consumption_de.add_annotation(
    x=1,y=0,  xref="paper", yref="paper", showarrow=False,
    text="Letzes Update: "+f"{fig1_last_modified:%d.%m.%Y}",
    font = dict(size = 11, color = "lightgray"))

fig_gas_consumption_de.update_layout(
    template="simple_white",
    margin=dict(l=0, r=0, t=50, b=0, pad=0),
    legend_groupclick = "toggleitem",
    legend = {
        'traceorder':'grouped', 'orientation':'h', 'x': 0.5, 'y': 0, 
        'yanchor': 'bottom',
        'xanchor': 'center',
        'borderwidth': 1},
    xaxis_hoverformat='Woche des %d.%m.%Y',
    hovermode = "x unified",
    updatemenus=[
        dict(
            type="buttons",
            direction="left",
            active=1,
            showactive=True,
            pad={"r": 10, "t": 0},
            xanchor="left", yanchor="top", x = 0.01, y = 1.01,
            buttons=list([
                dict(
                    label="2022-23",
                    method="relayout",
                    args=[{'xaxis.range': ["2022-08-16", "2023-06-01"]}]
                ),
                dict(
                    label="2023-24",
                    method="relayout",
                    args=[{'xaxis.range': ["2023-08-16", "2024-06-01"]}]
                )
            ]),
            )
    ]
)

fig_gas_consumption_de.layout.legend.font.size = 11

# English

fig_gas_consumption_en = go.Figure(fig_gas_consumption_de)

fig_gas_consumption_en['layout']['title']['text']          = "Weekly natural gas consumption<br><sup>Residential and commercial</sup>"
fig_gas_consumption_en['layout']['yaxis']['title']['text'] = "Terawatt-hours"

fig_gas_consumption_en['data'][0]['name'] = "Observed"
fig_gas_consumption_en['data'][1]['name'] = "Expected (linear forest)"
fig_gas_consumption_en['data'][2]['name'] = "Behavior"
fig_gas_consumption_en['data'][3]['name'] = "Average"
fig_gas_consumption_en['data'][6]['name'] = "Range"

fig_gas_consumption_en['data'][0]['legendgrouptitle']['text'] = "Current consumption"
fig_gas_consumption_en['data'][2]['legendgrouptitle']['text'] = "Savings"
fig_gas_consumption_en['data'][3]['legendgrouptitle']['text'] = "Consumption 2018-21"

fig_gas_consumption_en.layout.annotations[0]["text"] = "Last update: "+f"{fig1_last_modified:%d.%m.%Y}"

fig_gas_consumption_en.update_layout(xaxis_hoverformat = 'Week %d-%m-%Y')

# French 

fig_gas_consumption_fr = go.Figure(fig_gas_consumption_de)

fig_gas_consumption_fr['layout']['title']['text']          = "Consommation hebdomadaire de gaz naturel<br><sup>Ménages et tertiaire</sup>"
fig_gas_consumption_fr['layout']['yaxis']['title']['text'] = "Térawatt-heures"

fig_gas_consumption_fr['data'][0]['name']  = "Observée"
fig_gas_consumption_fr['data'][1]['name']  = "Prévu (forêt linéaire)"
fig_gas_consumption_fr['data'][2]['name']  = "Comportementales"
fig_gas_consumption_fr['data'][3]['name']  = "Moyenne"
fig_gas_consumption_fr['data'][6]['name']  = "Gamme"

fig_gas_consumption_fr['data'][0]['legendgrouptitle']['text'] = "Consommation"
fig_gas_consumption_fr['data'][2]['legendgrouptitle']['text'] = "Économies"
fig_gas_consumption_fr['data'][3]['legendgrouptitle']['text'] = "Consommation 2018-21"

fig_gas_consumption_fr.layout.annotations[0]["text"] = "Dernière mise à jour: "+f"{fig1_last_modified:%d.%m.%Y}"

fig_gas_consumption_fr.update_layout(xaxis_hoverformat = 'Semaine %d-%m-%Y')

fig_gas_consumption_de.write_html("docs/germany/figures/fig_gas_consumption.de.html", include_plotlyjs="directory")
fig_gas_consumption_en.write_html("docs/germany/figures/fig_gas_consumption.en.html", include_plotlyjs="directory")
fig_gas_consumption_fr.write_html("docs/germany/figures/fig_gas_consumption.fr.html", include_plotlyjs="directory")

# --------------------------------------------------------------------------------------------------
#%% Figure absolute gas savings
# --------------------------------------------------------------------------------------------------

fig_gas_savings_abs_de = make_subplots(
    rows = 1, cols = 2, 
    column_widths=[0.9, 0.2],
    horizontal_spacing = 0.15,
    subplot_titles = ("Wöchentlich", "Gesamt (September-Juni)"),
    specs=[[{"secondary_y": True}, {"type": "bar"}]])

# Absolute savings
fig_gas_savings_abs_de.add_trace(go.Scatter(name='Gesamt',
                                        x=data_gas_fig2['date'],
                                        y=data_gas_fig2['total_savings_abs'],
                                        legendgrouptitle_text="Einsparungen [TWh]",
                                        line = dict(color = "black"),
                                        mode = "lines",
                                        hovertemplate = '%{y:.1f} TWh',
                                        legendgroup="savings"
                                        ),
                            secondary_y = False)

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name = "in %",
        x    = data_gas_fig2['date'],
        y    = data_gas_fig2['total_savings'],
        mode = "none",
        hovertemplate = '%{y:.1f}%',
        textfont = dict(color='rgba(178,34,34,1)',size=10),
        legendgroup="savings",
        showlegend = False,
        ),
    secondary_y = False, row = 1, col = 1)

fig_gas_savings_abs_de.add_trace(go.Bar(name='Witterung',
                                    x=data_gas_fig2['date'],
                                    y=data_gas_fig2['weather_savings_abs'],
                                    marker = dict(color='rgba(189, 195, 199, 1)'),
                                    hovertemplate = '%{y:.1f} TWh',
                                    legendgroup="savings",
                                    ),
                            secondary_y = False)

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name = "in %",
        x    = data_gas_fig2['date'],
        y    = data_gas_fig2['weather_savings'],
        mode = "none",
        hovertemplate = '%{y:.1f}%',
        textfont = dict(color='rgba(178,34,34,1)',size=10),
        legendgroup="savings",
        showlegend = False,
        ),
    secondary_y = False, row = 1, col = 1)

fig_gas_savings_abs_de.add_trace(go.Bar(name='Verhalten',
                                    x=data_gas_fig2['date'],
                                    y=data_gas_fig2['beh_savings_abs'],
                                    marker = dict(color='rgba(178,34,34,.5)'),
                                    hovertemplate = '%{y:.1f} TWh',
                                    legendgroup="savings",
                                    ),
                            secondary_y = False)

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name = "in %",
        x    = data_gas_fig2['date'],
        y    = data_gas_fig2['beh_savings'],
        mode = "none",
        hovertemplate = '%{y:.1f}%',
        textfont = dict(color='rgba(178,34,34,1)',size=10),
        legendgroup="savings",
        showlegend = False,
        ),
    secondary_y = False, row = 1, col = 1)

# Temperature
fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name='Aktuell',
        x=data_gas_fig2['date'],
        y=data_gas_fig2['temp_current'],
        mode = "lines", 
        line = dict(color = 'rgba(37, 109, 133, 1)', width = 2),
        hovertemplate = '%{y:.1f} °C',
        legendgroup="temp",
        visible='legendonly'
        ),
    secondary_y = True)

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name='2018-2021',
        x=data_gas_fig2['date'],
        y=data_gas_fig2['temp_avg'],
        legendgrouptitle_text="Durchschnittstemperatur [°C]",
        mode = "lines", 
        line = dict(color = 'rgba(103,128,159,1)', width = 2),
        hovertemplate = '%{y:.1f} °C',
        legendgroup="temp",
        visible='legendonly'
        ),
    secondary_y = True
    )

# Right figure
fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        name='Gesamt',
        x=data_gas_fig2_cum['heating_season'],
        y=data_gas_fig2_cum['total_savings_abs'],
        legendgrouptitle_text="Einsparungen [TWh]",
        line = dict(color = "black"),
        mode = "markers",
        hovertemplate = '%{y:.1f} TWh',
        legendgroup="savings",
        showlegend = False,
        ),
    secondary_y = False, row = 1, col = 2)

fig_gas_savings_abs_de.add_trace(
    go.Bar(
        name='Witterung',
        x=data_gas_fig2_cum['heating_season'],
        y=data_gas_fig2_cum['weather_savings_abs'],
        marker = dict(color='rgba(189, 195, 199, 1)'),
        hovertemplate = '%{y:.1f} TWh',
        legendgroup="savings",
        showlegend = False,
        ),
    secondary_y = False, row = 1, col = 2)

fig_gas_savings_abs_de.add_trace(
    go.Bar(
        name='Verhalten',
        x=data_gas_fig2_cum['heating_season'],
        y=data_gas_fig2_cum['beh_savings_abs'],
        marker = dict(color='rgba(178,34,34,.5)'),
        hovertemplate = '%{y:.1f} TWh',
        legendgroup="savings",
        showlegend = False
        ),
    secondary_y = False, row = 1, col = 2)

# Add labels

weather_savings_label = round(data_gas_fig2_cum['weather_savings_rel']*100,1).tolist()
weather_savings_label = [str(x) + " %" for x in weather_savings_label]

beh_savings_label = round(data_gas_fig2_cum['beh_savings_rel']*100,1).tolist()
beh_savings_label = [str(x) + " %" for x in beh_savings_label]

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        x    = data_gas_fig2_cum['heating_season'],
        y    = data_gas_fig2_cum['total_savings_abs'] - 3,
        mode = 'text',
        text = beh_savings_label,
        hoverinfo = 'skip',
        showlegend = False,
        textfont = dict(color='rgba(178,34,34,1)',size=12),
        ),
    secondary_y = False, row = 1, col = 2)

fig_gas_savings_abs_de.add_trace(
    go.Scatter(
        x    = data_gas_fig2_cum['heating_season'],
        y    = [-2,-2],
        mode = 'text',
        text = weather_savings_label,
        hoverinfo = 'skip',
        showlegend = False,
        textfont = dict(color='rgb(5,5,5,1)',size=12),
        ),
    secondary_y = False, row = 1, col = 2)

fig_gas_savings_abs_de.add_hline(
    y=0, opacity=1, line_width=1, line_dash='solid', line_color='#000000')
# fig_gas_savings_abs_de.add_hline(
#     y=-20, opacity=1, line_width=0.75, line_dash='dot', line_color='#000000',
#     annotation_text = "Einsparziel")

# y-axis
fig_gas_savings_abs_de.update_yaxes(title = "Terawattstunden", range = [-12,6], row = 1, col = 1, 
                                    secondary_y = False)
fig_gas_savings_abs_de.update_yaxes(title = "Terawattstunden",range = [-65,6], row = 1, col = 2, 
                                    secondary_y = False)
fig_gas_savings_abs_de.update_yaxes(title = "°C", range = [-25,25], secondary_y = True, 
                                    row = 1, col = 1,)

# x-axis
fig_gas_savings_abs_de.update_xaxes(anchor = "free", #position = .335,
                                    #categoryorder = 'array',
                                    ticklabelmode= "period",
                                    dtick="M1",
                                    ticklen=15,
                                    tickformat = '%d.%m.\n%Y',
                                    minor=dict(
                                        ticklen=2,  
                                        dtick=7*24*60*60*1000,  
                                        tick0="2022-09-05", 
                                        griddash='dot', 
                                        gridcolor='white')
                                    #tickmode = 'array',
                                    #tickvals = data_gas_fig1['date'],
                                    #ticktext = data_gas_fig1['week']
                                    )

#fig_gas_savings_de.update_xaxes(title = "Kalenderwoche", anchor = "free",
#                                categoryorder = 'array',
#                                tickmode = 'array',
#                                tickvals = data_gas_fig2['date'],
#                                ticktext = data_gas_fig2['week'])

fig_gas_savings_abs_de.layout.xaxis.range = ['2023-08-16','2024-06-01']
fig_gas_savings_abs_de.layout.xaxis.tickfont.size = 10

# Subplot title sizes
fig_gas_savings_abs_de.update_annotations(font_size=14)

# Add text update
fig_gas_savings_abs_de.add_annotation(
    x=1,y=-0.13,  xref="paper", yref="paper", showarrow=False,
    text="Letzes Update: "+f"{fig2_last_modified:%d.%m.%Y}",
    font = dict(size = 11, color = "lightgray"))

fig_gas_savings_abs_de.update_layout(
    title = {
        'text':'Absolute Einsparungen ggü. Durchschnitt 2018-2021<br><sup>Haushalte und Gewerbe</sup>',
        'y' : 0.97
        },
    template="simple_white",
    margin=dict(l=0, r=0, t=50, b=50, pad=0),
    xaxis_hoverformat='Woche des %d.%m.%Y',
    barmode='relative',
    hovermode = 'x unified',
    legend = {
        'traceorder':'grouped', 'orientation':'h', 
        'yanchor': 'bottom', 'xanchor': 'left', 
        'x': 0.01, 'y': 0.01, 
        'font' : {'size':11},
        'borderwidth':0.5, 'bgcolor':'white',
        'groupclick':'toggleitem'
        },
    
    # Menu dates
    updatemenus=[
        dict(
            type="buttons",
            direction="right",
            active=1,
            showactive=True,
            pad={"r": 10, "t": 0},
            xanchor="left",
            yanchor="top", x = 0.01, y = 1.01,
            buttons=list([
                dict(
                    label="2022-23",
                    method="relayout",
                    args=[{'xaxis.range': ["2022-08-16", "2023-06-01"]}]
                ),
                dict(
                    label="2023-24",
                    method="relayout",
                    args=[{'xaxis.range': ["2023-08-16", "2024-06-01"]}]
                )
            ]),
            ),
    ]
)

# Add labels

# Add labels
fig_gas_savings_abs_de.add_annotation(
    x=data_gas_fig2.dropna()["date"].iloc[-1],
    y=abs(data_gas_fig2.dropna()["total_savings_abs"].iloc[-1])*-1,
    text = f"{round(data_gas_fig2.dropna()['beh_savings_abs'].iloc[-1],1)} TWh / {round(data_gas_fig2.dropna()['beh_savings'].iloc[-1],1)} %",
    #valign = "bottom",
    align="center",
    showarrow=True,
    ax = 75,
    ay = 50,
    #xshift = 5,
    standoff  = 10, 
    arrowhead = 1,
    arrowsize = 1,
    arrowwidth=1.5,
    font = dict(size = 12, color = "black"),
    bgcolor="rgba(178,34,34,.5)",
    borderwidth=0.5,
    borderpad=2,
    )

# Add labels
fig_gas_savings_abs_de.add_annotation(
    x=data_gas_fig2.dropna()["date"].iloc[-1],
    y=(data_gas_fig2.dropna()["weather_savings_abs"].iloc[-1])/2,
    text = f"{round(data_gas_fig2.dropna()['weather_savings_abs'].iloc[-1],1)} TWh / {round(data_gas_fig2.dropna()['weather_savings'].iloc[-1],1)} %",
    #valign = "bottom",
    align="center",
    showarrow=True,
    ax = 75,
    ay = -50,
    yshift = -5,
    xshift = 2,
    standoff  = 10, 
    arrowhead = 1,
    arrowsize = 1,
    arrowwidth=1.5,
    font = dict(size = 12, color = "black"),
    bgcolor="rgba(189, 195, 199, 1)",
    borderwidth=0.5,
    borderpad=2,
    )


# English

fig_gas_savings_abs_en = go.Figure(fig_gas_savings_abs_de)

fig_gas_savings_abs_en['layout']['title']['text']           = "Absolute savings compared to 2018-2021 average<br><sup>Residential and commercial</sup>"
fig_gas_savings_abs_en['layout']['yaxis']['title']['text']  = "Terawatt-hours"
fig_gas_savings_abs_en['layout']['yaxis3']['title']['text'] = "Terawatt-hours"

fig_gas_savings_abs_en['data'][0]['name'] = "Total"
fig_gas_savings_abs_en['data'][2]['name'] = "Weather"
fig_gas_savings_abs_en['data'][4]['name'] = "Behavior"
fig_gas_savings_abs_en['data'][6]['name'] = "Current"

fig_gas_savings_abs_en['data'][0]['legendgrouptitle']['text'] = "Savings [TWh]"
fig_gas_savings_abs_en['data'][7]['legendgrouptitle']['text'] = "Average temperature [°C]"

fig_gas_savings_abs_en.layout.annotations[0]["text"] = "Weekly"
fig_gas_savings_abs_en.layout.annotations[1]["text"] = "Total (September - June)"
fig_gas_savings_abs_en.layout.annotations[2]["text"] = "Last update: "+f"{fig2_last_modified:%d.%m.%Y}"

fig_gas_savings_abs_en.update_layout(xaxis_hoverformat = 'Week %d-%m-%Y')

# French 

fig_gas_savings_abs_fr = go.Figure(fig_gas_savings_abs_de)

fig_gas_savings_abs_fr['layout']['title']['text']           = "Économies absolutes par rapport à la moyenne 2018-2021<br><sup>Ménages et tertiaire</sup>"
fig_gas_savings_abs_fr['layout']['yaxis']['title']['text']  = "Térawatt-heures"
fig_gas_savings_abs_fr['layout']['yaxis3']['title']['text'] = "Térawatt-heures"

fig_gas_savings_abs_fr['data'][0]['name'] = "Total"
fig_gas_savings_abs_fr['data'][2]['name'] = "Météo"
fig_gas_savings_abs_fr['data'][4]['name'] = "Comportement"
fig_gas_savings_abs_fr['data'][6]['name'] = "Actuel"

fig_gas_savings_abs_fr['data'][0]['legendgrouptitle']['text'] = "Économies [TWh]"
fig_gas_savings_abs_fr['data'][7]['legendgrouptitle']['text'] = "Température moyenne [°C]"

fig_gas_savings_abs_fr.layout.annotations[0]["text"] = "Hebdomadaire"
fig_gas_savings_abs_fr.layout.annotations[1]["text"] = "Total (septembre - juin)"
fig_gas_savings_abs_fr.layout.annotations[2]["text"] = "Dernière mise à jour: "+f"{fig2_last_modified:%d.%m.%Y}"

fig_gas_savings_abs_fr.update_layout(xaxis_hoverformat = 'Semaine %d-%m-%Y')


fig_gas_savings_abs_de.write_html("docs/germany/figures/fig_gas_savings_abs.de.html", include_plotlyjs="directory")
fig_gas_savings_abs_en.write_html("docs/germany/figures/fig_gas_savings_abs.en.html", include_plotlyjs="directory")
fig_gas_savings_abs_fr.write_html("docs/germany/figures/fig_gas_savings_abs.fr.html", include_plotlyjs="directory")

# --------------------------------------------------------------------------------------------------
#%% Cumulative gas consumption
# --------------------------------------------------------------------------------------------------

fig_gas_reached_de = go.Figure()

# Current consumption
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['cur_cum'],
    name="Aktuell",
    mode = "lines", 
    line = dict(color="black",dash="solid"),
    hovertemplate = '%{y:.0f} TWh',
    legendgroup           = "con"
    ))

# Last year
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['2022-23'],
    name="2022-23",
    mode = "lines", 
    line = dict(color="darkgray",dash="solid"),
    hovertemplate = '%{y:.0f} TWh',
    legendgroup           = "con",))

# Average
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['avg_cum'],
    name="Durchschnitt",
    mode = "lines", 
    line = dict(color="gray",dash="dot"),
    hovertemplate = '%{y:.0f} TWh',
    legendgroup           = "18-21",
    legendgrouptitle_text = "2018-2021",
    ))

# Minimum
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['2019-20'],
    name = "Minimum",
    line = dict(color="lightgray",width=0),
    fillcolor='rgba(189, 195, 199, 0.5)',
    showlegend = False,
    hovertemplate = '%{y:.0f} TWh',
    legendgroup           = "18-21",
    ))

# Span
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['2020-21'],
    name = "Spannweite",
    mode = 'none',
    fill='tonexty',
    fillcolor='rgba(189, 195, 199, 0.5)',
    hoverinfo='skip',
    legendgroup           = "18-21",
    ))

# Maximum
fig_gas_reached_de.add_trace(go.Scatter(
    x=data_gas_fig3['date'],
    y=data_gas_fig3['2020-21'],
    name = "Maximum",
    line = dict(color="lightgray",width=0),
    fillcolor='rgba(189, 195, 199, 0.5)',
    showlegend = False,
    hovertemplate = '%{y:.0f} TWh',
    legendgroup           = "18-21",
    ))

# Add text update
fig_gas_reached_de.add_annotation(
    x=1,y=-0.20,  xref="paper", yref="paper", showarrow=False,
    text="Letzes Update: "+f"{fig3_last_modified:%d.%m.%Y}",
    font = dict(size = 11, color = "lightgray"))

# y-axis
fig_gas_reached_de.update_yaxes(title = "Terawattstunden", range = [0,400])

# x-axis
fig_gas_reached_de.layout.xaxis.tickfont.size = 10
fig_gas_reached_de.layout.xaxis.range = ['2023-09-01','2024-06-01']
fig_gas_reached_de.update_xaxes(anchor = "free", #position = .335,
                                    #categoryorder = 'array',
                                    ticklabelmode= "period",
                                    dtick="M1",
                                    ticklen=15,
                                    tickformat = '%d.%m.\n%Y',
                                    minor=dict(
                                        ticklen=2,  
                                        dtick=7*24*60*60*1000,  
                                        tick0="2022-09-05", 
                                        griddash='dot', 
                                        gridcolor='white')
                                    #tickmode = 'array',
                                    #tickvals = data_gas_fig1['date'],
                                    #ticktext = data_gas_fig1['week']
                                    )

fig_gas_reached_de.update_layout(
    title = {
        'text':'Kumulativer Verbrauch (September - Juni)<br><sup>Haushalte und Gewerbe</sup>',
        'y':0.97},
    template="simple_white",
    margin=dict(l=0, r=0, t=50, b=0, pad=0),
    xaxis_hoverformat='Woche des %d.%m.%Y',
    hovermode = 'x unified',
    legend = {
        'y': 1, 'x': 0.01, 
        'borderwidth':1,
        'groupclick':'toggleitem',
        'traceorder':'grouped'
    }
    )


# English

fig_gas_reached_en = go.Figure(fig_gas_reached_de)

fig_gas_reached_en['layout']['title']['text']          = "Cumulative consumption (September - June)<br><sup>Residential and commercial</sup>"
fig_gas_reached_en['layout']['yaxis']['title']['text'] = "Terawatt-hours"
fig_gas_reached_en['layout']['xaxis']['title']['text'] = "Calendar week"

fig_gas_reached_en['data'][0]['name'] = "Current"
fig_gas_reached_en['data'][1]['name'] = "2022-23"
fig_gas_reached_en['data'][2]['name'] = "Average"
fig_gas_reached_en['data'][4]['name'] = "Range"

fig_gas_reached_en.layout.annotations[0]["text"] = "Last update: "+f"{fig3_last_modified:%d.%m.%Y}"

fig_gas_reached_en.update_layout(xaxis_hoverformat = 'Week %d-%m-%Y')

# French 

fig_gas_reached_fr = go.Figure(fig_gas_reached_de)

fig_gas_reached_fr['layout']['title']['text']          = "Consommation cumulée<br><sup>Ménages et tertiaire</sup>"
fig_gas_reached_fr['layout']['yaxis']['title']['text'] = "Térawatt-heures"
fig_gas_reached_fr['layout']['xaxis']['title']['text'] = "Semaine"

fig_gas_reached_fr['data'][0]['name'] = "Actuel"
fig_gas_reached_fr['data'][1]['name'] = "2022-23"
fig_gas_reached_fr['data'][2]['name'] = "Moyenne"
fig_gas_reached_fr['data'][4]['name'] = "Envergure"

fig_gas_reached_fr.layout.annotations[0]["text"] = "Dernière mise à jour: "+f"{fig3_last_modified:%d.%m.%Y}"

fig_gas_reached_fr.update_layout(xaxis_hoverformat = 'Semaine %d-%m-%Y')

# Save

fig_gas_reached_de.write_html("docs/germany/figures/fig_gas_reached.de.html", include_plotlyjs="directory")
fig_gas_reached_en.write_html("docs/germany/figures/fig_gas_reached.en.html", include_plotlyjs="directory")
fig_gas_reached_fr.write_html("docs/germany/figures/fig_gas_reached.fr.html", include_plotlyjs="directory")

####################################################################################################
# Electricity prices ###############################################################################
####################################################################################################
















#%% ################################################################################################
### PV DEEP DIVE ###################################################################################
####################################################################################################

# Import MaStR and other pv data
mastr_data = pd.read_csv("docs/germany/data/pv_mastr_data.csv")
mastr_data = mastr_data.assign(date=pd.to_datetime(dict(year=mastr_data.year,month=mastr_data.month,day= 1)))

bnetza_stock_data = pd.read_csv("docs/germany/data/pv_bnetza_stock_data.csv")
bnetza_eeg_data   = pd.read_csv("docs/germany/data/pv_bnetza_eeg_data.csv")

potential_data = pd.read_csv("docs/germany/data/potential_data.csv").set_index("bundesland")

#pal = [
# "#591c19", 
# "#9b332b", 
# "#b64f32",
# "#d39a2d",
# "#f7c267",
# "#b9b9b8",
# "#8b8b99",
# "#5d6174",
# "#41485f",
# "#262d42",
# "rgba(0,55,0,0.7)"]
pal = [
    "#5EAAA8",  # Teal
    "#E87352",  # Warm Coral
    "#AAB593",  # Dusty Green
    "#FDBA8F",  # Apricot
    "#FFC94D",  # Sunflower Yellow
    "#A9D9C7",  # Aqua
    "#F7D394",  # Pale Gold
    "#FCD9A0",  # Light Amber
    "#9FC68E",  # Moss Green
    "#E3AE8A",  # Terra Cotta
    "#FFB16D",  # Melon
    "#8E9AA5",  # Slate Blue
    "#C2877B",  # Rusty Brown
    "#E7998C",  # Coral Pink
    "#7BA5C8",  # Cornflower Blue
    "#D5C2C3",  # Rose Taupe
]

#pal = [
#    colors.standard["first"],
#    colors.standard["second"],
#    colors.standard["third"],
#    colors.standard["fourth"]
#

# Prepare MaStR functions

def create_pv_type_split(mastr_dta,pv_dta,start_date,agee_data,relative,annual,legend_up):
    # Basic filtering and aggregation
    tp = mastr_dta.query(f"date>='{start_date}'")
    tp['reductions'] = tp['reductions'].fillna(0)
    tp['net_additions'] = tp['additions'] - tp['reductions']
    tp = tp.groupby(['date','Lage']).agg({'net_additions':'sum'}).reset_index()
    tp = tp.query(f"date<='{date.today()}'")

    if annual:
        tp = mastr_dta.query(f"date>='{start_date}'")
        tp['reductions'] = tp['reductions'].fillna(0)
        tp['net_additions'] = tp['additions'] - tp['reductions']
        tp = tp.groupby(['year','Lage']).agg({'net_additions':'sum'}).reset_index()
        tp = tp.rename(columns={'year':'date'})

    tp = tp.assign(net_additions = tp.net_additions/1e3)
    # Make wide
    tp = tp.pivot(index = "date",columns="Lage",values = "net_additions")
    tp['Bauliche Anlagen'] = tp['Bauliche Anlagen (Hausdach, Gebäude und Fassade)'] + tp['Bauliche Anlagen (Sonstige)']
    tp['Andere'] = tp['Gewässer']+tp['Großparkplatz']
    tp = tp.drop(['Bauliche Anlagen (Sonstige)','Bauliche Anlagen (Hausdach, Gebäude und Fassade)','Gewässer','Großparkplatz'],axis=1)
    
    iter_arr = tp.reset_index().columns.drop(['date'])
    tp = tp.reset_index()
    if agee_data:
        pv_dta = pv_dta[['date',"add_mw"]]
        tp = tp.merge(pv_data,on='date',how='left')

    if relative:
        tp["total"] = tp[iter_arr].sum(axis=1)
        tp[iter_arr] = tp[iter_arr].apply(lambda x: x/tp["total"]*100)
    
    # Create figure
    fig = go.Figure()
    i = 0
    hov_temp = '%{y:.1f}%' if relative else '%{y:.1f}'
    
    iter_arr = [
        "Bauliche Anlagen",
        "Freifläche",
        "Steckerfertige Solaranlage (sog. Balkonkraftwerk)",
        "Andere",
    ]
    
    for series in iter_arr:
        fig.add_trace(go.Bar(
            x = tp.date, y = tp[series],
            name = series,
            marker_color = pal[i],
            hovertemplate = hov_temp,
            #offsetgroup=0
            ))
        i=i+1
    
    if agee_data:
        fig.add_trace(go.Scatter(x=tp.date,y=tp["add_mw"],#
                            name = "Daten der AGEE Stat",marker_color="black"))
    if relative: 
        fig.update_yaxes(title="Prozent (%)")
        fig.update_layout(title="Anteiliger Netto-Zubau nach Anlagentyp")
    else:
        fig.update_yaxes(title="Megawatt")
        fig.update_layout(title="Absoluter Netto-Zubau nach Anlagentyp")
    
    if legend_up == True:
        x_pos  = 0.01
        y_pos  = 1
        y_anc  = "top"
        border = 1
    else:
        x_pos  = 0
        y_pos  = -0.4
        y_anc  = "bottom"
        border = 0
    
    fig.update_layout(
        template = "simple_white",
        uniformtext_mode = 'hide',
        font = dict(
            family = "'sans-serif','arial'",
            size = 12,
            color='#000000'),
        barmode = 'stack',
        legend = {
            "orientation"    :"v",
            "x"              : x_pos,
            "y"              : y_pos,
            "yanchor"        : y_anc,
            "xanchor"        : "left",
            "borderwidth"    : border,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
        },
        margin=dict(l=0, r=0, t=40, b=0, pad=0),
        xaxis_hoverformat='%b %Y',
        hovermode = "x unified"
        )

    return fig

def create_pv_size_split(mastr_dta,pv_dta,start_date):
    # Basic filtering and aggregation
    tp = mastr_dta.query(f"date>='{start_date}'")
    tp['reductions'] = tp['reductions'].fillna(0)
    tp['net_additions'] = tp['additions'] - tp['reductions']
    tp = tp.groupby(['date','Lage','bins']).agg({'net_additions':'sum'}).reset_index()
    curr_mon = date.today().month
    mon = curr_mon - 1 if curr_mon > 1 else 11
    yr = date.today().year if mon < 11 else date.today().year - 1
    max_date = date(yr,mon+1,1)
    tp = tp.query(f"date<='{date.today()}'")
    tp = tp.assign(net_additions = tp.net_additions/1e3)

    # recast bin definitions 
    tp['bins'].replace(
        {
            "(0.0, 10.0]"           : "0 - 10 kW",
            "(10.0, 25.0]"          : "10 - 25 kW",
            "(25.0, 100.0]"         : "25 - 100 kW",
            "(100.0, 500.0]"        : "100 - 500 kW" ,
            "(500.0, 1000.0]"       : "500 - 1000 kW" ,
            "(1000.0, 5000.0]"      : "1 - 5 MW" ,
            "(5000.0, 10000.0]"     : "5 - 10 MW" ,
            "(10000.0, 20000.0]"    : "10 - 20 MW" ,
            "(20000.0, 50000.0]"    : "20 - 50 MW" ,
            "(50000.0, 10000000.0]" : "> 50 MW" },
        inplace = True)
    
    tp['bau_bins'] = np.select([
        (tp['bins'] == "1 - 5 MW"),
        (tp['bins'] == "5 - 10 MW"), 
        (tp['bins'] == "10 - 20 MW"),
        (tp['bins'] == "20 - 50 MW"),
        (tp['bins'] == "> 50 MW")],[
            "> 1 MW",
            "> 1 MW",
            "> 1 MW",
            "> 1 MW",
            "> 1 MW"],
        default=tp['bins']
                        )
    
    tp['ground_bins'] = np.select([
        (tp['bins'] == "0 - 10 kW"),
        (tp['bins'] == "10 - 25 kW"), 
        (tp['bins'] == "25 - 100 kW"),
        (tp['bins'] == "100 - 500 kW"),
        (tp['bins'] == "500 - 1000 kW")],[
            "0 - 1 MW",
            "0 - 1 MW",
            "0 - 1 MW",
            "0 - 1 MW",
            "0 - 1 MW"],
        default=tp['bins']
                        )
    
    tp = tp.pivot(index=['date','bau_bins','ground_bins'],columns="Lage",values='net_additions')
    tp['Bauliche Anlagen'] = tp['Bauliche Anlagen (Hausdach, Gebäude und Fassade)']+ tp['Bauliche Anlagen (Sonstige)']
    tp = tp.filter(['Bauliche Anlagen','Freifläche'],axis=1)
    bau = tp.reset_index().groupby(['date','bau_bins']).agg({'Bauliche Anlagen':'sum'}).reset_index(drop=False).pivot(index="date",columns="bau_bins",values="Bauliche Anlagen").reset_index()
    fr = tp.reset_index().groupby(['date','ground_bins']).agg({'Freifläche':'sum'}).reset_index().pivot(index="date",columns="ground_bins",values="Freifläche").reset_index()
        
    bau_cols = [bau.columns.tolist()[i] for i in [1,2,4,3,5,6]]
    fr_cols = [fr.columns.tolist()[i] for i in [1,2,5,3,4,6]]

    fig = go.Figure()
    i = 0
    for k in bau_cols:
        fig.add_trace(go.Bar(
            x = bau['date'], y = bau[k], name = k, marker_color= pal[i],
            hovertemplate = '%{y:.1f}', 
            legendgroup = "bau",
            legendgrouptitle_text = "Bauliche Anlagen"))
        i = i+1
    for l in fr_cols:
        fig.add_trace(go.Bar(
            x = fr['date'], y = fr[l], name = l, marker_color=pal[i], marker_pattern_shape="x",
            hovertemplate = '%{y:.1f}', legendgroup = "fr", legendgrouptitle_text = "Freifläche"))
        i = i+1
    fig.update_yaxes(title="Megawatt")
    fig.update_xaxes(range = ["2017","2027"])
    fig.update_layout(
        title            = "Netto-Zubau nach Anlagengröße",
        template         = "simple_white",
        uniformtext_mode = 'hide',
        barmode='stack',
        font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        legend = {
            "orientation"     :"v",
            "x"              : 1,
            "y"              : 0.5,
            "yanchor"        : "middle",
            "xanchor"        : "right",
            "borderwidth"    : 0,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "traceorder"     : "grouped+reversed",
            "groupclick"     : "toggleitem"},
        margin=dict(l=0, r=0, t=50, b=0, pad=0),
        xaxis_hoverformat='%b %Y',
        hovermode = "x unified",
        bargap=0,bargroupgap=0.05)

    return fig

def create_pv_maps(mastr_data,potential_data):
    tp = mastr_data
    tp['reductions'] = tp['reductions'].fillna(0)
    tp['net_additions'] = tp['additions'] - tp['reductions']
    curr_mon = date.today().month
    mon = curr_mon -1 if curr_mon -1 > 0 else 11
    yr = date.today().year if mon < 11 else date.today().year - 1
    max_date = date(yr,mon+1,1)
    min_date = date(yr-1,mon+1,1) if mon < 12 else date(yr,1,1)
    tp_total = tp.query(f"date<'{max_date}'")
    tp_12m = tp.query(f"date<'{max_date}'").query(f"date>='{min_date}'")

    #tp = tp.groupby(['date','Bundesland']).agg({"net_additions":"sum"}).reset_index()#.pivot(index='date',columns='Bundesland',values='net_additions')
    tp_total = tp_total.groupby(['Bundesland',"Lage"]).agg({"net_additions":'sum'})
    tp_12m = tp_12m.groupby(['Bundesland',"Lage"]).agg({"net_additions":'sum'})
    tp = tp_total.merge(tp_12m,left_index=True,right_index=True,suffixes=["_total","_12m"])
    tp = tp.reset_index()
    tp = tp.melt(value_vars=["net_additions_total","net_additions_12m"],id_vars=["Bundesland","Lage"]).pivot(index=["Bundesland","variable"],columns="Lage")
    tp.columns = tp.columns.get_level_values(1)
    tp = tp.reset_index()

    tp["Bauliche Anlagen"] = tp["Bauliche Anlagen (Hausdach, Gebäude und Fassade)"] + tp["Bauliche Anlagen (Sonstige)"]
    tp["Total"] = tp["Bauliche Anlagen"] + tp["Freifläche"]
    tp = tp.set_index(["Bundesland","variable"])
    tp = tp[["Total","Bauliche Anlagen","Freifläche"]]

    potential_data.loc["Bremen","rt capa ARIADNE"] = tp.loc[("Bremen","net_additions_total"),"Bauliche Anlagen"]/1e6
    potential_data["total_capa"] = potential_data["gm capa ARIADNE"] + potential_data["rt capa ARIADNE"]
    
    tp = tp.reset_index().set_index("Bundesland").merge(potential_data,left_index=True,right_index=True,how="left").reset_index()
    tp = tp.rename(columns={"index":"Bundesland"})

    tp = tp.query("Bundesland!='Ausschließliche Wirtschaftszone'")
    tp["Bauliche Anlagen"] = tp["Bauliche Anlagen"]/(tp["rt capa ARIADNE"]*1e6)
    tp["Freifläche"] =  tp["Freifläche"]/(tp["gm capa ARIADNE"]*1e6)
    tp["Total"] = tp["Total"]/(tp["total_capa"]*1e6)
    tp = tp[["Bundesland","variable","Total","Bauliche Anlagen","Freifläche"]]
    tp = tp.rename(columns={"variable":"type"})
    tp.replace([np.inf,-np.inf],0,inplace=True)
    tp[["Total","Bauliche Anlagen","Freifläche"]] = tp[["Total","Bauliche Anlagen","Freifläche"]]*100
    #tp = tp.melt(value_vars=["Total","Bauliche Anlagen","Freifläche"],id_vars=["Bundesland","type"])
    tp_total = tp.reset_index().query("type=='net_additions_total'")
    tp_12m = tp.reset_index().query("type=='net_additions_12m'")


    with urlopen("https://raw.githubusercontent.com/isellsoap/deutschlandGeoJSON/main/2_bundeslaender/1_sehr_hoch.geo.json") as response:
        states = json.load(response)
    
    fig = make_subplots(cols=2,rows=1,specs=[[{"type":"choropleth"},{"type":"choropleth"}],],
                        subplot_titles=[
                            "Bestand <br><sup>relativ zum Potenzial [%]</sup>",
                            "Ausbau in den letzten 12 Monaten <br><sup>relativ zum Potenzial [%]</sup>"],
                        horizontal_spacing=0)
    fig.add_trace(go.Choropleth(
        locations = tp_total["Bundesland"],
        featureidkey = 'properties.name',
        geojson=states,
        z=tp_total['Total'],
        colorscale = "peach",
        #colorbar_title = "Bestand relativ zum Potenzial, %",
        colorbar_x = 0.45,
        #hoverinfo= "location+z",
        hovertemplate = "<extra> %{location} </extra> %{z:.2f} %"
        #projection = "mercator"
        #hoverlabel = tp_total["Bundesland"]
    ),1,1)
    fig.add_trace(go.Choropleth(
    locations = tp_total["Bundesland"],
    featureidkey = 'properties.name',
    geojson=states,
    z=tp_12m['Total'],
    colorscale = "peach",
    #colorbar_title = "Ausbau relativ zum Potenzial, %",
    colorbar_x = 0.95,
    hovertemplate = "<extra> %{location} </extra> %{z:.2f} %"
    #hoverinfo= "location+z"
    #projection = "mercator"
    ),1,2)
    fig.update_geos(
        fitbounds="locations",
        visible=False
        )
    fig.update_geos(projection_type="mercator")
    fig.update_coloraxes(colorbar = dict(orientation="h"),colorbar_x = 0)
    
    fig.update_layout(
        title = {
            "text" : "Gesamtbestand und Trend nach Bundesland, normiert mit Potenzialen",
            "y"    : 0.99,},
        margin=dict(l=0, r=0, t=75, b=0, pad=0),
        font = dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        ) 

    return fig

def create_pv_eeg(eeg_data):
    tp = eeg_data
    tp['date'] = pd.to_datetime(tp['Monat'])
    tp = tp.groupby(['Lage','Subventionstyp']).agg({"Netto":"sum"}).reset_index().pivot(index="Subventionstyp",columns="Lage",values="Netto").reset_index().fillna(0)
    
    fig = go.Figure()
    fig.add_trace(go.Bar(x=tp["Subventionstyp"],y=tp["Baul. Anlage"],marker_color=pal[-1],name="Baul. Anlage",hovertemplate = "%{y:.1f}"))
    fig.add_trace(go.Bar(x=tp["Subventionstyp"],y=tp["Freifläche"],marker_color=pal[8],name="Freifläche",hovertemplate = "%{y:.1f}"))
    fig.add_trace(go.Bar(x=tp["Subventionstyp"],y=tp["Mieterstrom"],marker_color=pal[3],name="Mieterstrom",hovertemplate = "%{y:.1f}"))
    
    fig.update_yaxes(title="Megawatt")
    fig.update_layout(
        title            = "Netto-Zubau in den letzten 12 Monaten nach EEG-Förderungsart",
        template         = "simple_white",
        barmode          = 'relative',
        uniformtext_mode = 'hide',
        font = dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        legend = {
            "orientation"    :"v",
            "x"              : 0.01,
            "y"              : 1,
            "yanchor"        : "top",
            "xanchor"        : "left",
            "borderwidth"    : 1,
            "font"           : {"size": 11},
            "grouptitlefont" : {"size": 11},
            "traceorder"     : "grouped",
            "groupclick"     : "toggleitem",},
        margin=dict(l=0, r=0, t=50, b=0, pad=0),
        xaxis_hoverformat='%b %Y',
        hovermode = "x")
    return fig


def build_butterfly_chart(mastr_data,potential_data):
    # compute net reductions
    tp = mastr_data
    tp['reductions'] = tp['reductions'].fillna(0)
    tp['net_additions'] = tp['additions'] - tp['reductions']
    # cutoff end of last month
    curr_mon = date.today().month
    mon = curr_mon -1 if curr_mon -1 > 0 else 11 + (curr_mon -1)
    yr = date.today().year if mon < 11 else date.today().year - 1
    max_date = date(yr,mon+1,1) if mon<11 else date(yr+1,1,1)
    min_date = date(yr-1,mon+1,1) if mon < 11 else date(yr,1,1)
    tp = tp.query(f"date<'{max_date}'")
    tp_gm = tp.query("Lage=='Freifläche'")
    tp_gm = tp_gm.groupby(['Bundesland']).agg({'net_additions':"sum"})/1e6
    #tp = tp.query(f"date>='{min_date}'")
    tp_rt = tp.query("Lage=='Bauliche Anlagen (Hausdach, Gebäude und Fassade)' or Lage=='Bauliche Anlagen (Sonstige)' ")
    tp_rt = tp_rt.groupby(['Bundesland']).agg({'net_additions':"sum"})/1e6
    tp = pd.merge(tp_gm,tp_rt,left_index=True,right_index=True,suffixes=('_gm','_rt'))
    tp["total"] = tp["net_additions_gm"]+tp["net_additions_rt"]
    tp = tp.query('index!="Ausschließliche Wirtschaftszone"')

    # compute capacity potential (conversion factor in MW/ha)
    tp["potential_gm"] = potential_data["gm capa ARIADNE"]
    tp["potential_rt"] = potential_data["rt capa ARIADNE"]

    tp['build_out_gm'] = tp["net_additions_gm"]/tp["potential_gm"]
    tp['build_out_rt'] = tp["net_additions_rt"]/tp["potential_rt"]
    
    tp = tp.sort_values(by=["total"])
    #tp2 = tp.sort_values(by=["net_additions_rt"])
        
    fig = make_subplots(rows=1,cols=3,
                        shared_xaxes=False,
                        shared_yaxes=True,
                        horizontal_spacing=0.00,
                        subplot_titles=["","","",""])

    fig.add_trace(go.Bar(
        y = tp.index, x = tp["net_additions_rt"],
        name = "Auf/an baulichen Anlagen",
        marker_color = colors.standard["first"],
        orientation = "h",
        offsetgroup = 1,
#        marker_pattern_shape = "+",
        legendgroup = "rooftop",
        hovertemplate = "%{x:.2f} GW"),
    row = 1, col = 1)
    
    fig.add_trace(go.Bar(
        y = tp.index, x = tp["net_additions_gm"],
        name = "Freifläche",
        marker_color = colors.standard["second"],
        orientation = "h",
        offsetgroup = 1,
        legendgroup="area",
        hovertemplate="%{x:.2f} GW"),
    row = 1, col = 1)

    fig.add_trace(go.Bar(
        y = tp.index, x = tp["build_out_rt"],
        name = "Bauliche Anlage",
        marker_color = colors.standard["first"],
        orientation='h',
        offsetgroup = 1,
        legendgroup = "rooftop",
        showlegend = False),
    row = 1, col = 2)
    
    fig.add_trace(go.Bar(
        y = tp.index, x = tp["build_out_gm"],
        name = "Freifläche",
        marker_color = colors.standard["second"],
        showlegend = False,
        offsetgroup = 1,
        #hovertemplate='%{x:.0f%}',
        orientation='h'),
        row = 1, col = 3)
    
    fig.update_xaxes(title_text=None,row=1,col=1, range=[0,22])
    fig.update_xaxes(title_text=None,row=1,col=2,range=[0.29,0.00],tickformat = ',.0%')
    fig.update_xaxes(title_text=None,row=1,col=3,range=[0.00,0.29],tickformat = ',.0%')
    fig.update_yaxes(dtick=1)
    fig.update_yaxes(visible=False,row=1,col=2)
    fig.update_yaxes(visible=False,row=1,col=3)
    
    fig.update_xaxes(showgrid=True, gridwidth=0.1, gridcolor='Lightgray')
    
    fig.add_annotation(
        text = "Installierte Leistung [GW]", x = 0.5, y = 1.08, col = 1, row = 1, 
        xref = "x domain", yref = "y domain", xanchor = "center",
        showarrow = False)
    fig.add_annotation(
        text = "Installierte Leistung relativ zu Potenzial [%]", x = 0, y = 1.08, col = 3, row = 1, 
        xref = "x domain", yref = "y domain", xanchor = "center",
        showarrow = False)
    
    fig.update_layout(
        title = {"text": "Photovoltaik in den Bundesländern", "y": 0.99},
        margin = dict(l=0,r=0,b=0,t=60,pad=0),
        font = dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        hovermode = "y unified",
        barmode = "stack",
        template = "simple_white",
        legend={"yanchor":"bottom","xanchor":"left","y":0.05,"x":0.05,})
    
    return fig

#%% Create Figures

# German

pv_type_split_plt      = create_pv_type_split(mastr_data,pv_data,"01-01-2017",False,False,False,True)
pv_type_annual_rel_plt = create_pv_type_split(mastr_data,pv_data,"01-01-2000",False,True,True,False)
pv_size_split_plt      = create_pv_size_split(mastr_data,pv_data,"01-01-2017")
pv_eeg_plt             = create_pv_eeg(bnetza_eeg_data)
pv_map                 = create_pv_maps(mastr_data,potential_data)
fig_pv_butterfly_DE    = build_butterfly_chart(mastr_data, potential_data)

# English

pv_type_split_plt_EN                                 = copy.deepcopy(pv_type_split_plt)
pv_type_split_plt_EN['layout']['title']['text']      = "Absolute net additions by installation type"
pv_type_split_plt_EN['layout']['yaxis']['title']['text']      = "Megawatts"
type_names                                           = ["Buildings","Ground-mounted","Plug-in","Other"]
for i in range(4):
    pv_type_split_plt_EN['data'][i]['name']          = type_names[i]

pv_type_annual_rel_plt_EN                            = copy.deepcopy(pv_type_annual_rel_plt)
pv_type_annual_rel_plt_EN['layout']['title']['text'] = "Relative net additions by installation type" 
pv_type_annual_rel_plt_EN['layout']['yaxis']['title']['text'] = "Percent (%)"
for i in range(4):
    pv_type_annual_rel_plt_EN['data'][i]['name']     = type_names[i]

pv_size_split_plt_EN                                 = copy.deepcopy(pv_size_split_plt)
pv_size_split_plt_EN['layout']['title']['text']      = "Net additions by installation size"
pv_size_split_plt_EN['layout']['yaxis']['title']['text']      = "Megawatts"
for i in range(11):
    pv_size_split_plt_EN['data'][i]['legendgrouptitle']['text'] = 'Buildings' if i<8 else 'Ground-mounted'
    pv_size_split_plt_EN['data'][i]['name'] = pv_size_split_plt_EN['data'][i]['name'].replace("zwischen","between").replace("und","and").replace("größer als","larger than")

pv_eeg_plt_EN = copy.deepcopy(pv_eeg_plt)
pv_eeg_plt_EN['layout']['title']['text'] = 'Net additions in the last 12 months by subsidy type'
pv_eeg_plt_EN['layout']['yaxis']['title']['text'] = "Megawatts"
typ_names = ['Buildings','Ground-mounted','Mieterstrom (tenant electricity)']
for i in range(3):
    pv_eeg_plt_EN['data'][i]['x'] = ['EEG auction','Feed-in tariff/premium','Unsubsidised*']
    pv_eeg_plt_EN['data'][i]['name'] = typ_names[i]

pv_map_EN = copy.deepcopy(pv_map)
pv_map_EN['layout']['title']['text'] = "Total capacity and trend by German state relative to potential"
pv_map_EN['layout']['annotations'][0]['text'] = 'Total capacity <br><sup>relative to potential [%]</sup>'
pv_map_EN['layout']['annotations'][1]['text'] = 'Net additions in the last 12 months <br><sup>relative to potential [%]</sup>'
#pv_map_EN['data'][0]['geojson']['features'][0]['properties']['name']
states = ['Baden-Württemberg','Bavaria','Berlin','Brandenburg','Bremen','Hamburg',"Hesse","Mecklenburg-West Pomerania","Lower Saxony",'North Rhine-Westphalia',"Rhineland-Palatinate", "Saarland","Saxony-Anhalt","Saxony","Schleswig-Holstein","Thuringia"]
pv_map_EN['data'][0]['locations'] = states
pv_map_EN['data'][1]['locations'] = states
for i in range(16):
    pv_map_EN['data'][0]['geojson']['features'][i]['properties']['name'] = states[i]
    pv_map_EN['data'][1]['geojson']['features'][i]['properties']['name'] = states[i]

states[12] = "Saxony"
states[13] = "Saxony-Anhalt"
but_df = pd.DataFrame({'ordered':fig_pv_butterfly_DE['data'][3]['y']}).sort_values(['ordered'])
but_df['english'] = states
states = pd.DataFrame({'sorted':fig_pv_butterfly_DE['data'][3]['y']}).merge(but_df,how="left",left_on="sorted",right_on="ordered")['english']
states = np.array(states)

fig_pv_butterfly_EN = copy.deepcopy(fig_pv_butterfly_DE)
fig_pv_butterfly_EN['layout']['title']['text'] = "PV capacity by German state"
for i in range(4):
    fig_pv_butterfly_EN['data'][i]['y'] = states
fig_pv_butterfly_EN['layout']['annotations'][0]['text'] = "Installed [GW]"
fig_pv_butterfly_EN['layout']['annotations'][1]['text'] = "Installed relative to Potential [%]"
fig_pv_butterfly_EN['data'][0]['name'] = "Buildings"
fig_pv_butterfly_EN['data'][1]['name'] = "Ground-mounted"

# Create French figures

pv_type_split_plt_FR                                 = copy.deepcopy(pv_type_split_plt)
pv_type_split_plt_FR['layout']['title']['text']      = "Ajouts nets absolus par type d'installation"
pv_type_split_plt_FR['layout']['yaxis']['title']['text']      = "Mégawatts"
type_names                                           = ["Bâtiments","Montage au sol","Plug-in","Autres"]
for i in range(4):
    pv_type_split_plt_FR['data'][i]['name']          = type_names[i]

pv_type_annual_rel_plt_FR                            = copy.deepcopy(pv_type_annual_rel_plt)
pv_type_annual_rel_plt_FR['layout']['title']['text'] = "Ajouts nets relatifs par type d'installation" 
pv_type_annual_rel_plt_FR['layout']['yaxis']['title']['text'] = "Pour cent (%)"
for i in range(4):
    pv_type_annual_rel_plt_FR['data'][i]['name']     = type_names[i]

pv_size_split_plt_FR                                 = copy.deepcopy(pv_size_split_plt)
pv_size_split_plt_FR['layout']['title']['text']      = "Ajouts nets par taille d'installation"
pv_size_split_plt_FR['layout']['yaxis']['title']['text']      = "Mégawatts"
for i in range(11):
    pv_size_split_plt_FR['data'][i]['legendgrouptitle']['text'] = 'Bâtiments' if i<8 else 'Montage au sol'
    pv_size_split_plt_FR['data'][i]['name'] = pv_size_split_plt_FR['data'][i]['name'].replace("zwischen","entre").replace("und","et").replace("größer als","plus grande que")

pv_eeg_plt_FR = copy.deepcopy(pv_eeg_plt)
pv_eeg_plt_FR['layout']['title']['text'] = 'Ajouts nets au cours des 12 derniers mois par type de subvention'
pv_eeg_plt_FR['layout']['yaxis']['title']['text'] = "Mégawatts"
typ_names = ["Bâtiments","Montage au sol",'Mieterstrom (Électricité locative)']
for i in range(3):
    pv_eeg_plt_FR['data'][i]['x'] = ['Enchères EEG','tarif/prime de rachat','non subventionné*']
    pv_eeg_plt_FR['data'][i]['name'] = typ_names[i]

pv_map_FR = copy.deepcopy(pv_map)
pv_map_FR['layout']['title']['text'] = "Capacité totale et évolution par État allemand par rapport au potentiel"
pv_map_FR['layout']['annotations'][0]['text'] = 'Capacité totale <br><sup>par rapport au potentiel [%]</sup>'
pv_map_FR['layout']['annotations'][1]['text'] = 'Ajouts nets au cours des 12 derniers mois <br><sup>par rapport au potentiel [%]</sup>'
#pv_map_FR['data'][0]['geojson']['features'][0]['properties']['name']
states = ['le Bade-Wurtemberg','la Bavière','Berlin','le Brandebourg','Brême','Hambourg',"la Hesse","Mecklembourg-Poméranie-Occidentale","la Basse-Saxe",'la Rhénanie-du-Nord-Westphalie',"la Rhénanie-Palatinat", "la Sarre","la Saxe-Anhalt","la Saxe","le Schleswig-Holstein","la Thuringe"]
pv_map_FR['data'][0]['locations'] = states
pv_map_FR['data'][1]['locations'] = states
for i in range(16):
    pv_map_FR['data'][0]['geojson']['features'][i]['properties']['name'] = states[i]
    pv_map_FR['data'][1]['geojson']['features'][i]['properties']['name'] = states[i]

states[12] = "la Saxe"
states[13] = "la Saxe-Anhalt"
but_df = pd.DataFrame({'ordered':fig_pv_butterfly_DE['data'][3]['y']}).sort_values(['ordered'])
but_df['english'] = states
states = pd.DataFrame({'sorted':fig_pv_butterfly_DE['data'][3]['y']}).merge(but_df,how="left",left_on="sorted",right_on="ordered")['english']
states = np.array(states)


fig_pv_butterfly_FR = copy.deepcopy(fig_pv_butterfly_DE)
fig_pv_butterfly_FR['layout']['title']['text'] = "Capacité photovoltaïque par État allemand"
for i in range(4):
    fig_pv_butterfly_FR['data'][i]['y'] = states
fig_pv_butterfly_FR['layout']['annotations'][0]['text'] = "Installé  [GW]"
fig_pv_butterfly_FR['layout']['annotations'][1]['text'] = "Installé par rapport au potentiel [%]"
fig_pv_butterfly_FR['data'][0]['name'] = "Bâtiments"
fig_pv_butterfly_FR['data'][1]['name'] = "Montage au sol"

# Write to html

pv_type_split_plt.write_html("docs/germany/figures/pv_type_split_plt.de.html", include_plotlyjs="directory")
pv_type_split_plt_EN.write_html("docs/germany/figures/pv_type_split_plt.en.html", include_plotlyjs="directory")
pv_type_split_plt_FR.write_html("docs/germany/figures/pv_type_split_plt.fr.html", include_plotlyjs="directory")

pv_type_annual_rel_plt.write_html("docs/germany/figures/pv_type_split_rel_plt.de.html", include_plotlyjs="directory")
pv_type_annual_rel_plt_EN.write_html("docs/germany/figures/pv_type_split_rel_plt.en.html", include_plotlyjs="directory")
pv_type_annual_rel_plt_FR.write_html("docs/germany/figures/pv_type_split_rel_plt.fr.html", include_plotlyjs="directory")

pv_size_split_plt.write_html("docs/germany/figures/pv_size_split_plt.de.html", include_plotlyjs="directory")
pv_size_split_plt_EN.write_html("docs/germany/figures/pv_size_split_plt.en.html", include_plotlyjs="directory")
pv_size_split_plt_FR.write_html("docs/germany/figures/pv_size_split_plt.fr.html", include_plotlyjs="directory")

pv_eeg_plt.write_html("docs/germany/figures/pv_eeg_plt.de.html", include_plotlyjs="directory")
pv_eeg_plt_EN.write_html("docs/germany/figures/pv_eeg_plt.en.html", include_plotlyjs="directory")
pv_eeg_plt_FR.write_html("docs/germany/figures/pv_eeg_plt.fr.html", include_plotlyjs="directory")

pv_map.write_html("docs/germany/figures/pv_maps.de.html",include_plotlyjs="directory")
pv_map_EN.write_html("docs/germany/figures/pv_maps.en.html",include_plotlyjs="directory")
pv_map_FR.write_html("docs/germany/figures/pv_maps.fr.html",include_plotlyjs="directory")

fig_pv_butterfly_DE.write_html("docs/germany/figures/fig_pv_butterfly.de.html", include_plotlyjs="directory")
fig_pv_butterfly_EN.write_html("docs/germany/figures/fig_pv_butterfly.en.html", include_plotlyjs="directory")
fig_pv_butterfly_FR.write_html("docs/germany/figures/fig_pv_butterfly.fr.html", include_plotlyjs="directory")

#%%#################################################################################################
# Onshore wind deep dive
###################################################################################################
#wind_data  = pd.read_csv("docs/germany/data/wind_on_mastr_data.csv")

#def wind_on_geo_split(data):
#    # compute net addtions by German state
#    data["date"] = pd.to_datetime(data["Inbetriebnahmedatum"])
#    additions = data.set_index("date")
#    additions["month"] = additions.index.month
#    additions["year"] = additions.index.year
#    additions = additions.groupby(["month","year","Bundesland"]).agg({'Bruttoleistung':"sum"}).reset_index()

#    data["date"] = pd.to_datetime(data["DatumEndgueltigeStilllegung"])
#    reductions = data.set_index("date")
#    reductions["month"] = reductions.index.month
#    reductions["year"] = reductions.index.year
#    reductions = reductions.groupby(["month","year","Bundesland"]).agg({'Bruttoleistung':"sum"}).reset_index()#

#    tmp = additions.merge(reductions,how="left",on=["month","year","Bundesland"])
#    tmp.columns = ['month','year','Bundesland','additions','reductions']
#    tmp['net_additions'] = tmp["additions"] - tmp["reductions"].fillna(0)
#    tmp["date"] = pd.to_datetime(dict(year=tmp.year,month=tmp.month,day = 1))
#    tmp = tmp.sort_values('date')
#    tmp['cum_net_additions'] = tmp.groupby('Bundesland')['net_additions'].cumsum()#

#    tmp = tmp.query("year>=2000")
    # create cumulative area chart
#    cum_additions = tmp[['date','Bundesland','cum_net_additions']]


#    cum_additions = cum_additions.pivot(index=["date"],columns="Bundesland",values="cum_net_additions").fillna(method="ffill")

size_data = pd.read_csv("docs/germany/data/wind_on_dd_cum_per_state.csv")
size_data['date'] = pd.to_datetime(size_data['date'])
laender = size_data.columns
laender = size_data.iloc[-1].reset_index().drop(0).sort_values(by=size_data.iloc[-1].reset_index().columns[1],ascending=False).reset_index()['index']

l12m_data = pd.read_csv("docs/germany/data/wind_on_dd_last12M_per_state.csv")
l12m_data["Total"] = "Letzte 12 Monate"
l12m_data = l12m_data.pivot(index="Total",columns="Bundesland",values="Last 12 M rel").reset_index()

state_order = size_data.iloc[-1].reset_index().drop(0).sort_values(by=size_data.iloc[-1].reset_index().columns[1],ascending=False)['index']

power_data = pd.read_csv("docs/germany/data/wind_on_dd_cum_by_size.csv")
power_data["date"] = pd.to_datetime(power_data["date"],dayfirst=True)

p_data_abs = pd.read_csv("docs/germany/data/wind_on_dd_2017_monthly_by_size.csv")
p_data_abs["date"] = pd.to_datetime(p_data_abs["date"],dayfirst=True)

potential_data = pd.read_csv("docs/germany/data/wind_on_dd_potential_by_state.csv")

# attributes
att_data = pd.read_csv("docs/germany/data/wind_on_dd_attributes_trailing.csv")

color_palette = [
    "#E87352",  # Warm Coral
    "#FDBA8F",  # Apricot
    "#FFC94D",  # Sunflower Yellow
    "#AAB593",  # Dusty Green
    "#A9D9C7",  # Aqua
    "#5EAAA8",  # Teal
    "#F7D394",  # Pale Gold
    "#FCD9A0",  # Light Amber
    "#9FC68E",  # Moss Green
    "#E3AE8A",  # Terra Cotta
    "#FFB16D",  # Melon
    "#8E9AA5",  # Slate Blue
    "#C2877B",  # Rusty Brown
    "#E7998C",  # Coral Pink
    "#7BA5C8",  # Cornflower Blue
    "#D5C2C3",  # Rose Taupe
]

#%% Functions to create figures

def create_soeder_figure(size_data,l12m_data,color_palette):

    fig = make_subplots(rows=1,
                        cols=2,
                        shared_yaxes=True,
                        column_widths=[0.8,0.2],
                        column_titles=["Anteile der Bundesländer an der <br> insgesamt installierten Windkraftleistung",
                                    "...und am Zubau <br> der letzten 12 Monate"],
#                        horizontal_spacing = 0.01
    )

    for l in range(16):
        fig.add_trace(go.Scatter(x=size_data['date'],y=100*size_data[laender[l]],
                                fill="tonexty",
                                stackgroup="one",
                                mode="none",
                                name = laender[l],
                                hovertemplate='%{y:.2f}%',
                                fillcolor = color_palette[l]),row=1,col=1)
        fig.add_trace(go.Bar(x=l12m_data["Total"],y=100*l12m_data[laender[l]],
                            showlegend = False,
                            name = laender[l],
                            marker_color = color_palette[l],
                            hovertemplate='%{y:.2f}%',
                            #base = 0
                            ),row=1,col=2)
    fig.update_yaxes(range=[0,100])
    fig.update_yaxes(visible=False,row=1,col=2)
    fig.update_layout(yaxis_ticksuffix="%",legend={'traceorder':'reversed'},
                    hovermode = 'x unified',
                    template = "simple_white",
                    barmode="relative",
                    margin = dict(l=0,r=0,b=0,t=50,pad=0),                    
                    height=400)
    return fig



def create_attribute_figure(att_dta,color_palette):
    tmp = att_dta
    tmp["date"] = pd.to_datetime(tmp["date"],dayfirst=True)

    fig = make_subplots(specs=[[{'secondary_y':True}]])
    fig.add_trace(go.Scatter(x=tmp["date"],
                             y=tmp["Rotor diameter"],
                             name = "Rotordurchmesser",
                             mode="lines",
                             marker_color = color_palette["first"],
                             hovertemplate='%{y:.0f} m',
                             ),secondary_y=False)
    fig.add_trace(go.Scatter(x=tmp["date"],
                             y=tmp["Hub height"],
                             name = "Nabenhöhe",
                             marker_color = color_palette["third"],
                             hovertemplate ='%{y:.0f} m',
                             mode="lines"),secondary_y=False)
    fig.add_trace(go.Scatter(x=tmp["date"],y=tmp["Average power"],marker_color = color_palette["fourth"],name = "Leistung",hovertemplate='%{y:.2f} MW',mode="lines"),secondary_y=True)

    fig.update_yaxes(title="Meter",secondary_y=False)
    fig.update_yaxes(title="Megawatt",secondary_y=True)
    fig.update_layout(template = "simple_white",title = "Größenentwicklung neu installierter Windkraftanlagen",
        hovermode="x unified")

    return fig

def create_size_dist_figure(size_data,abs_size,color_palette):

    size_classes = power_data.columns[1:7]

    fig = make_subplots(rows=1,
                        cols=2,
                        #shared_yaxes=True,
                        column_widths=[0.4,0.6],
                        column_titles=["Anteile Größenklassen an der <br> installierten Leistung",
                                    "Monatlicher Zubau <br> nach Größenklassen"],
                        horizontal_spacing = 0.1
    )
    #fig = go.Figure()
    for l in range(6):
        fig.add_trace(go.Scatter(x=size_data['date'],y=100*size_data[size_classes[l]],
                                fill="tonexty",
                                stackgroup="one",
                                mode="none",
                                name = size_classes[l],
                                hovertemplate='%{y:.2f}%',
                                fillcolor = color_palette[l]),row=1,col=1)
        fig.add_trace(go.Bar(x=abs_size["date"],y=abs_size[size_classes[l]],
                            showlegend = False,
                            name = size_classes[l],
                            marker_color = color_palette[l],
                            hovertemplate='%{y:.2f} MW',
                            ),row=1,col=2)
    fig.update_yaxes(range=[0,100],row=1,col=1)
    fig.update_yaxes(title="Megawatt",row=1,col=2)
    fig.update_layout(yaxis_ticksuffix="%",legend={'traceorder':'reversed',"yanchor":"top","xanchor":"right"},
                    hovermode = 'x unified',
                    template = "simple_white",
                    barmode="relative",
                    margin = dict(l=0,r=0,b=0,t=50,pad=0),
                    height=400)
    return fig

def create_wind_butterfly_chart(potential_data):
        
    fig = make_subplots(rows=1,cols=2,
                        shared_xaxes=False,
                        shared_yaxes=True,
                        horizontal_spacing=0.00,
                        subplot_titles=["",""])

    fig.add_trace(go.Bar(
        y = potential_data["Bundesland"], x = potential_data["Installiert"],
        name = "Installierte Leistung in GW",
        marker_color = colors.standard["first"],
        orientation = "h",
        offsetgroup = 1,
#        marker_pattern_shape = "+",
        legendgroup = "rooftop",
        hovertemplate = "%{x:.2f} GW"),
    row = 1, col = 1)
    
    fig.add_trace(go.Bar(
        y = potential_data["Bundesland"], x = 100*potential_data["Anteil am Scenario"],
        name = "Relativ zu Ariadne-Szenario 2045 in %",
        marker_color = colors.standard["second"],
        orientation = "h",
        offsetgroup = 1,
#        marker_pattern_shape = "+",
        legendgroup = "rooftop",
        hovertemplate = "%{x:.2f} %"),
    row = 1, col = 2)

    
    fig.update_xaxes(title_text=None,row=1,col=1, range=[15,0])
    #fig.update_xaxes(title_text=None,row=1,col=2,range=[0.29,0.00],tickformat = ',.0%')
    fig.update_xaxes(title_text=None,row=1,col=2,range=[0.01,105],ticksuffix="%")
    fig.update_yaxes(dtick=1)
    fig.update_yaxes(visible=False,row=1,col=2)
    #fig.update_yaxes(visible=False,row=1,col=3)
    
    fig.update_xaxes(showgrid=True, gridwidth=0.1, gridcolor='Lightgray')
    
    fig.update_layout(
        title = {"text": "Installierte Windkraft-Leistung nach Bundesländern", "y": 0.99},
        margin = dict(l=0,r=0,b=0,t=60,pad=0),
        font = dict(family = "'sans-serif','arial'", size=12, color='#000000'),
        hovermode = "y unified",
        barmode = "stack",
        template = "simple_white",
        legend={"yanchor":"bottom","xanchor":"left","y":0.95,"x":0.05,})
    
    return fig

#%% Create figures

size_share_fig = create_size_dist_figure(power_data,p_data_abs,color_palette)

state_share_fig = create_soeder_figure(size_data,l12m_data,color_palette)
attributes_fig = create_attribute_figure(att_data,colors.standard)
size_share_fig = create_size_dist_figure(power_data,p_data_abs,color_palette)
butterfly_wind_fig = create_wind_butterfly_chart(potential_data)

size_share_fig_EN = copy.deepcopy(size_share_fig)
size_share_fig_EN['layout']['annotations'][0]['text'] = 'Size class shares of <br> installed capacity'
size_share_fig_EN['layout']['annotations'][1]['text'] = 'Monthly additions <br> by size class'
size_share_fig_EN['layout']['yaxis2']['title']['text'] = 'Megawatts'

state_share_fig_EN = copy.deepcopy(state_share_fig)
states = ["Lower Saxony",'Brandenburg',"Schleswig-Holstein",'North Rhine-Westphalia',"Saxony-Anhalt","Rhineland-Palatinate","Mecklenburg-West Pomerania",'Bavaria',"Hesse","Thuringia",'Baden-Württemberg',"Saxony","Saarland",'Bremen','Hamburg','Berlin']
state_share_fig_EN['layout']['annotations'][0]['text'] = 'State shares of <br> installed onshore wind capacity'
state_share_fig_EN['layout']['annotations'][1]['text'] = '...and of the last 12 months <br> additions'

for i in range(16):
    state_share_fig_EN['data'][2*i]['name'] = states[i]
    state_share_fig_EN['data'][2*i+1]['name'] = states[i]
    state_share_fig_EN['data'][2*i+1]['x'] = ['Last 12 months']
    
attributes_fig_EN = copy.deepcopy(attributes_fig)
attributes_fig_EN['layout']['title']['text'] = "Turbine size development of newly installed capacity"
attributes_fig_EN['layout']['yaxis']['title']['text'] = 'Meters'
attributes_fig_EN['layout']['yaxis2']['title']['text'] = 'Megawatts'
attributes_fig_EN['data'][0]['name'] = 'Rotor diameter'
attributes_fig_EN['data'][1]['name'] = 'Hub height'
attributes_fig_EN['data'][2]['name'] = 'Average power'

butterfly_wind_fig_EN = copy.deepcopy(butterfly_wind_fig)
butterfly_wind_fig_EN['layout']['title']['text'] = 'Installed onshore wind capacity by German state'
butterfly_wind_fig_EN['data'][0]['y'] = sorted(states)
butterfly_wind_fig_EN['data'][1]['y'] = sorted(states)
butterfly_wind_fig_EN['data'][0]['name'] = 'Installed capacity in GW'
butterfly_wind_fig_EN['data'][1]['name'] = 'Relative to Ariadne scenario 2045 in %'

#%% Save figures

state_share_fig.write_html("docs/germany/figures/wind_on_dd_state_shares.de.html", include_plotlyjs="directory")
state_share_fig_EN.write_html("docs/germany/figures/wind_on_dd_state_shares.en.html", include_plotlyjs="directory")

size_share_fig_EN.write_html("docs/germany/figures/wind_on_dd_size_shares.en.html", include_plotlyjs="directory")
size_share_fig.write_html("docs/germany/figures/wind_on_dd_size_shares.de.html", include_plotlyjs="directory")

attributes_fig.write_html("docs/germany/figures/wind_on_dd_attributes.de.html", include_plotlyjs="directory")
attributes_fig_EN.write_html("docs/germany/figures/wind_on_dd_attributes.en.html", include_plotlyjs="directory")

butterfly_wind_fig.write_html("docs/germany/figures/wind_on_dd_butterfly_wind.de.html", include_plotlyjs="directory")
butterfly_wind_fig_EN.write_html("docs/germany/figures/wind_on_dd_butterfly_wind.en.html", include_plotlyjs="directory")

#%%#################################################################################################
# RE Shares Heat ###################################################################################
####################################################################################################

KN45_share_RE = pd.read_csv("docs/germany/data/heat_resshares_scenarios.csv")

# create a figure object
KN45_bld_share_RE = go.Figure()

KN45_bld_share_RE = go.Figure()
# add the first variable as a line to the figure
KN45_bld_share_RE.add_trace(go.Scatter(x=KN45_share_RE["Jahr"], y=KN45_share_RE["Kopernikus_REtotal"], name="Ariadne KN 2045 - EE gesamt", line=dict(color='purple'),
                           legendgroup="group1"))

# add the second variable as a line to the figure
KN45_bld_share_RE.add_trace(go.Scatter(x=KN45_share_RE["Jahr"], y=KN45_share_RE["Prognos_REtotal"], name="Agora KN 2045 - EE gesamt", 
                           line=dict(color='darkorange'), legendgroup="group2"))

# create stacked bar charts for each year Prognos
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Heatpumps"], 
                           name="Agora KN 2045 - Wärmepumpen" if i == 0 else None, showlegend=i==0,
                           offsetgroup=0, width=0.4, marker_color='rgb(255, 140, 0)', 
                           legendgroup="group3"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Districtheating"], 
                           name="Agora KN 2045 - Fernwärme" if i == 0 else None, showlegend=i==0,
                           offsetgroup=0, width=0.4, marker_color='rgb(255, 165, 0)', 
                           legendgroup="group3"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Biomass"], 
                           name="Agora KN 2045 - Biomasse" if i == 0 else None, showlegend=i==0,
                           offsetgroup=0, width=0.4, 
                           marker_color='rgb(255, 187, 51)', legendgroup="group3"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"], y=KN45_building_ph_ghg_year["Prognos_Solarthermal"], 
                           name="Agora KN 2045 - Solarthermie" if i == 0 else None, showlegend=i==0,
                           offsetgroup=0, width=0.4, 
                           marker_color='rgb(255, 223, 178)', legendgroup="group3"))

# create stacked bar charts for each year BCG
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_Heat pumps"], 
                           name="BDI Klimapfade 2.0 - Wärmepumpen" if i == 0 else None, showlegend=i==0,
                           offsetgroup=2, width=0.4, marker_color='rgb(0, 100, 0)', 
                           legendgroup="group4"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_District heating"], 
                           name="BDI Klimapfade 2.0 - Fernwärme" if i == 0 else None, showlegend=i==0,
                           offsetgroup=2, width=0.4, marker_color='rgb(50, 205, 50)', 
                           legendgroup="group4"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 0.5, y=KN45_building_ph_ghg_year["BCG_GreenGases_Biomass"], 
                           name="BDI Klimapfade 2.0 - Grüne Gase, Biomasse, Power-to-Liquids" if i == 0 else None, showlegend=i==0,
                           offsetgroup=2, width=0.4, 
                           marker_color='rgb(0, 255, 0)', legendgroup="group4"))

# create stacked bar charts for each year McKinsey
for i, year in enumerate(KN45_share_RE["Jahr"].unique()):
    KN45_building_ph_ghg_year = KN45_share_RE[KN45_share_RE["Jahr"] == year]
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1 , y=KN45_building_ph_ghg_year["McKinsey_HeatPumps"], 
                           name="McKinsey KN 2045 - Wärmepumpen" if i == 0 else None, showlegend=i==0,
                           offsetgroup=3, width=0.4, marker_color='rgb(139, 0, 0)', 
                           legendgroup="group5"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1, y=KN45_building_ph_ghg_year["McKinsey_DistrictHeating "], 
                           name="McKinsey KN 2045 - Fernwärme " if i == 0 else None, showlegend=i==0,
                           offsetgroup=3, width=0.4, 
                           marker_color='rgb(255, 0, 0)', legendgroup="group5"))
    KN45_bld_share_RE.add_trace(go.Bar(x=KN45_building_ph_ghg_year["Jahr"] + 1, y=KN45_building_ph_ghg_year["McKinsey_BGasSolarH2O"], 
                           name="McKinsey KN 2045 - Biogas, Solarthermie, Wasserstoff " if i == 0 else None, showlegend=i==0,
                           offsetgroup=3, width=0.4, 
                           marker_color='rgb(255, 150, 150)', legendgroup="group5"))


# layout
KN45_bld_share_RE.update_layout(
    title="Anteil Erneuerbare Energien im Wärme- und Gebäudesektor (Wohn- und Nichtwohngebäude) <br> in verschiedenen Szenarien",
    xaxis_title="Jahr",
    yaxis_title="Prozentualer Anteil",
    template="simple_white",
    barmode="stack",
    margin=dict(l=0, r=0, t=70, b=0, pad=0),
    font=dict(family = "'sans-serif','arial'", size=12, color='#000000'),
    yaxis=dict(tickformat="0%"),
    legend=dict(
        traceorder="normal",
        font=dict(
            family="'sans-serif','arial'",
            size=11,
            color='#000000'
        ),
        orientation="h",
        x=1, 
        y=1, 
        itemclick="toggleothers",
        itemsizing="constant",
        #items=[
        #    dict(
        #        label="Ariadne KN 2045",
        #        group="group1"
        #    ),
        #    dict(
        #        label="Agora KN 2045",
        #        group=["group2", "group3"]
        #    ),
        #    dict(
        #        label="BDI Klimapfade 2.0",
        #        group="group4"
        #    ),
        #    dict(
        #        label="McKinsey KN 2045",
        #        group="group5"
        #    )
        #],
        #title=dict(
        #    text="Legend Groups",
        #    side="top"
        #),
    ),
)

KN45_bld_share_RE.write_html("docs/germany/figures/heat_resshares_scenarios.de.html", include_plotlyjs="directory")

# %%
###############################################################################
## RES Auctions
###############################################################################

# Function to download auction data
def get_table_from_url(url):
    skip = 5 if 'Solar2' in url else 6
    skip = 7 if 'solar1' in url else skip
    footer = 3 if 'solar1' in url else 2
    data = pd.read_excel(url,sheet_name='Übersicht',skiprows=skip,skipfooter=footer)
    df = pd.DataFrame({'second' : data.iloc[0,]}).reset_index()
    # Replace 'Unnamed' in 'index' column by previous entry
    df['index'] = df['index'].where(~df['index'].str.contains('Unnamed'), df['index'].shift())
    df['index'] = df['index'].where(~df['index'].str.contains('Unnamed'), df['index'].shift())


    # Append 'second' to 'index' if 'second' is not NaN or NaT
    df['index'] = df.apply(lambda row: row['index'] + ' ' + str(row['second']) if pd.notnull(row['second']) else row['index'],axis=1)
    df['index'] = df['index'].apply(lambda row: re.sub(r'[0-9]','',row))
    df['index'] = df['index'].str.replace('.', '')
    df['index'] = df['index'].str.replace('\n', '')
    data.columns = df['index']
    data.drop(axis=0,index=0,inplace=True)

    # Deal with duplicate columns
    test = data['Zuschlagsmenge (kW)']
    if len(test.columns) > 2:
        test.columns = ['Initial','Mit Sicherung','Nachtrag']
    else:
        test.columns = ['Mit Sicherung','Nachtrag']
    test['Zuschlagsmenge (kW)'] = test["Mit Sicherung"] + test['Nachtrag'].fillna(0)
    data['Zuschlagsvolumen (kW)'] = test['Zuschlagsmenge (kW)']

    return data

# Function to build auction chart
def create_auction_plot(data, technology,cpi_data):
    fig_dta = data[['Gebotstermin','Ausschreibungsvolumen (kW)','Zuschlagsvolumen (kW)','Zuschlagswert (ct/kWh) Gew Mittel','Höchstwert (ct/kWh)','Preisregel','Gebotswerte mit Zuschlag (ct/kWh) Min','Gebotswerte mit Zuschlag (ct/kWh) Max']]
    fig_dta['Gebotstermin'] = pd.to_datetime(fig_dta['Gebotstermin'])
    fig_dta = fig_dta.set_index('Gebotstermin').merge(cpi_data,how='left',left_index=True,right_index=True)
    fig_dta = fig_dta.reset_index()
    fig_dta['CPI'] = fig_dta['CPI']/100
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Höchstwert (ct/kWh)'],
                                mode='none',
                            customdata=fig_dta['Preisregel'],
                            showlegend=False,
                            name = 'Preisregel',
                                hovertemplate='%{customdata}',
                                marker=dict(color="#c37130",symbol='x',size=10)),
                                secondary_y=True,
                                )
    fig.add_trace(go.Bar(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Ausschreibungsvolumen (kW)']/1e3,
                            customdata=fig_dta['Preisregel'],
                            name='Auschreibungsmenge',
                            marker=dict(color="#3c435a",),
                            hovertemplate='%{y:.2f} MW'),
                            secondary_y=False)
    fig.add_trace(go.Bar(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Zuschlagsvolumen (kW)']/1e3,
                            name='Zuschlagsmenge',
                            marker=dict(color="#2A6A89"),
                            hovertemplate='%{y:.2f} MW'),
                            secondary_y=False)
    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Höchstwert (ct/kWh)']/fig_dta['CPI'],
                                mode='markers',
                            customdata=fig_dta['Preisregel'],
                                name='Höchstwert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kWh',
                                marker=dict(color="#c37130",symbol='x',size=10),visible = "legendonly"),
                                secondary_y=True)

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'],
                            y=fig_dta['Zuschlagswert (ct/kWh) Gew Mittel']/fig_dta['CPI'],
                            mode='markers',
                            name='Gew. Zuschlagswert, 2020ct/kWh',
                            hovertemplate='%{y:.2f} ct/kWh',
                            marker=dict(color="#ac452f",size=10)),
                            secondary_y=True)

    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Gebotswerte mit Zuschlag (ct/kWh) Min']/fig_dta['CPI'],
                                mode='markers',
                            #customdata=fig_dta['Preisregel'],
                            name = 'Minimum Zuschlagswert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kW',
                                marker=dict(size=5,color="#ac452f",opacity=0.7),visible = "legendonly"),
                                secondary_y=True)    
    fig.add_trace(go.Scatter(x=fig_dta['Gebotstermin'], 
                                y=fig_dta['Gebotswerte mit Zuschlag (ct/kWh) Max']/fig_dta['CPI'],
                                mode='markers',
                            #customdata=fig_dta['Preisregel'],
                            name = 'Maximum Zuschlagswert, 2020ct/kWh',
                                hovertemplate='%{y:.2f} ct/kWh',
                                marker=dict(size=5,color="#ac452f",opacity=0.7),visible = "legendonly"),
                                secondary_y=True)                                      
    fig.update_yaxes(title=technology+", MW",linecolor='black',ticks='outside',range = [0,3500],secondary_y=False)
    fig.update_yaxes(title="2020ct/kWh",linecolor='black',ticks='outside',range = [0,12],secondary_y=True,showgrid=False)

    fig.update_layout(
        xaxis= dict(title='',tickangle=-45, tickmode='array',tickfont=dict(color='black'),tickvals=fig_dta['Gebotstermin'].tolist(),ticktext=fig_dta['Gebotstermin'].dt.strftime('%b %Y').tolist(),linecolor='black',ticks='outside'),
        #yaxis = dict(title='',linecolor='black',ticks='outside'),
        plot_bgcolor='white',
        hovermode         = "x unified",
        legend = {'x': 0 , 'y': 1},
    )
    leg_dict = {"show"   : True,
                      "x0"     : "2021-12",
                      "x1"     : "2025-11", 
                      "text"   : "Legislaturperiode",
                      "color"  : "first",
                      "opacity": 0.1,
                      }
    fig.add_vrect(x0                   = leg_dict["x0"],
                x1                   = leg_dict["x1"],
                fillcolor            = "#3c435a",
                opacity              = 0.1,
                layer                = "below",
                annotation_text      = "Legislaturperiode",
                annotation_font_size = 12,
                annotation_position  = "top right",
                row                  = "all",
                col                  = "all")

    fig.update_layout(
        legend=dict(
            x=0.01,
            y=1,
            title_font_family='arial',
            font=dict(
                family="arial",
                size=14,
                color="black"
            ),
            borderwidth=1
        )
    )
    return fig

def create_annual_auction_vols(data,technology):
    fig_dta = data[['Gebotstermin','Ausschreibungsvolumen (kW)','Zuschlagsvolumen (kW)','Zuschlagswert (ct/kWh) Gew Mittel','Höchstwert (ct/kWh)','Preisregel','Gebotswerte mit Zuschlag (ct/kWh) Min','Gebotswerte mit Zuschlag (ct/kWh) Max']]
    fig_dta['Gebotstermin'] = pd.to_datetime(fig_dta['Gebotstermin'])
    fig_dta = fig_dta.set_index('Gebotstermin').merge(cpi_data,how='left',left_index=True,right_index=True)
    fig_dta = fig_dta.reset_index()
    fig_dta['CPI'] = fig_dta['CPI']/100

    fig_dta['year'] = fig_dta['Gebotstermin'].dt.year
    fig_dta = fig_dta.groupby('year').agg({'Ausschreibungsvolumen (kW)':'sum','Zuschlagsvolumen (kW)':'sum'})/1e6
    fig_dta = fig_dta.reset_index()
    fig_dta = fig_dta.rename(columns={'Ausschreibungsvolumen (kW)':'Ausschreibungsmengen (GW)','Zuschlagsvolumen (kW)':'Zuschlagsmengen (GW)'})
    fig = go.Figure()
    fig.add_trace(go.Bar(x=fig_dta['year'],y=fig_dta['Ausschreibungsmengen (GW)'],name='Ausschreibungsmengen, GW',marker_color='#3c435a',hovertemplate='%{y:.2f} GW'))
    fig.add_trace(go.Bar(x=fig_dta['year'],y=fig_dta['Zuschlagsmengen (GW)'],name='Zuschlagsmengen, GW',marker_color='#2A6A89',hovertemplate='%{y:.2f} GW'))
    fig.update_yaxes(title=technology+", MW",linecolor='black',ticks='outside',range = [0,10])

    fig.update_layout(
        xaxis= dict(title='',tickangle=-45, tickmode='array',tickfont=dict(color='black'),linecolor='black',ticks='outside',tickformat = '%{y:0f}',showgrid=False),
        #yaxis = dict(title='',linecolor='black',ticks='outside'),
        plot_bgcolor='white',
        hovermode         = "x unified",
        legend = {'x': 0 , 'y': 1},
    )
    leg_dict = {"show"   : True,
                      "x0"     : "2021-12",
                      "x1"     : "2025-11", 
                      "text"   : "Legislaturperiode",
                      "color"  : "first",
                      "opacity": 0.1,
                      }

    fig.update_layout(
        legend=dict(
            x=0.01,
            y=1,
            title_font_family='arial',
            font=dict(
                family="arial",
                size=14,
                color="black"
            ),
            borderwidth=1
        )
    )
    
    return fig

# Download auction data
url_wind_on ="https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Onshore.xlsx?__blob=publicationFile&v=2"
url_wind_on = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Onshore.xlsx?__blob=publicationFile&v=3"
url_pv_fr = "https://data.bundesnetzagentur.de/Bundesnetzagentur/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/statistik_solar1.xlsx"
url_pv_fr = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/statistik_solar1.xlsx?__blob=publicationFile&v=2"
url_pv_ad = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Solar2.xlsx?__blob=publicationFile&v=2"
url_pv_ad = "https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/Ausschreibungen/Statistiken/Statistik_Solar2.xlsx?__blob=publicationFile&v=3"

wind_on = get_table_from_url(url_wind_on)
pv_fr = get_table_from_url(url_pv_fr)
pv_ad = get_table_from_url(url_pv_ad)

# Download CPI data (this needs to be automated at some point)
cpi_data = pd.read_csv('docs/germany/data/cpi.csv')
cpi_data.Date = pd.to_datetime(cpi_data.Date,format='%d/%m/%Y')
cpi_data = cpi_data.set_index('Date')

# Create auction plots
figs = [create_auction_plot(wind_on,'Windkraft an Land',cpi_data),create_auction_plot(pv_fr,'PV Freifläche',cpi_data),create_auction_plot(pv_ad,'PV Aufdach',cpi_data)]

annual_figs = [create_annual_auction_vols(wind_on,'Windkraft an Land'),create_annual_auction_vols(pv_fr,'PV Freifläche'),create_annual_auction_vols(pv_ad,'PV Aufdach')]

# Translations
names = ['Pricing regime','Tender volume','Awarded volume','Bid ceiling, 2020ct/kWh','Weighted average awarded bid, 2020ct/kWh','Minimum awarded bid, 2020ct/kWh','Maximum awarded bid, 2020ct/kWh']
titles = ['Wind onshore','Ground-mounted PV','Rooftop PV']
noms = ['Régime de tarification',"Volume de l'offre","Volume mis aux enchères","Plafond de l'offre, 2020ct/kWh","Offre moyenne pondérée, 2020ct/kWh","Offre minimale adjugée, 2020ct/kWh","Offre maximale adjugée, 2020ct/kWh"]
titres = ['Éoliennes terrestres','PV au sol','PV sur toiture']
figs_EN = copy.deepcopy(figs)
figs_FR = copy.deepcopy(figs)
for k in range(2):
    figs_EN[k]['layout']['yaxis']['title'] = titles[k]
    figs_FR[k]['layout']['yaxis']['title'] = titres[k]
    for i in range(len(names)):
        figs_EN[k]['data'][i]['name'] = names[i]
        figs_FR[k]['data'][i]['name'] = noms[i]


# Write to html
figs[0].write_html("docs/germany/figures/wind_on_auction.de.html", include_plotlyjs="directory")
figs[1].write_html("docs/germany/figures/pv_fr_auction.de.html", include_plotlyjs="directory")
figs[2].write_html("docs/germany/figures/pv_ad_auction.de.html", include_plotlyjs="directory")

figs_EN[0].write_html("docs/germany/figures/wind_on_auction.en.html", include_plotlyjs="directory")
figs_EN[1].write_html("docs/germany/figures/pv_fr_auction.en.html", include_plotlyjs="directory")
figs_EN[2].write_html("docs/germany/figures/pv_ad_auction.en.html", include_plotlyjs="directory")

figs_FR[0].write_html("docs/germany/figures/wind_on_auction.fr.html", include_plotlyjs="directory")
figs_FR[1].write_html("docs/germany/figures/pv_fr_auction.fr.html", include_plotlyjs="directory")
figs_FR[2].write_html("docs/germany/figures/pv_ad_auction.fr.html", include_plotlyjs="directory")

annual_figs[0].write_html("docs/germany/figures/wind_on_auction_annual.de.html", include_plotlyjs="directory")
annual_figs[1].write_html("docs/germany/figures/pv_fr_auction_annual.de.html", include_plotlyjs="directory")
annual_figs[2].write_html("docs/germany/figures/pv_ad_auction_annual.de.html", include_plotlyjs="directory")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Import subscripts

from scripts.DE.deep_dive_pv import *

from scripts.DE.create_prices_DE import *

from scripts.DE.create_emobility_DE import *

from scripts.DE.create_storage_DE import *

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

##### Duplicate plotly.min.js for the EN and FR #####

# Specify the source file path
plotly_min_file = "docs/germany/figures/plotly.min.js"

# Specify the destination file path with a different name
destination_file_en = "docs/germany/figures/plotly.min.en.js"
destination_file_fr = "docs/germany/figures/plotly.min.fr.js"

# Copy the file to the destination with a different name
shutil.copyfile(plotly_min_file, destination_file_en)
shutil.copyfile(plotly_min_file, destination_file_fr)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
